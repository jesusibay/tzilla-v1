var usValidate = ''; 
var sitemode = '';

function setSitemode(value){
	sitemode = value;
}

function getSitemode(){
	return sitemode;
}

function setUsValidate(value){
	usValidate = value ;
}
function getUsValidate(){
	return usValidate;
}

function save_sales_orders(){
    var ajaxURL = get_mainLink()+'checkout/save_sales_orders';
    
    

}

function compute_for_total(){
	
	var total = 0;
    $('.computeall').each(function(){
    	var all = $(this).text().replace("$", "" );
    	var allVal = all.replace(",", "");
        total += parseFloat(  allVal );
    });
    
    var total2 = numberWithCommas(total.toFixed(2) );
     $(".total").text( total2 );


}

function ch_gray_btn(){
    $(".complete-order-btn").attr("disabled", true);
    $(".complete-order-btn").removeClass("green-btn");
    $(".complete-order-btn").addClass("gray-btn");
    $(".checker-box2").removeClass("selected");
   
}

function ch_green_btn(){
    $(".complete-order-btn").attr("disabled", false);
    $(".complete-order-btn").removeClass("gray-btn");
    $(".complete-order-btn").addClass("green-btn");
   
}
function validateNumber(event) { //validate numbers only
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
          return true;
        } else if ( key < 48 || key > 57 ) {
         return false;
        } else {
         return true;
        }
     }


function check_fields(){
	$(".cc-input").each(function(){
		if( $(this).val() == ''){
	
			 $(this).find(".error").removeClass("error");
			 $(this).parent().find(".error-label").remove();
			 $(this).parent().append( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' );
			 $(this).addClass("error");
	         if( $(this).parent().find(".error-label").size() > 1 ){
	
	        } 
	
			}
	});

	$(".error").eq(0).focus();

	
}

function update_customer_address(order_id){
    var ajaxURL = get_mainLink()+'checkout/save_customer_address/'+order_id;
    var address_type = $("#address_type").val();
    var customer_id = $("#customer_id").val();

    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
           	customer_id: customer_id,            
           	address_type: address_type
      },success: function( data ){
        var result = JSON.parse( data );
        if( result['status'] == 'success'){
            
        }

      },                 
      async:false
    });
}


function get_tax_amount(){
	var ajaxurl = get_mainLink()+'checkout/Shipping/get_tax';
	var subtotal = Number ($(".subtotal").html().replace("$", "") );
	$.ajax({ type: "POST",
			url : ajaxurl,
			data:{
				state: $("#state").val(),
				subtotal: subtotal
		 	},success: function(ndata){
			var data = JSON.parse(ndata);
			var taxAMt = data['tax_amount'].replace(",", "");
			$(".tax-fee").text("$"+ taxAMt );
	
			compute_for_total();
	
			$(".ship-nxt-loader").hide();
		},
	});										
}

function check_billing_fields(){
	$(".billing-input").each(function(){
		if( $(this).val() == ''){
			console.log("suoi");

			 $(this).find(".error").removeClass("error");
			 $(this).parent().find(".error-label").remove();
			 $(this).parent().append( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' );
			 $(this).addClass("error");
	         if( $(this).parent().find(".error-label").size() > 1 ){

	        } 


		}
	});

	$(".error").eq(0).focus();


}

function get_billing_customer_address(){
	
    
    var email = $("#cc-email").val();
    var address_type = $("#address_type").val();
    var firstname = $("#customer_bill_firstname").val();
    var lastname = $("#customer_bill_lastname").val();

    var add1 = $("#customer_bill_address").val();
    var add2 = '';
    var contact = $("#customer_bill_phone").val();
    var city = $("#customer_bill_city").val();
    var state = $("#customer_bill_state").val();
    var zip = $("#customer_bill_zipcode").val();
    var customer_id = $("#customer_id").val();
    var bill_user_data = [];
    
      bill_data = {
           	customer_id: customer_id,            
           	address_type: address_type,            
            contact_no: contact,
            email: email,
            firstname: firstname,
            lastname: lastname,
            address_1: add1,
            address_2: add2,
            city: city,
            state: state,
            zip: zip } ;
      
    return bill_data;
}

function get_customer_shipping(){
	var ship_data = [];

	var email = $("#ship_customer_email").val();
    var address_type = $("#address_type").val();
    var firstname = $("#ship_firstname").val();
    var lastname = $("#ship_lastname").val();

    var add1 = $("#ship_address").val();
    var add2 = '';
    var contact = $("#ship_phone").val();
    var city = $("#ship_city").val();
    var state = $("#ship_state").val();
    var zip = $("#ship_zipcode").val();
    var customer_id = $("#customer_id").val();

    ship_data= {
           	customer_id: customer_id,            
           	address_type: address_type,            
            contact_no: contact,
            email: email,
            firstname: firstname,
            lastname: lastname,
            address_1: add1,
            address_2: add2,
            city: city,
            state: state,
            zip: zip
    } ;
      
    return ship_data;
}



function save_customer_address(order_id){
    var ajaxURL = get_mainLink()+'checkout/save_customer_address/'+order_id;
    var email = $(".user-email").text();
    var address_type = $("#address_type").val();
    var firstname = $("#firstname").val();
    var lastname = $("#lastname").val();

    var add1 = $("#add1").val();
    var add2 = '';
    var contact = $("#contact_no").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var zip = $("#zip").val();
    var customer_id = $("#customer_id").val();

    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
           	customer_id: customer_id,            
           	address_type: address_type,            
            contact_no: contact,
            email: email,
            firstname: firstname,
            lastname: lastname,
            address_1: add1,
            address_2: add2,
            city: city,
            state: state,
            zip: zip
      },success: function( data ){
        var result = JSON.parse( data );
        if( result['status'] == 'success'){
            
        }

      },                 
      async:false
    });
}

function check_zip_validity(state, zip){
		if(state)
		{

		 	$.ajax({
				type: "POST",
				url: getUsValidate(),
		        data: {
		          	state: state,
		          	zip_code: zip
		        },
				dataType:'json'
				}).done(function( data ) 
				{				

					if(data=="true" || data ===true){ 

						$('#validation_passed_status').val("true");

						
					}else{
						
						ch_gray_btn();				

						$('#credit-card-notification').addClass('alert-danger alert');

						
						$('#validation_passed_status').val("false");

						$(".checker-box2").removeClass("selected");
						
						$('.paymentinput').removeAttr('checked');
							
						if( data == "Invalid Zip Code for California."){
							$('#customer_bill_zipcode').parent().find('.error').removeClass('error');
							$("#customer_bill_zipcode").parent().find('.error-label').remove();
							$("#customer_bill_zipcode").parent().append('<label class="error error-label">'+data+'</label></label>');
							
						}

						if($("#agreement_chkbox:checked" ).length) {
					       
							$('#cc-submit').removeClass('disabled');
							$('#cc-submit').prop('disabled', false);	
					    }
					    setTimeout(function() {
							$('#credit-card-notification').hide();
						}, 2000);
					}					
							
				});

		}else{
			$('#cc-submit').val('Complete order');
			$('#cc-submit').addClass('disabled');
			$('#cc-submit').prop('disabled', true);

			$('#validation_passed_status').val("false");

			$(".checker-box2").removeClass("selected");
						
			$('.paymentinput').removeAttr('checked');
			
			if($("#agreement_chkbox:checked" ).length) {
		       
				$('#cc-submit').removeClass('disabled');
				$('#cc-submit').prop('disabled', false);	
		    }

			
		}

	}


	$(function() {
	


		if( getSitemode() == "DEV" ){ 
			var w_endpnt = "stage";
			var client_id = 12231; //todo: load from database settings
	 	}else{ 
			var w_endpnt = "production";
			var client_id = 121048; //todo: load from database settings
		}

    WePay.set_endpoint(w_endpnt); // change to "production" when live


	$( "body" ).delegate( "#cc-submit", "click", function() {
	 

		check_fields();

	 	if( $('.radio-add-type:checked').val()=="Billing" ) {
	 		check_billing_fields();
	 	}
	 	var cc_expiration = $('#cc-expiration').val().split('/');
	 	if($('#cc-number').val().length === 0 )
	 	{
	 

		 	$("#cc-number").parent().find(".error").remove();
			$("#cc-number").parent().append('<label class="error error-label">'+$("#cc-number").attr('data-name')+' is required.</label></label>');
		 	 setTimeout(function() {
				$('.error').hide();
			}, 2000);

	 	}else if( $("#cc-name").val().length === 0 ){
	 		$("#cc-name").parent().find(".error").remove();
			$("#cc-name").parent().append('<label class="error error-label">'+$("#cc-name").attr('data-name')+' is required.</label></label>');

	 	} else if( $("#cc-email").val().length == 0 ) {

	 		 var email = $("#cc-email").val();
			  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
			  
			  var new_email = email.replace(/\s/g, "");
				$("#cc-email").val( new_email.trim() );

			  if($("#cc-email").val() != ''){

			    if (re.test(email)) {
			       	$("#cc-cvv").parent().find(".error").remove();
					$("#cc-cvv").parent().append('<label class="error error-label">'+$("#cc-cvv").attr('data-name')+' is required.</label></label>');
			    } else{
			      
				   
				    $("#cc-email").parent().find(".error").remove();
					$("#cc-email").parent().append('<label class="error error-label">'+$("#cc-email").attr('data-name')+' is required.</label></label>');  
			    }

			  }else{

				    $("#cc-email").parent().find('.error').remove();
				    $("#cc-email").parent().find('.error-label').last().remove();
			  }



	 	}else {
		 	//validate 
		 	var cc_expiration = $('#cc-expiration').val().split('/');
		 /*	if(cc_expiration.length < 2 || cc_expiration[0] == '' || cc_expiration[1] == '' ||
		 		cc_expiration[0] == '0' || cc_expiration[1] == '0'){
		 		// $('#credit-card-notification').html('Invalid credit card expiration. Follow this format: MM/YY.');	
		 		// $('#credit-card-notification').addClass('alert-danger alert');
		 		$("#cc-expiration").parent().parent().find(".error").remove();
				$("#cc-expiration").parent().parent().append('<label class="error error-label">Invalid credit card expiration. Follow this format: MM/YY.</label>');
		 		 setTimeout(function() {
					$('#credit-card-notification').hide();
					$('.error').remove();
				}, 2000);

		 	}else{	 */
		 	if(  $("#cc-cvv").val().length < 3 || $("#cc-cvv").val().length  > 4 ){
		 			// $('#credit-card-notification').html('Invalid credit card CVV.');	
		 			// $('#credit-card-notification').addClass('alert-danger alert');

		 			$("#cc-cvv").parent().find(".error").remove();
					$("#cc-cvv").parent().append('<label class="error error-label">Invalid credit card CVV.</label>');
		 			 setTimeout(function() {
							$('.error').remove();
						}, 2000);

		 		}else{

				 	/*var validator = $( "#BillingForm" ).validate();
					validator.form();

					if(validator.valid()){
						var v = is_fieldempty();

						var cc_expiration = $('#cc-expiration').val().split('/');
					*/
						var state = $('#ship_state').val();
						var zip = $("#cc-zip").val();

						if($('.radio-add-type:checked').val()=="Billing") state = $('#customer_bill_state').val();
						if($('.radio-add-type:checked').val()=="Billing") zip = $('#customer_bill_zipcode').val();
						
						check_zip_validity(state, zip);

						if($('#validation_passed_status').val() == "true"){
							var v = is_fieldempty();
							if(v==true){
						        var userName = [$('#cc-name')].join(' ');

						            response = WePay.credit_card.create({
						            "client_id":        client_id,
						            "user_name":        $('#cc-name').val(),
						            "email":            $('#cc-email').val(),
						            "cc_number":        $('#cc-number').val(),
						            "cvv":              $('#cc-cvv').val(),
						            "expiration_month": cc_expiration[0],
						            "expiration_year":  cc_expiration[1],
						            "address": {
						                "zip": zip,
						                "region": state
						            }

						        }, function(data) {
						        	
						        	
						            if (data.error) {
						                // handle error response
						                var error_message = data.error_description;			               
						                var lastChar = error_message[error_message.length -1];
						                
						                if(lastChar != '.') error_message = error_message +'.';

						                
						                
						                if( error_message == "Invalid credit card number." ){
						                	$("#cc-number").parent().find(".error").remove();
						                	$("#cc-number").parent().append('<label class="error error-label">'+error_message+'</label>');
						                	
						                }
						                if( error_message == "Full name is required." ){
						                	$("#cc-name").parent().find(".error").remove();
						                	$("#cc-name").parent().append('<label class="error error-label">'+error_message+'</label>');
						                	
						                }
						                if( error_message == "Invalid credit card expiration month."){
						                	$("#cc-expiration").parent().find(".error").remove();
						                	$("#cc-expiration").parent().append('<label class="error error-label">'+error_message+'</label>');
						                } 
						               
						                if( error_message == "That credit card expiration date has already passed." ){
						                	$("#cc-expiration").parent().find(".error").remove();
						                	$("#cc-expiration").parent().append('<label class="error error-label">Expiration date has already passed.</label>');
						                }

						             /* if ($(".checker-box2").hasClass('selected')) {
											
											 ch_green_btn();
										}else{
											ch_gray_btn();
											
										}*/
										ch_gray_btn();

						                $('#credit-card-notification').addClass('alert-danger alert');
						                 setTimeout(function() {
											$('#credit-card-notification').hide();
										}, 2000);
						                $('#validation_passed_status').val("false");

						            } else {	
						            	
							            	do_payment(data.credit_card_id);	
				            		 	 
						            }

						        });
						    }else{
						    	
						    	$("#cc-expiration").parent().parent().find(".error").remove();
								$("#cc-expiration").parent().parent().append('<label class="error error-label">'+v+'.</label>');

						    	// $('#credit-card-notification').addClass('alert-danger alert');
						    	// $('#credit-card-notification').html(v);
						    	 setTimeout(function() {
									$('#credit-card-notification').hide();
								}, 2000);
						    }
						}
					/*}else{
						
					}*/

		 		}
			// }
		}
	});   

    /*todo: change this with jquery validation*/
    var is_fieldempty = function(){
    	
		var v = true;
	    $("input.required").each(function(){
	        if ($.trim($(this).val()).length == 0 && v==true){
	            $(this).addClass("highlight");
	            console.log('asd');
	            // v = $(this).attr('data-name')+" is required.";
	        }
	        else{
	            $(this).removeClass("highlight");	           
	        }

	    });
	    
	    return v;
    };

    //todo: jquery validation
    var do_payment = function (credit_card_id) {
	
		var url = get_mainLink()+'checkout/save_sales_order';
		
		var userdata = "" ;
		if( $('#address_type').val() == "Billing" ){
			userdata =  get_billing_customer_address();
		}else{
			userdata =  get_customer_shipping();
		}

		$('#credit-card-notification').removeClass('alert-danger alert').html("");

		$('#cc-submit').val('Processing...');
		$('#cc-submit').addClass('disabled');
		$('#cc-submit').prop('disabled', true);
		$(".order-btn-loader").show();
		
		var values = $('#ShippingForm').serialize();
		values += "&" + $('#BillingForm').serialize();		
		values += "&credit_card_id=" +credit_card_id;
		values += "&bill_email="+$('#cc-email').val();	

		$.ajax({
			type: "POST",
			url: url,
			data: values, 
			dataType:'json'
			}).done(function( data ) 
			{				
				
				if(data.status=="success"){
				
				// 	console.log('payment accepted');
					// window.location.href = get_mainLink()+"checkout/thankyou";
				}else{
				// 	//TODO: show friendly error message. and further instruction

					
					$('#cc-submit').val('Complete order');
					$('#cc-submit').removeClass('disabled');
					$('#cc-submit').prop('disabled', false);

				 
					$('#credit-card-notification').addClass('alert-danger alert');

					$('#credit-card-notification').html(data.message);
				}
				window.location.href = get_mainLink()+"checkout/thankyou";
							
			});

	}

});


$(window).load(function() {
	
	$(".shipadd").click();
		

});

$(document).ready(function(){
	$("#storetitle").text('TZilla.com - Payment Details');
	$(document).on("click", ".payment-radio-cont2" , function(){
		$(".billing-input").parent().find(".error").remove();
		$(".checker-box2").toggleClass("selected");
		ch_gray_btn();
	})

	$(document).on('click','.shipadd', function () {
	   	$(".payment-billing-cont").slideUp();
		$(".payment-radio-cont2").addClass("hide-bill");
		ch_gray_btn();
	});
	
	$(document).on('click','.billadd', function () {
	   	$(".payment-billing-cont").slideDown();
		$(".payment-radio-cont2").removeClass("hide-bill");
		ch_gray_btn();
	});
	
	$(document).on("keyup", ".cc-input", function() {
		if( $(this).val() != "" ){
	        $(this).removeClass("error");
	        $(this).parent().find('.error-label').remove();
	        // $(".add-shipping-details").addClass('green-btn').removeClass("gray-btn");
	        // ch_green_btn();

   		}else{
   			// $(".add-shipping-details").removeClass('green-btn').addClass("gray-btn");
   			ch_gray_btn();
   		}
	});

	$(document).on("keyup", ".billing-input", function() {
		if( $(this).val() != "" ){
	        $(this).removeClass("error");
	        $(this).parent().find('.error-label').remove();
	        // $(".add-shipping-details").addClass('green-btn').removeClass("gray-btn");
	        // ch_green_btn();
   		}else{
   			// $(".add-shipping-details").removeClass('green-btn').addClass("gray-btn");
   			ch_gray_btn();
   		}
	});

	/*$(document).on("click", ".complete-order-btn", function(){
		 var address_type = $("#address_type").val();
		 if( address_type == 'Both'){
		 	// update_customer_address(125);
		 }else{
		 	// save_customer_address(125);
		 }
		
	});*/

	// $(document).on('click','.checker-box2', function () {
	//     $(this).toggleClass("selected");
	//     if( $(this).hasClass("selected") ){         
	//         $(".complete-order-btn").attr("disabled", false);
	//         $(".complete-order-btn").removeClass("gray-btn");
	//         $(".complete-order-btn").addClass("green-btn");
	//     }else{
	//         $(".complete-order-btn").attr("disabled", true);
	//         $(".complete-order-btn").removeClass("green-btn");
	//         $(".complete-order-btn").addClass("gray-btn");
	//     }
	//     });  

	$(document).on("click", '.radio-add-type', function() {		
		$("#address_type").val( $(this).val() );
		if( $('.radio-add-type:checked').val()=="Billing" ){
			
			// check_billing_fields();
		}else{
			// check_fields();
			$("#cc-zip").val( $( "#ship_zipcode" ).val() );
			$(this).parent().parent().parent().find('.error').removeClass('error');
			$(this).parent().parent().parent().find('.error-label').remove();
		}
	});

	/*$(document).on("blur", '#cc-cvv', function() {		
		if( $("#cc-cvv").val().length < 3 || $("#cc-cvv").val().length  > 4  ){
			$("#cc-cvv").parent().parent().find(".error").remove();
			$("#cc-cvv").parent().parent().append('<label class="error error-label">Please enter valid ccv.</label>');

		}else{
			$("#cc-cvv").parent().parent().find(".error").remove();
		}

	});

	$(document).on("keyup", '#cc-cvv', function() {	
		if( $("#cc-cvv").val().length < 3 || $("#cc-cvv").val().length  > 4 ){
			$("#cc-cvv").parent().parent().find(".error").remove();
			$("#cc-cvv").parent().parent().append('<label class="error error-label">Please enter valid ccv.</label>');
		} else{
			$("#cc-cvv").parent().parent().find(".error").remove();
		}

	});*/


	/*this is from old jquery*/


	var maskoptions =  {
	  onComplete: function(cep) {
	    //alert('CEP Completed!:' + cep);
	  },
	  onKeyPress: function(cep, event, currentField, options){
	    // console.log('An key was pressed!:', cep, ' event: ', event, 'currentField: ', currentField, ' options: ', options);
	  },
	  onChange: function(cep){
	    //console.log('cep changed! ', cep);
	  },
	  onInvalid: function(val, e, f, invalid, options){
	    var error = invalid[0];
	    
	  }
	};

	$('#cc-number').mask('0#', maskoptions);
	$('#cc-cvv').mask('0000', maskoptions);
	$('#cc-expiration').mask('00/00', maskoptions);
	$('#cc-zip').mask('00000');



	$(document).on('click','#agreement_chkbox_span', function(){
		//check_fields(); //checking for creditcard value
		// check_billing_fields();

		 $(".checker-box2").toggleClass("selected");
		
		

		if($('.radio-add-type:checked').val()=="Billing")
		{	
			state = $('#customer_bill_state').val();
			$('#cc-zip').val($('#customer_bill_zipcode').val());
			check_billing_fields();
			check_fields();
			
			check_zip_validity( state, $("#customer_bill_zipcode").val());
			// check_zip_validity(state, $( "#cc-zip" ).val());
				
				if( $(".error").size() == 0 ){

					 ch_green_btn();
					// $(".checker-box2").toggleClass("selected");
					// $('.paymentinput').prop('checked','checked');
					// save_customer_address();
					// $(".ship-nxt-loader").show();

					if( $(".checker-box2").hasClass("selected") ){
						ch_green_btn();
					}else{
						ch_gray_btn();
					}
				}else{
					ch_gray_btn();
					// $('.billing-input').removeAttr('checked');
					
				}
			
		} else{

			// $(".billing-input").find(".error").removeClass('error');
			// $(".billing-input").parent().find(".error-label").remove();
			var state = $('#ship_state').val();
			check_zip_validity(state, $( "#ship_zipcode" ).val());
			check_fields();

				
				if( $(".error").size() == 0 ){
					
					if( $(".checker-box2").hasClass("selected") ){
						ch_green_btn();
					}else{
						ch_gray_btn();
					}
					 ch_green_btn();
					// $(".checker-box2").toggleClass("selected");
					// $('.paymentinput').prop('checked','checked');
					// save_customer_address();
					// $(".ship-nxt-loader").show();
				}else{
					 ch_gray_btn();
					
				}
			
		}

		

	});

	$( "#cc-zip" ).blur(function() 
	{
		
		var state = $('#ship_state').val();

		if($('.radio-add-type:checked').val()=="Billing") state = $('#customer_bill_state').val();

		check_zip_validity(state, $( this ).val());
	});



	
	$(document).on("keyup", "#customer_bill_zipcode", function() {
		var zipcode = $(this).val();
		var state = $("#customer_bill_state").val();
		if( zipcode.length == 5 ){
			check_zip_validity( state, zipcode); 
		}
	});

	$(document).on("blur", "#customer_bill_zipcode", function() {
		var zipcode = $(this).val();
		var state = $("#customer_bill_state").val();
		if( $(this).val() != ''){			
			check_zip_validity( state, zipcode);
		}	
	});
	
	
	$('#customer_bill_state').blur(function(){
		$( "#cc-zip" ).val($('#customer_bill_zipcode').val());
		$("#customer_bill_state").parent().find(".error-label").remove();
	});

	$('#customer_bill_state').change(function(){
		$( "#cc-zip" ).val($('#customer_bill_zipcode').val());
		$("#customer_bill_state").parent().find(".error-label").remove();
	});

	$(document).on("click",".backtoShipping", function(){		
		// window.location.href =  get_mainLink()+"checkout/shipping";
		event.preventDefault();
   		history.back(1);
	});


	 
});
