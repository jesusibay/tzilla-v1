var group_id = 0;
var temp_img = "";
var assets_url = "";
var parent_id = 0;
var template_id = 0;
var product_details = '';
var all_shirt_colors = '';
var all_shirt_styles = '';
var sell_price = 0;
var min_price = '0.00';
var shirt_alternatives ='';
var sizeData = [];
var xhr;
var gen_Colors = '';
var school_id = 0;
var campaign_id= 0;

function encodeHTML(str){
	var encodedStr = str.replace(/[\u00A0-\u9999<>\&\'\"\;\#]/gim, function(i) { return '&#'+i.charCodeAt(0)+';'; });
	return encodedStr.replace(/&/gim, '&');
}

function decodeHTML(str){
  return str.replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
    	var num = parseInt(numStr, 10); // read num as normal number
   	 	return String.fromCharCode(num);
	});
}

function setCampaignID(value){
	campaign_id = value;
}

function getCampaignID(){
	return campaign_id;
}

function setSchoolId(value){
	school_id = value;
}
function getSchoolId(){
	return school_id;
}

function setGenColors(value){
	gen_Colors = value;
}

function getGenColors(){
	return gen_Colors;
}
function setSizeData( value ) {
	sizeData = value;
}

function getSizeData() {
	return sizeData;
}

function setProductDetails(value){
	product_details = value;
}

function getProductDetails(){
	return product_details;
}

function setAllColors(value){
	all_shirt_colors = value;
}

function getAllColors(){
	return all_shirt_colors;
}

function setAllStyles(value){
	all_shirt_styles = value;
}

function getAllStyles(){
	return all_shirt_styles;
}

function setStyleAlternatives(value){
	shirt_alternatives = value;
}

function getStyleAlternatives(){
	return shirt_alternatives;
}

function setShirtGroupId(val){
	 group_id = val;
}

function getShirtGroupId(){
	return group_id;
}
function setSellingPrice(value){
	sell_price = value;
}
function getSellingPrice(){
	return sell_price;
}

function setMinPrice(value){
	min_price = value;
}
function getMinPrice(){
	return min_price;
}

function setAssetUrl(value){
	 assets_url = value;
}

function getAssetUrl(){
	return assets_url;
}


function setLoadTempImg(val){
	temp_img = val;
}

function getLoadTempImg(){
	return temp_img;
}

function setParentId(value){
	parent_id = value;
}

function getParentId(){
	return parent_id;
}

function setTemplateId(value){
	template_id = value;
}

function getTemplateId(){
	return template_id;
}

function compareMintoSellPrice(){
		$(".float-error").remove();
		
	 	var minprice = Number ( $(".managecamp-detail-number").find(".min-price").html().replace("$", "") ) ;
	 	var sellprice = Number ( $(".managecamp-detail-number").find(".sell-price").val().replace("$", "") );
		var profit =   sellprice - minprice ;
		

		$('.design-display').find('.product-details[data-parent="'+getParentId()+'"]').each( function(){
	       var	dataprice = $(this).attr('data-price');          	
	       	var dataMIN = $(this).attr('data-min');          	

	       	var profit =   dataprice - dataMIN ;
	       	$('.item-details[splan-id="'+$(this).attr('data-shirt')+'"]').find('.managecamp-right-number .profit').text( '$'+profit.toFixed(2) );
	     	
	       
	       	if( dataprice < dataMIN && profit != 0  ){
	       		$('.item-details[splan-id="'+$(this).attr('data-shirt')+'"]').find(" .managecamp-right-number .profit").css('color' ,'#D12F19' );
	       	}else{
	       		$('.item-details[splan-id="'+$(this).attr('data-shirt')+'"]').find(" .managecamp-right-number .profit").css('color' ,'#000' );
	       	}
		});
		
	 	
}

function showStyles(data){
	 $(".show-all-styles").html( data['showstyles']);
}

function showAlternativeColors(){
	var dataAlternatives = JSON.parse( getStyleAlternatives() );
	if( dataAlternatives.length > 0 ){
		for (var i = 0 ; i < dataAlternatives.length ; i++ ){

		}
	}
}

function show_shirtstyles(data){
	 if( data.length > 0 ){
	 	$('.show-all-styles .tshirt-plan ').find('.addmorestyle-shirt-holder[shirt-id="'+data[x]['id']+'"]').removeClass(".recent-selected");

		$(".addmore-stylesec").html("");

		 for (var x = 0 ; x < data.length ;x++) {
		 	var groupname = data[x]['group_name'];
				groupname = groupname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
		 	$(".addmore-stylesec").append(
					'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 tshirtdisplay"><div class="cart-review-item-holder item-details"  splan-id="'+data[x]['id']+'" shirt-data=""><span class="review-title gray-darker gregular font-small name-plan s-style" stylegroup-id="">'+groupname+'</span><span class="cart-review-remove removeshirt" data-toggle="modal" data-target="#confirm" data-dismiss="modal"></span><div class="cart-review-shirt-holder tshirt-image"><img class="img-responsive img-center load-plan-shirt-image" src="'+get_mainLink()+data[x]['front_image_link']+'" alt="" /><div class="design-holder"><img class="img-responsive img-center load-temp-image" hex-id="" src="'+getLoadTempImg()+'" alt="" /></div></div><div class="managecamp-detail-holder"><span class="review-title gregular font-small" style="padding-top: 5px; padding-bottom: 5px;">Selected Colors</span><div class="managecamp-stylecolor-cont" style-planid="'+data[x]['id']+'"><div class="color-catcher"></div><div class="clearfix"></div></div></div></div><div class="clearfix"></div></div></div><div class="managecamp-detail-number"><div class="managecamp-left-number"><div class="blackz gsemibold font-small text-center">Min. Price</div><div class="blackz gregular font-small text-center min-price" id="min-price">$</div></div><div class="managecamp-center-number"><div class="blackz gsemibold font-small text-center">Selling Price</div><input type="text" class="form-control gregular font-small gray-darker text-center managecamp-sellingprice sell-price numberwithdecimal" maxlength="6" name="sell-price" id="sell-price" onkeypress="return isNumberKey(event)" value="$0.00"/></div><div class="managecamp-right-number"><div class="blackz gsemibold font-small text-center">Profit</div><div class="blackz gsemibold font-small text-center profit" id="profit">$0.00</div></div><div class="clearfix"></div></div></div></div>'

				);
		 }
	}
}

function show_defaultColors(){
	var dataSelectedcolors = $('.product-details[data-parent="'+getParentId()+'"]').attr("data-colors");	
	var colorsid = new Array();

	colorsid =  dataSelectedcolors.split(",");
	var dataColors = JSON.parse( getAllColors() );

	if(dataColors.length > 0 ){
		$(".color-catcher").html("");
		for( var i = 0 ; i < dataColors.length ; i++ ){
			
			 for ( var x = 0 ; x < colorsid.length ; x++ ){

			 	if( colorsid[x] ==  dataColors[i]['color_id'] ){

					$(".color-catcher").append(
		 				'<div class="selected-stylecolor" data-id="'+dataColors[i]['color_id']+'" title="'+dataColors[i]['color_name']+'" data-hex="'+dataColors[i]['hex_value']+'" style="background:#'+dataColors[i]['hex_value']+'"></div>'
					);
			 		
			 	}

			 }
		}
	}
}
	
function show_colors(data){ //display available colors for this shirt style
	var setSelected = "";
	if( data.length > 0 ){

		$(".colors-data").html("");

		 for (var i = 0; i < data.length ; i++ ){
		 	
		 	if ( $('.managecamp-stylecolor-cont[style-planid="'+getShirtGroupId()+'"] .color-catcher').find('.selected-stylecolor[data-id="'+data[i]['color_id']+'"]').length > 0 ) {
		 		setSelected = "selected-color-active";

		 	}else{		 		
			 	setSelected = "";
		 	}

		 	$(".colors-data").append(
		 		'<div class="selected-color '+setSelected+'" data-id="'+data[i]['color_id']+'" title="'+data[i]['color_name']+'" data-hex="'+data[i]['hex_value']+'"  style="background:#'+data[i]['hex_value']+';"><img class="img-responsive" src="'+getAssetUrl()+'/images/check-icon-xs.png'+'" alt="" /></div>'
		 	);
		 
		 }
	}else{
	
	}
}


function show_max_price(data){
	if(data.length > 0 ){
		for(var i= 0; i < data.length ; i++ ){
			for(var j= 0; j < data[i].length ; j++ ){
				if(data[i][j]['maxprice'] === null ){
					$('.item-details[splan-id="'+data[i][j]['id']+'"]').find(".min-price").html("$10.00");
					$('.design-display .product-details[data-shirt="'+data[i][j]['id']+'"]').attr("data-min", 10 );
					
				}else if(data[i][j]['maxprice'] === '0.00' ){
					$('.item-details[splan-id="'+data[i][j]['id']+'"]').find(".min-price").html("$10.00");
					$('.design-display .product-details[data-shirt="'+data[i][j]['id']+'"]').attr("data-min", 10 );
					
				}else{
					
					$('.design-display .product-details[data-shirt="'+data[i][j]['id']+'"]').attr("data-min", data[i][j]['maxprice'] );
					$('.item-details[splan-id="'+data[i][j]['id']+'"]').find(".min-price").html("$"+data[i][j]['maxprice']);
					compareMintoSellPrice();
				}
			}

		}
			
	}

}

function designcat(data){
	$(".design-cats2").html( data['designcat']);

}

function compute_when_adding(){
	$('.tshirtdisplay ').each(function(i){
		// setShirtGroupId($(this).find(".item-details").attr("splan-id"));

		var selling_price = Number( $(this).find('.item-details[splan-id="'+$(this).find(".item-details").attr("splan-id")+'"] .sell-price').val().replace("$", "")  );
		var min_price =Number ( $(this).find('.item-details[splan-id="'+$(this).find(".item-details").attr("splan-id")+'"] .min-price').text().replace("$", "") ) ;

		var initialprofit = selling_price - min_price ;  
		
		$('.item-details[splan-id="'+$(this).find(".item-details").attr("splan-id")+'"]').find('.managecamp-right-number .profit').text( '$'+initialprofit.toFixed(2) );
		
		if( selling_price < min_price ){
	       		$('.item-details[splan-id="'+$(this).find(".item-details").attr("splan-id")+'"]').find(" .managecamp-right-number .profit").css('color' ,'#D12F19' );
	       	}else{
	       		$('.item-details[splan-id="'+$(this).find(".item-details").attr("splan-id")+'"]').find(" .managecamp-right-number .profit").css('color' ,'#000' );
	       	}
	});
}

function add_more_plan(data, datapricing){
	var pricing = 0;
	var selling_price = 0;
	var profit_price = 0;
	var customhtml = "";
	
	if(data.length > 0 ){ // update for exact display of minimum display zz
		for(var i= 0; i < datapricing.length ; i++ ){
			for(var j= 0; j < datapricing[i].length ; j++ ){

				if(datapricing[i][j]['maxprice'] === null ){
					pricing = 10;							
				}else{
					pricing = datapricing[i][j]['maxprice'];
					
					
				}	

				// var groupname = datapricing[i][j]['group_name'];
				// groupname = groupname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
				//     return letter.toUpperCase();
				// });
				var templateDesign = getLoadTempImg();
				var a = $('.design-display[data-parent="'+getParentId()+'"] .product-details').attr("data-canvas");
				var b = $('.design-display[data-parent="'+getParentId()+'"] .product-details').attr("data-art");
				$("body").attr("data-canvas",a);
				$("body").attr("data-art",b);
				
				customhtml += '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 tshirtdisplay"><div class="cart-review-item-holder item-details"  splan-id="'+datapricing[i][j]['id']+'" shirt-data=""><span class="review-title gray-darker gregular font-small name-plan s-style" stylegroup-id="'+datapricing[i][j]['id']+'" data-shirtstyleid="'+datapricing[i][j]['shirt_style_id']+'">'+datapricing[i][j]['group_name']+'</span><span class="cart-review-remove removeshirt" data-toggle="modal" data-target="#confirm" data-dismiss="modal"></span></span><div class="cart-review-shirt-holder tshirt-image"><img class="img-responsive img-center load-plan-shirt-image" src="'+get_mainLink()+datapricing[i][j]['front_image_link']+'" alt="" /><div class="print-canvas"><div class="design-holder art-holder"><img class="img-responsive img-center load-temp-image" hex-id="" src="'+templateDesign+'" alt="" /></div></div></div><div class="managecamp-detail-holder"><span class="review-title gregular font-small pointer data-sizechart" style="padding-top: 5px; text-decoration:underline;" href="javascript:void(0);" >Size Chart</span><span class="review-title gregular font-small" style="padding-top: 5px; padding-bottom: 5px;">Selected Colors</span><div class="managecamp-stylecolor-cont" style-planid="'+datapricing[i][j]['id']+'"><div class="color-catcher"></div><div class="clearfix"></div></div></div></div><div class="clearfix"></div></div></div><div class="managecamp-detail-number"><div class="managecamp-left-number"><div class="blackz gsemibold font-small text-center">Min. Price</div><div class="blackz gregular font-small text-center min-price" id="min-price">$'+pricing+'</div></div><div class="managecamp-center-number"><div class="blackz gsemibold font-small text-center">Selling Price</div><input type="text" class="form-control gregular font-small gray-darker text-center managecamp-sellingprice sell-price numberwithdecimal" maxlength="6" onkeypress="return isNumberKey(event)" name="sell-price" id="sell-price" value="$0.00"/></div><div class="managecamp-right-number"><div class="blackz gsemibold font-small text-center">Profit</div><div class="blackz gsemibold font-small text-center profit" id="profit">$0.00</div></div><div class="clearfix"></div></div></div></div>';
				
				var dataIDx = datapricing[i][j]['id'];
				var shirtgroupy = datapricing[i][j]['group_name'];
				setShirtPlan( shirtgroupy );
				setActiveStyle( dataIDx );
				setActiveParent( getParentId() );
				replacer( templateDesign, 0);				
				setArtPosition( getDesignLocation(), getActiveParent(), getActiveStyle(), 0, shirtgroupy);
			}				

		}
		
		$(".addmore-stylesec").prepend( customhtml );
		
		compute_when_adding();
	}

}

function getColorList() {
	var color_list = '';
	$('.managecamp-stylecolor-cont[style-planid="'+getShirtGroupId()+'"] .color-catcher .selected-stylecolor').each(function(i){
		if ( color_list.length > 0 ) {
			color_list += ","+$(this).attr("data-id");
		} else {
			color_list += $(this).attr("data-id");
		}
	});
	return color_list;
}

function getPrice(){
	var selling_price ='';
	$('.item-details[splan-id="'+getShirtGroupId()+'"] .managecamp-center-number .sell-price').each(function(i){
		if( selling_price.length > 0 ){
			selling_price += $(this).val().replace("$", "") ;
			
			
		}else{
			selling_price +=  $(this).val().replace("$", "") ;
			
		}

	});

	return selling_price;	
	
}

function show_more_styles(){
	var dataStyles = JSON.parse( getAllStyles() );	
	if(dataStyles.length > 0 ){
		$(".show-all-styles").html("");
		$(".show-all-styles .tshirt-plan").find('.addmorestyle-shirt-holder').removeClass('recent-selected'); 
			for (var i =  0; i < dataStyles.length ; i++ ) {
				var groupname = dataStyles[i]['group_name'];
				groupname = groupname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
				$(".show-all-styles").append(
					'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 tshirt-plan"><div class="gregular font-small gray group-name">'+groupname+'</div><div class="addmorestyle-shirt-holder " shirt-id="'+dataStyles[i]['id']+'" shirt-group-id="'+dataStyles[i]['shirt_group_id']+'"><span class="style-check "></span><img class="img-responsive img-center styleshirt-col"  src="'+get_mainLink()+dataStyles[i]['front_image_link']+'" alt="" /></div></div>'
				);
			}				
				
	}
}

function showThisShirtOnly(data){
	var dataStyles = JSON.parse( getAllStyles() );	
		$(".show-all-styles").html("");
		var classActive = "";
		var classActive2 = "";
		if( getShirtGroupId() == data ){
				classActive = "addmorestyle-shirt-holder-active" ;
				classActive2 = "style-check-active";
		}
				

		if(dataStyles.length > 0 ){
			$(".show-all-styles .tshirt-plan").find('.addmorestyle-shirt-holder').removeClass('recent-selected'); 
				for (var i =  0; i < dataStyles.length ; i++ ) {
										
					if( data == dataStyles[i]['shirt_group_id'] ){						

						$(".show-all-styles").append(
						'<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 tshirt-plan"><div class="gregular font-small gray group-name">'+dataStyles[i]['group_name']+'</div><div class="addmorestyle-shirt-holder '+classActive+'" shirt-id="'+dataStyles[i]['id']+'" shirt-group-id="'+dataStyles[i]['shirt_group_id']+'"><span class="style-check '+classActive2+'"></span><img class="img-responsive img-center styleshirt-col"  src="'+get_mainLink()+dataStyles[i]['front_image_link']+'" alt="" /></div></div>'
						);
						
					}
				
			}				
				
	}
}

function loadSelectedShirtStyles() {
	var selected_colors = '';
	var selected_colors2 = '';
	// var dataColors = JSON.parse( getGenColors() );
	var dataColors = JSON.parse( getAllColors() );
	var dataStyles = JSON.parse( getAllStyles() );
	$(".zoom-image").attr("src", getLoadTempImg() );
	$(".addmore-stylesec").html("");

	$('.design-display[data-parent="'+getParentId()+'"] .product-details').each(function(){
		selected_colors = '';
		selected_styles = '';
		selected_group_name = '';

		var dataStylePlan = $(this).attr("data-shirt");
			styleplan =  dataStylePlan.split(",");

		var dataSelectedcolors = $(this).attr("data-colors");
		var colorsid =  dataSelectedcolors.split(",");

		var sell_dataprice = '';
		var displayMore = '';
		var profitz = '';

		var imgtexture = getAssetUrl()+"/images/ash.jpg";
		var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
		var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
		var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg";
		var img_back = "";
		$('.design-templates .dsgn-hold[data-parent="'+getParentId()+'"]').attr("dataArt", $(this).attr("data-art"));
		for( var x = 0; x < styleplan.length; x++ ){
			for (var y = 0; y < dataStyles.length; y++ ){
				if( dataStyles[y]['id'] == styleplan[x] ) {
					
					var groupname = dataStyles[y]['group_name'];
					groupname = groupname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
					    return letter.toUpperCase();
					});
					selected_styles += '<img class="img-responsive img-center load-plan-shirt-image" src="'+get_mainLink()+'/'+dataStyles[y]['front_image_link']+'" alt="" /><div class="print-canvas" style="'+$(this).attr("data-canvas")+'"><div class="design-holder art-holder" style="'+$(this).attr("data-art")+'"><img class="img-responsive img-center load-temp-image" hex-id="" src="'+$(this).attr("data-image")+'" alt="" /></div></div>' ;

					selected_group_name += '<span class="review-title gray-darker gregular font-small name-plan s-style" stylegroup-id="'+dataStyles[y]['id']+'" data-shirtStyleId="'+dataStyles[y]['shirt_style_id']+'">'+groupname+'</span>';
					y = dataStyles.length;

				}
			}

		}

		if( colorsid.length > 10 ){
			// $(".show-all").show();
			displayMore = '<div class="show-color show-all font-small" style="" data-count="'+colorsid.length +'">(Show all colors)</div>';
		}else{
			
			displayMore = '<div class="show-color show-all font-small" style="visibility:hidden;" data-count="'+colorsid.length +'">(Show all colors)</div>';
			

		}

		for ( var a = 0; a < 11; a++ ) {
			for ( var b = 0; b < dataColors.length; b++ ) {
				
				if ( dataColors[b]['color_id'] == colorsid[a] ) {
				
					if(dataColors[b]['hex_value'] == "91999D") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture+')" ';
					}else if(dataColors[b]['hex_value'] == "E1E5E8") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture2+')" ';
					}else if(dataColors[b]['hex_value'] == "47506B") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture3+')" ';
					}else if(dataColors[b]['hex_value'] == "21292C") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture4+')" ';
					}else{
						img_back = 'style="background:#'+dataColors[b]['hex_value']+'"';
					}

					selected_colors += '<div class="selected-stylecolor" title="'+dataColors[b]['color_name']+'" data-id="'+dataColors[b]['color_id']+'" data-code="'+dataColors[b]['color_code']+'" data-hex="'+dataColors[b]['hex_value']+'" '+img_back+'></div>';
					
					
					$('.back-selector[data-id="'+colorsid[a]+'"]').show(); //display all added shirt colors
					b = dataColors.length;
				}	
			}
			
		}

		for ( var a = 0; a < colorsid.length; a++ ) {
			for ( var b = 0; b < dataColors.length; b++ ) {
				
				if ( dataColors[b]['color_id'] == colorsid[a] ) {
					if(dataColors[b]['hex_value'] == "91999D") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture+')" ';
					}else if(dataColors[b]['hex_value'] == "E1E5E8") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture2+')" ';
					}else if(dataColors[b]['hex_value'] == "47506B") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture3+')" ';
					}else if(dataColors[b]['hex_value'] == "21292C") {
						img_back = 'style="background:#'+dataColors[b]['hex_value']+' url('+imgtexture4+')" ';
					}else{
						img_back = 'style="background:#'+dataColors[b]['hex_value']+'"';
					}

					selected_colors2 += '<div class="selected-stylecolor" title="'+dataColors[b]['color_name']+'" data-id="'+dataColors[b]['color_id']+'" data-code="'+dataColors[b]['color_code']+'" data-hex="'+dataColors[b]['hex_value']+'" '+img_back+'></div>';
					
					
					$('.back-selector[data-id="'+colorsid[a]+'"]').show(); //display all added shirt colors
					b = dataColors.length;
				}	
			}
			
		}

		if (  $(this).attr("data-price").length != 0 ){
			
				sell_dataprice = '$'+$(this).attr("data-price");	 
			
	
		} else{
			 if( $(this).attr("data-price") === "undefined" ){
				
				sell_dataprice += '$0.00';
				
			}else{

				sell_dataprice += '$0.00';
			}
		}


		
		$(".addmore-stylesec").prepend('<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 tshirtdisplay">'+
										'<div class="cart-review-item-holder item-details" splan-id="'+$(this).attr("data-shirt")+'" shirt-data="">'+
											selected_group_name+
											'<span class="cart-review-remove removeshirt" data-toggle="modal" data-target="#confirm" data-dismiss="modal"></span>'+
											'<div class="cart-review-shirt-holder tshirt-image">'+selected_styles+
											'</div>'+
											// '<span class="review-title gregular font-small pointer data-sizechart s-chart" style="padding-top: 5px; text-decoration:underline;" href="javascript:void(0);" >Size Chart</span>'+
											'<div class="managecamp-detail-holder">'+
												
												
												'<span class="review-title gregular font-small" style="padding-top: 5px; padding-bottom: 5px;">Selected Colors</span>'+
												'<div class="managecamp-stylecolor-cont" style-planid="'+$(this).attr("data-shirt")+'">'+
													'<div class="color-catcher">'+selected_colors+'</div>'+
													'<div class="color-catcher-showall">'+selected_colors2+'<div class="show-color show-less font-small">(Show less)</div></div>'+
													'<div class="clearfix"></div>'+
												'</div>'+displayMore+
											'</div>'+
											'<div class="managecamp-detail-number">'+
												'<div class="managecamp-left-number bg-white">'+
													'<div class="blackz gsemibold font-small text-center">Min. Price <span class="q-icon"><div class="q-pop2">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div></span></div>'+
													'<div class="blackz gregular font-small text-center min-price min-price-pad" id="min-price">$'+$(this).attr("data-min")+'.00</div>'+
												'</div>'+
												'<div class="managecamp-center-number bg-white">'+
													'<div class="blackz gsemibold font-small text-center">Selling Price</div>'+
													'<input type="text" class="txt1 form-control gregular font-small gray-darker text-center managecamp-sellingprice sell-price numberwithdecimal"  maxlength="6" name="sell-price" id="sell-price" value="'+sell_dataprice+'"/>'+
												'</div>'+
												'<div class="managecamp-right-number">'+
													'<div class="blackz gsemibold font-small text-center">Profit</div>'+
													'<div class="blackz gsemibold font-small text-center profit profit-pad" id="profit"></div>'+
												'</div>'+
												'<div class="clearfix"></div>'+
											'</div>'+
										'</div>'+
									'</div>');
		
		$('.item-details[splan-id="'+$(this).attr("data-shirt")+'"]').find(".selected-stylecolor:first").click(); //onload display zoom image

		// add class for selected items in popup
		$('.addmorestyle-shirt-holder[shirt-id="'+$(this).attr("data-shirt")+'"]').addClass("addmorestyle-shirt-holder-active").find(".style-check").addClass("style-check-active");
		compareMintoSellPrice();
		
	});

	$('.selected-stylecolor[data-code="WHT"]').css({"border":"1px solid #E1DFDF"});
}

function refreshSelectedShirtsList() {
	$('.design-display[data-parent="'+getParentId()+'"] ').find('.product-details').remove();

	$(".addmore-stylesec .tshirtdisplay").each(function(i){
		setShirtGroupId($(this).find(".item-details").attr("splan-id"));
		$('.design-display[data-parent="'+getParentId()+'"]').prepend( $("<div></div>").attr({
			"class":"product-details datanew",
			"data-parent":getParentId(),
			"data-template":$('.design-display[data-parent="'+getParentId()+'"]').find(".selected-design").attr("data-id"),
			"data-shirt":$(this).find(".item-details").attr("splan-id"),
			"data-colors":getColorList(),
			"data-price": getPrice(),
			"data-image":$('.design-display[data-parent="'+getParentId()+'"]').find(".template-design").attr("src"),
			"back-color":$('.design-display[data-parent="'+getParentId()+'"]').attr("back-color"),
			"data-canvas": $(".tshirtdisplay").find('[splan-id*="'+$(this).find(".item-details").attr("splan-id")+'"]').find(".print-canvas").attr("style"),
			"data-art": $('.design-templates .dsgn-hold[data-parent="'+getParentId()+'"]').attr("dataArt")
		}) );

			
	});
}

function get_template_data(){ 

	var shirt_styleID = [] ;
	var shirt_colors = [];

	$('.design-display').each(function(){
		shirt_styleID.push( $(this).attr('group-id') );
		shirt_colors.push( $(this).attr('shirt-colors') );

	});

	var template_design = getLoadTempImg() ;	
}

function htmlEntities(str) {
     return String(str).replace(/"/g, '&#34;').replace(/'/g, '&#39;');
    // return String(str).replace(/&/g, '&#38;').replace(/$/g, '&#36;').replace(/!/g, '&#33;').replace(/"/g, '&#34;').replace(/'/g, '&#39;').replace(/#/g, '&#35;').replace('*', '&#42;').replace(/@/g, '&#64;');
}

function getALLdata(){ //this is applicable for saving to procucts
		var dataProductDetails = JSON.parse( getProductDetails() );
		var designDatas = {};
  		var allProducts_data =[];
		var temp_id = "";
		var data_image ='';
		var data_template ='';
		var data_price ='';
		var data_properties ='';
		var data_back_color ='';
		var data_designId ='';
		var data_parent='';
		var data_canvas='';
		var data_art = '';

		$(".design-display").each(function(i){
		    designDatas[i] = { product_id : $(this).attr("data-template") };
		    var shirt = {};
		      temp_id = $(this).attr("data-template");
		      var designDatas_details = [];
		        data_designId =  $(this).parent().attr('data-design-id');
		    $(this).find(".product-details").each(function(x){
	    	data_canvas = $(this).attr('data-canvas');
			data_art = $(this).attr('data-art');
		    designDatas_details.push({ style_id : $(this).attr("data-shirt"), color : $(this).attr("data-colors") , data_price : $(this).attr('data-price'), "art_setting" : data_art, "canvas_setting" : data_canvas });		   			   	
		   	data_template = $(this).attr('data-template');						
		   	data_product_id = $(this).parent().attr('product-id');						
			data_properties = htmlEntities($(this).parent().find('.managecamp-design-holder').attr('data-properties') );

			// data_prop =	data_properties.split(",");
			 

			data_back_color = $(this).parent().attr("back-color");
			data_parent = $(this).attr('data-parent');
			data_image = $(this).attr('data-image');
			
		  	allProducts_data[i] = { 

						'parent_id' : data_parent,
						'template_id' : data_template ,
						'shirt_style':  designDatas_details,						
						// 'shirt_colors': dataColors.split(','),
						// 'data_price': data_price,
						'template_image' : data_image,
						'template_image2' : data_image,
						'properties' : data_properties,
						'back_color' : data_back_color,
						'product_id' : data_product_id,
						'design_id' :  data_designId

					};
		   });


			});
	// }
		return allProducts_data;		
	

}

function getDataForUpdatingItems(){
	var dataProductDetails = JSON.parse( getProductDetails() );
	var designDatas = {};
	var allProducts_data =[];
	var temp_id = "";
	var data_image ='';
	var data_template ='';
	var data_price ='';
	var data_properties ='';
	var data_back_color ='';
	var data_designId ='';
	var data_parent='';
	var data_canvas = '';
	var data_art = '';

			$(".design-display").each(function(i){
				    designDatas[i] = { product_id : $(this).attr("data-template") };
				    var shirt = {};
				      temp_id = $(this).attr("data-template");
				      var designDatas_details =[];

				     data_designId =  $(this).parent().attr('data-design-id');

				    $(this).find(".product-details").each(function(x){

			    	data_canvas = $(this).attr('data-canvas');
					data_art = $(this).attr('data-art');
				    // designDatas_details.push({ style_id : $(this).attr("data-shirt"), color : $(this).attr("data-colors") , data_price : $(this).attr('data-price'), "art_setting" : data_art, "canvas_setting" : data_canvas });
				     designDatas_details[x] = { style_id : $(this).attr("data-shirt"), color : $(this).attr("data-colors"), id : $(this).attr("data-shirt"), colors : $(this).attr("data-colors").split(',') , data_price : $(this).attr('data-price'), "art_position" : data_art, "canvas_position" : data_canvas  };		   			   	
				   	data_template = $(this).attr('data-template');						
					// data_properties = htmlEntities($(this).parent().find('.managecamp-design-holder').attr('data-properties'));
					data_properties = $(this).parent().find('.managecamp-design-holder').attr('data-properties');
					data_back_color = $(this).parent().attr("back-color");
					data_parent = $(this).attr('data-parent');
					data_productId = $(this).parent().attr('product-id');
					data_image = $(this).attr('data-image');
					//propertyD =  dataProductDetails[i]['properties'];

				  	allProducts_data[i] = { 

								'parent_id' : data_parent,
								'template_id' : data_template ,
								// 'shirt_style':  designDatas_details,
								'shirts_and_colors':  designDatas_details,
								// 'shirt_colors': dataColors.split(','),
								// 'data_price': data_price,
								'template_image' : data_image,
								'template_image2' : data_image,
								// 'properties' : propertyD,
								'properties' : data_properties,
								'back_color' : data_back_color,
								'design_id' :  data_designId,
								'product_id' : data_productId,
								'hex_value' : data_back_color,						
							};
				   });


			});
		//}
	return allProducts_data;
	
}


function getDataForUpdatingItems_old(){
	var allProducts_data = [];
	var dataParent ='';
	
	var dataColors = [];
	var dataPrice = 0;
	var dataImage = '';
	var dataproperties = '';
	var dataTemplate = [];
	var datashirt = '';
	var ctr = 0;
	var data_productId = 0;
	var data_campaignId = $('.dsgn-hold').attr('data-campid');
	for ( var x = 0; x < $('.design-display').length; x++ ) {
		$('.design-display:eq('+x+') .product-details[data-parent="'+ $('.design-display').eq(x).find('.product-details').attr('data-parent')+'"]').each(function(){

			datashirt = $(this).attr('data-shirt');
			dataColors = $(this).attr('data-colors');
			
			dataPrice =  $(this).attr('data-price');
			dataImage = $(this).attr('data-image') ;
			dataParent = $(this).attr('data-parent') ;					
			dataproperties = htmlEntities($(this).parent().find('.managecamp-design-holder').attr('data-properties'));
			data_productId = $(this).parent().attr('product-id');
			

			allProducts_data[ ctr ] = {
				'parent_id' :dataParent,
				'template_id' : $(this).attr('data-template')  ,
				'shirt_style': datashirt,
				'shirt_colors': dataColors.split(","),
				'data_price': dataPrice,
				'template_image' : dataImage,
				'template_image2' : dataImage,
				'properties' : dataproperties,
				'product_id' : data_productId,
				'campaign_id' : data_campaignId,
				'design_id' : $(this).parent().parent().attr('data-design-id') ,
				'back_color' :$(this).attr("back-color"),
				'hex_value' :$(this).attr("back-color") 

			};
			ctr++;
		});		
	}

	
	return allProducts_data;
}

function loadAllProductDetails(){
	
	var dataProductDetails = JSON.parse( getProductDetails() );
	
	var dataGenColors = JSON.parse( getGenColors() );
	var get_colors = '';
	var get_colors2 = '';
	var selectBack_color = '';
	var dataColors = JSON.parse( getAllColors() );
	var product_id = '';

	var shirts_style = '';
	var dataprice = 0;
	if ( dataProductDetails.length > 0 ) {
		for(var a = 0; a< dataColors.length ; a++ ){
							
				selectBack_color += '<div class="back-selector selected-color " data-id="'+dataColors[a]['color_id']+'" title="'+dataColors[a]['color_name']+'" data-hex="'+dataColors[a]['hex_value']+'" style="background:#'+dataColors[a]['hex_value']+';"><img class="img-responsive " src="'+getAssetUrl()+'/images/check-icon-xs.png" alt="" /></div>';

				// a = dataColors.length ;
			}

		for ( var x = 0; x < dataProductDetails.length; x++ ) {


			if (dataProductDetails[x]['product_id'] === undefined ){
				product_id = 0 ;
			}else{
				product_id = dataProductDetails[x]['product_id'];
			}
			
			if( dataProductDetails[x]['shirts_and_colors']){ //From tGen and goes back to manage items

				var sellingprice = 0 ;
				var shirt_colors = [];
				

				for(var b = 0; b <  dataProductDetails[x]['shirts_and_colors'].length; b++ ){
				
					shirt_style =  dataProductDetails[x]['shirts_and_colors'][b]['style_id'];
					shirt_colors = dataProductDetails[x]['shirts_and_colors'][b]['colors'];
					sellingprice = dataProductDetails[x]['shirts_and_colors'][b]['data_price'];
					shirt_canvas = dataProductDetails[x]['shirts_and_colors'][b]['canvas_position'];
					shirt_art = dataProductDetails[x]['shirts_and_colors'][b]['art_position'];

				
					var templateSrc = dataProductDetails[x]['template_image']; 
					if ( sellingprice === undefined ){
						
						dataprice = 0;
						
					} else{ 

						dataprice = sellingprice;
					}

					

					if ( $('.design-templates .dsgn-hold .design-display[data-parent="'+dataProductDetails[x]["parent_id"]+'"]').length > 0 ) {
						$('.design-templates .dsgn-hold .design-display[data-parent="'+dataProductDetails[x]["parent_id"]+'"]').prepend( $("<div></div>").attr({
							"class":"product-details",
							"data-parent":dataProductDetails[x]["parent_id"],					
							"data-shirt":shirt_style,
							"data-template": dataProductDetails[x]["template_id"],
							"data-colors":shirt_colors,
							"data-price": dataprice,
							"data-min": '0',
							"data-image":templateSrc,
							"back-color": dataProductDetails[x]["back_color"],
							"data-canvas":shirt_canvas,
							"data-art": shirt_art
						}) );
					} else {
						
						if( $('.design-display[data-parent=""]').length > 0 ){
								$('.dsgn-hold[data-parent=""]').remove();
								
						}									
						$('.design-templates').append('<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 dsgn-hold bg-cont" data-design-id="'+dataProductDetails[x]['design_id']+'" data-parent="'+dataProductDetails[x]['parent_id']+'" data-campid="'+getCampaignID()+'"><div class="design-display" data-parent="'+dataProductDetails[x]['parent_id']+'" data-template="'+dataProductDetails[x]['template_id']+'" group-id="'+shirt_style+'" shirt-colors="'+shirt_colors+'" back-color="'+dataProductDetails[x]['back_color']+'" product-id="'+product_id+'"> <div class="product-details" data-canvas="'+shirt_canvas+'" data-art="'+shirt_art+'" data-parent="'+dataProductDetails[x]['parent_id']+'" data-shirt="'+shirt_style+'" data-template="'+dataProductDetails[x]['template_id']+'" data-colors="'+shirt_colors+'" data-price="'+dataprice+'" data-min="0" data-image="'+templateSrc+'" ></div><span class="cart-review-remove2" data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span><div class="managecamp-design-holder " data-id="'+dataProductDetails[x]['parent_id']+'" data-parent="'+dataProductDetails[x]['parent_id']+'" data-properties="'+dataProductDetails[x]['properties']+'" style="background-color:#'+dataProductDetails[x]['back_color']+'"><img class="img-responsive template-design img-center" src="'+templateSrc+'" alt=""><div class="managecamp-designbtn-holder onhover-editdelete" style="display: none;"><div class="managecamp-designbtn-holder-inner"><div class="managecamp-designbtn-edit"></div><div class="managecamp-designbtn-remove " data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></div></div></div></div> </div>    <div class="managecamp-do-btn-holder dsgn-hold" data-parent="'+dataProductDetails[x]['parent_id']+'"><div class="managecamp-designcolor-holder back-selectcolor"><div class="managecamp-designcolor-header gregular white font-xsmall">Set this design’s background color<br>to be previewed in your Campaign.</div><div class="managecamp-designcolor-cont select-background">'+selectBack_color+'<div class="clearfix"></div></div></div><button class="managecamp-do-btn font-xsmall gsemibold text-uppercase btn gray edit-des">Edit Design</button></div></div>'
						);
						
					}



				}

				
			

			}else{

				var backColors =  dataProductDetails[x]['shirt_colors'];
				var una = +backColors.toString().split(',')[0];
				
       			for(var a = 0; a< dataColors.length ; a++ ){

       				if( dataColors[a]['color_id'] == una ){
						
						get_colors = dataColors[a]['hex_value'];

       					$('.design-display[data-template="'+dataProductDetails[x]['template_id']+'"]').attr('back-color', get_colors);
       				}
       			} 

 
				var templateSrc = dataProductDetails[x]['template_image']; 
				if ( dataProductDetails[x]['selling_price'] === undefined ){
					
					dataprice = 0;
					
				} else{ 

					dataprice = dataProductDetails[x]['selling_price'];
				}

				
				

				if ( $('.design-templates .dsgn-hold .design-display[data-parent="'+dataProductDetails[x]["parent_id"]+'"]').length > 0 ) {
					$('.design-templates .dsgn-hold .design-display[data-parent="'+dataProductDetails[x]["parent_id"]+'"]').prepend( $("<div></div>").attr({
						"class":"product-details",
						"data-parent":dataProductDetails[x]["parent_id"],					
						"data-shirt":dataProductDetails[x]["shirt_style"],
						"data-template":$('.design-display[data-parent="'+dataProductDetails[x]["template_id"]+'"]').attr("data-template"),
						"data-colors":dataProductDetails[x]["shirt_colors"],
						"data-price": dataprice,
						"data-min": $('.design-display[data-shirt="'+dataProductDetails[x]["shirt_style"]+'"]').attr("data-min"),
						"data-image":$('.design-display[data-parent="'+dataProductDetails[x]["parent_id"]+'"]').find(".template-design").attr("src"),
						"back-color": get_colors,
						"data-canvas": dataProductDetails[x]["canvas_position"],
						"data-art": dataProductDetails[x]["art_position"]
					}) );
				} else {
					
					if( $('.design-display[data-parent=""]').length > 0 ){
							$('.dsgn-hold[data-parent=""]').remove();
							
					}									
					$('.design-templates').append('<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 dsgn-hold bg-cont" data-design-id="'+dataProductDetails[x]['design_id']+'" data-parent="'+dataProductDetails[x]['parent_id']+'" data-campid="'+getCampaignID()+'"><div class="design-display" data-parent="'+dataProductDetails[x]['parent_id']+'" data-template="'+dataProductDetails[x]['template_id']+'" group-id="'+dataProductDetails[x]['shirt_style']+'" shirt-colors="'+dataProductDetails[x]['shirt_colors']+'" back-color="'+get_colors+'" product-id="'+product_id+'"> <div class="product-details" data-canvas="'+dataProductDetails[x]['canvas_position']+'" data-art="'+dataProductDetails[x]['art_position']+'" data-parent="'+dataProductDetails[x]['parent_id']+'" data-shirt="'+dataProductDetails[x]['shirt_style']+'" data-template="'+dataProductDetails[x]['template_id']+'" data-colors="'+dataProductDetails[x]['shirt_colors']+'" data-price="'+dataprice+'" data-min="0" data-image="'+templateSrc+'" ></div><span class="cart-review-remove2" data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span><div class="managecamp-design-holder " data-id="'+dataProductDetails[x]['template_id']+'" data-parent="'+dataProductDetails[x]['parent_id']+'" data-properties="'+dataProductDetails[x]['properties'].replace('"\\"', "")+'" style="background-color:#'+get_colors+'"><img class="img-responsive template-design img-center" src="'+templateSrc+'" alt=""><div class="managecamp-designbtn-holder onhover-editdelete" style="display: none;"><div class="managecamp-designbtn-holder-inner"><div class="managecamp-designbtn-edit"></div><div class="managecamp-designbtn-remove " data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></div></div></div></div> </div>    <div class="managecamp-do-btn-holder dsgn-hold" data-parent="'+dataProductDetails[x]['parent_id']+'"><div class="managecamp-designcolor-holder back-selectcolor"><div class="managecamp-designcolor-header gregular white font-xsmall">Set this design’s background color<br>to be previewed in your Campaign.</div><div class="managecamp-designcolor-cont select-background">'+selectBack_color+'<div class="clearfix"></div></div></div><button class="managecamp-do-btn font-xsmall gsemibold text-uppercase btn gray edit-des">Edit Design</button></div></div>'
					);
					
				}

			}
			
		}
			var countDesign = $(".design-display").length ; 
			for ( var a = countDesign ; a < 6 ; a++ ){
				$('.design-templates').append('<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 blank-design" data-count="'+a+'"><div class="managecamp-design-holder-blank " ></div></div>');
			
			}
			// compareMintoSellPrice();
	}
}

function reloadProductDetails(){
	$.ajax({
			type: "POST",
			url: get_mainLink()+"template/create_campaign",
			data:{
				product_details: getDataForUpdatingItems()
			},success: function(data){
				
				if( $('.design-display').size() == 0 ){
					
					window.location.href = get_mainLink()+'template/customize/'+getCampaignID();
				}
				$(".loader-container").hide();
			},
		});
}



function append_shirt(){
	var shirt_id = [];
 		$(".show-all-styles").find(".recent-selected").each(function(){	 			
		     shirt_id.push( $(this).attr("shirt-id") );
		    
 		});
	
	var ajxurl = get_mainLink()+'campaign/add_style';

	if( shirt_id.length > 0 ){ //
		$.ajax({
			type: 'POST',
			url: ajxurl,
			data: {
				shirt_id: shirt_id
			}, success: function(data){
				var data = JSON.parse(data);	
				

			$('.show-all-styles').find('.addmorestyle-shirt-holder').removeClass('recent-selected');
			add_more_plan(data['add_plans'], data['style_pricing'])
			}, 
		})
		}
}


function show_chartSize(data){
	
	if(data.length > 0 ){
		$("#sizechart").modal('show');
		$('.size-chart-table thead tr').html($("<th />").addClass("gsemibold blackz").attr("data-code", "").text("Body Specs"));
		$('.size-chart-table tbody tr').html("");
		for(var x = 0; x < data.length; x++ ){
			// $('.style-label').html(data[x]['style_name']);
			// $(".size-chart-info").find('.text-left').html(data[x]['style_description']);
			
			$(".style-label").text(data[x]['style_name']);
				$(".size-chart-info").html($("<p />").addClass("gray gregular font-small text-left").text(data[x]['style_description']));

				var sizeString1 = data[x]['shirt_size_data'].replace("{","").replace("}","");
				var sizeArray1 = sizeString1.split(",");

				if ( $('.size-chart-table thead tr th[data-code="'+data[x]['shirt_size_code']+'"]').size() == 0 ) {
					$('.size-chart-table thead tr').append($("<th />").addClass("gsemibold blackz").attr("data-code", data[x]['shirt_size_code'] ).text(data[x]['shirt_size_code'].replace("(inch","")));
				}

				for ( var y = 0; y < sizeArray1.length; y++ ) {
					var strArr = sizeArray1[y].split(":");

					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq(0)').size() == 0 ) {
						$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").addClass("gregular blackz").text(strArr[0]));
					}

					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq('+$('.size-chart-table thead tr th[data-code="'+data[x]['shirt_size_code']+'"]').index()+')').size() == 0 ) {
						$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").text(strArr[1]));
					}
				}


		}

	}
}


function loadSizeData( toDo = "", shirt_id = "" ) {
	
		if(xhr && xhr.readystate != 4){
			xhr.abort();
		}

		// lets submit the form
		xhr = $.ajax({
			type: 'POST',
			url: get_mainLink()+'settings/get_size_chart_details'+( shirt_id.length > 0 ? '/'+shirt_id : ''),
			success: function(data){
			

				if( data.length > 0 ){
					setSizeData( data );
					
					if ( toDo == 'populate' ) {
						
						populateSpecifictionDetails();
						// $("#sizechart").modal('show');
					} else if ( toDo == 'chart' ) {
						
						populateChartDetails( shirt_id );
						// $("#sizechart").modal('show');
					}
				}
			},
			async:true
		});
	}

	function populateChartDetails( shirt_id = 0 ) {
		$("#sizechart").modal('show');
		var shirtData = JSON.parse(getSizeData() );//JSON.parse(getSizeData());
		$(".style-label").html($('.display-shirts[data-id="'+shirt_id+'"]').find(".shirt-title").html());
		
		$('.size-chart-table thead tr').html($("<th />").addClass("gsemibold blackz").attr("data-code", "").text("Body Specs"));
		$('.size-chart-table tbody tr').html("");

		for ( var x = 0; x < shirtData.length; x++ ) {
			if ( shirtData[x]['shirt_style_id'] == shirt_id ) {
		
				$(".style-label").text(shirtData[x]['style_name']);
				$(".size-chart-info").html($("<p />").addClass("gray gregular font-small text-left").text(shirtData[x]['style_description']));

				var sizeString1 = shirtData[x]['shirt_size_data'].replace("{","").replace("}","");
				var sizeArray1 = sizeString1.split(",");

				for ( var y = 0; y < sizeArray1.length; y++ ) {
					var strArr = sizeArray1[y].split(":");
					if ( $('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table thead tr').append($("<th />").addClass("gsemibold blackz").attr("data-code", shirtData[x]['shirt_size_code']).text(shirtData[x]['shirt_size_code']));
						}
					}	
					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq(0)').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").addClass("gregular blackz").text(strArr[0]));
						}
					}

					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq('+$('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').index()+')').size() == 0 ) {						
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").text(strArr[1]));	
						}
						
					}
				}
			}
		}
	}

	function populateSpecifictionDetails() {
		var shirtData = getSizeData(); // JSON.parse(getSizeData());
		var sizeIndex = 0;
		var sizeRange = "";
		var previousShirt = '';
 
		if ( shirtData.length > 0 ) {
			$(".specification-preview").html("");

			for ( var a = 0; a < shirtData.length; a++ ) {
				if ( shirtData[a]['style_name'] != previousShirt ) {
					$("<div />").addClass("col-lg-3 col-md-3 col-sm-6 col-xs-12 display-shirts").attr({"data-id":shirtData[a]['id'],"style-id":shirtData[a]['shirt_style_id'],"data-style":"0","data-category":shirtData[a]['shirt_group_id']}).css("min-height","500px").appendTo(".specification-preview").append($("<div />").addClass("design-item-holder camp-item cont-item").append($("<img />").attr({"class":"img-responsive img-center shirt-col","src":get_mainLink()+shirtData[a]['thumbnail'],"alt":""})).append($("<a />").attr({"class":"shirt-name-link","href":"#"}).append($("<h3 />").addClass("gregular font-medium-2 gray-darker shirt-title").text(shirtData[a]['style_name'])).append($("<span />").text(sizeRange)))).append($("<div />").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12").append($("<button />").attr({"data-toggle":"modal","data-target":"#sizechart","data-dismiss":"modal","class":"orderconf-btn green-btn white gsemibold font-small data-sizechart"}).text("View Size Chart")));
				}

				previousShirt = shirtData[a]['style_name'];
			}
		}
	}


	 

function check_selling_prices(){
	var ctrPrice = $('.product-details[data-price="0"]').size();	
 
 	if(  $('.product-details[data-price="0"]').size() === 0 ){
 			
		able_next();

	}else {

		 disable_next();
	 	

	}
	 
}

function able_next(){
	$(".goto-preview-campaign").removeClass('gray-btn');
	$(".goto-preview-campaign").addClass('green-btn');
	$(".goto-preview-campaign").removeAttr('disabled'); 
}
function disable_next(){
	 $(".goto-preview-campaign").removeClass('green-btn');
	 $(".goto-preview-campaign").addClass('gray-btn');
	 $(".goto-preview-campaign").attr('disabled', 'disabled'); 
}


$(window).load(function(){
	 $('.design-display[data-parent="'+getParentId()+'"]').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').attr("data-price",getPrice() );
	 
	 loadAllProductDetails();
	 $(".design-display:first").click();

       	var  colorbackg =	$('.managecamp-stylecolor-cont[style-planid="'+getShirtGroupId()+'"] .color-catcher').find('.selected-stylecolor').attr('data-id');
		
    
    check_selling_prices();
});


$(document).ready(function() {


	$(document).on("mouseover",".managecamp-sellingprice",function(){
		//alert("pasok");
			var x =$(this).val();// document.getElementById("sell-price");
			 var num=x.replace("$","");
    	$(this).val(num.toString());
	});

	$(function(){
		// compareMintoSellPrice();
		loadSizeData( 'populate', getSizeData() );
		$("#storetitle").text('TZilla.com - Edit Campaign');
	});

	// $(document).on("mouseover",".design-display", function(){
 //       $(this).parent().find(".onhover-editdelete").css("display", "block");
 //    });

 	$(document).on("click", ".show-all", function(){
		$(this).parent().parent().parent().find(".color-catcher-showall").css("display","block");
		$(this).parent().parent().parent().find(".managecamp-detail-number").css("border-top","none");
	});

	$(document).on("click", ".show-less", function(){
		$(".color-catcher-showall").css("display","none");
		$(".managecamp-detail-number").css("border-top","1px solid #ccc");
	});
	
	$(document).on("mouseover",".q-icon", function(){
		$(this).find(".q-pop").addClass("q-pop-show");
	});
	
	$(document).on("mouseout",".q-icon", function(){
		$(this).find(".q-pop").removeClass("q-pop-show");
	});
	
	$(document).on("mouseover",".q-icon", function(){
		$(this).find(".q-pop2").addClass("q-pop-show");
	});
	
	$(document).on("mouseout",".q-icon", function(){
		$(this).find(".q-pop2").removeClass("q-pop-show");
	});

    // $(document).on("mouseout", ".design-display", function(){
    //    $(this).parent().find(".onhover-editdelete").css("display", "none");
    //  });

	$(document).on("click", ".design-display", function(){
		
		$(".design-display").find(".managecamp-design-holder").removeClass("selected-design");
		$(this).find(".managecamp-design-holder").addClass("selected-design");

		if ($(this).find(".managecamp-design-holder").hasClass("selected-design")) {
			$(".design-display").parent().parent().find(".bg-cont").removeClass("des-bg");
			$(".design-display").find(".cart-review-remove2").css("display","none");
			$(this).parent().parent().find(".selected-design").parent().parent().addClass("des-bg");
			$(this).find(".cart-review-remove2").css("display","block");
		}else {
			$(".design-display").parent().parent().find(".bg-cont").removeClass("des-bg");
			$(".design-display").find(".cart-review-remove2").css("display","none");
		}

		var styles = new Array();
		var shirt_colors = new Array();
		
		setLoadTempImg( $(this).find(".template-design").attr('src') ); //set image template
       	setParentId( $(this).attr('data-parent') );
       	setShirtGroupId( $(this).attr('group-id') );

     

		$(this).find('.product-details').each(function(){
	       	styles.push( $(this).attr('data-shirt') );          	

		}); 
		
		show_more_styles();
		loadSelectedShirtStyles();
		// compareMintoSellPrice();

		$.ajax({
			type: "POST",
			url: get_mainLink()+"campaign/show_shirts/"+getShirtGroupId(),
			data:{
				
				styles : styles
			}, success: function(nData){
				
				var data = JSON.parse( nData );				
				show_max_price(data['style_pricing']);
				
				compareMintoSellPrice();
			},
		});


	});
	
	$(document).on("click", ".managecamp-do-btn", function(){ //display options selecting color
		$(".back-selector").hide();
		$('.managecamp-do-btn').parent().parent().find('.managecamp-design-holder').removeClass('selected-design');

		$(this).parent().parent().find('.design-display').click();
		$(this).parent().parent().find('.managecamp-design-holder').addClass('selected-design');
		// $(".managecamp-designcolor-cont").html("");
		
		$('.managecamp-do-btn').parent().find(".back-selectcolor").removeClass("managecamp-designcolor-holder-show");
		// $(this).parent().find(".back-selectcolor").addClass("managecamp-designcolor-holder-show");

		setShirtGroupId( $(this).parent().parent().find('.design-display').attr('group-id') );	
			

	});
	
	function get_color_by_style( splan_id ){
		var ajaxurl = get_mainLink()+'campaign/show_colors/'+getShirtGroupId();		
		
		$.post( ajaxurl, function(ndata){
			var data = JSON.parse(ndata);
			
			show_colors(data['style_plan_colors']);
			
		});
	}

	//add more style colors
	$(document).on("click", ".selected-stylecolor-plus", function(){ //selecting tshirt color
		$(this).find(".managecamp-designcolor-cont2").html("");
		var splan_id = $(this).parent().parent().parent().attr('splan-id');
		setShirtGroupId( splan_id );
		// $(this).find('.selected-color').removeClass("selected-color-active");
		$('.selected-stylecolor-plus').find(".addmore-colors").removeClass("managecamp-designcolor-holder-show");
		$(this).find(".managecamp-designcolor-holder").addClass("managecamp-designcolor-holder-show");

		// var splan_id = $(this).parent().parent().parent().attr('splan-id');
		$(this).find(".addmore-colors").addClass("managecamp-designcolor-holder-show");

		get_color_by_style( getShirtGroupId() );			
		

	});	
	


	$(document).on("click", ".select-background .back-selector", function(){ //add check for selected colors
		
		$('.back-selector').removeClass("selected-color-active");
		$(this).addClass("selected-color-active");
		// $(this).addClass("selected-color-active");
		var hex = $(this).attr("data-hex");
		var id = $(this).attr("data-id");
		$(this).parent().parent().parent().parent().find(".managecamp-design-holder").css({"background-color":"#"+hex});
			$('.design-display[data-parent="'+getParentId()+'"]').attr("back-color", hex);
		
		$(".managecamp-design-holder").find(".onhover-editdelete").css("display", "none");
	});

	$(document).on("click", ".colors-data .selected-color", function(){
		
		var hex = $(this).attr("data-hex");
		var id = $(this).attr("data-id");
		var name = $(this).attr("title");
		var item = '<div class="selected-stylecolor" data-id="'+id+'" title="'+name+'" data-hex="'+hex+'" style="background:#'+hex+'">';
			item += '</div>';
		
		$(this).addClass("selected-color-active");

		if( $(this).hasClass("selected-color-active")){
			
			$(this).parent().parent().parent().parent().parent().find(".managecamp-stylecolor-cont .color-catcher").prepend(item);			
			$('.design-display[data-parent="'+getParentId()+'"]').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').attr("data-colors",getColorList());

			$(this).parent().parent().parent().parent().find('.selected-stylecolor[data-hex="'+hex+'"]').click();
		}else{
			$(this).parent().parent().parent().parent().parent().parent().find('.load-plan-shirt-image').removeAttr('style');
		}

		if($(".selected-color-active").size() == 0 ){
			$(this).parent().parent().parent().parent().parent().parent().find('.load-plan-shirt-image').removeAttr('style');
		}
		
				

	});

	$(document).on("click", ".colors-data .selected-color-active", function(){
		var id = $(this).attr("data-id");
		$(this).removeClass("selected-color-active");
		
		$(this).parent().parent().parent().parent().parent().find(".managecamp-stylecolor-cont .color-catcher").find("[data-id='"+id+"']").remove();
		//get hasclass
		$(this).parent().parent().parent().parent().find('.selected-stylecolor:first').click();

		
		if($(".selected-color-active").size() == 0 ){
			
			$(this).parent().parent().parent().parent().find('.selected-stylecolor:first').click();
			$(this).parent().parent().parent().parent().parent().parent().find('.load-plan-shirt-image').removeAttr('style');
		}else{
			$(this).parent().parent().parent().parent().find('.selected-stylecolor:first').click();

		}
	});
	
	$(document).on("click", ".managecamp-designcolor-holder", function(e){
		e.stopPropagation();
	});
	
	$(document).mouseup( function (e) {
		var container = $(".managecamp-designcolor-holder");

		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			container.removeClass("managecamp-designcolor-holder-show");
		}

		// $(".selected-design").find(".onhover-editdelete").css("display", "block");
	});
	
	
	$('.left-slide').hide();
	$('.right-slide').click(function(event) {
		var pos1 = $('.prod-holder').scrollLeft() + 200;
		$('.prod-holder').scrollLeft(pos1);
		$('.left-slide').show();
	});
	$('.left-slide').click(function(event) {
		var pos2 = $('.prod-holder').scrollLeft() - 200;
		$('.prod-holder').scrollLeft(pos2);
	});

	$("#shirt-group-select").on("change", function(){
		var shirtgroup = $(this).val();
		if( shirtgroup == "" ){
			show_more_styles();
			loadSelectedShirtStyles();
		} else{

			
			var ajaxURL = get_mainLink()+'inventory/show_styles/'+$(this).val();
			$.ajax({
				type: "POST",
				url: ajaxURL,
				data:{},success: function(data){
					var nData = JSON.parse(data);
					$(".tshirt-plan").hide();
					
					if ( nData.length > 0 ) {
						for ( var x = 0; x < nData.length; x++ ) {
	        				$('.addmorestyle-shirt-holder[shirt-group-id="'+nData[x]['newgroup']+'"]').parent().show();
						}
	            	}
				}, async : false
			});
			
		}
	});



	$(document).on("click", ".selected-stylecolor", function(){
		
			$(this).addClass('active-color');
			var data_hex = $(this).attr("data-hex");
			var data_code = $(this).attr("data-code");

			var imgtexture = getAssetUrl()+"/images/ash.jpg";
			var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
			var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
			var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg";

			if(data_hex == "888888" || data_hex == "254195" || data_hex == "4e4e4e" || data_hex == "e1dfdf") {
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").css({"background":"#"+data_hex+" url("+imgtexture+")" });
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").attr("hex-id", data_hex );
				
			}else{
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").css({"background":"#"+data_hex });				
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").attr("hex-id", data_hex );
				
			}


			if(data_hex == "91999D") {
				// $(".tgen-shirt-holder img.shirt-col").css({"background":"#"+color_hex+" url("+imgtexture+")"});
				// $(".zoom-container").css({"background":"#"+color_hex+" url("+imgtexture+")"});
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").css({"background":"#"+data_hex+" url("+imgtexture+")" });
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").attr("hex-id", data_hex );
			}else if(data_hex == "E1E5E8") {
				
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").css({"background":"#"+data_hex+" url("+imgtexture2+")" });
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").attr("hex-id", data_hex );
			}else if(data_hex == "47506B") {
				
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").css({"background":"#"+data_hex+" url("+imgtexture3+")" });
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").attr("hex-id", data_hex );
			}else if(data_hex == "21292C") {
				
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").css({"background":"#"+data_hex+" url("+imgtexture4+")" });
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").attr("hex-id", data_hex );
			}else{
				
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").css({"background":"#"+data_hex });				
				$(this).parent().parent().parent().parent().find(".load-plan-shirt-image").attr("hex-id", data_hex );
			}
		
		

	});

	$(document).on("click",".cart-review-zoom", function(){
		$('#zoomer').modal('show');
		var hex_id = $(this).parent().find(".load-plan-shirt-image").attr('hex-id');
		
		var imgtexture = getAssetUrl()+"/images/heather-texture.png";
			if(hex_id == "888888" || hex_id == "254195" || hex_id == "4e4e4e" || hex_id == "e1dfdf") {
				$(".zoom-modal-holder").css({"background":"#"+hex_id+" url("+imgtexture+")"});
			}else{
				$(".zoom-modal-holder").css({"background":"#"+hex_id });				
				
			}
	});

	
    $(".design-category-select").on("change", function(){
		var catid = $(this).val();
		
		var ajaxURL = get_mainLink()+'campaign/show_category';
		$.ajax({
			type: "POST",
			url: ajaxURL,
			data:{
				catid: catid
			},success: function(data){
				var data = JSON.parse(data);	
				designcat(data);
			},
		});
	});
    

    $(document).on("click", ".cart-review-remove2", function(){
		
		$(this).parent().parent().addClass("removethis-design");
		setParentId($(this).parent().parent().find("removethis-design").attr('parent_id') );

	});

	$(document).on("click", '.remove-design-confirm', function(){
		var campaign_id = $('.dsgn-hold').attr('data-campid');
		$(".loader-container").hide();
		$('.removethis-design').remove(); //remove container of design
		//add blank design
		$("<div class='col-lg-2 col-md-4 col-sm-6 col-xs-12 blank-design' data-count=''><div class='managecamp-design-holder-blank'></div></div>").insertAfter(".blank-design:first");

		if( $(".blank-design").size() == 6 ){

			 $.ajax({
					type: 'POST',
					url:  get_mainLink()+'campaign/zero_design',
					data: {
						// product_details: getDataForUpdatingItems()
					}, success: function(ndata){
						var data = JSON.parse(ndata);	
						if( data['status'] == 'done'){
							// window.location.href = get_mainLink()+'cart/review';	
							window.location.href = get_mainLink()+'template/customize/'+campaign_id+'/0/1';						
						}
							
						
					}, 
				});
			// window.location.href = get_mainLink()+'template/customize/0/0/1';
		} 
		setTimeout(function() {
		localStorage.removeItem(getParentId());
			$(".design-display:first").click();
			$(".close").click();
			$(".loader-container").hide();
			reloadProductDetails();
		}, 2000);
	});

	$(document).on("click", ".managecamp-designbtn-remove", function(){
		// $(".blank-design").html("");
		$(this).parent().parent().parent().parent().parent().addClass("removethis-design");
		setParentId($(this).parent().parent().parent().parent().parent().find("removethis-design").attr('parent_id') );

	});

	$(document).on("click", ".style-check", function(){
		if($(this).hasClass("style-check-active")){			
			$(this).removeClass("style-check-active");
			$(this).parent().removeClass("addmorestyle-shirt-holder-active getMe recent-selected");
		}else{
			$(this).addClass("style-check-active");
			$(this).parent().addClass("addmorestyle-shirt-holder-active getMe recent-selected");

		}
	});

	$(document).on("click", ".btn-addmorestyles", function(){		
		var styles = "";
		var cont = "";
		var added_id = "";
		var selected_id = "";
		$(".cart-review-item-holder").removeClass("removethis_holder");
		$(".addmorestyle-shirt-holder").removeClass("removethis");
		$(".cart-review-item-holder").each(function(i){
			if ( i > 0 ) {
					styles += ","+$(this).attr("splan-id");
				}else {
					styles += $(this).attr("splan-id");
				}	
		});
		var array = styles.split(',');
		$(".addmorestyle-shirt-holder").removeClass("addmorestyle-shirt-holder-active");		
		$(".style-check").removeClass("style-check-active");
		for (var a = 0; a < array.length; a++) {
			added_id = array[a];
			$(".addmorestyle-shirt-holder").each(function(j){
				selected_id = $(this).attr("shirt-id");
				if( added_id == selected_id){
					$(this).addClass("addmorestyle-shirt-holder-active");
					$(this).find(".style-check").addClass("style-check-active");
				}
			});

		}					
		$(".addmorestyle-paginate").html( $(".addmorestyle-shirt-holder-active").size()+'/'+$(".addmorestyle-shirt-holder").size() );
	});

	$(document).on("click", ".addmorestyle-shirt-holder", function(){		
		var shirt_id = $(this).attr("shirt-id");
		if($(this).hasClass("addmorestyle-shirt-holder-active")){
			$(this).removeClass("addmorestyle-shirt-holder-active getMe recent-selected");
			$(this).find('.style-check').removeClass("style-check-active");
		}else{
			$(this).addClass("addmorestyle-shirt-holder-active getMe recent-selected");
			$(this).removeClass("removethis");
			$(this).find('.style-check').addClass("style-check-active");
			$(this).addClass("removethis");
			$(".cart-review-item-holder").each(function(i){
				if( $(this).attr("splan-id") == shirt_id ){
					$(this).removeClass("removethis_holder");
				}	
			});

		}
		$(".addmorestyle-paginate").html( $(".addmorestyle-shirt-holder-active").size()+'/'+$(".addmorestyle-shirt-holder").size() );
	});

	$(document).on("click", ".addmorestyle-shirt-holder-active", function(){
		var remove_shirt = $(this).attr("shirt-id");
		$(this).addClass("removethis");
		$(".cart-review-item-holder").each(function(i){
			if( $(this).attr("splan-id") == remove_shirt ){
				$(this).addClass("removethis_holder");
			}	
		});
	});
	

	$(document).on("click", ".removeshirt", function(){
		$(".removethis-style").removeClass("removethis-style");
		$(this).parent().parent().addClass("removethis-style");
		setShirtGroupId($('.removethis-style').find('.item-details').attr('splan-id'));
		
		$('.design-display[data-parent="'+getParentId()+'"] ').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').addClass('removethis-style');
	});

	$(document).on("click", ".remove-itemshirt", function(){
		
		var countshirt = $('.design-display[data-parent="'+getParentId()+'"] ').find('.product-details');
		

		if( countshirt.length == 1 ){
				
				if( $('.design-display .product-details').size() == 1 ){
				

					 $('.design-display[data-parent="'+getParentId()+'"] ').remove();
					 $('.dsgn-hold[data-parent="'+getParentId()+'"]').remove();
					 $("<div class='col-lg-2 col-md-4 col-sm-6 col-xs-12 blank-design' data-count=''><div class='managecamp-design-holder-blank'></div></div>").insertAfter(".blank-design:first");
					 $('.design-display:first').click();
					 $(".removethis-style").remove();
					 $(".close").click();


					 $.ajax({
						type: 'POST',
						url:  get_mainLink()+'campaign/zero_design',
						data: {
							product_details: getDataForUpdatingItems()
						}, success: function(ndata){
							var data = JSON.parse(ndata);	
							if( data['status'] == 'done'){
								// window.location.href = get_mainLink()+'cart/review';	
								window.location.href = get_mainLink()+'template/customize/'+getCampaignID()+'/0/1';						
							}
								
						}, 
					});

				}else{
				
					 $('.design-display[data-parent="'+getParentId()+'"] ').remove();
					 $('.dsgn-hold[data-parent="'+getParentId()+'"]').remove();
					 $("<div class='col-lg-2 col-md-4 col-sm-6 col-xs-12 blank-design' data-count=''><div class='managecamp-design-holder-blank'></div></div>").insertAfter(".blank-design:first");
					 $('.design-display:first').click();
					 $(".removethis-style").remove();
					 $(".close").click();
					 reloadProductDetails();
				}

		}else{
				
				$(".removethis-style").remove();
				$(".close").click();
				reloadProductDetails();

				
		}


	});

	
	$(document).on("click", ".Done-Style-Select",function(){
 		var styles = "";
		var cont = "";
		var added_id = "";
		var selected_id = "";
		$(".cart-review-item-holder").each(function(i){
			if ( i > 0 ) {
					styles += ","+$(this).attr("splan-id");
				}else {
					styles += $(this).attr("splan-id");
				}	
		});
		var array = styles.split(',');

		for (var a = 0; a < array.length; a++) {
			added_id = array[a];
			$(".addmorestyle-shirt-holder").each(function(j){
				selected_id = $(this).attr("shirt-id");
				if( added_id == selected_id){
					$(this).removeClass("getMe recent-selected");
				}
			});
		}
		if( $(".removethis").size() > 0 ){
			$(".removethis_holder").each(function(k){
				$(this).parent().remove();
				reloadProductDetails();	
			});
		}
		append_shirt();									
	});
	



	$(document).on("click",".store-selection-holder", function(){

    	$(this).find("design-select").addClass("design-active");
    }); 

	
	$(document).on("click", ".goto-preview-campaign", function(){  //GO to campaign preview
		
		ajaxUrl = get_mainLink()+'campaign/update_product_items';
		var price = '';
		var shirt = '';
		var min = '';
		var colors = '';
		var numshirt = 0;
		var numcolor = 0;
		var campaign_id = $('.dsgn-hold').attr('data-campid');
		var checkThisVal = 0;
		
		$('.design-display .product-details').each(function(i){
			
			price = Number( $(this).attr('data-price') );
			min = Number( $(this).attr('data-min') );
			colors = $(this).attr('data-colors');
			if( price == 0 ){					
				numshirt += 1;			
			}else{
				if( price < min){
					numshirt += 1;		
					checkThisVal = $(this).attr('data-price') ;			
				}
			}

			if(colors == ''){
				numcolor += 1; //count for no color shirts selected				
			}
			shirt = $(this).attr('data-shirt');
			
			if( numcolor > 0  ){
				$('.design-templates .dsgn-hold ').find('.design-display .product-details[data-colors=""]').click();
			}else if( numshirt > 0){
				$('.design-templates .dsgn-hold').find('.design-display .product-details[data-price="0"]').click();

			}else if(min < price ){
				$('.design-templates .dsgn-hold').find('.design-display .product-details[data-price="'+price+'"]').click();

			}
		});
				
		if( numshirt == 0 ){
			if( numcolor == 0 ){
					 $.ajax({
						type: 'POST',
						url: ajaxUrl,
						data: {

							product_details: getDataForUpdatingItems(),
							product_data: getALLdata(),
							campaign_id: campaign_id
						}, success: function(ndata){
							var data = JSON.parse(ndata);	
							
							if(  data['stat'] == "done"){
								window.location.href = get_mainLink()+'campaign/edit/'+campaign_id+'/'+data['customer_id'];							
							}else{
								$('#verifypop').modal('show');
							}
						}, 
					});

			}else{
				
				$("#alertmodal").modal('show');
				$('.text-confirm').text('Cannot proceed please add colors to your shirts.');

			}	  
		}else{
			$('.design-templates .dsgn-hold').find('.design-display .product-details[data-price="'+checkThisVal+'"]').click();
			
			
			
			$("#alertmodal").modal('show');
			$('.text-confirm').text('Cannot proceed please add price to your shirts');
		}	
		// setTimeout(function() {  $(".verify-close").click(); }, 3500);  

	});

	$(document).on("click", ".managecamp-design-holder-blank", function(){ //go back to TGEN
		 var campaign_id = getCampaignID();
			var parent_id = getParentId();		
		  $.ajax({
				type: 'POST',
				url: get_mainLink()+'campaign/campaign_update_items',
				data: {
					product_details: getDataForUpdatingItems(),
					product_data: getALLdata(),
					campaign_id: campaign_id,
					school : getSchoolId()


				}, success: function(ndata){
					var data = JSON.parse(ndata);	
						if( data['status'] == "done"){
							window.location.href = get_mainLink()+'template/customize/'+campaign_id+'/'+parent_id+'/1'; 
						}
						
				}, 
			});

	});



	$(document).on("click", ".edit-des", function(){ //go back to TGEN

		
		  $.ajax({
				type: 'POST',
				url: get_mainLink()+'campaign/buy_or_update_items',
				data: {
					product_details: getDataForUpdatingItems()
				}, success: function(ndata){
					var data = JSON.parse(ndata);

						if( data['status'] == "done"){
							window.location.href = get_mainLink()+'template/customize/'+getCampaignID()+'/'+getParentId()+'/1'; 
						}
						
				}, 
			});

	});
	
	$(document).on("click", ".update-items", function(){ //go back to TGEN
			var campaign_id = getCampaignID();			
			var parent_id = getParentId();	
			
		   $.ajax({
				type: 'POST',
				url: get_mainLink()+'campaign/campaign_update_items',
				data: {
					product_details: getDataForUpdatingItems(),
					campaign_id: campaign_id
				}, success: function(ndata){
					var data = JSON.parse(ndata);	
					
					if(  data['status'] == "done"){
						window.location.href = get_mainLink()+'template/customize/'+campaign_id+'/'+parent_id+'/1'; 
						
					}
					
				}, 
			});


	});

	 $(document).on("blur",".managecamp-sellingprice",function(){ 
	 		var selling_price = $(this).val().replace('$', '');
	 		
		 	// var sprice =	selling_price.replace("$", "");
	 		$(".float-error").remove();
		 	var minprice = Number ($(this).parent().parent().find(".min-price").text().replace("$", "") ) ;
		 		setShirtGroupId ($(this).parent().parent().parent().parent().find('.item-details').attr('splan-id') );
		 			 
		 	if( minprice == getPrice() ){
		 		$(this).parent().parent().find(".profit").css('color' ,'#000' );
		 	} 

		 	if( minprice > getPrice()  ){ 
				$(this).parent().append('<div class="float-error">Must not be lower than the Minimum price</div>');
				$(this).parent().parent().find(".profit").css('color' ,'#D12F19' );	
				// $(this).focus();
			}else{
				$(".float-error").remove();
				$(this).parent().parent().find(".profit").css('color' ,'#000' );
			}
			var profit =   getPrice() - minprice;
			
			$(this).val('$'+Number( selling_price ).toFixed(2) );


			$(this).parent().parent().find(".profit").text( '$'+profit.toFixed(2));
			// $(this).parent().parent().find(".sell-price").text( '$'+profit.toFixed(2));

		 	$('.design-display[data-parent="'+getParentId()+'"]').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').attr("data-price",getPrice() );
		 	$('.design-display[data-parent="'+getParentId()+'"]').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').attr("data-min", minprice ); 
		 	check_selling_prices();
	 	// }	// $("#sell-price").val( getPrice() );
	 });

	 
		/*function numeric_only(){
			$(document).on("keypress", ".managecamp-sellingprice", function (e) {
		    // 46 is the keypress keyCode for period     
			    // http://www.asquare.net/javascript/tests/KeyCode.html
			    if (e.keyCode === 46 && this.value.split('.').length === 2) {
			        return false;
			    }
			});
		}

	  $(document).on("keyup",".managecamp-sellingprice",function(){ 
	  	numeric_only();
	  	// $("#sell-price").val( $('.design-display[data-parent="'+getParentId()+'"]').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').attr("data-price") );
	  	var minprice = Number ($(this).parent().parent().find(".min-price").text().replace("$", "") ) ;
	 		setShirtGroupId ($(this).parent().parent().parent().parent().find('.item-details').attr('splan-id') );
	 	
	 	if( minprice == getPrice() ){
	 		$(this).parent().parent().find(".profit").css('color' ,'#D12F19' );
	 		 
	 	} 
	 	

	 	if( minprice > getPrice()  ){ 
			$(this).parent().append('<div class="float-error">Must not be lower than the Minimum price</div>');
			$(this).parent().parent().find(".profit").css('color' ,'#D12F19' );	
			
			// $(this).focus();
		}else{
			$(".float-error").remove();
			$(this).parent().parent().find(".profit").css('color' ,'#000' );
		

		}
	 	var profit =   getPrice() - minprice;
	 	
		$(this).parent().parent().find(".profit").text( '$'+profit.toFixed(2));
		$(this).parent().parent().find(".sell-price").val( '$'+getPrice() );
		
		$('.design-display[data-parent="'+getParentId()+'"]').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').attr("data-price",getPrice() );
		$('.design-display[data-parent="'+getParentId()+'"]').find('.product-details[data-shirt="'+getShirtGroupId()+'"]').attr("data-min", minprice );
	 });*/

	  $(document).on("click", ".managecamp-designbtn-edit", function(){
	  		var campaign_id = $('.dsgn-hold').attr('data-campid');
	  		var template_id = $(this).parent().parent().parent().parent().attr('data-parent');
	  		
		   $.ajax({
				type: 'POST',
				url: get_mainLink()+'campaign/campaign_update_items',
				data: {
					product_details: getDataForUpdatingItems(),
					campaign_id: campaign_id
				}, success: function(ndata){
					var data = JSON.parse(ndata);	
					
					if(  data['status'] == "done"){
						window.location.href = get_mainLink()+'template/customize/'+campaign_id+'/'+template_id+'/1'; 
					}
					
				}, 
			});
	  });
	  
	  
		$(document).on("click", ".data-sizechart", function(e){
			
			var shirt_style_id = $(this).parent().find('.name-plan').attr('data-shirtstyleid');
			populateChartDetails( shirt_style_id );
			
		});
});

	

