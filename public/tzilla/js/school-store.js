var xhr;
var xhr_search;
var bgColor = '';
var designName = '';
var prevDesigns = '';
var colorValue1 = '';
var colorValue2 = '';
var category_arr = '';
var imageThumbnail = '';
var parentID = 0;
var schoolId = 0;
var templateID = 0;
var items_count = 0;
var designLists = [];

function setPrimaryColor( value ) {
	colorValue1 = value;
}

function getPrimaryColor() {
	return colorValue1;
}

function setSecondaryColor( value ) {
	colorValue2 = value;
}

function getSecondaryColor() {
	return colorValue2;
}

function setPreviousLoadedDesigns( value ) {
	prevDesigns = value;
}

function getPreviousLoadedDesigns() {
	return JSON.parse(prevDesigns);
}

function setSchoolId( value ) {
	schoolId = value;
}

function getSchoolId() {
	return schoolId;
}

function setItemCount( value ) {
	items_count = value;
}

function getItemCount() {
	return items_count;
}

function setArtworkCategories( value ) {
	category_arr = value;
}

function getArtworkCategories() {
	return category_arr;
}

function setDesignName( value ) {
	designName = value;
}

function getDesignName() {
	return designName;
}

function setActiveParent( value ) {
	parentID = value;
}

function getActiveParent() {
	return parentID;
}

function setActiveArtwork( value ) {
	templateID = value;
}

function getActiveArtwork() {
	return templateID;
}

function setTemplateThumb( value ) {
	imageThumbnail = value;
}

function getTemplateThumb() {
	return imageThumbnail;
}

function setBackgroundColor( value ) {
	bgColor = value;
}

function getBackgroundColor() {
	return bgColor;
}

function checkForChildCategory( parentId, arrData, fieldToCheck ) {
	var retVal = false;

	if ( arrData.length > 0 ) {
		for (var i = 0; i < arrData.length; i++) {
			if ( parentId == arrData[i][fieldToCheck] ) {
				retVal = true;
				break;
			}
		}
	}

	return retVal;
}

function setDesignItems() {
	designLists = [];

    $('.selected-design-items').each(function(i){
    	designLists[i] = { parent_id : $(this).find(".design-image").attr("data-parent"), template_id : $(this).find(".design-image").attr("data-id") };
    });
}

function getDesignItems() {
	return designLists;
}

function loadArtworkCategories(){
	var nData = JSON.parse( getArtworkCategories() );
	var appendHTML = '<li class="active t-cat"><a href="javascript:void(0);">All</a></li>';
	if ( nData.length > 0 ) {
		$(".artwork-categories").html("");

		for (var a = 0; a < nData.length; a++) {
			if ( nData[a]['parent_id'] == 0 ) {
				if ( nData[a]['code'] == 'Joker' ) {

				} else {
					if ( checkForChildCategory( nData[a]['id'], nData, 'parent_id' ) ) {
						appendHTML += '<li class="dropdown">'+'<a class="dropdown-toggle t-cat" data-toggle="dropdown" href="javascript:void(0);" data-id="'+nData[a]['id']+'">'+nData[a]['code']+'<span class="caret"></span></a>'+'<ul class="dropdown-menu">';
						for (var b = 0; b < nData.length; b++) {
							if ( nData[a]['id'] == nData[b]['parent_id'] ) {
								appendHTML += '<li><a href="javascript:void(0);" class="sub-cat" data-id="'+nData[b]['id']+'">'+nData[b]['code']+'</a></li>';
							}
						}

						appendHTML += '</ul>'+'</li>';
					} else {
						appendHTML += '<li class="art-category"><a href="javascript:void(0);" data-id="'+nData[a]['id']+'">'+nData[a]['code']+'</a></li>';

					}
				}
			}

		}
	}

	$(".artwork-categories").prepend(appendHTML);
}

function addDesignItem( parentID = 0 ) {
	var item = '';
	var toUpdate = false;
	var _arr = {};
	$(".empty-collection").remove();

	if ( getItemCount() > 0 ) {
		// loop for selected design items
		$(".selected-design-items").each(function(i){
			if ( parentID == $(this).find(".design-image").attr("data-parent") ) {
				$('.selected-design-items .design-image[data-parent="'+getActiveParent()+'"]').attr( { "data-id" : getActiveArtwork(), "src" : getTemplateThumb() } );
				toUpdate = true;
			}
		});
		
		
	}

	if ( !toUpdate ) {
		setItemCount( getItemCount() + 1 );

		item = '<div class="design-item-holder-small selected-design-items">'+
					'<div class="store-small-box" style="background-color: #'+getBackgroundColor()+';">'+
						'<img class="img-responsive img-center design-image" data-parent="'+getActiveParent()+'" data-id="'+getActiveArtwork()+'" src="'+getTemplateThumb()+'" alt="" />'+
					'</div>'+
					'<div class="design-item-label gregular font-xsmall gray text-uppercase text-left"><span class="design-name gsemibold">'+getDesignName()+'</span></div>'+
					'<span class="remove-design-box"></span>'+
					'<div class="clearfix"></div>'+
				'</div>';

				$("img[data-id='"+getActiveParent()+"']").parent().find(".design-select").removeClass("design-active").addClass("design-active");
				$("img[data-id='"+getActiveParent()+"']").parent().parent().find(".add-design").html("Update this Design");
	
	}

	_arr[getActiveParent()] ={};
	_arr[getActiveParent()]["parent_id"] = getActiveParent();
	_arr[getActiveParent()]["variation_"] = getActiveArtwork();
	_arr[getActiveParent()]["background"] = getBackgroundColor();
	_arr[getActiveParent()]["designName"] = getDesignName();
	_arr[getActiveParent()]["image"] = getTemplateThumb();
	var art = JSON.stringify(_arr);
	localStorage.setItem(getActiveParent(),art);
	// append to
	$(".design-cont-item-holder").prepend( item );
	$("img[data-parent='"+getActiveArtwork()+"']").parent().find(".design-select").removeClass("design-active").addClass("design-active");
	$(".design-count").text(getItemCount());
	setDesignItems();

	changeButtonState();	
}

function loadDesignVariations( data ){
	$('.owl-carousel').trigger('replace.owl.carousel', data);
	$('.owl-carousel').trigger('refresh.owl.carousel');
}

function bannerColor(hex) {
	$('.store-header').css({'background':'url('+get_mainLink()+'public/tzilla/images/school-store-texture.png) center center no-repeat '+hex});
	$('.store-header').css({'background-size':'cover'});
}

function autoloadPreviousSelectedDesigns() {
	var selectedDesignsArr = getPreviousLoadedDesigns();

	for ( var y = 0; y < selectedDesignsArr.length; y++ ) {
				console.log(selectedDesignsArr[y]['template_id']);
		$.ajax({
			url: get_mainLink()+"template/load_school_artwork_by_id/"+selectedDesignsArr[y]['template_id'],
			type: "POST",
			success: function(data){
				var nData = JSON.parse( data );
				console.log(nData);

				if ( nData.length > 0 ) {
					for ( var a = 0; a < nData.length; a++ ) {
						var img = nData[a]['thumbnail'];
						setBackgroundColor(nData[a]['hex_value']);
						setActiveParent(nData[a]['parent_id']);
						setActiveArtwork(nData[a]['template_id']);
						setTemplateThumb( img);
						setDesignName(nData[a]['display_name']);

						addDesignItem(getActiveParent());
						$(".dc-close").click();
						$("img[data-id='"+nData[a]['parent_id']+"']").parent().find(".design-select").removeClass("design-active").addClass("design-active");
					}
				}
			},
			async:true
		});
	}
}

function changeButtonState() {
	if ( $(".customize-designs").hasClass("gray-btn") && getItemCount() > 0 ) {
		$(".customize-designs").addClass("green-btn").removeClass("gray-btn");
		$(".customize-designs").removeAttr('disabled');
	}
}

$(document).ready(function(){
	// UPDATED CODES
	$(function(){
		var hex = '#'+getPrimaryColor();
		bannerColor(hex);
		$("#storetitle").text('TZilla.com - School Store');
	});
	//Clean Local Storage
	var slideItem = ""
	for (var i = 0; i < localStorage.length; i++){
    	var _jsonData = localStorage.getItem(localStorage.key(i));
    	
		var _arrData = JSON.parse(_jsonData);
		if(i == 0)
		{
			if(localStorage.length > 0)
			{
				$(".design-cont-item-holder").empty();				
			}			
		}		
		
		$.each(_arrData, function(i,obj){
			if(obj != "" )
			{
				item = '<div class="design-item-holder-small selected-design-items">'+
					'<div class="store-small-box" style="background-color: #'+obj['background']+';">'+
						'<img class="img-responsive img-center design-image" data-parent="'+obj['parent_id']+'" data-id="'+obj['variation_']+'" src="'+obj['image']+'" alt="" />'+
					'</div>'+
					'<div class="design-item-label gregular font-xsmall gray text-uppercase text-left"><span class="design-name gsemibold">'+obj['designName']+'</span></div>'+
					'<span class="remove-design-box"></span>'+
					'<div class="clearfix"></div>'+
					'</div>';
				$(".design-cont-item-holder").append( item );
				$(".store-image-variation[data-parent="+obj['parent_id']+"]").parent().find(".add-design").html("Update this Design");
				$(".store-image-box .designmain.design-template-image[data-id='"+obj['parent_id']+"']").attr("src",obj['image']);	
			}			
			
		});
		if($(".design-cont-item-holder").children().length == 0)
		{
			setItemCount( 0 );
			$(".design-cont-item-holder").html('<div class="empty-collection gregular gray-light">Empty</div>');
			changeButtonState();
		}
		else
		{
			setItemCount($(".design-cont-item-holder").children().length);
			changeButtonState();
		}
		setDesignItems();
		
		$(".design-count").text(getItemCount());

	
	}
	$(document).on("click", ".art-category", function(){
		if ( $(this).attr("data-id") == 0 ) {
			window.location.href = get_mainLink()+"/school/store/"+getSchoolId();
		} else {
			window.location.href = get_mainLink()+"/school/store/"+getSchoolId()+"/"+$(this).attr("data-id");
		}
	});
	
	$(document).on("click", ".store-image-box", function(){
		$(this).parent().find(".load-variations").click();
	});
	
	$(document).on("click", ".load-variations", function(){
		// hide and show for the list of variations
		$(".store-holder-variation").removeClass("active-var");
		$(".design-variation-btn").removeClass("design-variation-btn-hide");
		$(".store-selection-holder").removeClass("active-var");
		$(this).parent().find(".store-selection-holder").addClass("active-var");
		$(this).parent().find(".design-backprint").addClass("active-var");
		$(this).parent().find(".store-holder-variation").addClass("active-var");
		$(this).addClass("design-variation-btn-hide");
		var img = $(this).parent().find(".design-template-image").attr("data-source");

		setActiveArtwork( $(this).parent().find(".design-template-image").attr("data-id") );
		setTemplateThumb( img );
		setActiveParent( getActiveArtwork() );
		setDesignName( $(this).parent().find(".design-name").text() );

		// load_artwork_variations
		if(xhr && xhr.readystate != 4){
			xhr.abort();
		}

		// load all child artworks from Database
		xhr = $.ajax({
			url: get_mainLink()+"template/load_artwork_variations/"+$(this).attr("data-id"),
			type: "POST",
			success: function(data){
				var nData = JSON.parse( data );
				var templateVariations = "";

				templateVariations = '<div class="design-variation-holder design-variation-item design-variation-active" data-id="'+getActiveArtwork()+'"><img class="img-responsive img-center design-image" src="'+getTemplateThumb()+'" alt="" /></div>';
				console.log( getTemplateThumb() );
				if ( nData.length > 0 ) {
					$(".variation-arrow").show();
					for ( var x = 0; x < nData.length; x++ ) {
						var img2 = nData[x]['thumbnail'];
						templateVariations += '<div class="design-variation-holder design-variation-item" data-id="'+nData[x]['template_id']+'"><img class="img-responsive img-center design-image" src="'+img2+'" alt="" /></div>';
					}
				}

				if( nData.length <= 2){
					$(".variation-arrow").hide();
				}
				
				loadDesignVariations( templateVariations );
			},
				async:false
		});
	});

	// ADD Artwork to selected designs
	$(document).on("click", ".add-design", function(){
		setBackgroundColor( $(this).parent().attr("data-hex") );

		// remove empty display		
		// check if the parent id has already been added
		if ( getItemCount() <= 1 ) {
			addDesignItem( $(this).parent().find(".store-image-variation").attr("data-parent") );

			// show slider
			if($(".design-cont-btn").hasClass("dc-close")){
				$(".design-cont-btn").click();
			}

		} else if ( $(".selected-design-items").length < 6 ) {
			$(this).html("Update this Design");
			$(".design-select").html("");
			
			addDesignItem( getActiveParent() );

			// show slider
			if($(".design-cont-btn").hasClass("dc-close")){
				$(".design-cont-btn").click();
			}
		} else {
			if ( $('.selected-design-items .design-image[data-parent="'+getActiveParent()+'"]').length > 0 ) {
				addDesignItem( getActiveParent() );
			} else {
				$("#confirmitem").modal("show");
			}
		}
	});

	// load selected designs on tgen
	$(document).on("click", ".customize-designs", function(){
		if(xhr && xhr.readystate != 4){
			xhr.abort();
		}

		// redirect to tgen with the selected designs
		xhr =  $.ajax({
			type: 'POST',
			url: get_mainLink()+'template/generator',
			data: {
				school : getSchoolId(),
				designs : getDesignItems()
			},
			success: function(data){
				window.location.href = get_mainLink() + data;
			},
			async:true
		});
	});

	$(document).on("click", ".design-variation-item", function(){
		var img = $(this).find(".design-image").attr("src");
		setActiveArtwork( $(this).attr("data-id") );
		setTemplateThumb( img );

		$('.design-template-image[data-id="'+getActiveParent()+'"]').attr( "src", getTemplateThumb() );
	});

	$(document).on("click", ".t-cat", function(){
		$(".t-cat").removeClass("selected-t-cat");
		$(".t-cat").parent().removeClass("active");
		$(this).addClass("selected-t-cat");
		$(this).parent().addClass("active");
	});

	$(document).on("click", ".sub-cat", function(){
		$(".sub-cat").removeClass("selected-sub-cat");
		$(this).addClass("selected-sub-cat");
	});

	// END UPDATED CODES

	$(document).on("click", ".remove-design-box", function(){
		var removeid = $(this).parent().find( ".store-small-box img" ).attr("data-id");
		var removeparent = $(this).parent().find( ".store-small-box img" ).attr("data-parent");
		$(this).parent().remove();
		$(".design-item-holder").find(".store-image-box").find( "img[data-id*='"+removeparent+"']" ).parent().find(".store-selection-holder").removeClass("active-on").find(".design-select").removeClass("design-active");
		$(".design-item-holder").find(".store-image-box").find( "img[data-id*='"+removeid+"']" ).parent().find(".store-selection-holder").removeClass("active-on").find(".design-select").removeClass("design-active");
		$(".design-item-holder").find(".store-image-box").find( "img[data-id*='"+removeid+"']" ).parent().parent().find(".adddesign-variation-btn").html("Add this Design");
		$(".design-item-holder").find(".store-image-box").find( "img[data-id*='"+removeparent+"']" ).parent().parent().find(".adddesign-variation-btn").html("Add this Design");

		var item = $(".design-item-holder-small").length;
		if(item<1) {
			$(".design-cont-item-holder").append("<div class='empty-collection gregular gray-light'>Empty</div>");
			$(".customize-designs").addClass("gray-btn").removeClass("green-btn");
			$(".customize-designs").attr("disabled","disabled");
		}else {
		
		}
		localStorage.removeItem(removeparent);
		setItemCount(item);
		$(".design-count").text(getItemCount());

		// this line of code is to refresh the collection content before going to tgen
		setDesignItems();
	});

	/* $(document).on("click", ".design-unselect", function(){
		$(this).parent().removeClass("active-on").find(".design-select").removeClass("design-active");
		var cartid = $(this).parent().parent().find("img.designmain").attr("data-id");
		$(".design-cont-item-holder").find("img[data-id*='"+cartid+"']").parent().parent().remove();
		$(".design-item-holder").find(".store-image-box").find( "img[data-id*='"+cartid+"']" ).parent().parent().find(".adddesign-variation-btn").html("Add this Design");
		var item = $(".design-item-holder-small").length;
		if(item<1) {
			$(".design-cont-item-holder").append("<div class='empty-collection gregular gray-light'>Empty</div>");
			$(".customize-designs").addClass("gray-btn").removeClass("green-btn");
		}	
	}); */

	$(document).on("click", ".design-variation-holder", function(){
		$(this).parent().parent().find(".design-variation-holder").removeClass("design-variation-active");
		$(this).addClass("design-variation-active");
		img = $(this).find("img").attr("src");
		id = $(this).attr("id");

		$(this).parent().parent().parent().parent().parent().parent().parent().parent().find(".designmain").attr("id",id).attr("src",img);
	});

	$(".store-image-variation").find(".design-variation-holder").removeClass("design-variation-active");
	$(".store-image-variation").find(".design-variation-holder:eq(0)").addClass("design-variation-active");

});