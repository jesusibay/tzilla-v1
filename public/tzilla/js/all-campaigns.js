var base_url = "";
var category_arr = '';
var activeFil = '';


function setActive(value){
	activeFil = value;
}

function getActive(){
	return activeFil;
}

function setBaseURL(val){
	base_url = val;
}

function getBaseURL() {
	return base_url;
}

function setArtworkCategories( value ) {
	category_arr = value;
}

function getArtworkCategories() {
	return category_arr;
}

function checkForChildCategory( parentId, arrData, fieldToCheck ) {
	var retVal = false;

	if ( arrData.length > 0 ) {
		for (var i = 0; i < arrData.length; i++) {
			if ( parentId == arrData[i][fieldToCheck] ) {
				retVal = true;
				break;
			}
		}
	}

	return retVal;
}


function loadArtworkCategories(){
	var nData = JSON.parse( getArtworkCategories() );
	var activeMe ="";
	var appendHTML = '<li ><a href="javascript:void(0);" class=" t-cat"><label  class="art-category"  data-id="">All</label></a></li>';

	if ( nData.length > 0 ) {
		$(".artwork-categories").html("");

		for (var a = 0; a < nData.length; a++) {
			if( getActive() ==  nData[a]['id'] ){
						activeMe = "active";
			}else{
				activeMe = "";
			}

			if ( nData[a]['parent_id'] == 0 ) {
				if ( nData[a]['code'] == 'Joker' ) {
					
				} else {
					if ( checkForChildCategory( nData[a]['id'], nData, 'parent_id' ) ) {
						
						appendHTML += '<li class="'+activeMe+'"><a href="javascript:void(0);" class=" t-cat"><label  class="art-category" data-id="'+nData[a]['id']+'">'+nData[a]['code']+'</label></a></li>';
						
					} else {
						appendHTML += '<li class="'+activeMe+'" ><a href="javascript:void(0);"class=" t-cat" ><label  class="art-category" data-id="'+nData[a]['id']+'">'+nData[a]['code']+'</label></a></li>';

					}
				}
			}

		}
	}

	$(".artwork-categories").prepend(appendHTML);
}

	

$(document).ready(function(){
	// pre-load function cvalls here
	$(function(){
		loadArtworkCategories();  
		$("#storetitle").text('TZilla.com - Current Campaigns');
	});


	$( "#category-ul" ).on("click","li.sub-cat", function(event) {    
		var subname = $(this).text();
		var ajaxURL = get_mainLink()+'campaign/lists';
      	
      	$.ajax({
      		type: "POST", 
      		url: ajaxURL,
      		data:{
      			subname : subname
      		},success: function(data){
      			
      		},

      	});
      	
    });


	$(document).on("click", ".design-variation-btn", function(){
		$(".store-holder-variation").removeClass("active-var");
		$(".design-variation-btn").removeClass("design-variation-btn-hide");
		$(".store-selection-holder").removeClass("active-var");
		$(".design-backprint").removeClass("active-var");
		$(this).parent().find(".store-selection-holder").addClass("active-var");
		$(this).parent().find(".design-backprint").addClass("active-var");
		$(this).parent().find(".store-holder-variation").addClass("active-var");
		$(this).addClass("design-variation-btn-hide");
	});
	
	$(document).on("click", ".adddesign-variation-btn", function(){
		var item = '<div class="design-item-holder-small">';
			item +='<div class="store-small-box">';
			item +='<img class="img-responsive img-center" src="'+getBaseURL()+'"/images/le-tigre.png" alt="" />';
			item +='</div>';
			item +='<div class="design-item-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">Slant</span></div>';
			item +='<span class="remove-design-box"></span>';
			item +='<div class="clearfix"></div>';
			item +='</div>';		
		$(".design-cont-item-holder").append(item);
		$(".customize-btn").addClass("green-btn").removeClass("gray-btn");
		$(this).parent().parent().find(".design-select").addClass("design-active");
		if($(".design-cont-btn").hasClass("dc-open")){
			$(".design-cont-btn").click();
		}
		
			
	});
	
	
	$(document).on("click", ".remove-design-box", function(){
		$(this).parent().remove();
	});
	
	$(document).on("click", ".design-variation-holder", function(){
		$(this).parent().parent().find(".design-variation-holder").removeClass("design-variation-active");
		$(this).addClass("design-variation-active");
	});
	
	
	$(document).on("click", ".art-category", function(){
		
		if ( $(this).attr("data-id") == 0 ) {
			window.location.href = get_mainLink()+"campaign/lists/";
		} else {
			window.location.href = get_mainLink()+"campaign/lists/"+$(this).attr("data-id");
		}
	});


	$(document).on("click", ".t-cat", function(){
		$(".t-cat").removeClass("selected-t-cat");
		$(".t-cat").parent().removeClass("active");
		$(this).addClass("selected-t-cat");
		$(this).parent().addClass("active");
	});

	$(document).on("click", ".sub-cat", function(){
		$(".sub-cat").removeClass("selected-sub-cat");
		$(this).addClass("selected-sub-cat");
	});

	//searchbox
	$(document).on("click", "#term_search", function(e){
			 
	 	$("#campaign-search").submit();

	});


	$(document).on("change", "#selectFilter", function(){
		var filter = $(this).val();
		var ajaxUrl = get_mainLink()+'campaign/filter';	
		if( filter == '' ){
			localStorage.removeItem("lastElement");
			window.location.href = get_mainLink()+'campaign/lists';
		}else if( filter == 'mostpopular'){			
			window.location.href = get_mainLink()+'campaign/filter/'+filter;
		}else if( filter == 'latest'){			
			window.location.href = get_mainLink()+'campaign/filter/'+filter;
		}else if( filter == 'endingsoon'){			
			window.location.href = get_mainLink()+'campaign/filter/'+filter;
		}else{			
			window.location.href = get_mainLink()+'campaign/filter/'+filter;
		}		

	});

});