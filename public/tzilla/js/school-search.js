function show_schoollist(data){
	$(".school-pop-data").html(data['sch']);
}

function get_schoollist(){
	var schoolname = $("#schoolname").val();
	var statename = $("#state-select").val();
	var zipcode = $("#zipcode").val();
	var ajaxURL = get_mainLink()+'school/get_school';

	$.ajax({
		type: "POST",
		url: ajaxURL,
		data:{
			schoolname: schoolname,
			statename: statename,
			zipcode: zipcode

		},
		success: function(data){
			var data = JSON.parse(data);
			show_schoollist(data);
			$(".search-term").html( schoolname );
			$(".search-count").html( '('+data['count']+')' );
		},
			async:false
	});	
}

function upload_photo(slug) {
    var _file = document.getElementById('userfile');
   if (_file.files.length === 0) {
        return;
    }
      var data = new FormData();
      data.append('SelectedFile', _file.files[0]);

      var request = new XMLHttpRequest();
      var upldURL = get_mainLink()+'school/upload_photo/'+slug;
      request.open( 'POST', upldURL );
      request.send(data);      
        request.onload = function () {
        // do something to response
            var result = JSON.parse( this.responseText );
            // alert( result['message'] );
            if( result['status'] == 'success' ){
            	update_school_data();
            }
        };                  
}


function update_school_data(){
	var ajaxURL = get_mainLink()+'school/send_email_no_school';

	$.ajax({
			type: "POST",
			url: ajaxURL,
			data:{
				fullname: $("#luk_fullname").val(),
				email: $("#luk_email").val(),
				school_name: $("#luk_school_name").val(),
				school_state: $("#luk_state").val(),
				contact_no: $("#luk_contact").val(),
				role: $("#luk_role").val(),
				color1: $(".selected-col-text1").html(),
				color2: $(".selected-col-text2").html(),
				color_1: $("#luk_color1").val(),
				color_2: $("#luk_color2").val(),
				notes: $("#luk_notes").val(),
				school_img: $('.cantfind-uploader').html(),
				recaptcha: grecaptcha.getResponse(gschool_search)

			},
			success: function(ndata){
				var data = JSON.parse(ndata);
				
				if(data['status']=='success'){
					$("#alertmodal").modal('show');
					$(".text-confirm").text("Inquiry successfully sent");
					setTimeout(function() {
						$("#alertmodal").modal('hide');
						$(".close").click();
						grecaptcha.reset(gschool_search);
						// location.reload();
					}, 2000);
				}else{
					$("#alertmodal").modal('show');
					$(".text-confirm").text("Inquiry Not Sent");
					setTimeout(function() {
						$("#alertmodal").modal('hide');
						// $(".close").click();
						grecaptcha.reset(gschool_search);
					}, 2000);

				}
			},
				async:false
		});
}


$(document).ready(function() {

	$("#storetitle").text('TZilla.com - School Search');
        /*$(function(){
            var gschool_search;
            var onloadCallback = function() {
                gschool_search = grecaptcha.render(document.getElementById('school_search_element'), {
                    'sitekey' : '6LfkVxkUAAAAAJ9P3_Kzq8T6N4Me2F8DiE4LyCGd',
                    'callback' : verifyCallback,
                    'theme' : 'light'
                });

                
            };
        });*/
        /*$(function(){
         var gschool_search;
     
	      var onloadCallback = function() {
	       
	        gschool_search = grecaptcha.render(document.getElementById('school_search_element'), {
	          'sitekey' : '6LfkVxkUAAAAAJ9P3_Kzq8T6N4Me2F8DiE4LyCGd'
	        });

	      };
		});*/
	
	$(document).on("click", ".cantfind-submit", function(e){

		e.preventDefault();
		validate_luk();

		var validated = true;
		if( grecaptcha.getResponse(gschool_search).length === 0 ){
              $(".validation-school").text( "Please check the captcha form." );
                setTimeout(function() { $(".validation-school").text(""); }, 4700);
                validated = false;
                return false;
          }


		if( $(".error").size() == 0 ){
			
			if( $(".email-fail").size() == 0 ){
				
				
				if( $('#userfile').val() != "" ){
					upload_photo( $('.cantfind-uploader').html() );
					// update_school_data();

				}else{
					
					update_school_data();
				}
			}
			
		}
	});

	

	//color selection primary
	$(".primarycol").click(function() {
		$(this).find(".color-primary").toggle();
		$(".color-secondary").hide();
		// $(this).find(".hideme").hide();
	});

	$(".primarycol .col-selection").click(function() {
		var title = $(this).attr("title");
		var hex = $(this).attr("data-hex");
		$(".selected-col-text1").html(title);
		$(".selected-col1").css("color","#"+hex);		
		$("[name='color1']").val(hex);
		if( $("#luk_color1").val() != "" ){
			$("#luk_color1").removeClass("error");
			$(".primarycol").parent().find('.error-label').remove();
		}
		$(".color-secondary").find(".col-selection[data-hex='"+$("#luk_color1").val()+"']").addClass("hideme");

	});

	//color selection secondary
	$(".secondarycol").click(function() {
		$(this).find(".color-secondary").toggle();
		$(".color-primary").hide();
		// $(this).find(".hideme").hide();
	});

	$(".secondarycol .col-selection").click(function() {
		var title = $(this).attr("title");
		var hex = $(this).attr("data-hex");
		
		$(".selected-col-text2").html(title);
		$(".selected-col2").css("color","#"+hex);
		$("[name='color2']").val(hex);
		$(".remove-col2").show();

		if( $("#luk_color2").val() != "" ){
			$("#luk_color2").removeClass("error");
			$(".secondarycol").parent().find('.error-label').remove();
		}
		$(".color-primary").find(".col-selection[data-hex='"+$("#luk_color2").val()+"']").addClass("hideme");

	});

	$(".remove-col2").click(function() {
		var title = $(this).attr("title");
		var hex = $(this).attr("data-hex");
		
		$(".selected-col-text2").html("").append("<span>Select Color 2</span>");
		$(".selected-col2").css("color","#ccc");
		$("[name='color2']").val("");
		$(".remove-col2").hide();
	});

	//upload img
	$('.cantfind-uploader, #uploadButton').on('click', function() {
	    $('#userfile').click();
	});
		
	 $('#userfile').change('change', function(){ 
		 var dir = $(this).val();
			    var lastIndex = dir.lastIndexOf("\\");
			    var ftype = $('#userfile')[0].files[0].type;
		 		var fsize = $('#userfile')[0].files[0].size;
			 	var _URL = window.URL || window.webkitURL;
		 		var file, img;
			 	 
		 		if (lastIndex >= 0) {
			        dir = dir.substring(lastIndex + 1);			   	 
		 		}
			 	
			  	if (ftype != "image/jpeg" && ftype != "image/jpg" && ftype != "image/png"    )
 				{
 					// alert(" Image should be in JPG/PNG format. ");	
 					$("#alertmodal").modal('show');
 					$('.text-confirm').text("Image should be in JPG/PNG format.");
 					setTimeout(function() {             
                    $("#alertmodal").modal('hide');
                    
	                }, 2000);
 				}
 				else
				{
					if ( fsize > 2097152 )
			 		{
			 			// alert("Image is larger than 2mb.");
			 			$("#alertmodal").modal('show');
 						$('.text-confirm').text("Image is larger than 2mb.");
 						setTimeout(function() {             
		                    $("#alertmodal").modal('hide');
		                }, 2000);
			 		}else{

			 			if ((file = this.files[0])) {
				 		 img = new Image();
					        img.onload = function() {
				 			
				 			/*var file_width = this.width;
				 			var file_height = this.height;
				 			if( file_width != "750" || file_height != "750"){
				 				alert("Image dimensions must be 750x750.")
				 			}else{*/
			 					$(".cantfind-uploader").text(dir.split('\\').pop());
			 									
				 			//}

					        };

					        img.src = _URL.createObjectURL(file);
					    }
			 		}
				}




		
	 });
	 
	//captcha
	$(".checkbox-robot").click(function(){
		$(this).toggleClass("checker-box-active");
	});

	$('.btn-search-sch').on('click', function() {
		var schoolname = $("#schoolname").val();
		var schoolstate = $("#state-select").val();
		var zipcode = $("#zipcode").val();
		localStorage.clear();
		if( schoolname == '' ){
			$("#schoolname").focus();
			$('<div class="float-error">This field is required.</div>').appendTo('.schoolname');
			$("#schoolname").addClass("error");
			if( $("#schoolname").find(".float-error").size() > 1 ){
                 $("#schoolname").find('.float-error').last().remove();
            }
		}else if(schoolstate == '' && zipcode == '' ){
			$("#state-select").focus();
			$("#zipcode").focus();
			$('<div class="float-error">Please enter state or zip code.</div>').insertAfter('#zipcode');

			
				// $("#notification-zip").show();
				// $("#zipcode").addClass("error");
			

			if( $("#state-select").parent().find(".float-error").size() > 1 ){
                 $("#state-select").parent().find('.float-error').last().remove();
	           
            }
            setTimeout(function() {
				$('.float-error').remove();
				$(".notification-zip").hide();
				$(".notification-state").hide();
			 $(this).removeClass('error');

			}, 2000);

		}else{	

			$('#searchz').modal('show');
			$(".search-result").show();
			$(".cant-find").hide();
			get_schoollist();
		}
	});

	$(document).on("keyup", "#schoolname", function(e) {
      if( $(this).val() != "" ){
        $(this).removeClass("error");
        $(".float-error").remove();
      }
      return false;
    });
	
	$('.cant-find-btn').on('click', function() {
		$(".error-label").hide();
		$(".luk-required").removeClass('error');
		$(".search-result").hide();
		$(".cant-find").show();
		/* set to null values before loading the input fields*/
		$(".luk-required").val("");
		$(".selected-col-text1").text("");
		$(".selected-col-text2").text("");
		$(".selected-col1").css('color', '#ccc');
		$(".selected-col2").css('color', '#ccc');
	});

});
	



$(document).on("keyup",".luk-required" , function(){
	
	if( $(this).val() != "" ){
		$(this).removeClass("error");
		$(this).parent().find('.error-label').remove();

	}
});

$(document).on("change",".luk-required" , function(){
	
	if( $(this).val() != "" ){
		$(this).removeClass("error");
		$(this).parent().find('.error-label').remove();

	}
});


function validate_luk(){

    $(".luk-required").each(function(i){
        if( $(this).val() == "" ){
             $(this).addClass('error');
             $( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' ).insertAfter( this );

            if( $(this).parent().find(".error-label").size() > 1 ){
                 $(this).parent().find('.error-label').last().remove();
                   
            }   
          setTimeout(function() {
				$(".error-label").hide();
				$(".luk-required").removeClass("error");
			}, 2000);
        }  
    });

    $(".error").eq(0).focus();
}

 