var mainLink = '';

function set_mainLink( value ) {
  mainLink = value;
}

function get_mainLink() {
  return mainLink;
}

function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function ucword(str){
  str = str.toLowerCase().replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g, function(replace_latter) { 
    return replace_latter.toUpperCase();
  });  //Can use also /\b[a-z]/g
  return str;  //First letter capital in each word
}

function isNumberKey(evt) {
  var charCode = (evt.which) ? evt.which : evt.keyCode;

  if ( charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57) ) {
    return false;
  } else {
    return true;
  }
}

$(document).ready(function(){
    $('input').bind('input', function() { 
      $(this).addClass('input-changed');
    });

    $(document).on("keyup", "#cust_bio", function(){
        if( $(this).val() != ""){
          $(this).addClass('input-changed');
        }
    });
    
    $(document).on("keyup", "#tracking_number", function(){
        if( $(this).val() != ""){
          $(this).removeClass("error");
        }
    });

    $(document).on("click", "#tracking_btn", function(){
      var t_number = $("#tracking_number").val().trim();
      if( $("#tracking_number").val() == ""){        
        $('#conModal').modal('show');
        $(".text-confirm").text( 'Please input tracking number.' );
        setTimeout(function() {  $(".verify-close").click(); $("#tracking_number").addClass("error").focus(); }, 3500);
      }
      $.ajax({
            type: 'GET',
            url: get_mainLink()+"account/tracking/"+ $("#tracking_number").val(),
            dataType: "json",
            success: function (response) {
           
            }
        }); 
        
    });

    $(document).on("click", ".resend-verification", function(){
        var ajaxURL = get_mainLink()+'account/resend_verification/';  
        $.post( ajaxURL, function(data){
            $(".for-verification").html('Your Confirmation link Has Been Sent To Your Email Address.');

            setTimeout(function() {  $(".for-verification").hide(); }, 3700);
        });    
        
    });

    $(document).on("keyup", ".handle_enter", function(e) {
        if (event.keyCode == '13') {
            $("#"+$(this).attr("data-enter")).click();
            $("#"+$(this).attr("data-enter")).focus();
        }
        return false;
    });

    $(document).on("click",".force_end_campaign", function(){
        $(".force_end_campaign").removeClass('camp-select');
        $(this).addClass('camp-select');
    });

    $(document).on("click","#cancel_campaign", function(){
        var campaign_id = $(".camp-select").attr('data-id');
        var myamount = $(".camp-select").attr('data-amount');
        $(".loader-container").show();
        update_campaign_status(campaign_id, myamount);
    });

    $(document).on("click",".sort", function(){
        var table = $(this).attr('data-table');
        if( $(this).hasClass("fa-angle-up")){
            window.location.href = get_mainLink()+'account/campaigns/'+table+'/ASC';
        }else{
            window.location.href = get_mainLink()+'account/campaigns/'+table+'/DESC';
        }
    });

    $(document).on("click",".sort-order", function(){
        var table = $(this).attr('data-table');
        if( $(this).hasClass("fa-angle-up")){
            window.location.href = get_mainLink()+'account/orders/'+table+'/ASC';
        }else{
            window.location.href = get_mainLink()+'account/orders/'+table+'/DESC';
        }
    });

    $(document).on("click",".sort-payout", function(){
        var table = $(this).attr('data-table');
        if( $(this).hasClass("fa-angle-up")){
            window.location.href = get_mainLink()+'account/payout/'+table+'/ASC';
        }else{
            window.location.href = get_mainLink()+'account/payout/'+table+'/DESC';
        }
    });

    $(document).on("click", ".logout_fb", function(){
      FB.logout(function(response) {
          // Person is now logged out
      });
    });

    $(document).on("click", "#cust_email", function(){
        // alert('Contact Support to Change');
         $('#conModal').modal('show');
          $(".text-confirm").text( 'Contact Support to Change.' );
          setTimeout(function() {  $(".verify-close").click(); }, 3500);
    });

    $(document).on("keyup", "#cust_zip", function(){
        if( $(this).val().length == 5){
            $(this).removeClass("failed-zip error");
            check_zip();
        }
    });

    $(document).on("blur", "#cust_zip", function(){
        if( $(this).val().length != 5){
            $(this).addClass("failed-zip error");
        }else{
            $(this).removeClass("failed-zip error");
            check_zip();
        }
    });

    $(document).on("blur", ".name", function(){ // firstname, last name or middle name
        var name = $(this).val();
        var re = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
        $(this).val( name.trim() );

        if( $(this).val() != ''){
          if (re.test(name)) {
              $(this).removeClass('name-fail error');
              $(this).parent().find('.error-label').last().remove();
          } else{
            $(this).addClass('name-fail error');
            $( '<label  class="error error-label" >Input a valid Name!</label>' ).insertAfter( this );
            if( $(this).parent().find(".error-label").size() > 1 ){
              $(this).parent().find('.error-label').last().remove();
            }   
          }
        }else{
          $(this).removeClass('name-fail error');
         $(this).parent().find('.error-label').last().remove();
        }
    });

    $(document).on("keyup", ".text-input", function(e) {
      if( $(this).val() != "" ){
        $(this).removeClass("input-failed");
      }
      return false;
    });

    $(document).on("blur", ".text-input", function(e) {
      var input = ucword($(this).val());
      $(this).val( input );
    });

    $(document).on("keyup", ".required", function(){
        if( $(this).val() != "" ){
            $(this).removeClass("error");
            $(this).parent().find('.error-label').remove();
        }
    });

    $(document).on("keyup", ".error-password", function(){
        if( $(this).val() != "" ){
            $(this).removeClass("error");
            $(this).parent().find('.error-label').remove();
        }
    });

    $(document).on("blur", ".error-password", function(){
        if( $(this).val() != "" ){
            $(this).removeClass("error");
            $(this).parent().find('.error-label').remove();
        }
    });

    $(document).on("keyup", "#cust_school", function(e) {
        var schlName = $( this ).val();    
        var checkindex = $(".search_active").attr('data-index');

        if (e.keyCode == 13) { 
           $('.search_active').click();
           $(".search_results").hide();      
        }
        else if (e.keyCode == 38) { 
            var up_index = Number( checkindex ) - 1; 
            if ( up_index >= 0 ) {
                $('.search_item').removeClass('search_selected search_active');     
                $(".search_item:eq("+up_index+")").addClass('search_selected search_active');                 
            }        
        }
        else if (e.keyCode == 40) { 
            var search_num =  Number(  $(".search_item").size() ) - 1;      
            var down_index = Number( checkindex ) + 1;
            $('.search_item').removeClass('search_selected search_active'); 
            $(".search_item:eq("+down_index+")").addClass('search_selected search_active'); 
            
            if( down_index > search_num ){
                $(".search_item:eq("+search_num+")").addClass('search_selected search_active');
                return false;
            }
                
        }else{
            if ( schlName.length > 3 ) {
                checkSchl( schlName );
            }else{
                $('.search_results').hide();
            }
        }
        return false;
      });

    $(document).on("click", ".search_item", function() {
        var schlName = $( this ).text();
        var schlID = $( this ).attr("data-id");
        $("#cust_school").val( schlName );
        $("#school_id").val( schlID );
        $(".search_results").hide();
     });

    $(document).on("click", "#update_profile_btn", function(){
      var firstname = $("#cust_fname").val();
      var lastname = $("#cust_lname").val();
      var contact = $("#cust_contact").val();
      var address = $("#cust_add").val();  
      var city = $("#cust_city").val();  
      var state = $("#cust_state").val();  
      var zip = $("#cust_state").val();  
      var birthdate = $("#cust_birthdate").val();  
      var schlID = $("#school_id").val();  
      var bio = $("#cust_bio").val();  
      var validated = true;
        required();   
        $('#cust_zip').blur();
        if( $(".failed-zip").length != 0 ){
            $(".update-profile-loader").hide();
            validated = false;
            return false;
        }

        if( schlID == "" ){
            $("#school_id").focus();
            $(".update-profile-loader").hide();
            validated = false;
            return false;
        }           

        if( validated == true ){
            if( $(".error").size() == 0 ){
              if( $(".input-changed").size() > 0){
                $(".update-profile-loader").show();
                update_profile();
              }
            }
            
        }

    });

});

 $(document).on("keypress", "#cust_contact", function(e){
  var key = e.charCode || e.keyCode || 0;
  var phone = $(this);
  if (phone.val().length === 0) {
    phone.val(phone.val() + '(');
  }

  if (phone.val().length === 1 && phone.val() !== '(' ) {
    phone.val('(' + phone.val() );
  }
  // Auto-format- do not expose the mask as the user begins to type
  if (key !== 8 && key !== 9) {
    if (phone.val().length === 4) {
      phone.val(phone.val() + ')');
    }
    if (phone.val().length === 5) {
      phone.val(phone.val() + ' ');
    }
    if (phone.val().length === 9) {
      phone.val(phone.val() + '-');
    }
    if (phone.val().length >= 14) {
      phone.val(phone.val().slice(0, 13));
    }
  }

  // Allow numeric (and tab, backspace, delete) keys only
  return (key == 8 ||
    key == 9 ||
    key == 46 ||
    (key >= 48 && key <= 57) ||
    (key >= 96 && key <= 105));
})

$(document).on("focus", "#cust_contact", function(){
  phone = $(this);

  if (phone.val().length === 0) {
    phone.val('(');
  } else {
    var val = phone.val();
    phone.val('').val(val); // Ensure cursor remains at the end
  }
})

$(document).on("blur", "#cust_contact", function(){
  $phone = $(this);

  if ($phone.val() === '(') {
    $phone.val('');
  }

});

function update_campaign_status(campaign_id, myamount){
    var ajaxURL = get_mainLink()+'account/force_end_campaign/'+campaign_id;         
    $.post( ajaxURL, { amount:myamount }, function(data){  
      location.reload();
    });
}

function check_zip(){
    var ajaxURL = get_mainLink()+'account/get_zip/';            
        $.post( ajaxURL, {
            zip_code: $('#cust_zip').val(),
            state: $("#cust_state").val()
          }, function(data){

            if( data == "no-data"){
                $('#cust_zip').addClass("failed-zip error");
                $( '<label  class="error error-label" >Input a Valid Zip Code.</label>' ).insertAfter( '.failed-zip' );
                if( $('#cust_zip').parent().find(".error-label").size() > 1 ){
                    $('#cust_zip').parent().find('.error-label').last().remove();
                }
            }else{
                $('#cust_zip').removeClass("failed-zip error");
                $('#cust_zip').parent().find('.error-label').remove();
            }

        });
}

function update_profile(){
    $(document).on("keydown", disableF5);
    var ajaxURL = get_mainLink()+'account/update_profile/';          
    $.post( ajaxURL, { 
        firstname: $("#cust_fname").val(),
        lastname: $("#cust_lname").val(),
        contact: $("#cust_contact").val(),
        address: $("#cust_add").val(),
        city: $("#cust_city").val(),
        state: $("#cust_state").val(),
        zip: $("#cust_zip").val(),
        birthdate: $("#cust_birthdate").val(),
        school: $("#school_id").val(),
        bio: $("#cust_bio").val()
      }, function(data){
        var result = JSON.parse( data );
        if(result['status'] == 'success'){
          if( $("#loginvia").val() == 'sign-in'){
            checkpassword(result['message']);
          }
        }
    });
}

function checkpassword(msg){
  var oldpass = $("#prev_pword").val();
  var newpass = $("#new_pword").val();
  var conpass = $("#con_pword").val();
  var url = get_mainLink()+'account/change_password';
  var validated = true;
  if ( oldpass != '' && newpass != '' && conpass != '' ){

    if( $("#new_pword").val().length < 6 ){
        $("#new_pword").focus();
        $(".update-profile-loader").hide();
        $("#new_pword").addClass("error-password error");
        $( '<label  class="error error-label" >Password length minimum of 6 characters..</label>' ).insertAfter( "#new_pword" );
        if( $("#new_pword").parent().find(".error-label").size() > 1 ){
            $("#new_pword").parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }

    if( $("#con_pword").val().length < 6 ){
        $("#con_pword").focus();
        $(".update-profile-loader").hide();
        $("#con_pword").addClass("error-password error");
        $( '<label  class="error error-label" >Password length minimum of 6 characters..</label>' ).insertAfter( "#con_pword" );
        if( $("#con_pword").parent().find(".error-label").size() > 1 ){
            $("#con_pword").parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }

    if( validated == true ){
        $.post(
          url,
          {
            oldpass:oldpass,
            newpass:newpass,
            conpass:conpass },function(data){
            var result = JSON.parse(data);   
           
            
            if( result['status'] == 'success' ){
              $('#conModal').modal('show');
              $(".update-profile-loader").hide();
              $("#prev_pword").val("");
              $("#new_pword").val("");
              $("#con_pword").val("");
                $(".text-confirm").text( result['message'] );
                setTimeout(function() {  $(".verify-close").click(); }, 3500);
            }

            if( result['status'] == 'incorrect' ){
              $("#prev_pword").focus();
              $(".update-profile-loader").hide();
              $("#prev_pword").addClass('error error-password');
              $( '<label  class="error error-label" >'+result['message']+'.</label>' ).insertAfter( "#prev_pword" );
              if( $("#prev_pword").parent().find(".error-label").size() > 1 ){
                  $("#prev_pword").parent().find('.error-label').last().remove();
              }
            }
            
            if( result['status'] == 'notsame' ){
              $("#new_pword").focus();
              $(".update-profile-loader").hide();
              $("#new_pword").addClass('error error-password');
              $("#con_pword").addClass('error error-password');
              $( '<label  class="error error-label" >'+result['message']+'.</label>' ).insertAfter( "#con_pword" );
              if( $("#con_pword").parent().find(".error-label").size() > 1 ){
                  $("#con_pword").parent().find('.error-label').last().remove();
              }
              
            }

        });
    }
    
  }else{
    
    
    if(  oldpass == '' && newpass != '' && conpass != ''){
      $("#prev_pword").focus();
      $(".update-profile-loader").hide();
      $("#prev_pword").addClass('error error-password');
      $( '<label  class="error error-label" >This field is required.</label>' ).insertAfter( "#prev_pword" );
      if( $("#prev_pword").parent().find(".error-label").size() > 1 ){
          $("#prev_pword").parent().find('.error-label').last().remove();
      }
    }else if(  oldpass != '' && newpass == '' && conpass != ''){
      $("#new_pword").focus();
      $(".update-profile-loader").hide();
      $("#new_pword").addClass('error error-password');
      $( '<label  class="error error-label" >This field is required.</label>' ).insertAfter( "#new_pword" );
      if( $("#new_pword").parent().find(".error-label").size() > 1 ){
          $("#new_pword").parent().find('.error-label').last().remove();
      }
    }else if( oldpass != '' && newpass != '' && conpass == ''){
      $("#con_pword").focus();
      $(".update-profile-loader").hide();
      $("#con_pword").addClass('error error-password');
      $( '<label  class="error error-label" >This field is required.</label>' ).insertAfter( "#con_pword" );
      if( $("#con_pword").parent().find(".error-label").size() > 1 ){
          $("#con_pword").parent().find('.error-label').last().remove();
      }
    }else{
      $('#conModal').modal('show');
      $(".update-profile-loader").hide();
      $(".text-confirm").text( msg );
      setTimeout(function() {  $(".verify-close").click(); }, 3500);
    }
  }
}

function checkSchl( textValue ) {
    var xhr;
    var schUrl = get_mainLink()+'account/search_school/';
    schUrl += textValue;

    if(xhr && xhr.readystate != 4){
      xhr.abort();
    }

    // lets submit the form
    xhr = $.ajax({
      type: "POST",
      url: schUrl
    }).done(function(data) {
      var data = JSON.parse(data);
      var htmlContent = "";
      var cnum = 0;
      $(".search_results").show();
      if ( data['school'].length > 0 ) {
            for ( var i = 0; i < data["school"].length; i++ ) {
                htmlContent += '<div data-index="'+cnum+'" data-id="'+data["school"][i].school_id+'" class="search_item">'+data["school"][i].school_name+'</div>';
                cnum++;
            }
      }
      $(".search_results").html( htmlContent );
      var index = $(".search_item").attr("data-index");
      if (index == 0){
        $(".search_item:eq("+index+")").addClass("search_selected search_active");
      }

      if( $(".search_item").size() > 7){
        $('.search_results').addClass('search-results-overflow');
      }else{
        $('.search_results').removeClass('search-results-overflow');
      }
      

    }).fail(function() {
      $( "#campaign_url").css({"border-color":"#FF3300"});
    });
}

function required(){
    $(".required").each(function(i){
        if( $(this).val() == "" ){
            $(this).addClass('error');
            $( '<label  class="error error-label" >This field is required.</label>' ).insertAfter( this );
            if( $(this).parent().find(".error-label").size() > 1 ){
                $(this).parent().find('.error-label').last().remove();
            }
            
        }           
        
    });
    $(".error").eq(0).focus();  
}