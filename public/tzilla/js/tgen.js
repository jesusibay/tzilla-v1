/*
* @desc opens a modal window to display a message
* @param string $msg - the message to be displayed
* @return bool - success or failure
* @author Mike Ibay mike.ibay@tzilla.com
* @required settings.php
*/

var xhrVariableName; // variables to be used for ajax requests
var xhrArtworks; // variables to be used for ajax requests to get all the artworks
var limit = 5;
var offset = 0;
var searchType = 0; // 0 = Default Search, 1 = Search by School, 2 = Search by Mascot, 3 = Search by Most Popular, 3 = Search by Newest
var intVariableName = 0;
var strVariableName = ""; // define possible string values here
var mainUrl = ""; // contains the base url
var keyword = "";
var artworksList = ""; // contains html to diplay the artworks for selection
var boolVariableName = true; // true or false

function setMainUrl( value )
{
	mainUrl = value;
}

function getMainUrl()
{
	return mainUrl;
}

function setSearchLimit( value )
{
	limit = value;
}

function getSearchLimit()
{
	return limit;
}

function setOffset( value )
{
	offset = value;
}

function getOffset()
{
	return offset;
}

function setSearchType( value )
{
	searchType = value;
}

function getSearchType()
{
	return searchType;
}

function setKeyword( value )
{
	keyword = value;
}

function getKeyword()
{
	return keyword;
}

function searchArtworks()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/search',
		data: {
			limit : getSearchLimit(),
			offset : getOffset(),
			searchType : getSearchType(),
			keyword : getKeyword()
		},
		success: function( data ){
			console.log( data );
		},
		async: false
	});
}

function getArtworkCategories()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/categories',
		success: function( data ){
			var nData = JSON.parse( data );

			displayCategories( nData );
		},
		async: false
	});
}

function displayCategories( data )
{
	if ( data.length > 0 )
	{
		$(".display-result-here").html( "" );

		for ( var i = 0; i < data.length; i++ )
		{
			if ( data[i]['parent_id'] == 0 )
			{
				// display the parent categories
				$(".display-result-here").append( $('<ul />').html( data[i]['name'] ).attr({"id":data[i]['id']}) );
			}
			else
			{
				// display the child categories
				$('.display-result-here ul[id="'+data[i]['parent_id']+'"]').append( $('<li />').html( data[i]['name'] ).attr({"id":data[i]['id']}) );
			}
		}
	}
}

$(document).ready(function(){

	$(document).on("@event", "@element", function(){

	});

	$(document).on("click", ".clear-result", function(){
		$(".display-result-here").html( "" );
	});

	$(document).on("click", ".get-categories", function(){
		getArtworkCategories();
	});

	$(document).on("click", ".search-school", function(){
		setKeyword("letter");
		setSearchType(1);
		searchArtworks();
	});

	$(document).on("click", ".search-mascot", function(){
		setKeyword("base");
		setSearchType(2);
		searchArtworks();
	});

	$(document).on("click", ".search-popular", function(){
		setSearchType(3);
		searchArtworks();
	});

	$(document).on("click", ".search-newest", function(){
		setSearchType(4);
		searchArtworks();
	});

	$(document).on("click", ".get-artworks", function(){

		if ( xhrArtworks && xhrArtworks.readystate != 4 )
		{
			xhrArtworks.abort();
		}

		xhrArtworks = $.ajax({
			type: 'POST',
			url: getMainUrl()+'template/view_artTemplate',
			success: function( data ){
				var nData = JSON.parse( data );
				var encodedImage = "";

				if ( nData.length > 0 )
				{
					artworksList = "";

					for ( var i = 0; i < nData.length; i++ ) {
						artworksList += '<img src="'+nData[i]['thumbnail']+'" width="200px" height="200px" />';
					}
				}

				$(".display-result-here").html( artworksList );
			},
			async: false
		});

	});

});