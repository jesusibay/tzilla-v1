var xhr_discount;
var xhr_validate_zip;
var shipping_fee = '';
var shipping_method = '';
var usValidate = '';
var totalWeight = 0;

function setUsValidate(value){
	usValidate = value ;
}
function getUsValidate(){
	return usValidate;
}

function setShippingMethod(value){
	shipping_method = value;
}

function getShippingMethod(){
	return shipping_method  ;
}

function setShippingFee(value){
	shipping_fee = value;
}

function getShippingFee(){
	return shipping_fee;
}

function setWeight( value ) {
	totalWeight = value;
}

function getWeight() {
	return totalWeight;
}

function ch_gray_btn(){
    $(".add-shipping-details").attr("disabled", "disabled");
    $(".add-shipping-details").removeClass("green-btn");
    $(".add-shipping-details").addClass("gray-btn");
}

function ch_green_btn(){
    $(".add-shipping-details").removeAttr("disabled");
    $(".add-shipping-details").removeClass("gray-btn");
    $(".add-shipping-details").addClass("green-btn");
}

function validateNumber(event) { //validate numbers only
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
          return true;
        } else if ( key < 48 || key > 57 ) {
         return false;
        } else {
         return true;
        }
     }


function check_fields(){
	$(".checkout-input-req").each(function(){
		if( $(this).val() == ''){
			 $(this).addClass('error');
			 $( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' ).insertAfter( this );
	         if( $(this).parent().find(".error-label").size() > 1 ){
	             $(this).parent().find('.error-label').last().remove();
	        } 
		}
	});

	$(".error").eq(0).focus();
}

function save_customer_address(){
    var ajaxURL = get_mainLink()+'checkout/set_shipping_session/';
    var email = $(".user-email").text();
    var address_type = 'Shipping';
    var firstname = $("#firstname").val();
    var lastname = $("#lastname").val();
    var add1 = $("#add1").val();
    var add2 = '';
    var contact = $("#contact_no").val();
    var city = $("#city").val();
    var state = $("#state").val();
    var zip = $("#zip").val();
    var customer_id = $("#customer_id").val();
    var school_id = $("#school_id").val();
    var shipping_method = getShippingMethod();
    var shipping_fee =$(".shipping-fee").text().replace("$", "") ;
    var subtotal =  $(".subtotal").text().replace("$", "");
    var tax_fee =  $(".tax-fee").text().replace("$", "") ;
    var totalPrice = $(".total").text();
 	var update_info = 0;
 	if( $(".save-for-next").parent().find('.checker-box').hasClass('selected') ){
 		update_info = 1; 
 	}else{
 		update_info = 0;
 	}
 	
    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
           	customer_id: customer_id,            
           	address_type: address_type,            
            contact_no: contact,
            email: email,
            firstname: firstname,
            lastname: lastname,
            address_1: add1,
            address_2: add2,
            city: city,
            state: state,
            zip: zip, 
            shipping_method : shipping_method,
            shipping_fee : shipping_fee,
            subtotal : subtotal,
            tax_fee : tax_fee,
            totalPrice : totalPrice,
            update_info : update_info
      },success: function( data ){
        var result = JSON.parse( data );
        if( result['status'] == 'success'){
        	
          	window.location.href = get_mainLink()+'checkout/payment';	  
        }

      },                 
      async:false
    });
}


function save_sales_orders(){ //save details for sales orders
    var ajaxURL = get_mainLink()+'checkout/save_sales_orders/';

	var subtotal = $('.subtotal').text();
	var shipping_fee = $('.shipping-fee').text();
	var tax_fee = $('.tax-fee').text();
	var total = $('.total').text();
	

	$.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
           	subtotal: subtotal,            
           	shipping_fee: shipping_fee,            
            tax_fee: tax_fee,
            total: total 
      },success: function( data ){
        var result = JSON.parse( data );
       
        if( result['status'] == 'success'){ 
            
        }

      },                 
      async:false
    });

}

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function setAllWithComma(){

	var subtotal = Number ($(".subtotal").html().replace("$", "") );
	// var tax_fee = Number ( $(".tax-fee").text().replace("$", "") );
	var sub = numberWithCommas( subtotal );
	var tax_fee = $(".tax-fee").text().replace("$", "" );
	var tax = numberWithCommas( tax_fee );
 
	var shipping_fee = Number ($(".shipping-fee").html().replace("$", "") );

	
}

function compute_for_total(){
	
	if( $(".tax-fee").text() == "$ 0.00" ){

    ch_gray_btn();
    } else{
    	ch_green_btn();
    }

	var total = 0;
    $('.computeall').each(function(){
    	var all = $(this).text().replace("$", "" );
    	var allVal = all.replace(",", "");
        total += parseFloat(  allVal );
    });
    
    var total2 = numberWithCommas(total.toFixed(2) );
     $(".total").text( total2 );


    
}

function get_shipping_method_zip(){
	var zipcode = $('.zipcode').val();
	var totalQty = $('.totalQty').attr('data-total-items');
	var state = $("#state").val();
	var email = $(".user-email").text();
    var address_type = 'Shipping';
    var firstname = $("#firstname").val();
    var lastname = $("#lastname").val();
    var add1 = $("#add1").val();
    var add2 = '';
    var contact = $("#contact_no").val();
    var city = $("#city").val();   
    
    var customer_id = $("#customer_id").val();
    var school_id = $("#school_id").val();
    var shipping_method = getShippingMethod();
    var shipping_fee = getShippingFee();

	$.ajax({
		type: "POST",
		url: get_mainLink()+"checkout/shipping/get_shipping_method_zip",
		data : {
			customer_id: customer_id,            
           	address_type: address_type,            
            contact_no: contact,
            email: email,
            cust_firstname: firstname,
            cust_lastname: lastname,
            cust_address_1: add1,
            cust_address_2: add2,
            cust_city: city,
            cust_state: state,
            cust_zip: zipcode, 
            shipping_method : shipping_method,
            shipping_fee : shipping_fee,
			totalQty : totalQty,
			totalWght : getWeight()
		},success: function(ndata){
			var data = JSON.parse(ndata);
			
			

			if(data['status'] == "success" ){
				$('.shipping-method-cont').html(data['shippingHtml']);				
				$('.validation_passed_status').val('true');
				setShippingMethod(data['shipping_method']);
				$(".shipping-fee").text("$"+ data['fedex_fee']);
				setShippingFee(data['fedex_fee']);
				
				compute_for_total();
				
			}else {
				ch_gray_btn();
			}
		}
	});
}

function get_tax_amount(){
	var ajaxurl = get_mainLink()+'checkout/Shipping/get_tax';
	var subtotal = Number ($(".subtotal").html().replace("$", "") );
	$.ajax({ type: "POST",
			url : ajaxurl,
			data:{
				state: $("#state").val(),
				subtotal: subtotal
		 	},success: function(ndata){
			var data = JSON.parse(ndata);
			var taxAMt = Number( data['tax_amount'].replace(",", "") );

			$(".tax-fee").text("$"+ taxAMt );
			
			compute_for_total();
		  
			$(".ship-nxt-loader").hide();

			var subtotal = Number ($(".subtotal").html().replace("$", "") );
			
			var sub = numberWithCommas( subtotal.toFixed(2) );
			var tax_fee = $(".tax-fee").text().replace("$", "" );
			var tax = numberWithCommas( tax_fee);
		 
			var shipping_fee = Number ($(".shipping-fee").html().replace("$", "") );

			$(".subtotal").text( "$"+ sub );
			$(".tax-fee").text( "$"+ tax );
		
			},
	});										
}

function check_statezip_validity(state, zip){
	$(".error-label").remove();

	if(xhr_validate_zip && xhr_validate_zip.readystate != 4){
		xhr_validate_zip.abort();
	}

	
	xhr_validate_zip = $.ajax({
		type: "POST",
		url: getUsValidate(),
        data: {
          	state: state,
          	zip_code: zip
        },
		dataType:'json'
		}).done(function( data ) 
		{
			if( data == true){ 
				get_shipping_method_zip();
				get_tax_amount();
				$('#notification').hide();
				$(".zipcode").removeClass('error-zip error');
				if( $('.shipping-radio-cont').size() > 1 ){
					$('.shipping-radio-cont').last().removeClass("payment-radio-cont").addClass("payment-radio-cont2");
					
				}
				

				// get_tax_amount();
				//get tax amount
			}else{
				setShippingMethod('invalid zipcode');
				$('#validation_passed_status').val("false");
				ch_gray_btn();
				$(".shipping-method-cont .shipping-radio-cont").find('.radio-title').text( "Validating Shipping method .. . . " );
				$(".shipping-method-cont .shipping-radio-cont").find('.radio-val').text( "" );
				$(".shipping-method-cont .select-shipping").attr('data-shipping-method', "");
				$(".shipping-method-cont .select-shipping").val("");

				$(".zipcode").addClass('error-zip error');				
				$('<label  class="error error-label" >Invalid Zipcode.</label>' ).insertAfter( '.zipcode' );
		        if( $('.zipcode').parent().find(".error-label").size() > 1 ){
		            $('.zipcode').parent().find('.error-label').last().remove();
		        } 
			}
		});

}

$(window).load(function() {
	
		$(".shipping-fee").text( $(".shipping-method-fee:first").text() );		

		if( getShippingMethod() == "invalid zipcode" ){
			
			ch_gray_btn();
			$(".zipcode").removeClass('error-zip error');
			// $(".zipcode").addClass('error-zip error');
			$('<label  class="error error-label" >Invalid Zipcode.</label>' ).insertAfter( '.zipcode' );
	        if( $('.zipcode').parent().find(".error-label").size() > 1 ){
	            $('.zipcode').parent().find('.error-label').last().remove();
	        }
	   	}else{
	   		
	   		check_statezip_validity( $("#state").val(), $(".zipcode").val() );
	   		compute_for_total();
	   		
	   	}
	   	$('.error-label').html("");
	   	$("#username").removeClass('email-fail error');
        $("#username").parent().find('.log-error-label').last().remove();

});

$(document).ready(function(){
	// TODO
	// this is for the application of discount on checkout
	$(document).on("click", ".apply-discount", function(){
		var discCode = $("#discount_detail").val();

		if(xhr_discount && xhr_discount.readystate != 4){
			xhr_discount.abort();
		}

		xhr_discount = $.ajax({
			type: 'POST',
			url: get_mainLink()+'discount/load_details/'+discCode,
			success: function(data){
				var nData = JSON.parse(data);
				if ( nData["status"] == "error" ) {
					alert(nData["details"]);
				} else {
					
					compute_for_total();
				}
			},
			async:true
		});
	});

	$(document).on('click','.checker-box', function () {   		
	   	$(this).toggleClass("selected");
	   	$("#storetitle").text('TZilla.com - Shipping Details');
	});	
	
	$(document).on("click", ".select-shipping", function() {
		setShippingMethod( $(this).attr("data-shipping-method") );

		var ship = numberWithCommas($(this).val());

		$(".shipping-fee").text("$"+ ship );
		
		var sub = $(".subtotal").text().replace(",", "");
		var tax =  $(".tax-fee").text().replace(",","");

		var subtotal = Number ( sub.replace("$", "") );
		var tax_fee = Number ( tax.replace("$", "") );
		var shipping_fee = Number ( $(".shipping-fee").html().replace("$", "") );
		

     	compute_for_total();
     	
		
	});
	 if( getShippingMethod() == "invalid zipcode" ){
		
		$(".shipping-radio-cont ").find('.shipping-method').text( "Validating Shipping method .. . . " );
		
	 }
	
	$(document).on("keyup", "#zip", function() {
		var zipcode = $(this).val();
		var state = $("#state").val();
		if( zipcode.length == 5 ){
			check_statezip_validity( state, zipcode); 
		}
	});

	$(document).on("blur", "#zip", function() {
		var zipcode = $(this).val();
		var state = $("#state").val();
		if( $(this).val() != ''){			
			check_statezip_validity( state, zipcode);
		}	
	});

	$(document).on("keyup", ".checkout-input-req", function() {
		if( $(this).val() != "" ){
	        $(this).removeClass("error");
	        $(this).parent().find('.error-label').remove();
	         
	        ch_green_btn();
   		}else{
   			 
   			ch_gray_btn();
   		}
	});
	
	
	$(document).on("click", ".add-shipping-details", function(){	
		check_fields();
		if( $("error-zip").size() == 0 ){
			if( $(".error").size() == 0 ){
				save_customer_address();
				$(".ship-nxt-loader").show();
			}else{
				
			}
		}
	});

	$(document).on("change", "#state", function(){
		if( $("#zip").val() != '' &&  $("#zip").val().length == 5 ){
			$('#notification').hide();
			check_statezip_validity( $(this).val(), $("#zip").val() ); 
		}else if( Number($('.tax-fee').text().replace("$", "") ) == 0 ){
			check_statezip_validity( $(this).val(), $("#zip").val() ); 
			
		}else{
			var ajaxurl = get_mainLink()+'checkout/Shipping/get_tax';		
			
			var subtotal = Number ($(".subtotal").html().replace("$", "") );
			
			$.ajax({ type: "POST",
					url : ajaxurl,
					data:{
						state: $("#state").val(),
						subtotal: subtotal
				 	},success: function(ndata){
					var data = JSON.parse(ndata);
					
					$(".tax-fee").text("$"+ data['tax_amount']);
				
					},
			});
			
		}
	});

	$(document).on("click", ".goback-cart", function(){
		window.location.href = get_mainLink()+'cart/review';	  
	});

	

}); // end of .ready