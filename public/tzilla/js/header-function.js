/*
* @desc opens a modal window to display a message
* @param string $msg - the message to be displayed
* @return bool - success or failure
* @author Mike Ibay mike.ibay@tzilla.com
* @required settings.php
*/

var xhrVariableName; // variables to be used for ajax requests
var xhrArtworks; // variables to be used for ajax requests to get all the artworks

var username = "";
var password = "";
var firstname = "";
var lastname = "";

function setMainUrl( value )
{
	mainUrl = value;
}

function getMainUrl()
{
	return mainUrl;
}

function setUsername(value)
{
	username = value;
}

function getUsername()
{
	return username;
}

function setPassword(value)
{
	password = value;
}

function getPassword()
{
	return password;
}

function setFirstname(value)
{
	firstname = value;
}

function getFirstname()
{
	return firstname;
}

function setLastname(value)
{
	lastname = value;
}

function getLastname()
{
	return lastname;
}

function getSearchLimit()
{
	return limit;
}


function login_proc()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'signin/login',
		data: {
			username : getUsername(),
			password : getPassword()
		},
		success: function( data ){
			var result = JSON.parse(data);
			var htmlentity= "";
			for(var key in result)
			{
				htmlentity = "<div><h3>"+ result['status']+"</h3><p>"+result['message']+"</p></div>";
			}
			$(".display-result-here").html(htmlentity);
		},
		async: false
	});
}

function registration_proc()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'signin/sign_up',
		data: {
			username : getUsername(),
			password : getPassword(),
			firstname : getFirstname(),
			lastname : getLastname(),
		    recaptcha: grecaptcha.getResponse(gregister)
		},
		success: function( data ){
			var result = JSON.parse(data);
			var htmlentity= "";
			for(var key in result)
			{
				htmlentity = "<div><h3>"+ result['status']+"</h3><p>"+result['message']+"</p></div>";
			}
			$(".display-result-here").html(htmlentity);
		},
		async: false
	});
}

function subscribe_proc()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'signin/subscribe',
		data: {
			email : getUsername(),
		
		},
		success: function( data ){
			var result = JSON.parse(data);
			var htmlentity= "";
			for(var key in result)
			{
				htmlentity = "<div><h3>"+ result['status']+"</h3><p>"+result['message']+"</p></div>";
			}
			$(".display-result-here").html(htmlentity);
		},
		async: false
	});
}

function loginFormGenerate()
{
	$("#register_element").css("display","none");
	$(".display-result-here").html("<input type='text' class='username' value='' placeholder='Username' /> \n <input type='password' class='password' value='' placeholder='Password' /> \n <button type='button' class='submitForm'>Submit</button> ");
}

function registrationFormGenerate()
{
	$("#register_element").css("display","block");
	$(".display-result-here").html("<input type='text' class='firstname' value='' placeholder='First Name' /> \n <input type='text' class='lastname' value='' placeholder='Last Name' /> \n <input type='text' class='username' value='' placeholder='Email' /> \n <input type='password' class='password' value='' placeholder='Password' /> \n <input type='password' class='confirmpassword' value='' placeholder='Confirm Password' /> \n <button type='button' class='submitRegForm'>Submit</button> ");
}

function subscribeFormGenerate()
{
	$("#register_element").css("display","none");
	$(".display-result-here").html("<input type='text' class='username' value='' placeholder='Email' /> \n <button type='button' class='submitEmailForm'>Submit</button> ");
}

$(document).ready(function(){

	$(document).on("@event", "@element", function(){

	});

	$(document).on("click", ".subscribeNewsletter", function(){
		$(".display-result-here").html( "" );
		subscribeFormGenerate();
	});
	$(document).on("click", ".loginForm", function(){
		$(".display-result-here").html( "" );
		loginFormGenerate();
	});
	$(document).on("click", ".registerAccount", function(){
		$(".display-result-here").html( "" );
		registrationFormGenerate();
		grecaptcha.reset();
	});

	$(document).on("click", ".submitForm", function(){
		var username = $(".username").val();
		var password = $(".password").val();
		setUsername(username);
		setPassword(password);
		login_proc();
	});
	$(document).on("click", ".submitEmailForm", function(){
		var username = $(".username").val();
	
		setUsername(username);

		subscribe_proc();
	});
	$(document).on("click", ".submitRegForm", function(){
		var username = $(".username").val();
		var password = $(".password").val();
		var confirmpassword = $(".confirmpassword").val();
		var firstname = $(".firstname").val();
		var lastname = $(".lastname").val();
		setUsername(username);
		setPassword(password);
		setFirstname(firstname);
		setLastname(lastname);
		if(password == confirmpassword)
		{
			registration_proc();	
		}
		else
		{
			$(".display-result-here").html("Password Mismatch");
		}
	});

});