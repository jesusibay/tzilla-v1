var xhr;
var xhr_colors;
var xhr_create;
var xhr_shirts;
var xhr_design;
var xhr_art_pos;
var xhr_variation;
var xhr_art_kerning;
var modal = 0;
var prodID = 0;
var reload = 1;
var schl_id = 0;
var parentID = 0;
var noOfExec = 0;
var campaignId = 0;
var customerid = "";
var templateID = 0;
var items_count = 0;
var defaultTemplate = 0;
var campaignprice = 0;
var AddOnType = "ICON";
var default_primary = "72431B";
var default_secondary = "d2b887";
var mainUrl = "";
var pageurl = "";
var designId = "";
var tgenMode = "";
var assetUrl = "";
var datatitle = "";
var imagelink = "";
var fire72dpi = "";
var schoolName = "";
var designsArr = "";
var cartitemsArr = "";
var artworkIcon = "";
var templateName = "";
var primaryColor = "";
var iconCategory = "";
var secondaryColor = "";
var imageThumbnail = "";
var artworkPattern = "";
var artworkDistress = "";
var productSettings = "";
var customProperties = "";
var customizationMode = "";
var templateProperties = "";
var imgurllink = "";
var designOpts = { template: {} };
var prodArr = [];
var kern_arr = [];
var designLists = [];
var shirt_style_colors = [];
var selected_style_array = [];
var selected_artwork_style_array = [];
var hasKern = false;


function setHasKerning( value ) {
	hasKern = value;
}

function getHasKerning() {
	return hasKern;
}

function setimgURL(value)
{
	imgurllink = value;
}

function getimgURL()
{
	return imgurllink;
}

function setAvailableColorsPerStyle( templateId, data ) {
	var newArr = [ templateId, data ];

	shirt_style_colors.push( newArr );
}

function getAvailableColorsPerStyle() {
	return shirt_style_colors;
}

function setCustomizationMode( value ) {
	customizationMode = value;
}

function getCustomizationMode() {
	var str = "";
	if( getMode() == 'CAMPAIGN'){
		str = 'edit_campaign/'+getCampaign();
	}else{
		if ( customizationMode.toLowerCase() == 'buy' ) { // condition for buying items directly from tgen
			str = 'buy_items'; // template controller function for buying tgen items
		} else if(customizationMode.toLowerCase() == 'create') { // create campaign parameter settings
			str = 'create_campaign'; // template controller function for buying tgen items
		}else{
			str = 'header-login';
		}
	}

	return str;
}

function setKerning( value ) {
    kern_arr = value;
}

function getKerning() {
    return kern_arr;
}

function setProductSettings( value ) {
    productSettings = value;
}

function getProductSettings() {
    return productSettings;
}

function setIconCategory( value ) {
    iconCategory = value;
}

function getIconCategory() {
    return iconCategory;
}

function setPattern( value ) {
	artworkPattern = value;
}

function getPattern() {
	return artworkPattern;
}

function setDistress( value ) {
	artworkDistress = value;
}

function getDistress() {
	return artworkDistress;
}

function setPrimaryColor( value ) {
	primaryColor = value;
}

function getPrimaryColor() {
	return primaryColor;
}

function setSecondaryColor( value ) {
	secondaryColor = value;
}

function getSecondaryColor() {
	return secondaryColor;
}

function setAddOnType( value ) {
	AddOnType = value;
}

function getAddOnType() {
	return AddOnType;
}

function setArtworkIcon( value ) {
	artworkIcon = value;
}

function getArtworkIcon() {
	return artworkIcon;
}

function setAssetUrl( value ) {
	assetUrl = value;
}

function getAssetUrl() {
	return assetUrl;
}

function setMainUrl( value ) {
	mainUrl = value;
}

function getMainUrl() {
	return mainUrl;
}

function setGoback( value ) {
	pageurl = value;
}

function getGoback() {
	return pageurl;
}

function setPreloadDesigns( value ) {
	designsArr = value;
}

function getPreloadDesigns() {
	return designsArr;
}

function setSchoolId( value ) {
	schl_id = value;
}

function getSchoolId() {
	return schl_id;
}

function setSchoolName( value ) {
	schoolName = value;
}

function getSchoolName() {
	return schoolName;
}

function setItemCount( value ) {
	items_count = value;
}

function getItemCount() {
	return items_count;
}

function setCampaignprice( value ) {
	campaignprice = value;
}

function getCampaignprice() {
	return campaignprice;
}

function setActiveProduct( value ) {
	prodID = value;
}

function getActiveProduct() {
	return prodID;
}

function setTemplateProperties( value ) {
	templateProperties = value;
}

function getTemplateProperties() {
	return templateProperties;
}

function setCustomizations( value ) {
	customProperties = value;
}

function getCustomizations() {
	return customProperties;
}

function setActiveArtwork( value ) {
	templateID = value;
}

function getActiveArtwork() {
	return templateID;
}

function setDesignId( value ) {
	designId = value;
}

function getDesignId() {
	return designId;
}

function setTemplateThumb( value ) {
	imageThumbnail = value;
}

function getTemplateThumb() {
	return imageThumbnail;
}

function setTemplateImg( value ) {
	imagelink = value;
}

function getTemplateImg() {
	return imagelink;
}

function setTemplateName( value ) {
	templateName = value;
}

function getTemplateName() {
	return templateName;
}

function setMode( value ) {
	tgenMode = value;
}

function getMode() {
	return tgenMode;
}

function setCampaign( value ) {
	campaignId = value;
}

function getCampaign() {
	return campaignId;
}

function setCustomer( value ) {
	customerid = value;
}

function getCustomer() {
	return customerid;
}

function setCartItems( value ) {
	cartitemsArr = value;
}

function getCartItems() {
	return cartitemsArr;
}

function setDefaultArtwork( value ) {
	defaultTemplate = value;
}

function getDefaultArtwork() {
	return defaultTemplate;
}


function convertionFirstload(xdesign = 0, ydesign = 0, wsize = 0, hsize = 0, percentage = 0, palleteDistance = 0) {
	
	//Width and Height multiply to percentage to get inches
	wsize = Number(percentage/100)*wsize;
	hsize = Number(percentage/100)*hsize;
	
	var compute = 0;
	
	if (window.matchMedia( "(max-width: 1520px)" ).matches) {
		compute = 12.67;
	}else {
		compute = 15.67;
	}
	if (window.matchMedia( "(max-width: 1366px)" ).matches) {
		compute = 12.67;
	}
	if (window.matchMedia( "(max-width: 1280px)" ).matches) {
		compute = 13.67;
	}
	if (window.matchMedia( "(max-width: 1024px)" ).matches) {
		compute = 15.67;
	}
	if (window.matchMedia( "(max-width: 768px)" ).matches) {
		compute = 20.67;
	}
	if (window.matchMedia( "(max-width: 640px)" ).matches) {
		compute = 16.67;
	}
	if (window.matchMedia( "(max-width: 480px)" ).matches) {
		compute = 11.67;
	}
	
	//formula for inches to px
	wsize = Number(wsize*compute);//10.67 18.493 is contant value to convert inches to px
	hsize = Number(hsize*compute);//10.67 is contant value to convert inches to px
	var a = $(".print-canvas").width();
	var b = $(".print-canvas").height();
	//converting px to %
	var c = Number((wsize/a)*100);
	var d = Number((hsize/b)*100);
	
	//x/y positioning formula for converting mm to px
	var xpos = Number(xdesign*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	xpos = (Number(10.97/100)*xpos);
	xpos = Number((xpos/a)*100);
	
	var ypos = Number(ydesign*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	ypos = (Number(10.97/100)*ypos);
	ypos = Number((ypos/b)*100);
	
	var pd = Number(palleteDistance*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	pd = (Number(10.97/100)*pd);
	pd = Number((pd/b)*100);

	
	if(c > 100) {
		c = 100;
	}else if(d > 100) {
		d = 100;
	}
		if(xpos < 0 ) {
			xpos = 0;
			palleteYaxis(pd.toFixed(2));
			$(".art-holder").css({"width":c.toFixed(2)+"%"});
			$(".art-holder").css({"height":d.toFixed(2)+"%"}); 
			$(".art-holder").css({"top":ypos.toFixed(2)+"%"});
			$(".art-holder").css({"left":xpos.toFixed(2)+"%"});
			$(".art-holder").css({"right":"0%"});
		}else {
			palleteYaxis(pd.toFixed(2));
			$(".art-holder").css({"width":c.toFixed(2)+"%"});
			$(".art-holder").css({"height":d.toFixed(2)+"%"}); 
			$(".art-holder").css({"top":ypos.toFixed(2)+"%"});
			$(".art-holder").css({"left":xpos.toFixed(2)+"%"});
		}
		
		$(".tgen-loader-overlay").hide();
}

function loadDesignsFromCampaign() {
	var nData = JSON.parse( getPreloadDesigns() );
	var srcthumb = "";
	var camp_image = "";
	var customing = "";

	if ( nData.length > 0 ) {
		for (var i = 0; i < nData.length; i++) {
			srcthumb = nData[i]['thumbnail'];
			camp_image = nData[i]['template_image'];
			if(getMode() != "SUPPORT") customing = nData[i]['parent_id']+'crt_trimming'+nData[i]['customization'];
			$('.custom-slider').slick('slickAdd','<div class="slide selected-design-item" data-hex="'+nData[i]['hex_value']+'"><img data-parent="'+nData[i]['parent_id']+'" data-id="'+nData[i]['template_id']+'" product-id="'+nData[i]['product_id']+'" data-design="'+nData[i]['design_id']+'" src="'+camp_image+'" data-property='+"'"+JSON.stringify(nData[i]['properties'])+"'"+' data-title="'+nData[i]['display_name']+'" data-fields="'+nData[i]['customization']+'" class="center-block img-responsive design-thumbnail">'+( getMode() != "SUPPORT" ? '<span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span>' : '')+'</div>');

			if ( nData[i]['shirt_style'].length > 0 ) {
				for ( var j = 0; j < nData[i]['shirt_style'].length; j++ ) {
					setActiveStyle(nData[i]['shirt_style'][j]["id"]);
					$('.style-selection[data-id="'+nData[i]['shirt_style'][j]["id"]+'"]').addClass("addmorestyle-shirt-holder-active");
				}

				if ( getMode() == "SUPPORT" ) $(".add-more-styles-button").click();
			}

			if(getMode() != "SUPPORT"){
				$(".selected-design-item").each(function(){
					var active_custom = customing.split("crt_trimming");
					if( $(this).find(".design-thumbnail").attr("data-parent") == active_custom[0] ){
						$(this).find(".design-thumbnail").attr("data-fields", active_custom[1] );
					}
				});	
			}

		}	



		setTimeout(function(){
			if ( getDefaultArtwork() > 0 ) {
				$('.selected-design-item .design-thumbnail[data-parent="'+getDefaultArtwork()+'"]').parent().click();
				setDesignId( $(".custom-slider").find('.selected-design-item .design-thumbnail[data-parent="'+getDefaultArtwork()+'"]').attr("data-design") );
			} else {
				$(".custom-slider").find(".selected-design-item").eq(0).click();
				setDesignId( $(".custom-slider").find('.selected-design-item .design-thumbnail').eq(0).attr("data-design") );
			}
			
			$(".design-zoom-image").attr( "src", $(".custom-slider").find(".selected-design-item").eq(0).find(".design-thumbnail").attr("src") );

			// set background color for each design items in slider
			$(".selected-design-item").each(function(){
				$(this).css( "background-color", "#"+$(this).attr("data-hex") );
			});
			
			if($('.custom-slider .slide').length < 6) {
				$(".custom-inner-hold .add-more-slide").removeClass("stop-adding");
				
			}else {
				$(".custom-inner-hold .add-more-slide").addClass("stop-adding");
			}
			
			
			if ( getMode() == "SUPPORT" ) $(".add-more-styles-button").click();
		}, 2000);
	}

}

function loadDesignsFromSchoolStore() {
	var nData = JSON.parse( getPreloadDesigns() );
	var srcthumb = "";
	if ( nData.length > 0 ) {
		for (var i = 0; i < nData.length; i++) {
			srcthumb = nData[i]['thumbnail'];
			$('.custom-slider').slick('slickAdd','<div class="slide selected-design-item" data-hex="'+nData[i]['hex_value']+'"><img data-parent="'+nData[i]['parent_id']+'" data-id="'+nData[i]['template_id']+'" src="'+srcthumb+'" data-property='+"'"+JSON.stringify(nData[i]['properties'])+"'"+' data-title="'+nData[i]['display_name']+'" class="center-block img-responsive design-thumbnail">'+( getMode() != "SUPPORT" ? '<span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span>' : '')+'</div>');
		}

		setTimeout(function(){
			$(".custom-slider").find(".selected-design-item").eq(0).click();
			$(".tgen-tab-holder").find(".style-holder-item").eq(0).click();
			$(".design-zoom-image").attr( "src", $(".custom-slider").find(".selected-design-item").eq(0).find(".design-thumbnail").attr("src") );

			// set background color for each design items in slider
			$(".selected-design-item").each(function(){
				$(this).css( "background-color", "#"+$(this).attr("data-hex") );
			});
			if($('.custom-slider .slide').length < 6) {
				$(".custom-inner-hold .add-more-slide").removeClass("stop-adding");
			}else {
				$(".custom-inner-hold .add-more-slide").addClass("stop-adding");
			}
		}, 1000);
	}
}

function loadShirtColorsFromCampaign() {
	var prodSettings = JSON.parse(getProductSettings());

	if ( getMode() == "SUPPORT" ) $(".style-holder-item").hide();
	if ( getMode() !== "SUPPORT" ) $(".style-selection").removeClass("addmorestyle-shirt-holder-active");
	$(".shirt-col-selection").hide();	
	$(".shirt-col-selection").removeClass("shirt-col-selection-active");

	$(".design-zoom-image").attr( "src", $('.design-thumbnail[data-parent="'+getActiveParent()+'"]').attr("src") );	

	if( prodSettings.length > 0  ){
		for ( var x = 0; x < prodSettings.length; x++ ) {			
			if( prodSettings[x][0] == getActiveParent() ){
				if ( prodSettings[x][1].length > 0 ) {					
					for ( var y = 0; y < prodSettings[x][1].length; y++ ) {
						$('.style-holder-item[data-id="'+prodSettings[x][1][y]['id']+'"]').show();
						if( prodSettings[x][1][y]['id'].length> 0 ){
							if( prodSettings[x][1][y]['id'] == getActiveStyle() ){
								for ( var z = 0; z < prodSettings[x][1][y]['colors'].length; z++ ) {
									$('.shirt-col-selection[data-id="'+prodSettings[x][1][y]['colors'][z]+'"]').show();
									var color_code = $('.shirt-col-selection[data-id="'+prodSettings[x][1][y]['colors'][z]+'"]').attr("data-code");
									var color_hex = $('.shirt-col-selection[data-id="'+prodSettings[x][1][y]['colors'][z]+'"]').attr("data-hex");

									get_shirt_color_background();			
								}
							}
						}
					}					
				}
			}
		}
	}

	if( $('.style-holder-item:not([style*="display: none"])').size() > 0 ){
		if(  $('.style-holder-active:not([style*="display: none"])').size() == 0){
			$('.style-holder-item:not([style*="display: none"])').eq(0).click();
		}
	}

	if ( selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()] !== undefined ) {
		for ( var x = 0; x < selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()].length; x++ ) {
			$('.shirt-col-selection[data-hex="'+selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()][x]+'"]').addClass("shirt-col-selection-active");
		}
	}
	var color_merch = $('.design-thumbnail[data-parent="'+getActiveParent()+'"]').parent().attr("data-hex");
	if( $('.shirt-col-selection:not([style*="display: none"])').size() > 0 ){
		if( $('.shirt-col-selection-active:not([style*="display: none"])').size() == 0){
			if( $('.shirt-col-selection[data-hex="'+color_merch+'"]:not([style*="display: none"])').size() > 0){
				$('.shirt-col-selection[data-hex="'+color_merch+'"]:not([style*="display: none"])').click();
			}else{
				if( $('.shirt-col-selection[data-code="BLK"]:not([style*="display: none"])').size() > 0){
					$('.shirt-col-selection[data-code="BLK"]:not([style*="display: none"])').click();
				}else{
					$('.shirt-col-selection:not([style*="display: none"])').eq(0).click();			
				}	
			}			
			
		}
	}

	manipulate_add_to_collection();	
	firstselectedcolor( $('.shirt-col-selection-active:not([style*="display: none"])').eq(0).attr("data-hex") );		
	
}

function loadShirtColors() {
	$(".shirt-col-selection").removeClass("shirt-col-selection-active");
	$(".shirt-col-selection").hide();
	disabled_addtocollection();	

	if(xhr_shirts && xhr_shirts.readystate != 4){
        xhr_shirts.abort();
    }

    xhr_shirts = $.ajax({
        type: 'POST',
        url: getMainUrl()+'inventory/load_shirt_colorss/'+getActiveStyle(),
        success: function(data){
            var nData = JSON.parse(data);
            var scolors = "";
			var hex = $(".shirt-col-selection-active:first").attr("data-hex");			      

        	if ( nData.length > 0 ) {
				for ( var x = 0; x < nData.length; x++ ) {
        			if ( getMode() != "SUPPORT" ) $('.shirt-col-selection[data-id="'+nData[x]['color_id']+'"]').show();

					get_shirt_color_background();

				}

				if( $('.shirt-col-selection:not([style*="display: none"])').size() > 0 ){
					if( $(".shirt-col-selection-active").size() == 0 && $('.shirt-col-selection-active:not([style*="display: none"])').size() == 0){
						if( $('.shirt-col-selection[data-code="BLK"]:not([style*="display: none"])').size() > 0){
							$('.shirt-col-selection[data-code="BLK"]:not([style*="display: none"])').click();
						}else{
							$('.shirt-col-selection:not([style*="display: none"])').eq(0).click();				
						}
					}else{
						if ( getMode() == "BUY" ) firstselectedcolor( $('.shirt-col-selection-active:not([style*="display: none"])').eq(0).attr("data-hex") );		
					}
				}
            }          
            
        },
        async:true
    });
	

	if ( selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()] !== undefined ) {
		for ( var x = 0; x < selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()].length; x++ ) {
			$('.shirt-col-selection[data-hex="'+selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()][x]+'"]').addClass("shirt-col-selection-active");
		}
	}
	if ( getMode() == "CAMPAIGN" || getMode() == "SUPPORT" ){
		if( $(".product-item").size() > 0){		
			$('.product-item[cart-id="'+getActiveParent()+'_'+getActiveStyle()+'"]').each(function(j) {			
				var pricee = $(this).attr("data-price");
				if( getMode() == "CAMPAIGN" ) setCampaignprice( pricee );
				$(this).find(".small-shirt-col").each(function(i) {
					var hex2 = $(this).attr("data-hex");			
					$('.shirt-col-selection[data-hex="'+hex2+'"]').addClass("shirt-col-selection-active");
				});

			});
		}	
	}

	if ( getMode() == "BUY" ) firstselectedcolor( $('.shirt-col-selection-active:not([style*="display: none"])').eq(0).attr("data-hex") );	
	manipulate_add_to_collection();	
}

function firstselectedcolor( color_hex ){
	var imgtexture = getAssetUrl()+"/images/ash.jpg";
	var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
	var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
	var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg";
	var getcolorval = $('.shirt-col-selection[data-hex="'+color_hex+'"]').attr("data-id");
	if(color_hex == "91999D") {
		$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+color_hex+" url("+imgtexture+")"});
		$(".zoom-container").css({"background":"#"+color_hex+" url("+imgtexture+")"});
	}else if(color_hex == "E1E5E8") {
		$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+color_hex+" url("+imgtexture2+")"});
		$(".zoom-container").css({"background":"#"+color_hex+" url("+imgtexture2+")"});
	}else if(color_hex == "47506B") {
		$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+color_hex+" url("+imgtexture3+")"});
		$(".zoom-container").css({"background":"#"+color_hex+" url("+imgtexture3+")"});
	}else if(color_hex == "21292C") {
		$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+color_hex+" url("+imgtexture4+")"});
		$(".zoom-container").css({"background":"#"+color_hex+" url("+imgtexture4+")"});
	}else{
		$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+color_hex});
		$(".zoom-container").css({"background":"#"+color_hex});	
	}

	$(".tgen-shirt-holder img.shirt-col").attr( 'data-color-id', getcolorval );	
	$(".tgen-shirt-holder img.shirt-col").attr( 'data-id', getActiveStyle() );	
	$(".tgen-shirt-holder img.shirt-col").attr( 'shirt-style-id', $('.style-selection[data-id="'+getActiveStyle()+'"]').attr("style-id") );
	loadshirtsizedata();
}

function loadVariations( data ){
	$('.owl-carousel').trigger('replace.owl.carousel', data);
	$('.owl-carousel').trigger('refresh.owl.carousel');

	$('.design-variation-item[data-id="'+$('.design-thumbnail[data-parent="'+getActiveParent()+'"]').attr("data-id")+'"]').click();
	$('.design-variation-item').css("background-color","#"+$('.design-thumbnail[data-parent="'+getActiveParent()+'"]').parent().attr("data-hex"));
}

function loadDesignVariations() {
    var srcthumb = "";
	$(".variation-arrow").hide();
	
	if(xhr_variation && xhr_variation.readystate != 4){
        xhr_variation.abort();
    }
    
    xhr_variation = $.ajax({
        type: 'POST',
        url: getMainUrl()+'template/load_artwork_variations/'+getActiveParent(),
        success: function(data){
            var nData = JSON.parse(data);
        	var templateVariations = "";

			templateVariations = '<div class="design-variation-holder design-variation-item" data-id="'+getActiveParent()+'" data-property='+"'"+getTemplateProperties()+"'"+'><img class="img-responsive img-center design-image" data-viewthumbnail="'+$('.design-selection[data-id="'+getActiveParent()+'"]').find(".box-design").attr("data-viewthumbnail")+'" data-viewurl="'+$('.design-selection[data-id="'+getActiveParent()+'"]').find(".box-design").attr("data-imgurl")+'" src="'+$('.design-selection[data-id="'+getActiveParent()+'"]').find(".box-design").attr("src")+'" alt="" /></div>';

            if ( nData.length > 0 ) {
				for ( var x = 0; x < nData.length; x++ ) {
					srcthumb = nData[x]['encodedThumbnailVariation'];
					srcImage = nData[x]['encodedImageVariation'];
					templateVariations += '<div class="design-variation-holder design-variation-item" data-id="'+nData[x]['template_id']+'" data-property='+"'"+nData[x]['properties']+"'"+'><img class="img-responsive img-center design-image" data-viewthumbnail="'+ srcImage +'" data-viewurl="'+nData[x]['thumbnail']+'" src="'+srcthumb+'" alt="" /></div>';
				}
            }
            
            if( nData.length > 2 ){
            	$(".variation-arrow").show();
            }

            loadVariations( templateVariations );
        },
        async:true
    });
}

function loadArtworkColors() {
	if ( xhr_colors && xhr_colors.readystate != 4 ) {
		xhr_colors.abort();
	}

	xhr_colors = $.ajax({
		type: 'POST',
		url: getMainUrl()+'template/load_selectable_colors/'+getActiveParent(),
		success: function(data){
			var nData = JSON.parse(data);
			var primaryColors = "";
			var secondaryColors = "";

			if ( nData['primary'].length > 0 ) {
				for ( var i = 0; i < nData['primary'].length; i++ ) {
					primaryColors += '<span class="col-selection" data-toggle="tooltip" data-placement="top" title="'+nData['primary'][i]['color_name']+'" data-hex="'+nData['primary'][i]['hex_value']+'" data-original-title="'+nData['primary'][i]['color_name']+'"></span>';
				}
			}

			if ( nData['secondary'].length > 0 ) {
				for ( var i = 0; i < nData['secondary'].length; i++ ) {
					secondaryColors += '<span class="col-selection" data-toggle="tooltip" data-placement="top" title="'+nData['secondary'][i]['color_name']+'" data-hex="'+nData['secondary'][i]['hex_value']+'" data-original-title="'+nData['secondary'][i]['color_name']+'"></span>';
				}
			}

			$(".color-primary").html(primaryColors+secondaryColors);
			$(".color-secondary").html(primaryColors+secondaryColors);
			$('.color-primary .col-selection[data-hex="'+default_primary+'"], .color-secondary .col-selection[data-hex="'+default_secondary+'"]').click();

			$(".col-selection").each(function(i){
				$(this).css("background-color","#"+$(this).attr("data-hex"));
			});
        },
        async:true
    });
}

function encodeHTML(str){
	var encodedStr = str.replace(/[\u00A0-\u9999<>\&\'\"\#]/gim, function(i) { return '&#'+i.charCodeAt(0)+';'; });
	return encodedStr.replace(/&/gim, '&');
}

function decodeHTML(str){
  return str.replace(/&#([0-9]{1,3});/gi, function(match, numStr) {
    	var num = parseInt(numStr, 10); // read num as normal number
   	 	return String.fromCharCode(num);
	});
}

function getCustomFieldSettings() {
	var str = "";

	$(".custom-fields").each(function(i){
		if ( str.length > 0  ) {
			str += ","+$(this).attr("id")+":"+encodeHTML( $(this).val() ); 
		} else {
			str += $(this).attr("id")+":"+encodeHTML( $(this).val() );
		}
	});

    if ( getPattern().length > 0 ) {
    	str += ( str.length > 0 ? "," : "" )+"PATTERN:"+getPattern();
    	str += ( str.length > 0 ? "," : "" )+"CATEGORY:"+getIconCategory();
    }

    if ( getDistress().length > 0 ) {
    	str += ( str.length > 0 ? "," : "" )+"DISTRESS:"+getDistress();
    }

    if ( getArtworkIcon().length > 0 ) {
    	str += ( str.length > 0 ? "," : "" )+"ICON:"+getArtworkIcon();
    	str += ( str.length > 0 ? "," : "" )+"CATEGORY:"+getIconCategory();
    }

    if ( getPrimaryColor().length > 0 || getSecondaryColor().length > 0 ) {
    	str += ( str.length > 0 ? "," : "" )+"PRIMARY:"+( ( convert_to_rgb( getPrimaryColor() ) ).replace( /:/g, "=" ) ).replace( /,/g, "-" );
    	str += ( str.length > 0 ? "," : "" )+"SECONDARY:"+( ( convert_to_rgb( getSecondaryColor() ) ).replace( /:/g, "=" ) ).replace( /,/g, "-" );
    }

	return str;
}

function preLoadCollectionItems() {
	var collectionItems = JSON.parse( getPreloadDesigns() );
	var color_hex = 'ffffff';
	var shirtColors = '';
	var showcolors = '';
	var dataPrice = '';
	var customing = '';

	if ( collectionItems.length > 0 ) {
		$(".empty-collection").remove();
		$(".proceed-btn").removeClass("gray-btn").addClass("green-btn").removeAttr("disabled");

		for ( var a = 0; a < collectionItems.length; a++ ) {
			for ( var b = 0; b < collectionItems[a]['shirt_style'].length; b++ ) {
				shirtColors = "";

				setActiveStyle(collectionItems[a]['shirt_style'][b]['id']);
				dataPrice = collectionItems[a]['shirt_style'][b]['data_price'];

				if ( collectionItems[a]['shirt_style'][b]['colors'].length > 0 ) {
					for ( var c = 0; c < collectionItems[a]['shirt_style'][b]['colors'].length; c++ ) {
						if ( c == 0 ) color_hex = $('.shirt-col-selection[data-id="'+collectionItems[a]['shirt_style'][b]['colors'][c]+'"]').attr("data-hex");

						shirtColors += '<span class="small-shirt-col" data-toggle="tooltip" data-placement="top" title="" data-id="'+collectionItems[a]['shirt_style'][b]['colors'][c]+'" data-hex="'+$('.shirt-col-selection[data-id="'+collectionItems[a]['shirt_style'][b]['colors'][c]+'"]').attr("data-hex")+'" data-original-title="'+$('.shirt-col-selection[data-id="'+collectionItems[a]['shirt_style'][b]['colors'][c]+'"]').attr("data-original-title")+'" style="background:#'+$('.shirt-col-selection[data-id="'+collectionItems[a]['shirt_style'][b]['colors'][c]+'"]').attr("data-hex")+';"></span>';
					}
					if ( collectionItems[a]['shirt_style'][b]['colors'].length > 10  ) {
						showcolors +=	'<div class="mycoll-showmore mycoll-showall gregular font-xsmall gray text-left">Show More</div>';
					}
				}
				customing = collectionItems[a]['parent_id']+'crt_trimming'+collectionItems[a]['customization'];
				$('.style-selection[data-id="'+getActiveStyle()+'"]').addClass("addmorestyle-shirt-holder-active");

				$(".mycoll-item-holder").append('<div class="mycoll-shirt-holder-item product-item" cart-id="'+collectionItems[a]['parent_id']+'_'+collectionItems[a]['shirt_style'][b]['id']+'" data-price="'+dataPrice+'" data-product="'+collectionItems[a]['product_id']+'" data-back-color="'+collectionItems[a]['back_color']+'" ><div class="mycoll-shirt-holder item-details" data-parent="'+collectionItems[a]['parent_id']+'" data-canvas-style="'+collectionItems[a]['shirt_style'][b]['canvas_position']+'" data-art-style="'+collectionItems[a]['shirt_style'][b]['art_position']+'" data-id="'+collectionItems[a]['template_id']+'" data-design="'+collectionItems[a]['design_id']+'" data-fields="'+collectionItems[a]['customization']+'"><img class="img-responsive img-center shirt-col" data-hex="'+color_hex+'" style="background:#'+color_hex+'" src="'+$('.style-selection[data-id="'+collectionItems[a]['shirt_style'][b]['id']+'"] .styleshirt-col').attr("src")+'" alt=""><div class="print-canvas2" style="'+collectionItems[a]['shirt_style'][b]['canvas_position']+'"><div class="mycoll-design-holder" style="'+collectionItems[a]['shirt_style'][b]['art_position']+'"><img class="img-responsive img-center item-image" src="'+collectionItems[a]['template_image']+'" alt=""></div></div></div><div class="mycoll-color-holder-outer"><span class="mycoll-design-name gsemibold font-xsmall gray-dark text-left text-uppercase display-name">'+collectionItems[a]['display_name']+'</span><span class="mycoll-style-name gregular font-xsmall gray text-left">'+$('.style-selection[data-id="'+collectionItems[a]['shirt_style'][b]['id']+'"]').parent().find(".style-desc").html()+'</span><span class="mycoll-style-name gregular font-xxsmall gray-dark text-left mycoll-color-label">Product Colors</span><div class="mycoll-color-holder-inner">'+shirtColors+'<div class="clearfix"></div></div>'+showcolors+'</div><span class="mycoll-remove" data-toggle="modal" data-target="#removecollection" data-dismiss="modal"></span><div class="clearfix"></div></div>');

				get_items_shirt_color_background( );
				$(".product-item").each(function(){
					var active_custom = customing.split("crt_trimming");
					var active_artwork = $(this).attr("cart-id").split("_");
					if( active_custom[0] == active_artwork[0] ){
						$(this).find(".item-details").attr("data-fields", active_custom[1] );
					}
				});	
			}
		}
	}
	
	$(".add-more-styles-button").click();
	if( $(".product-item").size() > 0){
		$('.product-item[cart-id="'+getActiveParent()+'_'+getActiveStyle()+'"]').find(".small-shirt-col").each(function(i) {
			var hex2 = $(this).attr("data-hex");
				selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()][i] = hex2;
		});
	}

	
}

function preLoadCartItems() {
	var collectionItems = JSON.parse( getCartItems() );
	var color_hex = 'ffffff';
	var shirtColors = '';
	var showcolors = '';
	var dataPrice = '';
	var customing = '';


	if ( collectionItems.length > 0 ) {
		$(".empty-collection").remove();
		$(".proceed-btn").removeClass("gray-btn").addClass("green-btn").removeAttr("disabled");
		for ( var a = 0; a < collectionItems.length; a++ ) {		
				shirtColors = "";
				setActiveStyle(collectionItems[a]['shirts_and_colors']['id']);
				setActiveArtwork(collectionItems[a]['template_id']);
				dataPrice = collectionItems[a]['shirts_and_colors']['data_price'];
				selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()]= [];

				if ( collectionItems[a]['shirts_and_colors']['colors'].length > 0 ) {
					for ( var c = 0; c < collectionItems[a]['shirts_and_colors']['colors'].length; c++ ) {
						if ( c == 0 ) color_hex = $('.shirt-col-selection[data-id="'+collectionItems[a]['shirts_and_colors']['colors'][c]+'"]').attr("data-hex");

						shirtColors += '<span class="small-shirt-col" data-toggle="tooltip" data-placement="top" title="" data-id="'+collectionItems[a]['shirts_and_colors']['colors'][c]+'" data-hex="'+$('.shirt-col-selection[data-id="'+collectionItems[a]['shirts_and_colors']['colors'][c]+'"]').attr("data-hex")+'" data-original-title="'+$('.shirt-col-selection[data-id="'+collectionItems[a]['shirts_and_colors']['colors'][c]+'"]').attr("data-original-title")+'" style="background:#'+$('.shirt-col-selection[data-id="'+collectionItems[a]['shirts_and_colors']['colors'][c]+'"]').attr("data-hex")+';"></span>';
					}

					if ( collectionItems[a]['shirts_and_colors']['colors'].length > 10  ) {
						showcolors +=	'<div class="mycoll-showmore mycoll-showall gregular font-xsmall gray text-left">Show More</div>';
					}
					
				}	
				customing = collectionItems[a]['parent_id']+'crt_trimming'+collectionItems[a]['properties'];

				$(".mycoll-item-holder").append('<div class="mycoll-shirt-holder-item product-item" cart-key="'+collectionItems[a]['parent_id']+'_'+collectionItems[a]['template_id']+'_'+collectionItems[a]['shirts_and_colors']['id']+'_'+encodeHTML(collectionItems[a]['properties'])+'" cart-id="'+collectionItems[a]['parent_id']+'_'+collectionItems[a]['shirts_and_colors']['id']+'" data-price="'+dataPrice+'" data-product="'+collectionItems[a]['product_id']+'" data-back-color="'+collectionItems[a]['hex_value']+'" ><div class="mycoll-shirt-holder item-details" data-parent="'+collectionItems[a]['parent_id']+'" data-canvas-style="'+collectionItems[a]['shirts_and_colors']['canvas_position']+'" data-art-style="'+collectionItems[a]['shirts_and_colors']['art_position']+'" data-id="'+collectionItems[a]['template_id']+'" data-design="'+collectionItems[a]['design_id']+'" data-fields="'+encodeHTML(collectionItems[a]['properties'])+'"><img class="img-responsive img-center shirt-col"  data-hex="'+color_hex+'" style="background:#'+color_hex+'" src="'+$('.style-selection[data-id="'+collectionItems[a]['shirts_and_colors']['id']+'"] .styleshirt-col').attr("src")+'" alt=""><div class="print-canvas2" style="'+collectionItems[a]['shirts_and_colors']['canvas_position']+'"><div class="mycoll-design-holder" style="'+collectionItems[a]['shirts_and_colors']['art_position']+'"><img class="img-responsive img-center item-image" src="'+collectionItems[a]['template_image']+'" alt=""></div></div></div><div class="mycoll-color-holder-outer"><span class="mycoll-design-name gsemibold font-xsmall gray-dark text-left text-uppercase display-name">'+collectionItems[a]['display_name']+'</span><span class="mycoll-style-name gregular font-xsmall gray text-left">'+$('.style-selection[data-id="'+collectionItems[a]['shirts_and_colors']['id']+'"]').parent().find(".style-desc").html()+'</span><span class="mycoll-style-name gregular font-xxsmall gray-dark text-left mycoll-color-label">Product Colors</span><div class="mycoll-color-holder-inner">'+shirtColors+'<div class="clearfix"></div></div>'+showcolors+'</div><span class="mycoll-remove" data-toggle="modal" data-target="#removecollection" data-dismiss="modal"></span><div class="clearfix"></div></div>');

					get_items_shirt_color_background();	
				$(".product-item").each(function(){
					var active_custom = customing.split("crt_trimming");
					var active_artwork = $(this).attr("cart-id").split("_");
					if( active_custom[0] == active_artwork[0] ){
						$(this).find(".item-details").attr("data-fields", active_custom[1] );
					}
				});	
		}


	}

	$('.style-holder-item[data-id="'+getActiveStyle()+'"]').click();
	if( $(".product-item").size() > 0){
		$('.product-item[cart-id="'+getActiveParent()+'_'+getActiveStyle()+'"]').find(".small-shirt-col").each(function(i) {
			var hex2 = $(this).attr("data-hex");
			selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()][i] = hex2;
		});
	}

}

function loadCollectionItems() {
	var collection_items = '';
	var canvas = $(".tgen-shirt-holder .print-canvas").attr("style");
	var art = $(".tgen-shirt-holder .art-holder").attr("style");
	var color_code = "";
	var color_hex = ""; 
	var custom_fields = ""; 
	$("#confirm").modal('hide');
	if ( getActiveStyle() != 0 ) {
		if ( $(".custom-shirtcol-holder .shirt-col-selection-active").length > 0 ) {
			var color_count = 0;
			var data_hex = $('.design-thumbnail[data-parent="'+getActiveParent()+'"]').parent().attr("data-hex");
			var imgbg = $('.style-holder-item[data-id="'+getActiveStyle()+'"]').find(".styler").attr("src");
			if( $('.mycoll-shirt-holder-item[cart-key="'+getActiveParent()+"_"+getActiveArtwork()+"_"+getActiveStyle()+"_"+getCustomFieldSettings()+'"]').length > 0 ) {				
				$('.mycoll-shirt-holder-item[cart-key="'+getActiveParent()+"_"+getActiveArtwork()+"_"+getActiveStyle()+"_"+getCustomFieldSettings()+'"]').remove();
			}		
			collection_items = '<div class="mycoll-shirt-holder-item product-item" cart-key="'+getActiveParent()+"_"+getActiveArtwork()+"_"+getActiveStyle()+"_"+encodeHTML( getCustomFieldSettings() )+'" cart-id="'+getActiveParent()+"_"+getActiveStyle()+'" data-price="'+getCampaignprice()+'" data-product="'+getActiveProduct()+'" data-back-color="'+data_hex+'">'+
										'<div class="mycoll-shirt-holder item-details" data-parent="'+getActiveParent()+'" data-canvas-style="'+canvas+'" data-art-style="'+art+'" data-id="'+getActiveArtwork()+'" data-design="'+getDesignId()+'" data-fields="'+encodeHTML( getCustomFieldSettings() )+'" >'+
											'<img class="img-responsive img-center shirt-col" data-hex="'+$(".custom-shirtcol-holder .shirt-col-selection-active").eq(0).attr("data-hex")+'" style="background:#'+$(".custom-shirtcol-holder .shirt-col-selection-active").eq(0).attr("data-hex")+'" src="'+imgbg+'" alt="">'+
											'<div class="print-canvas2" style="'+canvas+'"><div class="mycoll-design-holder" style="'+art+'">'+
												'<img class="img-responsive img-center item-image" data-imgurl="'+ getimgURL() +'" src="'+getTemplateThumb()+'" alt="">'+
											'</div></div>'+
										'</div>'+
										'<div class="mycoll-color-holder-outer">'+
											'<span class="mycoll-design-name gsemibold font-xsmall gray-dark text-left text-uppercase display-name">'+getTemplateName()+'</span>'+
											'<span class="mycoll-style-name gregular font-xsmall gray text-left">'+getActiveStyleName()+'</span>'+
											'<span class="mycoll-style-name gregular font-xxsmall gray-dark text-left mycoll-color-label">Product Colors</span>'+
											'<div class="mycoll-color-holder-inner">';

			$(".custom-shirtcol-holder .shirt-col-selection-active").each(function(){
				color_code = $(this).attr("data-original-title");
				color_hex = $(this).attr("data-hex");
				
				collection_items += '<span class="small-shirt-col" data-toggle="tooltip" data-placement="top" title="" data-id="'+$(this).attr("data-id")+'" data-hex="'+$(this).attr("data-hex")+'" data-original-title="'+color_code+'" style="background:#'+color_hex+';"></span>';
				color_count++;
			});

			collection_items +=					'<div class="clearfix"></div>'+
											'</div>';

			if ( color_count > 10  ) {
				collection_items +=			'<div class="mycoll-showmore mycoll-showall gregular font-xsmall gray text-left">Show More</div>';
			}

			collection_items +=			'</div>'+
										'<span class="mycoll-remove" data-toggle="modal" data-target="#removecollection" data-dismiss="modal"></span>'+
										'<div class="clearfix"></div>'+
									'</div>';						
			$(".empty-collection").remove();
			$(".mycoll-item-holder").prepend( collection_items );
			$(".design-cont-btn").click();
			$(".proceed-btn").removeClass("gray-btn").addClass("green-btn").removeAttr("disabled");
			get_items_shirt_color_background( );
			manipulate_add_to_collection();
		} else {
			$('#alertmodal').modal('show');
			$(".text-confirm").html("You need to select a style color first!");
		}
	} else {
		$('#alertmodal').modal('show');
		$(".text-confirm").html("You need to select a shirt style first!");
	}
}

//load artwork patterns
function getArtworkPatterns( templateID ) {
	var xhr_pattern;

	if(xhr_pattern && xhr_pattern.readystate != 4){
		xhr_pattern.abort();
	}

	$(".pattern-container").html("");

	// redirect to tgen with the selected designs
	xhr_pattern = $.ajax({
		type: 'POST',
		url: getMainUrl()+'template/load_artwork_addons/'+getActiveArtwork()+'/PTRN',
		success: function(data){
			var nData = JSON.parse(data);
			if ( nData.length > 0 ) {
				$(".tab-pattern").removeClass("tab-not-allowed").attr("href", "#patterns");
				for ( var x = 0; x < nData.length; x++ ) {
					$(".pattern-container").prepend('<div class="pattern-holder-item pattern-items" data-name="'+nData[x]['addon_name']+'" data-code="'+nData[x]['addon_code']+'" data-category="'+nData[x]['category']+'">'+
														'<img class="img-responsive img-center " src="'+get_mainLink()+'public/scp_admin'+nData[x]['thumbnail'].toLowerCase()+'" alt="">'+
														'<span class="remove-pattern"></span>'+
													'</div>');
					
				}

				$(".distress-holder").hide();
				$("#distress-selection").val(0);
				$(".pattern-selection").removeClass("no-click"); 
			}
			else{
				$(".tab-pattern").addClass("tab-not-allowed").removeAttr("href");
				$(".pattern-selection").addClass("no-click"); 
				$(".tab-panel").removeClass("active").find(".tab-styles").click(); 
				$(".tab-styles").addClass("active"); 
			}
		},
		async:true
	});
}
// TODO
// load artwork icons ( heads & emojies included )
function getArtworkAddons( templateID ) {
	var xhr_addons;
	var addOnString = 'Icons';
	var addOnType = 'ICON';

	$(".addon-selection").html("");

	if(xhr_addons && xhr_addons.readystate != 4){
		xhr_addons.abort();
	}

	// redirect to tgen with the selected designs
	xhr_addons = $.ajax({
		type: 'POST',
		url: getMainUrl()+'template/load_artwork_addons/'+getActiveArtwork()+'/'+addOnType,
		success: function(data){
			var nData = JSON.parse(data);
			var addOnsList = '';
			var currentCategory = '';
			var firstUL = '';
			var lastUL = '';
			if ( nData.length > 0 ) {

				for ( var x = 0; x < nData.length; x++ ) {
					if ( currentCategory.length == 0 ) {
						currentCategory = nData[x]['category'];
						firstUL = '<ul class="icon-toggle-list icons-lists" data-category="'+nData[x]['category']+'"><div class="text-left gregular font-xsmall gray-dark tgen-iconz">'+nData[x]['category']+'</div>';
					} else {
						if ( currentCategory != nData[x]['category'] ) {
							lastUL = '<div class="clearfix"></div></ul>';
							firstUL = '<ul class="icon-toggle-list icons-lists" data-category="'+nData[x]['category']+'"><div class="text-left gregular font-xsmall gray-dark tgen-iconz">'+nData[x]['category']+'</div>';
						} else {
							firstUL = lastUL = '';
						}
					}

					addOnsList += lastUL+firstUL+'<li>'+
									'<div class="icon-box addon-item" data-type="'+nData[x]['addon_type_name']+'" data-code="'+nData[x]['addon_code']+'" data-category="'+nData[x]['category']+'">'+
										'<i class="icon-style icon-'+nData[x]['addon_name'].toLowerCase()+'" title="'+nData[x]['addon_name']+'"></i>'+
									'</div>'+
								  '</li>'+(Number(nData.length - Number(x+1)) == 0 ? "</ul>" : "");

					currentCategory = nData[x]['category'];
				}

				$("<div></div>").addClass("form-group").appendTo(".addon-selection").append("<label></label>").find("label").attr({"class":"gregular font-xsmall gray-darker custom-label","for":""}).text(addOnString).parent().append("<div></div>").find("div").addClass("icons-text-holder custom-input gray-dark font-xsmall gitalic form-control").append("<span></span><i></i><div></div>").find("span").addClass("icon-title span-23 gitalic font-xsmall gray-dark custom-icon").text("Select Icon").parent().find("i").addClass("icon-caret-down pull-right custom-select-arrow").parent().find("div").addClass("icon-selector-holder").append("<div></div>").find("div").addClass("icon-selector-inner").append(addOnsList);
				
			}
			
		},
		async:true
	});
}

function setDesignItems() {
	var colorsList = [];
	
    $('.product-item').each(function(i){
    	colorsList = [];
    	var str = $(this).attr("cart-id").split("_");

    	for ( var x = 0; x < $(this).find(".small-shirt-col").length; x++ ) {
			colorsList[x] = $(this).find(".small-shirt-col").eq(x).attr("data-id");
		}
		templateID = $(this).find(".item-details").attr("data-id");
		thumbnail_img = $(this).find(".item-details .item-image").attr("src");
		
	
    	designLists[i] = {
    						template_id : $(this).find(".item-details").attr("data-id"),
    						display_name : $(this).find(".display-name").html(),
    						campaign_id : getCampaign(),
    						product_id : $(this).attr("data-product"),
    						design_id : $(this).find(".item-details").attr("data-design"),
    						back_color : $(this).attr("data-back-color"),
    						hex_value : $(this).attr("data-back-color"),
    						selling_price : $(this).attr("data-price"),
    						parent_id : str[0],
    						shirt_style : str[1],
    						shirt_colors : colorsList,
    						template_image : $(this).find(".item-details .item-image").attr("src"),
    						raw_template_image : $(this).find(".item-details .item-image").attr("data-imgurl"),
							properties : $(this).find(".item-details").attr("data-fields"),
							canvas_position : $(this).find(".item-details").attr("data-canvas-style"),
							art_position : $(this).find(".item-details").attr("data-art-style")

    					};
    });
 
}

function getDesignItems() {
	return designLists;
}

function TgePageMode() {
	var pageval = 0;
	if( getMode() == 'SUPPORT'){
		pageval = getCampaign();
	}
	return pageval;
}

// load selected designs / style / colors in create campaign page
function sendDetailsToCampaign() {
	if( getCustomizationMode() == 'header-login' ){
		location.reload();
	}else{
		if(xhr_create && xhr_create.readystate != 4){
		xhr_create.abort();
		}

		// redirect to tgen with the selected designs
		xhr_create =  $.ajax({
			type: 'POST',
			url: getMainUrl()+'template/'+getCustomizationMode(),
			data: {
				product_details : getDesignItems(),
				tgen_mode : TgePageMode()
			},
			success: function(data){
				window.location.href = get_mainLink() + data.replace(/(\r\n|\n|\r)/gm,"");
			},
			async:true
		});
	}	
}

function toCamelCase(str) {
    var strArr = str.split(" ");
    var words = '';

    if ( strArr.length > 0 ) {
        for (var a = 0; a < strArr.length; a++) {
            var fnlStr = strArr[a].split("");

            if ( fnlStr.length > 0 ) {
                for ( var b = 0; b < fnlStr.length; b++ ) {
                    if ( b == 0 ) {
                        fnlStr[0] = fnlStr[0].toUpperCase();
                    } else {
                        fnlStr[b] = fnlStr[b].toLowerCase();
                    }
                }
            }

            words += words.length > 0 ? " "+fnlStr.join("") : fnlStr.join("");
        }
    }

    return words;
}

function reloadCustomFields( dataTemp ) {
	setPattern("");
	setArtworkIcon("");

	var str1 = getCustomizations();

	str1 = ( str1 === undefined ) ? "" : str1;

	var custom_field = str1.split(",");
	
	if ( custom_field.length == 0 ) {
		custom_field = str1;
	}

    var temp_prop = JSON.parse( dataTemp );
    var c_field = '';
    var c_fix = '<div class="clearfix"></div>';

	temp_prop.sort(function(a, b){
	    if(a['Name'] < b['Name']) return -1;
	    if(a['Name'] > b['Name']) return 1;
	    return 0;
	});
	
    $(".custom-field-options").html("");
    $(".col-name").remove();
    $(".color-primary .col-selection").parent().parent().append('<div class="gitalic font-xsmall gray-dark col-name lbl-overflow" style="position:relative;top:2px;width:97px;"></div>');
    $(".color-secondary .col-selection").parent().parent().append('<div class="gitalic font-xsmall gray-dark col-name lbl-overflow" style="position:relative;top:2px;width:97px;"></div>');
    


    if ( temp_prop.length > 0 ) {
    	var prevField = "";

        for ( var a = 0; a < temp_prop.length; a++) {
            var f_rules = '';
            var r_class = '';
            var rules_arr = temp_prop[a]['Activities'][0]['Rules'];
 			
            // compose field rules
            for ( var b = 0; b < rules_arr.length; b++ ) {
                if ( rules_arr[b]['Name'] == 'minValue' ) {
                    f_rules += 'minlength="'+rules_arr[b]['Value']+'" ';
                }

                if ( rules_arr[b]['Name'] == 'maxValue' ) {
                    f_rules += 'maxlength="'+rules_arr[b]['Value']+'" ';
                }

                if ( rules_arr[b]['Name'] == 'lowerCase' ) {
                    r_class += " lowerCase";
                }

                if ( rules_arr[b]['Name'] == 'upperCase' ) {
                    r_class += " upperCase";
                }

                if ( rules_arr[b]['Name'] == 'camelCase' ) {
                    r_class += " camelCase";
                }

                if ( rules_arr[b]['Name'] == 'isNumeric' ) {
                    r_class += " isNumeric";
                }

                if ( rules_arr[b]['Name'] == 'alphaNumeric' ) {
                    r_class += " alphaNumeric";
                }
            }

            var field_value = temp_prop[a]['Activities'][0]['KeyValues']['Value'];
            
            if ( custom_field.length > 0) {
	            for ( var c = 0; c < custom_field.length; c++) {
	            	var new_value = custom_field[c].split(":");
	            	if ( new_value.length > 0 && temp_prop[a]['Name'] == new_value[0] ) {
	            		field_value = new_value[1];
	            	}

					if ( new_value[0] == "ICON" ) {
						setArtworkIcon( new_value[1] );
					}

					if ( new_value[0] == "CATEGORY" ) {
						setIconCategory( new_value[1] );
						if ( "PATTERN" == new_value[1] ) {
							setIconCategory( "PATTERNS" );
						}
					}

	            	if ( new_value[0] == "PATTERN" ) {
						setPattern( new_value[1] );
						$('.pattern-items[data-code="'+new_value[1]+'"] .remove-pattern').addClass("remove-pattern-active");
	            	}

	            	if ( new_value[0] == "DISTRESS" ) {
						setDistress( new_value[1] );
						$('#distress-selection option[value="1"]').prop('selected', 'selected').change();
	            	}

					if ( new_value[0] == "PRIMARY" ) {
						setPrimaryColor( convert_to_hex( ( new_value[1].replace( /=/g, ":" ) ).replace( /-/g, "," ) ) );
						$('.primarycol .custom-circle').css("background","#"+getPrimaryColor());						
						$(".primarycol .col-name").text( $('.color-primary .col-selection[data-hex="'+getPrimaryColor()+'"]').attr("title") );
					}

					if ( new_value[0] == "SECONDARY" ) {
						setSecondaryColor( convert_to_hex( ( new_value[1].replace( /=/g, ":" ) ).replace( /-/g, "," ) ) );
						$('.secondarycol .custom-circle').css("background","#"+getSecondaryColor());
						$(".secondarycol .col-name").text( $('.color-secondary .col-selection[data-hex="'+getSecondaryColor()+'"]').attr("title") );
					}
	            }
            }

            // compose field
            if ( prevField != temp_prop[a]['Name'] ) {
            	if(temp_prop[a]['Order'] == undefined)
        		{
        			temp_prop[a]['Order'] = 0
        		}
            	c_field += c_fix+'<div class="form-group" data-order="'+temp_prop[a]['Order']+'">'+
            						'<label class="gregular font-xsmall gray-darker custom-label" for="">'+ucword( temp_prop[a]['Name'].replace("_", " ").replace("TEXT", "TEXT ") )+'</label>'+
            						'<input type="text" class="form-control custom-input gitalic font-xsmall gray-dark custom-fields '+r_class+'" name="'+temp_prop[a]['Name']+'" id="'+temp_prop[a]['Name']+'" Placeholder="'+temp_prop[a]['Name'].replace("_", " ").replace("TEXT", "TEXT ")+'" value="'+field_value+'" '+f_rules+'>'+
                                '</div>';
            }

            prevField = temp_prop[a]['Name'];
        }

        $(".custom-field-options").append( c_field );
		arrangefield();

        if ( checkForFieldRules() && getMode() != "BUY" ) {
        	noOfExec = 0;
        	reload = 1;
    		delayGenerate();
        } else if ( str1.length > 0 ) {
        	noOfExec = 0;
        	reload = 1;
        	delayGenerate( 500 );
        }
    }

    displayDistress();
    
}

function arrangefield() {
	$(".custom-field-options .form-group").find(".custom-label").html("");
	$(".custom-field-options .form-group").sort(sort_li).appendTo('.custom-field-options');
	$(".custom-field-options .form-group").each(function(){
		$(this).find(".custom-label").html("Custom Text "+$(this).attr("data-order"));
	});
			function sort_li(a, b) {
				return ($(b).data('order')) < ($(a).data('order')) ? 1 : -1;
			}
}

function delayGenerate( interval = 2000 ) {
	if ( reload == 1 && noOfExec < 5) {
		setTimeout(function(){
			if ( getHasKerning() ) {
				generateDesign();
				reload = 0;
			} else {
				reload = 1;
			}

			delayGenerate();
			noOfExec++;
		}, interval );
	} else {
		if ( noOfExec == 5 ) {
			generateDesign();
		}
	}
}

function addDesignsToSlider( templateID = 0, hexCode = '000000', image = '', parentID = 0, tempProp = '[]', tempTitle = '' ) {	
		if ( templateID != 0 ) {
			$('.custom-slider').slick('slickAdd','<div class="slide selected-design-item" data-hex="'+hexCode+'"><img data-parent="'+parentID+'" data-id="'+templateID+'" data-property='+"'"+tempProp+"'"+' data-title='+"'"+tempTitle+"'"+' src="'+image+'" class="center-block img-responsive design-thumbnail">'+( getMode() != "SUPPORT" ? '<span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span>' : '')+'</div>');
			$('.custom-slider').slick('refresh');
			
			if($('.custom-slider .slide').length < 6) {
				$(".custom-inner-hold .add-more-slide").removeClass("stop-adding");
			}else {
				$(".custom-inner-hold .add-more-slide").addClass("stop-adding");
			}
		}
}

function reloadSliderContent() {
	var design = "";
	var added_id = "";
	var selected_id = "";		 
	$(".selected-design-item .design-thumbnail").each(function(i){
		if ( i > 0 ) {
				design += ","+$(this).attr("data-parent");
			}else {
				design += $(this).attr("data-parent");
			}	
	});
	var array = design.split(',');
	for (var a = 0; a < array.length; a++) {
		added_id = array[a];
		$(".design-selection").each(function(j){
			selected_id = $(this).attr("data-id");
			if( added_id == selected_id){
				$(this).find(".design-select").addClass('selected-slide');
				$(this).find(".design-select").removeClass('added-new-slide');
			}	

			if ( $(this).find(".selected-slide").hasClass("design-active") == false) {
				if( added_id == selected_id){
					$('.design-thumbnail[data-parent="'+selected_id+'"]').parent().remove();
				}
			}					
			
		});

	}

	$(".design-selection").each(function(i){
		if ( $(this).find(".design-select").hasClass("added-new-slide") ) {
			addDesignsToSlider( $(this).attr("data-id"), $(this).attr("data-hex"), $(this).find(".box-design").attr("src"), $(this).attr("data-id"), $(this).attr("data-property"), $(this).find(".design-name").text() );			
		}
	});
	

	setTimeout(function(){
		$(".custom-slider").find(".selected-design-item").eq(0).click();
		$(".design-zoom-image").attr( "src", $(".custom-slider").find(".selected-design-item").eq(0).find(".design-thumbnail").attr("src") );

		// set background color for each design items in slider
		$(".selected-design-item").each(function(){
			$(this).css( "background-color", "#"+$(this).attr("data-hex") );
		});	
	}, 1000);
}


function tagSelectedDesigns() {
	var design = "";
	var added_id = "";
	var selected_id = "";		 
	$(".selected-design-item .design-thumbnail").each(function(i){
		if ( i > 0 ) {
				design += ","+$(this).attr("data-parent");
			}else {
				design += $(this).attr("data-parent");
			}	
	});
	var array = design.split(',');
	$(".design-select").removeClass('selected-slide design-active added-new-slide');
	for (var a = 0; a < array.length; a++) {
		added_id = array[a];
		$(".design-selection").each(function(j){			
			selected_id = $(this).attr("data-id");
			if( added_id == selected_id){				
				$(this).find(".design-select").addClass('selected-slide design-active');
			}		
			
		});

	}
	
	// disbale the button if there are already six designs added
	if ( $(".custom-slider .selected-design-item").length < 6 ) {
		$(".add-more-designs-button").addClass("green-btn").removeClass("gray-btn").removeAttr("disabled");
		$('#addmoredesign').find(".close").attr("data-dismiss","modal");
		$(".add-more-designs-button").addClass("green-btn").removeClass("gray-btn").removeAttr("disabled");
	} else {
		$(".add-more-designs-button").addClass("gray-btn").removeClass("green-btn").attr("disabled","disabled");
		$('#addmoredesign').find(".close").attr("data-dismiss","");
		$(".add-more-designs-button").addClass("gray-btn").removeClass("green-btn").attr("disabled","disabled");
		$("#addmoredesign").off("click");
	}

	setItemCount( $('.design-item-holder .design-active').length );
}

function displaySchoolName() {
	var str = getSchoolName();
}

function get_shirt_color_background(){
	var imgtexture = getAssetUrl()+"/images/ash.jpg";
	var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
	var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
	var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg";
	var color_hex = "";

	$(".shirt-col-selection").each(function(){
		var c_hex = $(this).attr("data-hex");
		if( c_hex == "91999D") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture+")"});
		}else if( c_hex == "E1E5E8") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture2+")"});
		}else if( c_hex == "47506B") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture3+")"});
		}else if( c_hex == "21292C") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture4+")"});
		}
	});	
}

function get_items_shirt_color_background(){
	var imgtexture = getAssetUrl()+"/images/ash.jpg";
	var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
	var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
	var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg";
	var color_hex = "";

	$(".small-shirt-col").each(function(){
		var c_hex = $(this).attr("data-hex");
		if( c_hex == "91999D") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture+")"});
		}else if( c_hex == "E1E5E8") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture2+")"});
		}else if( c_hex == "47506B") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture3+")"});
		}else if( c_hex == "21292C") {
			$(this).css({"background":"#"+c_hex+" url("+imgtexture4+")"});
		}
	});	

	$(".product-item").find(".shirt-col").each(function(){
		color_hex = $(this).attr("data-hex");
		if(color_hex == "91999D") {
			$(this).css({"background":"#"+color_hex+" url("+imgtexture+")"});
		}else if(color_hex == "E1E5E8") {
			$(this).css({"background":"#"+color_hex+" url("+imgtexture2+")"});
		}else if(color_hex == "47506B") {
			$(this).css({"background":"#"+color_hex+" url("+imgtexture3+")"});
		}else if(color_hex == "21292C") {
			$(this).css({"background":"#"+color_hex+" url("+imgtexture4+")"});
		}else{
			$(this).css({"background":"#"+color_hex});
		}
	});	
	
}

function manipulate_add_to_collection(){
    if(document.URL.indexOf("build") ==-1) { 
	if( $('.shirt-col-selection:not([style*="display: none"])').size() > 0 ){
		if( $(".shirt-col-selection-active").size() == 0 && $('.shirt-col-selection-active:not([style*="display: none"])').size() == 0){
			disabled_addtocollection();
		}else{
			enabled_addtocollection();
		}						
	}else{
		disabled_addtocollection();
	}
    }
}

function enabled_addtocollection() {
	if( $('.tgen-loader-overlay:not([style*="display: none"])').size() == 0 ){
		var mode = getMode();
		if ( mode == "CAMPAIGN" ) {
			$(".tgen-addtocollection-btn").removeClass('gray-btn orange-btn').addClass('green-btn').attr("disabled", false);
		} else if ( mode == "SUPPORT" ) {
			$(".tgen-addtocollection-btn").removeClass('gray-btn').addClass('orange-btn').attr("disabled", false);
		} else {
			$(".tgen-addtocollection-btn").removeClass('gray-btn').addClass('orange-btn').attr("disabled", false);
		}
	}	
	
}

function disabled_addtocollection() {
	var mode = getMode();
	if ( mode == "CAMPAIGN" ) {
		$(".tgen-addtocollection-btn").removeClass('green-btn').addClass('gray-btn orange-btn').attr("disabled", true);
	} else if ( mode == "SUPPORT" ) {
		$(".tgen-addtocollection-btn").removeClass('orange-btn').addClass('gray-btn').attr("disabled", true);
	} else {
		$(".tgen-addtocollection-btn").removeClass('orange-btn').addClass('gray-btn').attr("disabled", true);
	}
	
}

function setArtworkCustomizations() {
	$("#CUSTOM_TEXT1, #CUSTOM_TEXT").blur();

	var currentId = designOpts.template.id;

	designOpts = { template: {} };

	designOpts.template.id = currentId;

    if ( getPrimaryColor().length > 0 || getSecondaryColor().length > 0 ) {
	    designOpts.template.pcolor = convert_to_rgb( getPrimaryColor() );
	    designOpts.template.scolor = convert_to_rgb( getSecondaryColor() );
    }

    if ( getPattern().length > 0 ) {
    	designOpts.template.MASCOT_PATTERN = getPattern();
    }

    if ( getArtworkIcon().length > 0 ) {
    	designOpts.template.MASCOT_HEAD = getArtworkIcon();
    	$('ul.icon-toggle-list li').find('i[title="'+designOpts.template.MASCOT_HEAD+'"]').click();
    }

    if ( $("#distress-selection").val() == 1 && $("#distress-selection").val().length > 0 ) {
        designOpts.template.DistressPattern = 'DISTRESS_1';
        setDistress(designOpts.template.DistressPattern );
    }

    designOpts.template.AddOnType = getAddOnType();
    designOpts.template.iconCategory = getIconCategory();

    // get the custom opts, usually the custom texts
    $('.custom-fields').each(function(){
        if ( $(this).hasClass("upperCase") ) {
            $(this).val( $(this).val().toUpperCase() );
        }

        if ( $(this).hasClass("isAlphaNumeric") ) {
            $(this).val( ucfirst( $(this).val() ) );
        }

        var this_val = $(this).val().trim();
        $(this).val( this_val );

        var key = $(this).attr('id');
        //escape this_val to accomodate special char
        var val = this_val;
        designOpts.template[key] = val;
    });

}

function generateDesign() {
	setArtworkCustomizations();
	disabled_addtocollection();
	$(".tgen-loader-overlay").show();	
    // validate fields
    $('.custom-fields').each(function(){
        if ( $(this).val() == '' ) {
            $(this).val(" "); // to generate even if the value of the field is blank
        }
        // check for empty fields
        if ( $(this).val() == '' && $(this).attr("id") != "SpotColor" ) {
            // alert('Please fill up the required fields');
            $('#alertmodal').modal('show');
			$(".text-confirm").html("Please fill up the required fields.");
            $(this).focus();
            validated = false;
            return false;
        }
    });
    // all fields are OK. Proceed

    if(xhr && xhr.readystate != 4){
        xhr.abort();
    }
    // lets submit the form
    xhr = $.ajax({
        type: 'POST',
        url: getMainUrl()+'template/update',
        data: designOpts,
        success: function(data){
            
            if( data ){
				
            	setTemplateThumb( data.replace(/(\r\n|\n|\r)/gm,"") );

                designOpts.template.src  = data.replace(/(\r\n|\n|\r)/gm,"");     

                if(document.URL.indexOf("build") != -1) { 
                    	$(".ProductView__design__img").attr("src", designOpts.template.src);
                    processArtPosition(designOpts.template.src); 
                    }
          
					replacer(designOpts.template.src);
					$(".tgen-loader-overlay").hide();
					manipulate_add_to_collection();
	                if(document.URL.indexOf("build") == -1) { 
		                $(".design-generated-image,.design-zoom-image").load(function() { }).attr( "src", designOpts.template.src );
	                }
                }
            
        },
        async:true
    });
}
function processArtPosition(DesignUrl){   
    var styleid=$(".ProductView__img").attr("styleid");
    if(styleid==''){
        styleid=33;
    }
   		setActiveStyle(styleid);
    if(typeof DesignUrl!='undefined'){
		replacer(DesignUrl);
	    setArtPosition( getDesignLocation(), getActiveParent(), getActiveStyle() );
    }
} 

function generateDesignId() {
	setArtworkCustomizations();

	if(xhr_design && xhr_design.readystate != 4){
		xhr_design.abort();
	}

	xhr_design = $.ajax({
		type: 'POST',
		url: getMainUrl()+'template/get_design_id',
		data: designOpts,
		success: function(nData){
			if( nData ){
				setDesignId( nData.replace(/(\r\n|\n|\r)/gm,"") );
				$('.selected-design-item .design-thumbnail[data-id="'+getActiveParent()+'"]').attr("data-design",getDesignId());

				checkArtworkexist();
			}
		},
		async:true
	});
}


function checkArtworkexist() {	
	var count = 0;	
	var loadCollection = '';
	$(".product-item").each(function(){
		var active_artwork = $(this).attr("cart-id").split("_");
		var product = $(this).attr("data-product");
		if( getMode() == 'BUY'){
			if( getActiveParent() == active_artwork[0] ){
				if( product != 0 ){
					count += 1;
				}			
			}
		}

		if( getMode() == 'SUPPORT'){
			if( getActiveParent() == active_artwork[0] ){
				if( product == 0 ){
					count += 1;
				}
			}
		}
		
	});	
	loadCollection = loadCollectionItems();	
	return loadCollection;
}


function convert_to_hex( rgb ) {
	var hex = "ffffff";

	if ( rgb.length > 0 ) {
		var new_rgb;
		var clrs = rgb.split(",");

		if ( clrs.length > 0 ) {
			new_rgb = { r : Number(clrs[0].split(":")[1]), g : Number(clrs[1].split(":")[1]), b : Number(clrs[2].split(":")[1]) };
		}
		hex = $.colpick.rgbToHex(new_rgb);
	}

	return hex;
}

function convert_to_rgb( hex ) {
	var rgb = $.colpick.hexToRgb( hex );

	var colorStr = "";
	var newRGB = $.map(rgb, function(value, index) {
		return [value];
	});

	for (var x = 0; x < newRGB.length; x++) {
		if ( x == 0 ) {
			colorStr = "Red:"+newRGB[x]+",";
		}
		else if ( x == 1 ) {
			colorStr += "Green:"+newRGB[x]+",";
		}
		else {
			colorStr += "Blue:"+newRGB[x];
		}
	}
	return colorStr;
}

function loadTgenSettings() {
	var mode = getMode();

	$(".distress-holder").hide();

	if ( mode == "CAMPAIGN" ) {
		$(".tgen-back-to-campaign-btn, .tgen-addtocollection-btn").show();
		$(".cart-items-btn").attr("data-code", "update-campaign").html("Update Campaign Items");
	} else if ( mode == "SUPPORT" ) {
		disabled_addtocollection();
		$(".cart-items-btn").attr("data-code", "support").html("Proceed to Checkout");
		$(".design-cont-header").text("My Cart");
		$(".tgen-addtocollection-btn").html("Add to Cart").show();
		$(".addstyle-holder-item").remove();
		$(".design-color-selection").hide();
	} else {
		$(".tgen-back-to-campaign-btn").hide();
		$(".tgen-addtocollection-btn").show();
		$(".cart-items-btn").attr("data-code", "buy-create").html("Next");
	}
}

function checkForFieldRules() {
	var strCustomizations = "";
	var customField;
	for ( var x = 0; x < $(".custom-fields").length; x++ ) {
		customField = $(".custom-fields").eq(x);

		// set values for lowerCase
		if ( $(".custom-fields").eq(x).hasClass("lowerCase") ) {
			$(".custom-fields").eq(x).val( customField.val().toLowerCase() );
		} else if ( $(".custom-fields").eq(x).hasClass("upperCase") ) {
			$(".custom-fields").eq(x).val( customField.val().toUpperCase() );
		}

        $('.selected-design-item .design-thumbnail[data-id="'+getActiveParent()+'"]').attr("p-color",getPrimaryColor());
        $('.selected-design-item .design-thumbnail[data-id="'+getActiveParent()+'"]').attr("s-color",getSecondaryColor());       

        // check for minimum value entry for fields
        if ( customField.val().length < customField.attr("minlength") ) {
        	customField.css("border","1px solid red").focus();

        	return false;
        	break;
        } else {
        	customField.css("border","1px solid #ccc");
        }
	}

	return true;
}

function getArtworkKerning( templateID = 0 ) {
	setKerning([]);
	setHasKerning(false);

	if(xhr_art_kerning && xhr_art_kerning.readystate != 4){
		xhr_art_kerning.abort();
	}

	xhr_art_kerning = $.ajax({
		type: 'POST',
		url: getMainUrl()+'template/load_kerning/'+templateID,
		success: function(data){
			var nData = JSON.parse(data);

			if ( nData['kerning'].length > 0 ) {
				setHasKerning(true);
				setKerning( nData['kerning'] );
			}
		},
		async:true
	});
}

function checkDetailsForCheckout( buyCreate = "BUY" ) {
	setCustomizationMode(buyCreate);
	if( get_loggedin() == 1){
		sendDetailsToCampaign();
	}else{
		$(".login-box").show();
		$(".reg-box").hide();
		$(".buy-create").hide();
		$(".login-modal-header").text("Please Log in or Register to Continue");			
	}

	$(".startbuy-btn, .startcreate-btn").removeAttr("disabled");
}

function displayDistress() {
	if ( getActiveParent() == 860 || getActiveParent() == 913 || getActiveParent() == 802 || getActiveParent() == 1050 || getActiveParent() == 946 ) {
		$(".distress-holder").hide();
	} else {
		$(".distress-holder").show();
	}
}

function loadSizeData( toDo = "", shirt_id = "", shirt_style_id = "", color_id = "" ) {
	if(xhr && xhr.readystate != 4){
        xhr.abort();
    }

    xhr = $.ajax({
        type : 'POST',
        url : get_mainLink()+'inventory/load_shirt_plan_details',
        data : {
			colors : ""
		},
        success: function(data){
        	var nData = JSON.parse(data);
        	setShirtItems( nData );

    		if( nData.length > 0 ){
				setSizeData( nData );

				if ( toDo == 'populate' ) {
					populateSpecifictionDetails();
				} else if ( toDo == 'chart' ) {
					populateChartDetails( shirt_id, shirt_style_id, color_id );
				}
			}
        },
        async:true
    });
}

function populateChartDetails( shirt_id = 0, shirt_style_id = 0, color_id = 0 ) {
	var shirtData = getSizeData(); // JSON.parse(getSizeData());

	$(".style-label").html($('.style-name').html());

	$('.size-chart-table thead tr').html($("<th />").addClass("gsemibold blackz").attr("data-code", "").text("Body Specs"));
	$('.size-chart-table tbody tr').html("");

	var inputsize = "";
	for ( var x = 0; x < shirtData.length; x++ ) {

		if ( shirtData[x]['shirt_plan_id'] == shirt_id && shirtData[x]['generic_color'] == color_id ) {
			$(".style-label").text(shirtData[x]['style_name']);
			$(".size-chart-info").html($("<p />").addClass("gray gregular font-small text-left").text(shirtData[x]['style_description']));

			var sizeString1 = shirtData[x]['shirt_size_data'].replace("{","").replace("}","");
			var sizeArray1 = sizeString1.split(",");

			for ( var y = 0; y < sizeArray1.length; y++ ) {
					var strArr = sizeArray1[y].split(":");
					if ( $('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table thead tr').append($("<th />").addClass("gsemibold blackz").attr("data-code", shirtData[x]['shirt_size_code']).text(shirtData[x]['shirt_size_code']));
						}
					}	
					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq(0)').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").addClass("gregular blackz").text(strArr[0]));
						}
					}

					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq('+$('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').index()+')').size() == 0 ) {						
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").text(strArr[1]));	
						}
						
					}
				}

			
		}
	}
}

function loadshirtsizedata(){
	var shirt_plan_id = $(".tgen-shirt-holder img.shirt-col").attr("data-id");
	var shirt_style_id = $(".tgen-shirt-holder img.shirt-col").attr("shirt-style-id");
	var color_id = $(".tgen-shirt-holder img.shirt-col").attr("data-color-id");
	populateChartDetails( shirt_plan_id, shirt_style_id, color_id );
}

// ======    ==================   ======   ====   =======   ===   ===          ====         ===      ==============
// =====      ====       ===         ===   ====    =====    ===   ===   ===   =====   =========   ==   ============
// ====   ==   ===   ==   =====   ======   ====  =  ===  =  ===   ========   ======       =====   ===   ===========
// ====   ==   ===   ==   =====   ======   ====  ==  =  ==  ===   =======   =======   =========   ===   ===========
// ====   ==   ===       ======   ======   ====  ===   ===  ===   ======   ========   =========   ===   ===========
// =====      ====   ==========   ======   ====  ==== ====  ===   =====   ===   ===   =========   ==   ============
// =======   =====   ==========   ======   ====  =========  ===   ====          ===         ===      ==============

$(document).ready(function(){


	$(function(){
	   if(document.URL.indexOf("build") == -1) {
       
			var checkForDesigns = JSON.parse( getPreloadDesigns() );
			$(".shirt-col-selection").hide();
			disabled_addtocollection();
			$('[data-toggle="tooltip"]').tooltip();
			
			if ( getMode() == "CAMPAIGN" ) {
				loadDesignsFromCampaign();
				preLoadCollectionItems();
				$("#storetitle").text('TZilla.com - Edit Campaign');
			} else if ( getMode() == "SUPPORT" ) {
				$(".third-step").text("Buy");
				$("#storetitle").text('TZilla.com - Support Campaign');
				loadDesignsFromCampaign();
				preLoadCartItems();			
				$(".add-more-slide").remove();
				$(".variation-container, .pattern-selection").hide();
			} else {	
				preLoadCartItems();		
				loadDesignsFromSchoolStore();
				$("#storetitle").text('TZilla.com - Customization');
				$('.style-selection[data-id="30"]').addClass("addmorestyle-shirt-holder-active"); // for development.tzilla.com
				$(".add-more-styles-button").click();
			}

			loadArtworkColors();
			displaySchoolName();
			loadTgenSettings();
			if ( checkForDesigns.length == 0 ) {
				$(".add-more-slide").click();
			}
			get_shirt_color_background();
			loadSizeData( 'populate', getSizeData() );		
        }
	});

	// load slick slider responsiveness
	$(".custom-slider").slick({
		dots: false,
		infinite: false,
		adaptiveHeight: true,
		slidesToShow: 4,
		verticalSwiping:true,
		vertical:true,
		slidesToScroll: 1,
		prevArrow: $('.vert-next'),
		nextArrow: $('.vert-prev'),
		arrows: true,
		responsive: [{
			breakpoint: 1366,
			settings: {
				verticalSwiping:true,
				vertical:true,
				slidesToShow: 4,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		},{
			breakpoint: 1280,
			settings: {
				verticalSwiping:true,
				vertical:true,
				slidesToShow: 4,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		},{
			breakpoint: 1200,
			settings: {
				verticalSwiping:false,
				vertical:false,
				slidesToShow: 6,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		},{
			breakpoint: 1024,
			settings: {
				verticalSwiping:false,
				vertical:false,
				slidesToShow: 6,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		},{
			breakpoint: 992,
			settings: {
				verticalSwiping:false,
				vertical:false,
				slidesToShow: 6,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		},{
			breakpoint: 768,
			settings: {
				verticalSwiping:false,
				vertical:false,
				slidesToShow: 5,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		},{
			breakpoint: 480,
			settings: {
				verticalSwiping:false,
				vertical:false,
				slidesToShow: 4,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		},{
			breakpoint: 320,
			settings: {
				verticalSwiping:false,
				vertical:false,
				slidesToShow: 3,
				slidesToScroll: 1,
				adaptiveHeight: true
			}
		}]
	});

	// EVENTS
	// updated codes
	$(document).on("click", ".size-chart", function(){
		$("#sizechart").modal('show');
		loadshirtsizedata();
	});

	// this code is for kerning rules implementation for every artwork
    $(document).on("blur keyup", "#CUSTOM_TEXT1, #CUSTOM_TEXT", function(){
        
        var kerning_list = getKerning();
        if ( kerning_list.length > 0 ) {
            setActiveArtwork( kerning_list[0]['parent_template_id'] );
    		designOpts.template.id = getActiveArtwork();
            for ( var a = 0; a < kerning_list.length; a++ ) {
                if ( $(this).val().length >= kerning_list[a]['kerning_length'] ) {
                    designOpts.template.id = kerning_list[a]['template_id'];
                    break;
                }
            }

        }
    });

	$(document).on("blur", ".custom-fields", function(){
		checkForFieldRules();
		$('.selected-design-item .design-thumbnail[data-parent="'+getActiveParent()+'"]').attr("data-fields", getCustomFieldSettings());
	});

	$(document).on("keyup", ".custom-fields", function(){
		if ( $(this).hasClass("camelCase") ) {
            if ( $(this).val().trim(" ").length != 0 ) $(this).val( toCamelCase( $(this).val().replace(/^\s/, '') ) );
        }
	});

	$("#shirt-group-select").on("change", function(){
		if( $(this).val() == ""){
			$(".main-style-holder").show();
		}else{
			var ajaxURL = get_mainLink()+'inventory/show_styles/'+$(this).val();
			$.ajax({
				type: "POST",
				url: ajaxURL,
				data:{},success: function(data){
					var nData = JSON.parse(data);
					$(".main-style-holder").hide();
					if ( nData.length > 0 ) {
						for ( var x = 0; x < nData.length; x++ ) {
	        				$('.addmorestyle-shirt-holder[group-id="'+nData[x]['newgroup']+'"]').parent().show();
						}
	            	}
				}, async : false
			});
		}
		
	});

	$("#design-category").on("change", function(){
		if( $(this).val() == ""){
			$(".design-cont-holder").show();
		}else{
			$(".design-cont-holder").hide();
			$('.design-cont-holder[cat-id="'+$(this).val()+'"]').show();
		}
		
	});

	$(document).on("click", ".addstyle-holder-item", function(){
		$("#shirt-group-select")[0].selectedIndex = 0;
		$("#shirt-group-select").prop("selected", true).change();
		$('#addmorestyle').modal('show');
		var styles = "";
		var cont = "";
		var added_id = "";
		var selected_id = "";		 
		$(".style-holder-item").each(function(i){
			if ( i > 0 ) {
					styles += ","+$(this).attr("data-id");
				}else {
					styles += $(this).attr("data-id");
				}	
		});
		var array = styles.split(',');

		if( getMode() !== 'CAMPAIGN') $(".addmorestyle-shirt-holder").removeClass("addmorestyle-shirt-holder-active");
		$(".addmorestyle-shirt-holder").find(".style-check").removeClass("style-check-active");
		for (var a = 0; a < array.length; a++) {
			added_id = array[a];
			$(".addmorestyle-shirt-holder").each(function(j){
				selected_id = $(this).attr("data-id");
				if( added_id == selected_id){
					$(this).addClass("addmorestyle-shirt-holder-active");
					$(this).find(".style-check").addClass("style-check-active");
				}
			});

		}			
	});	

	// update zoom image
	$(document).on("click", ".selected-design-item", function(){
		$(".style-selection").removeClass("addmorestyle-shirt-holder-active");
		$(".tgen-loader-overlay").show();
		disabled_addtocollection();
		$(".custom-circle").attr("style", "")
		default_primary = $(this).find(".design-thumbnail").attr("p-color");
		default_secondary = $(this).find(".design-thumbnail").attr("s-color");
		setPrimaryColor("");
		setSecondaryColor("");
		
		designOpts.template.id = $(this).find(".design-thumbnail").attr("data-id");
		$("#distress-selection").val(0);

		setCustomizations( $(this).find(".design-thumbnail").attr("data-fields") );
		setActiveParent( $(this).find(".design-thumbnail").attr("data-parent") );
		setActiveArtwork( $(this).find(".design-thumbnail").attr("data-id") );
		getArtworkKerning( getActiveArtwork() );

		setTemplateProperties( $(this).find(".design-thumbnail").attr("data-property") );
		setTemplateThumb( $(this).find(".design-thumbnail").attr("src") );
		setTemplateImg( $(this).find(".design-thumbnail").attr("src") );
		setTemplateName( $(this).find(".design-thumbnail").attr("data-title") )

		reloadCustomFields( $(this).find(".design-thumbnail").attr("data-property") );


		if ( getMode() == "BUY" || getMode() == "CAMPAIGN"  ) {
			loadDesignVariations();			
			if ( getMode() == "CAMPAIGN"  ) {
				loadShirtColorsFromCampaign();	
				$(".design-generated-image").attr("src", $(this).find(".design-thumbnail").attr("src"));
				setActiveProduct( $(this).find(".design-thumbnail").attr("product-id") );			
			}

			if ( getMode() == "BUY"  ) {
				if( $('.shirt-col-selection-active[data-code="BLK"]:not([style*="display: none"])').size() > 0){
					 firstselectedcolor( $('.shirt-col-selection-active[data-code="BLK"]:not([style*="display: none"])').attr("data-hex") );
				}else{
					 firstselectedcolor( $('.shirt-col-selection-active:not([style*="display: none"])').eq(0).attr("data-hex") );		
				}
			}

		} else {
			getArtworkPatterns( getActiveParent() );
			getArtworkAddons( getActiveParent() );

			if ( getMode() == "SUPPORT") {
				$(".design-generated-image").attr("src", $(this).find(".design-thumbnail").attr("src") );
				loadShirtColorsFromCampaign();
				setActiveProduct( $(this).find(".design-thumbnail").attr("product-id") );
				$(".distress-holder").hide();
				$(".addon-selection").hide();
			}
		}

		$(".design-zoom-image").attr( "src", getTemplateThumb() );
		fire72dpi = "firsload";
		if ( $(".style-holder-item").size() == 0 && $('.shirt-col-selection:not([style*="display: none"])').size() == 0) { $(".shirt-col-selection").hide(); disabled_addtocollection(); }
		$('.style-holder-item:not([style*="display: none"])').eq(0).click();		
	});

	// update items inside popup but limit selection upto 6 designs only
	$(document).on("click", ".store-image-box", function(){
		var msg = "";
		if ( getItemCount() < 6 ) {
			if ( $(this).find(".design-select").hasClass("design-active") ) {
				$(this).find(".design-select").removeClass("design-active added-new-slide");
			}else{
				$(this).find(".design-select").addClass("design-active added-new-slide");
			}

		} else {
			if ( $(this).find(".design-select").hasClass("design-active added-new-slide") ) {
				$(this).find(".design-select").removeClass("design-active added-new-slide");
			} else {
				$(".artwork-selection").find(".custom-notif").remove();
				msg = '<div class="alert alert-danger fade in custom-notif alert-dismissable">';
				msg	+='<a href="javascript:void(0);" class="close-flash-notif">×</a>';
				msg	+='<strong>Warning!</strong> you can select only 6 designs';
				msg	+='</div>';
				$(".artwork-selection").append(msg);
			}
		}
		$(".addmorestyle-paginate").text( $('.design-item-holder .design-active').length+'/6' );
		setItemCount( $('.design-item-holder .design-active').length );
		
		if( getItemCount() > 0 ) {
			$('#addmoredesign').find(".close").attr("data-dismiss","modal");
			$(".add-more-designs-button").addClass("green-btn").removeClass("gray-btn").removeAttr("disabled");
			modal = 1;
		}else {
			
			$('#addmoredesign').find(".close").attr("data-dismiss","");
			$(".add-more-designs-button").addClass("gray-btn").removeClass("green-btn").attr("disabled","disabled");
			$("#addmoredesign").off("click");
		}
	});
		
	$(document).on("click", ".close-flash-notif", function(){
		$(".alert-dismissable").remove();
	});
	
	// add selected from designs/artworks popup to slider
	$(document).on("click", "#addmoredesign .done-btn", function(){
		if ( getItemCount() <= 6 ) {
			// clear slider content
			// populate slider content
			reloadSliderContent();
			if(modal==1) {
				$("#addmoredesign").modal("hide");
			}
		}
	});


	// updated code for adding designs/artworks to slider
	$(document).on("click", ".add-more-slide", function(){			
		// mark the selected parent templates on the add more design popup	
		if( !$(this).hasClass("stop-adding") ){
			$("#design-category")[0].selectedIndex = 0;
			$("#design-category").prop("selected", true).change();
			$('#addmoredesign').modal('show');
			tagSelectedDesigns();		
			$(".addmorestyle-paginate").text( $('.design-item-holder .design-active').length+'/6' );		
		}
			$(".alert-dismissable").remove();
			
		if( $(".selected-design-item").size() == 0 ){			
			$('#addmoredesign').find(".close").attr("data-dismiss","");
			$(".add-more-designs-button").addClass("gray-btn").removeClass("green-btn").attr("disabled","disabled");
			$("#addmoredesign").off("click");
		}else {
			$('#addmoredesign').find(".close").attr("data-dismiss","modal");
			$(".add-more-designs-button").addClass("green-btn").removeClass("gray-btn").removeAttr("disabled");
		}
		
	});

	// on select of design vairations
	$(document).on("click", ".design-variation-item", function(){
		var variationimg = $(this).find("img").attr("src");
		var variationimgurl = $(this).find("img").attr("data-viewurl");
		$(".tgen-loader-overlay").show();
		designOpts.template.id = $(this).attr("data-id");
		$("#distress-selection").val(0);
		
		reloadCustomFields( $(this).attr("data-property") );
		setActiveArtwork( $(this).attr("data-id") );
		setTemplateThumb( $(this).find(".design-image").attr("src") );
		setTemplateImg(  $(this).find(".design-image").attr("data-viewthumbnail") );

		getArtworkPatterns( getActiveArtwork() );
		getArtworkAddons( getActiveArtwork() );
		getArtworkKerning( getActiveArtwork() );
		setimgURL(variationimgurl);
		setArtPosition( getDesignLocation(), getActiveParent(), getActiveStyle() );
		fire72dpi = "firsload";
		$(".design-generated-image, .design-zoom-image").attr( "src", getTemplateImg() );
		$(".design-generated-image, .design-zoom-image").attr( "data-imgurl", variationimgurl );
		manipulate_add_to_collection();
	});

	$(document).on("click", ".generate-design", function(){
		fire72dpi = "secondload";
		if ( checkForFieldRules() ) {
			generateDesign();
		}
	});

	$(document).on("click", ".updating-artwork", function(){
		loadCollectionItems();
	});

	$(document).on("click", ".pattern-items", function(){
		var lbl = $(this).attr("data-name");
		lbl = lbl.replace("_", " ").toLowerCase();

		if ($(this).find(".remove-pattern").hasClass("remove-pattern-active")) {
			$(this).find(".remove-pattern").removeClass("remove-pattern-active");
			$(".pattern-label").html("Select A Pattern");
			setPattern( "" );
			setIconCategory( "" );
		} else {
			$(".pattern-holder-item .remove-pattern").removeClass("remove-pattern-active");
			$(this).find(".remove-pattern").addClass("remove-pattern-active");
			$(".pattern-label").html(lbl);
			setPattern( $(this).attr('data-code') );
			setIconCategory( $(this).attr('data-category') );
		}

		if ( checkForFieldRules() ) {
			generateDesign();
		}
		
	});

	$(document).on("click", ".color-primary .col-selection, .color-secondary .col-selection", function(){
		$(this).parent().parent().find(".col-name").remove();
		$(this).parent().parent().find(".custom-circle").css("background", "#"+$(this).attr("data-hex"));
		var title = $(this).attr("title");
		$(this).parent().parent().append('<div class="gitalic font-xsmall gray-dark col-name lbl-overflow" style="position:relative;top:2px;width:97px;">'+title+'</div>');
		if ( $(this).parent().hasClass("color-primary") ) {
			setPrimaryColor( $(this).attr("data-hex") );
			default_primary = "";
		} else {
			setSecondaryColor( $(this).attr("data-hex") );
			default_secondary = "";
		}
	});

// ======    ====   =====    ========   ===============    ===================   ======   ===   =======   ===   ===          ====         ===      ==============
// ======     ===   ====      ====         ===========      ====       ====         ===   ===    =====    ===   ===   ===   =====   =========   ==   ============
// ======   =  ==   ===   ==   ======   =============   ==   ===   ==   ======   ======   ===  =  ===  =  ===   ========   ======       =====   ===   ===========
// ======   ==  =   ===   ==   ======   =============   ==   ===   ==   ======   ======   ===  ==  =  ==  ===   =======   =======   =========   ===   ===========
// ======   ===     ===   ==   ======   =============   ==   ===       =======   ======   ===  ===   ===  ===   ======   ========   =========   ===   ===========
// ======   ====    ====      =======   ==============      ====   ===========   ======   ===  ==== ====  ===   =====   ===   ===   =========   ==   ============
// ======   =====   ======   ========   ================   =====   ===========   ======   ===  =========  ===   ====          ===         ===      ==============


	$(document).on("click", ".shirt-col-selection", function(){
		selected_style_array[ getActiveStyle() ] = [];
		selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()]= [];
		var hex = $(this).attr("data-hex");
		var imgtexture = getAssetUrl()+"/images/ash.jpg";
		var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
		var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
		var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg";
		var colorid = $(".tgen-shirt-holder img.shirt-col").attr( 'data-color-id', $(this).attr("data-id") );
		var styleid = $(".tgen-shirt-holder img.shirt-col").attr( 'shirt-style-id', $(this).attr("style-id") );

		if(hex == "91999D") {
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture+")"});
			$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture+")"});
		}else if(hex == "E1E5E8") {
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture2+")"});
			$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture2+")"});
		}else if(hex == "47506B") {
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture3+")"});
			$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture3+")"});
		}else if(hex == "21292C") {
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture4+")"});
			$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture4+")"});
		}else{
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex});
			$(".zoom-container").css({"background":"#"+hex});
		}

		if($(this).hasClass('shirt-col-selection-active')){
			$(this).removeClass("shirt-col-selection-active");
			var last = $(".shirt-col-selection-active:last").attr("data-hex");
				if(last == "91999D") {
					$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+last+" url("+imgtexture+")"});
					$(".zoom-container").css({"background":"#"+last+" url("+imgtexture+")"});
				}else if(last == "E1E5E8") {
					$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+last+" url("+imgtexture2+")"});
					$(".zoom-container").css({"background":"#"+last+" url("+imgtexture2+")"});
				}else if(last == "47506B") {
					$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+last+" url("+imgtexture3+")"});
					$(".zoom-container").css({"background":"#"+last+" url("+imgtexture3+")"});
				}else if(last == "21292C") {
					$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+last+" url("+imgtexture4+")"});
					$(".zoom-container").css({"background":"#"+last+" url("+imgtexture4+")"});
				}else{
					$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+last});
					$(".zoom-container").css({"background":"#"+last});
				}
		}else {
			$(this).addClass("shirt-col-selection-active");
		}

		$('.shirt-col-selection-active:not([style*="display: none"])').each(function(i) {
			var hex2 = $(this).attr("data-hex");
			selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()][i] = hex2;
		});
		manipulate_add_to_collection();
	});

	$(document).on("click", ".style-holder-item", function(){
		var style_lbl = $(this).attr("data-title");
		setActiveStyle( $(this).attr("data-id") );
		setActiveStyleName( $(this).attr("data-title") );
		setShirtPlan( $(this).attr("data-title") );

		$(".style-holder-item").removeClass("style-holder-active");
		$(".style-holder-item").find(".remove-style-btn-holder").removeClass("remove-style-btn-active");
		$(this).addClass("style-holder-active");
		$(this).find(".remove-style-btn-holder").addClass("remove-style-btn-active");
		$(".tgen-shirt-holder img.shirt-col").attr("src", $(this).find("img.styler").attr("src"));

		if ($(this).find(".remove-style-btn-holder").hasClass("remove-style-btn-active")) {
			$(".style-name").html(style_lbl);
		}else if ( getMode() == "SUPPORT" ) {
			$(".style-name").html(style_lbl);
			loadShirtColorsFromCampaign();
		}

		if ( getMode() == "BUY" || getMode() == "CAMPAIGN" ) {
			if ( selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()] !== undefined ) {
				for ( var x = 0; x < selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()].length; x++ ) {
					$('.shirt-col-selection[data-hex="'+selected_artwork_style_array[getActiveParent()+'_'+getActiveStyle()][x]+'"]').addClass("shirt-col-selection-active");
				}
			}

			loadShirtColors();
		}

		 setArtPosition( getDesignLocation(), getActiveParent(), getActiveStyle() );
	});


	// remove design confirmation popup
	$('#removedesign .yesremove-btn').on('click', function() {
		var index = $(".custom-slider").find(".selected").attr("data-slick-index");
		var id = $(".custom-slider").find(".selected img").attr("data-id");
		var parent_id = $(".custom-slider").find(".selected img").attr("data-parent");
		var j = 0;
		localStorage.removeItem(parent_id);
		$("[data-id*='"+id+"'").find(".design-select").removeClass("design-active");
		$('.custom-slider').slick('slickRemove',index);

		$(".custom-slider .slick-slide").each(function(){
		   $(this).attr("data-slick-index",j);
		   j++;
		});
		
		$(".custom-slider .slide:eq(0)").click();

		if( $(".selected-design-item").size() == 0 ){
			// window.location.href = get_mainLink()+'school/store/'+getSchoolId();
			$(".add-more-slide").click();
			$('#addmoredesign').find(".close").attr("data-dismiss","");
			$(".add-more-designs-button").addClass("gray-btn").removeClass("green-btn").attr("disabled","disabled");
			$("#addmoredesign").off("click");
		}else {
			$('#addmoredesign').find(".close").attr("data-dismiss","modal");
			$(".add-more-designs-button").addClass("green-btn").removeClass("gray-btn").removeAttr("disabled");
		}
		
		if($('.custom-slider .slide').length < 6) {
				$(".custom-inner-hold .add-more-slide").removeClass("stop-adding");
			}else {
				$(".custom-inner-hold .add-more-slide").addClass("stop-adding");
			}

	});

	$(document).on("click", "ul.icon-toggle-list li", function(){
		$(".icons-text-holder").find("i.selected-icon").remove();
		var title = $(this).find("i").attr("title");
		var icon = $(this).find("i").attr("class");
		var res = icon.replace(" icon-style", "");
		$(".icons-text-holder .icon-title").text(title);
		$(".icons-text-holder").append("<i class='"+res+" selected-icon'></i>");
		$(".icons-text-holder .icon-caret-down").hide();
		setArtworkIcon( title );
		setAddOnType( $(this).find(".addon-item").attr("data-type") );
		setIconCategory( $(this).find(".addon-item").attr('data-category') );
	});

	$(document).on("click", ".icons-text-holder", function(){
		$(".icon-selector-holder").toggle();
	});

	$(document).on("click", ".startbuy-btn, .startcreate-btn", function(){
		$(this).attr("disabled", true);
		
		checkDetailsForCheckout( $(this).attr("data-code") );
	});

	$(document).on("click", ".proceed-btn", function(){		
		setDesignItems();
		if ( getMode() == "SUPPORT" ) {
			$(".startbuy-btn").click();
			if( get_loggedin() != 1){ 
				$("#loginmodal").modal("show");
				$(".login-box").show();
				$(".reg-box").hide();
				$(".buy-create").hide();
				$(".login-modal-header").text("Please Log in or Register to Continue");			
			}
		} else if ( getMode() == "CAMPAIGN" ) {
			checkDetailsForCheckout( 'CREATE' );
		} else {
			$("#loginmodal").modal("show");
			$(".login-box").hide();
			$(".reg-box").hide();
			$(".login-modal-header").text("Looks great! What would like to do next?");
			$(".buy-create").show();
		}
		$(".account-activate").hide();
		$(".success-register").hide();
	});

	$(document).on("click", ".tgen-icon-holder:eq(2)", function(){
		$(this).find(".social-share").toggle();
	});

	$(document).on("click", ".yesremove-btn-collection", function(){
		var key = $(this).attr("deleteid");
		$("[cart-key*='"+key+"']").remove();
		var item = $(".mycoll-shirt-holder-item").length;
		if(item < 1){
			$(".mycoll-item-holder").append("<div class='empty-collection gregular gray-light'>Empty</div>");
			$(".design-cont-btn").click();
			$(".proceed-btn").removeClass("green-btn").addClass("gray-btn").attr("disabled","disabled");
		}
	});
	
	$(document).on("click", ".mycoll-remove", function(){
		var removeid = $(this).parent().attr("cart-key");
		$(".yesremove-btn-collection").attr("deleteid",removeid);
	});

	$(document).on("click", ".style-remove", function(){
		$(this).parent().find(".style-check").removeClass("style-check-active");
		$(this).parent().removeClass("addmorestyle-shirt-holder-active");
	});

	$(document).on("click", ".remove-style-btn-active", function(){
		var id = $(this).parent().attr("data-id");
		$(".removestyle").attr("data-remove",id);
	});
	
	$(document).on("click", ".removestyle", function(){
		var id = $(this).attr("data-remove");
		$(".style-holder-inner").find('.style-holder-item[data-id="'+id+'"]').remove();
		var style = $(".style-holder-item").length;
		if(style == 1) {
			$(".style-holder-item").find(".remove-style-btn-holder").hide();
			$(".style-holder-item:eq(0)").click();
		}else {
			$(".style-holder-item:eq(0)").click();
		}
		
		if( $(".style-holder-item").size() == 0){ $(".shirt-col-selection").hide(); disabled_addtocollection(); $(".tgen-shirt-holder .shirt-col").attr("src", ""); }
	});
	
	$(document).on("click", ".addmorestyle-shirt-holder", function(){
		if($(this).hasClass("addmorestyle-shirt-holder-active")){
			$(this).find(".style-check").removeClass("style-check-active");
			$(this).removeClass("addmorestyle-shirt-holder-active");
		}else {
			$(this).find(".style-check").addClass("style-check-active");
			$(this).addClass("addmorestyle-shirt-holder-active");
		}
	});

	$(document).on("click", ".add-style", function(){
		var selectedstyle = $(".addmorestyle-shirt-holder-active").length;
		
		if ( selectedstyle > 0 ) {
			$(".style-holder-inner").find(".style-holder-item").remove();

			$(".addmorestyle-shirt-holder-active").each(function(i){
				var styleitem = '<div class="style-holder-item" data-id="'+$(this).attr("data-id")+'" style-id="'+$(this).attr("style-id")+'" data-title="'+$(this).parent().find(".style-desc").text()+'">';
					styleitem += '<img class="img-responsive img-center styler" src="'+$(this).find(".styleshirt-col").attr("src")+'" alt="" />';

					if ( getMode() != "SUPPORT" ) {
						styleitem += '<div class="remove-style-btn-holder" data-toggle="modal" data-target="#removestyle" data-dismiss="modal"><img class="img-responsive img-center" src="'+getAssetUrl()+'/images/large-remove-button2.png" alt="" /></div>';
					}

					styleitem += '</div>';
						
				$(".style-holder-inner").prepend(styleitem);
			});
			
			$(".style-holder-item:eq(0)").find(".remove-style-btn-holder").show();
		}
		if ( getMode() == "SUPPORT" ) {
			if( $('.product-item').size() > 0){
				var str = $('.product-item').eq(0).attr("cart-id").split("_");
				$('.design-thumbnail[data-parent="'+str[0]+'"]').parent().click();		
				$('.style-selection[data-id="'+str[1]+'"]').click();
			}else{
				$(".style-holder-item").eq(0).click();
			}
		}else{
			$(".style-holder-item").eq(0).click();
		}
		
			

	});

	$(document).on("click", ".slider-overlay, .close-collection-slider, .close-sc-slider-cont", function(){
		$(".design-container").removeClass("design-cont-open");
		$(".super-custom-container").removeClass("scc-show");
		$(".slider-overlay").fadeOut();
	});

	$(document).on("click", ".tgen-cart-btn", function(){
		if($(".super-custom-container").hasClass("scc-show")){
			$(".tgen-cart-btn").click();
		}
		
		$(".design-container").toggleClass("design-cont-open");
		
		if($(".design-container").hasClass("design-cont-open")){
			$(".slider-overlay").fadeIn();
		}else {
			$(".slider-overlay").fadeOut();
		}
	});

	$(document).on("click", ".tgen-back-to-campaign-btn", function(){
		if( getGoback() == '' ){
			window.location.href = get_mainLink()+"campaign/create";
		}else if( getGoback() == 'campaign_edit' ){
			window.location.href = get_mainLink()+'campaign/edit_items/'+getCampaign();
		}else{
			window.location.href = get_mainLink()+'campaign/edit/'+getCampaign()+'/'+getCustomer();
		}
	});

	$(document).on("click", ".tgen-addtocollection-btn", function(){
		generateDesignId();
	});

    $(document).on("keydown", ".isNumeric", function(e){
        var key = e.charCode || e.keyCode || 0;
        // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
        // home, end, period, and numpad decimal
        
        if ( key == 8 || key == 9 || key == 13 || key == 46 || (key >= 96 && key <= 105) || (key >= 48 && key <= 57) ) {
            return true;
        }
        else {
            return false;
        }
    });
    
	$(document).on("click", "#wrap .slide", function(){
		var id = $(this).find("img").attr("data-id");
		var style="";		
		$("#wrap .slide").removeClass("selected");
		$(this).addClass("selected");	
		$(".art-holder img").attr("data-id",id);
		selected_style_array[ getActiveStyle() ] = [];		

		if( $('.shirt-col-selection:not([style*="display: none"])').length > 0 ){
			if( $(".shirt-col-selection-active").size() == 0 ){ 
				$('.shirt-col-selection:not([style*="display: none"])').eq(0).click();
			}
			var imgtexture = getAssetUrl()+"/images/heather-texture.png";
			var hex = $(".shirt-col-selection-active:first").attr("data-hex");
			var imgtexture = getAssetUrl()+"/images/ash.jpg";
			var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
			var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
			var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg";
			var style = $(".style-holder-active").attr("data-id");

			if(hex == "91999D") {
				$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture+")"});
				$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture+")"});
			}else if(hex == "E1E5E8") {
				$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture2+")"});
				$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture2+")"});
			}else if(hex == "47506B") {
				$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture3+")"});
				$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture3+")"});
			}else if(hex == "21292C") {
				$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture4+")"});
				$(".zoom-container").css({"background":"#"+hex+" url("+imgtexture4+")"});
			}else{
				$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex});
				$(".zoom-container").css({"background":"#"+hex});
			}

			$(".shirt-col-selection-active").each(function(i) {
				var hex2 = $(this).attr("data-hex");
				selected_style_array[ getActiveStyle() ][i] = hex2;
			});

		}
	});

	$(document).on("click", ".design-variation-holder", function(){
		$(".design-variation-holder").removeClass("selected-variation");
		$(this).addClass("selected-variation");
	});

	$(document).on("click", ".primarycol", function(){
		$(this).find(".color-primary").toggle();
		$(".color-secondary").hide();
	});

	$(document).on("click", ".secondarycol", function(){
		$(this).find(".color-secondary").toggle();
		$(".color-primary").hide();
	});

	$(document).on("click", ".mycoll-showall", function(){
		$(this).parent().find(".mycoll-color-holder-inner").toggleClass("mycoll-color-holder-inner-show");
		var check = $(this).parent().find(".mycoll-color-holder-inner").hasClass("mycoll-color-holder-inner-show");
		if(check==true) {
			$(this).html("Show Less");
		}else{
			$(this).html("Show More");
		}
	});
	  
	$(document).on("click", ".super-custom-btn", function(){
		if($(".design-container").hasClass("design-cont-open")){
			$(".design-cont-btn").click();
		}

		$(".super-custom-container").toggleClass("scc-show");
		if($(".super-custom-container").hasClass("scc-show")){
			$(".slider-overlay").fadeIn();
		}else {
			$(".slider-overlay").fadeOut();
		}
	});

	$(document).mouseup(function (e){
		var container = $(".color-primary, .color-secondary");

		if (!container.is(e.target) // if the target of the click isn't the container...
			&& container.has(e.target).length === 0) // ... nor a descendant of the container
		{
			container.hide();
		}
	});
	
	$('#addmoredesign').on('show.bs.modal', function (e) {
     $('body').addClass('test');
});

});
