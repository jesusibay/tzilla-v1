/*
* @desc opens a modal window to display a message
* @param string $msg - the message to be displayed
* @return bool - success or failure
* @author Mike Ibay mike.ibay@tzilla.com
* @required settings.php
*/

var templateID = 0; // contains the active artwork ID
var shirtID = 0;
var maxArtworkCount = 0; // contains the limit of the artworks that can be selected inside TGEN
var maxCollectionCount = 0; // contains the limit of the artworks that can be selected inside TGEN
var mainUrl = ""; // contains the base url
var customizationMode = ""; // contains the customization mode for TGEN functions
var selected_artworks = "";
var imageUrl = "";
var colorCode = "";
var designId = "";
var xhrArtworks; // variables to be used for ajax requests to get all the artworks
var customizations = { id : 0, AddOnType : "", iconCategory : "" };

function ucword( value ){
	// Can use also /\b[a-z]/g
	value = value.toLowerCase().replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g, function(replace_latter) { 
		return replace_latter.toUpperCase();
	});

	// First letter capital in each word
	return value;
}

function setMainUrl( value )
{
	mainUrl = value;
}

function getMainUrl()
{
	return mainUrl;
}
/*
Collection Items
*/

var a = [{"color":"CAR","size":"S","price":"12"},{"color":"MNT","size":"M","price":"22"}];; // colors
var b = ["CHA","DHE","CHO"]; // colors
var c = ["ASH","CAR","AHE","FOR"]; // colors
var d = ["COL","DHE","DBL","FOR","GLD"]; // colors
var e = ["ASH","AHE","CAR","MNT","CHA","CHO"]; // colors

var v = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var v1 = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var v2 = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var w = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var w1 = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var w2 = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var y = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var y1 = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var y2 = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};
var y3 = {"template_id":"701","thumbnail":"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png","design_id":"d2856219-1b56-e711-8116-005056011517","shirts":[{"shirt_id":"30","colors":a}]};

var collection_items = [v, w, y];

/*

*/
function addCollection( iData = [] ) {
	var nData = {};

	var lData = localStorage.getItem( "collection" );

	var cData = JSON.parse( lData == null ? '[]' : lData );
	var designIdExist = false;

	console.log( "1 >>", cData );

	if ( cData.length > 0 )
	{
		for (var i = 0; i < cData.length; i++) {
			if ( iData['template_id'] == cData[i]['template_id'] || iData['design_id'] == cData[i]['design_id'] )
			{
				designIdExist = true;
			}
		}

		if ( designIdExist )
		{
			// update existing
			// if ( i <= 1 ) cData.splice( i, 1 );
		}
		else
		{
			// new entry
			var a = [];
			a.push( { "color":iData['color'], "size":"", "price":0 } );

			nData.template_id = iData['template_id'];
			nData.thumbnail = iData['image_url'];
			nData.design_id = iData['design_id'];
			nData.shirts = [{ "shirt_id":iData['product'], "colors":a }];

			cData.push( nData );

			localStorage.setItem( "collection", JSON.stringify( cData ) );
		}
	}
	else
	{
		// new entry
		var a = [];
		a.push( { "color":iData['color'], "size":"", "price":0 } );

		nData.template_id = iData['template_id'];
		nData.thumbnail = iData['image_url'];
		nData.design_id = iData['design_id'];
		nData.shirts = [{ "shirt_id":iData['product'], "colors":a }];

		cData.push( nData );

		localStorage.setItem( "collection", JSON.stringify( cData ) );
	}

	console.log( "2 >>", cData );
}

function updateCollection( data ) {
	console.log( data );
}

function setActiveArtwork( value )
{
	templateID = value;
}

function getActiveArtwork()
{
	return templateID;
}

function setActiveImage( value )
{
	imageUrl = value;
}

function getActiveImage()
{
	return imageUrl;
}

function setActiveShirt( value )
{
	shirtID = value;
}

function getActiveShirt()
{
	return shirtID;
}

function setActiveColor( value )
{
	colorCode = value;
}

function getActiveColor()
{
	return colorCode;
}

function setActiveDesignId( value )
{
	designId = value;
}

function getActiveDesignId()
{
	return designId;
}

function setCustomizationMode( value )
{
	customizationMode = value;
}

function getCustomizationMode()
{
	return customizationMode;
}

function setMaxArtworkCount( value )
{
	maxArtworkCount = value;
}

function getMaxArtworkCount()
{
	return maxArtworkCount;
}

function setMaxCollectionCount( value )
{
	maxCollectionCount = value;
}

function getMaxCollectionCount()
{
	return maxCollectionCount;
}

function loadSelectedArtworks()
{
	$(".selected-artworks").html("");

	var x = [];
	var y = -1;
	var z = localStorage.getItem( "selected_artworks" );

	if ( !( z == null ) )
	{
		y = z.indexOf(",");
	}

	if ( y > -1 )
	{
		x = z.split(",");
	}
	else
	{
		x[0] = z;
	}

	if ( x.length > 0 )
	{
	
		for ( var i = 0; i < x.length; i++ ) {
		
			var n = JSON.parse( localStorage.getItem( x[i] ) );
 
			selected_artworks += '<div class="art-template" data-id="'+n['id']+'"><img src="'+n['image']+'" /></div>';
		}
	}

	$(".selected-artworks").html( selected_artworks );
}

function loadCollectionItems()
{
	var lData = localStorage.getItem( "collection" );
	var cData = JSON.parse( lData == null ? '[]' : lData );

	if ( cData.length > 0 )
	{
		for (var i = 0; i < cData.length; i++) {
			$(".collection-items").append( $("<div />").attr({"data-id":cData[i]['template_id'],"data-design":cData[i]['design_id']}).addClass("collection-item").append( $("<img />").attr("data-url",cData[i]['thumbnail']).addClass("collection-image") ).append( $("<div />").addClass("item-details") ) );
			if ( cData[i]['shirts'].length > 0 )
			{
				for (var j = 0; j < cData[i]['shirts'].length; j++) {
					$('.collection-items .collection-item[data-design="'+cData[i]['design_id']+'"] .item-details').append( $("<div />").html(cData[i]['shirts'][j]['shirt_id']).addClass("product-item").append( $("<div>").addClass("product-colors") ) );

					if ( cData[i]['shirts'][j]['colors'].length > 0 )
					{
						for (var k = 0; k < cData[i]['shirts'][j]['colors'].length; k++) {
							$('.collection-items .collection-item[data-design="'+cData[i]['design_id']+'"] .item-details .product-item .product-colors').append( $("<div />").html(cData[i]['shirts'][j]['colors'][k]['color']) );
						}
					}
				}
			}
		}
	}

	convertCollectionImages();
}

function convertCollectionImages()
{
	$(".collection-item .collection-image").each(function(e){
		$.ajax({
			type: 'POST',
			url: getMainUrl()+'artwork/convertImage',
			data: { url : $(this).attr("data-url") },
			success: function( data ){
				console.log(e);
				$(".collection-item .collection-image").eq(e).attr( "src", data );
			},
			async: false
		});
	});
}

function loadShirts()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/customize/load_products',
		success: function( data ){
			var nData = JSON.parse( data );
			console.log( nData );

			if ( nData.length > 0 )
			{
				var htmlData = "";

				if ( localStorage.getItem( "shirts" ) == null ) localStorage.setItem( "shirt_list", JSON.stringify( nData ) );

				for (var i = 0; i < nData.length; i++) {
					$(".product-settings").append( $("<div />").append( $("<div />").addClass("product-selection").attr("data-id",nData[i]['id']).append( $("<img />").attr("src", getMainUrl()+nData[i]['front_image_link']) ) ).append( $("<div />").addClass("available-colors") ) );
				}

			}
		},
		async: false
	});
}

function loadShirtColors()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/customize/load_shirt_colors',
		success: function( data ){
			console.log( data );
		},
		async: false
	});
}

function loadShirtSpecifications( id = 0 )
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/customize/load_specs/'+id,
		success: function( data ){
			console.log( data );
		},
		async: false
	});
}

// for removal
function loadColorSelection()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/customize/load_selectable_colors',
		success: function( data ){
			console.log( data );
		},
		async: false
	});
}

function loadProductColors( id = 0 )
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/customize/load_shirt_color_by_product/'+id,
		success: function( data ){
			var nData = JSON.parse( data );

			$(".available-colors").html("");

			if ( nData.length > 0 )
			{
				for (var i = 0; i < nData.length; i++)
				{
					$('.product-selection[data-id="'+id+'"]').parent().find(".available-colors").append( $("<div />").addClass("shirt-color").attr({"data-hex":nData[i]['hex_value'],"data-code":nData[i]['color_code']}).css({"float":"left","width":"20px","height":"20px","background-color":"#"+nData[i]['hex_value']}) );
				}
			}

		},
		async: false
	});
}

function loadFilters( type = "", id = 707 )
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/customize/load_artwork_addons/'+id+'/'+type,
		success: function( data ){
			console.log( data );
		},
		async: false
	});
}

function loadArtworkVariations()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/variations/'+getActiveArtwork(),
		success: function( data ){
			var nData = JSON.parse( data );
			var htmlData = "";

			if ( nData.length > 0 )
			{
				$(".artwork-variations").html("");

				for (var i = 0; i < nData.length; i++)
				{
					$(".artwork-variations").append( $("<div />").addClass("art-template").attr("data-id",nData[i]['template_id']).append($("<img />").attr("src",nData[i]['thumbnail'])) );
				}

			}
			else
			{
				$(".artwork-variations").html( "no available variations" );
			}
		},
		async: false
	});
}

function loadArtworkProperties()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/properties/'+getActiveArtwork(),
		success: function( data ){
			var nData = JSON.parse( data );
			
			reloadCustomFields( nData );
		},
		async: false
	});
}

function loadArtworkPosition()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/position/'+getActiveArtwork(),
		success: function( data ){
			var nData = JSON.parse( data );
			
			console.log( nData );
		},
		async: false
	});
}

function getDesignID( customizations )
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/design_id',
		data: customizations,
		success: function( data ){
			alert( "generated design id " + data );

			setActiveDesignId( data );
		},
		async: false
	});
}

function getCustomization()
{
	var data = {};

	data.id = getActiveArtwork();
	data.AddOnType = "ICON";
	data.iconCategory = "";

	$(".custom-fields").each(function(e){ 
		if ( $(this).hasClass("upperCase") ) {
			$(this).val( $(this).val().toUpperCase() );
		}

		if ( $(this).hasClass("isAlphaNumeric") ) {
			$(this).val( ucfirst( $(this).val() ) );
		}

		var this_val = $(this).val().trim();
		$(this).val( this_val );

		var key = $(this).attr('name');

		data[ key ] = this_val;
	});

	return data;
}

function getDesignImage( customizations )
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/image',
		data: customizations,
		success: function( data ){
			var nData = JSON.parse( data );

			setActiveImage( nData['url'] );

			$(".generated-image").attr( { "src": nData['image'], "data-url": nData['url'] } );
		},
		async: false
	});
}

function reloadCustomFields( dataTemp )
{
    var c_field = '';
	var temp_prop = JSON.parse( dataTemp[0]['properties'] );

	temp_prop.sort(function(a, b){
		if(a['Name'] < b['Name']) return -1;
		if(a['Name'] > b['Name']) return 1;
		return 0;
	});

	$(".artwork-properties").html("");

	if ( temp_prop.length > 0 ) {
		var prevField = "";

		for ( var a = 0; a < temp_prop.length; a++) {
			var f_rules = '';
			var r_class = '';
			var rules_arr = temp_prop[a]['Activities'][0]['Rules'];

			// compose field rules
			for ( var b = 0; b < rules_arr.length; b++ ) {
				if ( rules_arr[b]['Name'] == 'minValue' ) {
					f_rules += 'minlength="'+rules_arr[b]['Value']+'" ';
				}

				if ( rules_arr[b]['Name'] == 'maxValue' ) f_rules += 'maxlength="'+rules_arr[b]['Value']+'" ';

				if ( rules_arr[b]['Name'] == 'lowerCase' ) {
					r_class += " lowerCase";
				}

				if ( rules_arr[b]['Name'] == 'upperCase' ) {
					r_class += " upperCase";
				}

				if ( rules_arr[b]['Name'] == 'camelCase' ) {
					r_class += " camelCase";
				}

				if ( rules_arr[b]['Name'] == 'isNumeric' ) {
					r_class += " isNumeric";
				}

				if ( rules_arr[b]['Name'] == 'alphaNumeric' ) {
					r_class += " alphaNumeric";
				}
			}

			var field_value = temp_prop[a]['Activities'][0]['KeyValues']['Value'];

			// compose field
			if ( prevField != temp_prop[a]['Name'] ) {
				if ( temp_prop[a]['Order'] == undefined ) temp_prop[a]['Order'] = 0;

				// should optimize this part
				c_field += '<div class="form-group" data-order="'+temp_prop[a]['Order']+'">'+
							'<label class="" for="'+temp_prop[a]['Name']+'">'+ucword( temp_prop[a]['Name'].replace("_", " ").replace("TEXT", "TEXT ") )+'</label>'+
							'<input type="text" class="custom-fields '+r_class+'" name="'+temp_prop[a]['Name']+'" id="'+temp_prop[a]['Name']+'" placeholder="'+temp_prop[a]['Name'].replace("_", " ").replace("TEXT", "TEXT ")+'" value="'+field_value+'" '+f_rules+'>'+
							'</div>';
			}

			prevField = temp_prop[a]['Name'];
		}

		$(".artwork-properties").html( c_field );
		$(".artwork-properties").append( $("<input />").attr({"type":"button", "name":"Apply", "value":"Generate Artwork"}).addClass("generate-artwork") );
		$(".artwork-properties").append( $("<input />").attr({"type":"button", "name":"Design", "value":"Generate Design ID"}).addClass("generate-design-id") );
    }
}

function loadAllArtwork()
{
		if ( xhrArtworks && xhrArtworks.readystate != 4 )
		{
			xhrArtworks.abort();
		}

		xhrArtworks = $.ajax({
			type: 'POST',
			url: getMainUrl()+'artwork/templates',
			success: function( data ){
				var nData = JSON.parse( data );
				var encodedImage = "";

				if ( nData.length > 0 )
				{
					artworksList = "";

					for ( var i = 0; i < nData.length; i++ ) {
						// art-template class is needed for the trigger of selection for artworks
						artworksList += '<div class="art-template" data-id="'+nData[i]['template_id']+'"><img src="'+nData[i]['thumbnail']+'" /></div>';
					}
				}

				$(".display-result-here").html( artworksList );
			},
			async: false
		});
}

$(document).ready(function(){

	// use this code to load all default functions
	$(function(){
		loadSelectedArtworks();
		loadCollectionItems();
	});

	$(document).on("click", ".set-artwork-limit, .check-slider-artwork-limit", function(){
		setMaxArtworkCount(24);

		console.log("maximumnumber of artworks has been set to "+getMaxArtworkCount());
	});

	$(document).on("click", ".check-collection-limit-fundraiser", function(){
		setMaxCollectionCount(6);

		console.log("maximumnumber of collection items has been set to "+getMaxCollectionCount());
	});

	$(document).on("click", ".load-color-selection", function(){
		loadColorSelection();
	});

	$(document).on("click", ".load-icons", function(){
		loadFilters("ICON");
	});

	$(document).on("click", ".load-patterns", function(){
		loadFilters("PTRN");
	});

	$(document).on("click", ".load-distress", function(){
		loadFilters("DSTR");
	});

	$(document).on("click", ".load-garments", function(){
		loadShirts();
	});

	$(document).on("click", ".load-shirt-colors", function(){
		loadShirtColors();
	});

	$(document).on("click", ".shirt-color", function(){
		$(this).parent().parent().find("img").css("background-color","#"+$(this).attr("data-hex"));
		setActiveColor( $(this).attr("data-code") );
	});

	$(document).on("click", ".load-size-chart", function(){
		loadShirtSpecifications( getActiveShirt() );
	});

	$(document).on("click", ".product-selection", function(){
		loadProductColors( $(this).attr("data-id") );
		setActiveShirt( $(this).attr("data-id") );
	});

	$(document).on("click", ".load-products, .load-position", function(){
		loadArtworkPosition( getActiveArtwork() );
	});

	$(document).on("click", ".collection-add", function(){
		var isItemOk = false;

		if ( getActiveArtwork() == 0 )
		{
			alert( "no template selected" );
		}
		else if ( getActiveDesignId() == "" )
		{
			alert( "no design id generated" );
		}
		else if ( getActiveImage() == "" )
		{
			alert( "no thumbnail generated, generate a design first" );
		}
		else if ( getCustomization() == "" )
		{
			alert( "no customizations" );
		}
		else if ( getActiveShirt() == 0 )
		{
			alert( "no product selected" );
		}
		else if ( getActiveColor() == "" )
		{
			alert( "no shirt color selected" );
		}
		else
		{
			isItemOk = true;
		}

		if ( isItemOk )
		{
			var item = [];
			item['template_id'] = getActiveArtwork();
			item['design_id'] = getActiveDesignId();
			item['image_url'] = getActiveImage();
			item['customization'] = getCustomization();
			item['product'] = getActiveShirt();
			item['color'] = getActiveColor();

			addCollection( item );
		}
	});

	$(document).on("click", ".generate-artwork", function(){
		var cData = getCustomization();

		getDesignImage( cData );
	});

	$(document).on("click", ".generate-design-id", function(){
		var cData = getCustomization();

		getDesignID( cData );
	});

	$(document).on("click", ".next-page", function(){
		window.location.href = getMainUrl()+"fundraiser";
	});

	$(document).on("click", ".art-template", function(){
		setActiveArtwork( $(this).attr("data-id") );
		loadArtworkProperties( getActiveArtwork() );

		$(".generated-image").attr("src", $(this).find("img").attr("src"));1
	});

	$(document).on("click", ".load-variations", function(){
		loadArtworkVariations();
	});

	$(document).on("click", ".load-all-artworks", function(){
		loadAllArtwork();
	});

});