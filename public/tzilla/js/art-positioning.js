//ken
var style_id = 0;
var designWidth = 0;
var designHeight = 0;
var dataid = "";
var style_name = "";
var fire72dpi = "";
var designLoc = "FRNT";
var xhr_art_pos;
var tgenMode = "";

function setImgWidth( value ) {
	designWidth = value;
}

function getImgWidth() {
	return designWidth;
}

function setImgHeight( value ) {
	designHeight = value;
}

function getImgHeight() {
	return designHeight;
}

function setActiveStyle( value ) {
	style_id = value;
}

function getActiveStyle() {
	return style_id;
}

function setShirtPlan( value ) {
	dataid = value;
}

function getShirtPlan() {
	return dataid;
}

function setActiveStyleName( value ) {
	style_name = value;
}

function getActiveStyleName() {
	return style_name;
}

function setActiveParent( value ) {
	parentID = value;
}

function getActiveParent() {
	return parentID;
}

function setDesignLocation( value ) {
	designLoc = value;
}

function getDesignLocation() {
	return designLoc;
}

function setMode( value ) {
	tgenMode = value;
}

function getMode() {
	return tgenMode;
}

function manipulate_add_to_collection(){
	if( $('.shirt-col-selection:not([style*="display: none"])').size() > 0 ){
		if( $(".shirt-col-selection-active").size() == 0 && $('.shirt-col-selection-active:not([style*="display: none"])').size() == 0){
			disabled_addtocollection();
		}else{
			enabled_addtocollection();
		}						
	}else{
		disabled_addtocollection();
	}
}

function enabled_addtocollection() {
	if( $('.tgen-loader-overlay:not([style*="display: none"])').size() == 0 ){
		var mode = getMode();
		if ( mode == "CAMPAIGN" ) {
			$(".tgen-addtocollection-btn").removeClass('gray-btn orange-btn').addClass('green-btn').attr("disabled", false);
		} else if ( mode == "SUPPORT" ) {
			$(".tgen-addtocollection-btn").removeClass('gray-btn').addClass('orange-btn').attr("disabled", false);
		} else {
			$(".tgen-addtocollection-btn").removeClass('gray-btn').addClass('orange-btn').attr("disabled", false);
		}
	}	
	
}

function disabled_addtocollection() {
	var mode = getMode();
	if ( mode == "CAMPAIGN" ) {
		$(".tgen-addtocollection-btn").removeClass('green-btn').addClass('gray-btn orange-btn').attr("disabled", true);
	} else if ( mode == "SUPPORT" ) {
		$(".tgen-addtocollection-btn").removeClass('orange-btn').addClass('gray-btn').attr("disabled", true);
	} else {
		$(".tgen-addtocollection-btn").removeClass('orange-btn').addClass('gray-btn').attr("disabled", true);
	}
	
}

function printableArea( xdesign = 0, ydesign = 0, wdesign = 0, hdesign = 0, staticW = 0, staticH = 0, percentage = 0, palleteDistance = 0) {//wdesign/hdesign must be in inches
	//Width and Height multiply to percentage to get inches
	var percent = Number(percentage/100);

		if(staticW == 1 && staticH == 0) {
			
			var newHdesign = Number(getImgHeight()/71.78*percent).toFixed(2);
			convertiontopercent( xdesign , ydesign , wdesign, newHdesign, palleteDistance);

		}
		else if(staticW == 0 && staticH == 1) {
			
			var newWdesign = Number(getImgWidth()/71.78*percent).toFixed(2);
			convertiontopercent( xdesign , ydesign , newWdesign, hdesign, palleteDistance);

			
			
		}
		else if(staticW == 0 && staticH == 0) 
		{
			var newHdesign = Number(getImgHeight()/71.78*percent).toFixed(2);
			var newWdesign = Number(getImgWidth()/71.78*percent).toFixed(2);
			convertiontopercent( xdesign , ydesign , newWdesign, newHdesign, palleteDistance);

		}
		else {
			var w = Number(wdesign*percent);
			var h = Number(hdesign*percent);
			convertiontopercent( xdesign , ydesign , w, h, palleteDistance);
		
		}
		
		
}

function printableAreaforCamp(ydesign, palleteDistance = 0, shityId = 0, styler = 0, styleName = '', parentId = 0) {
	var b = $(".print-canvas").height();
	var designArt = $("body").attr("data-art");
	var ypos = Number(ydesign*3.7795);//3.7795 is contant value to convert mm to px (8.331)

	ypos = (Number(5.22/100)*ypos);
	ypos = Number((ypos/b)*100);

	var pd = Number(palleteDistance*3.7795);//3.7795 is contant value to convert mm to px (8.331)

	pd = (Number(5.22/100)*pd);
	pd = Number((pd/b)*100);
	
	palleteYaxisCamp(pd.toFixed(2), shityId, styleName, designArt, ypos.toFixed(2));
	
}

function convertiontopercent(xdesign = 0, ydesign = 0, wsize = 0, hsize = 0, palleteDistance = 0) {
	
	var compute = 0;
	
	if (window.matchMedia( "(max-width: 1520px)" ).matches) {
		compute = 12.80;
	}else {
		compute = 15.80;
	}
	if (window.matchMedia( "(max-width: 1366px)" ).matches) {
		compute = 12.80;
	}
	if (window.matchMedia( "(max-width: 1280px)" ).matches) {
		compute = 13.80;
	}
	if (window.matchMedia( "(max-width: 1024px)" ).matches) {
		compute = 15.80;
	}
	if (window.matchMedia( "(max-width: 768px)" ).matches) {
		compute = 20.80;
	}
	if (window.matchMedia( "(max-width: 640px)" ).matches) {
		compute = 16.80;
	}
	if (window.matchMedia( "(max-width: 480px)" ).matches) {
		compute = 11.80;
	}
	
	
	//formula for inches to px
	wsize = Number(wsize*compute);//10.67 18.493 is contant value to convert inches to px
	hsize = Number(hsize*compute);//10.67 is contant value to convert inches to px
	var a = $(".print-canvas").width();
	var b = $(".print-canvas").height();
	//converting px to %
	var c = Number((wsize/a)*100);
	var d = Number((hsize/b)*100);

	//x/y positioning formula for converting mm to px
	var xpos = Number(xdesign*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	xpos = (Number(10.97/100)*xpos);
	xpos = Number((xpos/a)*100);
	
	var ypos = Number(ydesign*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	ypos = (Number(10.97/100)*ypos);
	ypos = Number((ypos/b)*100);
	
	var pd = Number(palleteDistance*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	pd = (Number(10.97/100)*pd);
	pd = Number((pd/b)*100);
	
	if(c > 100) {
		c = 100;
	}else if(d > 100) {
		d = 100;
	}
		if(xpos < 0 ) { 
			xpos = 0;

			palleteYaxis(pd.toFixed(2));
			$(".art-holder").css({"width":c.toFixed(2)+"%"});
			$(".art-holder").css({"height":d.toFixed(2)+"%"}); 
			$(".art-holder").css({"top":ypos.toFixed(2)+"%"});
			$(".art-holder").css({"left":xpos.toFixed(2)+"%"});
			$(".art-holder").css({"right":"0%"});
		}else {
			
			palleteYaxis(pd.toFixed(2));
			$(".art-holder").css({"width":c.toFixed(2)+"%"});
			$(".art-holder").css({"height":d.toFixed(2)+"%"}); 
			$(".art-holder").css({"top":ypos.toFixed(2)+"%"});
			$(".art-holder").css({"left":xpos.toFixed(2)+"%"});
		}
		
	$(".design-generated-image, .design-zoom-image").load(function() {
		$(".tgen-loader-overlay").hide();	
    });
}



function palleteYaxis(val) {	
	if(val == undefined) {
		val = 0;
	}

	// condition for specific shirt plans	
	switch ( getShirtPlan() ) {
		case "Toddler Classic Tee":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(22.5% + "+val+"%)"});
			break;
		case "Girls Premium Tee":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19% + "+val+"%)"});
			break;
		case "Juniors/Womens Racerback":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(30.6% + "+val+"%)"});
			break;
		case "Juniors/Womens Premium V-Neck":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(31% + "+val+"%)"});
			break;
		case "Juniors/womens Premium Tee":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19.3% + "+val+"%)"});
			break;
		case "Youth Pullover Hoodie":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(15.8% + "+val+"%)"});
			break;
		case "Adult Crewneck Sweatshirt":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16% + "+val+"%)"});
			break;
		case "Youth Long Sleeve":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.5% + "+val+"%)"});
			break;
		case "Youth Classic Tee":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(17.6% + "+val+"%)"});
			break;
		case "Adult Pullover Hoodie":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19.2% + "+val+"%)"});
			break;
		case "Adult Premium Long Sleeve Tee":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.8% + "+val+"%)"});
			break;
		case "Adult Premium Tee":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16% + "+val+"%)"});
			break;
		case "Adult Classic Tee":
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.7% + "+val+"%)"});
			break;
		default:
			$(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"19%"});
			break;
	}
}


function palleteYaxisCamp(val, shityId, stylerName, art, top) {
	if(val == undefined) {
		val = 0;
	}
	// condition for specific shirt plans	
	switch ( stylerName ) {
		case "Toddler Classic Tee":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(22.5% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Girls Premium Tee":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Juniors/Womens Racerback":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(30.6% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Juniors/Womens Premium V-Neck":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(31% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Juniors/Womens Premium Tee":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19.3% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Youth Pullover Hoodie":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(15.8% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Adult Crewneck Sweatshirt":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Youth Long Sleeve":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.5% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Youth Classic Tee":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(17.6% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Adult Pullover Hoodie":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19.2% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Adult Premium Long Sleeve Tee":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.8% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Adult Premium Tee":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		case "Adult Classic Tee":
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.7% + "+val+"%)"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
		default:
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".print-canvas").css({"width":"35.3%","height":"44.7%","top":"19%"});
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".art-holder").attr("style",art);
			$(".tshirtdisplay").find('[splan-id*="'+shityId+'"]').find(".design-holder").css({'top':top+'%'});
			break;
	}

	$('.design-display[data-parent="'+getActiveParent()+'"] ').find('.product-details').remove();
	var col = $('.design-templates .dsgn-hold[data-parent="'+getActiveParent()+'"]').attr("dataArt");

	$(".addmore-stylesec .tshirtdisplay").each(function(i){
		setShirtGroupId($(this).find(".item-details").attr("splan-id"));
		$('.design-display[data-parent="'+getActiveParent()+'"]').prepend( $("<div></div>").attr({
			"class":"product-details datanew", 
			"data-parent":getActiveParent(),
			"data-template":$('.design-display[data-parent="'+getActiveParent()+'"]').find(".selected-design").attr("data-id"),
			"data-shirt":$(this).find(".item-details").attr("splan-id"),
			"data-colors":getColorList(),
			"data-price": getPrice(),
			"data-image":$('.design-display[data-parent="'+getActiveParent()+'"]').find(".template-design").attr("src"),
			"back-color":$('.design-display[data-parent="'+getActiveParent()+'"]').attr("back-color"),
			"data-canvas": $(".tshirtdisplay").find('[splan-id*="'+$(this).find(".item-details").attr("splan-id")+'"]').find(".print-canvas").attr("style"),
			"data-art": col
			
			
		}) );

	}); 
}

function replacer(link,camp,style) {
	
	var img = new Image();
	img.onload = function() {
		setImgWidth( this.width );
		setImgHeight( this.height ); 
		if(camp != 1){
			setArtPosition( getDesignLocation(), getActiveParent(), getActiveStyle() );
		}
	}
	
	img.src = link;
}
// load data for art positioning from AMM in SCP
function setArtPosition( designLocation, templateID, styleID, camp = '', styleName = '') {
	xhr_art_pos = $.ajax({
		type: 'POST',
		url: get_mainLink()+'template/load_artwork_management_settings/'+designLocation+'/'+templateID,
		success: function(data){
			var nData = JSON.parse(data);
			// set artwork position here only
			if ( nData.length > 0 ) {
					if(camp != 1){
						palleteYaxis();
					}
				for ( var x = 0; x <= nData.length; x++ ) {
					if ( nData[x]['shirt_style_id'] == styleID && nData[x]['is_default'] == 1) {
						
						if(fire72dpi=="firsload"){
							convertionFirstload( nData[x]['x_axis'], nData[x]['y_axis'], nData[x]['design_width'], nData[x]['design_height'], nData[x]['tgen_percentage'], nData[x]['palletDistance'] );
							
						}else{
							console.log("second "+camp);
							if(camp != 1){
								printableArea( nData[x]['x_axis'], nData[x]['y_axis'], nData[x]['design_width'], nData[x]['design_height'], nData[x]['staticWidth'], nData[x]['staticHeight'], nData[x]['tgen_percentage'], nData[x]['palletDistance'] );
							}else {
								printableAreaforCamp(nData[x]['y_axis'], nData[x]['palletDistance'], nData[x]['shirt_style_id'], camp, styleName, templateID);
							}
							
						}
						manipulate_add_to_collection();
						break;
					}

				}
			}
		},
		async:true
	});
}
