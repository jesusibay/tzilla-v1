var ctx = canvas.getContext("2d");
var img1 = loadImage( $(".img-shirt").attr("src"), main);
var img2 = loadImage( $(".img-artwork").attr("src"), main);
var imagesLoaded = 0;

function main() {
    imagesLoaded += 1;

    if(imagesLoaded == 2) {
        // composite now
        ctx.drawImage(img1, 0, 0);   
        ctx.drawImage(img2, 130, 100);
    }
}

function loadImage(src, onload) {
    var img = new Image();
    
    img.onload = onload;
    img.src = src;

    return img;
}

$(document).ready(function(){
  var canvas = document.getElementById("canvas");
  loadImage( $(".img-shirt").attr("src"), main);
  loadImage( $(".img-artwork").attr("src"), main);  
});
