var xhr;
var xhr_items;
var schoolId = 0;
var itemIndex = 0;
var productId = 0;
var campaignId = 0;
var parentId = 0;
var basePrice = 10.50;
var cartItemCount = 0;
var schoolName = "";
var colorsLists = "";
var assetUrl = "";
var cartItems = [];
var orderItems = [];
var shirtWeight = []
var shirtColors = [];
var colorSizeData = [];
var productDetails = [];


function setProductId( value ) {
	productId = value;
}

function getProductId() {
	return productId;
}

function setParentId( value ) {
	parentId = value;
}

function getParentId() {
	return parentId;
}

function setCampaignId( value ) {
	campaignId = value;
}

function getCampaignId() {
	return campaignId;
}

function setOrderItems( value ) {
	orderItems = value;
}

function getOrderItems() {
	return orderItems;
}

function setBasePrice( value ) {
	basePrice = value;
}

function getBasePrice() {
	return basePrice;
}

function setSchoolId( value ) {
	schoolId = value;
}

function getSchoolId() {
	return schoolId;
}

function setSchoolName( value ) {
	schoolName = value;
}

function getSchoolName() {
	return schoolName;
}

function setCartItems( value ) {
	cartItems = value;
}

function getCartItems() {
	return cartItems;
}

function setProductDetails( value ) {
	productDetails = value;
}

function getProductDetails() {
	return productDetails;
}

function setShirtColors( value ) {
	shirtColors = value;
}

function getShirtColors() {
	return shirtColors;
}

function setItemCount( value ) {
	cartItemCount = value;
}

function getItemCount() {
	return cartItemCount;
}

function setItemIndex( value ) {
	itemIndex = value;
}

function getItemIndex() {
	return itemIndex;
}

function setColorsLists( value ) {
	colorsLists = value;
}

function getColorsLists() {
	return colorsLists;
}

function setSizeArrangement( data ) {
	colorSizeData = JSON.parse(data);
}

function getSizeArrangement() {
	return colorSizeData;
}

function setAssetUrl( value ) {
	assetUrl = value;
}

function getAssetUrl() {
	return assetUrl;
}

function setShirtWeight( value ) {
	shirtWeight = JSON.parse(value);
}

function getShirtWeight() {
	return shirtWeight;
}

function setTgenValue( value ) {
	campaignId = value;
}

function getTgenValue() {
	return campaignId;
}

function activateButtonToCheckout() {
	if ( getItemCount() > 0 ) {
		$(".proceed-btn").addClass("orange-btn").removeClass("gray-btn").attr("disabled", false);
	} else {
		$(".proceed-btn").addClass("gray-btn").removeClass("orange-btn").attr("disabled", true);
	}
}

function gotoTgen(){
	var school_id = getSchoolId();
	var parent_id = 0;
    var default_template = 0;
    var camp = getTgenValue();
    var dataProductDetails = JSON.parse( getProductDetails() );
	if(xhr && xhr.readystate != 4){
        xhr.abort();
      }
      xhr =  $.ajax({
          type: 'POST',
          url: get_mainLink()+'template/cart_items',
          data: {
              school : school_id,
              product_details : dataProductDetails,
              designs : dataProductDetails,
              default_parent : parent_id,
              default_template : default_template
          },
          success: function(data){
          	if(  getTgenValue() == 0 ){
          		window.location.href = get_mainLink()+"template/customize/";
          	}else{
          		window.location.href = get_mainLink()+"template/customize/"+getTgenValue()+"/"+getParentId();
          	}
          	
          },
          async:false
      });
}

function loadcartsession(){
	var school_id = getSchoolId();
	var parent_id = 0;
    var default_template = 0;
    var camp = getTgenValue();
    var dataProductDetails = JSON.parse( getProductDetails() );
	if(xhr && xhr.readystate != 4){
        xhr.abort();
      }
      xhr =  $.ajax({
          type: 'POST',
          url: get_mainLink()+'template/cart_items',
          data: {
              school : school_id,
              product_details : dataProductDetails,
              designs : dataProductDetails,
              default_parent : parent_id,
              default_template : default_template
          },
          success: function(data){        	
          },
          async:false
      });
}

function displaySchoolName() {
	var str = getSchoolName();
}

function loadItems() {
	if(xhr && xhr.readystate != 4){
        xhr.abort();
    }

    xhr = $.ajax({
        type: 'POST',
        url: get_mainLink()+'inventory/load_shirt_plan_details',
        success: function(data){
        	setShirtItems( JSON.parse( data ) );
    		loadCartItems();
        },
        async:true
    });
}

function getItemPrice( sid, fieldName, color = '', size = '' ) {
	var itemsList = getShirtItems();
	var return_value = '';

	if ( itemsList.length > 0 ) {
		for ( var x = 0; x < itemsList.length; x++ ) {
			if ( color == itemsList[x]['generic_color'] && size == itemsList[x]['shirt_size_code'] && itemsList[x]['shirt_plan_id'] == sid ) {
				return_value = itemsList[x][fieldName];
				break;
			} else if ( itemsList[x]['shirt_plan_id'] == sid ) {
				return_value = itemsList[x][fieldName];
				break;
			}
		}
	}

	return return_value;
}

function calculateItems() {
	var itemCount = $(".cart-review-item").length;
	var shirt_id = 0;
	var addAmount = 0;
	var item_total = 0;
	var total_item = 0;
	var total_amount = 0;
	var total_weight = 0;
	var weight_list = getShirtWeight();

	if ( Number(itemCount) > 0 ) {
		for ( var a = 0; a < itemCount; a++ ) {
			item_total = 0;
			shirt_id = $(".cart-review-item").eq(a).attr("data-id");

			$(".cart-review-item").eq(a).find(".size-input").each(function(){
				// calculate for total weight
				if ( Number( $(this).val() ) > 0 ) {
					total_weight += Number( weight_list[ $(this).attr("style-code") ] === undefined ? 0.8 : weight_list[ $(this).attr("style-code") ][ $(this).attr("data-code") ] ) * Number( $(this).val() );
				}

				total_item += Number( $(this).val() );

				var data_product = $(this).parent().parent().parent().parent().find(".design-image").attr("data-product");
			 	var shirt_price = (data_product == 0)? getItemPrice( shirt_id, 'retail' ) : $(this).parent().parent().parent().parent().find(".item-price").text().replace("$ ", "");
				item_total += ( Number( $(this).val() ) * shirt_price );

				if ( Number( $(this).val() ) > 0 && $(this).hasClass("sz-pls") == true ) {
					item_total += 1 * Number( $(this).val() );
				}
			});

			total_amount += item_total;

			$(".cart-review-item").eq(a).find(".item-total").html( "$ " + Number( item_total ).toFixed( 2 ) );
		}

		setItemCount( total_item );
		$(".total-items-count").html( total_item );
		$(".total-items-amount").html( "$ " + Number( total_amount ).toFixed( 2 ) );
	}

	if ( total_weight > 2000 ) {
		$(".proceed-btn").addClass("gray-btn").removeClass("orange-btn").attr("disabled", true);
		alert("Your order exceeds our weight limit of 2,000 lbs. please remove a few qty or contact our customer service at: support@tzilla.com");
	} else {
		activateButtonToCheckout();
	}

}

function saveCartItems() {
	var cartItemsArr = [];
	var shirt_id = 0;
	var shirt_price = 0;
	var shirt_sell_price = 0;
	var a = 0;

	$(".size-input").each(function(i){
		add_amount = ( $(this).hasClass("sz-pls") == true ? 1 : 0 );		
		if ( Number( $(this).val() ) > 0 ) {
			// sz-pls this is the class used for tagging sizes with additional 1 dollar
			shirt_id = $(this).parent().parent().parent().parent().attr("data-id");
			var shirt_style_id = $(this).attr("color-size-style-id");
			var data_product = $(this).parent().parent().parent().parent().find(".design-image").attr("data-product");
			shirt_price = (data_product == 0)? getItemPrice( shirt_id, 'retail' ) : getItemPrice( shirt_id, 'wholesale' );
			shirt_sell_price = (data_product == 0)? getItemPrice( shirt_id, 'retail' ) : $(this).parent().parent().parent().parent().find(".item-price").text().replace("$ ", "");
			var sub_total = Number( shirt_sell_price ) + Number( add_amount );
			cartItemsArr[a] = {
    						base_price : getBasePrice(),
    						price : Number( shirt_price ) + Number( add_amount ),
    						selling_price : Number( shirt_sell_price ) + Number( add_amount ),
    						quantity : Number( $(this).val() ),
    						subtotal : Number( sub_total * Number( $(this).val() ) ),
    						shirt_plan_id : shirt_id,
    						shirt_style_id : shirt_style_id,
							shirt_group_plan : $('.cart-review-item[data-id="'+shirt_id+'"] .review-title-left .review-title').html(),
							generic_shirt_color_code : $(this).parent().parent().parent().parent().find(".review-title-right .review-color").attr("data-code"),
							generic_color_name : $(this).parent().parent().parent().parent().find(".review-title-right .review-color").attr("title"),
							note : "",
							size_code : $(this).attr("data-code"),
							thumbnail : $(this).parent().parent().parent().parent().find(".design-image").attr("src"),
							product_id : data_product,
							design_id : $(this).parent().parent().parent().parent().find(".design-image").attr("data-design"),
							front_template_id : $(this).parent().parent().parent().parent().find(".design-image").attr("data-parent")
    					};

			/*
			// these are the fields that can be set inside the controller
			cartItemsArr[a]['printer_setup'] = "";
			cartItemsArr[a]['garment_type'] = "";
			cartItemsArr[a]['matrix_items'] = "";
			// load design settings from AMM for specific artwork
			cartItemsArr[a]['designs'] = [{
											"location": "FRONT",
											"design_id": "31e8de09-72f8-e411-80dc-00155de18007",
											"x_coordinate": "123.12", 
											"y_coordinate": "456.78", 
											"position_uom":"millimeter",
											"design_width": "11.5",

											"design_height": "12",
											"design_uom": "inches",					
											"printing_pallet": "5",	
											"art_printer_setup" : "2",			
											"temp_image": "http:\/\/vectorengineapi.cloudapp.net\/Temp-Images\/78481394-37ea-4983-a500-918e4956ed29.png"
										}]; // Load FRONT by default
			
			// dependent fields on campaign_id / product_id
			cartItemsArr[a]['product_id'] = 0;
			cartItemsArr[a]['campaign_id'] = 0;
			cartItemsArr[a]['supplier_code'] = "";
			cartItemsArr[a]['template_code'] = ""; // replacement for item class field needed by erp
			cartItemsArr[a]['item_code'] = ""; // Value will be from ERP, must have value before passing to FA



			cartItemsArr[a]['custom_options'] = [{

			}];
			*/
			a++;
		}
	});

	setOrderItems(cartItemsArr);
	sendItemsToCheckout(cartItemsArr);

}
var ctr = 0;
var objData_array = [];
function sendItemsToCheckout( objData, counter = 50 ) {
	if(xhr_items && xhr_items.readystate != 4){
        xhr_items.abort();
    }            
   	for(var key in objData)
	{

	objData_array.push(objData[key]);
		if( ctr < counter-1 )			
		ctr++;
		else
			ctr=0;

		
		if(ctr==0 || key == objData.length -1 ){
			ajaxForCheckout(objData_array);
			
			objData_array=[];
		}
		
	}
    
}
function ajaxForCheckout(objData_array)
{
	xhr_items = $.ajax({
        type : 'POST',
        url : get_mainLink()+'cart/checkout_items',
        data : {
			cart_items : objData_array,
			selected_colors : getColorsLists()
		},
        success: function(data){
            var result = JSON.parse( data );
            $(".proceed-btn").attr("disabled", true);
	        if(result['stat'] == 'success'){
	          window.location.href = get_mainLink()+result['message'];
	          $(".proceed-loader").show();
	        }else{
	        	$('#verifypop').modal('show');
	        	$(".proceed-btn").attr("disabled", false);
	        }
        },
        async:false
    });
}

function loadCartItems() {
	$(".cart-items").html("");

	var cartItemsArr = JSON.parse( getCartItems() );
	var sizeArrangement = getSizeArrangement();
	var availableSizes = '';
	var availablePlusSizes = '';
	var imgtexture = getAssetUrl()+"/images/ash.jpg";
	var imgtexture2 = getAssetUrl()+"/images/atheltic-heather.jpg";
	var imgtexture3 = getAssetUrl()+"/images/denim-heather.jpg";
	var imgtexture4 = getAssetUrl()+"/images/charcoal-heather.jpg"; 

	if ( cartItemsArr.length > 0 ) {

		for ( var a = 0; a < cartItemsArr.length; a++ ) {
				
			availableSizes = $("<div />").addClass("input-holder-left");
			// availablePlusSizes = $("<div />").addClass("input-holder-right").append($("<div />").addClass("sq-holder text-center").append($("<span />").addClass("sq-label gitalic blackz font-small text-uppercase").text("Plus")).append($("<span />").addClass("sq-label gregular blackz font-small text-uppercase").text("+$1")));
			var shirtImage = get_mainLink()+cartItemsArr[a]['shirt_plan_image'];
			var designImage = cartItemsArr[a]['template_image'];
			var product_id = cartItemsArr[a]['product_id'];			
			var shirtName = cartItemsArr[a]['shirt_plan_name'];
			var shirtStyleIdval = cartItemsArr[a]['shirt_style_id'];
			var sizedata = cartItemsArr[a]['shirt_color_sizes'].split(",");
			var canvas = cartItemsArr[a]['canvas_position'];
			var artholder = cartItemsArr[a]['art_position'];	
			var item_price = (product_id == 0)? getItemPrice( cartItemsArr[a]['shirt_plan_id'], 'retail' ) : cartItemsArr[a]['selling_price'];		
			var color_hex = cartItemsArr[a]['shirt_color_hex'];		
			var color_code = cartItemsArr[a]['shirt_color_code'];

			// populate sizes that are available per color
			for ( var x = 0; x < sizeArrangement.length; x++ ) {
				for ( var y = 0; y < sizedata.length; y++ ) {
					var shirtStyleId = sizedata[y].split(":");			
					var arrDistinct = new Array();
					arrDistinct.push(shirtStyleId);
			
					for ( var i = 0; i < arrDistinct.length; i++ ) {
						var scode = arrDistinct[i][0];
						var valuee = arrDistinct[i][1];	
						var style_code = arrDistinct[i][2];	

						if ( sizeArrangement[x]['sizeid'] == scode ) {		

							switch ( scode ) {
								case "2XL":
								case "3XL":
									availableSizes.append($("<div />").addClass("sq-holder text-center").append($("<span />").addClass("sq-label gitalic blackz font-small text-uppercase").text(sizeArrangement[x]['sizeid'])).append($("<input />").attr({"color-size-style-id":valuee,"style-code":style_code,"data-code":sizeArrangement[x]['sizeid'],"type":"text","class":"sz-pls sq-text gitalic blackz font-small text-center size-input","maxlength":"4"})));
									break;
								default:
									availableSizes.append($("<div />").addClass("sq-holder text-center").append($("<span />").addClass("sq-label gitalic blackz font-small text-uppercase").text(sizeArrangement[x]['sizeid'])).append($("<input />").attr({"color-size-style-id":valuee,"style-code":style_code,"data-code":sizeArrangement[x]['sizeid'],"type":"text","class":"sq-text gitalic blackz font-small text-center size-input","maxlength":"4"})));
									break;
							}
						}		
					}
					
				}
			}	

			setCampaignId( cartItemsArr[a]['campaign_id'] );			
			$("<div />").addClass("col-lg-4 col-md-4 col-sm-12 col-xs-12 cart-item").appendTo(".cart-items").append("<div />").find("div").attr("data-id",cartItemsArr[a]['shirt_plan_id']).addClass("cart-review-item-holder cart-review-item").append($("<span />").attr({"class":"cart-review-remove","data-toggle":"modal","data-target":"#confirm","data-dismiss":"modal"})).append($("<span />").attr({"class":"cart-review-zoom","data-toggle":"modal","data-target":"#modzoom","data-dismiss":"modal"})).append($("<div />").addClass("cart-review-shirt-holder").append($("<img />").attr({"class":"img-responsive img-center shirt-col shirt-image","src":shirtImage,"alt":"", "data-hex":color_hex}).css("background","#"+cartItemsArr[a]['shirt_color_hex'])).append($("<div />").addClass("print-canvas").attr("style",canvas).append($("<div />").addClass("design-holder art-holder").attr("style",artholder).append($("<img />").attr({"class":"img-responsive img-center design-image","data-parent":cartItemsArr[a]['parent_id'], "data-id":cartItemsArr[a]['template_id'],"data-design":cartItemsArr[a]['design_id'],"back-color":cartItemsArr[a]['back_color'], "data-product":cartItemsArr[a]['product_id'], "data-properties":cartItemsArr[a]['custom_fields'], "src":designImage,"alt":""}))))).append($("<div />").addClass("left-right-parent").append($("<div />").addClass("review-title-left").append($("<span />").addClass("review-title gray-darker gregular font-small lbl-overflow").text(shirtName)).append($("<span />").attr({"class":"create-camp-link gregular font-small pointer size-chart"}).text("Size Chart"))).append($("<div />").addClass("review-title-right").append($("<span />").addClass("review-title gregular font-small").text("Style Color")).append($("<span />").addClass("review-color").attr({"data-id":cartItemsArr[a]['shirt_color_id'],"shirt-style-id":shirtStyleIdval,"data-code":cartItemsArr[a]['shirt_color_code'],"title":cartItemsArr[a]['shirt_color_name']}).css("background","#"+cartItemsArr[a]['shirt_color_hex']))).append($("<div />").addClass("clearfix"))).append($("<div />").addClass("review-size-quantity").append($("<div />").addClass("sq-title gray-dark gregular font-small").text("Sizes and Quantity")).append(availableSizes).append(availablePlusSizes).append($("<div />").addClass("clearfix"))).append($("<div />").addClass("review-price-left").append($("<div />").addClass("blackz gsemibold font-small text-center").text("Item Price")).append($("<div />").addClass("blackz gregular font-small text-center item-price").text("$ "+Number(item_price ).toFixed(2)))).append($("<div />").addClass("review-price-right").append($("<div />").addClass("blackz gsemibold font-small text-center").text("Item Total")).append($("<div />").addClass("blackz gsemibold font-small text-center item-total").text("$0.00"))).append($("<div />").addClass("clearfix"));
			var loadd = ( getCampaignId() == 0 )? $(".create-camp-instead").show() : $(".create-camp-instead").hide();

			if( color_code == "ASH") {
				$('.review-color[data-code*="ASH"]').css({"background":"#"+color_hex+" url("+imgtexture+")"});
				$('.shirt-col[data-hex="'+color_hex+'"]').css({"background":"#"+color_hex+" url("+imgtexture+")"});
			}else if( color_code == "AHE") {
				$('.review-color[data-code*="AHE"]').css({"background":"#"+color_hex+" url("+imgtexture2+")"});
				$('.shirt-col[data-hex="'+color_hex+'"]').css({"background":"#"+color_hex+" url("+imgtexture2+")"});
			}else if( color_code == "DHE") {
				$('.review-color[data-code*="DHE"]').css({"background":"#"+color_hex+" url("+imgtexture3+")"});
				$('.shirt-col[data-hex="'+color_hex+'"]').css({"background":"#"+color_hex+" url("+imgtexture3+")"});
			}else if( color_code == "CHE") {
				$('.review-color[data-code*="CHE"]').css({"background":"#"+color_hex+" url("+imgtexture4+")"});
				$('.shirt-col[data-hex="'+color_hex+'"]').css({"background":"#"+color_hex+" url("+imgtexture4+")"});
			}

		}
	}
	setParentId( $(".design-image").eq(0).attr("data-parent") );
	loadcartsession();
}

function checkForEmptyInputs() {
	var emptyInputs = true;

	$(".cart-review-item").each(function(i){
		emptyInputs = true
		var sizeInputCount = $(this).find(".size-input").length;

		for ( var x = 0; x < sizeInputCount; x++ ) {
			if ( Number( $(".cart-review-item").eq( i ).find(".size-input").eq( x ).val() ) > 0 ) {
				emptyInputs = false;
			}
		}

		if ( emptyInputs ) {
			$('#alertmodal').modal('show');
			$(".text-confirm").html("Please fillout this entry.");
			$(".cart-review-item").eq( i ).find(".size-input").eq(0).focus();

			return false;
		}
	});

	if ( !emptyInputs ) {
		saveCartItems();
	}
}

function loadSizeData( toDo = "", shirt_id = "", shirt_style_id = "", color_id = "" ) {
	var colorsData = getColorsLists();

	if(xhr && xhr.readystate != 4){
        xhr.abort();
    }

    xhr = $.ajax({
        type : 'POST',
        url : get_mainLink()+'inventory/load_shirt_plan_details',
        data : {
			colors : colorsData
		},
        success: function(data){
        	var nData = JSON.parse(data);
        	setShirtItems( nData );
    		loadCartItems();

    		if( nData.length > 0 ){
				setSizeData( nData );

				if ( toDo == 'populate' ) {
					populateSpecifictionDetails();
				} else if ( toDo == 'chart' ) {
					populateChartDetails( shirt_id, shirt_style_id, color_id );
				}
			}
        },
        async:true
    });
}

function populateChartDetails( shirt_id = 0, shirt_style_id = 0, color_id = 0 ) {
	var shirtData = getSizeData(); // JSON.parse(getSizeData());

	$(".style-label").html($('.cart-review-item[data-id="'+shirt_id+'"]').find(".review-title").html());

	$('.size-chart-table thead tr').html($("<th />").addClass("gsemibold blackz").attr("data-code", "").text("Body Specs"));
	$('.size-chart-table tbody tr').html("");

	var inputsize = "";
	for ( var x = 0; x < shirtData.length; x++ ) {

		if ( shirtData[x]['shirt_plan_id'] == shirt_id && shirtData[x]['generic_color'] == color_id ) {
			$(".style-label").text(shirtData[x]['style_name']);
			$(".size-chart-info").html($("<p />").addClass("gray gregular font-small text-left").text(shirtData[x]['style_description']));

			var sizeString1 = shirtData[x]['shirt_size_data'].replace("{","").replace("}","");
			var sizeArray1 = sizeString1.split(",");

			for ( var y = 0; y < sizeArray1.length; y++ ) {
					var strArr = sizeArray1[y].split(":");
					if ( $('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table thead tr').append($("<th />").addClass("gsemibold blackz").attr("data-code", shirtData[x]['shirt_size_code']).text(shirtData[x]['shirt_size_code']));
						}
					}	
					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq(0)').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").addClass("gregular blackz").text(strArr[0]));
						}
					}

					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq('+$('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').index()+')').size() == 0 ) {						
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").text(strArr[1]));	
						}
						
					}
				}

			
		}
	}
	// $('.cart-review-item[data-id="'+shirt_id+'"]').find(".review-size-quantity").html('<div class="sq-title gray-dark gregular font-small">Sizes and Quantity</div>'+inputsize);
}

$(window).load(function(){
	$(".update-design-items").attr("disabled", false);
});

$(document).ready(function(){

	$(function(){	
		$(".update-design-items").attr("disabled", true);	
		$('.left-slide').hide();
		$(".breadcrumb li.active").html("Buy");

		$(".dc-open").click();

		displaySchoolName();
		loadSizeData( 'populate', getSizeData() );
		$("#storetitle").text('TZilla.com - Cart Review');
	});

	$('.right-slide').click(function(event) {
		var pos1 = $('.prod-holder').scrollLeft() + 50;
		$('.prod-holder').scrollLeft(pos1);
		$('.left-slide').show();
	});

	$('.left-slide').click(function(event) {
		var pos2 = $('.prod-holder').scrollLeft() - 50;
		$('.prod-holder').scrollLeft(pos2);
	});

	$(document).on("click", ".cart-review-zoom", function(){
		$(".zoomed-image").attr("src",$(this).parent().find(".design-image").attr("src"));
	});

	$(document).on("click", ".size-chart", function(){
		$("#sizechart").modal("show");
		var shirt_plan_id = $(this).parent().parent().parent().attr("data-id");
		var shirt_style_id = $(this).parent().parent().parent().find(".review-color").attr("shirt-style-id");
		var color_id = $(this).parent().parent().parent().find(".review-color").attr("data-id");
		populateChartDetails( shirt_plan_id, shirt_style_id, color_id );
	});	

	$(document).on("keyup", ".size-input", function(){
		if ( $(".design-cont-btn").hasClass("dc-open") ) {
			$(".dc-open").click();
		}
		calculateItems( );
    });

    $(document).on("keypress", ".size-input", function(e){
		var key = event.which;    
	    if( ! ( key >= 48 && key <= 57 ) )
	        event.preventDefault();
    });

	$(document).on("keydown", ".size-input", function(e){
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl/cmd+A
            (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+C
            (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: Ctrl/cmd+X
            (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105) ) {
            e.preventDefault();
        }
    });

	$(document).on("click", ".update-design-items", function(){
		gotoTgen();
	});

	$(document).on("click", ".create-campaign-instead", function(){
	    window.location.href = get_mainLink()+"campaign/create";
	});

	$(document).on("click", ".proceed-btn", function(){
		if ( $(this).hasClass("orange-btn") ) {
			checkForEmptyInputs();
		}
	});

	$(document).on("click", ".remove-item", function(){
		$(".cart-item").eq(getItemIndex()).remove();
		$(".close").click();
		if( $(".cart-review-remove").size() == 0 ){
			gotoTgen();
		}
		calculateItems( );
	});

	$(document).on("click", ".nogoback-btn", function(){
		$(".close").click();
	});

	$(document).on("click", ".cart-review-remove", function(){
		setItemIndex($(this).parent().parent().index());
	});

	$(document).on("click", ".cart-review-zoom", function(){
		$(".item-image-background").attr("style",$(this).parent().find(".shirt-image").attr("style"));
	});

});