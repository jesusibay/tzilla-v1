 function send_message() {
    var contact_name = $("#contact-name").val();
    var contact_email = $("#contact-email").val();
    var contact_phone = $("#contact-phone").val();
    var contact_school = $("#contact-schoolname").val();
    var contact_subject = $("#contact-subject").val();
    var contact_message = $("#contact-message").val();

    var ajaxURL = get_mainLink()+"signin/contact_us";
     
    $.ajax({
        url: ajaxURL,
        type: "POST",
        data:{ 
            contact_name: contact_name,
            contact_email: contact_email,
            contact_phone: contact_phone,
            contact_school: contact_school,
            contact_subject: contact_subject,
            contact_message: contact_message,
            recaptcha: grecaptcha.getResponse(gcontactus)

        },success: function(ndata){
            var data = JSON.parse(ndata);
            console.log(data);
            if( data['status'] == "success"){
                $("#alertmodal").modal('show');
                $(".modal-notification-width").css({"height" : "152px"});
                $(".search-loader").hide();
                $(".text-confirm").text("Email Sent Successfully.");
                 grecaptcha.reset(gcontactus);
                setTimeout(function() {             
                    $("#alertmodal").modal('hide');
                    $(".req-field").val("");
                      grecaptcha.reset(gcontactus);
                }, 2000);
                console.log("send2success");
            }else{
                  grecaptcha.reset(gcontactus);
            }


        },


    });
 }

 function validateNumber(event) { //validate numbers only
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
          return true;
        } else if ( key < 48 || key > 57 ) {
         return false;
        } else {
         return true;
        }
     }


    function validateinput(){
      
       $(".req-field").each(function(i){
            if( $(this).val() == "" ){
                $(this).addClass('error');
                $( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' ).insertAfter( this );
                if( $(this).parent().find(".error-label").size() > 1 ){
                    $(this).parent().find('.error-label').last().remove();
                }
            }           
            
         });
         $(".error").eq(0).focus();

    }



    $(document).ready(function(){
         $('#contact-phone').keypress(validateNumber);

         $('#contact-email').on('keyup', function() {
            var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
            if(!re) {
                $(this).addClass("failed-email error");
                $( '<label  class="error error-label" >Invalid Email</label>' ).insertAfter( this );
                if( $(this).parent().find(".error-label").size() > 1 ){
                    $(this).parent().find('.error-label').last().remove();
                }
              
            } else {
                $(this).removeClass("failed-email error");
                $(this).parent().find('.error-label').last().remove();
              
            }
        });

          $(document).on("blur", "#contact-email", function(){
            var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
            if(!re) {
                $(this).addClass("failed-email error");
                $( '<label  class="error error-label" >Invalid Email</label>' ).insertAfter( this );
                if( $(this).parent().find(".error-label").size() > 1 ){
                    $(this).parent().find('.error-label').last().remove();
                }
               
            } else {
                $(this).removeClass("failed-email error");
                $(this).parent().find('.error-label').last().remove();
                
            }
        });

         $(document).on("keyup", ".req-field", function(){
            if( $(this).val() != "" ){
                $(this).removeClass("error");
                $(this).parent().find('.error-label').remove();
            }
         });

        $(document).on("click", ".send-message", function(){

            validateinput();
            var validated = true;
            if( grecaptcha.getResponse(gcontactus).length === 0 ){
              $(".validation-error-contactus").text( "Please check the captcha form." );
                setTimeout(function() { $(".validation-error-contactus").text(""); }, 3700);
                validated = false;
                return false;
            }

            if( $(".error").size() == 0 ){
                send_message();
                $(".search-loader").show();
            }
        });

    });
