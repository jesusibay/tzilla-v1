
var xhrVariableName; // variables to be used for ajax requests
var xhrArtworks; // variables to be used for ajax requests to get all the artworks
var intVariableName = 0;
var strVariableName = ""; // define possible string values here
var mainUrl = ""; // contains the base url
var artworksList = ""; // contains html to diplay the artworks for selection
var boolVariableName = true; // true or false

function function_name( param1 = "", param2 = 0 ) {

}

function setMainUrl( value ) {
	mainUrl = value;
}

function getMainUrl() {
	return mainUrl;
}


$(document).ready(function(){

	$(document).on("@event", "@element", function(){

	});
	$
	
	$(document).on("click", ".get-artworks", function()
	{

		if ( xhrArtworks && xhrArtworks.readystate != 4 )
		{
			xhrArtworks.abort();
		}

		xhrArtworks = $.ajax({
			type: 'POST',
			url: getMainUrl()+'artwork/fetchFeaturedFundraiser',
			success: function(data){
				var nData = JSON.parse(data);
			
				if ( nData.length > 0 )
				{
					artworksList = "";

					for ( var i = 0; i < nData.length; i++ ) {
						// encodedImage = encodeImage( nData[i]['thumbnail'] );
						artworksList += '<img src="'+nData[i]['thumbnail1']+'" width="200px" height="200px" />';
					}
				}

				$(".display-result-here").html( artworksList );
			},
			async: false
		});

	});

});