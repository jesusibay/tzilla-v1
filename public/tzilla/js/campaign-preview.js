var xhr;
var designDatas = [];
var campaignGoal = '';

function setDateStarted( value ) {
    start_date = value;
}

function getDateStarted() {
    var temp_var = new Date( start_date );
    return temp_var;
}

function setDateEnded( value ) {
    ended_date = value;
}

function getDateEnded() {
    var temp_var = new Date( ended_date );
    return temp_var;
}

function setDateToday( value ) {
    today_date = value;
}

function getDateToday() {
    var temp_var = new Date( today_date );
    return temp_var;
}

function setcampaignGoal( value ) {
    campaignGoal = value;
}

function getcampaignGoal( value ) {
    campaignGoal = value;
}

function doCountDown() {
    var ds = getDateStarted().getTime();
    var de = getDateEnded().getTime();
    var dt = new Date().getTime();
    var rt = ( de - dt ) / 1000;
    var dateend = new Date($('#end_date').val());
    var datestart = new Date($('#start_date').val());

    var remaining_days = parseInt( rt / 86400 );
    var rh = parseInt( rt / 3600 );
    var remaining_hours = ( rh % 24 );
    var rm = parseInt( rt / 60 );
    var remaining_minutes = ( rm % 60 );
    var remaining_seconds = ( parseInt( rt % 60 ) );           

    $('#camptimer').countdown({until: '+'+remaining_days+'d +'+remaining_hours+'h +'+remaining_minutes+'m +'+remaining_seconds+'s', padZeroes: true});
}

function gray_btn(){
    $(".save-changes").attr("disabled", true);
    $(".save-changes").removeClass("orange-btn");
    $(".save-changes").addClass("gray-btn");
}

function orange_btn(){
    $(".save-changes").attr("disabled", false);
    $(".save-changes").removeClass("gray-btn");
    $(".save-changes").addClass("orange-btn");
}

function checkImg(src){
   var jqxhr = $.get(src, function() {
    return true;
   }).fail(function() { 
    $(".camp-header-holder").attr("style", "");
    return false;
   });
}

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

$(document).ready(function(){
    // gray_btn();   
	checkImg( $("#checkphoto_path").attr("src") );
    $("#storetitle").text('TZilla.com - Campaign Preview');
    var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua']; 
    
    if( $("#campaign_id").val() == "" ){
        $('#setcampdetails').modal({
            backdrop: 'static',
            keyboard: false
        });
    }

    $( ".start_date, #datepicker1" ).datepicker({
      defaultDate: "0w",
      changeMonth: true,
      numberOfMonths: 1,
      minDate: '0',
      dateFormat: "yy-mm-dd",
      clickInput:true,
      onSelect: function( dateText, inst, selectedDate ) {
        if( $(this).val() != "" ){
            $(this).removeClass("error");
            $(this).parent().find('.error-label').remove();
        }
        $(".start_date").val( $(this).val() );
        $(this).addClass("input-changed");
        orange_btn();
        $('.campaign-duration-div').parent().find('.error-label').last().remove();
      }
    });


    $( ".end_date, #datepicker2" ).datepicker({
      defaultDate: new Date($(this).val()),
      changeMonth: true,
      minDate: '+1d',
      numberOfMonths: 1,
      dateFormat: "yy-mm-dd",
      clickInput:true,
      onSelect: function( dateText, inst, selectedDate ) {
        if( $(this).val() != "" ){
            $(this).removeClass("error");
            $("#datepicker1").parent().find('.error-label').remove();
        }
        var currentDate = new Date( $(this).val());
        var months = [ "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec" ];
        var twoDigitDate=((currentDate.getDate())>=10)? (currentDate.getDate()) : '0' + (currentDate.getDate());
        var createdDateTo = months[currentDate.getMonth()]+" "+twoDigitDate+", "+currentDate.getFullYear(); 
        $(".camp-last-day").text( createdDateTo );
        $(".end_date").val( $(this).val() );
        $(this).addClass("input-changed");
        orange_btn();
        $('.campaign-duration-div').parent().find('.error-label').last().remove();
      }
    });

    $('#singleFieldTags2').tagit({
        availableTags: sampleTags
    });

    /* var handle = $( "#custom-handle" ); */
    $( "#slider-range-min" ).slider({
      range: "min",
      value: getcampaignGoal(),
      min: 0,
      max: 15000,
     /*  create: function() {
        handle.text("$" + $( this ).slider( "value" ) + ".00");
      },  */
      slide: function( event, ui ) {
        var sliderval = numberWithCommas( Number( ui.value).toFixed(2) );
        $(".camp-goal-max").html().replace(/[,$]/g , '');
        $('#amount').val().replace(/[,$]/g , '');
        $('#input-goal').val().replace(/[,$]/g , '');
        $( "#amount, #input-goal" ).val( "$" + sliderval );
        $(".camp-goal-max").html("$" + sliderval );
        $( "#amount, #input-goal" ).addClass("input-changed");
        orange_btn();
        $('#amount').parent().find('.error-label').last().remove();
        $('#amount').removeClass("error");
        $('#input-goal').parent().find('.error-label').last().remove();
        $('#input-goal').removeClass("error"); 
        /* handle.text("$" + ui.value + ".00"); */
      }
    });

    $(document).on("mouseover",".q-icon", function(){
        $(this).find(".q-pop").addClass("q-pop-show");
    });
    
    $(document).on("mouseout",".q-icon", function(){
        $(this).find(".q-pop").removeClass("q-pop-show");
    });
    
    $(document).on("keyup", "#amount", function(e) {
        if(Number($(this).val())){
            $("#slider-range-min").slider("value",this.value);   
        } 
         if ( e.keyCode == '37' || e.keyCode == '38' || e.keyCode == '39' || e.keyCode == '40' || e.keyCode == '13' ) {
             return false;
        }else{
            return true;
            orange_btn(); 
            $( this ).addClass("input-changed");
        }               
    });

    $(document).on("keyup", "#campaign_title", function() {
        var str = $(this).val().replace(/[^a-zA-Z0-9]/g,'-');
       
        if( $("#campaign_url").val() == "" ){
            $("#campaign_url").val( str );
        }              
    });

     $(document).on("blur", "#campaign_title", function() {
        var str = $(this).val().replace(/[^a-zA-Z0-9]/g,'-');
        var  campaignURL = $("#campaign_url").val();
        if( campaignURL == "" ){
            $("#campaign_url").val( str );
            if ( campaignURL.length > 3 ) { checkURL( campaignURL );    }
        }        
    });

    $(document).on("blur", "#amount", function() {
        var val = 0;
        var thisVal = $(this).val().replace(/[,$]/g , '');
        var progressbar = $( "#slider-range-min" ).slider( "value", Number(thisVal) );
        if( thisVal >= 15000){
            val = 15000;
        }else{
            val = thisVal;
        }

        $(this).val( "$" + numberWithCommas( Number( val ).toFixed(2) ) );             
        $("#input-goal").val( "$" + numberWithCommas( Number( val ).toFixed(2) ) ); 
        $(".camp-goal-max").html("$" + numberWithCommas( Number( val ).toFixed(2) ) ); 
        $(this).parent().find('.error-label').last().remove();
        $(this).removeClass("error");           
    });

    $(document).on("mouseover", ".sidebar-loadrail", function() {  
         $('.p-hover').show();    
    });

    $(document).on("mouseout", ".sidebar-loadrail", function() {      
        $('.p-hover').css("display","none");
    });

    $(document).on("keyup", "#input-goal", function(e) {
        var thisVal = $(this).val().replace(/[,$]/g , '');
        if(Number(thisVal)){
            $(".camp-goal-max").html("$"+ numberWithCommas( Number( thisVal ).toFixed(2) ) );
            $("#slider-range-min").slider("value", Number(thisVal) );   
        }           
        
        if ( e.keyCode == '37' || e.keyCode == '38' || e.keyCode == '39' || e.keyCode == '40' || e.keyCode == '13' ) {
             return false;
        }else{
            return true;
            orange_btn(); 
            $( this ).addClass("input-changed");
        }  
        $(".error-input-goal").text('');
        $(this).removeClass('error');     
    });

    $(document).on("blur", "#input-goal", function() {
        var val = 0;
        var thisVal = $(this).val().replace(/[,$]/g , '');
        var progressbar = $( "#slider-range-min" ).slider( "value", Number(thisVal) );
        if( thisVal >= 15000){
            val = 15000;
        }else{
            val = thisVal;
        }

        $(this).val('$'+ numberWithCommas( Number(val).toFixed(2) ));
        $(".camp-goal-max").html("$" + numberWithCommas( Number( val ).toFixed(2) ));
        $("#amount").val( "$" + numberWithCommas( Number( val ).toFixed(2) ));    
        orange_btn();  
        $(".error-input-goal").text('');
        $(this).removeClass('error');
    });    

    var replaceWith = $('<input name="temp" type="text" class="form-control campeditor required_div" Placeholder="Campaign Title" maxlength="100" value="'+$("#campaign_title").val()+'"/>'),
    replaceWith2 = $('<textarea class="form-control required_div" rows="5" Placeholder="Create a short description for your campaign. Your visitors will read this first, so be sure to include the most relevant information here.">'+$("#campaign_description").val()+'</textarea>'),
    connectWith = $('input[name="hiddenField"]');
    inlineEdit(replaceWith, connectWith);
    inlineEdit2(replaceWith2, connectWith);

    
    $(document).on('click','.checker-box', function(){
        $(this).toggleClass("selected");
        if( $(this).hasClass("selected") ){         
            $("#next_btn2").attr("disabled", false);
            $("#next_btn2").removeClass("gray-btn");
            $("#next_btn2").addClass("green-btn");
        }else{
            $("#next_btn2").attr("disabled", true);
            $("#next_btn2").removeClass("green-btn");
            $("#next_btn2").addClass("gray-btn");
        }
    });

});


$(document).on("keyup", "#campaign_url", function() {
    var campaignURL = $( this ).val();
    var str = $(this).val().replace(/[^a-zA-Z0-9]/g,'-');
    
    $(this).val( str.toLowerCase() );
    if( $("#campaign_id").val() == "" ){
        if ( campaignURL.length > 3 ) { checkURL( campaignURL );    }
    }else{
        if ( campaignURL.length > 3 ) { check_campaign_URL( campaignURL );    }
    }    
    
});

$(document).on("keyup", ".required, .required2", function(){
    if( $(this).val() != "" ){
        $(this).removeClass("error");
        $(this).parent().find('.error-label').remove();
    }
});

$(document).on("change", "#account_state", function(){
    if( $(this).val() != "" ){
        $(this).removeClass("error");
        $(this).parent().find('.error-label').remove();
    }
});

$(document).on("blur", "#account_zip", function(){
    if( $(this).val().length == 5){
        $(this).removeClass("failed-zip error");
        check_zip();
    }
});

$(document).on("keyup", "#account_zip", function(){
    if( $(this).val().length != 5){
        $(this).addClass("failed-zip error");
    }else{
        $(this).removeClass("failed-zip error");
        check_zip();
    }
});

$(document).on("focusin", ".camp-text", function(){
    $(this).attr('placeholder','');
});

$(document).on("focusout", ".camp-text", function(){
    $(this).attr('placeholder', $(this).attr('data-holder') );
});

$(document).on("click", ".camp-upload-btn", function(){
    $("#cover_photo").click();
});

$(document).on("click", ".camp-remove-cover-btn", function(){
    $('#removecover').modal('show');
});

$(document).on("click", "#remove_cover_confirm", function(){
    remove_cover_photo();
});

$(document).on("click", "#back_btn1", function(){
    window.location.href = get_mainLink()+'campaign/create';
});

$(document).on("click", "#back_btn2", function(){
    first_container();
});

$(document).on("click", "#next_btn1", function(){
   var goal = $("#amount").val();
   var now = new Date();
   var start_date = new Date( $(".start_date").val() );
   var end_date = new Date( $(".end_date").val() );
   var validated = true;   
    required();
    if ( Number( goal.replace(/[,$]/g , '') ) <= 499 ) {
        $("#amount").addClass('error').focus();
        $( '<label  class="error error-label" >Campaign goal should not be less than $500.</label>' ).insertAfter( "#amount" );
        if( $('#amount').parent().find(".error-label").size() > 1 ){
            $('#amount').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }

    if ( start_date > end_date || now > end_date ) {
        $( '<label  class="error error-label" >Start Date is later than End Date.</label>' ).insertAfter( ".campaign-duration-div" );
        if( $('.campaign-duration-div').parent().find(".error-label").size() > 1 ){
            $('.campaign-duration-div').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }

    if( $(".error").size() == 0 ){
        second_container();
    }
});

$(document).on("click", "#next_btn2", function(){
    var end_date = $(".end_date").val();
    var now = new Date();
    var validated = true;
    var re = /[^a-zA-Z0-9]/;
    required2();
    if( re.test( $("#account_add1").val() ) == false ) {
        $( '<label  class="error error-label" >Input a valid address.</label>' ).insertAfter( "#account_add1" );
        if( $('#account_add1').parent().find(".error-label").size() > 1 ){
            $('#account_add1').parent().find('.error-label').last().remove();
        }
       validated = false;
        return false;
    }
    if( $(".error").size() == 0 ){
        $(".next-loader").show(); 
         setTimeout(function() {  
            if( $("#edit_campaign").val() == 0 ){            
                save_campaign_details();
            }else{
                if($("#status").val() == 3 ){
                    // if(end_date > now){
                    //     save_campaign_details_extend();
                    // }else{
                        $("#alertmodal").modal('show');
                        $('.text-confirm').text('Campaign has already ended!');
                        setTimeout(function() {  $(".verify-close").click(); }, 3500);  
                    // } 
                }else{
                    update_campaign_details(); 
                }
                 
            }
        }, 1000);
    }

});

$(document).on("click", ".save-campaign", function() { 
  var goal = $("#amount").val();
  var now = new Date();
  var start_date = new Date( $(".start_date").val() );
  var end_date = new Date( $(".end_date").val() );
  var camp_title = $("#campaign_title").val();
  var camp_url = $("#campaign_url").val();
  var camp_description = $("#campaign_description").val();
  var camp_benefit = $("#campaign_benefit").val();
  var name = $("#account_name").val();
  var add1 = $("#account_add1").val();
  var contact = $("#campaign_contact").val();
  var city = $("#account_city").val();
  var state = $("#account_state").val();
  var zip = $("#account_zip").val();
  var validated = true;

    if( $(".required").val() != 0 ){
        validated = false;
        return false;
    }

    $('.required').each(function(){
        if( $(this).val() == "" ){
            validated = false;
             return false;
        }
    });

    if ( start_date > end_date || now > end_date ) {
        $( '<label  class="error error-label">Start Date is later than End Date.</label>' ).insertAfter( "#datepicker3" );
        if( $('#datepicker3').parent().find(".error-label").size() > 1 ){
            $('#datepicker3').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }else{
        $("#datepicker3").removeClass('error');
        $('#datepicker3').parent().find('.error-label').last().remove();

    }

    $('.required2').each(function(){
        if( $(this).val() == "" ){
            validated = false;
            return false;
        }
    });

    if( $(".error").size() > 0){
        validated = false;
        return false;
    }
    
    if( validated == true ){        
        if( $("#edit_campaign").val() == 0 ){            
            save_campaign_details();
        }else{
            if($("#status").val() == 3 ){
                // if(end_date > now){
                //     save_campaign_details_extend();
                // }else{
                    $("#alertmodal").modal('show');
                    $('.text-confirm').text('Campaign has already ended!');
                    setTimeout(function() {  $(".verify-close").click(); }, 3500);
                // }                
            }else{
                update_campaign_details(); 
            }
             
        }
    }

});

$(document).on("click", ".save-changes", function() { 
    var start_date = new Date( $("#datepicker1").val() );
    var end_date = new Date( $("#datepicker2").val() );
    var now = new Date();
    var goal = $("#amount").val();
    var validated = true;
    required_div();
    if ( start_date > end_date || now > end_date ) {
        if($("#status").val() == 3 ){  $("#alertmodal").modal('show'); $('.text-confirm').text('Campaign has already ended!'); }
        $( '<label  class="error error-label" >Start Date is later than End Date.</label>' ).insertAfter( "#datepicker1" );
        if( $('#datepicker1').parent().find(".error-label").size() > 1 ){
            $('#datepicker1').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }else{
        $("#datepicker1").removeClass('error');
        $('#datepicker1').parent().find('.error-label').last().remove();
    }

    if ( start_date == "Invalid Date") {
        $("#datepicker1").addClass("error").focus();
        $( '<label  class="error error-label" >Start Date is required.</label>' ).insertAfter( "#datepicker1" );
        if( $('#datepicker1').parent().find(".error-label").size() > 1 ){
            $('#datepicker1').parent().find('.error-label').last().remove();
        }        
        validated = false;
        return false;
    }

    if ( end_date == "Invalid Date") {
        $("#datepicker2").addClass("error").focus();
        $( '<label  class="error error-label" >End Date is required.</label>' ).insertAfter( "#datepicker1" );
        if( $('#datepicker1').parent().find(".error-label").size() > 1 ){
            $('#datepicker1').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }

    if ( Number( goal.replace(/[,$]/g , '') ) <= 499 ) {
        $("#input-goal").addClass('error').focus();
        $(".error-input-goal").text('Campaign goal should not be less than $500.');
        validated = false;
        return false;
    }
    

    if( validated == true ){
        if( $("#cover_photo").val() != "" || $("#cover_photo_int").val() == 1 ){
            upload_cover_photo( $("#campaign_url").val() );
        }
        if( $(".input-changed").size() > 0){
            if($("#status").val() == 3 ){
                // if(end_date > now){
                //     save_campaign_details_extend();
                // }else{
                    $("#alertmodal").modal('show');
                    $('.text-confirm').text('Campaign has already ended!');
                    setTimeout(function() {  $(".verify-close").click(); }, 3500);
                // }  
            }else{
                update_campaign_div(); 
            }
        }
        
    }
});

$(document).on("click", ".yeslaunch-btn", function() { 
    var start_date = new Date( $("#datepicker1").val() );
    var end_date = new Date( $("#datepicker2").val() );
    var now = new Date();
    var validated = true;
    required_div();

     $(".publish-loader").show();

    if ( start_date == "Invalid Date") {
        $("#datepicker1").addClass("error").focus();
        $(".publish-loader").hide();
        $( '<label  class="error error-label" >Start Date is required.</label>' ).insertAfter( "#datepicker1" );
        if( $('#datepicker1').parent().find(".error-label").size() > 1 ){
            $('#datepicker1').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }else{
        $("#datepicker1").removeClass('error');
        $('#datepicker1').parent().find('.error-label').last().remove();
    }

    if ( end_date == "Invalid Date") {
        $("#datepicker2").addClass("error").focus();
        $(".publish-loader").hide();
        $( '<label  class="error error-label" >End Date is required.</label>' ).insertAfter( "#datepicker1" );
        if( $('#datepicker1').parent().find(".error-label").size() > 1 ){
            $('#datepicker1').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }else{
        $("#datepicker2").removeClass('error');
        $('#datepicker1').parent().find('.error-label').last().remove();
    }
    
    if ( end_date <= now) {
        $( '<label  class="error error-label" >End date must not less than the current date.</label>' ).insertAfter( "#datepicker1" );
        if( $('#datepicker1').parent().find(".error-label").size() > 1 ){
            $('#datepicker1').parent().find('.error-label').last().remove();
        }
        validated = false;
        return false;
    }else{
        $("#datepicker1").removeClass('error');
        $('#datepicker1').parent().find('.error-label').last().remove();

    }
    if( $(".error").size() > 0){
        validated = false;
        return false;
    }
    if( validated == true ){
        publish();  
    }
});

$(document).on("click", ".update-campaign-details", function() {
    first_container();
    var ajaxURL = get_mainLink()+'campaign/get_campaign_details/'+$("#campaign_id").val();  
    $.get( ajaxURL, function(data){
        var campdetails = JSON.parse(data);
        if ( campdetails.length > 0 ) {
            for ( var x = 0; x < campdetails.length; x++ ) {
                $("#amount").val( campdetails[x].campaign_goal).blur();
                $("#campaign_title").val( campdetails[x].campaign_title);
                $("#campaign_description").val( campdetails[x].campaign_description);
                $("#campaign_benefit").val( campdetails[x].campaign_benefits);
                $(".campaign-keywords").val( campdetails[x].campaign_hashtags);
                $("#account_name").val( campdetails[x].name);
                $("#account_add1").val( campdetails[x].address_1);
                $("#account_add2").val( campdetails[x].address_2);
                $("#campaign_contact").val( campdetails[x].contact_no);
                $("#account_city").val( campdetails[x].city);
                $('account_state option[value="'+campdetails[x].state+'"]').prop("selected", true).change();
                $("#account_zip").val( campdetails[x].zip);
            }
        }
    });     
    $("#setcampdetails").modal('show');
});

$(document).on("click", ".camp-update-btn", function(){
   
    var school_id = $("#school_id").val();
    var parent_id = $(this).attr("data-parent");
    var default_template = $(this).attr("data-id");
    var designDatas = {};
    var allProducts_data =[];
    var temp_id = "";
    var data_price ='';
    var data_designId ='';
    var data_parent ='';
    var product_id ='';

    $(".camp-update-btn").each(function(i){
            var shirt = {};
            var designDatas_details = {};

            data_designId =  $(this).parent().parent().find(".shirt-plan-id").attr("design-id");
            data_parent =  $(this).attr("data-parent");
            product_id =  $(this).attr("product-id");
            temp_id = $(this).attr("data-id");

            $(this).parent().parent().find(".shirt-plan-id").each(function(x){
                designDatas_details[x] = { shirt_style : $(this).attr("plan-id"), color : $(this).attr("data-colors"), id : $(this).attr("plan-id"), colors : $(this).attr("data-colors").split(","), data_price : $(this).attr('data-price'), art_position :  $(this).attr("data-art-position"), canvas_position :  $(this).attr("data-canvas-position") };               
                allProducts_data[i] = { 
                            'parent_id' : data_parent,
                            'template_id' : temp_id,
                            'shirts_and_colors':  designDatas_details,
                            'template_image' : $(this).parent().find(".camp-image-box img").attr("src"),
                            'template_image2' : $(this).parent().find(".camp-image-box img").attr("src"),
                            'properties' : $(this).attr("data-properties"),
                            'back_color' : $(this).attr("background-color"),
                            'hex_value' : $(this).attr("background-color"),
                            'design_id' :  $(this).attr("design-id"),
                            'product_id' :  product_id

                        };
                       
            });
    });

    if(xhr && xhr.readystate != 4){
        xhr.abort();
      }
      xhr =  $.ajax({
          type: 'POST',
          url: get_mainLink()+'template/update_campaign_items',
          data: {
              school : school_id,
              product_details : allProducts_data,
              designs : allProducts_data,
              default_parent : parent_id,
              default_template : default_template
          },
          success: function(data){
              window.location.href = get_mainLink()+data+'/'+$("#campaign_id").val()+'/'+parent_id+'/1';
          },
          async:true
      });
});

$("#cover_photo").on("change", function(e){
    for (var i = 0; i < e.originalEvent.target.files.length; i++) {
        var file = e.originalEvent.target.files[i];
        var file_src = e.originalEvent.srcElement.files[i];
        var img = document.createElement("img");
        var getImagePath = URL.createObjectURL(event.target.files[0]);
        var reader = new FileReader();

        if(e.originalEvent.target.files[i].size > 2097152){
            // 2 MB is 2097152 bytes.
            $("#alertmodal").modal('show');
            $('.text-confirm').text('Image should not exceed 2MB.');  
            $(this).val(''); 
        }else if(e.originalEvent.target.files[i].type != "image/jpeg"){
            $("#alertmodal").modal('show');
            $('.text-confirm').text('Only JPG file type is allowed');
            $(this).val('');
        }else{
            reader.onloadend = function () {
            $('.camp-header-holder').css({"background": "url('"+reader.result+"')", "background-repeat" : "no-repeat", "background-size": "cover", "background-position" : "center", "position" : "relative"});            
            }
            if (file) {
                reader.readAsDataURL(file);
            }
            upload_cover_photo( $("#campaign_url").val() );
        }
    
    }
});

function inlineEdit(replaceWith, connectWith) {
    $(".campaign-title-div").on("click", function(){
        var elem = $(".edit-title");
        $(".edit-lbl1").hide();
        elem.hide();
        elem.after(replaceWith);
        replaceWith.focus();
        replaceWith.blur(function() {

            if ($(this).val() != "") {
                connectWith.val($(this).val()).change();
                elem.text($(this).val());
                $("#campaign_title").addClass("input-changed");
                $("#campaign_title").val( $(this).val() );
                orange_btn();
            }else{
                $(this).val( $(".camp-sidebar-title").text() );
            }

            $(this).remove();
            elem.show();
            $(".edit-lbl1").show();
        });
    });
}


function inlineEdit2(replaceWith2, connectWith) {
    $(".campaign-des-p").on("click", function(){
        var elem = $(".edit-desc");
        $(".edit-lbl2").hide();
        elem.hide();
        elem.after(replaceWith2);
        replaceWith2.focus();
        replaceWith2.blur(function() {

            if ($(this).val() != "") {
                connectWith.val($(this).val()).change();
                elem.text($(this).val());
                $("#campaign_description").addClass("input-changed");
                $("#campaign_description").val( $(this).val() );
                orange_btn();
            }else{
                $(this).val( $(".camp-sidebar-p").text() );
            }

            $(this).remove();
            elem.show();
            $(".edit-lbl2").show();
        });
    });
}

function checkURL( textValue ) {
    var campUrl = get_mainLink()+'campaign/check_url/';
    campUrl += textValue;
    if( textValue == 'campaign'){
        $("#campaign_url").addClass('failed-url error');
        $( '<label  class="error error-label" >This url is not available.</label>' ).insertAfter( ".failed-url" );
        if( $('#campaign_url').parent().find(".error-label").size() > 1 ){
            $('#campaign_url').parent().find('.error-label').last().remove();
        }
    }else{
        $.post(campUrl, function(data){
            var result = JSON.parse(data); 
            if( result['status'] == 'exist'){
                $( "#campaign_url").addClass("failed-url error");
                $( '<label  class="error error-label" >This url is not available.</label>' ).insertAfter( ".failed-url" );
                if( $('#campaign_url').parent().find(".error-label").size() > 1 ){
                    $('#campaign_url').parent().find('.error-label').last().remove();
                }
            }else{
                $( "#campaign_url").removeClass("failed-url error");
                $('#campaign_url').parent().find('.error-label').remove();
            }
        });
    }
    
}

function check_camp_URL( textValue ) {
    var campUrl = get_mainLink()+'campaign/check_campaign_url/';
    campUrl += textValue;
    if( textValue == 'campaign'){
        $("#campaign_url").addClass('failed-url error');
        $( '<label  class="error error-label" >This url is not available.</label>' ).insertAfter( ".failed-url" );
        if( $('#campaign_url').parent().find(".error-label").size() > 1 ){
            $('#campaign_url').parent().find('.error-label').last().remove();
        }
    }else{
        $.post(campUrl, { campaign_id: $("#campaign_id").val() }, function(data){
            var result = JSON.parse(data); 
            if( result['status'] == 'exist'){
                $( "#campaign_url").addClass("failed-url error");
                $( '<label  class="error error-label" >This url is not available.</label>' ).insertAfter( ".failed-url" );
                if( $('#campaign_url').parent().find(".error-label").size() > 1 ){
                    $('#campaign_url').parent().find('.error-label').last().remove();
                }
            }else{
                $( "#campaign_url").removeClass("failed-url error");
                $('#campaign_url').parent().find('.error-label').remove();
            }
        });
    }
}

function check_zip(){
    var ajaxURL = get_mainLink()+'account/get_zip/';            
        $.post( ajaxURL, {
            zip_code: $('#account_zip').val(),
            state: $("#account_state").val()
          }, function(data){        
            if( data == "no-data"){
                $('#account_zip').addClass("failed-zip error");
                $( '<label  class="error error-label" >Input a Valid Zip Code.</label>' ).insertAfter( '.failed-zip' );
                if( $('#account_zip').parent().find(".error-label").size() > 1 ){
                    $('#account_zip').parent().find('.error-label').last().remove();
                }
            }else{
                $('#account_zip').removeClass("failed-zip error");
                $('#account_zip').parent().find('.error-label').remove();
            }
        });
}

function first_container(){ 
    $(".setup-cont").show();
    $(".payeedetail-cont").hide();
}

function second_container(){    
    $(".setup-cont").hide();
    $(".payeedetail-cont").show();
}

function save_campaign_details(){
    var ajaxURL = get_mainLink()+'campaign/save_campaign_details/';
    var goal = $("#amount").val();
    var start_date = $(".start_date").val();
    var end_date = $(".end_date").val();
    var camp_title = $("#campaign_title").val();
    var camp_url = $("#campaign_url").val();
    var camp_description = $("#campaign_description").val();
    var camp_benefit = $("#campaign_benefit").val();
    var camp_keywords = $(".campaign-keywords").val();
    var email = $("#user_email").val();
    var name = $("#account_name").val();
    var contact = $("#campaign_contact").val();
    var customer_id = $("#customer_id").val();
    var school_id = $("#school_id").val();
    var supplier_code = $("#supplier_code").val();
    
    // lets submit the form
    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
            campaign_user_id: customer_id,
            school_id: school_id,
            campaign_url: camp_url,
            campaign_title: camp_title,
            campaign_benefits: camp_benefit,
            campaign_description: camp_description,
            campaign_hashtags: camp_keywords,
            campaign_goal: goal.replace(/[,$]/g , ''),
            campaign_cover_photo: $("#cover_photo_int").val(),
            campaign_feature: $("#campaign_feature").val(),
            campaign_contact: contact,
            campaign_email: email,
            campaign_account_name: name,
            start_date: start_date,
            end_date: end_date,
            campaign_status: $("#status").val(),
            supplier_code: $("#supplier_code").val()
      },success: function( data ){
        var result = JSON.parse( data );
        if(result['status'] == 'exist'){
            first_container();
            $("#campaign_url").addClass('failed-url error');
            $(".next-loader").hide();
            $( '<label  class="error error-label" >This url is not available.</label>' ).insertAfter( ".failed-url" );
            if( $('#campaign_url').parent().find(".error-label").size() > 1 ){
                $('#campaign_url').parent().find('.error-label').last().remove();
            }
        }else{
            $("#next_btn2").attr("disabled", true);
            save_design_details(result['status']);
            save_payee_details(result['status']);
        }

      },                 
      async:false
    });
}

function save_campaign_details_extend(){
    var ajaxURL = get_mainLink()+'campaign/save_campaign_details_extend/'+$("#campaign_id").val();
    var goal = $("#amount").val();
    var start_date = $(".start_date").val();
    var end_date = $(".end_date").val();
    var camp_title = $("#campaign_title").val();
    var camp_url = $("#campaign_url").val();
    var camp_description = $("#campaign_description").val();
    var camp_benefit = $("#campaign_benefit").val();
    var camp_keywords = $(".campaign-keywords").val();
    var email = $("#user_email").val();
    var name = $("#account_name").val();
    var contact = $("#campaign_contact").val();
    var customer_id = $("#customer_id").val();
    var school_id = $("#school_id").val();
    var supplier_code = $("#supplier_code").val();

    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
            campaign_user_id: customer_id,
            school_id: school_id,
            campaign_url: camp_url,
            campaign_title: camp_title,
            campaign_benefits: camp_benefit,
            campaign_description: camp_description,
            campaign_hashtags: camp_keywords,
            campaign_goal: goal.replace(/[,$]/g , ''),
            campaign_cover_photo: $("#cover_photo_int").val(),
            campaign_feature: $("#campaign_feature").val(),
            campaign_contact: contact,
            campaign_email: email,
            campaign_account_name: name,
            start_date: start_date,
            end_date: end_date,
            campaign_status: $("#status").val(),
            supplier_code: $("#supplier_code").val()
      },success: function( data ){
        var result = JSON.parse( data ); 
            $("#next_btn2").attr("disabled", true);
            save_design_details_extended(result['status']);
            save_payee_details(result['status']);
      },                 
      async:false
    });
}

function save_design_details(camp_ID){
    var customer_id = $("#customer_id").val();
    var camp_title = $("#campaign_title").val();
    var email = $("#user_email").val();

    var ajaxURL = get_mainLink()+'campaign/save_design_details/'+camp_ID;
     $(".update-btn").each(function(i){
        designDatas[i] = { product_id : $(this).attr("product-id") };
    });
    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
        designs: designDatas,
        campaign_user_id: customer_id,
        campaign_title: camp_title,
        campaign_email: email
      },success: function( data ){
        var result = JSON.parse( data );        

      },                 
      async:false
    });
}

function save_design_details_extended(camp_ID){
    var customer_id = $("#customer_id").val();
    var camp_title = $("#campaign_title").val();
    var email = $("#user_email").val();

    var ajaxURL = get_mainLink()+'campaign/save_design_details_extended/'+camp_ID;
     $(".update-btn").each(function(i){
        designDatas[i] = { product_id : $(this).attr("product-id") };
    });
    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
        designs: designDatas,
        campaign_user_id: customer_id,
        campaign_title: camp_title,
        campaign_email: email
      },success: function( data ){
        var result = JSON.parse( data );  
      },                 
      async:false
    });
}

function update_campaign_details(){ 
    var goal = $("#amount").val();
    var start_date = $(".start_date").val();
    var end_date = $(".end_date").val();
    var camp_title = $("#campaign_title").val();
    var camp_url = $("#campaign_url").val();
    var camp_description = $("#campaign_description").val();
    var camp_benefit = $("#campaign_benefit").val();
    var camp_keywords = $(".campaign-keywords").val();
    var email = $("#user_email").val();
    var name = $("#account_name").val();
    var contact = $("#campaign_contact").val();
    var customer_id = $("#customer_id").val();
    var school_id = $("#school_id").val();
    var camp_id = $("#campaign_id").val();
    var ajaxURL = get_mainLink()+'campaign/update_campaign_details/'+camp_id;

    $.post( ajaxURL, {
            campaign_user_id: customer_id,
            school_id: school_id,
            campaign_url: camp_url,
            campaign_title: camp_title,
            campaign_benefits: camp_benefit,
            campaign_description: camp_description,
            campaign_hashtags: camp_keywords,
            campaign_goal: goal.replace(/[,$]/g , ''),
            campaign_cover_photo: $("#cover_photo_int").val(),
            campaign_contact: contact,
            campaign_email: email,
            campaign_account_name: name,
            start_date: start_date,
            end_date: end_date,
            campaign_status: $("#status").val()
          }, function(data){ 
          var result = JSON.parse( data );               
        if(result['status'] == 'exist-url'){
            first_container();
            $("#campaign_url").addClass('failed-url error');
            $( '<label  class="error error-label" >This url is not available.</label>' ).insertAfter( ".failed-url" );
            if( $('#campaign_url').parent().find(".error-label").size() > 1 ){
                $('#campaign_url').parent().find('.error-label').last().remove();
            }
        }else{
            $("#next_btn2").attr("disabled", true);
            save_payee_details(camp_id);
        }
   });
}

function update_campaign_div(){ 
    var goal = $("#amount").val();
    var start_date = $("#datepicker1").val();
    var end_date = $("#datepicker2").val();
    var camp_title = $(".edit-title").text();
    var camp_description = $(".edit-desc").text();
    var customer_id = $("#customer_id").val();
    var school_id = $("#school_id").val();
    var camp_id = $("#campaign_id").val();
    var ajaxURL = get_mainLink()+'campaign/update_campaign_div/'+camp_id;

    $.post( ajaxURL, {
            campaign_user_id: customer_id,
            school_id: school_id,
            campaign_title: camp_title,
            campaign_description: camp_description,
            campaign_goal: goal.replace(/[,$]/g , ''),
            campaign_cover_photo: $("#cover_photo_int").val(),
            start_date: start_date,
            end_date: end_date,
            campaign_status: $("#status").val()
          }, function(data){             
            location.reload();
        });
}

function save_payee_details(camp_ID){
    var ajaxURL = get_mainLink()+'campaign/save_payee_details/'+camp_ID;
    var email = $("#user_email").val();
    var name = $("#account_name").val();
    var add1 = $("#account_add1").val();
    var add2 = $("#account_add2").val();
    var contact = $("#campaign_contact").val();
    var city = $("#account_city").val();
    var state = $("#account_state").val();
    var zip = $("#account_zip").val();
    var customer_id = $("#customer_id").val();
    var school_id = $("#school_id").val();

    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
            campaign_user_id: customer_id,            
            campaign_contact: contact,
            campaign_email: email,
            campaign_account_name: name,
            address_1: add1,
            address_2: add2,
            city: city,
            state: state,
            zip: zip
      },success: function( data ){
        var result = JSON.parse( data );
        $("#next_btn2").attr("disabled", true);
        if( result['status'] == 'success'){
            if( $("#campaign_id").val() == "" ){ 
                window.location.href = get_mainLink()+'campaign/edit/'+camp_ID+'/'+customer_id; 
            }else{
                if( $("#status").val() == 1 || $("#status").val() == 0 ){  location.reload();  }else{
                    window.location.href = get_mainLink()+'campaign/edit/'+camp_ID+'/'+customer_id;
                }
            }
        }

      },                 
      async:false
    });
}

function publish(){
    var camp_ID = $("#campaign_id").val();
    var ajaxURL = get_mainLink()+'campaign/publish/'+camp_ID;
    $.post( ajaxURL, function(data){ 
        var result = JSON.parse( data );
        if( result['status'] == 'success'){
            $(".confirmlaunch-cont").hide();
            $(".successlaunch-cont").show();
            $(".publish").hide();
            $("#status").val('1');
        }
        
    });    
}

function upload_cover_photo(slug) {
    var _file = document.getElementById('cover_photo');
   if (_file.files.length === 0) {
        return;
    }
      var data = new FormData();
      data.append('SelectedFile', _file.files[0]);

      var request = new XMLHttpRequest();
      var upldURL = get_mainLink()+'campaign/upload_cover_photo/'+slug;
      request.open( 'POST', upldURL );
      request.send(data);      
        request.onload = function () {
        // do something to response
            var result = JSON.parse( this.responseText );
            $("#alertmodal").modal('show');
            $('.text-confirm').text( result['message'] );
            setTimeout(function() {  $(".verify-close").click(); location.reload(); }, 3500);
        };                  
}

function remove_cover_photo() {
    var slug = $("#campaign_url").val();
    var data = new FormData();
    var request = new XMLHttpRequest();
    var upldURL = get_mainLink()+'campaign/remove_cover_photo/'+slug;
      request.open( 'POST', upldURL );
      request.send(data);      
    request.onload = function () {
    // do something to response
        var result = JSON.parse( this.responseText );
        alert( result['message'] );
        location.reload();
    };                  
}

function required(){
    $(".required").each(function(i){
        if( $(this).val() == "" ){
            $(this).addClass('error');
            $( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' ).insertAfter( this );
            if( $(this).parent().find(".error-label").size() > 1 ){
                $(this).parent().find('.error-label').last().remove();
            }
            
        }           
        
    });
    $(".error").eq(0).focus();  
}

function required2(){
    $(".required2").each(function(i){
        if( $(this).val() == "" ){
            $(this).addClass('error');
            $( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' ).insertAfter( this );
            if( $(this).parent().find(".error-label").size() > 1 ){
                $(this).parent().find('.error-label').last().remove();
            }
        }           
        
    });
    $(".error").eq(0).focus();
}

function required_div(){
    $(".required_div").each(function(i){
        if( $(this).val() == "" ){
            $(this).addClass('error');
            $( '<label  class="error error-label" >This field is required.</label>' ).insertAfter( this );
            if( $(this).parent().find(".error-label").size() > 1 ){
                $(this).parent().find('.error-label').last().remove();
            }
        }           
        
    });
    $(".error").eq(0).focus();
}


