/*
* @desc opens a modal window to display a message
* @param string $msg - the message to be displayed
* @return bool - success or failure
* @author Mike Ibay mike.ibay@tzilla.com
* @required settings.php
*/

var xhrVariableName; // variables to be used for ajax requests
var xhrArtworks; // variables to be used for ajax requests to get all the artworks
var limit = 5;
var offset = 0;
var searchType = 0; // 0 = Default Search, 1 = Search by School, 2 = Search by Mascot, 3 = Search by Most Popular, 4 = Search by Newest
var artworkCount = 0;
var intVariableName = 0;
var strVariableName = ""; // define possible string values here
var mainUrl = ""; // contains the base url
var keyword = "";
var artworksList = ""; // contains html to diplay the artworks for selection
var selected_artworks = "";
var boolVariableName = true; // true or false
var encodeArtwork = [];
var selected_artwork_list = [];

function setMainUrl( value )
{
	mainUrl = value;
}

function getMainUrl()
{
	return mainUrl;
}

function setArtworkCount( value )
{
	artworkCount = value;
}

function getArtworkCount()
{
	return artworkCount;
}

function setSearchLimit( value )
{
	limit = value;
}

function getSearchLimit()
{
	return limit;
}

function setOffset( value )
{
	offset = value;
}

function getOffset()
{
	return offset;
}

function setSearchType( value )
{
	searchType = value;
}

function getSearchType()
{
	return searchType;
}

function setKeyword( value )
{
	keyword = value;
}

function getKeyword()
{
	return keyword;
}

function searchArtworks()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/search',
		data: {
			local : false,
			limit : getSearchLimit(),
			offset : getOffset(),
			searchType : getSearchType(),
			keyword : getKeyword()
		},
		success: function( data ){
			console.log( data );
		},
		async: false
	});
}

function loadAllArtworks()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/search',
		data: {
			local : true
		},
		success: function( data ){
			var nData = JSON.parse( data );

			if ( nData.length > 0 )
			{
				var c_lists = [];
				localStorage.setItem( "art_templates", true );

				for ( var i = 0; i < nData.length; i++ )
				{
					c_lists[i] = {};
					c_lists[i]['id'] = nData[i]['id'];
					// c_lists[i]['encoded'] = nData[i]['encoded'];

					localStorage.setItem( nData[i]['id'], JSON.stringify( nData[i] ) );
					sessionStorage.setItem( "artwork_list", JSON.stringify( c_lists ) );
				}

			}
		},
		async: false
	});
}

function getArtworkCategories()
{
	$.ajax({
		type: 'POST',
		url: getMainUrl()+'artwork/categories',
		success: function( data ){
			var nData = JSON.parse( data );

			displayCategories( nData );
		},
		async: false
	});
}

function displayCategories( data )
{
	if ( data.length > 0 )
	{
		$(".display-result-here").html( "" );

		for ( var i = 0; i < data.length; i++ )
		{
			if ( data[i]['parent_id'] == 0 )
			{
				// display the parent categories
				$(".display-result-here").append( $('<ul />').html( data[i]['name'] ).attr({"id":data[i]['id']}) );
			}
			else
			{
				// display the child categories
				$('.display-result-here ul[id="'+data[i]['parent_id']+'"]').append( $('<li />').html( data[i]['name'] ).attr({"id":data[i]['id']}) );
			}
		}
	}
}

function loadSelectedArtworks()
{
	$(".selected-artworks").html("");

	var x = localStorage.getItem( "selected_artworks" ).split(",");
	
	if ( x.length > 0 )
	{
		for ( var i = 0; i < x.length; i++ ) {
			var n = JSON.parse( localStorage.getItem( x[i] ) );

			selected_artworks += '<img src="'+n['image']+'" />';
		}
	}

	$(".selected-artworks").html( selected_artworks );
}

function removeLocalStorageItem( key )
{
	var a = selected_artwork_list;
	selected_artwork_list.splice( $.inArray( key, a ) , 1 );

	localStorage.setItem( "selected_artworks", selected_artwork_list );
	localStorage.removeItem( key );
}

$(document).ready(function(){

	// use this code to load all default functions
	$(function(){
		loadSelectedArtworks();
	});

	$(document).on("@event", "@element", function(){

	});

	$(document).on("click", ".clear-result", function(){
		$(".display-result-here").html( "" );
	});

	$(document).on("click", ".get-categories", function(){
		getArtworkCategories();
	});

	$(document).on("click", ".search-school", function(){
		setKeyword("letter");
		setSearchType(1);
		searchArtworks();
	});

	$(document).on("click", ".search-mascot", function(){
		setKeyword("base");
		setSearchType(2);
		searchArtworks();
	});

	$(document).on("click", ".search-popular", function(){
		setSearchType(3);
		searchArtworks();
	});

	$(document).on("click", ".search-newest", function(){
		setSearchType(4);
		searchArtworks();
	});

	$(document).on("click", ".art-template", function(){
		var currentArtworkCount = getArtworkCount();

		if ( !$(this).hasClass("selected") )
		{
			if ( currentArtworkCount < 24 )
			{
				encodeArtwork = {};
				encodeArtwork['id'] = $(this).attr("data-id");
				encodeArtwork['image'] = $(this).find("img").attr("src");

				localStorage.setItem( encodeArtwork['id'], JSON.stringify( encodeArtwork ) );
				
				selected_artwork_list.push( encodeArtwork['id'] );
				localStorage.setItem( "selected_artworks", selected_artwork_list );

				currentArtworkCount++;
				setArtworkCount( currentArtworkCount );

				$(this).addClass("selected");
			}
		}
		else
		{
			if ( currentArtworkCount != 0 )
			{
				currentArtworkCount--;
				setArtworkCount( currentArtworkCount );
				
				removeLocalStorageItem( $(this).attr("data-id") );

				$(this).removeClass("selected");
			}
		}

		loadSelectedArtworks();
	});

	$(document).on("click", ".next-page", function(){
		window.location.href = getMainUrl()+"artwork/customize";
	});

	$(document).on("click", ".get-artworks", function(){

		if ( xhrArtworks && xhrArtworks.readystate != 4 )
		{
			xhrArtworks.abort();
		}

		xhrArtworks = $.ajax({
			type: 'POST',
			url: getMainUrl()+'artwork/templates',
			success: function( data ){
				var nData = JSON.parse( data );
				var encodedImage = "";

				if ( nData.length > 0 )
				{
					artworksList = "";

					for ( var i = 0; i < nData.length; i++ ) {
						// art-template class is needed for the trigger of selection for artworks
						artworksList += '<div class="art-template" data-id="'+nData[i]['template_id']+'" data-pid=""><img src="'+nData[i]['thumbnail']+'" /></div>';
					}
				}

				$(".display-result-here").html( artworksList );
			},
			async: false
		});

	});

});