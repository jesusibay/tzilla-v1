// PROPER COMMENT EXAMPLE
/*
* @desc opens a modal window to display a message
* @param string $msg - the message to be displayed
* @return bool - success or failure
* @author Jake Rocheleau jakerocheleau@gmail.com
* @required settings.php
*/

var xhrVariableName; // variables to be used for ajax requests
var intVariableName = 0;
var strVariableName = ""; // define possible string values here : 
var boolVariableName = true; // true or false

function function_name( param1 = "", param2 = 0 ) {

}

$(document).ready(function(){

	$(document).on("@event", "@element", function(){

	});

});