var xhr;
var designData = [];

function setDateStarted( value ) {
  start_date = value;
}

function getDateStarted() {
  var temp_var = new Date( start_date );
  return temp_var;
}

function setDateEnded( value ) {
  ended_date = value;
}

function getDateEnded() {
  var temp_var = new Date( ended_date );
  return temp_var;
}

function setDateToday( value ) {
  today_date = value;
}

function getDateToday() {
  var temp_var = new Date( today_date );
  return temp_var;
}

function doCountDown() {
  var ds = getDateStarted().getTime();
  var de = getDateEnded().getTime();
  var dt = new Date().getTime();
  var rt = ( de - dt ) / 1000;
  var dateend = new Date($('#end_date').val());
  var datestart = new Date($('#start_date').val());

  var remaining_days = parseInt( rt / 86400 );
  var rh = parseInt( rt / 3600 );
  var remaining_hours = ( rh % 24 );
  var rm = parseInt( rt / 60 );
  var remaining_minutes = ( rm % 60 );
  var remaining_seconds = ( parseInt( rt % 60 ) );           
  // $('#camptimer2').countdown({until: '+1d +4h +5m +10s', padZeroes: true});
  $('#camptimer2').countdown({until: '+'+remaining_days+'d +'+remaining_hours+'h +'+remaining_minutes+'m +'+remaining_seconds+'s', padZeroes: true});
}

function checkImg(src){
   var jqxhr = $.get(src, function() {
    return true;
   }).fail(function() { 
    $(".camp-header-holder").attr("style", "");
    return false;
   });
}

$(document).ready(function(){
  checkImg( $("#checkphoto_path").attr("src") );
  $("#storetitle").text('TZilla.com - '+$(".camp-title").text() );

  $(document).on("click", ".buy-btn", function(){
    // var school_id = $("#school_id").val();
    // var parent_id = $(this).attr("data-parent");

    // $(".buy-btn").each(function(i){
    //     designData[i] = { parent_id : $(this).attr("data-parent"), 
    //                       template_id : $(this).attr("data-id"), 
    //                       product_id : $(this).attr("product-id"), 
    //                       style_id : $(this).attr("product-id") };
    // });

    var school_id = $("#school_id").val();
    var parent_id = $(this).attr("data-parent");
    var designDatas = {};
    var allProducts_data =[];
    var temp_id = "";
    var data_price ='';
    var data_designId ='';
    var data_parent ='';
    var product_id ='';

    $(".camp-update-btn").each(function(i){

            // designDatas[i] = { product_id : $(this).attr("data-id") };
            var shirt = {};            
            var designDatas_details = {};

            data_designId =  $(this).parent().parent().find(".shirt-plan-id").attr("design-id");
            data_parent =  $(this).attr("data-parent");
            product_id =  $(this).attr("product-id");
            temp_id = $(this).attr("data-id");

            $(this).parent().parent().find(".shirt-plan-id").each(function(x){
                designDatas_details[x] = { shirt_style : $(this).attr("plan-id"), color : $(this).attr("data-colors"), id : $(this).attr("plan-id"), colors : $(this).attr("data-colors").split(",") , data_price : $(this).attr('data-price'), art_position :  $(this).attr("data-art-position"), canvas_position :  $(this).attr("data-canvas-position") };
                allProducts_data[i] = { 
                            'parent_id' : data_parent,
                            'template_id' : temp_id,
                            'shirts_and_colors':  designDatas_details,
                            'template_image' : $(this).parent().find(".camp-image-box img").attr("src").replace("/200/200", ""),
                            'properties' : $(this).attr("data-properties"),
                            'back_color' : $(this).attr("background-color"),
                            'hex_value' : $(this).attr("background-color"),
                            'design_id' :  $(this).attr("design-id"),
                            // 'art_position' :  $(this).attr("art-position"),
                            // 'canvas_position' :  $(this).attr("canvas-postion"),
                            'product_id' :  product_id

                        };
            });
    });

    if(xhr && xhr.readystate != 4){
        xhr.abort();
      }
      xhr =  $.ajax({
          type: 'POST',
          url: get_mainLink()+'template/generator',
          data: {
              school : school_id,
              product_details : allProducts_data,
              designs : allProducts_data,
              default_parent : parent_id,
              default_template : $(this).attr("data-id")
          },
          success: function(data){
              window.location.href = get_mainLink()+data+'/'+$("#campaign_id").val()+'/'+parent_id;
          },
          async:true
      });
  });

});

$(document).on("mouseover", ".sidebar-loadrail", function() {  
    $('.p-hover').show();    
});

$(document).on("mouseout", ".sidebar-loadrail", function() {      
    $('.p-hover').css("display","none");
});
$(document).on("mouseover",".q-icon", function(){
  $(this).find(".q-pop").addClass("q-pop-show");
});
    
$(document).on("mouseout",".q-icon", function(){
  $(this).find(".q-pop").removeClass("q-pop-show");
});