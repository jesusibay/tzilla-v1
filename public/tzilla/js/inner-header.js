var mainLink = '';
var directlink = '';
var loggedcheck = '';
var shirtItems = "";
var sizeData = [];

function setSizeData( value ) {
  sizeData = value;
}

function getSizeData() {
  return sizeData;
}

function setShirtItems( value ) {
  shirtItems = value;
}

function getShirtItems() {
  return shirtItems;
}

function set_directlink( value ) {
  directlink = value;
}

function get_directlink() {
  return directlink;
}

function set_loggedin( value ) {
  loggedcheck = value;
}

function get_loggedin() {
  return loggedcheck;
}

function set_mainLink( value ) {
  mainLink = value;
}

function get_mainLink() {
  return mainLink;
}

function disableF5(e) { if ((e.which || e.keyCode) == 116) e.preventDefault(); };

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function ucword(str){
  str = str.toLowerCase().replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g, function(replace_latter) { 
    return replace_latter.toUpperCase();
  });  //Can use also /\b[a-z]/g
  return str;  //First letter capital in each word
}

function isNumberKey(evt) {
  var charCode = (evt.which) ? evt.which : evt.keyCode;

  if ( charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57) ) {
    return false;
  } else {
    return true;
  }
}

function populateSpecifictionDetails() {
  var shirtData = getSizeData(); // JSON.parse(getSizeData());
  var sizeIndex = 0;
  var sizeRange = "";
  var previousShirt = '';

  if ( shirtData.length > 0 ) {
    $(".specification-preview").html("");

    for ( var a = 0; a < shirtData.length; a++ ) {
      if ( shirtData[a]['style_name'] != previousShirt ) {
        $("<div />").addClass("col-lg-3 col-md-3 col-sm-6 col-xs-12 display-shirts").attr({"data-id":shirtData[a]['id'],"style-id":shirtData[a]['shirt_style_id'],"data-style":"0","data-category":shirtData[a]['shirt_group_id']}).css("min-height","500px").appendTo(".specification-preview").append($("<div />").addClass("design-item-holder camp-item cont-item").append($("<img />").attr({"class":"img-responsive img-center shirt-col","src":get_mainLink()+shirtData[a]['thumbnail'],"alt":""})).append($("<a />").attr({"class":"shirt-name-link","href":"#"}).append($("<h3 />").addClass("gregular font-medium-2 gray-darker shirt-title").text(shirtData[a]['style_name'])).append($("<span />").text(sizeRange)))).append($("<div />").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12").append($("<button />").attr({"data-toggle":"modal","data-target":"#sizechart","data-dismiss":"modal","class":"orderconf-btn green-btn white gsemibold font-small view-size-chart"}).text("View Size Chart")));
      }

      previousShirt = shirtData[a]['style_name'];
    }
  }
}

$(document).ready(function(){
  var owl = $('.owl-carousel');
      owl.owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'flipInY',
      addClassActive: true,
      items:1,
      loop:false,
      nav:false,
      margin:10,
      autoplay:false,
      autoplayHoverPause:true,
      responsiveClass:true,
      responsive:{
        0:{
          items:2
        },
        991:{
          items:2
        },
        1000:{
          items:2
        }
      }
  });


  $('input').bind('input', function() { 
    $(this).addClass('input-changed');
  });

  if( $("#resetpassword_userID").val() == 1){
    $('#loginmodal').modal({
            backdrop: 'static',
            keyboard: false
        });
        $(".login-box").hide();
    $(".reg-box").hide();
    $(".forgotpass-box").hide();
    $(".account-activate").hide();
    $(".success-register").hide();
    $(".enterpass-box").show();
    $(".header-title").html("Reset your password");
  }

  $(document).on("click", ".design-variation-prev", function(){
    $(".owl-prev").click();
  });

  $(document).on("click", ".design-variation-next", function(){
    $(".owl-next").click();
  });

  $(document).on("click", ".login-close", function(){
    $(".text-field").each(function(i){
      $(this).val('');
      $(this).blur();
    });
    $("#loginmodal").modal('hide');
  });

  $(document).on("click", ".sign-in, .user-icon-header", function(){
    $('#loginmodal').modal('show');
    $(".login-box").show();
    $(".reg-box").hide();
    $(".forgotpass-box").hide();
    $(".enterpass-box").hide();
    $(".account-activate").hide();
    $(".success-register").hide();
    $(".buy-create").hide();
    $(".login-req").each(function(i){
      $(this).val("");
      $(this).parent().find(".log-error-label").remove();      
      $(this).removeClass("error");      
    });
  });  

  $(document).on("click", ".reg-goback", function(){
    $(".text-field").each(function(i){
      $(this).removeClass('error');
      $(this).val('');
      $(this).parent().find('.error-label').remove();
    });
    grecaptcha.reset();
  });  
  
  $(document).on("click", ".design-cont-btn", function(){
    if($(".super-custom-container").hasClass("scc-show")){
      $(".super-custom-btn").click();
    }
    $(this).toggleClass("dc-open");
    $(this).toggleClass("dc-close");
    $(".design-container").toggleClass("design-cont-open");
    
    if($(".design-container").hasClass("design-cont-open")){
      $(".slider-overlay").fadeIn();
    }else {
      $(".slider-overlay").fadeOut();
    }
  });
  
  $(document).on("click", ".reg-btn", function(){
    $(".login-box").hide();
    $(".reg-box").show();
    $(".forgotpass-box").hide();
    $(".enterpass-box").hide();
    $(".account-activate").hide();
    $(".success-register").hide();
    $(".header-title").html("Please Log in or Register to Continue");
  });
  
  $(document).on("click", ".forgotpass", function(){
    $(".login-box").hide();
    $(".reg-box").hide();
    $(".forgotpass-box").show();
    $(".enterpass-box").hide();
    $(".account-activate").hide();
    $(".success-register").hide();
    $(".header-title").html("Forgot Your Password?");
  });

  $(document).on("click", ".reg-goback", function(){
    $(".login-box").show();
    $(".reg-box").hide();
    $(".forgotpass-box").hide();
    $(".enterpass-box").hide();
    $(".account-activate").hide();
    $(".success-register").hide();
    $(".header-title").html("Please Log in or Register to Continue");
  });
  
  $(document).on("click", "#forgot_password_btn", function(){
    if( $("#forgotemail").val() == "" ){
        $(".sent-notif").html('Email is required!');
        $(".sent-notif").removeClass("green");
        $(".sent-notif").addClass("red");
    }else{
        $(this).attr("disabled", true); 
        if( $(".email-fail").size() > 0){
          $(this).attr("disabled", false); 
          $(".email-fail").eq(0).focus();
        }else{          
          sent_email_forgot();
        }
    }        
  });

  $(document).on("click", ".success-close", function(){
    sendDetailsToCampaign();
  }); 

  $(document).on("click", "#forgot_password_confirm_btn", function(){
    require_forgotpass();
    if( $(".error-val").size() > 0){
      $(".error").eq(0).focus();
      $(".password-notif").html( ( $(".error-val").size() > 1 ) ? 'Fields are Required!' :  $(".error-val").eq(0).attr("data-name")+' is required!' );
      $(".password-notif").removeClass("green");
      $(".password-notif").addClass("red");
    }else{
      if( $("#forgot_newpassword_text").val() != $("#forgot_conpassword_text").val() ){
        $(".password-notif").html('Passwords do not match.');
        $(".password-notif").removeClass("green");
        $(".password-notif").addClass("red");
        $("#forgot_newpassword_text, #forgot_conpassword_text").addClass("error");
      }else{
        if( $("#forgot_newpassword_text").val().length < 6){
          $(".password-notif").html("Password must be at least 6 characters.");
          $("#forgot_newpassword_text, #forgot_conpassword_text").addClass("error");
          $(".password-notif").removeClass("green");
          $(".password-notif").addClass("red");
        }else{
          $(".password-notif").html('');
          $(".password-notif").addClass("green");
          $(".password-notif").removeClass("red");
          $("#forgot_newpassword_text, #forgot_conpassword_text").removeClass("error");
          $(this).attr("disabled", true);
          forgot_password();
        }
      }

    }  
  }); 
  
});

$(document).on("keyup", ".text-input", function(e) {
  if( $(this).val() != "" ){
    $(this).removeClass("input-failed");
  }
  return false;
});

$(document).on("blur", ".text-input", function(e) {
  var input = ucword($(this).val());
  $(this).val( input );
});

$(document).on("keyup", ".handle_enter", function(e) {
    
    if (event.keyCode == '13') {
        $("#"+$(this).attr("data-enter")).click();
        $("#"+$(this).attr("data-enter")).focus();
    }
    return false;
});

$(document).on("keyup", ".reg-required, .require-forgotpass", function(){
    if( $(this).val() != "" ){
        $(this).removeClass("error");
        $(this).removeClass("error-val");
        $(this).parent().find('.error-label').remove();
        $(".password-notif").html('');
        $(".sent-notif").html('');
    }
});

$(document).on("keyup", ".login-req, .luk-required", function(){
    if( $(this).val() != "" ){
        $(this).removeClass("error");
        $(this).parent().find(".log-error-label").remove();
    }
});

$(document).on("blur", "#username", function(){
    var email = $(this).val();
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    var new_email = email.replace(/\s/g, "");
    $(this).val( new_email.trim() );

    if( $(this).val() != ''){
      if (re.test(email)) {
          $(this).removeClass('email-fail error');
          $(this).parent().find('.log-error-label').last().remove();
      } else{
        $(this).addClass('email-fail error');
        $( '<label  class="error log-error-label" >Input a valid Email!</label>' ).insertAfter( this );
        if( $(this).parent().find(".log-error-label").size() > 1 ){
          $(this).parent().find('.log-error-label').last().remove();
        }   
      }
    }else{
      $(this).removeClass('email-fail error');
     $(this).parent().find('.error-label').last().remove();
    }
});

$(document).on("blur", "#reg_email, #luk_email", function(){
  var email = $(this).val();
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
  var new_email = email.replace(/\s/g, "");
  $(this).val( new_email.trim() );

  if( $(this).val() != ''){
    if (re.test(email)) {
        $(this).removeClass('email-fail error');
        $(this).parent().find('.error-label').last().remove();
    } else{
      $(this).addClass('email-fail error');
      $( '<label  class="error error-label" >Input a valid Email!</label>' ).insertAfter( this );
      if( $(this).parent().find(".error-label").size() > 1 ){
        $(this).parent().find('.error-label').last().remove();
      }   
    }
  }else{
    $(this).removeClass('email-fail error');
   $(this).parent().find('.error-label').last().remove();
  }  
    
});

$(document).on("blur", ".person-name", function(){ // full name
    var name = $(this).val();
    var re = /^(([A-Za-z]+[\-\']?)*([A-Za-z]+)?\s)+([A-Za-z]+[\-\']?)*([A-Za-z]+)?$/;
    $(this).val( name.trim() );

    if( $(this).val() != ''){
      if (re.test(name)) {
          $(this).removeClass('name-fail error');
          $(this).parent().find('.error-label').last().remove();
      } else{
        $(this).addClass('name-fail error');
        $( '<label  class="error error-label" >Input a valid Name!</label>' ).insertAfter( this );
        if( $(this).parent().find(".error-label").size() > 1 ){
          $(this).parent().find('.error-label').last().remove();
        }   
      }
    }else{
      $(this).removeClass('name-fail error');
     $(this).parent().find('.error-label').last().remove();
    }
});

$(document).on("blur", ".name", function(){ // firstname, last name or middle name
    var name = $(this).val();
    var re = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
    $(this).val( name.trim() );

    if( $(this).val() != ''){
      if (re.test(name)) {
          $(this).removeClass('name-fail error');
          $(this).parent().find('.error-label').last().remove();
      } else{
        $(this).addClass('name-fail error');
        $( '<label  class="error error-label" >Input a valid Name!</label>' ).insertAfter( this );
        if( $(this).parent().find(".error-label").size() > 1 ){
          $(this).parent().find('.error-label').last().remove();
        }   
      }
    }else{
      $(this).removeClass('name-fail error');
     $(this).parent().find('.error-label').last().remove();
    }
});

$(document).on("blur", "#forgotemail", function(){
  var email = $(this).val();
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
  var new_email = email.replace(/\s/g, "");
  $(this).val( new_email.trim() );

  if( $(this).val() != ''){
    if (re.test(email)) {
        $(this).removeClass('email-fail error');
        $(".sent-notif").html('');
        $(".sent-notif").addClass("green");
        $(".sent-notif").removeClass("red");
    } else{
      $(this).addClass('email-fail error');  
      $(".sent-notif").html('Input a valid Email!');
      $(".sent-notif").addClass("red");
      $(".sent-notif").removeClass("green"); 
    }
  } 
    
});

$(document).on("keyup", "#forgotemail", function(){
  if( $(this).val() != ''){
    $(".sent-notif").html('');
    $(".sent-notif").addClass("green");
    $(".sent-notif").removeClass("red");
  }    
});


$(document).on("click", "#register_btn", function(){ // validation
  var firstname = $("#reg_fname").val();
  var lastname = $("#reg_lname").val();
  var email = $("#reg_email").val();
  var username = $("#cust_email").val();
  var pword = $("#reg_pword").val();
  var confirm = $("#reg_con").val();
  var validated = true;
  reg();
  $(".validation-error-reg").text("");
    if( firstname == "" ){
        $("#reg_fname").focus();
        validated = false;
        return false;
    }

    if( lastname == "" ){
        $("#reg_lname").focus();
        validated = false;
        return false;
    }

    if( email == "" ){
        $("#reg_email").focus();
        validated = false;
        return false;
    }

    if( $(".email-fail").length != 0 ){
        $("#reg_email").focus();
        validated = false;
        return false;
    }

    if( $("#reg_pword").val().length < 6 ){
        $("#reg_pword").focus();
        $(".validation-error-reg").text("Password must be at least 6 characters.");       
        validated = false;
        return false;
    }

    if( pword != confirm ){
        $("#reg_con").focus();
        $(".validation-error-reg").text("Passwords do not match.");
        validated = false;
        return false;
    }

    if( grecaptcha.getResponse(tgengregister).length === 0 ){
      $(".validation-error-reg").text( "Please check the the captcha form." );
        setTimeout(function() { $(".validation-error-reg").text(""); }, 3700);
        validated = false;
        return false;
    }

    if( validated == true ){
        $(this).attr("disabled", true);
        $(".register-loader").show();
        register();
    }

});


$(document).on("click", "#like_fb, #follow_twitter, #follow_gplus, #follow_instagram", function(){
    var url = $(this).attr('data-url'); 
    window.open(url,"_blank","top=200,left=350,width=800,height=400");
    return false; 
});

$(document).on("click", "#twitter_share_page, .share-twitter, .live-twitter", function(){
    var url = $(this).attr('data-url');
    var text = $(this).attr('data-text');
    var via = $(this).attr('data-via');
    window.open("https://twitter.com/intent/tweet"+
    "?url="+url+"&text="+text+"&via="+via,"_blank","top=200,left=350,width=800,height=400");
    return false; 
});

$(document).on("click", "#gplus_share_page, .share-gplus, .live-gplus", function(){
    var url = $(this).attr('data-url');
    var text = $(this).attr('data-text');
    var via = $(this).attr('data-via');
    window.open("https://plus.google.com/share"+
    "?url="+url+"&text="+text+"&via="+via,"_blank","top=200,left=350,width=800,height=400");
    return false; 
});

$(document).on("click", "#email_share_page, .live-email, .share-email", function(){
    var url = $(this).attr('data-url');
    var text = $(this).attr('data-text')+' '+url;
    var via = 'support@tzilla.com';
    var sub = 'Check it out!';
    window.open("https://mail.google.com/mail/?view=cm&fs=1"+
    "&to=&su="+sub+"&body="+text+"&bcc="+via,"_blank","top=200,left=350,width=800,height=400");
    return false; 
});

$(document).on("click", "#subscribe_email_btn", function(){
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    var email = $("#subscribe_email_text").val();
      var new_email = email.replace(/\s/g, "");
      $("#subscribe_email_text").val( new_email.trim() );
      $(".footer-email-text-holder").attr("title", "");
      $(".footer-email-text-holder").attr("data-original-title", "");
    if( email != ''){
        if (re.test( $("#subscribe_email_text").val() )) {
          $("#subscribe_email_text").removeClass("error");
            subscribe();
        } else{
          $(".footer-email-text-holder").attr("title", "Input a valid email!");
          $(".footer-email-text-holder").attr("data-original-title", "Input a valid email!");
              $(".footer-email-text-holder").tooltip({placement: 'top',trigger: 'manual'}).tooltip('show');
          $("#subscribe_email_text").focus();
          $("#subscribe_email_text").addClass("error");
          setTimeout(function() {  $("#subscribe_email_text").removeClass("error"); $(".footer-email-text-holder").tooltip({placement: 'top',trigger: 'manual'}).tooltip('hide'); }, 3700);
        }
      
    }else{
      $(".footer-email-text-holder").attr("title", "Email is required!");
      $(".footer-email-text-holder").attr("data-original-title", "Email is required!");
      $(".footer-email-text-holder").tooltip({placement: 'top',trigger: 'manual'}).tooltip('show');
      $("#subscribe_email_text").focus();
       setTimeout(function() {  $(".footer-email-text-holder").tooltip({placement: 'top',trigger: 'manual'}).tooltip('hide'); }, 3700);
    } 
   
});

$(document).on("click", ".resend-verification", function(){
    var ajaxURL = get_mainLink()+'account/resend_verification/';  
    $.post( ajaxURL, function(data){
        $(".for-verification").html('Your Confirmation link Has Been Sent To Your Email Address.');
        setTimeout(function() {  $(".verify-close").click(); }, 3700);
    });    
    
});

$(document).on("click", "#login_btn", function(){
  var ajaxURL = get_mainLink()+'signin/login/';
  var username = $("#username").val();
  var password = $("#password").val();
  var validated = true;
  loginerror();
  if( username == "" ){
        $("#username").focus();
        validated = false;
        return false;
    }

    if( $(".email-fail").length != 0 ){
        $("#username").focus();
        validated = false;
        return false;
    }

    if( password == "" ){
        $("#password").focus();
        validated = false;
        return false;
    }

    if( $("#password").val().length < 6 ){
        $("#password").addClass("error").focus();
        $( '<label  class="error log-error-label" >Password must be at least 6 characters.</label>' ).insertAfter( "#password" );
        if( $("#password").parent().find(".log-error-label").size() > 1 ){
          $("#password").parent().find('.log-error-label').last().remove();
        }
        validated = false;
        return false;
    }
  
  if( validated == true ){
      $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
            username: $("#username").val(), 
            password:$("#password").val()
        },success: function( data ){
          var result = JSON.parse(data); 
            if ( result['status'] == 'success' ) {  
                sendDetailsToCampaign();
            }else if( result['status'] == 'notmatched' ){
              $("#password").addClass("error").focus();
              $( '<label  class="error log-error-label" >'+result['message']+'</label>' ).insertAfter( "#password" );
              if( $("#password").parent().find(".log-error-label").size() > 1 ){
                $("#password").parent().find('.log-error-label').last().remove();
              }              
            }else if( result['status'] == 'exceed' ){
              $("#password").addClass("error").focus();
              $( '<label  class="error log-error-label" >'+result['message']+'</label>' ).insertAfter( "#password" );
              if( $("#password").parent().find(".log-error-label").size() > 1 ){
                $("#password").parent().find('.log-error-label').last().remove();
              }
              setTimeout(function() {   $(".login-box").hide(); $(".reg-box").hide();  $(".forgotpass-box").show(); $(".enterpass-box").hide(); $(".header-title").html("Forgot Your Password?");   }, 2500);            
            }else{
              $("#username").addClass("error").focus();
              $( '<label  class="error log-error-label" >'+result['message']+'</label>' ).insertAfter( "#username" );
              if( $("#username").parent().find(".log-error-label").size() > 1 ){
                $("#username").parent().find('.log-error-label').last().remove();
              }
            }                 
        },                 
      async:true
      });
    }  
});

function register(){
    var ajaxURL = get_mainLink()+'signin/sign_up_from_tgen/';          
    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
            firstname: $("#reg_fname").val(),
            lastname: $("#reg_lname").val(),
            username: $("#reg_email").val(),
            password: $("#reg_pword").val(),
            // recaptcha:grecaptcha.getResponse()
            recaptcha: grecaptcha.getResponse(tgengregister)
      },success: function( data ){
        var result = JSON.parse(data); 
        if( result['status'] == 'exist' ){
          $("#register_btn").attr("disabled", false);
          $(".validation-error-reg").text( result['message'] );
          grecaptcha.reset();
          setTimeout(function() { $(".validation-error-reg").text(""); }, 3700);
        }else if( result['status'] == 'error' ){
          $("#register_btn").attr("disabled", false);
          $(".validation-error-reg").text( result['message'] );
          setTimeout(function() { $(".validation-error-reg").text(""); }, 3700);
        }else{
          $(".header-title").html("Successfully Registered");
          $(".reg-box").hide();
          $(".success-register").show();
          $(".login-close").addClass("success-close");
          $(".reg-val").addClass("green");
          $(".reg-val").removeClass("validation-error-reg");
          $(".register-loader").hide();
          sendDetailsToCampaign();
        }
      },                 
      async:true
    });
}

function sent_email_forgot(){  
    var ajaxURL = get_mainLink()+'signin/sent_email_forgot/';          
    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
            email: $("#forgotemail").val()
      },success: function( data ){
        var result = JSON.parse(data);
        $("#forgotemail").val('');
        if(result['status'] == 'success' ){
            $("#forgot_password_btn").attr("disabled", true);
            $(".sent-notif").removeClass("red");
            $(".sent-notif").addClass("green");  
            $("#forgotemail").val("");
            $(".sent-notif").html(result['message']);
            setTimeout(function() {  location.reload(); }, 3700);
        }else{
            $("#forgot_password_btn").attr("disabled", false);
            $(".sent-notif").removeClass("green");
            $(".sent-notif").addClass("red");  
            $("#forgotemail").focus();
            $(".sent-notif").html(result['message']);
        }
      },                 
      async:true
    });
}

function forgot_password(){
    var ajaxURL = get_mainLink()+'signin/resetpassword/';         
    $.ajax({
      type: 'POST',
      url: ajaxURL,
      data: {
            id: $("#resetpassword_userID").attr("data-id"),
            password: $("#forgot_newpassword_text").val()
      },success: function( data ){
         var result = JSON.parse(data);       
          if(result['status'] == 'success' ){
              $(".text-field").each(function(x){
                $(this).val("");
              });
            $(".password-notif").html(result['message']);
            setTimeout(function() {  location.reload(); }, 3700);
          }
          if(result['status'] == 'error' ){
             $("#forgot_password_confirm_btn").attr("disabled", false);
            $(".password-notif").removeClass("green");
            $(".password-notif").addClass("red");
            $(".password-notif").html(result['message']);
          }
      },                 
      async:true
    });
}

function loginerror(){
    $(".login-req").each(function(i){
        if( $(this).val() == "" ){
            $(this).addClass('error');
            $( '<label  class="error log-error-label" >'+$(this).attr('data-name')+' is required.</label>' ).insertAfter( this );
             if( $(this).parent().find(".log-error-label").size() > 1 ){
                 $(this).parent().find('.log-error-label').last().remove();
            }
        }           
        
    });
    $(".error").eq(0).focus();
}

function reg(){
    $(".reg-required").each(function(i){
        if( $(this).val() == "" ){
            $(this).addClass('error');
             $( '<label  class="error error-label" >'+$(this).attr('data-name')+' is required.</label>' ).insertAfter( this );
             if( $(this).parent().find(".error-label").size() > 1 ){
                 $(this).parent().find('.error-label').last().remove();
            }
        }           
        
    });
    $(".error").eq(0).focus();
}

function require_forgotpass(){
    $(".require-forgotpass").each(function(i){
        if( $(this).val() == "" ){
          $(this).addClass('error error-val');
        }  
    });
    $(".error").eq(0).focus();
}