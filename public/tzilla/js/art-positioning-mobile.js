//ken
var style_id = 0;
var designWidth = 0;
var designHeight = 0;
var dataid = "";
var style_name = "";
var fire72dpi = "";
var designLoc = "FRNT";
var xhr_art_pos;
var tgenMode = "";
var mainUrl = "";

function setMainUrl( value ) {
	mainUrl = value;
}

function getMainUrl() {
	return mainUrl;
}

function setImgWidth( value ) {
	designWidth = value;
}

function getImgWidth() {
	return designWidth;
}

function setImgHeight( value ) {
	designHeight = value;
}

function getImgHeight() {
	return designHeight;
}

function setActiveStyle( value ) {
	style_id = value;
}

function getActiveStyle() {
	return style_id;
}

function setShirtPlan( value ) {
	dataid = value;
}

function getShirtPlan() {
	return dataid;
}

function setActiveStyleName( value ) {
	style_name = value;
}

function getActiveStyleName() {
	return style_name;
}

function setActiveParent( value ) {
	parentID = value;
}

function getActiveParent() {
	return parentID;
}

function setDesignLocation( value ) {
	designLoc = value;
}

function getDesignLocation() {
	return designLoc;
}

function setMode( value ) {
	tgenMode = value;
}

function getMode() {
	return tgenMode;
}

function manipulate_add_to_collection(){
	if( $('.shirt-col-selection:not([style*="display: none"])').size() > 0 ){
		if( $(".shirt-col-selection-active").size() == 0 && $('.shirt-col-selection-active:not([style*="display: none"])').size() == 0){
			disabled_addtocollection();
		}else{
			enabled_addtocollection();
		}						
	}else{
		disabled_addtocollection();
	}
}

function enabled_addtocollection() {
	if( $('.tgen-loader-overlay:not([style*="display: none"])').size() == 0 ){
		var mode = getMode();
		if ( mode == "CAMPAIGN" ) {
			$(".tgen-addtocollection-btn").removeClass('gray-btn orange-btn').addClass('green-btn').attr("disabled", false);
		} else if ( mode == "SUPPORT" ) {
			$(".tgen-addtocollection-btn").removeClass('gray-btn').addClass('orange-btn').attr("disabled", false);
		} else {
			$(".tgen-addtocollection-btn").removeClass('gray-btn').addClass('orange-btn').attr("disabled", false);
		}
	}	
	
}

function disabled_addtocollection() {
	var mode = getMode();
	if ( mode == "CAMPAIGN" ) {
		$(".tgen-addtocollection-btn").removeClass('green-btn').addClass('gray-btn orange-btn').attr("disabled", true);
	} else if ( mode == "SUPPORT" ) {
		$(".tgen-addtocollection-btn").removeClass('orange-btn').addClass('gray-btn').attr("disabled", true);
	} else {
		$(".tgen-addtocollection-btn").removeClass('orange-btn').addClass('gray-btn').attr("disabled", true);
	}
	
}

function printableArea( xdesign = 0, ydesign = 0, wdesign = 0, hdesign = 0, staticW = 0, staticH = 0, percentage = 0, palleteDistance = 0) {//wdesign/hdesign must be in inches
	//Width and Height multiply to percentage to get inches
	var percent = Number(percentage/100);
			console.log("xdesign "+xdesign);
            console.log("ydesign "+ydesign);
            console.log("wdesign "+wdesign);
            console.log("hdesign "+hdesign);
            console.log("staticW "+staticW);
            console.log("staticH "+staticH);
            console.log("percentage "+percentage);
            console.log("palleteDistance "+palleteDistance);

			var newHdesign = Number(getImgHeight()/71.78*percent).toFixed(2);
			var newWdesign = Number(getImgWidth()/71.78*percent).toFixed(2);
            
            console.log("newHdesign "+newHdesign);
            console.log("newWdesign "+newWdesign);
            
			convertiontopercent( xdesign , ydesign , newWdesign, newHdesign, palleteDistance);
		
}


function convertiontopercent(xdesign = 0, ydesign = 0, wsize = 0, hsize = 0, palleteDistance = 0) {
	
	//formula for inches to px
	wsize = Number(wsize*11.80);//10.67 18.493 is contant value to convert inches to px
	hsize = Number(hsize*11.80);//10.67 is contant value to convert inches to px
	var a = $(".ProductView__design_canvas").width();
	var b = $(".ProductView__design_canvas").height();
	//converting px to %
	var c = Number((wsize/a)*100);
	var d = Number((hsize/b)*100);

	//x/y positioning formula for converting mm to px
	var xpos = Number(xdesign*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	xpos = (Number(5.22/100)*xpos);
	xpos = Number((xpos/a)*100);
	
	var ypos = Number(ydesign*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	ypos = (Number(5.22/100)*ypos);
	ypos = Number((ypos/b)*100);
	
	var pd = Number(palleteDistance*3.7795);//3.7795 is contant value to convert mm to px (8.331)
	pd = (Number(5.22/100)*pd);
	pd = Number((pd/b)*100);
	
	if(c > 100) {
		c = 100;
	}else if(d > 100) {
		d = 100;
	}
		if(xpos < 0 ) { 
			xpos = 0;

			palleteYaxis(pd.toFixed(2));
			$(".ProductView__design").css({"width":c.toFixed(2)+"%"});
			$(".ProductView__design").css({"height":d.toFixed(2)+"%"}); 
			$(".ProductView__design").css({"top":ypos.toFixed(2)+"%"});
			$(".ProductView__design").css({"left":xpos.toFixed(2)+"%"});
			$(".ProductView__design").css({"right":"0%"});
		}else {
			
			palleteYaxis(pd.toFixed(2));
			$(".ProductView__design").css({"width":c.toFixed(2)+"%"});
			$(".ProductView__design").css({"height":d.toFixed(2)+"%"}); 
			$(".ProductView__design").css({"top":ypos.toFixed(2)+"%"});
			$(".ProductView__design").css({"left":xpos.toFixed(2)+"%"});
		}
		
}


function palleteYaxis(val) {
	
	if(val == undefined) {
		val = 0;
	}
	console.log("palletezz "+getActiveStyle());
    console.log("palleteDistancezz "+val);
	// condition for specific shirt plans	
	switch ( getActiveStyle() ) {
        case "30":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.7% + "+val+"%)"});
			break;
		case "42":
        case "43":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(22.5% + "+val+"%)"});
			break;
		case "41":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19% + "+val+"%)"});
			break;
		case "40":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(30.6% + "+val+"%)"});
			break;
		case "39":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(31% + "+val+"%)"});
			break;
		case "38":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19.3% + "+val+"%)"});
			break;
		case "37":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(15.8% + "+val+"%)"});
			break;
		case "34":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16% + "+val+"%)"});
			break;
		case "36":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.5% + "+val+"%)"});
			break;
		case "35":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(17.6% + "+val+"%)"});
			break;
		case "33":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(19.2% + "+val+"%)"});
			break;
		case "32":
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16.8% + "+val+"%)"});
			break;
		case "31":
            $(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"calc(16% + "+val+"%)"});
			break;
		
		default:
			$(".ProductView__design_canvas").css({"width":"35.3%","height":"44.7%","top":"19%"});
            console.log("tae");
			break;
	}
}




function replacer(link, style) {
	
	var img = new Image();
	img.onload = function() {
		setImgWidth( this.width );
		setImgHeight( this.height ); 
	
	   setArtPosition( getDesignLocation(), getActiveParent(), getActiveStyle() );

	}
	
	img.src = link;
}

// load data for art positioning from AMM in SCP

function setArtPosition( designLocation, templateID, styleID, camp, styleName) {
	/* if(xhr_art_pos && xhr_art_pos.readystate != 4){
		xhr_art_pos.abort();
	} */

	xhr_art_pos = $.ajax({
		type: 'POST',
		url: apiUrl+'template/load_artwork_management_settings/'+designLocation+'/'+templateID,
		success: function(data){
			var nData = JSON.parse(data);
			//console.log(nData);
			// set artwork position here only
			if ( nData.length > 0 ) {
					if(camp != ""){
						palleteYaxis();
					}
				for ( var x = 0; x <= nData.length; x++ ) {
					//console.log("X "+nData[x]['x_axis'], "Y "+nData[x]['y_axis'], "W "+nData[x]['design_width'], "H "+nData[x]['design_height'], "SW "+nData[x]['staticWidth'], "SH "+nData[x]['staticHeight']);
					
					
					if ( nData[x]['shirt_style_id'] == styleID && nData[x]['is_default'] == 1) {
						
						if(fire72dpi=="firsload"){
							console.log("first");
							convertionFirstload( nData[x]['x_axis'], nData[x]['y_axis'], nData[x]['design_width'], nData[x]['design_height'], nData[x]['tgen_percentage'], nData[x]['palletDistance'] );
							console.log("first load "+ nData[x]['x_axis'], nData[x]['y_axis'], nData[x]['design_width'], nData[x]['design_height'], nData[x]['tgen_percentage'], nData[x]['palletDistance']);
						}else{
							console.log("second");
							if(camp != ""){
								printableArea( nData[x]['x_axis'], nData[x]['y_axis'], nData[x]['design_width'], nData[x]['design_height'], nData[x]['staticWidth'], nData[x]['staticHeight'], nData[x]['tgen_percentage'], nData[x]['palletDistance'] );
							}else {
								printableAreaforCamp(nData[x]['y_axis'], nData[x]['palletDistance'], nData[x]['shirt_style_id'], camp, styleName, templateID);
							}
							
							console.log("print "+nData[x]['x_axis'], nData[x]['y_axis'], nData[x]['design_width'], nData[x]['design_height'], nData[x]['staticWidth'], nData[x]['staticHeight'], nData[x]['tgen_percentage'], nData[x]['palletDistance'], nData[x]['shirt_style_id'], nData[x]['is_default'], camp, styleName);
							
						}
						manipulate_add_to_collection();
						break;
					}

				}
			}
		},
		async:true
	});
}
