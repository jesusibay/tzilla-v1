<html>
<head>
</head>
<body>
<div id="paypal-button"></div>

<script src="https://www.paypalobjects.com/api/checkout.js"></script>

<script>
    paypal.Button.render({

        env: 'sandbox',//'production', // Or 'sandbox'

        client: {
            sandbox:    'AQ3A0Kq2_KpJkg-28QTPVhm-K2iWDsmSJo3Vk7ZDP7D20gtFtlnrOE5giyg2D1j_VZ2PiiyGHIwebv45',
            production: 'xxxxxxxxx'
        },

        commit: true, // Show a 'Pay Now' button

        payment: function(data, actions) {
            console.log(data);
            return actions.payment.create({
                payment: {
                    transactions: [
                        {
                            amount: { total: '10000.00', currency: 'USD' }
                        }
                    ]
                }
            });
        },

        onAuthorize: function(data, actions) {
            //console.log(data);
            return actions.payment.execute().then(function(payment) {
console.log(payment);
                // The payment is complete!
                // You can now show a confirmation message to the customer
            });
        }

    }, '#paypal-button');
</script>
</body>
</html>