<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';



$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

require_once( BASEPATH .'database/DB'. EXT );
$db =& DB();


/*set the current site accessed based on the domain name*/
$query = $db->get_where( 'scp_stores', array('is_active'=>1, 'store_domain' => $_SERVER['HTTP_HOST']));
$row = $query->row();

//print_r($row);
if (isset($row))
{
	define('STORE',$row->store_name);
    define('STOREID',$row->store_id);
	define('SITEMODE',$row->site_mode);

}else{
	define('STORE','scp_admin');
	define('STOREID','1');
    define('SITEMODE','DEV');
}

$route['product/(:any)'] = 'product_lookup/show/$1';
$route['catalog/(:any)'] = 'catalog_lookup/show/$1';

$q = $db->get_where( 'scp_app_routes', array('store_id'=>STOREID));
$result = $q->result();
foreach ( $result as $row )
{	
	// echo $row->slug;

    $route[ $row->slug ]                 = $row->controller;
    $route[ $row->slug.'/([^/]*)/([^/]*)/(.*)' ]         = $row->controller;
    $route[ $row->slug.'/:any' ]         = $row->controller;
    $route[ $row->slug.'/:any/:any' ]         = $row->controller;
    $route[ $row->controller ]           = 'error404';
    $route[ $row->controller.'/:any' ]   = 'error404';

    if( $row->slug == 'slug' ){
        $route['404_override'] = 'campaign/slug/';
    }

}

$route['upload'] = 'Upload'; // added for uploading files



