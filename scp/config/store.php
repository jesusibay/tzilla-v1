<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set('America/Los_Angeles');

$config['smtp_host'] = '';
$config['smtp_user'] = '';
$config['smtp_pass'] = '';
$config['smtp_port'] = '';
$config['smtp_timeout'] = '';

$config['image_display_size'] = '400/400';
$config['erp_api_create_customer'] = '';
$config['erp_api_create_supplier'] = '';
$config['erp_api_create_sales_order'] = '';

$config['send_email_to_design_team'] = array('giancarlo.santiago@tzilla.com');
// $config['artwork_statuses'] = array('New', 'For QA Testing', 'QA Failed', 'For UAT Testing', 'UAT Failed', 'Passed', 'On-Hold');

$config['artwork_statuses'] = array( 'NEW', 'FOR QA TESTING', 'QA FAILED', 'FOR UAT TESTING', 'UAT FAILED', 'PASSED', 'ON-HOLD' );
// $config['artwork_statuses'] = array( 'NEW', 'FOR QA TESTING', 'QA FAILED', 'FOR UAT TESTING', 'UAT FAILED', 'PASSED', 'ON-HOLD', 'CHILD' );
	
// $config['weight_per_shirt'] = '8.5';
$config['weight_per_shirt'] = array(
								'3930R' => array(
									"S" => "0.35",
									"M" => "0.4",
									"L" => "0.45",
									"XL" => "0.5",
									"2XL" => "0.55",
									"3XL" => "0.6"
								),
								'363MR' => array(
									"S" => "0.35",
									"M" => "0.4",
									"L" => "0.45",
									"XL" => "0.5",
									"2XL" => "0.55",
									"3XL" => "0.6"
								),
								'29MR' => array(
									"S" => "0.35",
									"M" => "0.4",
									"L" => "0.45",
									"XL" => "0.5",
									"2XL" => "0.55",
									"3XL" => "0.6"
								),
								'11730' => array(
									"S" => "0.35",
									"M" => "0.4",
									"L" => "0.45",
									"XL" => "0.5",
									"2XL" => "0.55",
									"3XL" => "0.6"
								),
								'1301' => array(
									"S" => "0.35",
									"M" => "0.4",
									"L" => "0.45",
									"XL" => "0.5",
									"2XL" => "0.55",
									"3XL" => "0.6"
								),
								'SF45R' => array(
									"S" => "0.3",
									"M" => "0.35",
									"L" => "0.4",
									"XL" => "0.45",
									"2XL" => "0.5",
									"3XL" => "0.55"
								),
								'SFLR' => array(
									"S" => "0.45",
									"M" => "0.5",
									"L" => "0.55",
									"XL" => "0.6",
									"2XL" => "0.6",
									"3XL" => "0.7"
								),
								'M2600A' => array(
									"S" => "1.2",
									"M" => "1.35",
									"L" => "1.4",
									"XL" => "1.55",
									"2XL" => "1.6",
									"3XL" => "1.75"
								),
								'201' => array(
									"S" => "1.2",
									"M" => "1.35",
									"L" => "1.4",
									"XL" => "1.55",
									"2XL" => "1.6",
									"3XL" => "1.75"
								),
								'M2450A' => array(
									"S" => "0.85",
									"M" => "0.95",
									"L" => "1",
									"XL" => "1.15",
									"2XL" => "1.25",
									"3XL" => "1.3"
								),
								'103' => array(
									"S" => "0.85",
									"M" => "0.95",
									"L" => "1",
									"XL" => "1.15",
									"2XL" => "1.25",
									"3XL" => "1.3"
								),
								'1510' => array(
									"XS" => "0.25",
									"S" => "0.25",
									"M" => "0.25",
									"L" => "0.3",
									"XL" => "0.3",
									"2XL" => "0.3"
								),
								'1540' => array(
									"XS" => "0.25",
									"S" => "0.25",
									"M" => "0.25",
									"L" => "0.25",
									"XL" => "0.3",
									"2XL" => "0.3"
								),
								'1533' => array(
									"XS" => "0.2",
									"S" => "0.2",
									"M" => "0.25",
									"L" => "0.25",
									"XL" => "0.25",
									"2XL" => "0.25"
								),
								'3930BR' => array(
									"XS" => "0.25",
									"S" => "0.25",
									"M" => "0.3",
									"L" => "0.3",
									"XL" => "0.35"
								),
								'363BR' => array(
									"XS" => "0.25",
									"S" => "0.25",
									"M" => "0.3",
									"L" => "0.3",
									"XL" => "0.35"
								),
								'29BR' => array(
									"XS" => "0.25",
									"S" => "0.25",
									"M" => "0.3",
									"L" => "0.3",
									"XL" => "0.35"
								),
								'65900' => array(
									"XS" => "0.25",
									"S" => "0.25",
									"M" => "0.3",
									"L" => "0.3",
									"XL" => "0.35"
								),
								'4930BR' => array(
									"XS" => "0.3",
									"S" => "0.3",
									"M" => "0.35",
									"L" => "0.4",
									"XL" => "0.45"
								),
								'5400B' => array(
									"XS" => "0.3",
									"S" => "0.3",
									"M" => "0.35",
									"L" => "0.4",
									"XL" => "0.45"
								),
								'Y2600' => array(
									"XS" => "0.65",
									"S" => "0.7",
									"M" => "0.8",
									"L" => "0.9",
									"XL" => "0.85"
								),
								'303' => array(
									"XS" => "0.65",
									"S" => "0.7",
									"M" => "0.8",
									"L" => "0.9",
									"XL" => "0.85"
								),
								'3710' => array(
									"XS" => "0.15",
									"S" => "0.15",
									"M" => "0.2",
									"L" => "0.2",
									"XL" => "0.25"
								),
								'T3930R' => array(
									"2T" => "0.15",
									"3T" => "0.2",
									"4T" => "0.2"
								)
							);

$config['weight_units'] = array( 
							array('unitcode'=> 'T', 'unitname'=>'Tonne'),
							array('unitcode'=> 'KG', 'unitname'=>'Kilogram'),
							array('unitcode'=> 'G', 'unitname'=>'Gram'),
							array('unitcode'=> 'MG', 'unitname'=>'Miligram'),
							array('unitcode'=> 'OZ', 'unitname'=>'Ounce'),
							array('unitcode'=> 'LB', 'unitname'=>'Pound')
						);
$config['dimentional_units'] = array( 
							array('unitcode'=> 'M', 'unitname'=>'Meter'),
							array('unitcode'=> 'CM', 'unitname'=>'Centimeter'),
							array('unitcode'=> 'IN', 'unitname'=>'Inch'),
							array('unitcode'=> 'YD', 'unitname'=>'Yard'),
							array('unitcode'=> 'FT', 'unitname'=>'Foot')
						);

$config['shirt_sizes'] = array( 
							array('sizeid'=> '4', 'sizename'=>'4', 'sizearrange'=>'1'),
							array('sizeid'=> '5/6', 'sizename'=> '5/6', 'sizearrange'=>'2'),
							array('sizeid'=> '7', 'sizename'=> '7', 'sizearrange'=>'3'),
							array('sizeid'=> 'XXS', 'sizename'=> 'Extra extra Small', 'sizearrange'=>'4'),
							array('sizeid'=> 'XS', 'sizename'=> 'Extra Small', 'sizearrange'=>'5'),
							array('sizeid'=> 'S', 'sizename'=> 'Small', 'sizearrange'=>'6'), 
							array('sizeid'=> 'M', 'sizename'=> 'Medium', 'sizearrange'=>'7'),
							array('sizeid'=> 'L', 'sizename'=> 'Large', 'sizearrange'=>'8'),
							array('sizeid'=> 'XL', 'sizename'=> 'Extra Large', 'sizearrange'=>'9'),
							array('sizeid'=> '2XL', 'sizename'=> '2Extra Large', 'sizearrange'=>'10'),
							array('sizeid'=> '3XL', 'sizename'=> '3Extra Large', 'sizearrange'=>'11'),
							array('sizeid'=> '4XL', 'sizename'=> '4Extra Large', 'sizearrange'=>'12'),
							array('sizeid'=> '5XL', 'sizename'=> '5Extra Large', 'sizearrange'=>'13'),
							array('sizeid'=> '6XL', 'sizename'=> '6Extra Large', 'sizearrange'=>'14'),
							array('sizeid'=> '7XL', 'sizename'=> '7Extra Large', 'sizearrange'=>'15'),
							array('sizeid'=> '2T', 'sizename'=>'2T', 'sizearrange'=>'16'),
							array('sizeid'=> '3T', 'sizename'=>'3T', 'sizearrange'=>'17'),
							array('sizeid'=> '4T', 'sizename'=>'4T', 'sizearrange'=>'18')
						);

$config['garment_types'] = array( 'Fleece', 'Polyester', 'Cotton' );

$config['printable_locations'] = array(
							array( 'location_code'=> 'FRNT', 'location'=> 'Front' ),
							array( 'location_code'=> 'BACK', 'location'=> 'Back' ),
							array( 'location_code'=> 'LSLV', 'location'=> 'Left Sleeve' ),
							array( 'location_code'=> 'RSLV', 'location'=> 'Right Sleeve' )
						);

$config['campaign_statuses'] = array( '0'=> 'Draft', '1'=> 'Active', '3' => 'Ended' );

//Status for the orders 
$config['order_status'] = array(
								'Order Placed',
								'Open',
								'Processing',
								'Blank Shirts Picked',
								'Queued for Printing',
								'Shirts are Printed',
								'Shirts are Packed',
								'Shipped',
								'Received By Customer',
								'Completed',
								'Order Cancelled',
								'Void');

$config['wepay_checkoutCall'] = array('authorized',
								'captured',
								'cancelled',
								'refunded');

$config['fill_variation'] = array('Solid','Pattern');

// ERP API Credentials
$config['erp_api_customer_code'] = "CUST-000082";
$config['erp_api_username'] = "lamont@gmail.com"; // email
$config['erp_api_password'] = "admin";
$config['tzilla_supplier_code'] = "SUP-000020";