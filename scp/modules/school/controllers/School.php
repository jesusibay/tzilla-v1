<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends MX_Controller {
	
	function __construct() 
	{
		parent::__construct();
		$this->load->module( array('settings', 'email_sender') );
		
		$this->load->model(	array('Schools_model', 'settings/Category_model', 'template/Template_model', 'campaign/Campaigns_model', 'account/Account_model') );
		
		$this->load->library( array('Aauth', 'session', 'email' , 'upload' , 'pagination') );
		
        $this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 	
		date_default_timezone_set('America/Los_Angeles');	
	}

	public function index(){
		
		$data['states'] = $this->Account_model->get_states('236');
		$data['featured_campaign'] = $this->Campaigns_model->get_all_featured_campaigns();


		$this->load->view_store('school-search', $data);


	}

	public function search(){
		
		$data['colors'] = $this->Schools_model->get_all_genColors();
		$data['states'] =  $this->Account_model->get_states('236');
		$data['featured_campaign'] = $this->Campaigns_model->get_all_featured_campaigns();
		$data['trending_campaign'] = $this->Campaigns_model->get_trending_campaign();
		
		
		$this->session->unset_userdata("designs");
		$this->session->unset_userdata("product_details");

		$this->load->view_store('school-search', $data);
	}

	// Mike Codes
	public function store( $schoolId = 7, $cat=0, $page=0, $rows=12 ) { // load this school White Rock Elementary School
		$this->load->helper('textparser');
		if ( $schoolId != 0 ) {
			$this->load->module("Template");
			$min = true;
			// load school data ( mascot, primary, secondary )
			$data['school'] = $this->Schools_model->get_school( $schoolId );
			array_parseToString( $data['school'] );

			// set default color for hex value
			$primary_color = $this->Template_model->get_color_hex( $data['school']->school_primary_color );
			$secondary_color = $this->Template_model->get_color_hex( $data['school']->school_secondary_color );

			if ( !( count( $primary_color ) > 0 ) ) {
				$primary_color = new stdClass();
				$primary_color->hex_value = "282828";
			}

			if ( !( count( $secondary_color ) > 0 ) ) {
				$secondary_color = new stdClass();
				$secondary_color->hex_value = "282828";
			}

			$data['primary_color_hex'] = $primary_color;
			$data['secondary_color_hex'] = $secondary_color;

			$this->session->set_userdata( "school_id", db_toString($data['school']->school_id) );
			$this->session->set_userdata( "school_name", db_toString($data['school']->school_name) );
			$this->session->unset_userdata("product_details");			
			$this->session->unset_userdata("go_back");
			
			// load all artwork categories together wth sub categories
			$data['category_z'] = $this->Category_model->get_all_artwork_parent_categories( );
			$data['category'] = json_encode( $this->Category_model->get_all_artwork_categories() );

			// load all artworks to be shown in the school store
			$data['templates'] = $this->Template_model->get_all_artworks_z( $min, $cat, $rows, $page );
			$count = $this->Template_model->get_all_artworks_z_count( $min, $cat )->num_rows();
			// load school campaigns
			$data['school_campaign'] = $this->Schools_model->get_school_campaign( $schoolId );
			$data['cat_selected'] = $cat;

			$pagedata['base_url']			= base_url('school/store/'.$schoolId.'/'.$cat.'/');
			$pagedata['total_rows']			= $count;
			$pagedata['per_page']			= $rows;
			$pagedata['uri_segment']		= 5;
			$pagedata['first_link']			= 'First';
			$pagedata['first_tag_open']		= '<li>';
			$pagedata['first_tag_close']	= '</li>';
			$pagedata['last_link']			= 'Last';
			$pagedata['last_tag_open']		= '<li>';
			$pagedata['last_tag_close']		= '</li>';
			$pagedata['num_links'] 			= 3 ;

			$pagedata['full_tag_open']		= '<div class="pagination"><ul class="pagination">';
			$pagedata['full_tag_close']		= '</ul></div>';
			$pagedata['cur_tag_open']		= '<li class="active"><a href="#">';
			$pagedata['cur_tag_close']		= '</a></li>';
			
			$pagedata['num_tag_open']		= '<li>';
			$pagedata['num_tag_close']		= '</li>';
			
			$pagedata['prev_link']			= '&laquo;';
			$pagedata['prev_tag_open']		= '<li>';
			$pagedata['prev_tag_close']		= '</li>';

			$pagedata['next_link']			= '&raquo;';
			$pagedata['next_tag_open']		= '<li>';
			$pagedata['next_tag_close']		= '</li>';
		
			$this->pagination->initialize($pagedata);

			for ($ctr = 0; $ctr < count($data['templates']); $ctr++ )
			{
				$encodedThumbnailImage = $this->encodeArtImage($data['templates'][$ctr]->thumbnail);
				$data['templates'][$ctr]->thumbnail = $encodedThumbnailImage;
				
			}
		
			$this->load->view_store('school-store', $data);
		} else {
			redirect("/");
		}
	}

	// End Mike codes

	public function get_school(){

		$this->load->helper('textparser');
		$school_name = string_toDB($this->input->post('schoolname'));
		// $school_name = $this->input->post('schoolname');
		$state = $this->input->post('statename');
		$zipcode = $this->input->post('zipcode');
		$data['sch'] = "";
		$school = $this->Schools_model->get_schools($school_name, $state, $zipcode);
		$data['count'] = count( $school );
		object_parseToString($school);
		// foreach ($school as $key => $schoolval) {
		// 	$name = $schoolval->school_name;
		// 	$data['sch'] .= '<a href="'.base_url('school/store/'.$schoolval->school_id).'" class="gray-darker school-storelink"><div class="school-pop-holder"><div class="school-pop-title gsemibold font-regular" schoolid="'.$schoolval->school_id.'">'.$name.'</div><p class="school-pop-desc gregular font-regular">'.$schoolval->school_address_1.'</p></div></a>';

		// 	}	
		if($data['count'] != 0 )
		{
			foreach ($school as $key => $schoolval) {
			$name = $schoolval->school_name;
			$data['sch'] .= '<a href="'.base_url('school/store/'.$schoolval->school_id).'" class="gray-darker school-storelink"><div class="school-pop-holder"><div class="school-pop-title gsemibold font-regular" schoolid="'.$schoolval->school_id.'">'.$name.'</div><p class="school-pop-desc gregular font-regular">'.$schoolval->school_address_1.'</p></div></a>';

			}	
		}
		else
		{			
			$this->test_great_school_api($school_name,$zipcode,$state);
			
			$school = $this->Schools_model->get_schools($school_name, $state, $zipcode);
			$data['count'] = count( $school );
			object_parseToString($school);
			foreach ($school as $key => $schoolval) {
			$name = $schoolval->school_name;
			$data['sch'] .= '<a href="'.base_url('school/store/'.$schoolval->school_id).'" class="gray-darker school-storelink"><div class="school-pop-holder"><div class="school-pop-title gsemibold font-regular" schoolid="'.$schoolval->school_id.'">'.$name.'</div><p class="school-pop-desc gregular font-regular">'.$schoolval->school_address_1.'</p></div></a>';

			}	
		}
		

		echo json_encode($data);
	}

	public function test_great_school_api( $search_str = "", $search_zip = "", $state = 'CA', $great_schools_api_key = 'tpwueavx6u4z70eitztxyqxx' ) {
		// ini_set('error_reporting', E_ALL);
		$this->load->helper('textparser');
		$str_search='';
		$api = '';
		
		/*
		* Which api to use: nearby or school search?
		* BUILD the api url
		*/

		if ( is_numeric( $search_zip ) && $search_zip > 0 ) {
			$api = 'nearbyschools';
			#nearby schools, 
			/*
			 * required: key, state
			 */
			$apiurl = "http://api.greatschools.org/schools/nearby?key=".$great_schools_api_key;
			//&address=160+Spear+St&city=San+Francisco&state=CA&zip=94105&schoolType=public-charter&levelCode=elementary-schools&radius=10&limit=100"

			#only nearby school API have this parameter
			$apiurl .= "&minimumSchools=1";
			$apiurl .= "&radius=1";
			
			#zip is optional
			if(strlen($search_zip) == 5)
			{
				$str_search = $search_zip;
				
				$apiurl .= "&zip=".$search_zip;

			}
			
			$text_field_used = "zipcode_txt";

		}else{
			$api = 'schoolsearch';
			
			#school search, 
			/*
			 * required: key, state, q = query string
			 */
			$apiurl = "http://api.greatschools.org/search/schools?key=".$great_schools_api_key;
			//&state=CA&q=Alameda&sort=alpha&levelCode=elementary-schools&limit=10"
			
			#only search school have this parameter
			#$apiurl .= "&sort=alpha";

			$apiurl .= "&q=".str_replace(' ','-',$search_str);
			
			$str_search = $search_str;
			
			$text_field_used = "school_name";
		}
		
		$level = '';
		
		#both APIs have these parameters
		// if(isset($_POST['levelCode'])) if($_POST['levelCode'] != '')
		// {
		// 	$apiurl .= "&levelCode=".$_POST['levelCode'];
		// 	$level = $_POST['levelCode'];
		// }
		
		$apiurl .= "&state=".$state;
		$apiurl .= "&limit=500";
		/* END BUILD the api url */
	
		$result = $this->curl->simple_get($apiurl);
		
		if ( $result != '' )
		{
			$xml = new SimpleXMLElement($result);
			objectString_toDB($xml);
			
			foreach ( $xml as $key => $row ) {
				$zipcode = substr(trim($row->address), -5);
				$chker = $this->Schools_model->checkGreatSchool($row->gsId);

				$data = array(
						'gsid' => $row->gsId,
						'ncesid' => $row->ncesId,
						'store_id' => STOREID,
						'school_name' => $row->name,
						'school_mascot' => "",
						'school_primary_color' => "",
						'school_secondary_color' => "",
						'school_address_1' => $row->address ,
						'school_address_2' => "",
						'school_city' => $row->city,
						'school_state' => $row->state,
						'school_zip' => $zipcode,
						'school_organizer' => "",
						'school_contact' => $row->phone,
						'date_created' => date("Y-m-d H:i:s")
					);
				if($chker[0]['ctr'] == 0 )
				{
					$this->Schools_model->store_to_school($data);	
				}
				
				
			}
		}

		#Search own schools database first, if no result, search on GreatSchools API
		$search_qry['state'] = $state;		
		$search_qry['zip'] = $search_zip;
		//$search_qry['school_name'] = quotemeta(quotemeta(quotemeta($search_str)));			
		$search_qry['school_name'] = trim($search_str);			
		$search_qry['level'] = $level;
		
		$count = 0;
		
		#if zip is searched
		if(is_numeric($search_zip) && $search_zip > 0)
		{/*
			#check if zip code has been searched before, and that the search result above is equal on count and that it is searched by zip
			$this->db->where('search_term', $search_zip);
			$this->db->where('state', $state);
			$this->db->where('result_count', $count);
			$this->db->where('text_field_used', 'zipcode_txt');
			$query = $this->db->get('tzilla_school_searched');
			
			if($query->num_rows() == 0){
				$count = 0; //let us consider it as no results found on db so we can search on greatschools api
			}
		*/}
		
		#no results? then search on GreatSchools API, let's try our luck
		if($count==0){/*
			echo "<div style='display:none'>Searched from GS-API</div>";
			#if($api == 'schoolsearch' && $state)		
		
			$result = $this->curl->simple_get($apiurl);

			#echo "<div style='height:100px; width: 100%; overflow:auto;'>".$result."</div>";				
		
			#print_r( $result );
		
			if($result != '')
			{
				$xml = new SimpleXMLElement($result);
				
				#print_r($xml);
					
				#echo "api url: ".$apiurl;
						
				#echo 'dubug>> str='.$search_str.' zip='.$search_zip.' state='.$state;
				
				//$count = 0;
				$school_results = array();
				foreach($xml as $row)
				{			
					
					#create school slug
					$school_slug = $row->name.' '.str_replace(' ','_', $row->city).' '.$row->state.' '.$row->ncesId;
					
					$slug = url_title($school_slug,'-', true);
					
					//gsId is GreatSchool id
					$zipcode = substr(trim($row->address), -5);
					
					$data = array(
						'gsId' => mysql_real_escape_string ($row->gsId),
						'ncesId' => mysql_real_escape_string ($row->ncesId),
						'school_name' => mysql_real_escape_string ($row->name),
						'school_description' => ($row->districtid) ? mysql_real_escape_string ('DisctrictID_'.$row->districtid):'',
						'address' => mysql_real_escape_string ($row->address),
						'city' => mysql_real_escape_string ($row->city),
						'state' => mysql_real_escape_string ($row->state),
						'county' => 'US',
						'zip' => $zipcode,
						'phone' => mysql_real_escape_string ($row->phone),
						'school_emblem' => "",
						'slug' => $slug,
						'district' => mysql_real_escape_string ($row->district),
						'gradelevel' => mysql_real_escape_string ($row->gradeRange),
						'latitude' => mysql_real_escape_string ($row->lat),
						'longitude' => mysql_real_escape_string ($row->lon),
						'schooltype' => mysql_real_escape_string ($row->type),
						'website' => mysql_real_escape_string ($row->website),
						'date_created' => date("Y-m-d H:i:s")					
					);
								
					#check if the school slug is existing
					$rr = $this->db->get_where( 'schools', array( 'slug' => $slug ) );
					
					
					if ($rr->num_rows()==0) 
					{ 				
						if($row->name && $row->city && $row->state)
						{
							#save to schools table
							if( strlen($row->name) > 1 ) $this->Abstract_model->insert($data);
						}
					}else{
						
						$this->db->where('slug', $slug);
						$this->db->update('schools', $data);
					}
					
						
					// print_r($data);
					
					#$school_results[] = $data;
					
					#$count++;
				}//END FOREACH LOOP
				
				
			}
		
			#Then, do another search on own schools table since we inserted whatever results we found on GreatSchools
				/* $search_qry['state'] = $state;		
				$search_qry['zip'] = $search_zip;
				$search_qry['school_name'] = $search_str;			
				$search_qry['level'] = $level;	 */		
			/*
			$school_results = $this->Schools_model->find($search_qry)->result_array();		
			$count = count($school_results);		
			
			if($count == 0)
			{			 
				#at this point there is no result both on our own school database and on GreatSchools API
				$data['error']='No results found';
				$count=0;			
			}
			
		
		*/}
		
		
		#let's pass the search results and other data on the view
		// $data['results'] = $school_results;			
		$data['srch_school_name'] = $search_str;
		$data['srch_zip'] = $search_zip;
		$data['srch_state'] = $state;		
		$data['count'] = $count;		
		$data['str_search']=$str_search;

		#log the search made by the user
		#get the ip
		// $this->load->helper('location');
		// $this->load->library('BrowserDetection');	

		// $browser = new BrowserDetection();

		// $userBrowserName = $this->browserdetection->getBrowser();
		// $userBrowserVer = $this->browserdetection->getVersion();
		
		// $userBrowserName = $browser->getBrowser();
		// $userBrowserVer = $browser->getVersion();

		//if ($userBrowserName == BrowserDetection::BROWSER_FIREFOX && $browser->compareVersions($userBrowserVer, '5.0.1') !== 1) {
		//	echo 'You have FireFox version 5.0.1 or greater. ';
		//}
		// $user_browser = $userBrowserName.' version:'.$userBrowserVer;		
		
		// $data_log = array(
		// 	'search_term'=>$str_search,
		// 	'state'=> $state,
		// 	'result_count'=>$count ,
		// 	'text_field_used'=>$text_field_used,
		// 	'ip'=> get_ip()." or ".get_ipv2(),
		// 	'browser'=>$user_browser,
		// );
		// $this->Abstract_model->initialize('tzilla_school_searched', 'id');	
		// $this->Abstract_model->insert($data_log);
		
		
	}

	public function store_old($id){
		// $data['schoolname'] = "";
		$data['school'] = $this->Schools_model->get_school($id);
		
		$data['getschoolTemp'] = $this->Schools_model->get_school_template();
		// echo $id;
		$data['school_campaign'] = $this->Schools_model->get_school_campaign($id);
		// echo "ASDA";
		// print_r($data['school_campaign']);

		foreach ($data['school'] as $key => $schoolval) {
			$data['schoolname'] = $schoolval->school_name;
			$data['school_mascot'] = $schoolval->school_mascot;
		}


		$this->load->view_store('school-store', $data);

	}
	
	public function get_variations(){
		$temp_id = $this->input->post('temp_id');
		$data['child_variation'] = "";
		$get_variations = $this->Schools_model->get_template_variations($temp_id);
		// print_r($get_variations);
		foreach ($get_variations as $key => $variation_val) {
			
			$data['child_variation'] .= '<div class="design-variation-holder" id="design1"><img class="img-responsive img-center design-image" src="'.$variation_val->thumbnail.'" alt="" /></div>';
		}

		echo json_encode($data);
	}


	function send_email_no_school(){
		$post = $this->input->post(null, true);
		
		$captcha = $post['recaptcha'];
        $str['status'] = "";
        $str['message'] = "";
        if(empty($captcha)){
          $str['status'] = "error";
          $str['message'] = 'Please check the the captcha form.';
          // echo json_encode($str);
          // exit;
        }

        $secretKey = "6LfkVxkUAAAAALtalRHUfC8_NEcJ-UBiHNLGCCSN";
        $ip = $_SERVER['REMOTE_ADDR'];

        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        if(intval($responseKeys["success"]) !== 1) {
          $str['status'] = "error";
          $str['message'] = 'Verification Expired';
        }else{

			$data_school = array(
			'contact_person'=> $post['fullname'],
			'store_id' =>STOREID,
			'school_name' =>$post['school_name'],
			'school_state' =>$post['school_state'],
			'school_contact' =>$post['contact_no'],
			'role' => $post['role'],
			'school_primary_color'=> $post['color_1'],
			'school_primary_color'=> $post['color_1'],
			'email_from' => $post['email'],
			'school_about' =>$post['notes'],
			'mascot_image_path'=> $post['school_img'],
			'date_created'=> date('Y-m-d H:i:s')
				);


			$save_school = $this->Schools_model->letUsKnow($data_school);
			if( $save_school )
			{	
					// $email_body = $this->_load_email_template( $post );				
				
					$email_body =  "<p>  Full Name: ". $post['fullname'] ." </p><br>";
					$email_body .= "School name:". $post['school_name'] ."</p> <br>";
					$email_body .= "School State:" .$post['school_state']. "</p> <br>";
					$email_body .= "Contact No.:" .$post['contact_no'].  "</p><br>";
					$email_body .= "Role: ".$post['role']. "</p><br>";
					$email_body .= "Color 1:".$post['color1']. "</p><br>";
					$email_body .= "Color 2:.".$post['color2']."</p><br>";
					$email_body .= "Notes :". $post['notes']. "</p><br>";
					$email_body .= "School image: </p></br>";

					$image = "public/".STORE."/letusknow/". $post['school_img'];

					// $sendTo = 'cheezel.tin@tzilla.com'; //support@tzilla.com
					$sendTo = 'support@tzilla.com'; 
					$email_data = array(
						'email'=> $sendTo,
						'from'=> $post['email'],
						'from_name'=>  ucfirst(STORE),
						'title' => 'School Request Notification',
						// 'bcc'=> 'jovani.ogaya@tzilla.com',
						'subject'=> ucfirst(STORE).' Cannot find my school ',
						'message'=> $email_body
						// 'fileupload' => $image
					);
		
					$return = $this->send_to_support($email_data);
					$str['status'] = $return;
			}
					 echo json_encode($str);
		}
	}



	function send_to_support($data=null)
	{
		if($data != null){
			
		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout');
		
		#send email for ipn
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = $this->smtp_host;
		$config['smtp_user'] = $this->smtp_user;
		$config['smtp_pass'] = $this->smtp_pass;
		$config['smtp_port'] = $this->smtp_port;
		$config['smtp_timeout'] = $this->smtp_timeout;
		$config['mailtype'] = 'html';
		$config['priority'] = '1';	
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['crlf'] =  "\r\n";
		$config['newline'] = "\r\n";
		
		
		$this->email->initialize($config);
		$this->email->from($data['from'], $data['title']);
		$this->email->to( $data['email'] );
		$this->email->bcc( array('jovani.ogaya@tzilla.com', 'giancarlo.santiago@tzilla.com', 'cherry.valdoz@tzilla.com', 'cheezel.tin@tzilla.com') );
		$this->email->subject($data['subject']);
		// $this->email->attach($data['fileupload'],'inline');	
		$this->email->message($data['message']);
		$r = $this->email->send(); //
			// echo "Send email";
		if($r){
			$r = 'success';
			$message = 'Email sent.';
		}else{
			$r = 'failed';
			$message = json_encode($this->email->print_debugger());
		}
		return $r;

			
	}

}



	function upload_photo( $slug ) {
		$directory = 'public/'.strtolower(STORE).'/letusknow/';
		$str['status'] = "";
		$str['message'] = "";
		if ($_FILES['SelectedFile']['error'] == 0) {
			$str['status'] = "error"; 
			$str['message'] = "An error occurred when uploading.";
		}

		if (!getimagesize($_FILES['SelectedFile']['tmp_name'])) {
			$str['status'] = "error"; 
			$str['message'] = "Please ensure you are uploading an image.";
		}
		
		// Check filetype
		if ( $_FILES['SelectedFile']['type'] != 'image/png' && $_FILES['SelectedFile']['type'] != 'image/jpeg' && $_FILES['SelectedFile']['type'] != 'image/gif' && $_FILES['SelectedFile']['type'] != 'image/jpg' ) {
			$str['status'] = "error"; 
			$str['message'] = "Unsupported filetype uploaded.";
		}

		if ($_FILES['SelectedFile']['size'] > 2097152) {
			$str['status'] = "error"; 
			$str['message'] = "File uploaded exceeds maximum upload size of 2 MB.";
		}

		if ( !file_exists( $directory ) ) { 
			mkdir( $directory, 0777, true ); 
		}

		$extension = pathinfo($_FILES['SelectedFile']['name'], PATHINFO_EXTENSION);
		$_FILES['SelectedFile']['name'] = $slug.".".strtolower($extension);

		
		if (!move_uploaded_file($_FILES['SelectedFile']['tmp_name'], $directory.$_FILES['SelectedFile']['name'])) {
			$str['status'] = "error"; 
			$str['message'] = "Error uploading file - check destination is writeable.";
		}else{
			// SAVE THE LET US KNOW DATA 
			
			$str['status'] = "success";
			$str['message'] = "File upload successfully.";
		}

		
		echo json_encode($str) ;
		// unlink($directory.$_FILES['SelectedFile']['name']); // to delete the image
	}


}


?>