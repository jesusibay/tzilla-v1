<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Schools_model extends CI_Model {

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }


    public function get_schools($school_name=false, $state=false, $zipcode=false )
	{
		// $this->db->select('*');
		// $this->db->from('scp_school');
		// $this->db->Join('scp_states', 'scp_school.school_state = scp_state.state_code');
		// $this->db->Join('scp_zip_to_state', 'scp_school.school_state = scp_zip_to_state.state');
		if(!empty($zipcode) ){
			$this->db->where('scp_school.school_zip', $zipcode);			
		}
		if(!empty($school_name) ){
			$this->db->like('scp_school.school_name', $school_name);			
		}
		if(!empty($state) ){
			$this->db->where('scp_school.school_state', $state);			
		}
		

		return $this->db->get('scp_school')->result();
	}

	public function get_school($id)
	{
		$this->db->where('scp_school.school_id', $id);			
		
		return $this->db->get('scp_school')->row();
	}

	public function get_school_by_campaign_id( $id )
	{
		$this->db->select("scp_school.*");

        $this->db->JOIN('scp_campaigns',  'scp_campaigns.school_id = scp_school.school_id' );

		$this->db->where('scp_campaigns.campaign_id', $id);			
		
		return $this->db->get('scp_school')->row();
	}

	public function get_school_template(){
		$this->db->where('artwork_status !=', 'CHILD');
		return $this->db->get('scp_design_templates')->result();
	}

	public function get_school_campaign($school_id){
		$row =  array( 'scp_campaigns.school_id' =>  $school_id, 'scp_campaigns.campaign_status !=' => 0, 'scp_campaigns.campaign_ended !=' => 1  );
        $this->db->order_by('rand()');
        $this->db->group_by('scp_campaigns.campaign_id');
        $this->db->limit(4);
        $this->db->select('scp_design_templates.*, scp_campaigns.*, scp_school.*, scp_product_templates.shirt_background_color');
        $this->db->JOIN('scp_product_templates',  'scp_design_templates.template_id = scp_product_templates.template_id' );
        $this->db->JOIN('scp_campaign_products',  'scp_product_templates.product_id = scp_campaign_products.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaign_products.campaign_id = scp_campaigns.campaign_id', 'LEFT' );
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $return = $this->db->get_where('scp_design_templates', $row )->result_array();
        return $return;
	}


	public function get_template_variations($temp_id){
		$this->db->select('*');
		$arr  = array('child_type' => 'VRTN', 'parent_template_id' => $temp_id );
		$this->db->where($arr);
		$this->db->JOIN('scp_design_template_children', 'scp_design_templates.template_id = scp_design_template_children.parent_template_id');
		return $this->db->get('scp_design_templates')->result();

	}

	public function letUsKnow($data){

		$this->db->insert('scp_letusknow', $data);
		return $this->db->insert_id();
	}
	
	public function get_all_genColors(){
		$return = $this->db->get('scp_shirt_colors')->result();
		return $return;
	}

	public function store_to_school($data){
		$resource = array();
		
		$value = str_replace("'", "", $this->db->escape($data));
		
		$resource = array (
			'gsid' => $value['gsid'],
			'ncesid' => $value['ncesid'],
			'store_id' => $value['store_id'],
			'school_name' => $value['school_name'],
			'school_mascot'=> $value['school_mascot'],
			'school_primary_color' => $value['school_primary_color'],
			'school_secondary_color' => $value['school_secondary_color'],
			'school_address_1' => $value['school_address_1'],
			'school_address_2' => $value['school_address_2'],
			'school_state' => $value['school_state'],
			'school_zip' => $value['school_zip'],
			'school_organizer' => $value['school_organizer'],
			'school_contact' => $value['school_contact'],
			'date_created'=> $value['date_created']
		);	
			
	 	$this->db->insert('scp_school', $resource );
		$return = $this->db->insert_id();
		return $return;
	}
	public function checkGreatSchool($data){
		$arr  = array('gsid' => $data );
		$this->db->select('COUNT(*) AS ctr');
		$this->db->where($arr);		
		return $this->db->get('scp_school')->result_array();
	}
}

?>