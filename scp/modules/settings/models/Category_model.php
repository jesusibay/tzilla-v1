<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_all_artwork_categories( $parent_category_id = 0 )
	{
		if ( $parent_category_id != 0 ) {
			$this->db->where( "parent_id", $parent_category_id );
		}

		$this->db->where( "type", "CAT" ); // only look into categories records
		$this->db->where( "is_active", 1 );

		$result = $this->db->get("scp_design_template_categories")->result();

		return $result;
	}

	public function get_all_artwork_parent_categories() //scpv1-1
	{
		$this->db->where( "parent_id", 0 );
		$this->db->where( "type", "CAT" ); // only look into categories records
		$this->db->where( "is_active", 1 );
		$result = $this->db->get("scp_design_template_categories")->result();
		return $result;
	}

	public function get_all_artwork_sub_categories( $parent_category_id ) //scpv1-1
	{
		$this->db->where( "parent_id", $parent_category_id );
		$this->db->where( "type", "CAT" ); // only look into categories records
		$this->db->where( "is_active", 1 );
		$result = $this->db->get("scp_design_template_categories")->result();
		return $result;
	}

}

?>