<?php
Class Settings_model extends CI_Model
{
	function __construct()
	{
			parent::__construct();

	}
	
	function get_customer_address( $customerId, $orderId, $shipOrBill = "" ) {
		// Billing, Shipping

		$this->db->where("customer_id", $customerId);

		if ( strlen( $shipOrBill ) > 0 ) $this->db->where("address_type", $shipOrBill)->or_where("address_type", "Both");

		if ( $orderId != 0 ) $this->db->where("order_id", $orderId);

		$return = $this->db->get("scp_customer_addresses")->row(); // must return 1 result only

		return $return;
	}

	function get_customer_details( $customerId ) {
		// Billing, Shipping

		$this->db->where("customer_id", $customerId);

		$return = $this->db->get("scp_customer")->row(); // must return 1 result only

		return $return;
	}

	function get_color_by_code( $colorCode ) {
		$this->db->where("color_code", $colorCode);

		$return = $this->db->get("scp_shirt_colors")->row(); // must return 1 result only

		return $return;
	}

	function get_printer_pallet_by_param( $brandCode, $styleCode, $sizeCode ) {
		$this->db->select("scp_kornit_pallets.*");
		$this->db->join("scp_manage_shirt_style_pallets", "scp_manage_shirt_style_pallets.kornit_pallet_id = scp_kornit_pallets.kornit_pallet_id");

		$this->db->where("scp_manage_shirt_style_pallets.shirt_brand_id", $brandCode);
		$this->db->where("scp_manage_shirt_style_pallets.shirt_style_id", $styleCode);
		$this->db->where("scp_manage_shirt_style_pallets.size_code", $sizeCode);

		$return = $this->db->get("scp_kornit_pallets")->result(); // must return 1 result only

		return $return;
	}

	function get_shirt_alternative_details( $styleId, $colorId, $sizeCode ) {


		$this->db->select("scp_inventory_shirt_color.shirt_color_code");
		$this->db->where("scp_inventory_shirt_color.shirt_color_id", $colorId);
		$colorCode = $this->db->get("scp_inventory_shirt_color")->result();

		$this->db->select("scp_shirt_alternative_plan.shirt_alternative_group_id");
		$this->db->where("scp_shirt_alternative_plan.shirt_style_id", $styleId);
		$shirtAltGroupID = $this->db->get("scp_shirt_alternative_plan")->result();

		$this->db->select("scp_shirt_alternative_group.group_name, scp_inventory_shirt_brand.brand_code, scp_inventory_shirt_style.style_code, scp_inventory_shirt_color.shirt_color_code, scp_inventory_shirt_size.shirt_size_code, scp_inventory_shirt_size.item_code");

		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_inventory_shirt_style.shirt_style_id");
		$this->db->join("scp_inventory_shirt_brand", "scp_inventory_shirt_brand.shirt_brand_id = scp_inventory_shirt_style.shirt_brand_id");
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_inventory_shirt_color.generic_color_id");
		$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id");
		$this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");

		$this->db->where("scp_shirt_alternative_plan.shirt_alternative_group_id", $shirtAltGroupID[0]->shirt_alternative_group_id);
		$this->db->where("scp_inventory_shirt_color.shirt_color_code", $colorCode[0]->shirt_color_code);
		$this->db->where("scp_inventory_shirt_size.shirt_size_code", $sizeCode);
		$this->db->where("scp_inventory_shirt_size.item_code !=", "");

		$this->db->group_by("scp_inventory_shirt_size.item_code");

		$return = $this->db->get("scp_shirt_alternative_plan")->result();

		return $return;
	}

	function get_shirt_plan_field_by_style_code( $fieldName, $styleCode ) {
		$this->db->select( $fieldName );

		$this->db->join( "scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id" );
		$this->db->join( "scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id" );

		$this->db->where( "scp_inventory_shirt_style.style_code", $styleCode );

		$return = $this->db->get( "scp_shirt_alternative_plan" )->row();

		return $return;
	}

	function get_printer_profile_by_param( $styleId, $colorCode ) {
		$this->db->select("scp_inventory_shirt_color.*, scp_kornit_printer_setup.*");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.kornit_setup_id = scp_kornit_printer_setup.kornit_printer_id");

		$this->db->where("scp_inventory_shirt_color.shirt_style_id", $styleId);
		$this->db->where("scp_inventory_shirt_color.shirt_color_code", $colorCode);

		$return = $this->db->get("scp_kornit_printer_setup")->row(); // must return 1 result only

		return $return;
	}
	
	function get_settings($code, $store_id)
	{
		
		$this->db->where('settings_code', $code);
		$this->db->where('store_id', STOREID );
		$result	= $this->db->get('scp_settings');
		
		
		$return	= array();
		foreach($result->result() as $results)
		{
			$return[$results->settings_key]	= $results->setting;
		}
		return $return;	
	}
	
	/*
	settings should be an array
	array('setting_key'=>'setting')
	$code is the item that is calling it
	ex. any shipping settings have the code "shipping"
	*/
	function save_settings($code, $values)
	{
	
		//get the settings first, this way, we can know if we need to update or insert settings
		//we're going to create an array of keys for the requested code
		$settings	= $this->get_settings($code);
	
		
		//loop through the settings and add each one as a new row
		foreach($values as $key=>$value)
		{
			//if the key currently exists, update the setting
			if(array_key_exists($key, $settings))
			{
				$update	= array('setting'=>$value);
				$this->db->where('code', $code);
				$this->db->where('setting_key',$key);
				$this->db->update('settings', $update);
			}
			//if the key does not exist, add it
			else
			{
				$insert	= array('code'=>$code, 'setting_key'=>$key, 'setting'=>$value);
				$this->db->insert('settings', $insert);
			}
			
		}
		
	}
	
	//delete any settings having to do with this particular code
	function delete_settings($code)
	{
		$this->db->where('code', $code);
		$this->db->delete('settings');
	}
	
	//this deletes a specific setting
	function delete_setting($code, $setting_key)
	{
		$this->db->where('code', $code);
		$this->db->where('setting_key', $setting_key);
		$this->db->delete('settings');
	}
}