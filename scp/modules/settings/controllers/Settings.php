<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MX_Controller {
	
	function __construct() 
	{
		parent::__construct();

		$this->load->model(	array('Category_model', 'inventory/Inventory_model', 'settings/Settings_model') );
	}

	// load page for size chart
	public function size_specifications( $styleID = 0, $invGroupID = 0 ){
		$data['styleID'] = $styleID;
		$data['inventoryGroupID'] = $invGroupID;

		$data['category'] = $this->Inventory_model->get_shirt_category();

		if ( $styleID == 0 ) {
			// get all shirt size chart
			$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $invGroupID );
			
		} else {
			// get size chart by shirt group id
			$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
			// $data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID );
		}

		$this->load->view_store('products', $data);
	}

	// load artwork categories
	public function get_artwork_categories( $parentId = 0 ){
		$categories = $this->Category_model->get_all_artwork_categories( $parentId );

		echo json_encode( $categories );
	}

	// load size chart details
	public function get_size_chart_details( $parentId = 0 ){
		$sizedata = $this->Inventory_model->get_size_chart_details( $parentId );

		echo json_encode( $sizedata );
	}

	// load customer shipping and billing address by customer id and order id
	public function get_shipping__address( $customerId = 0, $orderId = 0, $type = "Both" ) {
		$address = $this->Settings_model->get_customer_address( $customerId, $orderId, $type );

		echo json_encode( $address );
	}

	public function get_billing_address( $customerId = 0, $orderId = 0, $type = "Both" ) {
		$address = $this->Settings_model->get_customer_address( $customerId, $orderId, $type );

		echo json_encode( $address );
	}

	public function get($module){

		$result = $this->Settings_model->get_settings($module,STOREID);

		return $result;		
		
	}

}

?>

