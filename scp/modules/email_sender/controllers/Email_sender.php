<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Email_sender extends MX_Controller   {
	
	function __construct()
    {
        parent::__construct();
		$this->load->module( array( 'account', 'order') );
        $this->load->model(	array('Account_model', 'campaign/Campaigns_model', 'signin/Signin_model', 'order/Order_model') );
		$this->load->helper('url');
		$this->load->helper('security');
        $this->load->library( array('form_validation', 'Aauth', 'Curl', 'session', 'pagination') );

        $this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 	
		date_default_timezone_set('America/Los_Angeles');	
    }
	
	//prevent direct access via url
	// public function _remap(){}

	public function load_email_template($order_id = "", $template = "", $template_data = "")
	{	
		// $data['link'] = 'orders';
		// $data['get_order_details'] = $this->Order_model->get_sales_order( $order_id, $template_data['customer_id'] );
		// $data['get_order_items'] = $this->Order_model->get_order_details($order_id );
		// $address_type = $this->Order_model->get_address_by_order_id( $order_id );

		// foreach ($address_type as $key => $val) {
		// 	$type =  $val->address_type;
		// 	if( $type == 'Shipping' ){
		// 		$data['shipping'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
		// 	}
		// 	if( $type == 'Billing' ){
		// 		$data['billing'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
		// 	}
		// 	if( $type == 'Both' ){
		// 		$data['shipping'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
		// 		$data['billing'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
		// 	}
		// }
		
 		// print_r($template_data);

			// foreach ($data['get_order_details'] as $key => $details) { 
			// 	$data['order_date'] = $details['order_date'];
			// 	$data['order_status'] = $details['order_status'];
			// 	$data['order_number'] = $details['order_number'];			

			// }

		$data['get_order_details']  = array(
			array('thumbnail'=>"http://development.tzilla.com/public/images/artworks/d649df18-a373-462b-8088-b56e8c31dea2.png", 
					'color'=> "MNT" ),
			array('thumbnail'=>"http://development.tzilla.com/public/images/buy/055b482d-9878-4dfe-b8dd-4cd95adbfa9a.png", 
					'color'=> "CAR" ),
			array('thumbnail'=>"http://development.tzilla.com/public/images/buy/1c7be3a0-4af3-492f-b742-5d2988dc117f.png", 
					'color'=> "MNT" )
			);

		// $email_body = $this->load->view_store('email-templates/'.$template, $data);
		$email_body = $this->load->view_store('email-templates/sales-order-summary', $data, TRUE);

		return $email_body;
	}

	public function send_smtp($data=null)
	{
		
		if($data != null){
			$this->load->module('settings');
			$setting = $this->settings->get('smtp');
				
			

			//print_r($setting);
			# smtp settings
						
			#send email for ipn
			$this->load->library('email');
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = $setting['smtp_host'];
			$config['smtp_user'] = $setting['smtp_user'];
			$config['smtp_pass'] = $setting['smtp_pass'];
			$config['smtp_port'] = $setting['smtp_port'];
			$config['smtp_timeout'] = $setting['smtp_timeout'];
			$config['mailtype'] = 'html';
			$config['priority'] = '1';
			//$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['crlf'] =  "\r\n";
			$config['newline'] = "\r\n";
		
			$this->email->initialize($config);
			$this->email->from($data['from'], $data['from_name']);
			$this->email->to($data['to']); 
			
			if(isset($data['cc'])) $this->email->cc($data['cc']); 

			if(isset($data['bcc']))	$this->email->bcc($data['bcc']); 

			$this->email->subject($data['subject']);
			
			$this->email->message($data['message']);	

			$r = $this->email->send(); //
			// echo "Send email";
			if($r){
				$r = 'success 1';
				$message = 'Email sent.';
			}else{
				$r = 'failed';
				$message = json_encode($this->email->print_debugger());
			}
			return $r;
			// return "{status:'".$r."',message:'".$message."'}";
		}else{
			return $r;
			// return "{status:'failed',message:'Email data is null'}";
		}
			
	}

	public function testsender(){

		$message = 'message test';
		$error_code = 'errorcode1234';
		$processing_status = 'payment error';

			

		##Notify dev & csr send error codes and messages
		//	$this->load->module('email_sender');
			
			$email_template = 'checkout-payment-error';
			$email_template_data = array(
				'message'=>$message, 
				'error_code'=>$error_code, 
				'processing_status'=> $processing_status
				);			
			$email_body = $this->load_email_template($email_template, $email_template_data);

			$email_data = array(
				'to'=> 'jovani.ogaya@tzilla.com',
				'from'=> 'systemgenerated@supercustomize.com',
				'from_name'=> 'System Generated SCP Email',
				'cc'=> 'mike.ibay@tzilla.com',
				'subject'=> STORE.' Checkout Payment Error',
				'message'=> $email_body,
			);
			echo $this->send_smtp($email_data);
	}



	public function contact_us(){

		// echo $this->input->post();
		// echo "ATAAS";

		$name = $this->input->post('contact_name');
		$email = $this->input->post('contact_email');
		$phone = $this->input->post('contact_phone');
		$schoolname = $this->input->post('contact_school');
		$subject = $this->input->post('contact_subject');
		$message_to = $this->input->post('contact_nessage');
		
			$message = "<html><head></head><body>";
			$message .= 'Hi '.$name.'!<br/>';
			$message .= 'Thank you for reaching out to us ...... Thank you for choosing '.STORE.'<br/><br/>';
			
			$message .= '<br/> ';
			$message .= "</body></html>";
			
			$data = array( 
				'title'=> STORE,
				 'email'=> $email, 
				 'subject'=>$subject, 
				 'message' => $message
			 );

			$this->reply_email($data);

			$data2 = array( 
				'title'=> STORE, 
				'email'=> $email, 
				'subject'=>$subject, 
				'message' => $message_to
			);

			//$this->support_email($data2);
			//save here the message in contact us table
					
			echo $str = 'success';



	}

	public function emailStreamImage()
	{
		$message = 'message test';
		$error_code = 'errorcode1234';
		$processing_status = 'payment error';

		$post = $this->input->post(null, true);

		##Notify dev & csr send error codes and messages
		//	$this->load->module('email_sender');
			
		$email_template = 'sales-order-summary';
		$email_template_data = array(
			'message'=>$message, 
			'error_code'=>$error_code, 
			'processing_status'=> $processing_status
			);	
		
		$email_body = $this->load_email_template($email_template, $email_template_data);

		$email_data = array(
			'to'=> $post['email'],
			'from'=> 'systemgenerated@supercustomize.com',
			'from_name'=> 'System Generated SCP Email',
			'cc'=> 'kim.oliveros@tzilla.com',
			'subject'=> STORE.' Sales Order Summary',
			'message'=> $email_body,
		);
	
		echo $this->send_smtp($email_data);

	}

	private function reply_email($data){ //reply email to the sender
		# smtp settings
		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout');
		
		#send email for ipn
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = $this->smtp_host;
		$config['smtp_user'] = $this->smtp_user;
		$config['smtp_pass'] = $this->smtp_pass;
		$config['smtp_port'] = $this->smtp_port;
		$config['smtp_timeout'] = $this->smtp_timeout;
		$config['mailtype'] = 'html';
		$config['priority'] = '1';	
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['crlf'] =  "\r\n";
		$config['newline'] = "\r\n";
		
		
		$this->email->initialize($config);
		$this->email->from('noreply@tzilla.com', $data['title']);
		$this->email->to( $data['email'] );
		$this->email->subject($data['subject']);		
		$this->email->message($data['message']);
		$this->email->send();
	}

	private function support_email($data){ //forward message to the support team
		# smtp settings
		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout');
		
		#send email for ipn
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = $this->smtp_host;
		$config['smtp_user'] = $this->smtp_user;
		$config['smtp_pass'] = $this->smtp_pass;
		$config['smtp_port'] = $this->smtp_port;
		$config['smtp_timeout'] = $this->smtp_timeout;
		$config['mailtype'] = 'html';
		$config['priority'] = '1';	
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['crlf'] =  "\r\n";
		$config['newline'] = "\r\n";
		
		
		$this->email->initialize($config);
		$this->email->from($data['email'], $data['title']);
		$this->email->to( 'support@tzilla.com' );
		$this->email->subject($data['subject']);		
		$this->email->message($data['message']);
		$this->email->send();
	}

}