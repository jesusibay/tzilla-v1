<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fundraiser_model extends CI_Model {

	function __construct()
	{

		parent::__construct();

	}

	/*
	 * What the function does
	 *
	 * @param (name) about this param
	 * @return (name)
	 */
	public function testModelFunction( $param1 = "" )
	{

		return $param1;

	}

	public function sampleCart($offset = 0, $limit = 5)
	{

		/*
		 * Get these fields from the query
		 * campaign_id
		 * campaign_title
		 * product_id
		 * thumbnail1
		 * school_name
		 * count_all_ordered_qty
		 */
		
		 $row =  array('scp_campaigns.campaign_ended' => 0 );

		$this->db->select( "scp_campaigns.campaign_id, scp_campaigns.campaign_title,scp_products.product_id AS pid,scp_products.thumbnail1, 
 scp_school.school_name, SUM(scp_sales_order_items.quantity) AS count_all_ordered_qty ", FALSE);

		$this->db->join("scp_school", "scp_sales_order_items.school_id = scp_school.school_id");

		$this->db->join("scp_products", "scp_sales_order_items.product_id = scp_products.product_id");

		$this->db->join("scp_campaign_products", "scp_products.product_id = scp_campaign_products.product_id");

		$this->db->join("scp_campaigns", "scp_campaign_products.campaign_id = scp_campaigns.campaign_id");

		$this->db->group_by("scp_campaigns.campaign_id");
		$this->db->order_by("count_all_ordered_qty","DESC");
	
		$this->db->limit($limit, $offset);
		//Return an object result
		$result = $this->db->get_where("scp_sales_order_items", $row)->result();

		return $result;

	}
}