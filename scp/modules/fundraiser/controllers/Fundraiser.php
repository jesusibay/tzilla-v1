<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Fundraiser extends MX_Controller {

	function __construct() 
	{

		parent::__construct();

		$this->load->model(	array( 'Fundraiser_model', 'inventory/Inventory_model' ) );

	}

	/*
	 * Default function for the fundraiser controller
	 *
	 * @param (name) about this param
	 * @return (name)
	 */
	public function index()
	{

		$this->load->view_store( 'fundraiser' ); 

	}

	/*
	 * What the function does
	 *
	 * @param (name) about this param
	 * @return (name)
	 */
	public function pricing()
	{
		echo "Fundraiser Set Pricing";
	}

	/*
	 * What the function does
	 *
	 * @param (name) about this param
	 * @return (name)
	 */
	public function artCollection()
	{
		//remove if FE is ready and data is ready
		$data = $this->Fundraiser_model->sampleCart();
		//end
		if ( count( $data ) > 0 )
		{
			for ( $i = 0; $i < count( $data ); $i++ )
			{ 
				$result[$i] = $data[$i];
				//For testing only please remove once artworks are all updated
				$data[$i]->thumbnail1 = str_replace("162.217.193.70", "localhost", $data[$i]->thumbnail1);
				//until here
				$data[$i]->thumbnail1 = $this->encodeArtImage($data[$i]->thumbnail1);
			}
		}
		echo json_encode( $data );
	}

	public function manageStorefront_preview()
	{
		//remove if FE is ready and data is ready
		$data = $this->Fundraiser_model->sampleCart();
		//end
		if ( count( $data ) > 0 )
		{
			for ( $i = 0; $i < count( $data ); $i++ )
			{ 
				$result[$i] = $data[$i];
				//For testing only please remove once artworks are all updated
				$data[$i]->thumbnail1 = str_replace("162.217.193.70", "localhost", $data[$i]->thumbnail1);
				//until here
				$data[$i]->thumbnail1 = $this->encodeArtImage($data[$i]->thumbnail1);
			}
		}
		echo json_encode( $data );
	}


}