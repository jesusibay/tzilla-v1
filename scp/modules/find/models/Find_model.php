<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Find_model extends CI_Model {

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

   

     public function search_terms($search_text){
            $this->db->select( 'scp_products.product_name, scp_products.product_id, scp_products.slug , scp_product_settings.selling_price,scp_design_templates.schema ');

            $this->db->distinct();
            $this->db->JOIN(  'scp_product_templates', 'scp_products.product_id = scp_product_templates.product_id' ); 
            $this->db->JOIN ('scp_design_templates' ,'scp_product_templates.template_id = scp_design_templates.template_id' );
            $this->db->JOIN('scp_product_settings', 'scp_products.product_id = scp_product_settings.product_id');
            // $this->db->JOIN('scp_product_category',  'scp_products.product_id = scp_product_category.product_id' );
            $this->db->JOIN('scp_order_items',  'scp_products.product_id = scp_order_items.product_id', 'LEFT' );
            $this->db->JOIN('scp_orders',  'scp_order_items.order_id = scp_orders.order_id', 'LEFT' );
            $this->db->JOIN('scp_stores',  'scp_products.store_id = scp_stores.store_id' );

             $arr  = array('scp_products.product_status' => 'ACTIVE', 'scp_stores.store_name' => STORE );
             
            $this->db->like('scp_products.product_name', $search_text  );
            // $this->db->or_like('scp_products.product_description', $search_text  );
            // $this->db->limit($limit, $offset);
            $this->db->where( $arr );
            $this->db->group_by('scp_products.product_id' );
            $search = $this->db->get('scp_products')->result();

            return count($search);
    }

   public function fetch_data($search_text, $limit=0, $offset=0){
             $this->db->select( 'scp_products.product_name, scp_products.product_id, scp_products.slug , scp_product_settings.selling_price,scp_design_templates.schema ');

            $this->db->distinct();
            $this->db->JOIN(  'scp_product_templates', 'scp_products.product_id = scp_product_templates.product_id' ); 
            $this->db->JOIN ('scp_design_templates' ,'scp_product_templates.template_id = scp_design_templates.template_id' );
            $this->db->JOIN('scp_product_settings', 'scp_products.product_id = scp_product_settings.product_id');
            // $this->db->JOIN('scp_product_category',  'scp_products.product_id = scp_product_category.product_id' );
            $this->db->JOIN('scp_order_items',  'scp_products.product_id = scp_order_items.product_id', 'LEFT' );
            $this->db->JOIN('scp_orders',  'scp_order_items.order_id = scp_orders.order_id', 'LEFT' );
            $this->db->JOIN('scp_stores',  'scp_products.store_id = scp_stores.store_id' );

             $arr  = array('scp_products.product_status' => 'ACTIVE', 'scp_stores.store_name' => STORE );
             
            $this->db->like('scp_products.product_name', $search_text  );
            // $this->db->or_like('scp_products.product_description', $search_text  );
            // $this->db->limit($limit, $offset);
            if($limit>0)
			{
				$this->db->limit($limit, $offset);
			}
            $this->db->where( $arr );
            $this->db->group_by('scp_products.product_id' );
            $search = $this->db->get('scp_products');

            return $search->result();
   }

	public function sort_by_param($data) {
		$customHtml = "";
		$sqlinsviewas = "";
		$sqlinsshopby = "";
		$qryinsertCategories = "";
		$qryinsertLimitRecords = "";
		$sqlArray = "";
		$result = "";
		
		
		// modify array to sql IN data
		if (!empty($data["shop_by"])) {
			//$_SESSION['shopby'] = $data["shop_by"];
			
			for ($x=1; $x<=sizeof($data["shop_by"]);$x++){
				if ($x==1) {
					$sqlArray = '('.$data["shop_by"][$x-1];
				} else {
					$sqlArray .= ','.$data["shop_by"][$x-1];
				}
			} 
		}
		$sqlArray .= ')';
		
		if ($sqlArray=="" || $sqlArray==')') {
			$sqlinsshopby = '';
		} else {
			$sqlinsshopby = $sqlArray;
		}
		
		// for page 1
		if ($data["view_as"]=="") {
			$sqlinsviewas = ' limit 0,6';
		} else {
			if ($data["view_as"]=="all") {
				$sqlinsviewas = '';
			} else {
				$sqlinsviewas = ' limit 0,'.$data["view_as"];
			}
		}
		
		// for succeeding pages
		//limit (#perpage x (page-1), #perpage) formula
		if ($data["page_no"]>=2) {
			if ($data["view_as"]=="") {
				$sqlinsviewas = ' limit '.(6*($data["page_no"]-1)).',6';
			} else {
				if ($data["view_as"]=="all") {
					$sqlinsviewas = ' limit 0,0 ';
				} else {
					$sqlinsviewas = ' limit '.(($data["view_as"])*($data["page_no"]-1)).','.$data["view_as"];
				}
			}
		} 
		
		// sqlinserts
		if (!$sqlinsshopby=="") {
			$qryinsertCategories = ' and c.category_id in '.$sqlinsshopby;
		}
		
		if (!$sqlinsviewas=="") {
			$qryinsertLimitRecords = $sqlinsviewas;
		}
		// total row is only used to get the total number of rows per query
		switch ($data["sort_id"]) {
			case 0: // featured
				// create a "Featured" category name in db
				
				$result = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id 
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id 
											join scp_product_settings ps on ps.product_id = p.product_id 
											where c.category_id = (select cat_id from scp_categories where category_name = 'featured') 
											and p.product_status = 'ACTIVE' 
											and p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories.
											" group by p.product_id ".$qryinsertLimitRecords)->result(); // 
				$totalrow = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id 
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id 
											join scp_product_settings ps on ps.product_id = p.product_id 
											where c.category_id = (select cat_id from scp_categories where category_name = 'featured') 
											and p.product_status = 'ACTIVE' 											
											and p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories.
											" group by p.product_id ")->result(); // 

			break;
			case 1: // supercustomized
				// create a "SuperCustomized" category name in db

				$result = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id 
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where c.category_id = (select cat_id from scp_categories where category_name = 'supercustomized') 
											and p.product_status = 'ACTIVE'  
											and p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories.
											" group by p.product_id ".$qryinsertLimitRecords)->result(); // 
				$totalrow = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id 
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where c.category_id = (select cat_id from scp_categories where category_name = 'supercustomized') 
											and p.product_status = 'ACTIVE' 
											and p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories.
											" group by p.product_id ")->result(); // 
											
			break;
			case 2: // best selling
				// note: p.product_status = 'ACTIVE' to check when the prod is still bestseller after a period of time 
				$result = $this->db->query("select *,p.product_id,p.product_name ,count(*) as 'ProductCount' 
											from scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											left join scp_order_items o on o.product_id = p.product_id 
											join scp_product_templates  t on p.product_id = t.product_id 
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											left join scp_stores s on s.store_id = p.store_id 
											where p.product_status = 'ACTIVE'  
											and p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories."
											group by p.product_id 
											order by ProductCount Desc ".$qryinsertLimitRecords)->result();
				$totalrow = $this->db->query("select *,p.product_id,p.product_name ,count(*) as 'ProductCount' 
											from scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											left join scp_order_items o on o.product_id = p.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											left join scp_stores s on s.store_id = p.store_id 
											where p.product_status = 'ACTIVE'  
											and p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories."
											group by p.product_id 
											order by ProductCount Desc ")->result(); 			
			break;
			case 3: // name a->z
				//$qryinsertCategories = ' where c.category_id in '.$sqlinsshopby;
				$result = $this->db->query("select *  
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.product_name ".$qryinsertLimitRecords)->result();
				$totalrow = $this->db->query("select *  
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.product_name ")->result();
											
			break;
			case 4: // name z->a
				//$qryinsertCategories = ' where c.category_id in '.$sqlinsshopby;
				$result = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.product_name desc ".$qryinsertLimitRecords)->result();
				$totalrow = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.product_name desc ")->result();
			break;
			case 5: // price low->high
				//$qryinsertCategories = ' where c.category_id in '.$sqlinsshopby;
				$result = $this->db->query("SELECT *,min(s.selling_price) as MinSellingPrice
											FROM scp_product_settings s 
											join scp_products p on p.product_id = s.product_id 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id 
											order by MinSellingPrice ASC ".$qryinsertLimitRecords)->result();
				$totalrow = $this->db->query("SELECT *,min(s.selling_price) as MinSellingPrice
											FROM scp_product_settings s 
											join scp_products p on p.product_id = s.product_id 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id 
											order by MinSellingPrice ASC ")->result();
			break;
			case 6: // price high->low
				//$qryinsertCategories = ' where c.category_id in '.$sqlinsshopby;
				$result = $this->db->query("SELECT *,max(s.selling_price) as MaxSellingPrice
											FROM scp_product_settings s 
											join scp_products p on p.product_id = s.product_id 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id   
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id  
											order by MaxSellingPrice DESC ".$qryinsertLimitRecords)->result();
				$totalrow = $this->db->query("SELECT *,max(s.selling_price) as MaxSellingPrice
											FROM scp_product_settings s 
											join scp_products p on p.product_id = s.product_id 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id   
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id  
											order by MaxSellingPrice DESC ")->result();
											
				
			break;
			case 7: // date new->old
				//$qryinsertCategories = ' where c.category_id in '.$sqlinsshopby;
				$result = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.date_created desc ".$qryinsertLimitRecords)->result(); 
				$totalrow = $this->db->query("select * 
											from  scp_products p 
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.date_created desc ")->result(); 
			break;
			case 8: // date old->new
				//$qryinsertCategories = ' where c.category_id in '.$sqlinsshopby;
				$result = $this->db->query("select * 
											from  scp_products p
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.date_created asc ".$qryinsertLimitRecords)->result();
				$totalrow = $this->db->query("select * 
											from  scp_products p
											join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates  t on p.product_id = t.product_id  
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id  
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') ".$qryinsertCategories." 
											and p.product_status = 'ACTIVE' 
											group by p.product_id
											order by p.date_created asc ")->result();
			break;

			default:
				//$qryinsertCategories = ' where c.category_id in '.$sqlinsshopby;
				$result = $this->db->query("select * 
											from  scp_products p
											left join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates t on p.product_id = t.product_id 
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id 
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') 
											and p.product_status = 'ACTIVE' ".$qryinsertCategories.
											" group by p.product_id ".$qryinsertLimitRecords)->result();
				
				$totalrow = $this->db->query("select * 
											from  scp_products p
											left join scp_product_category c on p.product_id = c.product_id 
											join scp_product_templates t on p.product_id = t.product_id 
											join scp_product_template_settings ts on t.product_template_setting_id = ts.product_template_setting_id 
											join scp_design_templates dt on t.template_id = dt.template_id 
											join scp_product_settings ps on ps.product_id = p.product_id 
											where p.store_id = (select store_id from scp_stores where store_name = '".STORE."') 
											and p.product_status = 'ACTIVE' ".$qryinsertCategories.
											" group by p.product_id ")->result();
				
		}
		
		
		foreach ($result as $resultkey => $resultvalue) {
			
			if ($data["sort_id"]==6) {
				$sellingprice = number_format($resultvalue->MaxSellingPrice, 2);
			} else if ($data["sort_id"]==5) {
				$sellingprice = number_format($resultvalue->MinSellingPrice, 2);
			} else {
				$sellingprice = number_format($resultvalue->selling_price, 2);
			}
			// <?php echo $prod_value->product_name; > $resultvalue->product_name
			// <?php echo $prod_value->slug; > $resultvalue->slug
			// <?php echo base_url('public/'.STORE.'/images/model3.png"'); > base_url('public/'.STORE.'/images/model3.png"')
			// <?php echo json_decode( $prod_value->schema )->SourceUrl;   > json_decode( $resultvalue->custom_schema )->SourceUrl
			// <?php echo base_url('product/'.$prod_value->slug); > base_url("product/".$resultvalue->slug)
			//
			/*
			$customHtml .= '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center catalog-item-holder filtr-item prodlist" data-category="1" data-sort="'.$resultvalue->product_name.'" >
								<div class="design-item-cont product_design" id="product_id" data-prod-slug="'.$resultvalue->slug.'" >
									
									<div class="new-design-holder">
										<img class="img-responsive design-padder" src="'. base_url('public/'.STORE.'/images/model3.png"').'" style="margin:0 auto;"/>
										<div class="design-holderzzz">
											<img class="img-responsive z1" src= "'.json_decode( $resultvalue->custom_schema )->SourceUrl.'" alt="" style="margin:0 auto;">
										</div>
										<div class="design-x">
											<img class="img-responsive z2 design-padder" src= "'.json_decode( $resultvalue->custom_schema )->SourceUrl.'" alt="" style="margin:0 auto; background-color: #000; width: 100%;">
										</div>
									</div>
									<div class="actionz">
										<div class="action-list quickview hidden-xs">
											<div class="quickview-wrapper"><i class="fa fa-search-plus"></i></div>
										</div>
										<div class="modal fade" id="myModal" role="dialog">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title">'.$resultvalue->product_name.'</h4>
													</div>
													<div class="modal-body">
														<p>Classic: By far our best selling tee: Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. åÊCheck out the specs for the actual measurements and dimensions before you order. Premium Fitted: 100% Soft spun cotton...</p>
													</div>
												</div>
											</div>
										</div>
										<div class="action-list addtocart">
											 <a href="'.base_url("product/".$resultvalue->slug).'">
											 <div class="quickview-wrapper cart-owl-icon"><i class="fa fa-shopping-cart"></i></div>
											 </a>
										</div>
									</div>
								</div>

							</a>

							<div class="desc-holder">
								 <a href="'.base_url('product/'.$resultvalue->slug).'"><h2 class="catalog-item-title text-uppercase" title="'.$resultvalue->product_name.'">'.$resultvalue->product_name.'</h2></a>
								<p class="product-desc">Classic: By far our best selling tee: Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. åÊCheck out the specs for the actual measurements and dimensions before you order. Premium Fitted: 100% Soft spun cotton...</p>
								<span class="span-3">$ '.$sellingprice.'</span>
							</div>
						   </div>';  */
						   
			$customHtml .= '<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center catalog-item-holder filtr-item prodlist" data-category="1" data-sort="'.$resultvalue->product_name.'" >
						<div class="design-item-cont">
							<div class="new-design-holder product_design" id="product_id" data-prod-slug="'.$resultvalue->slug.'">
								<img class="img-responsive design-padder" src="'.base_url('public/'.STORE.'/images/model3.png"').'" style="margin:0 auto;"/>
								<div class="design-holderzzz">
								 <img class="img-responsive z1" src="'.json_decode( $resultvalue->custom_schema )->SourceUrl.'" alt="" style="margin:0 auto;">
								</div>
								<div class="design-x">
								 <img class="img-responsive z2 design-padder" src="'.json_decode( $resultvalue->custom_schema )->SourceUrl.'" alt="" style="margin:0 auto; background-color: #000; width: 100%;"> 
								</div>
							</div>
							<div class="actionz">
								<div class="action-list quickview hidden-xs">
									<div class="quickview-wrapper" data-toggle="modal" data-target="#myModal"><i class="fa fa-search-plus"></i></div>
								</div>
								<div class="action-list addtocart">
									<a href="'.base_url("product/".$resultvalue->slug).'">
										<div class="quickview-wrapper cart-owl-icon"><i class="fa fa-shopping-cart"></i></div>
									</a>
								</div>
							</div>
						</div>
						<div class="desc-holder">
							<a href="'.base_url('product/'.$resultvalue->slug).'"><h2 class="catalog-item-title text-uppercase" title="'.$resultvalue->product_name.'">'.$resultvalue->product_name.'</h2></a>
							<p class="product-desc">Classic: By far our best selling tee: Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. åÊCheck out the specs for the actual measurements and dimensions before you order. Premium Fitted: 100% Soft spun cotton...</p>
							<span class="span-3">$ '.$sellingprice.'</span>
						</div>
					</div>';
		}

		return $customHtml."|".count($totalrow);
		//return $customHtml;

	}


	function record_term($term)
	{
		$code	= md5($term);
		$this->db->where('code', $code);
		$exists	= $this->db->count_all_results('scp_search');
		if ($exists < 1)
		{
			$this->db->insert('scp_search', array('code'=>$code, 'term'=>$term));
		}
		return $code;
	}

	
	function get_term($code)
	{
		$this->db->select('term');
		$result	= $this->db->get_where('scp_search', array('code'=>$code));
		$result	= $result->row();
		return $result->term;
	}
}