<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Find extends MX_Controller   {
	
	// private $apiURL = "http://192.168.57.20/vector-engine-v3/api/Generate"; //scale matrix local ip
	// private $apiURL = "http://162.217.193.70/vector-engine-v3/api/Generate"; //scale matrix public ip

	function __construct()
	{

		parent::__construct();

		$this->load->model('Find/Find_model');
		$this->load->library('session');
		$this->load->library('Curl');
		// $this->load->model('pagination_model');
		$this->load->helper('pagination');
		$this->load->library('pagination');
		// session_start();

	}

	public function search_term( $codes='', $offset=0, $rows=4){

		$post = $this->input->post('search_text');
		if($post){
			$data['search_text'] = $this->input->post('search_text');
			$code	= $data['search_text'];
			// $code	= md5($data['search_text']);
		}else{
			//getting value text from page
			$code	= $codes;
			$data['search_text'] = $codes;
		}

		$this->session->set_userdata('search_term', $data['search_text']);		
		if( empty($data['search_text']) ){
			$data['count_search'] =	''; 
			$data['msg'] = "Type something you're searching for...";
		}else{
			$data['count_search'] =	$this->Find_model->search_terms($data['search_text']);
		}	

			if(STORE == 'scp_admin') redirect('/admin');
			
			$page_title = 'title of the page here...';
			$data['title'] = $page_title;
			$data['content'] = 'search-result-view';
		

			$data["search_result"] = $this->Find_model->fetch_data($data['search_text'], $rows, $offset);

			$uri_params = $this->uri->uri_to_assoc();
			$data['uri_assoc'] = $uri_params;

			$data['uri_segments'] = $this->uri->segment_array(); 


			//pagination
			// $config = array();
			$config["base_url"] = base_url('find/Find/search_term/'.$code.'/');
			$total_row = $data['count_search'];
			$config["total_rows"] = $total_row;
			$config["per_page"] = $rows;
			$config['uri_segment'] = 5;
			// $config["num_links"] = 2;
			$config['use_page_numbers'] = TRUE; 

			$choice = $config["total_rows"]/$config["per_page"];

	        $config["num_links"] = floor($choice);

			$config['full_tag_open']	= '<div class="pagination"><ul class="pagination">';
			$config['full_tag_close']	= '</ul></div>';
			$config['cur_tag_open']		= '<li class="active"><a href="#">';
			$config['cur_tag_close']	= '</a></li>';
			
			$config['num_tag_open']		= '<li>';
			$config['num_tag_close']	= '</li>';
			
			$config['prev_link']		= '&laquo;';
			$config['prev_tag_open']	= '<li>';
			$config['prev_tag_close']	= '</li>';

			$config['next_link']		= '&raquo;';
			$config['next_tag_open']	= '<li>';
			$config['next_tag_close']	= '</li>';

			$config['first_link']		= 'First';
			$config['first_tag_open']	= '<li>';
			$config['first_tag_close']	= '</li>';
			$config['last_link']		= 'Last';
			$config['last_tag_open']	= '<li>';
			$config['last_tag_close']	= '</li>';

			$this->pagination->initialize($config);
			// if($this->uri->segment(4)){
			// 	$page = ($this->uri->segment(4)) ;
			// }
			// else{
			// 	$page = 1;
			// }

			$data['str_links'] = $this->pagination->create_links();
			// $data["links"] = explode('&nbsp;',$str_links );

			// echo count($data["search_result"]);
			$this->load->view_store('search-result-view', $data);
	}	

	public function search_find( $limit=1 , $offset=0){
		
		$post	= $this->input->post(null, false);

		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term			= json_encode($post);
			$code			= $this->Find_model->record_term($term);
			
			$data['code']	= $code;
			//reset the term to an object for use
			$term	= (object)$post;

		}
		elseif ($code)
		{
			$term	= $this->Finds_model->get_term($code);
			$term	= json_decode($term);
		} 
 		$data['term']	= $term;
 		
		$data['count_search'] =	$this->Find_model->search_terms($term);
 		
	
		if(STORE == 'scp_admin') redirect('/admin');
		
		$page_title = 'title of the page here...';
		$data['title'] = $page_title;
		$data['content'] = 'search-result-view';
	
		$uri_params = $this->uri->uri_to_assoc();
		$data['uri_assoc'] = $uri_params;

		$data['uri_segments'] = $this->uri->segment_array(); 


		//pagination
		$config = array();
		$config["base_url"] = base_url() . "search-result/".$term;
		$total_row = $data['count_search'];
		$config["total_rows"] = $total_row;
		$config["per_page"] = $limit;
		$config['use_page_numbers'] = TRUE; 
		$config['num_links'] = 2;

		$config['full_tag_open']	= '<div class="pagination"><ul class="pagination">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';

		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$this->pagination->initialize($config);
		if($this->uri->segment(4)){
			$page = ($this->uri->segment(4)) ;
		}
		else{
			$page = 1;
		}

		$data['str_links'] = $this->pagination->create_links();
		// $data["links"] = explode('&nbsp;',$str_links );

		$data["search_result"] = $this->Find_model->fetch_data($term, $config["per_page"], $offset);
		// echo count($data["search_result"]);
		$this->load->view_store('search-result-view', $data);
	}

	public function search_keyword($search_text="", $limit=1 , $offset=0){
		

		$search_text = $this->input->post('search_text');
 
		 $this->session->set_userdata('text_search', $search_text);
		
		
		$data['count_search'] =	$this->Find_model->search_terms($search_text);
		
		if(STORE == 'scp_admin') redirect('/admin');
		
		$page_title = 'title of the page here...';
		$data['title'] = $page_title;
		$data['content'] = 'search-result-view';
	
		$uri_params = $this->uri->uri_to_assoc();
		$data['uri_assoc'] = $uri_params;

		$data['uri_segments'] = $this->uri->segment_array(); 


		//pagination
		$config = array();
		$config["base_url"] = base_url() . "search-result/".$search_text;
		$total_row = $data['count_search'];
		$config["total_rows"] = $total_row;
		$config["per_page"] = $limit;
		$config['use_page_numbers'] = TRUE; 
		$config['num_links'] = 2;

		$config['full_tag_open']	= '<div class="pagination"><ul class="pagination">';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';

		$config['first_link']		= 'First';
		$config['first_tag_open']	= '<li>';
		$config['first_tag_close']	= '</li>';
		$config['last_link']		= 'Last';
		$config['last_tag_open']	= '<li>';
		$config['last_tag_close']	= '</li>';

		$this->pagination->initialize($config);
		if($this->uri->segment(4)){
			$page = ($this->uri->segment(4)) ;
		}
		else{
			$page = 1;
		}

		$data['str_links'] = $this->pagination->create_links();
		// $data["links"] = explode('&nbsp;',$str_links );

		$data["search_result"] = $this->Find_model->fetch_data($search_text, $config["per_page"], $offset);
		// echo count($data["search_result"]);
		$this->load->view_store('search-result-view', $data);
	}	
	 
	
	public function sort_by_param(){
		 //$_SESSION['shopby'] = $this->input->post('shopby');
		
		//$paramlist  = $this->session->set_userdata($this->input->post());

		//////////////////////////////////////////////////////////////
		$dataHtmlandSql = $this->Find_model->sort_by_param($this->input->post());
		$returndata =  explode("|",$dataHtmlandSql);
		
		$data['result'] = $returndata[0];
		$data['numrow'] = $returndata[1];
		
		echo $dataHtmlandSql; 
		//echo json_encode( $data ); 
	}


	public function test()
	{
		echo "sample return";
	}

}