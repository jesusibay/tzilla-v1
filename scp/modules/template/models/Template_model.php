<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_model extends CI_Model {

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function fetch($id)
    {
        $this->db->where('template_id', $id);

        return $this->db->get('scp_design_templates')->row();
    }

    public function get_artworks_by_id( $id = 0, $min = false )
    {
    	// $this->db->select( "scp_design_templates.template_id, scp_design_templates.properties, scp_design_templates.thumbnail, scp_design_templates.shirt_color_id as hex_value" );
    	$this->db->select( "scp_design_templates.template_id, scp_design_templates.display_name, scp_design_templates.properties, scp_design_templates.thumbnail, IFNULL(parent_color.hex_value,scp_shirt_colors.hex_value) AS hex_value, IFNULL( scp_design_template_children.parent_template_id, scp_design_templates.template_id ) AS 'parent_id'" );

		$this->db->join("scp_design_template_children", "scp_design_template_children.template_id = scp_design_templates.template_id", "left");
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_design_templates.shirt_color_id", "left");

		// load artwork background color the same as the parent
		$this->db->join("scp_design_templates AS parent_bg_color", "parent_bg_color.template_id = scp_design_template_children.parent_template_id", "left");
		$this->db->join("scp_shirt_colors AS parent_color", "parent_color.color_id = parent_bg_color.shirt_color_id", "left");

    	$this->db->where( array( "scp_design_templates.template_id" => $id, "scp_design_templates.is_active" => 1 ) );
		// TODO: update the query to show only the Approved UAT Passed Artworks

		$result = $this->db->get("scp_design_templates")->result();

		return $result;
    }

	// scp v1.1 function for getting template kernings
	public function get_template_kernings( $template_id ) {
		$this->db->order_by( 'kerning_length', 'desc' );

		$result = $this->db->get_where( 'scp_design_template_children', array('parent_template_id' => $template_id, 'child_type' => 'KRNG') )->result();

		return $result;
	}


    public function get_artwork_management_settings( $template_id = 0, $design_location = 'FRNT' )
    {
		// load artwork position settings
		$this->db->join("scp_design_location", "scp_design_location.design_position_id = scp_design_template_pairs.design_position_id", "left");
		$this->db->join("scp_design_template_position_settings", "scp_design_template_position_settings.template_pair_id = scp_design_template_pairs.id", "left");

		$this->db->where( array( "scp_design_template_pairs.template_id" => $template_id, "scp_design_location.location_code" => $design_location ) );
		// $this->db->where( array( "scp_design_location.location_code" => $design_location ) );

		$result = $this->db->get("scp_design_template_pairs")->result();

		return $result;
	}

    public function get_all_artworks( $min = false, $tag = "" )
	{
		if ( $min ) {
			$this->db->select( "scp_design_templates.template_id, scp_design_templates.template_category_id, scp_design_templates.properties, scp_design_templates.display_name, scp_design_templates.tags, scp_design_templates.thumbnail, a.name AS template_category, b.name AS template_type, scp_shirt_colors.hex_value" );

			$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_design_templates.shirt_color_id", "left");
			$this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");
			
			$this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
		} else {
			$this->db->select( "scp_design_templates.*" );
		}

		// $this->db->where( array( "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" ) );
		$this->db->where( "scp_design_templates.is_active", 1 );
		$this->db->where( "scp_design_templates.artwork_status !=", "CHILD" );

		if ( strlen( $tag ) > 0 ) {
			$this->db->like( "scp_design_templates.tags", $tag);
		}
		// TODO: update the query to show only the Approved UAT Passed Artworks


		$result = $this->db->get("scp_design_templates")->result();

		return $result;
	}

	public function get_all_artworks_z($min = false, $cat, $rows, $page )
	{
		if($rows>0){
            $this->db->limit($rows, $page);
        }        

		if ( $min ) {
			$this->db->select( "scp_design_templates.template_id, scp_design_templates.display_name, scp_design_templates.tags, scp_design_templates.thumbnail, a.name AS template_category, b.name AS template_type, scp_shirt_colors.hex_value" );

			$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_design_templates.shirt_color_id", "left");
			$this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");
			
			$this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");

			if( $cat != 0){
				$this->db->where( 'scp_design_templates.template_category_id', $cat );
       		}
		} else {
			$this->db->select( "scp_design_templates.*" );
			if( $cat != 0){
				$this->db->where( 'template_category_id', $cat );
       		}
		}

		$this->db->where( array( "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD", "scp_design_templates.artwork_status" => "PASSED" ) );
		// TODO: update the query to show only the Approved UAT Passed Artworks
		$this->db->order_by( 'scp_design_templates.ordering', 'asc' );
        $this->db->order_by( 'scp_design_templates.display_name', 'asc' );
		$result = $this->db->get("scp_design_templates")->result();

		return $result;
	}

	public function get_all_artworks_z_count($min = false, $cat)
	{
		if ( $min ) {
			$this->db->select( "scp_design_templates.template_id, scp_design_templates.display_name, scp_design_templates.tags, scp_design_templates.thumbnail, a.name AS template_category, b.name AS template_type, scp_shirt_colors.hex_value" );

			$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_design_templates.shirt_color_id", "left");
			$this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");			
			$this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
			if( $cat != 0){
				$this->db->where( 'scp_design_templates.template_category_id', $cat );
       		}
		} else {
			$this->db->select( "scp_design_templates.*" );
			if( $cat != 0){
				$this->db->where( 'template_category_id', $cat );
       		}
		}

		$this->db->where( array( "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" ) );
		// TODO: update the query to show only the Approved UAT Passed Artworks
		$result = $this->db->get("scp_design_templates");

		return $result;
	}


	

	public function get_artwork_colors( $primary = true ) {
		if ( $primary ) {
			$this->db->where( array( "scp_design_template_colors.color_group" => 1 ) );
		} else {
			$this->db->where( array( "scp_design_template_colors.color_group" => 0 ) );
		}

		$result = $this->db->get("scp_design_template_colors")->result();

		return $result;
	}

	public function get_artwork_variations( $template_id, $min = false ) {
		if ( $min ) {
			$this->db->select( "scp_design_templates.template_id, scp_design_templates.properties, scp_design_templates.display_name, scp_design_templates.tags, scp_design_templates.thumbnail, a.name AS template_category, b.name AS template_type, scp_shirt_colors.hex_value" );

			$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_design_templates.shirt_color_id", "left");
			$this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "left");
			
			$this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "left");
		} else {
			$this->db->select( "scp_design_templates.*" );
		}

		$this->db->join("scp_design_template_children", "scp_design_template_children.template_id = scp_design_templates.template_id");

		$this->db->where( array( "scp_design_templates.is_active" => 1, "scp_design_template_children.parent_template_id" => $template_id, "scp_design_template_children.child_type" => "VRTN" ) );

		$this->db->order_by("scp_design_templates.display_name");
		// TODO: update the query to show only the Approved UAT Passed Artworks

		$result = $this->db->get("scp_design_templates")->result();

		return $result;
	}

	public function get_color_hex( $color )
	{
		$this->db->where( 'scp_design_template_colors.color_name', str_replace("-", " ", $color) );			
		
		return $this->db->get('scp_design_template_colors')->row();
	}

	public function get_artwork_addons( $template_id, $addon_type ) {
		
		$this->db->select( "scp_design_template_addon_types.addon_type_code, scp_design_template_addon_types.addon_type_name, scp_design_template_addons.addon_code, scp_design_template_addons.category, scp_design_template_addons.addon_name, scp_design_template_addons.thumbnail" );
		$this->db->join("scp_design_templates", "scp_design_templates.template_id = scp_design_template_addon_settings.template_id");
		$this->db->join("scp_design_template_addons", "scp_design_template_addons.id = scp_design_template_addon_settings.addon_id");
		$this->db->join("scp_design_template_addon_types", "scp_design_template_addon_types.id = scp_design_template_addons.addon_type_id");

		$this->db->where( array( "scp_design_templates.template_id" => $template_id, "scp_design_template_addons.is_active" => 1 ) );

		if ( strlen( $addon_type ) > 0 ) {
			$this->db->where( "scp_design_template_addon_types.addon_type_code", $addon_type);
		}
			$this->db->order_by("scp_design_template_addons.category", "ASC");	
		$result = $this->db->get("scp_design_template_addon_settings")->result();

		return $result;
	}

	public function get_artwork_addons_by_code( $addon_code ) {
		$result = "";

		return $result;
	}

	public function searchMoreArtworks( $limit = 10, $offset = 0, $searchType = 0, $keyword = "" )
	{

		/*
		 * Get these fields from the query
		 * template id
		 * display name
		 * thumbnail url
		 * hex value ( background color )
		 * parent template id
		 */
		$this->db->select( "scp_design_templates.template_id, scp_design_templates.display_name, scp_design_templates.thumbnail, scp_design_templates.tags, IFNULL(parent_color.hex_value,scp_shirt_colors.hex_value) AS hex_value, IFNULL( scp_design_template_children.parent_template_id, scp_design_templates.template_id ) AS 'parent_id'" );

		// join this table to get also the child artwork variations
		$this->db->join("scp_design_template_children", "scp_design_template_children.template_id = scp_design_templates.template_id", "left");

		// join this table to get the back ground color set for the artwork
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_design_templates.shirt_color_id", "left");

		// load artwork background color the same as the parent
		$this->db->join("scp_design_templates AS parent_bg_color", "parent_bg_color.template_id = scp_design_template_children.parent_template_id", "left");
		$this->db->join("scp_shirt_colors AS parent_color", "parent_color.color_id = parent_bg_color.shirt_color_id", "left");

		// $this->db->where( array( "scp_design_templates.template_id" => $id, "scp_design_templates.is_active" => 1 ) );
		// TODO: update the query to show only the Approved UAT Passed Artworks

		if ( $searchType == 1 || $searchType == 2 )
		{
			$this->db->like( "scp_design_templates.tags", $keyword );
		}

		if ( $searchType == 3 )
		{
			$this->db->select( "SUM(scp_sales_order_items.quantity) AS quantity" );

			// join this tables to get the most units sold over the previous month. ( Most Popular Search )
			$this->db->join("scp_sales_order_item_options", "scp_sales_order_item_options.template_id = scp_design_templates.template_id");
			$this->db->join("scp_sales_order_items", "scp_sales_order_items.order_items_id = scp_sales_order_item_options.order_items_id");

			$this->db->group_by( "scp_design_templates.template_id" );
			$this->db->order_by( "quantity", "desc" );
		}

		if ( $searchType == 4 )
		{
			$this->db->order_by( "scp_design_templates.date_created", "desc" );
		}

		$this->db->limit( $limit );

		$result = $this->db->get( "scp_design_templates" )->result();

		return $result;

	}

}

?>