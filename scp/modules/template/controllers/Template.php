<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template extends MX_Controller {
	
	function __construct() 
	{
		parent::__construct();

		$this->load->model(	array( 'Template_model', 'school/Schools_model', 'inventory/Inventory_model', 'campaign/Campaigns_model', 'settings/Category_model' ) );
		$this->load->library( array('session', 'user_agent') );
	}

	public function index(){
		redirect( base_url() );
	}

	public function load_school_artwork_by_id( $template_id = 0 ) {
		$data['templates'] = $this->Template_model->get_artworks_by_id( $template_id );

		echo json_encode( $data['templates'] );
	}

	public function load_school_artworks( $category = "" ) {
		$data['templates'] = $this->Template_model->get_all_artworks( true, $category );

		echo json_encode( $data );
	}

	public function load_artwork_variations( $templateId = 0 ) {
		$data['templates'] = $this->Template_model->get_artwork_variations( $templateId, true );
		for($i=0;$i<=count($data['templates'])-1;$i++)
		{
			$data['templates'][$i]->encodedThumbnailVariation = $this->encodeArtImage($data['templates'][$i]->thumbnail);
			$data['templates'][$i]->encodedImageVariation = $this->encodeArtImage(str_replace("/200/200","", $data['templates'][$i]->thumbnail));
		}
		echo json_encode( $data['templates'] );
	}

	public function load_artwork_addons( $templateId = 0, $addOnType = '' ) {
		$data['addons'] = $this->Template_model->get_artwork_addons( $templateId, $addOnType );

		echo json_encode( $data['addons'] );
	}
    public function load_shirt_styles(){
        $data['styles'] = $this->Inventory_model->get_all_shirt_plans();
        echo json_encode($data);
    }
    public function load_selectable_shirt_colors(){
        $data['shirt_colors'] = $this->Inventory_model->get_all_shirt_colors();
        echo json_encode($data);
    }
	public function load_selectable_colors() {
		$data["primary"] = $this->Template_model->get_artwork_colors(true);
		$data["secondary"] = $this->Template_model->get_artwork_colors(false);
		echo json_encode( $data );
	}

	public function load_kerning( $templateID = 0 ) {
		$data['kerning'] = $this->Template_model->get_template_kernings( $templateID );

		echo json_encode( $data );
	}

	public function load_artwork_management_settings( $designLocation = '', $templateId = 0 ) {
		$data["art_settings"] = $this->Template_model->get_artwork_management_settings( $templateId, $designLocation );

		echo json_encode( $data["art_settings"] );
	}

	public function generator() {
		if ( count( $this->input->post('designs') ) > 0 ) {
			$this->session->set_userdata( "school_id", $this->input->post('school') );
			$this->session->set_userdata( "designs", $this->input->post('designs') );

			$this->session->set_userdata( "default_prnt", $this->input->post('default_parent') );
			$this->session->set_userdata( "default_temp", $this->input->post('default_template') );
			// $this->session->set_userdata( "buy_or_create", $this->input->post('buy_or_create') );

			$goto = "template/customize";
		} else {
			$goto = "#";
		}

		echo $goto; // redirect user tgen if all data needed has been saved/passed
	}

	public function update_campaign_items() {
		if ( count( $this->input->post('product_details') ) > 0 ) {
			$this->session->set_userdata( "school_id", $this->input->post('school') );
			$this->session->set_userdata( "designs", $this->input->post('designs') );

			$this->session->set_userdata( "default_prnt", $this->input->post('default_parent') );
			$this->session->set_userdata( "default_temp", $this->input->post('default_template') );
			$this->session->set_userdata( "go_back", 'campaign_preview' );
			$this->session->set_userdata( "product_details", $this->input->post('product_details') );

			$goto = "template/customize";
		} else {
			$goto = "#";
		}

		// echo "<pre>";
		// print_r( $this->input->post('product_details') );
		// echo "</pre>";

		echo $goto; // redirect user tgen if all data needed has been saved/passed
	}

	public function create_campaign() {
		if ( count( $this->input->post('product_details') ) > 0 ) {
			$this->session->set_userdata( "product_details", $this->input->post('product_details') );

			$goto = "campaign/create";
		} else {
			$goto = "#";
		}

		echo $goto; // redirect user tgen if all data needed has been saved/passed
	}
	

	public function edit_campaign($id) {
		if ( count( $this->input->post('product_details') ) > 0 ) {
			$this->session->set_userdata( "product_details", $this->input->post('product_details') );
			if($id > 0){
				$goto = "campaign/edit_items/".$id;
			}else{
				$goto = "campaign/create/";
			}
			
		} else {
			$goto = "#";
		}

		echo $goto; // redirect user tgen if all data needed has been saved/passed
	}

	public function buy_items() {
		if ( count( $this->input->post('product_details') ) > 0 ) {
			$this->session->set_userdata( "product_details", $this->input->post('product_details') );
			$this->session->set_userdata( "last_tgen_mode", $this->input->post('tgen_mode') );
			$goto = "cart/review";
		} else {
			$goto = "#";
		}

		echo $goto; // redirect user tgen if all data needed has been saved/passed
	}

	public function cart_items() {
		if ( count( $this->input->post('product_details') ) > 0 ) {
			$this->session->set_userdata( "cart_items", $this->input->post('product_details') );

			$goto = "cart/customize";
		} else {
			$goto = "#";
		}

		echo $goto; // redirect user tgen if all data needed has been saved/passed
	}

	public function testresult( $campaignId = 0 ) {

		$test_result = $this->Template_model->get_artwork_management_settings($campaignId);

		echo "<pre>";
		print_r($test_result);
		echo "</pre>";

		$prevTemplate = 0;
		$prevStyle = 0;
		$ctr = 0;
		$indx = 0;

		$colors = array();
		$styles = array();
		$new_value = array();

		$campaign_details = $this->Campaigns_model->get_design_details( $campaignId );

		foreach ( $campaign_details as $key => $value ) {
			if ( $prevTemplate == 0 ) {
				$prevTemplate = $value->parent_id;

				$indx = $ctr;
				$new_value[$ctr]['template_id'] = $value->parent_id;
				$ctr++;
			}

			if ( $prevStyle != $value->shirt_style_id ) {
				if ( $prevTemplate != $value->parent_id ) {
					$indx = $ctr;
					$new_value[$ctr]['template_id'] = $value->parent_id;
					$ctr++;
					$styles = array();
				}
			} else if ( $prevTemplate != $value->parent_id ) {
				$indx = $ctr;
				$new_value[$ctr]['template_id'] = $value->parent_id;
				$ctr++;
				$styles = array();
			}

			if ( $prevTemplate == $value->parent_id && $prevStyle != $value->shirt_style_id ) {
				array_push( $styles, array( "id" => $value->shirt_style_id ) );

				$new_value[$indx]['shirts_and_colors'] = $styles;
			} else if ( $prevTemplate != $value->parent_id && $prevStyle != $value->shirt_style_id ) {
				array_push( $styles, array( "id" => $value->shirt_style_id ) );

				$new_value[$indx]['shirts_and_colors'] = $styles;
			} else {
				// $styles = array();
			}

			echo $value->shirt_style_id." = "+$value->shirt_color_id."<br>";

			$prevTemplate = $value->parent_id;
			$prevStyle = $value->shirt_style_id;
		}

		$ctr = 0;
		// loop styles and colors
		foreach ( $campaign_details as $key => $value ) {
			if ( $prevTemplate == $value->parent_id && $prevStyle != $value->shirt_style_id ) {
				$colors = array();
			} else if ( $prevTemplate != $value->parent_id && $prevStyle != $value->shirt_style_id ) {
				$colors = array();
			}

			array_push( $colors, $value->shirt_color_id );

			for ( $x = 0; $x < count($new_value); $x++ ) {
				for ( $y = 0; $y < count($new_value[$x]['shirts_and_colors']); $y++ ) {
					if ( $new_value[$x]['template_id'] == $value->parent_id && $new_value[$x]['shirts_and_colors'][$y]['id'] == $value->shirt_style_id ) {
						$new_value[$x]['shirts_and_colors'][$y]['colors'] = $colors;
					}
				}
			}

			$prevTemplate = $value->parent_id;
			$prevStyle = $value->shirt_style_id;
		}
	}

	public function customize( $campaignId = 0, $templateId = 0, $mode = 0, $defaultShirt = 0, $defaultShirtColor = 0 ) {
		$product_settings = array();

		if ( $this->session->userdata('school_id') == null ) {
			redirect("school/store");
		} else {
			// get school details by id
			$schoolData = $this->Schools_model->get_school( $this->session->userdata('school_id') );
		}

		if ( $campaignId > 0 ) {
			// get school details by id
			$schoolData = $this->Schools_model->get_school_by_campaign_id( $campaignId );
		}
		
		if ( ( $campaignId == 0 || $campaignId != 0 ) && $mode == 1 && count( $this->session->userdata('product_details') ) >= 0 ) {
			$data['mode'] = "CAMPAIGN"; // mode setting for campaign creator & for edit campaign design settings & manage campaign items
			$data['designs'] = [];
		} else if ( $campaignId != 0 && $templateId != 0 && $mode == 0 ) {
			$data['mode'] = "SUPPORT"; // mode setting for campaign supporters
		} else {
			$data['mode'] = "BUY"; // default TGEN mode
		}

		$data['school'] = $schoolData;	
		// load selected designs ID
		if ( $data['mode'] == "BUY" ) {
			$designsLists = $this->session->userdata( 'designs' );
		} else if ( $data['mode'] == "CAMPAIGN" ) {
			$designsLists = $this->session->userdata('product_details');
		} else {			
				$campaign_details = $this->Campaigns_model->get_design_details_z( $campaignId );
				$prevTemplate = 0;
				$prevStyle = 0;
				$ctr = 0;
				$indx = 0;
				$styles = array();
				$new_value = array();				

				 foreach ( $campaign_details as $key => $val) {
				 	if ( $prevTemplate == 0 ) {
							$prevTemplate = $val['parent_id'];

							$indx = $ctr;
							$new_value[$ctr]['parent_id'] = $val['parent_id'];
							$new_value[$ctr]['product_id'] = $val['product_id'];
							$ctr++;
						}

						if ( $prevStyle != $val['shirt_style_id'] ) {
							if ( $prevTemplate != $val['parent_id'] ) {
								$indx = $ctr;
								$new_value[$ctr]['parent_id'] = $val['parent_id'];
								$new_value[$ctr]['product_id'] = $val['product_id'];
								$ctr++;
								$styles = array();
							}
						} else if ( $prevTemplate != $val['parent_id'] ) {
							$indx = $ctr;
							$new_value[$ctr]['parent_id'] = $val['parent_id'];
							$new_value[$ctr]['product_id'] = $val['product_id'];
							$ctr++;
							$styles = array();
						}

						if ( $prevTemplate == $val['parent_id'] && $prevStyle != $val['shirt_style_id'] ) {
							array_push( $styles, array( "id" => $val['shirt_style_id'],"shirt_style_id" => $val['shirt_style_id'] ) );

							$new_value[$indx]['shirts_and_colors'] = $styles;
						} else if ( $prevTemplate != $val['parent_id'] && $prevStyle != $val['shirt_style_id'] ) {
							array_push( $styles, array( "id" => $val['shirt_style_id'],"shirt_style_id" => $val['shirt_style_id'] ) );

							$new_value[$indx]['shirts_and_colors'] = $styles;
						} else if ( $prevTemplate != $val['parent_id'] && $prevStyle == $val['shirt_style_id'] ) {
							array_push( $styles, array( "id" => $val['shirt_style_id'],"shirt_style_id" => $val['shirt_style_id'] ) );

							$new_value[$indx]['shirts_and_colors'] = $styles;
						}

						$prevTemplate = $val['parent_id'];
						$prevStyle = $val['shirt_style_id'];
				 }

				 	$ctr = 0;
					// loop styles and colors
					foreach ( $campaign_details as $key => $val) {
						for ( $x = 0; $x < count($new_value); $x++ ) {
							for ( $y = 0; $y < count($new_value[$x]['shirts_and_colors']); $y++ ) {
								$colors = $this->Campaigns_model->get_campaign_colors_by_style_id($new_value[$x]['product_id'], $new_value[$x]['shirts_and_colors'][$y]['id']);
					            $cols = array();
					            foreach ($colors as $col) {
					                $cols[] .= $col;        
								}

								if ( $new_value[$x]['product_id'] == $val['product_id'] && $new_value[$x]['shirts_and_colors'][$y]['id'] == $val['shirt_style_id'] ) {
										$new_value[$x]['shirts_and_colors'][$y]['color'] = implode(",",$cols);
										$new_value[$x]['template_id'] = $val['template_id'];
										$new_value[$x]['shirts_and_colors'][$y]['id'] = $val['shirt_style_id'];
										$new_value[$x]['shirts_and_colors'][$y]['colors'] = $cols;
										$new_value[$x]['shirts_and_colors'][$y]['data_price'] = $val['selling_price'];
										$new_value[$x]['shirts_and_colors'][$y]['canvas_position'] = ( isset( $val['canvas_position'] ) ) ? $val['canvas_position'] : "";
										$new_value[$x]['shirts_and_colors'][$y]['art_position'] = ( isset( $val['art_position'] ) ) ? $val['art_position'] : "";
										$new_value[$x]['template_image'] = $val['thumbnail'];
										$new_value[$x]['properties'] = $val['properties'];
										$new_value[$x]['design_id'] = $val['design_id'];
										$new_value[$x]['campaign_id'] = $val['campaign_id'];
										$new_value[$x]['product_id'] = $val['product_id'];
										$new_value[$x]['hex_value'] = $val['hex_value'];
										// $new_value[$x]['canvas_position'] = ( isset( $val['canvas_position'] ) ) ? $val['canvas_position'] : "";
										// $new_value[$x]['art_position'] = ( isset( $val['art_position'] ) ) ? $val['art_position'] : "";
								}
							}
						}

						$prevTemplate = $val['parent_id'];
						$prevStyle = $val['shirt_style_id'];
					}


			$designsLists = $new_value;
		}	
		
		if ( count( $designsLists ) > 0 ) {
			$prevArtwork = 0;
			$setKey = 0;

			foreach ($designsLists as $key => $value) {
				$designDetails = $this->Template_model->get_artworks_by_id( $value['template_id'] );

				if ( $data['mode'] != "BUY" ) {
					$data['designs'][$key]['shirt_style'] = $value['shirts_and_colors'];
					$data['designs'][$key]['data_price'] = ( isset( $value['data_price'] ) ) ? $value['data_price'] : 0 ;
					$data['designs'][$key]['product_id'] = ( isset( $value['product_id'] ) ) ? $value['product_id'] : 0 ;
					$data['designs'][$key]['back_color'] = ( isset( $value['back_color'] ) ) ? $value['back_color'] : '' ;
					$data['designs'][$key]['template_image'] = $value['template_image'];
					$data['designs'][$key]['customization'] = htmlentities($value['properties']);
					$data['designs'][$key]['design_id'] = $value['design_id'];
					$data['designs'][$key]['hex_value'] = $value['hex_value'];
					$data['designs'][$key]['canvas_position'] = ( isset( $value['canvas_position'] ) ) ? $value['canvas_position'] : "";
					$data['designs'][$key]['art_position'] = ( isset( $value['art_position'] ) ) ? $value['art_position'] : "";
				}

				if ( count( $designDetails ) > 0 ) {
					foreach ($designDetails as $design_key => $design_value) {
						$data['designs'][$key]['template_id'] = $design_value->template_id;
						$data['designs'][$key]['parent_id'] = $design_value->parent_id;
						$data['designs'][$key]['thumbnail'] = $this->encodeArtImage($design_value->thumbnail);
						$data['designs'][$key]['hex_value'] = isset( $data['designs'][$key]['hex_value'] ) ? $data['designs'][$key]['hex_value'] : $design_value->hex_value;
						$data['designs'][$key]['display_name'] = $design_value->display_name;
						$data['designs'][$key]['properties'] = json_decode( $design_value->properties );
					}

					$setKey++;
				}

				$prevArtwork = $value['parent_id'];

				//Added temporary condition to fix undefined index
				if($data['mode'] == 'SUPPORT' || $data['mode'] == 'CAMPAIGN')
				{
					array_push( $product_settings, array( $value['parent_id'], $value['shirts_and_colors'] ) );	
				}
				else
				{
					$product_settings = "";
				}
				
			}
		}
		
		$data['cart_items'] = "";
		if( count( $this->session->userdata('cart_items') ) > 0 ){ // print for cart items from supporting
			$cart_items = $this->session->userdata('cart_items');
			$prevArtwork = 0;
			$data['cart_items'] =  $cart_items;
		}
		
		$data['product_settings'] = $product_settings;
		
		if( $campaignId == 0){
			$data['defaultShirt'] = $this->Inventory_model->get_default_shirt();
		}

		$data['shirt_category'] = $this->Inventory_model->get_shirt_category(); // load available shirts	
		$data['art_category'] = $this->Category_model->get_all_artwork_parent_categories();

		
		$data['shirt_colors'] = $this->Inventory_model->get_all_shirt_colors(); // load available shirt colors

		// load available parent designs / artworks
		$data['templates'] = $this->Template_model->get_all_artworks( true );
		for($i=0;$i<=count($data['templates'])-1;$i++)
		{
			$data['templates'][$i]->encodedThumbnail = $this->encodeArtImage($data['templates'][$i]->thumbnail);
			$data['templates'][$i]->encodedImage = $this->encodeArtImage(str_replace("/200/200","", $data['templates'][$i]->thumbnail));
		}
		// load template customizable colors
		$data['styles'] = $this->Inventory_model->get_all_shirt_plans();

		// details from campaign
		$data['customer_id'] = (isset($this->session->customer_id)) ? $this->session->customer_id : "";
		$data['go_back'] = (isset($this->session->go_back)) ? $this->session->go_back : "";	

		$data['campaign_id'] = $campaignId;
		$data['template_id'] = $templateId;

		$this->load->view_store('tgen-visitor', $data);
	}

	// CODE FOR GENERATING IMAGE FOR ARTWORKS
	function update()
	{

		if( $post = $this->input->post(null, true) )
		{

			// lets save the shirt details first...
			$templateOpts  = $post['template'];

			// echo $this->Inventory_model->get_artwork_addons_by_code( $templateOpts['MASCOT_HEAD'] );
			$request = $this->_compose_request($templateOpts);
			$request = str_replace("___", "-", $request);
			// echo $request."<br>";
			$this->_call_api($request);

		}

	}

	// CODE FOR GENERATING DESIGN ID NEEDED FOR PRINTING
	function get_design_id( $data = array() )
	{
		if( $post = $this->input->post(null, true) )
		{
			// lets save the shirt details first...
			$templateOpts = $post['template'];

			$request = '['.$this->_compose_request($templateOpts).']';
			$newId = $this->vector_save_template( $request );

		}
		else if ( count( $data ) > 0 ) {
			$request = '['.$this->_compose_request( array( 'id' => $data['id'], 'AddOnType' => $data['AddOnType'], 'iconCategory' => $data['iconCategory'] ) ).']';
			$newId = $this->vector_save_template( $request );
			
			return $newId;
		}

		echo $newId;

	}

	// new functions for generating Hi-Res Image
	private function vector_save_template( $templateArray )
	{
		$resultdata = '';
		$url = 'http://69.43.181.56/vector-engine-v4/api/pv/design/save';
		$contenttype = "application/json"; // or application/xml
		
		$requestData = $templateArray;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype;charset=UTF-8") );
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$requestData);
		
		$result = curl_exec ($ch);

		curl_close ($ch);

		if ( $result !== false ) {
			$array = json_decode($result,true);
			
			foreach ($array as $getValue) {
				$resultdata = $getValue['Id'];
			}

			return $resultdata;
		} else {
			return $resultdata;
		}
	}
	private function _compose_request( $templateOpts )
	{
		$primary_color = ""; $secondary_color = "";
		
		if ( isset( $templateOpts['pcolor'] ) ) {
			$primary_color = $templateOpts['pcolor'];
			unset( $templateOpts['pcolor'] );
		}
		
		if ( isset( $templateOpts['scolor'] ) ) {
			$secondary_color = $templateOpts['scolor'];
			unset( $templateOpts['scolor'] );
		}

		// lets get the template data from db
		$rec = $this->Template_model->fetch( $templateOpts['id'] );

		unset( $templateOpts['id'] );
		unset( $templateOpts['src'] );

		$heads_directory = "E://VE API - SCP/".$templateOpts['AddOnType']."/".$templateOpts['iconCategory']."/";

		$distress_directory = "E://VE API - SCP/DISTRESS/DISTRESS/";
		$patterns_directory = "E://VE API - SCP/PATTERNS/".$templateOpts['iconCategory']."/";

		$request = new stdClass() ;

		$request->ReferenceId  = $rec->reference_id;
		$request->Id           = $rec->vector_engine_template_id;
		$request->Name         = $rec->template_name;
		$request->TemplateType = $rec->template_type;
		$request->SourceUrl    = $rec->thumbnail;
		$request->OutputUrl    = "";
		$request->Tags         = array();

		$propArr = json_decode($rec->properties);

		// we dont need the stock properties, lets update it using the user supplied data
		$ctr = 0;
		$arr_cnt = count($templateOpts);
		$properties = [];

		foreach($templateOpts as $opt_name => $opt_data)
		{
			$ctr++;
			// $option = new stdClass();

			if ( $opt_name == "DistressPattern" && strlen( $opt_data ) > 0) {

				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "SetDistress";

				$activity_values = new stdClass();
				$activity_values->href = $distress_directory.$opt_data.".pv";
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_HEAD" && strlen( $opt_data ) > 0 ) {

				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "ReplaceMascot";

				$activity_values = new stdClass();
				$activity_values->href = $heads_directory.$opt_data.".pv";
				$activity_values->X___axis = 0; // replace underscore with dash
				$activity_values->Y___axis = 0; // replace underscore with dash
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_PATTERN" && strlen( $opt_data ) > 0 ) {
				$option = new stdClass();
				$option->Name = 'MASCOT_HEAD';

				$activities = new stdClass();
				$activities->Name = "ReplaceMascot";

				$activity_values = new stdClass();
				$activity_values->href = $patterns_directory.$opt_data.".pv";
				$activity_values->X___axis = 0;
				$activity_values->Y___axis = 0;
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_HEAD_PATTERN" && strlen( $opt_data ) > 0 ) {
				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "ReplaceBackgroundImage";

				$activity_values = new stdClass();
				$activity_values->href = $patterns_directory.$opt_data.".png";
				$activity_values->Name = $opt_data;
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "SpotColor" ) {

				// do nothing it will be added at the end of the loop

			}
			else {

				if ( strlen( $opt_data ) > 0 ) {
					$option = new stdClass();
					$option->Name = $opt_name;

					$activities = new stdClass();
					$activities->Name = "ReplaceText";

					$activity_values = new stdClass();
					$activity_values->Value = $opt_data;
					$activities->KeyValues  = $activity_values;

					$option->Activities[] = $activities; 

					$properties[] = $option;
				}

			}

			// add SpotColor to the item
			if ( strlen( $primary_color ) > 0 && strlen( $secondary_color ) > 0 ) {
				if ( $ctr == $arr_cnt ) {
					$option = new stdClass();
					$option->Name = "SpotColor";

					$activities = new stdClass();
					$activities->Name = "SetColor";

					$activity_values = new stdClass();
					$activity_values->PRIMARY_COLOR = $primary_color;
					$activity_values->SECONDARY_COLOR = $secondary_color;
					$activities->KeyValues  = $activity_values;

					$option->Activities[] = $activities; 

					$properties[] = $option;
				}
			}
		}

		$request->Properties = $properties;

		return json_encode($request);
	}
	
	private function _call_api( $request )	
	{
		// if ( $_SERVER['HTTP_HOST'] == 'staging.tzilla.com' || $_SERVER['HTTP_HOST'] == 'devtest.tzilla.com' || $_SERVER['HTTP_HOST'] == 'www.tzilla.com' || $_SERVER['HTTP_HOST'] == 'tzilla.com' ) {
			// $this->apiURL = "http://192.168.57.20/vector-engine-v3/api/Generate"; //scale matrix 
		// } else {
		// 	$this->apiURL = "http://vectorengineapi.cloudapp.net/vector-engine-v3/api/Generate"; // azure
		// }

		// if ( $_SERVER['HTTP_HOST'] == 'localhost' ) {
			// $this->apiURL = "http://162.217.193.70/vector-engine-v3/api/Generate"; //scale matrix 
			$this->apiURL = "http://69.43.181.56/vector-engine-v4/api/Generate"; //scale matrix mac version
		// }

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apiURL);
		// curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
													'Content-Type: application/json',
													'Content-Length: ' . strlen( $request )
													) );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, '='.$request); 
		$content = curl_exec($ch);
		
		curl_close($ch);	
		
		$objResponse = json_decode($content);

		if(isset($objResponse->OutputUrl)){
			echo $this->encodeArtImage($objResponse->OutputUrl);
		}else{
			echo "OutputUrl non-existing".$objResponse.' '.$this->apiURL;
		}
		
	}

	private function _image_trim( $image )
	{
		$image = "http://162.217.193.70/Temp-Images/2ff753b2-2d0b-479f-809a-5a1510269f9a.png";

		header("Content-Type: image/png");

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $image);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.1 Safari/537.11');

		$res = curl_exec($ch);
		$rescode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 

		curl_close($ch) ;

		return $res;
	}

	public function tgen(){

		$this->load->view_store( 'tgen-page' );

	}

	public function view_artTemplate(){

		$data = $this->Template_model->searchMoreArtworks();
			if ( count( $data ) > 0 )
		{
			for ( $i = 0; $i < count( $data ); $i++ )
			{ 
				$result[$i] = $data[$i];
				//For testing only please remove once artworks are all updated
				$result[$i]->thumbnail = str_replace(" ", "%20", $result[$i]->thumbnail);
				//until here
				$result[$i]->thumbnail = $this->encodeArtImage($result[$i]->thumbnail);

			}
		}

		echo json_encode( $result );
	}


}

?>

