<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discount extends MX_Controller {

	function __construct() {

		parent::__construct();

		$this->load->model(	array('Discounts_model') );

	}

	public function index() {
		redirect("/");
	}

	public function load_details( $code = "" ) {
		$result = $this->Discounts_model->get_discount_by_code( $code );

		if ( count($result) > 0 ) {
			$data["status"] = "success";
			$data["details"] = $result;
		} else {
			$data["status"] = "error";
			$data["details"] = "Invalid Code.";
		}

		echo json_encode( $data );
	}

}