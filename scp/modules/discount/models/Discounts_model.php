<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discounts_model extends CI_Model {

    function __construct()
    {

        parent::__construct();

    }

    function get_discount_by_code( $code ){
        $row =  array( 'disc_code' => $code );
        $return = $this->db->get_where('scp_discount_coupon', $row )->row();
        
        return $return;
    }

}