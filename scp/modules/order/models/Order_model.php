<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_orders($id, $sort_by, $sort_order, $limit, $offset){ //logged in user used for account module
        $row =  array( 'scp_customer.customer_id' => $id, 'scp_sales_orders.store_id' => STOREID );
        $this->db->select("scp_sales_orders.*");                
        $this->db->group_by('scp_sales_orders.order_id');      

        $this->db->select('sum(`quantity`) as totalquantity', FAlSE);
        $this->db->JOIN('scp_customer',  'scp_sales_orders.customer_id = scp_customer.customer_id', 'LEFT' );
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );
       
        if($limit>0){
            $this->db->limit($limit, $offset);
        }

        if( $sort_by == 'quantity'){
            $this->db->order_by('totalquantity', $sort_order);
        }else{
            $this->db->order_by('scp_sales_orders.'.$sort_by, $sort_order);
        }      

        $return = $this->db->get_where('scp_sales_orders', $row )->result();
        return $return;
    }

    public function get_orders_count($id, $sort_by, $sort_order){ //logged in user used for account module
        $row =  array( 'scp_customer.customer_id' => $id, 'scp_sales_orders.store_id' => STOREID );
        $this->db->select("scp_sales_orders.*");                
        $this->db->group_by('scp_sales_orders.order_id');      

        $this->db->select('sum(`quantity`) as totalquantity', FAlSE);
        $this->db->JOIN('scp_customer',  'scp_sales_orders.customer_id = scp_customer.customer_id', 'LEFT' );
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );
        if( $sort_by == 'quantity'){
            $this->db->order_by('totalquantity', $sort_order);
        }else{
            $this->db->order_by('scp_sales_orders.'.$sort_by, $sort_order);
        } 
        $return = $this->db->get_where('scp_sales_orders', $row );
        return $return;
    }

    public function get_sales_order( $order_id, $id ){ //logged in user used for account module
        $row =  array( 'scp_sales_orders.customer_id' => $id, 'scp_sales_orders.store_id' => STOREID, 'scp_sales_order_items.order_id' => $order_id );
        $this->db->JOIN('scp_sales_orders',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_order_items', $row )->result_array();
        return $return;
    }

    public function get_sales_order_number( $order_id, $id ){ //logged in user used for account module
        $row =  array( 'scp_sales_orders.customer_id' => $id, 'scp_sales_orders.store_id' => STOREID, 'scp_sales_order_items.order_id' => $order_id );
        $this->db->select("scp_sales_orders.order_number");
        $this->db->JOIN('scp_sales_orders',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_order_items', $row )->row_array();
        return $return;
    }

    public function get_order_details( $order_id){ //logged in user used for account module
        $row =  array( 'scp_sales_order_items.order_id' => $order_id );
        $this->db->join('scp_sales_order_item_options', 'scp_sales_order_items.order_items_id = scp_sales_order_item_options.order_items_id' );
        $this->db->join('scp_campaign_products', 'scp_campaign_products.product_id = scp_sales_order_items.product_id', "left" );
        $this->db->join('scp_campaigns', 'scp_campaign_products.campaign_id = scp_campaigns.campaign_id', "left" );
        $this->db->join('scp_inventory_shirt_style', 'scp_inventory_shirt_style.shirt_style_id = scp_sales_order_items.shirt_style_code', "left" );
        $this->db->join('scp_inventory_shirt_brand', 'scp_inventory_shirt_brand.shirt_brand_id = scp_inventory_shirt_style.shirt_brand_id', "left" );        
        $this->db->join('scp_shirt_alternative_plan', 'scp_shirt_alternative_plan.shirt_style_id = scp_inventory_shirt_style.shirt_style_id', "left" );
        $this->db->join('scp_shirt_alternative_group', 'scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id', "left" );
        $this->db->group_by("scp_sales_order_items.order_items_id");
        $return = $this->db->get_where('scp_sales_order_items', $row )->result_array();
        return $return;
    }

    public function get_order_shirt_styles( $id){
        $this->db->select("scp_shirt_alternative_group.id, scp_shirt_alternative_group.shirt_group_id, scp_shirt_alternative_group.group_name");        
        $this->db->where("id", $id);
        $result = $this->db->get('scp_shirt_alternative_group')->row_array();
        return $result;
    }

     public function get_order_shirt_styles2( $styleid){
        // $this->db->limit(1);
        $this->db->select("scp_shirt_alternative_group.*, scp_inventory_shirt_style.garment_type, scp_shirt_alternative_group.id, scp_shirt_alternative_group.shirt_group_id, scp_shirt_alternative_group.group_name");
        $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
        $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
        $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
        $this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $row =  array( 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active' => 1 );
        $row =  array( 'scp_shirt_color_alternatives.shirt_alternative_id' => $styleid );
        $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
        
        $result = $this->db->get("scp_shirt_color_alternatives")->row_array();
        return $result;
    }
    
    public function get_order_shirt_styles_details( $styleid ){ // account sales order details
        $this->db->limit(1);
        $this->db->select("scp_shirt_alternative_group.*, scp_inventory_shirt_style.garment_type");
        $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
        $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
        $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
        $this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $row =  array( 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active' => 1, 'scp_shirt_color_alternatives.shirt_alternative_id' => $styleid );
        $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
        
        $result = $this->db->get_where("scp_shirt_color_alternatives", $row)->row_array();
        return $result;
    }

    public function get_order_shirt_template( $id){
        $this->db->where("template_id", $id);
        $result = $this->db->get('scp_design_templates')->row_array();
        return $result;
    }  

     public function get_shirt_size_name( $code){
        $this->db->select("shirt_size_name");
        $this->db->limit(1);
        $this->db->group_by('shirt_size_code');
        $this->db->where("shirt_size_code", $code);        
        $result = $this->db->get('scp_inventory_shirt_size')->row_array();
        return $result;
    }
    

    public function get_order_details_item( $order_id ){ //logged in user used for order confirmation
        $row =  array( 'scp_sales_orders.store_id' => STOREID, 'scp_sales_orders.order_id' => $order_id );
        $this->db->order_by('scp_sales_order_items.order_items_id', 'asc');  
        $this->db->limit(1);   
        $this->db->JOIN('scp_sales_orders',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_order_items', $row )->result_array();
        return $return;
    }

    public function get_order_details_item_color( $color_code ){ //logged in user used for order confirmation   
        $row =  array( 'color_code' => $color_code);
        $return = $this->db->get_where('scp_shirt_colors', $row )->row_array();
        return $return;
    }
    

    public function get_tracking_id($id, $customer_id){ //logged in user used for account module
        $row =  array( 'tracking_no' => $id, 'store_id' => STOREID );
        $return = $this->db->get_where('scp_sales_orders', $row )->result_array();
        return $return;
    }

    public function get_orders_by_order_id( $order_id ){ //get orders by order id for orders confirmation
        $row =  array( 'scp_sales_orders.store_id' => STOREID, 'scp_sales_orders.order_id' => $order_id );
        $this->db->select('scp_sales_orders.*, scp_customer.cust_email, scp_customer.cust_firstname, scp_sales_order_items.school_id, scp_sales_order_items.matrix_code, scp_sales_order_items.quantity, scp_sales_order_items.price, scp_sales_order_items.sale_price, scp_sales_order_items.campaign_profit', FALSE);

        $this->db->JOIN('scp_customer',  'scp_sales_orders.customer_id = scp_customer.customer_id', 'LEFT' );
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );

        $return = $this->db->get_where('scp_sales_orders', $row )->result();
        return $return;
    }

    public function get_shirt_by_order_id( $order_id ){ //get orders by order id for orders confirmation
        $row =  array( 'scp_sales_orders.store_id' => STOREID, 'scp_sales_orders.order_id' => $order_id );
        $this->db->select('scp_sales_orders.*, scp_shirt_alternative_group.front_image_link', FALSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_orders.order_id = scp_sales_order_items.order_id');
        $this->db->JOIN('scp_shirt_alternative_plan',  'scp_shirt_alternative_plan.shirt_style_id = scp_sales_order_items.shirt_style_code' );
        $this->db->JOIN('scp_shirt_alternative_group',  'scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id' );
        $this->db->limit("1");
        $this->db->order_by( "scp_sales_order_items.order_items_id", 'ASC' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }

    public function get_orders_details_by_order_id( $order_id ){ //get orders by order id for orders confirmation
        $row =  array( 'scp_sales_orders.store_id' => STOREID, 'scp_sales_orders.order_id' => $order_id );
        $this->db->select('scp_sales_order_items.*, scp_product_templates.template_id, scp_design_templates.template_type_id, scp_design_templates.template_name', FALSE);
        $this->db->select('sum(`price`*`quantity`) as totalprice', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );
        $this->db->JOIN('scp_product_templates',  'scp_product_templates.product_id = scp_sales_order_items.product_id', 'LEFT' );
        $this->db->JOIN('scp_design_templates',  'scp_design_templates.template_id = scp_product_templates.template_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->result_array();
        return $return;
    }

    public function get_address_by_order_id( $order_id ){ //get orders by order id for orders confirmation
        $row =  array( 'scp_customer_addresses.order_id' => $order_id );
        $return = $this->db->get_where('scp_customer_addresses', $row )->result();
        return $return;
    }

    public function get_orders_by_order_type( $order_id, $address_type ){ //get orders by order type for orders confirmation
        $row =  array( 'scp_sales_orders.store_id' => STOREID, 'scp_sales_orders.order_id' => $order_id, 'scp_customer_addresses.address_type' => $address_type );
        $this->db->select('scp_sales_orders.*,scp_customer.cust_email, scp_customer_addresses.address_type, scp_customer_addresses.address_type, scp_customer_addresses.firstname, scp_customer_addresses.lastname, scp_customer_addresses.contact_no, scp_customer_addresses.address_1, scp_customer_addresses.address_2, scp_customer_addresses.city, scp_customer_addresses.state, scp_customer_addresses.zip', FALSE);
        $this->db->JOIN('scp_customer',  'scp_sales_orders.customer_id = scp_customer.customer_id', 'LEFT' );
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_orders.order_id = scp_sales_order_items.order_id', 'LEFT' );
        $this->db->JOIN('scp_customer_addresses',  'scp_sales_orders.order_id = scp_customer_addresses.order_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }
      public function get_tracking_info($trackingID)
    {
        $this->db->select('scp_customer.cust_email, scp_customer.customer_code, scp_sales_orders.erp_so');
        $this->db->where( "tracking_no" , $trackingID );   
        $this->db->JOIN('scp_customer',  'scp_sales_orders.customer_id = scp_customer.customer_id', 'LEFT' );    
        $result = $this->db->get("scp_sales_orders")->result_array();
        return $result;
    }

}

?>