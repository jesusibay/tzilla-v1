<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Campaigns_model extends CI_Model {

    function __construct()
    {

        parent::__construct();

    }

    public function get_campaign_id($campaign_id){
        $row =  array( 'campaign_id' => $campaign_id );
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

    public function get_campaign_by_product_id( $productId ) {
        $row =  array( 'scp_campaign_products.product_id' => $productId );

        $this->db->join( 'scp_campaign_products', 'scp_campaign_products.campaign_id = scp_campaigns.campaign_id' );
        
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

    public function get_campaigns_by_customer( $id, $store_id ) {
        
        $this->db->join( 'scp_customer', 'scp_customer.customer_id = scp_campaigns.campaign_user_id' );
          $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->where('scp_customer.customer_id', $id);
        $this->db->where('scp_campaigns.school_id', $school_id);
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

    function get_csList_sort($search=false, $pageNo=0, $all="false", $sort_by='id', $sort_order='desc'){
        $this->db->select("scp_sales_order_refund.*, scp_customer.customer_id as cust_id, scp_customer.cust_firstname, scp_customer.cust_lastname, scp_customer.cust_email, scp_sales_order_refund.sales_order_id as parent_so, scp_users.name, scp_sales_orders.order_id as soID, scp_sales_orders.total");
        $this->db->join( 'scp_sales_orders', 'scp_sales_order_refund.sales_order_id = scp_sales_orders.erp_so ', 'LEFT' );
        $this->db->join( 'scp_customer', 'scp_sales_orders.customer_id = scp_customer.customer_id ', 'LEFT' );
        $this->db->join( 'scp_users', 'scp_sales_order_refund.refund_by = scp_users.id ', 'LEFT' );
        $this->db->from("scp_sales_order_refund");

        if ($search){
            $date_start = new DateTime($search['from']);
            $date_end = new DateTime($search['to']);
            $start = $date_start->format('Y-m-d H:i:s'); 
            $end = $date_end->format('Y-m-d H:i:s');

            if(!empty($search['SoNo'])){
                $not        = '';
                $operator   = 'OR';
                $like   = '';
                $like   .= "( `scp_sales_order_refund.sales_order_id` ".$not."LIKE '%".$search['SoNo']."%' " ;
                $like   .= ")" ;
                $this->db->where($like);
            }
            if(!empty($search['from'])){
                $this->db->where(' scp_sales_order_refund.date_created >=', $start);           
            }

            if(!empty($search['to'])){
                $end_date = date('Y-m-d', strtotime($search['to'])+86400);           
                $this->db->where(' scp_sales_order_refund.date_created <=', $end_date);            
            }
        }

        if(!empty($sort_by)){
            $this->db->order_by($sort_by, $sort_order);
        }

        if($all=="false"){
            $recordNo=$pageNo*$this->page_limit;
            $this->db->limit($this->page_limit,$recordNo);
        }

        $result=$this->db->get()->result_array();
        return json_encode($result);
    } 
    
    public function get_by_campaign_id($campaign_id){
        $row =  array( 'scp_campaigns.campaign_id' => $campaign_id );
         $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        return $this->db->get_where('scp_campaigns', $row )->result_array();
    }

    public function get_payee_details_by_campaign_id($campaign_id){ // check by campaign id
        $this->db->select('*');
        $this->db->limit(1);
        $this->db->order_by( "customer_address_id", 'DESC' );
        $row =  array( 'campaign_id' => $campaign_id, 'address_type' => 'Campaign Payee', 'order_id' => 0 );
        return $this->db->get_where('scp_customer_addresses', $row )->result_array();
    }

    public function get_payee_details($campaign_id, $post){ // check if post is desame from previous record
        $row =  array( 'order_id' => 0, 'campaign_id' => $campaign_id, 'address_type' => 'Campaign Payee', 'name' => $post['campaign_account_name'], 'address_1' => $post['address_1'], 'address_2' => $post['address_2'], 'city' => $post['city'], 'state' => $post['state'], 'zip' => $post['zip'] );
        return $this->db->get_where('scp_customer_addresses', $row )->result_array();
    }

    public function get_payee_details_campaign_id($campaign_id){ // check if post is desame from previous record
        $row =  array( 'order_id' => 0, 'campaign_id' => $campaign_id, 'address_type' => 'Campaign Payee' );
        return $this->db->get_where('scp_customer_addresses', $row )->result_array();
    }


    public function get_campaigns($customer_id, $sort_by, $sort_order, $limit, $offset){ // used by account campaigns
        $row =  array('scp_campaigns.campaign_user_id' => $customer_id, 'scp_campaigns.campaign_ended !=' => 1 );
        $row2 =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.payment_state' => 'captured', 'scp_sales_orders.checkout_id !=' => null  ); 
        
        if($limit>0){
            $this->db->limit($limit, $offset);
        }

        if( $sort_by == 'profit'){            
            $this->db->order_by('remains_value', $sort_order);            
            $this->db->select("scp_campaigns.*, REPLACE(scp_campaigns.campaign_url, '-oldcampaign', '') AS 'campaign'");
            $this->db->select('sum(`price`*`quantity`) as totalprice', FAlSE);
            $this->db->select('sum(`base_price`*`quantity`) as totalbase_price', FAlSE);
            $this->db->select('(sum(`price`*`quantity`) - sum(`base_price`*`quantity`)) AS `remains_value`', FAlSE);
            $this->db->group_by('campaign');
            $this->db->JOIN('scp_campaign_products',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
            $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id', 'LEFT' );
            $this->db->JOIN('scp_sales_orders',  'scp_sales_order_items.order_id = scp_sales_orders.order_id', 'LEFT' );
        }else if( $sort_by == 'sales'){
            $this->db->order_by('totalprice', $sort_order);            
            $this->db->select("scp_campaigns.*, REPLACE(scp_campaigns.campaign_url, '-oldcampaign', '') AS 'campaign'");
            $this->db->select('sum(`price`*`quantity`) as totalprice', FAlSE);
            $this->db->select('sum(`base_price`*`quantity`) as totalbase_price', FAlSE);
            $this->db->select('(sum(`price`*`quantity`) - sum(`base_price`*`quantity`)) AS `remains_value`', FAlSE);
            $this->db->group_by('campaign');
            $this->db->JOIN('scp_campaign_products',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
            $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id', 'LEFT' );
            $this->db->JOIN('scp_sales_orders',  'scp_sales_order_items.order_id = scp_sales_orders.order_id', 'LEFT' );          
        } else{
           $this->db->order_by($sort_by, $sort_order);            
        }   
        $return = $this->db->get_where('scp_campaigns', $row )->result();     
        return $return;
    }    

    public function get_campaigns_count($customer_id, $sort_by, $sort_order){ // used by account campaigns
        $row =  array('scp_campaigns.campaign_user_id' => $customer_id, 'scp_campaigns.campaign_ended !=' => 1 );
        $row2 =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.payment_state' => 'captured', 'scp_sales_orders.checkout_id !=' => null  ); 
        
        if( $sort_by == 'profit'){            
            $this->db->order_by('remains_value', $sort_order);            
            $this->db->select("scp_campaigns.*, REPLACE(scp_campaigns.campaign_url, '-oldcampaign', '') AS 'campaign'");
            $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
            $this->db->select('sum(`base_price`*`quantity`) as totalbase_price', FAlSE);
            $this->db->select('(sum(`sale_price`*`quantity`) - sum(`base_price`*`quantity`)) AS `remains_value`', FAlSE);
            $this->db->group_by('campaign');
            $this->db->JOIN('scp_campaign_products',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
            $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id', 'LEFT' );
            $this->db->JOIN('scp_sales_orders',  'scp_sales_order_items.order_id = scp_sales_orders.order_id', 'LEFT' );
        }else if( $sort_by == 'sales'){
            $this->db->order_by('totalprice', $sort_order);            
            $this->db->select("scp_campaigns.*, REPLACE(scp_campaigns.campaign_url, '-oldcampaign', '') AS 'campaign'");
            $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
            $this->db->select('sum(`base_price`*`quantity`) as totalbase_price', FAlSE);
            $this->db->select('(sum(`sale_price`*`quantity`) - sum(`base_price`*`quantity`)) AS `remains_value`', FAlSE);
            $this->db->group_by('campaign');
            $this->db->JOIN('scp_campaign_products',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
            $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id', 'LEFT' );
            $this->db->JOIN('scp_sales_orders',  'scp_sales_order_items.order_id = scp_sales_orders.order_id', 'LEFT' );          
        } else{
           $this->db->order_by($sort_by, $sort_order);            
        }   
        $return = $this->db->get_where('scp_campaigns', $row ); 
        return $return;
    }

    public function get_campaign_sales($slug){ // used by account campaigns
        $newslug = str_replace("-oldcampaign","", $slug);

        $row =  array( 'scp_campaigns.campaign_url' => $newslug, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null  );
        $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }

    public function get_campaign_base_price($slug){ // used by account campaigns
        $newslug = str_replace("-oldcampaign","", $slug);

        $row =  array( 'scp_campaigns.campaign_url' => $newslug, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null  );
        $this->db->select('sum(`campaign_profit`*`quantity`) as total_profit', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }

    public function get_all_campaigns($cat=false, $limit, $offset){
        $this->db->select('scp_campaigns.*, scp_products.thumbnail1 as thumbnail, scp_school.school_name, scp_product_templates.shirt_background_color, scp_design_templates.template_type_id, scp_design_templates.display_name');
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->JOIN('scp_campaign_products' , 'scp_campaigns.campaign_id = scp_campaign_products.campaign_id' , 'LEFT');
        $this->db->JOIN('scp_products' , 'scp_campaign_products.product_id = scp_products.product_id' , 'LEFT');
        $this->db->JOIN('scp_product_templates' , 'scp_campaign_products.product_id = scp_product_templates.product_id', 'LEFT'  );
        $this->db->JOIN('scp_design_templates' , 'scp_product_templates.template_id = scp_design_templates.template_id', 'LEFT');
        $this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");            
        $this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
        
         $row =  array( 'scp_campaigns.campaign_status' => 1 , 'scp_campaigns.campaign_ended !=' => 1 ,  "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" );

        if($cat != 0){
           $this->db->where( 'scp_design_templates.template_category_id', $cat );
        }

        if($limit>0){
            $this->db->limit($limit, $offset);
         }
         $this->db->group_by('scp_campaigns.campaign_id');
         $this->db->order_by('scp_campaigns.campaign_id', 'desc');

        $this->db->where($row);
        $return = $this->db->get('scp_campaigns')->result();
        return $return;
    }

    public function get_all_campaigns2($cat=false){
         $this->db->select('scp_campaigns.*, scp_products.thumbnail1 as thumbnail, scp_school.school_name, scp_product_templates.shirt_background_color, scp_design_templates.template_type_id, scp_design_templates.display_name');
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->JOIN('scp_campaign_products' , 'scp_campaigns.campaign_id = scp_campaign_products.campaign_id' , 'LEFT');
        $this->db->JOIN('scp_products' , 'scp_campaign_products.product_id = scp_products.product_id' , 'LEFT');
        $this->db->JOIN('scp_product_templates' , 'scp_campaign_products.product_id = scp_product_templates.product_id', 'LEFT'  );
        $this->db->JOIN('scp_design_templates' , 'scp_product_templates.template_id = scp_design_templates.template_id', 'LEFT');
        $this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");            
        $this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
        
         $row =  array( 'scp_campaigns.campaign_status' => 1 , 'scp_campaigns.campaign_ended !=' => 1 ,  "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" );

        if($cat != 0){
           $this->db->where( 'scp_design_templates.template_category_id', $cat );
        }
        
        $this->db->group_by('scp_campaigns.campaign_id');
        $this->db->order_by('scp_campaigns.campaign_id', 'desc');

        $this->db->where($row);
        $return = $this->db->get('scp_campaigns')->result();
        return $return;
    }

     public function get_search_campaigns($term=false, $cat=false, $limit, $offset){
        $this->db->select('scp_campaigns.*, scp_products.thumbnail1 as thumbnail, scp_school.school_name, scp_product_templates.shirt_background_color, scp_design_templates.template_type_id, scp_design_templates.display_name');
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->JOIN('scp_campaign_products' , 'scp_campaigns.campaign_id = scp_campaign_products.campaign_id' , 'LEFT');
        $this->db->JOIN('scp_products' , 'scp_campaign_products.product_id = scp_products.product_id' , 'LEFT');
        $this->db->JOIN('scp_product_templates' , 'scp_campaign_products.product_id = scp_product_templates.product_id', 'LEFT'  );
        $this->db->JOIN('scp_design_templates' , 'scp_product_templates.template_id = scp_design_templates.template_id', 'LEFT');
        $this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");            
        $this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
       

         $row =  array( 'scp_campaigns.campaign_status' => 1 , 'scp_campaigns.campaign_ended !=' => 1 ,  "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" );
          
        if(!empty($term)){
            $not = '';                     
            $operator   = 'OR';
            $like   = '';
            $like   .= "( `scp_campaigns.campaign_title` ".$not."LIKE '%".$term."%' " ;
            $like   .= $operator." `scp_school.school_name` ".$not."LIKE '%".$term."%' )";   
            $this->db->where($like);
            
        }

        
        if($cat != 0){
           $this->db->where( 'scp_design_templates.template_category_id', $cat );
        }

        if($limit>0){
            $this->db->limit($limit, $offset);
         }
         $this->db->group_by('scp_campaigns.campaign_id');
         $this->db->order_by('scp_campaigns.campaign_id', 'desc');

        $this->db->where($row);
        $return = $this->db->get('scp_campaigns')->result();
        return $return;
 
    }

     public function get_search_campaigns2($term=false, $cat=false){
        $this->db->select('scp_campaigns.*, scp_products.thumbnail1 as thumbnail, scp_school.school_name, scp_product_templates.shirt_background_color, scp_design_templates.template_type_id, scp_design_templates.display_name');
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->JOIN('scp_campaign_products' , 'scp_campaigns.campaign_id = scp_campaign_products.campaign_id' , 'LEFT');
        $this->db->JOIN('scp_products' , 'scp_campaign_products.product_id = scp_products.product_id' , 'LEFT');
        $this->db->JOIN('scp_product_templates' , 'scp_campaign_products.product_id = scp_product_templates.product_id', 'LEFT'  );
        $this->db->JOIN('scp_design_templates' , 'scp_product_templates.template_id = scp_design_templates.template_id', 'LEFT');
        $this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");            
        $this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
       

         $row =  array( 'scp_campaigns.campaign_status' => 1 , 'scp_campaigns.campaign_ended !=' => 1 ,  "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" );


         if(!empty($term)){
            $not = '';                     
            $operator   = 'OR';
            $like   = '';
            $like   .= "( `scp_campaigns.campaign_title` ".$not."LIKE '%".$term."%' " ;
            $like   .= $operator." `scp_school.school_name` ".$not."LIKE '%".$term."%' )";   
            $this->db->where($like);
            
        }

        if($cat != 0){
           $this->db->where( 'scp_design_templates.template_category_id', $cat );
        }  
        
         $this->db->group_by('scp_campaigns.campaign_id');
         $this->db->order_by('scp_campaigns.campaign_id', 'desc');

        $this->db->where($row);
        $return = $this->db->get('scp_campaigns')->result();
        return $return;
    }


     public function get_filter_campaigns($filter=false, $cat=false, $limit, $offset){
        $this->db->select('scp_campaigns.*, scp_products.thumbnail1 as thumbnail, scp_school.school_name, scp_product_templates.shirt_background_color, scp_design_templates.template_type_id, scp_design_templates.display_name');
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->JOIN('scp_campaign_products' , 'scp_campaigns.campaign_id = scp_campaign_products.campaign_id' , 'LEFT');
        $this->db->JOIN('scp_products' , 'scp_campaign_products.product_id = scp_products.product_id' , 'LEFT');
        $this->db->JOIN('scp_product_templates' , 'scp_campaign_products.product_id = scp_product_templates.product_id', 'LEFT'  );
        $this->db->JOIN('scp_design_templates' , 'scp_product_templates.template_id = scp_design_templates.template_id', 'LEFT');
        $this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");            
        $this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
        
         $row =  array( 'scp_campaigns.campaign_status' => 1 , 'scp_campaigns.campaign_ended !=' => 1 ,  "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" );


        if($cat != 0){
           $this->db->where( 'scp_design_templates.template_category_id', $cat );
        }  
        
         if($filter == "latest") {  
           $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->order_by('scp_campaigns.campaign_id', 'desc');
            $this->db->where($row);           

         }else if($filter == "mostpopular"){
             $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
             $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->order_by('rand()');
            $this->db->order_by('scp_sales_order_items.quantity', 'DESC');
            $this->db->order_by('scp_campaigns.campaign_id', 'desc');   
                $this->db->where($row);  

         }else if($filter == "endingsoon"){
              $now = date('Y-m-d H:i:s');
            $next_date= date('Y-m-d H:i:s', strtotime($now. ' + 20 days'));
             $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->where( 'scp_campaigns.end_date <= ', $next_date );
            $this->db->order_by('scp_campaigns.end_date', 'asc');     
      
            $this->db->where($row);
         }else {
            $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->order_by('scp_campaigns.campaign_id', 'desc');
            $this->db->where($row);   
         }

         if($limit>0){
            $this->db->limit($limit, $offset);
         }

        $return = $this->db->get('scp_campaigns')->result();
        return $return;
    }

     public function get_filter_campaigns2($filter=false, $cat=false){
        $this->db->select('scp_campaigns.*, scp_products.thumbnail1 as thumbnail, scp_school.school_name, scp_product_templates.shirt_background_color, scp_design_templates.template_type_id, scp_design_templates.display_name');
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->JOIN('scp_campaign_products' , 'scp_campaigns.campaign_id = scp_campaign_products.campaign_id' , 'LEFT');
        $this->db->JOIN('scp_products' , 'scp_campaign_products.product_id = scp_products.product_id' , 'LEFT');
        $this->db->JOIN('scp_product_templates' , 'scp_campaign_products.product_id = scp_product_templates.product_id', 'LEFT'  );
        $this->db->JOIN('scp_design_templates' , 'scp_product_templates.template_id = scp_design_templates.template_id', 'LEFT');
        $this->db->join("scp_design_template_categories AS a", "a.id = scp_design_templates.template_category_id", "right");            
        $this->db->join("scp_design_template_categories AS b", "b.id = scp_design_templates.template_type_id", "right");
        
         $row =  array( 'scp_campaigns.campaign_status' => 1 , 'scp_campaigns.campaign_ended !=' => 1 ,  "scp_design_templates.is_active" => 1, "scp_design_templates.artwork_status !=" => "CHILD" );


        if($cat != 0){
           $this->db->where( 'scp_design_templates.template_category_id', $cat );
        }  
        
         if($filter == "latest") {  
           $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->order_by('scp_campaigns.campaign_id', 'desc');
            $this->db->where($row);           

         }else if($filter == "mostpopular"){
             $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
             $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->order_by('rand()');
            $this->db->order_by('scp_sales_order_items.quantity', 'DESC');
            $this->db->order_by('scp_campaigns.campaign_id', 'desc');   
                $this->db->where($row);  

         }else if($filter == "endingsoon"){
              $now = date('Y-m-d H:i:s');
            $next_date= date('Y-m-d H:i:s', strtotime($now. ' + 20 days'));
             $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->where( 'scp_campaigns.end_date <= ', $next_date );
            $this->db->order_by('scp_campaigns.end_date', 'asc');     
      
            $this->db->where($row);
         }else {
            $this->db->group_by('scp_campaigns.campaign_id');
            $this->db->order_by('scp_campaigns.campaign_id', 'desc');
            $this->db->where($row);   
         }
        $return = $this->db->get('scp_campaigns')->result();
    }


    public function get_all_categories(){
        $arr = array('parent_id' => 0 , 'is_active' => 1 );
        $this->db->where($arr);
        $return = $this->db->get('scp_design_template_categories')->result();
        return $return;

    }
    public function get_child_category($parent_id){
        $arr = array('parent_id' => $parent_id , 'is_active' => 1 );
        $this->db->where($arr);
        $return = $this->db->get('scp_design_template_categories')->result();
        return $return;
    }

    public function get_design_details( $campaign_id ) {
        $this->db->select('scp_products.thumbnail1 AS thumbnail, scp_product_templates.ve_design_id AS design_id, scp_product_templates.template_properties AS properties, scp_product_templates.shirt_background_color AS hex_value, IFNULL( scp_design_template_children.parent_template_id, scp_product_templates.template_id ) AS parent_id, scp_product_templates.template_id, scp_campaign_products.campaign_id, scp_campaign_products.product_id, scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id, scp_product_settings.selling_price');

        $this->db->join( 'scp_product_templates',  'scp_product_templates.product_id = scp_campaign_products.product_id' );
        $this->db->join( 'scp_products',  'scp_products.product_id = scp_campaign_products.product_id' );
        $this->db->join( 'scp_design_template_children',  'scp_design_template_children.template_id = scp_product_templates.template_id', 'LEFT' );
        $this->db->join( 'scp_product_settings',  'scp_product_settings.product_template_id = scp_product_templates.product_template_id', 'LEFT' );
        
        $this->db->where( "scp_campaign_products.campaign_id", $campaign_id );

        $this->db->order_by( "scp_product_settings.product_template_id, scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id" );

        $return = $this->db->get('scp_campaign_products')->result();

        return $return;
    }

    public function get_design_details_z( $campaign_id ) {
        $this->db->select('scp_products.thumbnail1 AS thumbnail, scp_product_templates.ve_design_id AS design_id, scp_product_templates.template_properties AS properties, scp_product_templates.shirt_background_color AS hex_value, IFNULL( scp_design_template_children.parent_template_id, scp_product_templates.template_id ) AS parent_id, scp_product_templates.template_id, scp_campaign_products.campaign_id, scp_campaign_products.product_id, scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id, scp_product_settings.selling_price');

        $this->db->join( 'scp_product_templates',  'scp_product_templates.product_id = scp_campaign_products.product_id' );
        $this->db->join( 'scp_products',  'scp_products.product_id = scp_campaign_products.product_id' );
        $this->db->join( 'scp_design_template_children',  'scp_design_template_children.template_id = scp_product_templates.template_id', 'LEFT' );
        $this->db->join( 'scp_product_settings',  'scp_product_settings.product_template_id = scp_product_templates.product_template_id', 'LEFT' );
        
        $this->db->where( "scp_campaign_products.campaign_id", $campaign_id );
        $this->db->where( "scp_campaign_products.active", 1 );

        $this->db->order_by( "scp_product_settings.product_template_id, scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id" );

        $return = $this->db->get('scp_campaign_products')->result_array();

        return $return;
    }

    public function get_all_featured_campaigns(){
        $row =  array( 'scp_campaigns.campaign_feature' => 1, 'scp_campaigns.campaign_status !=' => 0, 'scp_campaigns.campaign_ended !=' => 1, 'scp_campaign_products.active' => 1  );
        $this->db->order_by('rand()');
        $this->db->group_by('scp_campaigns.campaign_id');
        $this->db->limit(4);
        $this->db->select('scp_design_templates.*, scp_campaigns.*, scp_school.*, scp_products.thumbnail1, scp_product_templates.shirt_background_color');
        $this->db->JOIN('scp_product_templates',  'scp_design_templates.template_id = scp_product_templates.template_id' );
        $this->db->JOIN('scp_campaign_products',  'scp_product_templates.product_id = scp_campaign_products.product_id' );
        $this->db->JOIN('scp_products',  'scp_products.product_id = scp_product_templates.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaign_products.campaign_id = scp_campaigns.campaign_id', 'LEFT' );
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $return = $this->db->get_where('scp_design_templates', $row )->result_array();
        return $return;
    }

    public function get_trending_campaign(){
        $row =  array( 'scp_campaigns.campaign_status !=' => 0, 'scp_campaigns.campaign_ended !=' => 1, 'scp_campaign_products.active' => 1  );
        $this->db->order_by('rand()');
        $this->db->order_by('scp_sales_order_items.quantity', 'DESC');
        $this->db->group_by('scp_campaigns.campaign_id');
        $this->db->limit(4);
        $this->db->select('scp_design_templates.*, scp_campaigns.*, scp_school.*, scp_products.thumbnail1, scp_product_templates.shirt_background_color');
        $this->db->JOIN('scp_product_templates',  'scp_design_templates.template_id = scp_product_templates.template_id' );
        $this->db->JOIN('scp_campaign_products',  'scp_product_templates.product_id = scp_campaign_products.product_id' );
        $this->db->JOIN('scp_products',  'scp_products.product_id = scp_product_templates.product_id' );
        $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaign_products.campaign_id = scp_campaigns.campaign_id', 'LEFT' );
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $return = $this->db->get_where('scp_design_templates', $row )->result_array();
        return $return;
    }

    public function get_all_featured_campaigns_count_sold( $slug ){
        $newslug = str_replace("-oldcampaign","", $slug);
        $row =  array( 'scp_campaigns.campaign_status !=' => 0, 'scp_campaigns.campaign_ended !=' => 1, 'scp_campaigns.campaign_url' => $newslug  );
        $this->db->select_sum('scp_sales_order_items.quantity');
        $this->db->group_by('scp_campaigns.campaign_id');
        $this->db->JOIN('scp_product_templates',  'scp_design_templates.template_id = scp_product_templates.template_id' );
        $this->db->JOIN('scp_campaign_products',  'scp_product_templates.product_id = scp_campaign_products.product_id' );
        $this->db->JOIN('scp_sales_order_items',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaign_products.campaign_id = scp_campaigns.campaign_id', 'LEFT' );
        $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $return = $this->db->get_where('scp_design_templates', $row )->row_array();
        return $return;
    }

    /*ADDED FUNCTION FOR SEARCHING THE TABLE BY CAMPAIGN URL / SLUG*/ 
    public function check_by_slug($slug){
        $newslug = str_replace("-oldcampaign","", $slug);
        $this->db->select('*');
        $this->db->select_max('scp_campaigns.campaign_id');
        $this->db->where('scp_campaigns.campaign_url', $newslug);
        $this->db->join('scp_customer', 'scp_campaigns.campaign_user_id = scp_customer.customer_id'); 
        return $this->db->get('scp_campaigns')->row_array();
    }

    public function get_by_slug($slug){
        $newslug = str_replace("-oldcampaign","", $slug);
        $this->db->select('*');    
        $this->db->select_max('scp_campaigns.campaign_id');
        $this->db->join('scp_customer', 'scp_campaigns.campaign_user_id = scp_customer.customer_id');     
        return $this->db->get_where('scp_campaigns', 'scp_campaigns.campaign_url', $newslug )->row();
    }

    public function get_campaign_slug($slug){ 
        $newslug = str_replace("-oldcampaign","", $slug);
        $this->db->select('*');
        $this->db->select_max('scp_campaigns.campaign_id');
        $this->db->where('scp_campaigns.campaign_url', $newslug);
        $this->db->join('scp_customer', 'scp_campaigns.campaign_user_id = scp_customer.customer_id'); 
        $this->db->JOIN('scp_school', 'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );    
        return $this->db->get('scp_campaigns')->row_array();
    }

    public function get_by_design( $product_id ){
        $row =  array( 'scp_products.product_id' => $product_id );
        $this->db->select('scp_design_templates.*,scp_products.product_id, scp_products.thumbnail1, scp_product_templates.shirt_background_color');
        $this->db->JOIN('scp_product_templates',  'scp_product_templates.template_id = scp_design_templates.template_id' );
        $this->db->JOIN('scp_products',  'scp_products.product_id = scp_product_templates.product_id' );
        $return = $this->db->get_where('scp_design_templates', $row )->row_array();
        return $return;
    }

    public function get_by_campaign_design( $camp_id ){
        $row =  array( 'scp_campaigns.campaign_id' => $camp_id, 'scp_campaign_products.active' => 1 );
        $this->db->select('scp_design_templates.*,scp_campaign_products.product_id, scp_products.thumbnail1, scp_product_templates.shirt_background_color');
        $this->db->JOIN('scp_product_templates',  'scp_design_templates.template_id = scp_product_templates.template_id' );
        $this->db->JOIN('scp_campaign_products',  'scp_product_templates.product_id = scp_campaign_products.product_id' );
        $this->db->JOIN('scp_products',  'scp_products.product_id = scp_product_templates.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaign_products.campaign_id = scp_campaigns.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_design_templates', $row )->result_array();
        return $return;        
    }

    public function get_campaign_product_items($product_id){ //setting update campaign items
        $this->db->select('shirt_style_id as shirt_plan_id, scp_product_templates.ve_design_id as design_id, scp_product_templates.shirt_background_color as bg_color, scp_product_settings.selling_price as price, scp_product_templates.template_properties, scp_product_settings.art_setting, scp_product_settings.canvas_setting');
        $this->db->JOIN('scp_product_templates', 'scp_products.product_id = scp_product_templates.product_id');
        $this->db->JOIN('scp_product_settings', 'scp_product_templates.product_template_id  = scp_product_settings.product_template_id');
        $this->db->JOIN('scp_product_template_settings', 'scp_product_templates.product_template_setting_id  = scp_product_template_settings.product_template_setting_id');

        $this->db->where('scp_products.product_id', $product_id);
        $this->db->order_by('scp_product_templates.template_id, scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id');
        
        $this->db->group_by('shirt_plan_id');
        $result = $this->db->get('scp_products')->result_array();
        return $result;
    }

    public function get_campaign_colors_by_style_id($product_id, $plan_id){ //setting update campaign items

        $this->db->select(' scp_product_settings.shirt_color_id as shirt_colors'  );
        $this->db->JOIN('scp_product_templates', 'scp_products.product_id = scp_product_templates.product_id');
        $this->db->JOIN('scp_product_settings', 'scp_product_templates.product_template_id  = scp_product_settings.product_template_id');
        $this->db->JOIN('scp_product_template_settings', 'scp_product_templates.product_template_setting_id  = scp_product_template_settings.product_template_setting_id');

        $arr = array('scp_products.product_id' => $product_id ,'scp_product_settings.shirt_style_id'=>  $plan_id);
        $this->db->where($arr);
         $this->db->group_by('scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id');
        $result = $this->db->get('scp_products')->result_array();

        $arr_colors = Array();
        foreach ($result as $key => $items) {
             $arr_colors[] .=$items['shirt_colors'];            
        }
        return $arr_colors;
    }

    public function get_by_campaign_design_type($template_type_id){
        $this->db->where('id', $template_type_id);
        return $this->db->get('scp_design_template_categories')->row_array();
    }

    public function get_by_campaign_design_parent($template_id){
        $this->db->where('template_id', $template_id);
        return $this->db->get('scp_design_template_children')->row_array();
    }

    public function get_user_active_campaigns($id){ // need to get campaigns under user logged in
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_status' => 1, 'scp_campaigns.campaign_ended !=' => 1 );
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $return = $this->db->get_where('scp_campaigns', $row )->result_array();
        return $return;
    }

    public function get_user_campaigns_ended_soon($id){ // need to get campaigns under user logged in
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_status' => 1, 'scp_campaigns.campaign_ended !=' => 1 );
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $result = $this->db->get_where('scp_campaigns', $row )->result_array();
        $count = 0;
        
        foreach ($result as $key => $value) {            
            $now = time(); // or your date as well
            $end_date = strtotime( $value['end_date'] );
            $datediff = $end_date - $now;
            $days_remaining = round((($datediff/24)/60)/60);

            if( 0 <= $days_remaining && $days_remaining <= 7 ){
               $count++;
            }
        }

        return $count;
    }

    public function get_status_campaign(){ // used to get campaign status 1 and current date is greater that end date
        $now = date('Y-m-d H:i:s'); 
        $row =  array( 'end_date <=' => $now, 'campaign_status' => 1 );
        $result = $this->db->get_where('scp_campaigns', $row )->result();
        return $result;
    }

    public function auto_update_status($id, $status, $slug){ // used to update campaign status ended
        $array = array('end_date' => date('Y-m-d'), 'campaign_status' => $status,  'campaign_url' => $slug );
        $this->db->where('campaign_id', $id);
        return $this->db->update('scp_campaigns', $array);
    }
    
    public function update_status($id, $status, $slug){ // used to update campaign status ended
        $array = array('campaign_status' => $status,  'campaign_url' => $slug );
        $this->db->where('campaign_id', $id);
        return $this->db->update('scp_campaigns', $array);
    }

    public function update_campaign_fields( $id, $field, $val ){ //scpv1-1 update attempts
        $arr = array('campaign_id'=> $id);
        $this->db->where( $arr );
        $this->db->set($field, $val);
        $return_id = $this->db->update( 'scp_campaigns'); 
        return $return_id;
    }

    public function update_publish_campaign($id, $status){ // used to update campaign status active
        $array = array('campaign_status' => $status );
        $this->db->where('campaign_id', $id);
        return $this->db->update('scp_campaigns', $array);
    }

    public function update_status_extended($id, $newslug){ // used to update campaign ended to extend duration
        $array = array('campaign_ended' => 1, 'campaign_ended' => 1, 'campaign_url' => $newslug );
        $this->db->where('campaign_id', $id);
        return $this->db->update('scp_campaigns', $array);
    }

    public function force_end_campaign($campaign_id){ // account to forced campaign to end
        $array = array('end_date' => date('Y-m-d'), 'campaign_status' => 3);
        $this->db->where('campaign_id', $campaign_id);
        return $this->db->update('scp_campaigns', $array);
    }

    public function add_campaign_details($data, $status){ // campaign preview add campaign details and also used to create another campaign that extend duration 
        $row =  array(                        
                    'store_id' => STOREID,
                    'campaign_user_id' => $data["campaign_user_id"],
                    'school_id' => $data["school_id"],
                    'campaign_url' => $data["campaign_url"],
                    'campaign_title' => $data["campaign_title"],
                    'campaign_benefits' => $data["campaign_benefits"],
                    'campaign_description' => $data["campaign_description"],
                    'campaign_hashtags' => $data["campaign_hashtags"],
                    'campaign_goal' => $data["campaign_goal"],
                    'campaign_cover_photo' => $data["campaign_cover_photo"],
                    'campaign_feature' => $data["campaign_feature"],
                    'campaign_contact' => $data["campaign_contact"],
                    'campaign_email' => $data["campaign_email"],
                    'campaign_account_name' => $data["campaign_account_name"],
                    'start_date' => $data["start_date"],
                    'end_date' => $data["end_date"],
                    'supplier_code' => $data["supplier_code"],
                    'campaign_status' => $status,
                    'date_created' => date("Y-m-d H:i:s")
                    );

        $this->db->insert('scp_campaigns', $row);
        return $this->db->insert_id();
    }

     public function add_design_details($product_id, $camp_id){ // campaign preview add campaign details and also used to create another campaign that extend duration 
        $row =  array(                        
                    'campaign_id' => $camp_id,
                    'product_id' => $product_id,
                    'date_created' => date("Y-m-d H:i:s")
                    );

        $saved = $this->db->insert('scp_campaign_products', $row);
        return $saved;
    }

    public function update_campaign_details($data, $campaign_id){
        $this->db->where( 'campaign_id', $campaign_id );
        $row =  array(
                    'campaign_url' => $data["campaign_url"],
                    'campaign_title' => $data["campaign_title"],
                    'campaign_benefits' => $data["campaign_benefits"],
                    'campaign_description' => $data["campaign_description"],
                    'campaign_hashtags' => $data["campaign_hashtags"],
                    'campaign_goal' => $data["campaign_goal"],
                    'campaign_cover_photo' => $data["campaign_cover_photo"],
                    'campaign_contact' => $data["campaign_contact"],
                    'campaign_email' => $data["campaign_email"],
                    'campaign_account_name' => $data["campaign_account_name"],
                    'start_date' => $data["start_date"],
                    'end_date' => $data["end_date"],
                    'campaign_status' => $data["campaign_status"],
                    'date_created' => date("Y-m-d H:i:s")
                    );
        
        return $this->db->update( 'scp_campaigns', $row);
    }

    public function update_campaign_div($data, $campaign_id){ // update campaign left container section
        $this->db->where( 'campaign_id', $campaign_id );        
        return $this->db->update( 'scp_campaigns', $data);
    }

    public function save_payee_details($camp_id, $data){ // add customer addresses by campaign id
        $row =  array(
                    'customer_id' => $data["campaign_user_id"],
                    'address_type' => 'Campaign Payee',
                    'name' => $data["campaign_account_name"],
                    'contact_no' => $data["campaign_contact"],
                    'email' => $data["campaign_email"],
                    'address_1' => $data["address_1"],
                    'address_2' => $data["address_2"],
                    'city' => $data["city"],
                    'state' => $data["state"],
                    'zip' => $data["zip"],
                    'campaign_id' => $camp_id,
                    'date_created' => date("Y-m-d H:i:s")
                    );

        $saved = $this->db->insert('scp_customer_addresses', $row);
        return $saved;
    }        

    public function update_payee_details($camp_id, $data){
        $this->db->where( 'campaign_id', $camp_id );    
        $this->db->where( 'address_type', 'Campaign Payee' );    
        $row =  array(
                    'name' => $data["campaign_account_name"],
                    'contact_no' => $data["campaign_contact"],
                    'email' => $data["campaign_email"],
                    'address_1' => $data["address_1"],
                    'address_2' => $data["address_2"],
                    'city' => $data["city"],
                    'state' => $data["state"],
                    'zip' => $data["zip"]
                    );

        return $this->db->update('scp_customer_addresses', $row);
    }

    public function get_campaign_shirt_styles(){
         $this->db->select("scp_shirt_alternative_group.*, scp_shirt_alternative_plan.shirt_style_id,scp_inventory_shirt_style.shirt_group_id as newgroup");
        $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
        $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
        $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
        $this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
        
        $result = $this->db->get("scp_shirt_color_alternatives")->result();
        return $result;
    }


    public function get_all_shirt_alternative(){

         $this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value, scp_shirt_alternative_group.id");
            $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
            $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
            $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
            $result = $this->db->get("scp_shirt_color_alternatives")->result();
        return $result;
    }

    public function get_campaign_allshirt_styles($styles){
        $this->db->select("scp_shirt_alternative_group.id, scp_shirt_alternative_group.shirt_group_id, scp_shirt_alternative_group.group_name, scp_shirt_alternative_group.front_image_link ");
        $this->db->where_in('scp_shirt_alternative_group.id', $styles);
        $result = $this->db->get('scp_shirt_alternative_group')->result();
        return $result;       
    }

    public function get_shirt_category(){
        return $this->db->get("scp_inventory_shirt_group")->result();
        
    }
    public function get_cat(){
        return $this->db->get("scp_design_template_categories")->result();
        
    }
    public function get_campaign_styles($groupid){
        $this->db->select('scp_inventory_shirt_group.*,scp_inventory_shirt_style.* ,scp_shirt_alternative_group.*');
        $this->db->JOIN("scp_inventory_shirt_group", 'scp_inventory_shirt_style.shirt_group_id = scp_inventory_shirt_group.shirt_group_id');
        $this->db->JOIN("scp_shirt_alternative_plan", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $this->db->JOIN("scp_shirt_alternative_group", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
        $this->db->where("scp_inventory_shirt_group.shirt_group_id", $groupid);
        $this->db->group_by("scp_inventory_shirt_group.shirt_group_id");
        return $this->db->get("scp_inventory_shirt_style")->result();
    }

    public function get_design_temps(){
        $this->db->JOIN('scp_design_template_categories', 'scp_design_templates.template_type_id = scp_design_template_categories.id');
        $arr  = array('scp_design_templates.is_active' => 1);
        $this->db->where($arr);
       return $this->db->get('scp_design_templates')->result();
    }

    public function design_temp_category($catid){
        $this->db->JOIN('scp_design_template_categories', 'scp_design_templates.template_type_id = scp_design_template_categories.id');
        $arr  = array('scp_design_templates.is_active' => 1 ,'scp_design_templates.template_type_id' => $catid , 'scp_design_template_categories.parent_id' => 0);
        $this->db->where($arr);
        return $this->db->get('scp_design_templates')->result();
    }

    public function get_group_plans( $shirt_group_id ){          
        $this->db->JOIN('scp_shirt_alternative_plan', 'scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id');
        $this->db->JOIN('scp_shirt_color_alternatives', 'scp_shirt_alternative_plan.id = scp_shirt_color_alternatives.shirt_alternative_id', 'LEFT');
        $this->db->JOIN('scp_shirt_colors', 'scp_shirt_color_alternatives.shirt_color_id = scp_shirt_colors.color_id', 'left');
        $this->db->JOIN('scp_inventory_shirt_color', 'scp_shirt_colors.color_id = scp_inventory_shirt_color.generic_color_id', 'left');
        $this->db->JOIN('scp_inventory_shirt_size', 'scp_inventory_shirt_color.shirt_color_id = scp_inventory_shirt_size.shirt_color_id', 'left');

        $arr = array('scp_shirt_alternative_group.id' => $shirt_group_id);
        $this->db->where($arr);
        $this->db->group_by('scp_shirt_alternative_group.shirt_group_id' , $shirt_group_id);
        return   $this->db->get('scp_shirt_alternative_group')->result();
     }

    public function design_temp_colors(){
        return $this->db->get("scp_shirt_colors")->result();
    }

    public function design_temp_colors2($color){
        $this->db->where('color_id', $color);
        return $this->db->get("scp_shirt_colors")->result();
    }

    public function design_getcolors($shirt_group_id){
        $this->db->JOIN('scp_shirt_color_alternatives', 'scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id', 'LEFT');
       $this->db->JOIN(' scp_shirt_alternative_plan', 'scp_shirt_color_alternatives.shirt_alternative_id =  scp_shirt_alternative_plan.id', 'LEFT');
       $this->db->JOIN(' scp_shirt_alternative_group', 'scp_shirt_alternative_plan.shirt_alternative_group_id =  scp_shirt_alternative_group.id', 'LEFT');
       $this->db->where('scp_shirt_alternative_group.id', $shirt_group_id);
        return $this->db->get("scp_shirt_colors")->result();
    }

    public function get_shirt_plan_by_id( $planId ) { 
        $this->db->select("scp_shirt_alternative_group.id, scp_shirt_alternative_group.shirt_group_id, scp_shirt_alternative_group.group_name, scp_shirt_alternative_group.front_image_link ");

        $this->db->where("scp_shirt_alternative_group.id", $planId);
        $result = $this->db->get('scp_shirt_alternative_group')->result();

        return $result;
    }

    public function get_all_colors_by_plan_id( $planId ) {
        
            $this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value, scp_shirt_alternative_plan.shirt_style_id");

            $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id", "left");
            $this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id", "left");
            $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
            $row =  array( 'scp_shirt_alternative_group.id' => $planId, 'scp_shirt_alternative_plan.is_active' => 1 );
            $this->db->where( $row );
            $this->db->group_by( "scp_shirt_colors.color_code" );
            $this->db->order_by( "scp_shirt_alternative_plan.id", 'DESC' );
            
            $result = $this->db->get("scp_shirt_alternative_group")->result();
             return $result;
    }

    public function get_all_colors_by_plan_id2($planId){
            $this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value");
            $this->db->join("scp_shirt_color_alternatives", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
            $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
            $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");

            $row =  array( 'scp_shirt_alternative_group.id' => $planId, 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active !=' => 0 );
            $this->db->where( $row );
            
            $result = $this->db->get("scp_shirt_colors")->result();
            return $result;
    }




    public function get_plan_colors_by_colorid($arr_colors){
        $this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value");
        $this->db->where_in("scp_shirt_colors.color_id",$arr_colors);
        $result  = $this->db->get('scp_shirt_colors')->result();

        return $result;

    }

    public function get_plan_colors(){
        $this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value");
        $result  = $this->db->get('scp_shirt_colors')->result();

        return $result;

    }

    public function get_plan_allcolors_by_colorid($styles, $arr_colors){
        $this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_color_id = scp_shirt_colors.color_id");
        $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.id = scp_shirt_color_alternatives.shirt_alternative_id");
        $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
        $this->db->where_in("scp_shirt_colors.color_id",$arr_colors);

        $this->db->where_in("scp_shirt_alternative_group.id", $styles);
        $this->db->group_by("scp_shirt_alternative_group.id");
        $result = $this->db->get("scp_shirt_colors")->result();

        return $result;

    }

    public function get_price_by_plan_id($planId){        

            $this->db->SELECT('MAX(scp_inventory_shirt_size.wholesale_price) as maxprice , scp_shirt_alternative_group.id, scp_inventory_shirt_color.shirt_color_id , scp_inventory_shirt_color.shirt_color_code, scp_inventory_shirt_color.color_name,  scp_shirt_alternative_group.group_name ,scp_shirt_alternative_group.front_image_link ,scp_shirt_alternative_plan.shirt_style_id , scp_shirt_alternative_group.shirt_group_id , 
                (select scp_shirt_colors.hex_value from scp_shirt_colors where scp_shirt_colors.color_id=scp_inventory_shirt_size.shirt_color_id) as hex_value');

            $this->db->JOIN(' scp_inventory_shirt_color' , 'scp_inventory_shirt_color.shirt_color_id = scp_inventory_shirt_size.shirt_color_id ');
            $this->db->JOIN('scp_inventory_shirt_style ','scp_inventory_shirt_style.shirt_style_id = scp_inventory_shirt_color.shirt_style_id');

            $this->db->JOIN('scp_shirt_alternative_plan',  'scp_shirt_alternative_plan.shirt_style_id = scp_inventory_shirt_style.shirt_style_id'); 

            $this->db->JOIN('scp_shirt_alternative_group ','scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id'); 
            $row =  array( 'scp_shirt_alternative_group.id' => $planId, 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active !=' => 0 );

            $this->db->where( $row );
            $result = $this->db->get('scp_inventory_shirt_size')->result(); 


            return $result;
    }

     public function get_plan_by_multi_id($planId){
        $this->db->select("scp_shirt_alternative_group.id, scp_inventory_shirt_group.shirt_group_id , scp_shirt_alternative_plan.shirt_alternative_group_id, scp_inventory_shirt_group.shirt_group_name, scp_inventory_shirt_style.style_name, scp_shirt_alternative_group.front_image_link, scp_shirt_alternative_group.group_name, ");

        $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
       
        $this->db->JOIN("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $this->db->JOIN("scp_inventory_shirt_group", 'scp_inventory_shirt_style.shirt_group_id = scp_inventory_shirt_group.shirt_group_id');
        $this->db->where_in("scp_shirt_alternative_group.id", $planId);


        return $this->db->get('scp_shirt_alternative_plan')->result();


     }


    public function save_product_items($data){ // save to scp_products per design  
        $result = $this->db->insert('scp_products',$data);
        return $this->db->insert_id();
    }

    public function save_product_template_settings($template_id, $custom){
        $data = array(
            'template_id' => $template_id,
            'custom_schema' => $custom,
            'date_created' => date('Y-m-d H:i:s')
        );

        $result = $this->db->insert('scp_product_template_settings',$data);

        return $this->db->insert_id();
    }


    public function update_product_template_settings($prodtempsettings_id, $template_id, $custom){
        
        $data = array(
            'template_id' => $template_id,
            'custom_schema' => $custom,
            'date_updated' => date('Y-m-d H:i:s')

            );
        
        $this->db->where('product_template_setting_id', $prodtempsettings_id);

        $result = $this->db->update('scp_product_template_settings', $data );          
        
         if( $result ){
            $this->db->select('product_template_setting_id');
            $this->db->where('product_template_setting_id', $prodtempsettings_id);
            $ptsid = $this->db->get('scp_product_template_settings');

            if ($ptsid->num_rows() > 0)
              {
                // just get the first row
                $row = $ptsid->row();
                return $row->product_template_setting_id;
              }


             return FALSE;
        }
 
    }


    public function save_product_to_templates($data){
       
        $result =  $this->db->insert('scp_product_templates', $data);
        
        return $this->db->insert_id();

    }

     public function update_product_to_templates($data, $product_id, $template_id){

        $arr = array('product_id' => $product_id, 'template_id' => $template_id);
        $this->db->where($arr);
        $result =  $this->db->update('scp_product_templates', $data);
        
        if( $result ){
            $this->db->select('product_template_id');
            $this->db->where($arr);
            $ptsid = $this->db->get('scp_product_templates');

            if ($ptsid->num_rows() > 0)
              {
                // just get the first row
                $row = $ptsid->row();
                return $row->product_template_id;
              }


             return FALSE;
        }


    }


    public function save_product_settings($id, $shirt_colors, $shirt_style, $price, $art_setting, $canvas_setting){

              
         for( $x = 0; $x < count($shirt_colors) ; $x++ )
            {

                    $dataprodsettings = array(
                        'product_template_id' => $id,
                        'product_sku' => '',                        
                        'shirt_style_id' => $shirt_style, 
                        'shirt_color_id' => $shirt_colors[$x],                        
                        'selling_price' =>  $price,
                        'art_setting' =>  $art_setting,
                        'canvas_setting' =>  $canvas_setting,
                        'date_created' => date('Y-m-d H:i:s')
                     ); 

                    $result = $this->db->insert('scp_product_settings', $dataprodsettings );
            }

        return  $result;  
          


    }

    public function get_campaign_school($campaign_id){
        $this->db->select('scp_campaigns.school_id');
         $this->db->JOIN('scp_school',  'scp_campaigns.school_id = scp_school.school_id', 'LEFT' );
        $this->db->where('scp_campaigns.campaign_id', $campaign_id);
        $result = $this->db->get('scp_campaigns')->row_array();
        return $result;
    }

    public function get_campaign_products($campaign_id){
        $this->db->select('product_id');
         $arr = array('campaign_id' => $campaign_id, 'active' => 1 ) ;
        $this->db->where( $arr );
        // $this->db->where('campaign_id', $campaign_id);
        $result = $this->db->get('scp_campaign_products')->result();
        return $result;
    }

    public function get_product_items($product_id){
        $this->db->select('scp_products.thumbnail1 as template_image, scp_product_templates.template_id, scp_product_templates.template_properties as properties, scp_product_templates.shirt_background_color , scp_product_settings.shirt_style_id as shirt_style, scp_product_settings.shirt_color_id as shirt_colors, scp_product_settings.selling_price, scp_product_template_settings.template_id as parent_id , scp_products.product_id, scp_product_templates.ve_design_id, scp_product_settings.art_setting, scp_product_settings.canvas_setting' );
        $this->db->JOIN('scp_product_templates', 'scp_products.product_id = scp_product_templates.product_id');
        $this->db->JOIN('scp_product_settings', 'scp_product_templates.product_template_id  = scp_product_settings.product_template_id');
        $this->db->JOIN('scp_product_template_settings', 'scp_product_templates.product_template_setting_id  = scp_product_template_settings.product_template_setting_id');

        $this->db->where('scp_products.product_id', $product_id);
        $this->db->order_by('scp_product_templates.template_id, scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id');
        
        $this->db->group_by('scp_product_settings.shirt_style_id');
        $result = $this->db->get('scp_products')->result_array();
        return $result;
    }

    public function get_colors_by_style_id($product_id, $style_id){

        $this->db->select(' scp_product_settings.shirt_color_id as shirt_colors ' );
        $this->db->JOIN('scp_product_templates', 'scp_products.product_id = scp_product_templates.product_id');
        $this->db->JOIN('scp_product_settings', 'scp_product_templates.product_template_id  = scp_product_settings.product_template_id');
        $this->db->JOIN('scp_product_template_settings', 'scp_product_templates.product_template_setting_id  = scp_product_template_settings.product_template_setting_id');

        $arr = array('scp_products.product_id' => $product_id ,'scp_product_settings.shirt_style_id'=>  $style_id);
        $this->db->where($arr);
         $this->db->group_by('scp_product_settings.shirt_style_id, scp_product_settings.shirt_color_id');
        $result = $this->db->get('scp_products')->result_array();

        $arr_colors = Array();
        foreach ($result as $key => $items) {
             $arr_colors[] .=$items['shirt_colors'];            
        }
        return $arr_colors;
    }
 

    public function update_product_items($data, $id){
        
            $this->db->where('product_id', $id);
            $result = $this->db->update('scp_products', $data );          
             
             return $id;  

    }

    public function get_product_template_settings($id){
        $this->db->select('scp_product_templates.product_template_setting_id');
        $this->db->JOIN('scp_product_templates', 'scp_product_template_settings.product_template_setting_id = scp_product_templates.product_template_setting_id');
        $this->db->where_in('scp_product_templates.product_id', $id);
        return $this->db->get('scp_product_template_settings')->result_array();

    }

    public function del_product_tempSettings($id){
        foreach ($id as $key => $prodsettingsId) {
            # code...
            $this->db->where('product_template_setting_id',$prodsettingsId['product_template_setting_id']);
            return $this->db->delete('scp_product_template_settings');
        }

    }


     public function get_product_settings($productTempId){
        $this->db->select('product_template_id');

        $this->db->where('product_template_id',$productTempId);
        return $this->db->get('scp_product_settings')->result();
    }


     public function del_product_settings($id){
        $newTempid = Array();    
        foreach ($id as $key => $value) {
            $newTempid[] .= $value->product_template_id;
        }
        $this->db->where_in('product_template_id',$newTempid);
        return $this->db->delete('scp_product_settings');
    }

    public function del_product_temps($id){
        $this->db->where_in('product_template_id',$id);
        return $this->db->delete('scp_product_templates');
    }


    public function delete_campaign_products($campaign_id){
        $arr = array(
                'campaign_id'=> $campaign_id
            );

        $this->db->where($arr);
        return $this->db->delete('scp_campaign_products');
    }

    public function insert_campaign_products($campaign_id, $product_id){

        $arr = array(
                'campaign_id'=> $campaign_id,
                'product_id'=> $product_id,
                'active'=> 1,
                'date_created' => date('Y-m-d H:i:s')
            );
       
        return $this->db->insert('scp_campaign_products', $arr);
    }

    public function update_campaign_products($data_arr ,$campaign_id, $product_id){

        $arr_where = array(
            'campaign_id' => $campaign_id            
        );
        
        $this->db->where($arr_where);
        $this->db->where_in('product_id' , $product_id);
        $this->db->update('scp_campaign_products', $data_arr);
        return true;
    }

    public function get_colors_temps(){
        $this->db->select('template_id, scp_shirt_colors.color_id, scp_shirt_colors.hex_value ');
        $this->db->JOIN('scp_design_templates',  'scp_design_templates.shirt_color_id = scp_shirt_colors.color_id');
        return $this->db->get('scp_shirt_colors')->result();
    }


    public function get_sale_order_items_productid($product_id){
        $this->db->select('scp_products.product_id');
        $this->db->JOIN('scp_campaign_products', 'scp_products.product_id = scp_campaign_products.product_id');
        $this->db->JOIN('scp_sales_order_items', 'scp_products.product_id = scp_sales_order_items.product_id');
        $this->db->where_in('scp_sales_order_items.product_id', $product_id);
        return $this->db->get('scp_products')->result();
    }



    public function get_campaign_products_items($campaign_id){
        $this->db->select('product_id');
        
            $arr = array('campaign_id' => $campaign_id, 'active' => 1 ) ;
        $this->db->where( $arr );
        $result = $this->db->get('scp_campaign_products')->result();

        return $result;
    }




}