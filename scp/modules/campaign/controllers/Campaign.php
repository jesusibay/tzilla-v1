<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Campaign extends MX_Controller {

	function __construct() {
		
		parent::__construct();
									
		$this->load->module('Settings', 'admin');
		$this->load->model(	array('account/Account_model', 'Campaigns_model', 'signin/Signin_model', 'signin/Fb_user_model', 'admin/Image_manipulator_model', 'admin/Search_model', 'Settings/Category_model', 'inventory/Inventory_model') );
		$this->load->helper('url');
		$this->load->helper('security');
        $this->load->library( array('form_validation', 'Aauth', 'Curl', 'session', 'pagination', 'upload', 'session') );


        $this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 	
		date_default_timezone_set('America/Los_Angeles');	
	}

	public function create($campaign_id=0 , $styleID = 0, $invGroupID = 0){
			$this->session->unset_userdata("go_back");
			if($this->session->is_logged_in==FALSE){
				redirect( base_url() );
			}else{

				$data['styleID'] = $styleID;
				$data['inventoryGroupID'] = $invGroupID;
				$data_arr = Array();
				//set session get session data
				if( $campaign_id == 0){
					
					if(!$this->session->product_details ){
						redirect( base_url('school/store') );
					}

					$data['product_details'] = $this->session->userdata('product_details');
										

					$data['get_style_colors'] = $this->Campaigns_model->get_plan_colors();
					$data['shirt_styles'] = $this->Campaigns_model->get_campaign_shirt_styles(); 			

					$shirt_group = $this->Campaigns_model->get_all_shirt_alternative();
					$ctr = 0;
					$colorctr = 0;
					$prev_shirt = 0;
					foreach ($shirt_group as $key => $value) {
						$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;

						if ( $value->id == $prev_shirt ) {
							$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
							$colorctr++;
						} else {
							$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;
							$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
							$colorctr++;
							$ctr++;
						}

						$prev_shirt = $value->id;
					}

					$data['design_cat'] = $this->Campaigns_model->get_all_categories();
					$data['shirt_category'] = $this->Campaigns_model->get_shirt_category(); 
					
					$data['campaign_id'] = $campaign_id;
					$data['templates'] = $this->Campaigns_model->get_design_temps();
					$data['generic_colors'] = $this->Campaigns_model->get_colors_temps();
					/*this is for sizechart*/
					$styleID = $this->input->post('styleId');
					$groupID = $this->input->post('groupStyleId');
					
					$data['category'] = $this->Inventory_model->get_shirt_category();

					if ( $styleID == 0 ) {
						// get all shirt size chart
						$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $groupID );
						
					} else {
						// get size chart by shirt group id
						$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
						// $data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID );
					}

					$this->load->view_store('manage-campaign-items', $data);

				}else{ //get campaign manage items 
					
					$data['customer_id'] = $this->session->customer_id;
					$data['campaigns'] = $this->Campaigns_model->get_by_campaign_id( $campaign_id );
					if(!$this->session->product_details ){ //from database
						
					
						$campaign_product = $this->Campaigns_model->get_campaign_products($campaign_id);

						$campaigns = $this->Campaigns_model->get_campaign_school($campaign_id);
						$data['school_id'] = $campaigns['school_id'];
						
						$shirtstyle = Array();
						$template_ids = '';
						$template_image = '';
						$properties = '';
						$template_id = '';
						$parent_id = '';
						$prevTemplate = "";
						$prevStyle = "";
						$shirtcolor1 = "";
						$shirtstyle1 = '';
						

						foreach ($campaign_product as $key => $product_val) {
							$arr_prod = $product_val->product_id;

							$product_items = $this->Campaigns_model->get_product_items($arr_prod);
								$color_style_id = "";
								$shirtsPerColor = [];
								foreach ($product_items as $key => $items) {
									$color_style_id = $this->Campaigns_model->get_colors_by_style_id($items['product_id'], $items['shirt_style']);
									
									$data_arr[] = array( 'template_id'=>  $items['template_id'], 'parent_id'=>$items['parent_id'] , 'shirt_style' => $items['shirt_style'], 'shirt_colors' => $color_style_id ,'template_image'=>$items['template_image']  , 'properties'=> $items['properties'] , 'selling_price'=> $items['selling_price'], 'back_color'=>$items['shirt_background_color'], 'product_id'=>$items['product_id'] ,'campaign_id'=>$campaign_id , 'design_id'=>$items['ve_design_id'] );

								}
								
						
						}
							$data['product_details'] = $data_arr ;
							
							$data['get_style_colors'] = $this->Campaigns_model->get_plan_colors();
							$data['shirt_styles'] = $this->Campaigns_model->get_campaign_shirt_styles(); 
							$shirt_group = $this->Campaigns_model->get_all_shirt_alternative(); 
							$ctr = 0;
							$colorctr = 0;
							$prev_shirt = 0;
							foreach ($shirt_group as $key => $value) {
								$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;

								if ( $value->id == $prev_shirt ) {
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
									$colorctr++;
								} else {
									$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
									$colorctr++;
									$ctr++;
								}

								$prev_shirt = $value->id;
							}

							$data['design_cat'] = $this->Campaigns_model->get_all_categories();
							$data['shirt_category'] = $this->Campaigns_model->get_shirt_category(); 				
							$data['campaign_id'] = $campaign_id;
							$data['templates'] = $this->Campaigns_model->get_design_temps();
							$data['generic_colors'] = $this->Campaigns_model->get_colors_temps();
							//for size chart
							$styleID = $this->input->post('styleId');
							$groupID = $this->input->post('groupStyleId');
							
							$data['category'] = $this->Inventory_model->get_shirt_category();

							if ( $styleID == 0 ) {
								// get all shirt size chart
								$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $groupID );
								
							} else {
								// get size chart by shirt group id
								$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
								// $data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID );
							}

							$this->load->view_store('manage-campaign-items-edit', $data);
						
					} else{ 
						### this is from TGEN when campaign is editted
						$data['product_details'] = $this->session->product_details ;
						$campaigns = $this->Campaigns_model->get_campaign_school($campaign_id);
						$data['school_id'] = $campaigns['school_id'];
						
						$data['get_style_colors'] = $this->Campaigns_model->get_plan_colors();
							$data['shirt_styles'] = $this->Campaigns_model->get_campaign_shirt_styles(); 
							$shirt_group = $this->Campaigns_model->get_all_shirt_alternative();
							$ctr = 0;
							$colorctr = 0;
							$prev_shirt = 0;
							foreach ($shirt_group as $key => $value) {
								$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;

								if ( $value->id == $prev_shirt ) {
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
									$colorctr++;
								} else {
									$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
									$colorctr++;
									$ctr++;
								}

								$prev_shirt = $value->id;
							}

							$data['design_cat'] = $this->Campaigns_model->get_all_categories();
							$data['shirt_category'] = $this->Campaigns_model->get_shirt_category(); 				
							$data['campaign_id'] = $campaign_id;
							$data['templates'] = $this->Campaigns_model->get_design_temps();
							$data['generic_colors'] = $this->Campaigns_model->get_colors_temps();
							//for size chart
							$styleID = $this->input->post('styleId');
							$groupID = $this->input->post('groupStyleId');
							
							$data['category'] = $this->Inventory_model->get_shirt_category();

							if ( $styleID == 0 ) {
								// get all shirt size chart
								$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $groupID );
								
							} else {
								// get size chart by shirt group id
								$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
							}

						$this->load->view_store('manage-campaign-items-edit', $data);
					}

				}		
		}
	}


	public function edit_items($campaign_id=0 , $styleID = 0, $invGroupID = 0){
					//get campaign manage items 
					$data['styleID'] = $styleID;
					$data['inventoryGroupID'] = $invGroupID;
					$data_arr = Array();

					$this->session->unset_userdata("go_back");
					if(!$this->session->product_details ){ //from database
						
						$data['customer_id'] = $this->session->customer_id;
						$data['campaigns'] = $this->Campaigns_model->get_by_campaign_id( $campaign_id );
						$campaign_product = $this->Campaigns_model->get_campaign_products($campaign_id);

						$campaigns = $this->Campaigns_model->get_campaign_school($campaign_id);
						$data['school_id'] = $campaigns['school_id'];
						
						$shirtstyle = Array();
						$template_ids = '';
						$template_image = '';
						$properties = '';
						$template_id = '';
						$parent_id = '';
						$prevTemplate = "";
						$prevStyle = "";
						$shirtcolor1 = "";
						$shirtstyle1 = '';
						$parent_id = '';
						

						foreach ($campaign_product as $key => $product_val) {
							$arr_prod = $product_val->product_id;

							$product_items = $this->Campaigns_model->get_product_items($arr_prod);
								$color_style_id = "";
								$shirtsPerColor = [];
								foreach ($product_items as $key => $items) {	
									$color_style_id = $this->Campaigns_model->get_colors_by_style_id($items['product_id'], $items['shirt_style']);
									
									$parent = $this->Campaigns_model->get_by_campaign_design_parent( $items['template_id']); 
									
									$parent_id = (!empty($parent['parent_template_id']))? $parent['parent_template_id'] : $items['template_id'];

									$data_arr[] = array( 'template_id'=>  $items['template_id'], 'parent_id'=> $parent_id, 'shirt_style' => $items['shirt_style'], 'shirt_colors' => $color_style_id ,'template_image'=>$items['template_image'], 'properties'=> $items['properties'] , 'selling_price'=> $items['selling_price'], 'back_color'=>$items['shirt_background_color'], 'product_id'=>$items['product_id'] ,'campaign_id'=>$campaign_id , 'design_id'=>$items['ve_design_id'],
										'art_position'=>$items['art_setting'], 'canvas_position' => $items['canvas_setting'] );

								}
								
						
						}

							$data['product_details'] = $data_arr ;							
							$data['get_style_colors'] = $this->Campaigns_model->get_plan_colors();
							$data['shirt_styles'] = $this->Campaigns_model->get_campaign_shirt_styles(); 
							$shirt_group = $this->Campaigns_model->get_all_shirt_alternative();
							$ctr = 0;
							$colorctr = 0;
							$prev_shirt = 0;
							foreach ($shirt_group as $key => $value) {
								$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;

								if ( $value->id == $prev_shirt ) {
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
									$colorctr++;
								} else {
									$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;

									$colorctr++;
									$ctr++;
								}

								$prev_shirt = $value->id;
							}

							$data['design_cat'] = $this->Campaigns_model->get_all_categories();
							$data['shirt_category'] = $this->Campaigns_model->get_shirt_category(); 				
							$data['campaign_id'] = $campaign_id;
							$data['templates'] = $this->Campaigns_model->get_design_temps();
							$data['generic_colors'] = $this->Campaigns_model->get_colors_temps();
							$data['shirt_colors'] = $this->Inventory_model->get_all_shirt_colors(); // load available shirt colors
							//for size chart
							$styleID = $this->input->post('styleId');
							$groupID = $this->input->post('groupStyleId');
							
							$data['category'] = $this->Inventory_model->get_shirt_category();

							if ( $styleID == 0 ) {
								// get all shirt size chart
								$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $groupID );
								
							} else {
								// get size chart by shirt group id
								$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
								// $data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID );
							}

							$this->load->view_store('manage-campaign-items-edit', $data);
						
					} else{ 
						### this is from TGEN when campaign is editted
						$data['customer_id'] = $this->session->customer_id;
						$data['campaigns'] = $this->Campaigns_model->get_by_campaign_id( $campaign_id );
						$data['product_details'] = $this->session->product_details ;	
						$campaigns = $this->Campaigns_model->get_campaign_school($campaign_id);
						$data['school_id'] = $campaigns['school_id'];
						
						$data['get_style_colors'] = $this->Campaigns_model->get_plan_colors();
							$data['shirt_styles'] = $this->Campaigns_model->get_campaign_shirt_styles(); 
							$shirt_group = $this->Campaigns_model->get_all_shirt_alternative();
							$ctr = 0;
							$colorctr = 0;
							$prev_shirt = 0;
							foreach ($shirt_group as $key => $value) {
								$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;

								if ( $value->id == $prev_shirt ) {
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
									$colorctr++;
								} else {
									$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;
									$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
									$colorctr++;
									$ctr++;
								}

								$prev_shirt = $value->id;
							}

							$data['design_cat'] = $this->Campaigns_model->get_all_categories();
							$data['shirt_category'] = $this->Campaigns_model->get_shirt_category(); 				
							$data['campaign_id'] = $campaign_id;
							
							$data['templates'] = $this->Campaigns_model->get_design_temps();
							$data['generic_colors'] = $this->Campaigns_model->get_colors_temps();
							$data['shirt_colors'] = $this->Inventory_model->get_all_shirt_colors(); // load available shirt colors
							//for size chart
							$styleID = $this->input->post('styleId');
							$groupID = $this->input->post('groupStyleId');
							
							$data['category'] = $this->Inventory_model->get_shirt_category();

							if ( $styleID == 0 ) {
								// get all shirt size chart
								$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $groupID );
								
							} else {
								// get size chart by shirt group id
								$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
								// $data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID );
							}
							
						$this->load->view_store('manage-campaign-items-edit', $data);
					}

				
	}


	public function edit_campaign_items(){

					$data['product_details'] = $this->session->product_details ;
					
					$data['get_style_colors'] = $this->Campaigns_model->get_plan_colors();
					$data['shirt_styles'] = $this->Campaigns_model->get_campaign_shirt_styles(); 
					$shirt_group = $this->Campaigns_model->get_all_shirt_alternative(); 
					$ctr = 0;
					$colorctr = 0;
					$prev_shirt = 0;
					foreach ($shirt_group as $key => $value) {
						$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;

						if ( $value->id == $prev_shirt ) {
							$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
							$colorctr++;
						} else {
							$data['shirt_alternatives'][$ctr]["shirt_id"] = $value->id;
							$data['shirt_alternatives'][$ctr]["shirt_colors"][$colorctr] = $value->color_id;
							$colorctr++;
							$ctr++;
						}

						$prev_shirt = $value->id;
					}

					$data['design_cat'] = $this->Campaigns_model->get_all_categories();
					$data['shirt_category'] = $this->Campaigns_model->get_shirt_category(); 				
					$data['campaign_id'] = $campaign_id;
					$data['templates'] = $this->Campaigns_model->get_design_temps();
					$data['generic_colors'] = $this->Campaigns_model->get_colors_temps();
					//for size chart
					$styleID = $this->input->post('styleId');
					$groupID = $this->input->post('groupStyleId');
					
					$data['category'] = $this->Inventory_model->get_shirt_category();

					if ( $styleID == 0 ) {
						// get all shirt size chart
						$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $groupID );
						
					} else {
						// get size chart by shirt group id
						$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
						// $data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID );
					}
					$this->load->view_store('manage-campaign-items-edit', $data);
	}

	public function buy_or_update_items($campaign_id=0){
		//set session get session data
		$product_details= $this->input->post('product_details');
		if( count( $product_details)  > 0){
			$this->session->set_userdata('product_details', $product_details );
			$data['status']='done';			
		}else{
			$data['status']="error";
		}
		echo json_encode($data);
	}

	public function campaign_update_items($campaign_id=0){
		$product_details= $this->input->post('product_details');
		if( count( $product_details)  > 0){
			$this->session->set_userdata('product_details', $product_details );
			$this->session->set_userdata( "go_back", 'campaign_edit' );
			$data['status']='done';			
		}else{
			$data['status']="error";
		}
		echo json_encode($data);
	}

	public function zero_design($campaign_id=0){
		$this->session->unset_userdata('product_details');
		
		$data['status']='done';		
		echo json_encode($data);
	}

	public function show_styles(){
		$groupid = $this->input->post('shirtgroup');
		$data['shirt_styles'] = $this->Campaigns_model->get_campaign_styles($groupid); 
		$data['showstyles'] = "";
		foreach ($data['shirt_styles'] as $key => $shirtstyle) {
			$active = "";
			$active2 = "";
			if( $groupid == $shirtstyle->shirt_group_id){
				$active = "addmorestyle-shirt-holder-active";
				$active2 = "style-check-active";
			}
			$data['showstyles'] .= '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 design-cats2"><div class="gsemibold font-xsmall gray">'.$shirtstyle->shirt_group_name.'</div><div class="addmorestyle-shirt-holder '.$active.'" shirt-id="'.$shirtstyle->shirt_group_id.'"><span class="style-remove"></span><span class="style-check '.$active2.'"></span><img class="img-responsive img-center styleshirt-col" src="'.base_url($shirtstyle->front_image_link).'" alt="" /></div></div>';
		}
		echo json_encode($data);
	}

	public function show_category(){
		$catid = $this->input->post('catid');
		$data['cat_list'] = $this->Campaigns_model->design_temp_category($catid); 
		$data['designcat'] = "";
		foreach ($data['cat_list'] as $key => $list) {
		$data['designcat'] .= '<div class="design-item-holder" tempid="'.$list->template_id.'"><div class="design-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">'.$list->name.'</span></div><div class="store-image-box"><span class="design-backprint"></span><div class="store-selection-holder"><span class="design-unselect"></span><span class="design-select design-active"></span></div><img class="img-responsive img-center" src='.$list->thumbnail.' alt="" /></div>';
		}
		echo json_encode($data);
	}

	public function add_style(){ //add more styles per design
		$arr_shirtid = 	$this->input->post('shirt_id');
		$style_pricing = array();
		foreach ($arr_shirtid as $style_id) {
			$style_pricing2 = $this->Campaigns_model->get_price_by_plan_id($style_id);
			array_push($style_pricing, $style_pricing2);
		}

		$data['style_pricing'] = $style_pricing;
		$data['add_plans'] = $this->Campaigns_model->get_plan_by_multi_id($arr_shirtid);
		echo json_encode($data);
	}

	public function show_colors_display_opts($planId){
		
		$data['style_plan_colors'] = $this->Campaigns_model->get_all_colors_by_plan_id($planId); 
		
		echo json_encode($data);
	}

	public function show_shirts( $planId = 0 ){

		$styles = $this->input->post("styles");		
		$data['style_plan'] = $this->Campaigns_model->get_shirt_plan_by_id($planId);

		// load all colors saved per style/garment plan ( use plan id )
		$data['style_plan_colors'] = $this->Campaigns_model->get_all_colors_by_plan_id($planId);
		$style_pricing = array();
		foreach ($styles as $style_id) {
			$style_pricing2 = $this->Campaigns_model->get_price_by_plan_id($style_id);
			array_push($style_pricing, $style_pricing2);
		}
		$data['style_pricing'] = $style_pricing;
		$data['shirt_styles'] = $this->Campaigns_model->get_campaign_shirt_styles(); 

		$data['all_shirt_styles'] = $this->Campaigns_model->get_campaign_allshirt_styles($styles); 
		$data['selectbak'] = "";
		if( count($data['style_plan_colors']) > 0 ){ 

			foreach ($data['style_plan_colors'] as $key => $planColors) {
				
					$data['selectbak'] .= '<div class="back-selector selected-color" data-id="'.$planColors->color_id.'" title="'.$planColors->color_name.'" data-hex="'.$planColors->hex_value.'" style="background:#'.$planColors->hex_value.';"><img class="img-responsive " src="'.base_url('public/'.STORE).'/images/check-icon-xs.png" alt="" /></div>';
				
			}
		}
		
		echo json_encode($data);
	}


	public function show_colors($planId){
		
		$data['style_plan_colors'] = $this->Campaigns_model->get_all_colors_by_plan_id($planId); 
		echo json_encode($data);
	}


	public function save_product_items(){
		$product_details = $this->input->post('product_details');
		$product_data = $this->input->post('product_data');
		$signin = $this->Signin_model->get_user_by_email( $this->session->logged_in_email );
		$data['stat'] = "";
		$data['message'] = "";
		if( $signin['status'] == 0 ){
			$data['stat'] = "error";
			$data['message'] = "Please verify your account";
			echo json_encode($data);
		}else{
			
			$customer_id = $this->session->customer_id;
			$user_id = $this->session->id;
			$this->session->set_userdata('product_details', $product_details);
			
			$products = [];
				
				$this->session->set_userdata('product_id', $this->session->product_id);
				if(empty($this->session->product_id)){
					
					foreach ($product_data as $key => $pr_value) {
						$dataprod = array(
							'store_id' => STOREID,
							'customer_id' => $customer_id,
							'thumbnail1' => $pr_value['template_image'],					
							'date_created' => date('Y-m-d H:i:s')
							);
						// save to products
						$prod_id = $this->Campaigns_model->save_product_items($dataprod);	
						
						//save to product template settings
						$prodTempSettings_Id = $this->Campaigns_model->save_product_template_settings($pr_value['template_id'], $pr_value['properties']);
							
							if(  $prod_id > 0 || $prodTempSettings_Id > 0 ){


								$dataTemplateSettings = array(
									'product_id' => $prod_id,
									'template_id' => $pr_value['template_id'],
									've_design_id' => $pr_value['design_id'],
									'product_template_setting_id' => $prodTempSettings_Id,									
									'template_properties' => $pr_value['properties'],
									'back_print' => '',
									'sort_order' => 1,
									'shirt_background_color' => $pr_value['back_color'],
									'set_as_default_design' => 1,
									'date_created' => date('Y-m-d H:i:s')
								
								);	 
								//save to product template 
								$prodTemplates = $this->Campaigns_model->save_product_to_templates($dataTemplateSettings);

								foreach ($pr_value['shirt_style'] as $key => $shirts) {
									// print_r($shirts);
									$shirt_colors = explode(",", $shirts['color'])  ; //array shirt colors						

									$prodSettings = $this->Campaigns_model->save_product_settings($prodTemplates ,$shirt_colors, $shirts['style_id'], $shirts['data_price'], $shirts['art_setting'], $shirts['canvas_setting']);
								
									if( count($prodTemplates) > 0) {
										$data['stat'] = "done";
									}	


								}
							} 
						$products[] = $prod_id; //declare product id
			      			
					} 
					$this->session->set_userdata('product_id', $products); //set session for product id to campaign page
					
				}else{
					// echo "SDADA";
					foreach ($product_data as $key => $pr_value) {
						$dataprod = array(
							'store_id' => STOREID,
							'customer_id' => $customer_id,
							'thumbnail1' => $pr_value['template_image'],					
							'date_created' => date('Y-m-d H:i:s')
							);
						// save to products
						$prod_id = $this->Campaigns_model->save_product_items($dataprod);	
						
						//save to product template settings
						$prodTempSettings_Id = $this->Campaigns_model->save_product_template_settings($pr_value['template_id'], $pr_value['properties']);
							
							if(  $prod_id > 0 || $prodTempSettings_Id > 0 ){


								$dataTemplateSettings = array(
									'product_id' => $prod_id,
									'template_id' => $pr_value['template_id'],
									've_design_id' => $pr_value['design_id'],
									'product_template_setting_id' => $prodTempSettings_Id,									
									'template_properties' => $pr_value['properties'],
									'back_print' => '',
									'sort_order' => 1,
									'shirt_background_color' => $pr_value['back_color'],
									'set_as_default_design' => 1,
									'date_created' => date('Y-m-d H:i:s')
								
								);	 
								//save to product template 
								$prodTemplates = $this->Campaigns_model->save_product_to_templates($dataTemplateSettings);

								foreach ($pr_value['shirt_style'] as $key => $shirts) {
									
									$shirt_colors = explode(",", $shirts['color'])  ; //array shirt colors						

									$prodSettings = $this->Campaigns_model->save_product_settings($prodTemplates ,$shirt_colors, $shirts['style_id'], $shirts['data_price'], $shirts['art_setting'], $shirts['canvas_setting']);
								
									if( count($prodTemplates) > 0) {
										$data['stat'] = "done";
									}	


								}
							} 
						$products[] = $prod_id; //declare product id
			      			
					} 
				
					$this->session->set_userdata('product_id', $products); //set session for product id to campaign page
				// }

				
			}

			if($data['stat'] == "done"){
				// $this->session->unset_userdata('product_details');
			}

			echo json_encode($data);
		}
		
	}

	public function update_product_items(){ //update campaign product items  EDIT

		$signin = $this->Signin_model->get_user_by_email( $this->session->logged_in_email );
		$data['stat'] = "";
		$data['message'] = "";
		if( $signin['status'] == 0 ){
			$data['stat'] = "error";
			$data['message'] = "Please verify your account";
			echo json_encode($data);
		}else{ //place updating for product details here

			$data['customer_id'] = $this->session->customer_id;
			$campaign_id = $this->input->post('campaign_id');
			$product_details = $this->input->post('product_details');
			$product_data = $this->input->post('product_data');

				$ctr = 0;
				$ctr_product = count($product_data);
				$product1 = array();
				foreach ($product_data as $key => $checkdeleted) {
									
						$product1[] .= $checkdeleted['product_id'];
						

				}
				$product2 = array();
				
				$checkProductID = $this->Campaigns_model->get_campaign_products_items($campaign_id);
				foreach ($checkProductID as $key => $prod2) {
					$product2[] .= $prod2->product_id;
				}

				
				$result=array_diff($product2, $product1);
					
				if( count($result) > 0 ){
					$data_arr = array(					               
			                'active'=> 0,
			                'date_updated' => date('Y-m-d H:i:s')
	            	);
					$campaign_prods = $this->Campaigns_model->update_campaign_products($data_arr, $campaign_id, $result); //scp campaign_products

				}

				foreach ($product_data as $key => $pr_value) {
					
						
						
						if( $pr_value['product_id'] != 0 ){

										$dataprod = array(
											'store_id' => STOREID,
											'customer_id' =>$this->session->customer_id,
											'thumbnail1' => $pr_value['template_image'],					
											'date_updated' => date('Y-m-d H:i:s')
											);
									// update to products
										$prod_id = $this->Campaigns_model->update_product_items($dataprod , $pr_value['product_id']); //update scp_products						

										$data_arr = array(					               
							                'active'=> 1,
							                'date_updated' => date('Y-m-d H:i:s')
						            	);
										$campaign_prods = $this->Campaigns_model->update_campaign_products($data_arr, $campaign_id, $prod_id); //scp campaign_products

												// $this->Campaigns_model->save_product_template_settings($pr_value['template_id'], $pr_value['properties']);
											
											if(  $prod_id > 0  ){
												
												
												$dataTemplateSettings = array(
												//	'product_id' => $prod_id,
												//	'template_id' => $pr_value['template_id'],
													've_design_id' => $pr_value['design_id'],
													// 'product_template_setting_id' => $prodTempSettings_Id,													
													'template_properties' => $pr_value['properties'],
													'back_print' => '',
													'sort_order' => 1,
													'shirt_background_color' => $pr_value['back_color'],
													'set_as_default_design' => 1,
													'date_updated' => date('Y-m-d H:i:s')
												
												);	 
												
														
												$prodTemplates = $this->Campaigns_model->update_product_to_templates($dataTemplateSettings, $prod_id, $pr_value['template_id']);//scp_product_templates	
														
												
												$productTemSettings = $this->Campaigns_model->update_product_template_settings($prodTemplates, $pr_value['template_id'], $pr_value['properties']); //scp_product_template_settings
												$arr_prodtemplateId= $this->Campaigns_model->get_product_settings($prodTemplates ); // scp_product_settings
												//delete product settings here 
												if( count($arr_prodtemplateId) > 0 ){
													$get_product_settings = $this->Campaigns_model->del_product_settings($arr_prodtemplateId); // scp_product_settings
												

													if( $get_product_settings  ){
														//insert update for product settings
														foreach ($pr_value['shirt_style'] as $key => $shirts) {
															
															$shirt_colors = explode(",", $shirts['color'])  ; //array shirt colors						

															$prodSettings = $this->Campaigns_model->save_product_settings($productTemSettings ,$shirt_colors, $shirts['style_id'], $shirts['data_price'],$shirts['art_setting'],$shirts['canvas_setting']); //scp product settings

															if( count($prodTemplates) > 0) {
																$data['stat'] = "updated";
															}	


														}
													}
												}else{

													foreach ($pr_value['shirt_style'] as $key => $shirts) {
															
															$shirt_colors = explode(",", $shirts['color'])  ; //array shirt colors						

															$prodSettings = $this->Campaigns_model->save_product_settings($productTemSettings ,$shirt_colors, $shirts['style_id'], $shirts['data_price'],$shirts['art_setting'],$shirts['canvas_setting']); //scp product settings

															if( count($prodTemplates) > 0) {
																$data['stat'] = "updated";
															}	


														}
												}
											}

									// }
									

						}else{ 


							
							if( $pr_value['product_id'] == 0 ){

							$dataprod = array(
								'store_id' => STOREID,
								'customer_id' => $this->session->customer_id,
								'thumbnail1' => $pr_value['template_image'],					
								'date_created' => date('Y-m-d H:i:s')
							);
						// save to products
							$prod_id = $this->Campaigns_model->save_product_items($dataprod);	
							$campaign_prods = $this->Campaigns_model->insert_campaign_products($campaign_id, $prod_id);	 //insert to scp_campaign_product	
							$prodTempSettings_Id = $this->Campaigns_model->save_product_template_settings($pr_value['template_id'], $pr_value['properties']); //insert scp product template settings
							if(  $prod_id > 0 || $prodTempSettings_Id > 0 ){


									$dataTemplateSettings = array(
										'product_id' => $prod_id,
										'template_id' => $pr_value['template_id'],
										've_design_id' => $pr_value['design_id'],
										'product_template_setting_id' => $prodTempSettings_Id,
										'template_properties' => $pr_value['properties'],
										'back_print' => '',
										'sort_order' => 1,
										'shirt_background_color' => $pr_value['back_color'],
										'set_as_default_design' => 1,
										'date_created' => date('Y-m-d H:i:s')
									
									);	 
									//save to product template 
									$prodTemplates = $this->Campaigns_model->save_product_to_templates($dataTemplateSettings); //insert scp product templates

									foreach ($pr_value['shirt_style'] as $key => $shirts) {
										
										$shirt_colors = explode(",", $shirts['color'])  ; //array shirt colors						

										$prodSettings = $this->Campaigns_model->save_product_settings($prodTemplates ,$shirt_colors, $shirts['style_id'], $shirts['data_price'] , $shirts['data_price'],$shirts['art_setting'],$shirts['canvas_setting']);
									
										if( count($prodTemplates) > 0) {
											$data['stat'] = "inserted new ";
										}	

									}

								} 


							}
							

						}


						$ctr++  ;


					}
						if( $ctr_product == $ctr ){							

							$data['stat'] = "done";		
							$this->session->unset_userdata('product_details');
						}					

				
				echo json_encode($data);
			}

		}



	public function slug(){
		$slug = $this->uri->segment(1);

		$this->load->helper('textparser');
		$data['campaign_live'] = $this->Campaigns_model->get_campaign_slug($slug);
		$camp = array_parseToString( $data['campaign_live'] );		
		$data['campaign_designs'] = $this->Campaigns_model->get_by_campaign_design( $camp['campaign_id'] );
		if ( !empty( $camp['campaign_url'] ) ) {
			if( $camp['campaign_status'] == 0){
				redirect( base_url() );
			}else{
				$this->load->view_store('campaign-live', $data);
			}
			
		}else{
			redirect( base_url() );
		}
	}

	public function preview(){
		$data['edit_campaign'] = 0;
		$this->load->helper('textparser');		
		$data['product'] = $this->session->product_id;
		$data['states'] =  $this->Account_model->get_states('236');
		$customers =  $this->Account_model->get_user_info( $this->session->id );
		$data['customer_id'] = $customers['customer_id'];
		(!empty($data['product']))? 1 : redirect( base_url('campaign/create') );
		$this->load->view_store('campaign-preview', $data);	
	}

	public function edit($campaign_id="", $customer_id=""){		
		$uri_params = $this->uri->uri_to_assoc();
		$this->load->helper('textparser');	
		$data['uri_assoc'] = $uri_params;
		$data['uri_segments'] = $this->uri->segment_array();
		$data['campaign_id'] = $data['uri_segments'][3];
		$data['customer_id'] = $this->session->customer_id;
		$data['edit_campaign'] = 1;
		$data['states'] =  $this->Account_model->get_states('236');
		$data['campaigns'] = $this->Campaigns_model->get_by_campaign_id( $data['campaign_id'] );
		array_parseToString( $data['campaigns'] );
		$data['campaign_designs'] = $this->Campaigns_model->get_by_campaign_design( $data['campaign_id'] );
		$data['payee_details'] = $this->Campaigns_model->get_payee_details_by_campaign_id( $data['campaign_id'] );	
		$this->session->unset_userdata("product_details");
		$this->load->view_store('campaign-preview', $data);		
	}

	public function get_campaign_details($campaign_id){
		$this->db->limit("1");
		$this->db->select("scp_campaigns.*, scp_customer_addresses.name, scp_customer_addresses.contact_no, scp_customer_addresses.address_1, scp_customer_addresses.address_2, scp_customer_addresses.city, scp_customer_addresses.state, , scp_customer_addresses.zip");	
		$this->db->order_by('scp_customer_addresses.customer_address_id', 'DESC');
		$this->db->JOIN("scp_customer_addresses", "scp_customer_addresses.campaign_id = scp_campaigns.campaign_id", "LEFT");
		$checkCamp = $this->db->get_where("scp_campaigns", array( 'scp_campaigns.campaign_id' => $campaign_id))->result();
		echo json_encode( $checkCamp );
	}

	public function check_url($param){
		$campaign = $this->Campaigns_model->check_by_slug( $param );
		$str['status'] = "";
		if ( !empty( $campaign['campaign_url'] ) ) {
			$str['status'] = 'exist';
		}

		echo json_encode($str);
	}

	public function check_campaign_url($param){
		$post = $this->input->post(null, true);
		$campaign = $this->Campaigns_model->get_campaign_slug( $param);
		if ( !empty( $campaign['campaign_url'] ) ) {
			if( $post['campaign_id'] == $campaign['campaign_id']){
				$str['status'] = "success";
			}else{
				$str['status'] = "exist-url";
			}
		}else{
			$str['status'] = "success";
		}
		echo json_encode($str);
	}

	public function save_campaign_details(){ /* Function for saving the campaign details*/ 		
		$post = $this->input->post(null, true);
		$campaign = $this->Campaigns_model->check_by_slug( $post['campaign_url'] );
		if ( !empty( $campaign['campaign_url'] ) ) {
			$str['status'] = "exist";
		}else{
			$campaign_id = $this->Campaigns_model->add_campaign_details($post, 0);
			$str['status'] = $campaign_id;
		}	

		echo json_encode($str);		
		$this->session->unset_userdata('product_id');	
	}

	public function save_campaign_details_extend($id){ /* Function for saving the campaign details*/ 		
		$post = $this->input->post(null, true);
		$campaign = $this->Campaigns_model->check_by_slug( $post['campaign_url'] );
		if ( !empty( $campaign['campaign_url'] ) ) {
			$this->update_extend_campaign($id, $post['campaign_url'], $post);
		}	
	}


	public function update_extend_campaign($id, $slug, $data){ 
		$newslug = $slug.'-oldcampaign';				 
		$saved = $this->Campaigns_model->update_status_extended($id, $newslug);
		$str['status'] = "";
		if($saved){
			$stat = 1;
			$campaign_id = $this->Campaigns_model->add_campaign_details( $data, $stat );
			$str['status'] = $campaign_id;
		}		
		echo json_encode($str);
	}

	public function update_campaign_details($campaign_id){ /* Function for saving the campaign details*/ 		
		$post = $this->input->post(null, true);
		$campaign = $this->Campaigns_model->get_campaign_slug( $post['campaign_url'] );
		
		if( $campaign_id == $campaign['campaign_id']){
			$this->Campaigns_model->update_campaign_details( $post, $campaign_id );
			$str['status'] = "update";
		}

		if( !empty( $campaign['campaign_url'] ) ){
			if( $campaign_id == $campaign['campaign_id']){
				$this->Campaigns_model->update_campaign_details( $post, $campaign_id );
				$str['status'] = "success";
			}else{
				$str['status'] = "exist-url";
			}
		}else{
			$this->Campaigns_model->update_campaign_details( $post, $campaign_id );
			$str['status'] = "success";
		}

		echo json_encode($str);
	}

	public function update_campaign_div($campaign_id){ /* Function for saving the campaign details*/ 		
		$post = $this->input->post(null, true);		
		$updated = $this->Campaigns_model->update_campaign_div( $post, $campaign_id );
		$str['status'] = "";
		if( $updated ){
			$str['status'] = "success";
		}	
		echo json_encode($str);
	}

	public function save_payee_details($camp_id){ /* Function for saving the payee details*/		
		$post = $this->input->post(null, true);		
		$get_campaign = $this->Campaigns_model->get_payee_details($camp_id, $post);
		if( count($get_campaign) > 0){
			$this->Campaigns_model->update_payee_details($camp_id, $post);
			$str['status'] = "success";
		}else{
			$saved = $this->Campaigns_model->save_payee_details($camp_id, $post);
			if( $saved ){
				$str['status'] = "success";
			}
		}		
		echo json_encode($str);
	}

	public function save_design_details($camp_id){ /* Function for saving the product details*/		
		$post = $this->input->post(null, true);	
		$str['status'] = '';
		$count = 0;
		foreach ($post['designs'] as $key => $prod) {
			$saved = $this->Campaigns_model->add_design_details($prod['product_id'], $camp_id);
			$count++;
		}	
		if( $count == count($post['designs']) ){
			$str['status'] = "success";
			$link = base_url('campaign/edit').'/'.$camp_id.'/'.$post['campaign_user_id'];				
			$title = $post['campaign_title'];
			$email = $post['campaign_email'];
			$dashboard = base_url("account-campaigns");
			$logo = base_url('public/'.STORE.'/images/tzilla-logo.png');
							
			$subject = 'Your Design and Campaign Draft from  TZilla.com.';
			$message = "Your design and campaign draft from <a href='".base_url()."'>TZilla.com </a>has been saved!<br/>
			<br/> Keep this email handy for when you're ready to begin your campaign. <br/>	
			If you have any questions or need a little help with getting started, our friendly Customer Support Advocates are here for you. <br/> 
			<br/> <a href=".$link."> ".$title." </a> <br/>
			<br/> Email:  Support@TZilla.com <br/> Phone:  (888) 901-1636 <br/> <br/> <br/>  The TZilla Support Team  <br/> <br/> <img src='".$logo."' style='max-width: 227px;' />";

			$dataemail = array( 'title'=> 'TZilla', 'email'=> $email, 'subject'=> $subject, 'message' => $message );
			$this->email_the_user($dataemail);
		}	
		echo json_encode($str);
	}

	public function save_design_details_extended($camp_id){ /* Function for saving the product details extended campaign*/		
		$post = $this->input->post(null, true);	
		$str['status'] = '';
		$count = 0;
		foreach ($post['designs'] as $key => $prod) {
			$saved = $this->Campaigns_model->add_design_details($prod['product_id'], $camp_id);
			$count++;
		}	
		if( $count == count($post['designs']) ){
			$str['status'] = "success";			
		}	
		echo json_encode($str);
	}

	public function publish($camp_id){ /* Function for update status to publish*/		
		$status = 1;
		$str['status'] = '';
		$campaign = $this->Campaigns_model->get_by_campaign_id($camp_id);
		foreach ($campaign as $key => $value) {}
		$user = $this->Account_model->get_customer_by_id( $value['campaign_user_id'] );
			
		$supplier_code = $this->create_supplier_on_erp( $camp_id );

		if($supplier_code){
			$saved = $this->Campaigns_model->update_publish_campaign($camp_id, $status);		
			$updated = $this->Campaigns_model->update_campaign_fields( $camp_id, $field = "supplier_code", $supplier_code );	
			if( $updated ){			
				
				$link = base_url($value['campaign_url']);				
				$title = $value['campaign_title'];
				$email = $value['campaign_email'];
				$dashboard = base_url("account-campaigns");
				$logo = base_url('public/'.STORE.'/images/tzilla-logo.png');
								
				$subject = 'Your campaign is now live. ';
				$message = "Thank you for creating a campaign on TZilla! <br/> We're pleased to inform you that your campaign is now LIVE!
				<br/> Share this link <a href=".$link.">".$title."</a> to make your fundraiser a viral success! <br/> You can view all campaigns here <a href='".$dashboard."'> Go to my campaigns </a> . 
				<br/> <br/>  If you need further assistance, contact us at <strong>support@tzilla.com</strong>, <br/>
				   or would like support with a personal touch, give us a ring at <strong>(888) 901-1636 </strong>. <br/> <br/> <br/>  The TZilla Support Team  <br/> <br/> <img src='".$logo."' style='max-width: 227px;' />  ";
				
				$data = array( 'title'=> 'TZilla', 'email'=> $email, 'subject'=> $subject, 'message' => $message );
				$this->email_the_user($data);
				$str['status'] = "success";
			}
			
			echo json_encode($str);
		}
	}

	public function create_supplier_on_erp( $id ) {
		$r = "error";

		if ( $id ){
			// $url = 'http://12.9.181.75/erpwebapi/TzillaApi/CreateSupplier'; // erp dev testing
			$url = $this->config->item('erp_api_create_supplier');
			$contenttype = "application/json"; // or application/xml
			$requestData = $this->get_campaign_creation_json( $id );

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype;charset=UTF-8") );
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestData);
			//curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			//curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			
			$result = curl_exec ($ch);

			if ( $result !== false ) {	
				$aResponse = json_decode($result,true);	
				$r = $aResponse["supplierCode"];
			}

            curl_close($ch);
		}

		return $r;
	}

	function get_campaign_creation_json( $campaign_id = 0 ) {
		if ( $campaign_id ){
			$this->db->select( "scp_customer.customer_code, scp_customer.cust_firstname, scp_customer.cust_lastname, scp_customer.cust_email, scp_campaigns.campaign_url, scp_customer.cust_address_1, scp_customer.cust_city, scp_customer.cust_state, scp_customer.cust_zip" );

			$this->db->join( "scp_customer", "scp_customer.customer_id = scp_campaigns.campaign_user_id" );
			
			$this->db->where( "scp_campaigns.campaign_id", $campaign_id );

			$details = $this->db->get( "scp_campaigns" );

			$data = array();
			$campaign = $details->row();

			if ( $details->num_rows() == 1 ) {
				$data["CustomerCode"] = $campaign->customer_code;
				$data["UserName"] = $campaign->cust_email;
				$data["Password"] = "admin"; // default password
				$data["SupplierName"] = $campaign->cust_firstname." ".$campaign->cust_lastname;
				$data["Address"] = $campaign->cust_address_1;
				$data["City"] = $campaign->cust_city;
				$data["State"] = $campaign->cust_state;
				$data["County"] = "";
				$data["PostalCode"] = $campaign->cust_zip;
				$data["Country"] = "United States of America";
				$data["AddressType"] = "";
				$data["Supplier_CID"] = "";

				return json_encode( $data );
			} else {
				return "error";
			}
		} else {
			return "error";
		}
	}

	public function lists( $cat=0, $page=0, $rows=12){ /* Function for list of all campaigns*/
		$data['filter'] = "";
		$data['search'] = "";
		$this->load->helper('textparser');
		$term = string_toDB( $this->input->post('term') );
		$data["search"] = $this->input->post('term');
		// $cat = $this->input->post("subname");
		$data['category_z'] = $this->Category_model->get_all_artwork_parent_categories( );
		$data['category'] = json_encode( $this->Category_model->get_all_artwork_categories() );
		
		//$data['get_category'] = $this->Campaigns_model->get_all_categories();
		$data['get_campaigns'] = $this->Campaigns_model->get_all_campaigns($cat, $rows, $page);	
		
		$data['get_campaigns_count'] = $this->Campaigns_model->get_all_campaigns2($cat);
		$data['cat_selected'] = $cat;

		$data['base_url']			= base_url('campaign/lists/'.$cat.'/');
		$data['total_rows']			= count($data['get_campaigns_count']);
		$data['per_page']			= $rows;
		$data['uri_segment']		= 4;
		$data['first_link']			= 'First';
		$data['first_tag_open']		= '<li>';
		$data['first_tag_close']	= '</li>';
		$data['last_link']			= 'Last';
		$data['last_tag_open']		= '<li>';
		$data['last_tag_close']		= '</li>';
		$data['num_links'] 			= 3 ;

		$data['full_tag_open']		= '<div class="pagination"><ul class="pagination">';
		$data['full_tag_close']		= '</ul></div>';
		$data['cur_tag_open']		= '<li class="active"><a href="#">';
		$data['cur_tag_close']		= '</a></li>';
		
		$data['num_tag_open']		= '<li>';
		$data['num_tag_close']		= '</li>';
		
		$data['prev_link']			= '&laquo;';
		$data['prev_tag_open']		= '<li>';
		$data['prev_tag_close']		= '</li>';

		$data['next_link']			= '&raquo;';
		$data['next_tag_open']		= '<li>';
		$data['next_tag_close']		= '</li>';
		
		$this->pagination->initialize($data);
		for ($ctr = 0; $ctr < count($data['get_campaigns']); $ctr++ )
		{
			$encodedThumbnailImage = $this->encodeArtImage($data['get_campaigns'][$ctr]->thumbnail);
			$data['get_campaigns'][$ctr]->thumbnail = $encodedThumbnailImage;
			
		}
	
		$this->load->view_store('all-campaigns', $data);
	}


	public function search($cat=0, $converstring=0, $page=0, $rows=12){	
		$term = false;
		$data['filter'] = "";
		$data['search'] = "";
		$data['msg'] = "";		
		$data['term'] = "";	
		$this->load->helper('textparser');
		$data["search"] = $this->input->post('term');
		$term_ = string_toDB( $this->input->post('term') );

		$post = $this->input->post('term');

		$data['converstring']		= $converstring;
		if($post)
		{
			$term			= json_encode($post);
			$converstring			= $this->Search_model->record_term($term);
			$data['converstring']	= $converstring;
			$term	= (object)$post;
			$data["search"] = $this->input->post('term');
			$data['term']	= string_toDB( $term->scalar );

		}
		elseif ($converstring)
		{
			$term	= $this->Search_model->get_term($converstring);
			$term	= json_decode($term);
			$data["search"] = $term;
			$data['term']	= $term;
		} 

		$data['get_campaigns'] = $this->Campaigns_model->get_search_campaigns( $data['term'], $cat, $rows, $page);	
		
		$data['get_campaigns_count'] = $this->Campaigns_model->get_search_campaigns2( $data['term'], $cat);
		$data['cat_selected'] = $cat;

		if($term){
			if(count( $data['get_campaigns'] ) == 0 ){
				$data['msg'] = "Search result is zero.";
			}
		}

		$data['base_url']			= base_url('campaign/search/'.$cat.'/'.$converstring.'/');
		$data['total_rows']			= count($data['get_campaigns_count']);
		$data['per_page']			= $rows;
		$data['uri_segment']		= 5;
		$data['first_link']			= 'First';
		$data['first_tag_open']		= '<li>';
		$data['first_tag_close']	= '</li>';
		$data['last_link']			= 'Last';
		$data['last_tag_open']		= '<li>';
		$data['last_tag_close']		= '</li>';
		$data['num_links'] 			= 3 ;

		$data['full_tag_open']		= '<div class="pagination"><ul class="pagination">';
		$data['full_tag_close']		= '</ul></div>';
		$data['cur_tag_open']		= '<li class="active"><a href="#">';
		$data['cur_tag_close']		= '</a></li>';
		
		$data['num_tag_open']		= '<li>';
		$data['num_tag_close']		= '</li>';
		
		$data['prev_link']			= '&laquo;';
		$data['prev_tag_open']		= '<li>';
		$data['prev_tag_close']		= '</li>';

		$data['next_link']			= '&raquo;';
		$data['next_tag_open']		= '<li>';
		$data['next_tag_close']		= '</li>';
		
		$this->pagination->initialize($data);	
		$this->load->view_store('all-campaigns', $data);
	}

	public function filter($filter='false', $cat=0, $page=0, $rows=12){
		
		$data['search'] = "";
		$data['msg'] = "";		
		$data['term'] = "";
		$data['category_z'] = $this->Category_model->get_all_artwork_parent_categories( );
			$data['category'] = json_encode( $this->Category_model->get_all_artwork_categories() );
		$data['filter'] = $filter;
		$data['get_campaigns'] = $this->Campaigns_model->get_filter_campaigns($filter, $cat, $rows, $page);	
		
		$data['get_campaigns_count'] = $this->Campaigns_model->get_filter_campaigns2($filter, $cat);
		$data['cat_selected'] = $cat;
		$data['base_url']			= base_url('campaign/filter/'.$filter.'/'.$cat.'/');
		$data['total_rows']			= count($data['get_campaigns_count']);
		$data['per_page']			= $rows;
		$data['uri_segment']		= 5;
		$data['first_link']			= 'First';
		$data['first_tag_open']		= '<li>';
		$data['first_tag_close']	= '</li>';
		$data['last_link']			= 'Last';
		$data['last_tag_open']		= '<li>';
		$data['last_tag_close']		= '</li>';
		$data['num_links'] 			= 3 ;

		$data['full_tag_open']		= '<div class="pagination"><ul class="pagination">';
		$data['full_tag_close']		= '</ul></div>';
		$data['cur_tag_open']		= '<li class="active"><a href="#">';
		$data['cur_tag_close']		= '</a></li>';
		
		$data['num_tag_open']		= '<li>';
		$data['num_tag_close']		= '</li>';
		
		$data['prev_link']			= '&laquo;';
		$data['prev_tag_open']		= '<li>';
		$data['prev_tag_close']		= '</li>';

		$data['next_link']			= '&raquo;';
		$data['next_tag_open']		= '<li>';
		$data['next_tag_close']		= '</li>';
		
		$this->pagination->initialize($data);
		$this->load->view_store('all-campaigns', $data);
	}


	function upload_cover_photo( $slug ) {
		$directory = 'public/'.strtolower(STORE).'/campaigns/';
		$str['status'] = "";
		$str['message'] = "";
		if ($_FILES['SelectedFile']['error'] == 0) {
			$str['status'] = "error"; 
			$str['message'] = "An error occurred when uploading.";
		}

		if (!getimagesize($_FILES['SelectedFile']['tmp_name'])) {
			$str['status'] = "error"; 
			$str['message'] = "Please ensure you are uploading an image.";
		}
		
		// Check filetype
		if ( $_FILES['SelectedFile']['type'] != 'image/png' && $_FILES['SelectedFile']['type'] != 'image/jpeg' && $_FILES['SelectedFile']['type'] != 'image/gif' && $_FILES['SelectedFile']['type'] != 'image/jpg' ) {
			$str['status'] = "error"; 
			$str['message'] = "Unsupported filetype uploaded.";
		}

		if ($_FILES['SelectedFile']['size'] > 2097152) {
			$str['status'] = "error"; 
			$str['message'] = "File uploaded exceeds maximum upload size of 2 MB.";
		}

		if ( !file_exists( $directory ) ) { 
			mkdir( $directory, 0777, true ); 
		}

		$extension = pathinfo($_FILES['SelectedFile']['name'], PATHINFO_EXTENSION);
		$_FILES['SelectedFile']['name'] = $slug.".".strtolower($extension);

		
		if (!move_uploaded_file($_FILES['SelectedFile']['tmp_name'], $directory.$_FILES['SelectedFile']['name'])) {
			$str['status'] = "error"; 
			$str['message'] = "Error uploading file - check destination is writeable.";
		}else{
			// UPDATE THE CAMPAIGN HEADER
			$data = array( 'campaign_cover_photo' => 1 );
			$this->db->where('campaign_url', $slug);
			$upd = $this->db->update('scp_campaigns', $data);

			$str['status'] = "success";
			$str['message'] = "File upload successfully.";
		}

		
		echo json_encode($str);
		// unlink($directory.$_FILES['SelectedFile']['name']); // to delete the image
	}

	function remove_cover_photo( $slug ) {
		$directory = 'public/'.strtolower(STORE).'/campaigns/';
		$str['status'] = "";
		$str['message'] = "";

		$str['status'] = "success";
		$str['message'] = "Cover photo remove successfully.";

		// UPDATE THE CAMPAIGN HEADER
		$data = array( 'campaign_cover_photo' => 0 );
		$this->db->where('campaign_url', $slug);
		$upd = $this->db->update('scp_campaigns', $data);

		unlink($directory.$slug.'.jpg'); // to delete the image
		echo json_encode($str);
	}

	private function email_the_user($data){
		# smtp settings
		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout');
		
		#send email for ipn
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = $this->smtp_host;
		$config['smtp_user'] = $this->smtp_user;
		$config['smtp_pass'] = $this->smtp_pass;
		$config['smtp_port'] = $this->smtp_port;
		$config['smtp_timeout'] = $this->smtp_timeout;
		$config['mailtype'] = 'html';
		$config['priority'] = '1';	
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['crlf'] =  "\r\n";
		$config['newline'] = "\r\n";
		
		
		$this->email->initialize($config);
		$this->email->from('noreply@tzilla.com', $data['title']);
		$this->email->to( $data['email'] );
		$this->email->subject($data['subject']);		
		$this->email->message($data['message']);
		$this->email->send();
	}

	public function chart_size(){
		$styleID = $this->input->post('styleId');
		$groupID = $this->input->post('groupStyleId');
		
		$data['category'] = $this->Inventory_model->get_shirt_category();

		if ( $styleID == 0 ) {
			// get all shirt size chart
			$data['size_details'] = $this->Inventory_model->get_size_chart_details( $styleID, $groupID );
			
		} else {
			// get size chart by shirt group id
			$data['size_details'] = $this->Inventory_model->get_size_chart_details_style( $styleID );
		}
		
		echo json_encode($data);

	}

}