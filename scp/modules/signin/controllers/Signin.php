<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends MX_Controller   {

	function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->load->library( array('form_validation', 'Aauth', 'Curl', 'session', 'email') );
		$this->load->model('Signin_model');

		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 
    }

	public function testlink( $var = 1 ){
		echo "test this";
		phpinfo();
	}
	public function websiteheaders()
	{
		$this->load->view_store( 'header-views' ); 
	}
	public function login()
	{
		$this->form_validation->set_rules('username', 'username', 'trim|valid_email');	
		$this->form_validation->set_rules('password', 'password', 'required|min_length[6]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$data['status'] = 'error';
			$data['message'] = validation_errors();			

		}else{

			$user_email = $this->input->post('username');
			$user_pass = $this->input->post('password');


			$user =  $this->Signin_model->check_user_by_email($user_email); 
			 foreach ($user as $key => $value) {
				$id = $value->id;
				$user_email = $value->email;
				$customer_id = $value->customer_id;
				$log_attempts = $value->login_attempts;
			 } 
			 
			 
			 if(count($user) > 0){

                $password = $this->aauth->hash_password($user_pass, $id);

			 	if ( $this->aauth->login_store($id, $user_pass ) ){

				 	if ( $this->Signin_model->login_check($user_email, $password) )
					{
						if( $log_attempts >= 6 ){
							$data['status'] = 'exceed';
							$data['message'] = 'Your account has been blocked! Please reset your password.';
							$updated = $this->Signin_model->update_user_fields($id, $field = "password", $val = 'blockedaccount');
							$banned = $this->Signin_model->update_user_fields($id, $field = "banned", $val = 1);
							$this->session->set_userdata('is_logged_in', FALSE);
						}else{
							$data['status'] = 'success';
							$data['message'] = 'Welcome, '.$user_email;
							$updated = $this->Signin_model->update_user_fields($id, $field = "login_attempts", $val = 0);
							$unbanned = $this->Signin_model->update_user_fields($id, $field = "banned", $val = 0);
							$this->session->set_userdata('is_logged_in', TRUE);
							$this->session->set_userdata('customer_id', $customer_id);
							$this->session->set_userdata('logged_in_email', $user_email);
							$this->session->set_userdata('customer_email', $user_email);
						}
						
					}else{
						$data['status'] = 'notmatched';
						$data['message'] = "Invalid Password. Please try again";
						$this->session->set_userdata('is_logged_in', FALSE);
					}
					
				}else{
					if( $log_attempts >= 6 ){
						$data['status'] = 'exceed';
						$data['message'] = 'Your account has been blocked! Please reset your password.';
						$updated = $this->Signin_model->update_user_fields($id, $field = "password", $val = 'blockedaccount');

					}else{
						$data['status'] = 'notmatched';
						$data['message'] = 'Invalid Password. Please try again';
						
					}
					$this->session->set_userdata('is_logged_in', FALSE);
				}

			 }else{
			 	//not registered yet
			 	$data['status'] = 'not-registered';
				$data['message'] = "Email is not yet registered.";

			 }	
		}

		echo json_encode($data);
	}

	public function is_loggedin() {

		if ($this->aauth->is_loggedin())
			echo "girdin";
	}

	public function signout()
	{
		// $this->aauth->logout();
		$this->session->unset_userdata('is_logged_in');
		$this->session->unset_userdata('customer_id');
		$this->session->unset_userdata('logged_in_email');
		$this->session->unset_userdata('customer_email');
		$this->session->unset_userdata('logged_in_first_name');
		$this->session->unset_userdata('logged_in_last_name');
		$this->session->unset_userdata('full_name');
		redirect('/');
		$this->db->empty_table('scp_search');
	}

	public function sign_up(){

		$this->form_validation->set_message('required', 'The %s field is required.');
		$this->form_validation->set_rules('username', 'username', 'trim|valid_email');	
		
		$this->form_validation->set_rules('password', 'password', 'min_length[6]|required');
		$this->form_validation->set_rules('firstname', 'firstname', 'required');
		$this->form_validation->set_rules('lastname', 'lastname', 'required');
		$this->form_validation->set_rules('confirm', 'password confirmation', 'min_length[6]|matches[password]|required');
		$this->form_validation->set_message('matches', 'The password and %s field does not match.');

		$post = $this->input->post(null, true);
        $captcha = $post['recaptcha'];
        $str['status'] = "";
        $str['message'] = "";
        if(empty($captcha)){
          $str['status'] = "error";
          $str['message'] = 'Please check the the captcha form.';
          echo json_encode($str);
          exit;
        }

        $secretKey = "6LfkVxkUAAAAALtalRHUfC8_NEcJ-UBiHNLGCCSN";
        $ip = $_SERVER['REMOTE_ADDR'];

        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        if(intval($responseKeys["success"]) !== 1) {
          $str['status'] = "error";
          $str['message'] = 'Verification Expired';
        } else {

        	$random_string = $this->generateRandomString(10);
			$str = '';
			$initial_pass =	$this->aauth->hash_password( $post["password"], 0);
			$check_email = $this->Signin_model->check_user_by_email( $post["username"] );		
			$check_user_group = $this->Signin_model->check_user_group();
			foreach ($check_user_group as $key => $group) {}		
			$group_id =  $group->id;

			if( count($check_email) > 0 ){
				$str['status'] = "exist";
          		$str['message'] = 'Email is already registered.';
			}else{
				$saved_id = $this->Signin_model->add_new_user( $post, $random_string );
				$password = $this->aauth->hash_password($post["password"], $saved_id);
				$saved	= $this->Signin_model->update_password($password, $saved_id);
				$new_customer = $this->Signin_model->add_new_cust( $post, $saved_id );
				$new_user_group = $this->Signin_model->add_user_to_group($saved_id, $group_id);
				
				$link = base_url('signin/activate/'.$random_string.'/'.$saved_id.'/'.base64_encode($post['username']));
				date_default_timezone_set('Asia/Manila');
				$message = "<html><head></head><body>";
				$message .= 'Welcome to TZilla!<br/>';
				$message .= 'Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.<br/><br/>';
				$message .= '<br/>Your USERNAME/EMAIL: '.$post["username"];
				$message .= '<br/>Your password is <b>'.$post["password"].'</b>';
				$message .= '<br/><br/>Please keep this email for future reference.';
				$message .= '<br/><br/>';
				$message .= 'Please click this link to activate your account:<br/>';
				$message .= '<a href="'.$link.'">'.$link.'</a><br/>';
				$message .= '<br/>Date Created: '.date('F d, Y H:i A');
				$message .= "</body></html>";
				
				$data = array( 'title'=> 'TZilla', 'email'=> $post['username'], 'subject'=>'New User', 'message' => $message );

				$this->email_the_user($data);
						
				$str['status'] = "success";
          		$str['message'] = 'Your account has been made, please verify it by clicking the activation link that has been send to your email.';
				
			} 
          
        }	

		echo json_encode($str);
	}

	public function sign_up_from_tgen(){

		$this->form_validation->set_message('required', 'The %s field is required.');
		$this->form_validation->set_rules('username', 'username', 'trim|valid_email');			
		$this->form_validation->set_rules('password', 'password', 'min_length[6]|required');
		$this->form_validation->set_rules('firstname', 'firstname', 'required');
		$this->form_validation->set_rules('lastname', 'lastname', 'required');
		$this->form_validation->set_rules('confirm', 'password confirmation', 'min_length[6]|matches[password]|required');
		$this->form_validation->set_message('matches', 'The password and %s field does not match.');

		$post = $this->input->post(null, true);
        $captcha = $post['recaptcha'];
        $str['status'] = "";
        $str['message'] = "";
        if(empty($captcha)){
          $str['status'] = "error";
          $str['message'] = 'Please check the the captcha form.';
          echo json_encode($str);
          exit;
        }

        $secretKey = "6LfkVxkUAAAAALtalRHUfC8_NEcJ-UBiHNLGCCSN";
        $ip = $_SERVER['REMOTE_ADDR'];

        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        if(intval($responseKeys["success"]) !== 1) {
          $str['status'] = "error";
          $str['message'] = 'Verification Expired';
        } else {

        	$random_string = $this->generateRandomString(10);
			$str = '';
			$initial_pass =	$this->aauth->hash_password( $post["password"], 0);
			$check_email = $this->Signin_model->check_user_by_email( $post["username"] );		
			$check_user_group = $this->Signin_model->check_user_group();
			foreach ($check_user_group as $key => $group) {}		
			$group_id =  $group->id;

			if( count($check_email) > 0 ){
				$str['status'] = "exist";
          		$str['message'] = 'Email is already registered.';
			}else{
				$saved_id = $this->Signin_model->add_new_user( $post, $random_string );
				$password = $this->aauth->hash_password($post["password"], $saved_id);
				$saved	= $this->Signin_model->update_password($password, $saved_id);
				$new_customer = $this->Signin_model->add_new_cust( $post, $saved_id );
				$new_user_group = $this->Signin_model->add_user_to_group($saved_id, $group_id);
				
				$link = base_url('signin/activate/'.$random_string.'/'.$saved_id.'/'.base64_encode($post['username']));
				date_default_timezone_set('Asia/Manila');
				$full_name = $post["firstname"].' '.$post["lastname"];
				$message = "<html><head></head><body>";
				$message .= 'Welcome to TZilla!<br/>';
				$message .= 'Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.<br/><br/>';
				$message .= '<br/>Your USERNAME/EMAIL: '.$post["username"];
				$message .= '<br/>Your password is <b>'.$post["password"].'</b>';
				$message .= '<br/><br/>Please keep this email for future reference.';
				$message .= '<br/><br/>';
				$message .= 'Please click this link to activate your account:<br/>';
				$message .= '<a href="'.$link.'">'.$link.'</a><br/>';
				$message .= '<br/>Date Created: '.date('F d, Y H:i A');
				$message .= "</body></html>";
				
				$data = array( 'title'=> 'TZilla', 'email'=> $post['username'], 'subject'=>'New User', 'message' => $message );

				$this->email_the_user($data);
						
				$str['status'] = "success";
          		$str['message'] = 'Your account has been made, please verify it by clicking the activation link that has been send to your email.';

          		$this->session->set_userdata('is_logged_in', TRUE);
          		$this->session->set_userdata('id', $saved_id );
          		$this->session->set_userdata('customer_id', $new_customer );
				$this->session->set_userdata('logged_in_email', $post["username"] );
				$this->session->set_userdata('customer_email', $post["username"]);
				$this->session->set_userdata('logged_in_first_name', $post["firstname"]);
				$this->session->set_userdata('logged_in_last_name', $post["lastname"]);
				$this->session->set_userdata('full_name', $full_name);				
			} 
          
        }		
		echo json_encode($str);
	}

	public function activate($random, $user_id, $email){ //scpv1-1		
		$data['email'] = $email;
		$activate_user = $this->Signin_model->check_email_to_activate($random, $user_id, base64_decode($data['email']));
		$signin = $this->Signin_model->check_user_by_email(base64_decode($data['email']));

		foreach ($signin as $key => $value) {
			$id = $value->id;
			$user_email = $value->email;
			$customer_id = $value->customer_id;
			$name = $value->name;
		} 

		if(!empty($activate_user['id']) ){
			$update = $this->Signin_model->activate_user( $user_id );

			if($update){
				$logged_info = array(
					'id' => $id,
					'name' => $name,
					'email' => $user_email,
					'loggedin' => TRUE
				);

				$message = 'Welcome to <b>TZilla<b> <br/>';
				$message .= "You have successfully activated your account.<br/><br/>";
				$message .= "Thank you.";
				
				$data = array( 'title'=> 'TZilla', 'email'=> $user_email, 'subject'=>"Activation", 'message' => $message );
				$this->email_the_user($data);

				$this->session->set_userdata($logged_info);	
				$this->session->set_userdata('is_logged_in', TRUE);
				$this->session->set_userdata('customer_id', $customer_id);
				$this->session->set_userdata('logged_in_email', $user_email);
				$this->session->set_userdata('customer_email', $user_email);
			    
				$data['activated_user_id'] = $user_id;
				$this->load->view_store('header', $data);
				$this->load->view_store('blocks/banner.php');
				$this->load->view_store('blocks/custom.php');
				$this->load->view_store('blocks/featured.php');
				$this->load->view_store('blocks/latest.php');
				$this->load->view_store('footer');
			}			
		}else{
			redirect( base_url() );
		}
	}

	public function sent_email_forgot(){ //scpv1-1		
		$post = $this->input->post(null, true);
		$user = $this->Signin_model->get_user_by_email($post['email']);
		$str['status'] = '';
		$str['message'] = '';


		if( !empty($user['id']) ){
			$visit_store = base_url();
			$random = $this->generateRandomString(10);			
			$res = $this->Signin_model->reset_password_verification($random, $user['id'], $post['email']);
			$link = base_url('signin/viewresetpassword/'.$random.'/'.$user['id'].'/'.base64_encode($post['email']));

			$message = '<b>'.ucfirst(STORE).' <br/>';
			$message .= "Reset your password. <br/>";
			$message .= "</b>Follow this link to reset your customer account password at  <b><a href='".$visit_store."' > TZilla.</a> </b> If you didn't request a new password, you can safely delete this email.<br/> <br/> <br/> <br/> <br/><b><a href='".$link."'>Reset your password </a> </b> or <b> <a href='".$visit_store."'> Visit our store </a> <br/>";
			
			$data = array( 'title'=> 'TZilla'.' Reset Password', 'email'=> $post['email'], 'subject'=>"Here's how to reset your password.", 'message' => $message );
			$this->email_the_user($data);

			$str['status'] = 'success';
			$str['message'] = 'Email sent!';

		}else{
			$str['status'] = 'no-data';
			$str['message'] = "Email is not yet registered.";
		}

		echo json_encode($str);
	}


	public function viewresetpassword($random, $id, $email){ //scpv1-1
		$data['resetpassword_userID'] = 1;
		$data['resetuserID'] = $id;
		$get_data = $this->Signin_model->get_user_by_verification($random, $id, base64_decode($email));  
		if( count($get_data) > 0 ){			
			$this->load->view_store('header', $data);
			$this->load->view_store('blocks/banner.php');
			$this->load->view_store('blocks/custom.php');
			$this->load->view_store('blocks/featured.php');
			$this->load->view_store('blocks/latest.php');
			$this->load->view_store('footer');
		}else{
			redirect( base_url() );
		}
	}

	public function resetpassword(){ //scpv1-1
		$post = $this->input->post(null, true);
		$password = $this->aauth->hash_password($post['password'], $post['id'] );
		$check_old_pass	= $this->Signin_model->check_old_password($post['id'], $password );
		$getuser	= $this->Signin_model->get_user_by_id( $post['id'] );

		$str['status'] = '';
		$str['message'] = '';
		if(empty($check_old_pass['id'])){			
		$saved	= $this->Signin_model->forgot_password_reset($post['id'], $password );		
			if( $saved ){
				$subject = "Your  password was changed.";
				$message = 'Hi '.$getuser['firstname'].'! <br/><br/>';
				$message .= "This is just a notification that the password for your  account was recently changed. <br/> <br/> If you did not make this change, please reply to this email and a member of our friendly Customer Support Team will assist you. Thank you! <br/> <br/>";
				
				$data = array( 'title'=> 'TZilla Password Change', 'email'=> $getuser['email'], 'subject'=> $subject, 'message' => $message );
				$this->email_the_user($data);
				$str['status'] = 'success';
				$str['message'] = 'Your password was successfully changed!';
				$unbanned = $this->Signin_model->update_user_fields($post['id'], $field = "banned", $val = 0);
			}
		}else{
			$str['status'] = 'error';
			$str['message'] = "Can't proceed, old password used!";
		}
		
		echo json_encode($str);
	}

	public function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, strlen($characters) - 1)];
	    }
	    return $randomString;
	}

	public function subscribe(){
		$post = $this->input->post(null, true);
		$check_reg	=  $this->Signin_model->check_cust_by_email( $post['email'] );
		if (!filter_var( $post['email'] , FILTER_VALIDATE_EMAIL)) {
			$str['status'] = 'error';
			$str['message'] = 'Email format is invalid';
			echo json_encode($str);
			exit;
		} 
		$str['status'] = '';
		$str['message'] = '';
		if( count($check_reg) > 0 ){
			foreach ($check_reg as $key => $cust) {}
				$firstname = $cust->cust_firstname;
				$customer_id = $cust->customer_id;
				$subscribe = $cust->cust_subscribe;
				if( $subscribe == 0 ){					
					$check_exist	=  $this->Signin_model->get_subscription_by_email( $post['email'] );
					if( count($check_exist) > 0){
						$update_data	=  $this->Signin_model->update_cust_subscribe( $post['email'], $customer_id );
					}else{
						$update_data	=  $this->Signin_model->add_new_email_subscription( $post['email'] );
					}
					if( $update_data ){
						$message = "<html><head></head><body>";
						$message .= 'Hi '.$firstname.'! <br/><br/>';
						$message .= 'Thanks so much for signing up for the TZilla newsletter!';
						$message .= "</body></html>";
						
						$data = array( 'title'=> 'TZilla Newsletter', 'email'=> $post['email'], 'subject'=>'Thanks for Subscribing to TZilla', 'message' => $message );
						// $this->email_the_user($data);

						$str['status'] = 'success';
						$str['message'] = 'Email successfully subscribed.';
					}

				}else{
					$str['status'] = 'exist';
					$str['message'] = 'Email is already subscribed.';
				}
				

		}else{
			$check_exist	=  $this->Signin_model->get_subscription_by_email( $post['email'] );
			if( count($check_exist) > 0){
				$str['status'] = 'exist';
				$str['message'] = 'Email is already subscribed.';
			}else{
				$new_subscription	=  $this->Signin_model->add_new_email_subscription( $post['email'] );
				if( $new_subscription ){
						$message = "<html><head></head><body>";
						$message .= 'Hi! <br/><br/>';
						$message .= 'Thanks so much for signing up for the TZilla newsletter!';
						$message .= "</body></html>";
						
						$data = array( 'title'=> 'TZilla Newsletter', 'email'=> $post['email'], 'subject'=>'Thanks for Subscribing to TZilla ', 'message' => $message );
						// $this->email_the_user($data);

						$str['status'] = 'success';
						$str['message'] = 'Email successfully subscribed.';
				}
			}
			
		}

		echo json_encode($str);
	}

	private function email_the_user($data){
		
		$s = $this->smtp_setup();		
		
		$this->email->initialize($s);
		$this->email->from('noreply@tzilla.com', $data['title']);
		$this->email->to( $data['email'] );
		$this->email->subject($data['subject']);		
		$this->email->message($data['message']);
		$this->email->send();
	}

	public function contact_us(){
		
		$datanow = date('Y-m-d H:i:s');
		$name = $this->input->post('contact_name');
		$email = $this->input->post('contact_email');
		$phone = $this->input->post('contact_phone');
		$schoolname = $this->input->post('contact_school');
		$subject = $this->input->post('contact_subject');
		$message_to = $this->input->post('contact_message');
		
		$captcha = $this->input->post('recaptcha');
		
        $str['status'] = "";
        $str['message'] = "";
        if(empty($captcha)){
          $str['status'] = "error";
          $str['message'] = 'Please check the the captcha form.';
          echo json_encode($str);
          exit;
        }

        $secretKey = "6LfkVxkUAAAAALtalRHUfC8_NEcJ-UBiHNLGCCSN";
        $ip = $_SERVER['REMOTE_ADDR'];

        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secretKey."&response=".$captcha."&remoteip=".$ip);
        $responseKeys = json_decode($response,true);
        if(intval($responseKeys["success"]) !== 1) {
          $str['status'] = "error";
          $str['message'] = 'Verification Expired';
        }else{


			 $save_data = array('store_id' => STOREID, 'email_from' =>$email, 'phone_no' =>$phone, 'customer_name'=>$name, 'message_body'=>$message_to, 'subject' =>$subject, 'school_name'=>$schoolname, 'date_entry' => $datanow );
			 $re = $this->Signin_model->insert_contact($save_data);

			$message = "<html><head></head><body>";
			$message .= 'Hi '.$name.'!<br/>';
			$message .= 'Thank you for reaching out to us ...... Thank you for choosing '.STORE.'<br/><br/>';
			
			$message .= '<br/> ';
			$message .= "</body></html>";
			
			$data = array( 
				'title'=> STORE,
				 'email'=> $email, 
				 'subject'=>$subject, 
				 'message' => $message
			 );

			$data2 = array( 
				'title'=> STORE, 
				'email'=> $email, 
				'subject'=>$subject, 
				'message' => $message_to
			);

			 if(count($re) > 0 ){

			 	$result = $this->reply_email($data);
			 	 $str['status'] = $result ;

				if(  $str['status']  == "success" ){
					$this->support_email($data2);
				}
			 }

		}
		echo json_encode($str);

	}

	
	 private function reply_email($data){ //reply email to the sender
		
		$s = $this->smtp_setup();		
		
		$this->email->initialize($s);
		$this->email->from('noreply@tzilla.com', $data['title']);
		$this->email->to( $data['email'] );
		$this->email->subject($data['subject']);		
		$this->email->message($data['message']);
		$r = $this->email->send();
		if($r){
			$r = 'success';
			$message = 'Email sent.';
		}else{
			$r = 'failed';
			$message = json_encode($this->email->print_debugger());
		}
		return $r;
	}

	private function support_email($data){ //forward message to the support team
	
		$s = $this->smtp_setup();

		$this->email->initialize($s);
		$this->email->from($data['email'], $data['title']);
		$this->email->to( 'support@tzilla.com' );
		$this->email->bcc( 'cherry.valdoz@tzilla.com', 'cheezel.tin@tzilla.com' );
		$this->email->subject($data['subject']);		
		$this->email->message($data['message']);
		$this->email->send();
	}

	public function set_customer_code() {
		$customers = $this->db->get_where("scp_customer", array( 'store_id' => STOREID))->result();

		foreach ($customers as $key => $value) {
			if( empty($value->customer_code) ){
				$customer_code = $this->Signin_model->create_customer_on_erp( $value->cust_firstname."".$value->cust_lastname, $value->cust_email );
				$this->db->where( 'customer_id', $value->customer_id );

				$this->db->set( 'customer_code', $customer_code);

				$this->db->update('scp_customer');

				echo "<pre>";
				$customer_record = $this->db->get("scp_customer")->result();
				echo "</pre>";
			}
			
		}
	}

    /**
     * Signin::CreateCustomer()
     * Use to create customer in TZilla ERP
     * Author: Macaroni '85 Pascual
     * Date Created: 04/21/2017
     * Changelogs:
     * 
     * @param string $CustomerName
     * @param string $Address
     * @param string $City
     * @param string $State
     * @param string $County
     * @param string $PostalCode
     * @param string $Country
     * @param string $Telephone
     * @param string $Email
     * @param string $Password
     * @return bool response
     */
    public function CreateCustomer($CustomerName="",$Address="",$City="",$State="",$County="",$PostalCode="",$Country="",$Telephone="",$Email="",$Password=""){
        //fill all array
        //http://tzilla.loc:81/signin/createcustomer/Waynes%20World%202/9500%20Gilman%20Drive/La%20Jolla/CA/CA/92093/United%20States%20of%20America/09179611964/jovani.ogaya%40gmail.com/pass
        $ctr=0;
        $error="";
        $url="http://12.9.181.75/erpwebapi/TzillaApi/CreateCustomer";
        $aPassJSON=array("CustomerName"=>urldecode($CustomerName),
                        "Address"=>urldecode($Address),
                        "City"=>urldecode($City),
                        "State"=>urldecode($State),
                        "County"=>urldecode($County),
                        "PostalCode"=>urldecode($PostalCode),
                        "Country"=>urldecode($Country),
                        "Telephone"=>urldecode($Telephone),
                        "email"=>urldecode($Email),
                        "password"=>urldecode($Password)
                        );      
        $jsonData=json_encode($aPassJSON);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  text/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); 
        $content = curl_exec($ch);
        curl_close($ch);        
        $aResponse=json_decode($content,true);
            echo $aResponse["customerCode"];
            return true;
    }
    public function CreateSupplier($CustomerCode="",$UserName="",$Password="",$SupplierName="",$Address="",$City="",$State="",$County="",$PostalCode="",$Country="",$AddressType="",$Supplier_CID=""){
        //fill all array
        //http://tzilla.loc:81/signin/createsupplier/CUST-000009/production%40tzilla.com/admin/Athena%20Alexander/NY/NY/NY/NY/34242/United%20States%20of%20America/Permanent/FEDEX
        $ctr=0;
        $error="";
        $url="http://12.9.181.75/erpwebapi/TzillaApi/CreateSupplier";
        $aPassJSON=array("CustomerCode"=>urldecode($CustomerCode),
                        "UserName"=>urldecode($UserName),
                        "Password"=>urldecode($Password),
                        "SupplierName"=>urldecode($SupplierName),
                        "Address"=>urldecode($Address),
                        "City"=>urldecode($City),
                        "State"=>urldecode($State),
                        "County"=>urldecode($County),
                        "PostalCode"=>urldecode($PostalCode),
                        "Country"=>urldecode($Country),
                        "AddressType"=>urldecode($AddressType),
                        "Supplier_CID"=>urldecode($Supplier_CID)
                        );      
        $jsonData=json_encode($aPassJSON);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  text/json"));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); 
        $content = curl_exec($ch);
        curl_close($ch);        
        $aResponse=json_decode($content,true);
        echo $aResponse["supplierCode"];
        return true;
    }

    public function smtp_setup()
    {
    	# smtp settings
		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout');
		
		#send email for ipn
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = $this->smtp_host;
		$config['smtp_user'] = $this->smtp_user;
		$config['smtp_pass'] = $this->smtp_pass;
		$config['smtp_port'] = $this->smtp_port;
		$config['smtp_timeout'] = $this->smtp_timeout;
		$config['mailtype'] = 'html';
		$config['priority'] = '1';	
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['crlf'] =  "\r\n";
		$config['newline'] = "\r\n";

		return $config;
    }

}