<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fb_login extends MX_Controller {

	function __construct()
    {
        parent::__construct();
        //$this->load->database();
        $this->load->library( array('form_validation', 'Aauth', 'Curl', 'session') );
		$this->load->model( array('Signin_model', 'Fb_user_model') );

		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 
    }

	public function index()
	{ 
		
		if(STORE == 'SCP_ADMIN') redirect('/admin');
	}

	public function setlogin()
	{		
		##see if the request is via ajax
		if($this->input->is_ajax_request())
		{
			$email = $this->input->post('email', TRUE);
			$first_name = $this->input->post('first_name', TRUE);
			$last_name = $this->input->post('last_name', TRUE);
			$middle_name = $this->input->post('middle_name', TRUE);
			$full_name = $this->input->post('name', TRUE);
			$gender = $this->input->post('gender', TRUE);
			$str['stat'] = 'ongoing';
			if($email !== NULL)
			{
				$data = array(				        
				        'email' => $email,
				        'name' => $first_name.' '.$last_name,
				        'firstname' => $first_name,
				        'middlename' => $middle_name,
				        'lastname' => $last_name,
				        'gender' => $gender,
				        'loggedin_via' => 'facebook',
				        'status' => 1,
				        'last_login' => date('Y-m-d H:i:s'),
				        'last_login_attempt' => date('Y-m-d H:i:s'),
				        'ip_address' => $this->input->ip_address()
					);

				$user = $this->Fb_user_model->check_user_by_email($email);				
				if( count($user) > 0)
				{	
					foreach ($user as $key => $value) {}				
					#update	
					$user_id = $value->user_id;
					$customer_id = $value->customer_id;
					$update_user = $this->Fb_user_model->update_fb_user($email, $user_id, $data);
					if( $update_user ){
						$update_cust = $this->Fb_user_model->update_fb_cust($email, $user_id, $data);
						$this->session->set_userdata('customer_id', $customer_id);
						$str['stat'] = 'success';
					}					

				}else{
					#insert
					$this->load->module("Signin");
					
					$user_id = $this->Fb_user_model->save_fb_user($data);
					if( $user_id ){
						$customer_code = $this->Signin_model->create_customer_on_erp( $first_name." ".$last_name, $email );
						if( $customer_code ){
							$data2 = array(
					        'cust_email' => $email,
					        'cust_firstname' => $first_name,
					        'cust_lastname' => $last_name,
                        	'customer_code' => $customer_code
					        );
						$new_customer = $this->Fb_user_model->save_fb_cust( $data2, $user_id );
						$this->session->set_userdata('customer_id', $new_customer );
						$str['stat'] = 'success';
						}							
						
					}					

				}
				if( $str['stat'] == 'success' ){
					$str['message'] = 'Welcome, '.$email.' Facebook user logged in successfully.';
					$logged_info = array(
						'id' => $user_id,
						'name' => $full_name,
						'email' => $email,
						'loggedin' => TRUE
					);
					$this->session->set_userdata($logged_info);				
					##
					# Save to session
					##
					$this->session->set_userdata('is_logged_in', TRUE);
					$this->session->set_userdata('logged_in_email', $email);
					$this->session->set_userdata('customer_email', $email);
					$this->session->set_userdata('logged_in_first_name', $first_name);
					$this->session->set_userdata('logged_in_last_name', $last_name);
					$this->session->set_userdata('full_name', $full_name);
				}
				// $str['stat'] = 'success';
				
			}else{
				$str['stat'] = 'failed';
				$str['message'] = 'Invalid email.';				
			}
			echo json_encode( $str );
		}
	}

	public function logout_user(){
		$this->session->unset_userdata('is_logged_in');
		$this->session->unset_userdata('customer_id');
		$this->session->unset_userdata('logged_in_email');
		$this->session->unset_userdata('customer_email');
		$this->session->unset_userdata('logged_in_first_name');
		$this->session->unset_userdata('logged_in_last_name');
		$this->session->unset_userdata('full_name');
		redirect('/');
		$this->db->empty_table('scp_search');
	}


}