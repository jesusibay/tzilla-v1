<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_account extends MX_Controller {

	function __construct() {
		
		parent::__construct();
									
		$this->load->model(	array('Account_model', 'campaign/Campaigns_model', 'signin/Signin_model', 'order/Order_model') );
		$this->load->helper('url');
		$this->load->helper('security');
        $this->load->library( array('form_validation', 'Aauth', 'Curl', 'session', 'pagination') );

        $this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 	
		date_default_timezone_set('America/Los_Angeles');	
	}

	public function dashboard() {
		$data['getdata'] = $this->Account_model->get_details( $this->session->id );
		$data['active'] = $this->Campaigns_model->get_user_active_campaigns( $this->session->id );
		$data['ended_soon'] = $this->Campaigns_model->get_user_campaigns_ended_soon( $this->session->id );
		$data['link'] = 'dashboard';
		$this->load->view_store('account-dashboard', $data);
		
	}

	public function resend_verification() {
		$data['getdata'] = $this->Account_model->get_details( $this->session->id );
		foreach ($data['getdata'] as $key => $user) { }
			$name = $user->name;
			$random = $user->verification_code;
			$user_id = $user->id;
			$email = $user->email;

			$link = base_url('signin/activate/'.$random.'/'.$user_id.'/'.base64_encode($email));
			// date_default_timezone_set('Asia/Manila');
			$subject = 'Email verification on '.STORE;
			$message = "<html><head></head><body>";
			$message .= 'Hi '.$name.',<br/><br/>';
			$message .= 'Please click the url below to verify your email:<br/><br/>';
			$message .= $link.'<br/><br/>';
			$message .= 'OR copy this url and paste in your browser address bar: '.$link;
			$message .= '<br/><br/>';
			$message .= 'Thank you.';
			$message .= "</body></html>";
			
			$data = array( 'title'=> STORE, 'email'=> $email, 'subject'=> $subject, 'message' => $message );

			$this->email_the_user($data);
			echo "success";
	}

	public function profile() {
		$data['link'] = 'profile';
		$data['getdata'] = $this->Account_model->get_details( $this->session->id );
		$data['states'] =  $this->Account_model->get_states('236');
		$this->load->view_store('account-profile-settings', $data);
		
	}

	function search_school($term){	// search
		$data['school'] = array();

		if ( strlen($term) > 0 ) {
			$data['school']	= $this->Account_model->get_school_name($term); //campaign title
			echo json_encode($data);
		}
	}

	public function update_profile(){
		$post = $this->input->post(null, true);
		$update = $this->Account_model->update_profile( $post, $this->session->id );
		if($update){
			echo "success";
		}
	}

	public function change_password(){
		
		$newpass = 	$this->input->post('newpass');	
		$confirmpass = $this->input->post('conpass');

		$currentp = $this->aauth->hash_password($this->input->post('oldpass'), $this->session->id);
		$newp =	$this->aauth->hash_password($this->input->post('newpass'), $this->session->id);
		

		$getuser = $this->Account_model->get_details( $this->session->id );  
				
		foreach ($getuser as $key => $user) {} 

		if( $currentp != $user->password ){
				$data['status'] = "incorrect";  
				$data['message']= "Current Password is incorrect.";
		}else if( $currentp == $newp ){
		 	$data['status']  = "same"; 
		 	$data["message"]= "password is the same with the current password.";

		 }else if( $newpass != $confirmpass  ) {
			$data['status']  = "notsame";
			$data['message'] = "new password is not the same with the confirm password.";
		}else if( strlen($newpass) <= 5 ){
			$data['status'] = "error";  
			$data['message']= "Password must be minimum of 6 characters.";
		}else {
			$password = $this->aauth->hash_password($newpass, $this->session->id);
			$saved	= $this->Account_model->change_pass($password, $this->session->id);
		 	
			if($saved)
			{
				$this->session->set_flashdata('msg', '<strong>Success!</strong> Your password was successfully changed!');	
				$data['status'] =  "success";
				$data['message'] = "Your password was successfully changed!";
			}			
		}

		echo json_encode($data);
	}

	public function get_zip() {
		$post = $this->input->post(null, true);
		$result = $this->Account_model->get_zip( $post['zip_code'], $post['state'] );
		if( count($result) > 0 ){
			echo "success";
		}else{
			echo "no-data";
		}
		
	}	

	public function campaigns( $sort_by='campaign_id', $sort_order='DESC', $page=0, $rows=10 ) {
		$data['link'] = 'campaigns';
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['campaigns'] = $this->Campaigns_model->get_campaigns( $this->session->id, $sort_by, $sort_order, $rows, $page );	
		$count = $this->Campaigns_model->get_campaigns_count( $this->session->id )->num_rows();	

		$pagedata['base_url']			= base_url('account/campaigns/'.$sort_by.'/'.$sort_order.'/');
		$pagedata['total_rows']			= $count;
		$pagedata['per_page']			= $rows;
		$pagedata['uri_segment']		= 5;
		$pagedata['first_link']			= 'First';
		$pagedata['first_tag_open']		= '<li>';
		$pagedata['first_tag_close']	= '</li>';
		$pagedata['last_link']			= 'Last';
		$pagedata['last_tag_open']		= '<li>';
		$pagedata['last_tag_close']		= '</li>';
		$pagedata['num_links'] 			= 3 ;

		$pagedata['full_tag_open']		= '<div class="pagination"><ul class="pagination">';
		$pagedata['full_tag_close']		= '</ul></div>';
		$pagedata['cur_tag_open']		= '<li class="active"><a href="#">';
		$pagedata['cur_tag_close']		= '</a></li>';
		
		$pagedata['num_tag_open']		= '<li>';
		$pagedata['num_tag_close']		= '</li>';
		
		$pagedata['prev_link']			= '&laquo;';
		$pagedata['prev_tag_open']		= '<li>';
		$pagedata['prev_tag_close']		= '</li>';

		$pagedata['next_link']			= '&raquo;';
		$pagedata['next_tag_open']		= '<li>';
		$pagedata['next_tag_close']		= '</li>';
		
		$this->pagination->initialize($pagedata);
		$this->load->view_store('account-my-campaigns', $data);		
	}

	function force_end_campaign($campaign_id){
		$ended = $this->Campaigns_model->force_end_campaign( $campaign_id );
		$campaign_details = $this->Campaigns_model->get_campaign_id( $campaign_id );
		$user_details = $this->Account_model->get_details( $this->session->id );
		if( $ended ){
			foreach ($campaign_details as $key => $campaign) { }
			foreach ($user_details as $key => $user) { }
				$name = $user->name;
				$random = $user->verification_code;
				$user_id = $user->id;
				$email = $user->email;

				$subject = 'Campaign Notification on '.STORE;
				$message = "<html><head></head><body>";
				$message .= 'You just ended your Campaign <b>'.$campaign->campaign_title.'</b>';
				$message .= '<br/><br/>';
				$message .= 'Thank you.';
				$message .= "</body></html>";
				
				$data = array( 'title'=> STORE.' Campaign', 'email'=> $email, 'subject'=> $subject, 'message' => $message );

				$this->email_the_user($data);
				echo "success";
		}
	}

	function auto_update_campaign_status(){ 
		$getstatus = $this->Campaigns_model->get_status_campaign();
		$now = date('Y-m-d H:i:s'); 
		foreach ($getstatus  as $key =>$value) {
			 $customer = $this->Account_model->get_customer_by_id( $getstatus[$key]->campaign_user_id );
			 $campaign_id = $getstatus[$key]->campaign_id;
			 $status = 3;
			 $end = $getstatus[$key]->end_date;
			 $title = $getstatus[$key]->campaign_title;
			 $slug = $getstatus[$key]->campaign_url;
			 $link = base_url($slug);
			 $logo = base_url('public/'.STORE.'/images/tzilla-logo.png');
			 
			 $id = $campaign_id;
			 if($end <= $now){
			 	$updated = $this->Campaigns_model->update_status($id, $status, $slug);
			 	if($updated){
					$subject = 'Campaign Notification on '.STORE;
					$message = "<html><head></head><body>";
					$message .= "Your Campaign <b><a href='".$link."'>".$title."</a></b> just ended.";
					$message .= '<br/><br/>';
					$message .= 'Thank you.';
					$message .= "<br/><img src='".$logo."' style='max-width: 227px;' />";
					$message .= "</body></html>";
					
					$data = array( 'title'=> STORE.' Campaign', 'email'=> $customer['email'], 'subject'=> $subject, 'message' => $message );
					$this->email_the_user($data);					
			 	}

			}			
		}
		echo count($getstatus);
	}
	
	public function orders( $sort_by='order_id', $sort_order='DESC', $page=0, $rows=10 ) {
		$data['link'] = 'orders';
		$data['sort_by'] = $sort_by;
		$data['sort_order'] = $sort_order;
		$data['orders'] = $this->Order_model->get_orders( $this->session->customer_id, $sort_by, $sort_order, $rows, $page );	
		$count = $this->Order_model->get_orders_count( $this->session->customer_id, $sort_by, $sort_order )->num_rows();	

		$pagedata['base_url']			= base_url('account/orders/'.$sort_by.'/'.$sort_order.'/');
		$pagedata['total_rows']			= $count;
		$pagedata['per_page']			= $rows;
		$pagedata['uri_segment']		= 5;
		$pagedata['first_link']			= 'First';
		$pagedata['first_tag_open']		= '<li>';
		$pagedata['first_tag_close']	= '</li>';
		$pagedata['last_link']			= 'Last';
		$pagedata['last_tag_open']		= '<li>';
		$pagedata['last_tag_close']		= '</li>';
		$pagedata['num_links'] 			= 3 ;

		$pagedata['full_tag_open']		= '<div class="pagination"><ul class="pagination">';
		$pagedata['full_tag_close']		= '</ul></div>';
		$pagedata['cur_tag_open']		= '<li class="active"><a href="#">';
		$pagedata['cur_tag_close']		= '</a></li>';
		
		$pagedata['num_tag_open']		= '<li>';
		$pagedata['num_tag_close']		= '</li>';
		
		$pagedata['prev_link']			= '&laquo;';
		$pagedata['prev_tag_open']		= '<li>';
		$pagedata['prev_tag_close']		= '</li>';

		$pagedata['next_link']			= '&raquo;';
		$pagedata['next_tag_open']		= '<li>';
		$pagedata['next_tag_close']		= '</li>';
		
		$this->pagination->initialize($pagedata);

		$this->load->view_store('account-orders', $data);		
	}

	public function order($id) {
		$data['link'] = 'orders';
		$data['get_order_details'] = $this->Order_model->get_order_details( $id, $this->session->customer_id );	
		print_r( $data['get_order_details'] );	
		$this->load->view_store('account-sales-order-details', $data);
		
	}

	public function payout() {
		$data['link'] = 'payout';
		$data['accounts'] = $this->Account_model->get_account_per_ended_campaign( $this->session->id );	

		$this->load->view_store('account-payout-settings', $data);
		
	}

	public function payout_update() {
		$post = $this->input->post(null, true);
		$user_id = $this->session->id;
		$campaigns = $this->Account_model->get_campaigns_ended( $user_id );				
		$cutoff = $this->Account_model->get_payout_cutoff();
		$str = "";				
		foreach ($cutoff as $key => $date) {}
			$date = $date->cutoff_date;
			foreach ($campaigns as $key => $camp) {
				if ( strtotime( $camp->end_date) < time() || $camp->campaign_status == 3 ) {
				$camp_id = $camp->campaign_id;
				$get_payout_settings = $this->Account_model->get_payout_settings( $user_id, $camp_id );
				if( count($get_payout_settings) == 0){
					$insert_payout_settings = $this->Account_model->insert_payout_settings( $user_id, $camp_id, $post['amount'], $date );
					if($insert_payout_settings){
						$str = 'success';
					}
				}else{
					$update_payout_settings = $this->Account_model->update_payout_settings( $user_id, $camp_id, $post['amount'] );
					if($update_payout_settings){
						$str = 'success';
					}
				}

			}
		}

		echo $str;
		
	}

	public function get_campaigns_ended_user_account(){
		$post = $this->input->post(null, true);
		$result = $this->Account_model->get_campaigns_ended_user_account( $post['name'], $this->session->id );
		$customHTML = "";
		foreach ($result as $key => $cust) {
			$get_payout = $this->Account_model->get_payout_settings( $cust->user_id, $cust->campaign_id );
			foreach ($get_payout as $key => $value) {
				 $customHTML.='<tr>';
				 $customHTML.='<td class="text-center">'.$value->payout_cutoff.'</td>';
				 $customHTML.='<td class="text-center">'.$value->payout_status.'</td>';
				 $customHTML.='<td class="text-center">'.$value->payout_amount.'</td>';
				 $customHTML.='</tr>';
			}
			
		}
		echo json_encode($customHTML);
	}

	public function track($id='') {
		$data['link'] = 'orders';
		$data['tracking_number'] = 	$id;	
		$data['tracking'] = $this->Order_model->get_tracking_id( $id, $this->session->customer_id );
		$this->load->view_store('account-order-tracking', $data);
		
	}

	private function email_the_user($data){
		# smtp settings
		$this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout');
		
		#send email for ipn
		$this->load->library('email');
		$config['protocol'] = 'smtp';
		$config['smtp_host'] = $this->smtp_host;
		$config['smtp_user'] = $this->smtp_user;
		$config['smtp_pass'] = $this->smtp_pass;
		$config['smtp_port'] = $this->smtp_port;
		$config['smtp_timeout'] = $this->smtp_timeout;
		$config['mailtype'] = 'html';
		$config['priority'] = '1';	
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['crlf'] =  "\r\n";
		$config['newline'] = "\r\n";
		
		
		$this->email->initialize($config);
		$this->email->from('noreply@tzilla.com', $data['title']);
		$this->email->to( $data['email'] );
		$this->email->subject($data['subject']);		
		$this->email->message($data['message']);
		$this->email->send();
	}


}