<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fb_user_model extends CI_Model {

    function __construct()
    {

        parent::__construct();

    }

    public function check_user_by_email($email){ //scpv1-1 update
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );
        $this->db->where( array( "scp_users.email" => $email, "scp_customer.store_id" => STOREID ) );        
        $result = $this->db->get_where("scp_customer", array( "scp_users.email" => $email, "scp_customer.store_id" => STOREID ) )->result();
        return $result;
    }

    public function save_fb_user($data){ //scpv1-1 update
		$this->db->insert('scp_users', $data);		
		return $this->db->insert_id();
    }

    public function update_fb_user($email, $id, $data){ // scpv1-1 update
        $arr = array( "email" => $email, "id" => $id );
		$this->db->where( $arr );
		$this->db->update('scp_users', $data);

		return $this->db->affected_rows();
    }

    public function save_fb_cust($data, $id){ // scpv1-1 update
        $row =  array(                        
                        'user_id' => $id,
                        'store_id' => STOREID,
                        'cust_firstname' => $data["cust_firstname"],
                        'cust_lastname' => $data["cust_lastname"],
                        'cust_email' => $data["cust_email"],
                        'customer_code' => $data["customer_code"],
                        'date_created' => date("Y-m-d H:i:s")
                        );

        $this->db->insert('scp_customer', $row);
        return $this->db->insert_id();
    }

     public function update_fb_cust($email, $id, $data){ // scpv1-1 update
        $arr = array( "cust_email" => $email, "user_id" => $id );
        $this->db->where( $arr );
        $this->db->set('cust_firstname', $data['firstname']);
        $this->db->set('cust_lastname', $data['lastname']);
        $return_id = $this->db->update( 'scp_customer'); 
        return $return_id;

    }

}

