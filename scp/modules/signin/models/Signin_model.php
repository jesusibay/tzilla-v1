<?php
class Signin_model extends CI_Model {

    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->load->library( array('form_validation', 'Aauth', 'session', 'email') );
        // config/aauth.php

        $this->load->helper( array( 'url','string','email','googleauthenticator_helper' ) );

        // config/aauth.php
        $this->config->load('aauth');
        $this->config_vars = $this->load->config->item('aauth');

        $this->aauth_db = $this->load->database($this->config_vars['db_profile'], TRUE); 
        
        // load error and info messages from flashdata (but don't store back in flashdata)
        $this->errors = $this->session->flashdata('errors') ?: array();
        $this->infos = $this->session->flashdata('infos') ?: array();
    }

    public function check_user_by_email($email){     
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );
        $this->db->where( array( "scp_users.email" => $email, "scp_customer.store_id" => STOREID ) );        
        $result = $this->db->get("scp_customer")->result();
        return $result;
    }

    public function check_cust_by_email($email){ // check customer if exist within the store front
        return $this->db->get_where( 'scp_customer', array( 'cust_email' => $email, 'store_id' => STOREID ) )->result();
    }

    public function get_user_by_email( $email ){ //scpv1-1 update
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );
        $this->db->where( array( "scp_users.email" => $email, "scp_customer.store_id" => STOREID ) );       
        $result = $this->db->get("scp_customer")->row_array();
        return $result;
    }

    public function get_user_by_id( $id ){
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );
        $this->db->where( array( "scp_users.id" => $id, "scp_customer.store_id" => STOREID ) );       
        $result = $this->db->get("scp_customer")->row_array();
        return $result;
    }

    public function update_cust_subscribe($cust_email, $id){ 
        $arr = array( 'customer_id' => $id, 'store_id' => STOREID);
        $this->db->where($arr);
        return $this->db->update('scp_customer', array( 'cust_subscribe' => 1 )); 
            
    }
    
    public function check_user_by_userid($email){ 
        $result = $this->db->get_where( 'scp_customer', array( 'email' => $email ) );
        return $result;
    }

    public function get_subscription_by_email( $email ){ //scpv1-1 update
        $this->db->where( array( "subscribe_email" => $email, "store_id" => STOREID ) );       
        $result = $this->db->get("scp_email_subscription")->result();
        return $result;
    }

    public function add_new_email_subscription($email){ //scpv1-1 update
        $row =  array(                        
                        'subscribe_email' => $email,
                        'store_id' => STOREID,                        
                        'date_created' => date("Y-m-d H:i:s")
                        );

        $this->db->insert('scp_email_subscription', $row);
        return $this->db->insert_id();
    }

    public function add_new_user($data, $verification_code){
        $row =  array(                        
                        'email' => $data["username"],
                        'password' => $data["password"],
                        'name' => $data["firstname"].' '.$data["lastname"],
                        'firstname' => $data["firstname"],
                        'lastname' => $data["lastname"],
                        'status' => 0,
                        'loggedin_via' => 'sign-in',
                        'verification_code' => $verification_code
                        );

        $this->db->insert('scp_users', $row);
        return $this->db->insert_id();
    }

    public function add_new_cust($data, $id){
        $customer_code = $this->create_customer_on_erp( $data["firstname"]." ".$data["lastname"], $data["username"] );
        if( $customer_code ){
           $row =  array(                        
                        'user_id' => $id,
                        'store_id' => STOREID,
                        'cust_firstname' => $data["firstname"],
                        'cust_lastname' => $data["lastname"],
                        'cust_email' => $data["username"],
                        'customer_code' => $customer_code,
                        'date_created' => date("Y-m-d H:i:s")
                        );

            $id = $this->db->insert('scp_customer', $row);
            return $id; 
        }
        
    }

    public function create_customer_on_erp( $name = "sample name", $username = "aaa@aaa.aaa", $password = "admin" ) {
        $r = "error";
        if ( $username ){
            $url = $this->config->item('erp_api_create_customer');
            $contenttype = "application/json"; // or application/xml

            $jsonData = array(
                                "CustomerName" => $name,
                                "Address" => "9500 Gilman Drive",
                                "City" => "La Jolla",
                                "State" => "CA",
                                "County" => "CA",
                                "PostalCode" => "92093",
                                "Country" => "United States of America",
                                "Telephone" => "",
                                "email" => $username,
                                "password" => $password,
                            );

            $ch = curl_init();

            curl_setopt( $ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype;charset=UTF-8") );
            curl_setopt( $ch, CURLOPT_HEADER, 0 );
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_POST, true );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $jsonData ) );
            //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            //curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            
            $content = curl_exec($ch);

            $aResponse = json_decode($content,true);

            $r = $aResponse["customerCode"];

            curl_close($ch);
        }

        return $r;
    }

    private function create_customer_code( $name, $username, $password = "admin" ) {
        // $url = "http://12.9.181.75/erp/api/salesorder/save";
        $url = $this->config->item('erp_api_create_sales_order');
        $contenttype = "application/json"; // or application/xml
        $requestData = json_encode(array("CustomerName"=>$name,"Address"=>"","City"=>"","State"=>"CA","County"=>"CA","PostalCode"=>"92093","Country"=>"United States of America","Telephone"=>"","email"=>$username,"password"=>$password));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype;charset=UTF-8") );
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$requestData);
        //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        //curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");

        $result = curl_exec ($ch);

        if ( $result !== false ) {
            $array = json_decode($result,true);    

            print_r($array);
            
        } else {
            print( curl_error( $ch ) );
        }

        curl_close ($ch);
    }

    public function check_user_group(){
        return $this->db->get_where( 'scp_groups', array( 'name' => 'Customer' ) )->result();
    }
    public function add_user_to_group($user_id, $group_id){
        $row =  array(                        
                        'user_id' => $user_id,
                        'group_id' => $group_id
                        );

        return $this->db->insert('scp_user_to_group', $row);
    }

    public function check_email_to_activate($random, $user_id, $email){
        $arr = array('verification_code'=> $random, 'id'=> $user_id, 'email'=> $email);
        $r = $this->db->get_where( 'scp_users', $arr)->row_array();
        
        return $r; 

    }

    public function activate_user($id){
        $this->db->where('id', $id );
        $this->db->set('verification_code', '');
        $this->db->set('status', 1);
        return $this->db->update('scp_users');
    }

     public function update_user_fields( $id, $field, $val ){ //scpv1-1 update attempts
        $arr = array('id'=> $id);
        $this->db->where( $arr );
        $this->db->set($field, $val);
        $return_id = $this->db->update( 'scp_users'); 
        return $return_id;
    }

    public function update_password($pass, $id){ //scpv1-1 update for register user 
        $arr = array('id'=> $id);
        $this->db->where( $arr );
        $this->db->set('password', $pass);
        $return_id = $this->db->update( 'scp_users'); 
        return $return_id;
    }

    public function update_user($email, $data){
        $this->db->where('email', $email);
        $this->db->update('scp_users', $data);

        return $this->db->row()->id;
    }

    public function update_customer($user_id, $data){
        $this->db->where('user_id', $user_id);
        $this->db->update('scp_customer', $data);

        return $this->db->affected_rows();
    }

    public function login_check($email, $pass){
        $this->db->select('scp_users.*');
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );
        $this->db->where( array( "scp_users.email" => $email, "scp_users.password" => $pass, "scp_customer.store_id" => STOREID ) );       
        $result = $this->db->get("scp_customer")->result_array();
        return $result;
    }

    public function save_user($data){
        $this->db->insert('scp_users', $data);
        
        return $this->db->insert_id();

    }
    public function save_customer($data1){
        $this->db->insert('scp_customer', $data1);
        
        return $this->db->insert_id();
        
    }

    public function get_store_id(){
        $result = $this->db->get_where('scp_stores', array('store_name' => STORE ) );
        return $result->row()->store_id;
    }


    public function update_pass($user_id, $data2){
             
        $this->db->where('id', $user_id);
        $this->db->update('scp_users', $data2 );
        return $this->db->affected_rows();
    }
    
    public function get_my_user($email){
        $this->aauth_db->select('*');
       $result =  $this->aauth_db->get_where( $this->aauth->config_vars['users'] , array('email' =>  $email) )->result_array();
        return $result;
    }


    public function reset_password($email) {
   
        $user = $this->get_my_user($email);
        if ($user)
        { 
            $this->load->helper('string');
            $this->load->library('email');
            
            $new_password       = random_string('alnum', 8);
            $user['password']   = $this->aauth->hash_password($new_password, $user_id);
            $this->db->where('email', $email);
            return $this->aauth_db->update($this->aauth->config_vars['users'], array( 'password' => $password ));

            $this->email->from($this->config->item('email'), $this->config->item('site_name'));
            $this->email->to($email);
            $this->email->subject($this->config->item('site_name').': Password Reset');
            $this->email->message('Your password has been reset to <strong>'. $new_password .'</strong>.');
            $this->email->send();
            
            return true;
        }
        else
        {
            return false;
        }
    }

    public function reset_pass($random, $email){
        
        $this->aauth_db->where('email', $email);
        $this->aauth_db->update($this->aauth->config_vars['users'], array('verification_code'=> $random) );
         
        return $this->db->affected_rows();
    
    }

    public function reset_password_verification($random, $id, $email){ //scpv1-1 update for password verification
        
        $this->aauth_db->where( array( "email" => $email, "id" => $id ) );
        $this->aauth_db->update($this->aauth->config_vars['users'], array('forgot_exp'=> $random) );
         
        return $this->db->affected_rows();
    
    }

    public function get_user_by_verification( $random, $id, $email ){ //scpv1-1 update for check user and email
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );
        $this->db->where( array( "scp_users.email" => $email, "scp_customer.store_id" => STOREID, "scp_users.id" => $id, "scp_users.forgot_exp" => $random ) );       
        $result = $this->db->get("scp_customer")->row_array();
        return $result;
    }

    public function check_old_password($id, $password){ //scpv1-1 update for forgot password        
        return $this->db->get_where('scp_users', array( 'id'=> $id, 'password' => $password ))->row_array();     
    }

    public function forgot_password_reset($id, $password){ //scpv1-1 update for forgot password        
        $this->db->where( array("id" => $id ) );        
        return $this->db->update('scp_users', array( 'forgot_exp'=> "", 'password' => $password, 'login_attempts' => 0 ));     
    }


    public function check_email_random($random, $email){
        $arr = array('verification_code'=> $random, 'email'=> $email);
        $r = $this->aauth_db->get_where($this->aauth->config_vars['users'],$arr);
        
        return $r->num_rows(); 

    }

    public function update_pass_new($newpass, $email , $id){
       $new =  $this->aauth->hash_password($newpass, $id);
       $arr = array('email' =>  $email, 'id' => $id );
       $this->aauth_db->where( $arr );
       $this->aauth_db->update('scp_users', array('password'=> $new, 'verification_code' => '') );
         
        return $this->db->affected_rows();

    }

     public function check_to_get($email){
            $result = $this->db->get_where( 'scp_customer', array( 'cust_email' => $email ) );

            return $result->row()->customer_id;
    }

     public function get_password( $id ){
        $this->db->where( "id" , $id );  
        $result = $this->db->get("scp_users")->result();
        return $result;
    }

    public function change_pass($password , $id)
        { 

            $this->db->where('id',$id);
            $result = $this->db->update('scp_users', array( 'password' => $password ));
            return $result;
            
        }


    public function insert_contact($save_data){

        $this->db->insert('scp_contact_us', $save_data);
        return $this->db->insert_id();
    }

    }