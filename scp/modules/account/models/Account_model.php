<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_model extends CI_Model {

    function __construct()
    {

        parent::__construct();

    }

    public function get_details( $id ){
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );
        $this->db->where( array( "scp_customer.user_id" => $id, "scp_customer.store_id" => STOREID ) );        
        $result = $this->db->get("scp_customer")->result();
        return $result;
    }    

    public function get_user_info( $id ){
        $this->db->where( "scp_customer.user_id" , $id );
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );      
        $result = $this->db->get("scp_customer")->row_array();
        return $result;
    }

    public function get_customer_by_id( $id ){
        $this->db->where( "scp_customer.customer_id" , $id );
        $this->db->JOIN('scp_users',  'scp_customer.user_id = scp_users.id', 'LEFT' );      
        $result = $this->db->get("scp_customer")->row_array();
        return $result;
    }

    public function update_profile($data, $id){
        $row =  array(                        
                        'cust_firstname' => $data["firstname"],
                        'cust_lastname' => $data["lastname"],
                        'cust_contact_no' => $data["contact"],
                        'cust_address_1' => $data["address"],
                        'cust_city' => $data["city"],
                        'cust_state' => $data["state"],
                        'cust_zip' => $data["zip"],
                        'cust_birthdate' => $data["birthdate"],
                        'school' => $data["school"],
                        'cust_bio' => $data["bio"]
                        );
        $this->db->where( 'user_id', $id );
        $customer =  $this->db->update( 'scp_customer', $row);
        if($customer){
            $this->db->where('id', $id );
            $this->db->set('name', $data['firstname']." ".$data['lastname']);
            $this->db->set('firstname', $data['firstname']);
            $this->db->set('lastname', $data['lastname']);
            $return_id = $this->db->update( 'scp_users'); 
            return $return_id;
        } 
    }

    public function update_user_supplier_code($scode, $id){
        $arr = array( "id" => $id );
        $val = array( 'supplier_code' => $scode );
        $this->db->where( $arr );
        $this->db->update('scp_users', $val);
        return $this->db->affected_rows();
    }

    public function change_pass($password, $id){ 
        $arr = array( 'id' => $id);
        $this->db->where($arr);
        return $this->db->update('scp_users', array( 'password' => $password )); 
            
    }

    public function get_states($id){
        return $this->db->get_where('scp_states', array('country_id'=>$id) )->result_array();

    }

    public function get_zip($zip_code, $state){
        return $this->db->get_where('scp_zip_to_state', array( 'zip_code'=>$zip_code, 'state'=>$state ) )->result_array();

    }

     public function get_school(){
        return $this->db->get('scp_school')->result_array();

    }

    public function get_school_id($id){
        $this->db->where('school_id', $id);   
        $return = $this->db->get('scp_school')->result();
        return $return;
    }

    public function get_school_name($search=false){   
        $search2 = str_replace("%20"," ",$search);  
        $not = '';
        $operator   = 'OR';
        $like   = '';
        $like   .= "( `school_name` ".$not."LIKE '%".$search2."%' )"  ;

        $this->db->where($like);   
        $return = $this->db->get('scp_school')->result();
        return $return;
    }

    /*payout transactions begin*/    
    public function get_account_per_campaign($id){
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_status !=' => 0 );
        $this->db->group_by('scp_campaigns.campaign_account_name');
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }
    public function get_account_per_ended_campaign($id){
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_status' => 3 );
        $this->db->group_by('scp_campaigns.campaign_account_name');
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

    public function get_campaigns_ended($id){ // pending
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_status' => 3 );
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

     public function get_campaigns_active($id){
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_status' => 1 );
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

    public function get_campaigns_ended_user_account($account, $id){ // pending
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_account_name' => $account,  'scp_campaigns.campaign_status' => 3 );
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

    public function get_campaigns_user_account($account, $id){
        $row =  array( 'scp_customer.user_id' => $id, 'scp_campaigns.campaign_account_name' => $account,  'scp_campaigns.campaign_status' => 1 );
        $this->db->JOIN('scp_customer',  'scp_campaigns.campaign_user_id = scp_customer.customer_id', 'LEFT' );
        $return = $this->db->get_where('scp_campaigns', $row )->result();
        return $return;
    }

    public function get_all_total_sales_count( $customer_id){ // get total number of sales date for all campaign
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null  );
        $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    } 

    public function get_all_total_profit_count( $customer_id){ // get total number of profit for all campaign
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null  );
        $this->db->select('sum(`campaign_profit`*`quantity`) as total_profit', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }

    public function get_all_total_items_sold_count( $customer_id){ // get total number of items sold for all campaign
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null  );
        $this->db->select_sum('scp_sales_order_items.quantity');
        // $this->db->select('sum(`quantity`)', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }

    public function get_all_total_sales_count_today( $customer_id){ // get total number of sales current date for all campaign
        $now = date('Y-m-d'); 
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null, 'scp_sales_orders.order_date >=' => $now  );
        $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    } 

    public function get_all_total_profit_count_today( $customer_id){ // get total number of profit current date for all campaign
        $now = date('Y-m-d'); 
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null, 'scp_sales_orders.order_date >=' => $now  );
        $this->db->select('sum(`campaign_profit`*`quantity`) as total_profit', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }

    public function get_all_total_items_sold_count_today( $customer_id){ // get total number of items sold current date for all campaign
        $now = date('Y-m-d'); 
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null, 'scp_sales_orders.order_date >=' => $now  );
        $this->db->select_sum('scp_sales_order_items.quantity');
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;

    }

    public function get_payout_settings( $id, $camp_id, $name ){ // this query will display payout amount per cut off
        $row =  array( 'user_id' => $id, 'campaign_id' => $camp_id, 'payout_name' => $name );
        $return = $this->db->get_where('scp_payout_transaction', $row )->result();
        return $return;
    }  

    public function get_payout_cutoff(){
        $row =  array( 'is_active' => 1 );
        $return = $this->db->get_where('scp_payout_cutoff', $row )->result();
        return $return;
    }

    public function get_payout_amount($id){
        $row =  array( 'user_id' => $id );
        $this->db->limit(1);
        $this->db->select('payout_amount');
        $this->db->order_by("transaction_id", "DESC");
        $return = $this->db->get_where('scp_payout_transaction', $row )->row_array();
        return $return;
    }    

    public function insert_payout_settings($id, $campaign_id, $amount, $name, $date){
        $row =  array(                        
                        'user_id' => $id,
                        'campaign_id' => $campaign_id,
                        'payout_amount' => $amount,
                        'payout_cutoff' => $date,
                        'payout_name' => $name,
                        'payout_status' => 'Processing',
                        'date_created' => date("Y-m-d H:i:s")
                        );

        return $this->db->insert('scp_payout_transaction', $row);
    }

     public function insert_payouts($logs){
        return $this->db->insert('scp_payouts', $logs);
    }

    public function update_payout_settings($id, $camp_id, $amount){
        $row =  array( 'payout_status' => 'Processing', 'user_id' => $id, 'campaign_id' => $camp_id );
        $this->db->where( $row );
        $this->db->set('payout_amount', $amount);
        $return_id = $this->db->update( 'scp_payout_transaction'); 
        return $return_id;
    }

    public function get_campaign_total_sales($campID){ // used by account campaigns
        $row =  array( 'scp_campaigns.campaign_id' => $campID, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null  );
        $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }
    
    /*payout transactions end*/


}