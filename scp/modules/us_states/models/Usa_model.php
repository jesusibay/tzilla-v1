<?php
class Usa_model extends CI_Model {

    public function __construct()
    {
            // Call the CI_Model constructor
            parent::__construct();
    }

    function get_states(){
        $this->db->select('state, scp_states.state_name'); 
        $this->db->from('scp_zip_to_state');
        $this->db->group_by('state');

        /*exclude
        specific ZIP Code™ for an APO/FPO/DPO (Air/Army Post Office, Fleet Post Office or Diplomatic Post Office).
        */
        $specials = array('APO', 'FPO', 'DPO');
        $this->db->where_not_in('city', $specials);
        $this->db->order_by('state', 'ASC');

        $this->db->join('scp_states','scp_states.state_code = scp_zip_to_state.state', 'left');

        $query  = $this->db->get();
        
        return $query->result();
    }

    function get_state_zip($data){

        $this->db->select('*');
        $this->db->where('state',$data['state']);
        $this->db->where('zip_code',$data['zip_code']);        
        $query  = $this->db->get('scp_zip_to_state');
        
        return $query->result();
    }

    function get_state_name($data)
    {   
        $this->db->select('*');
        $this->db->where('state_code',$data['state']);
        $query  = $this->db->get('scp_states');
        
        return $query;
    }
    
}