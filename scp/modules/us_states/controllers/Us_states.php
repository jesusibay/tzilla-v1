<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Us_states extends MX_Controller   {

	function __construct()
	{

		parent::__construct();
		$this->load->model('Us_states/Usa_model');
 	}
	
	public function index()
	{ 
		
	}

	function get_states(){
		$data = $this->Usa_model->get_states();

		return json_encode($data);
	}

	function get_cities(){
		
	}

	function get_zipcodes(){
		
	}

	function validate_state_zip(){
		$data = $this->Usa_model->get_state_zip($_POST);
		$query_state = $this->Usa_model->get_state_name($_POST);
		$staterow = $query_state->row();
		
		if (isset($staterow))
		{	
			$statename = $staterow->state_name;
		}else{
			$statename = $_POST['state'];
		}

		if(empty($data)){
			$r = array('Invalid Zip Code for '.$statename.'.');
			echo json_encode($r);
		}else{
			/*$r = array(
				'status'=>'valid',
				'message'=>'Valid Zip Code for '.$statename.'.',
				'data' => $data
				);*/
			//echo "{status:'valid', message:'Valid Zip Code for ".$_POST['state'].".',data: ".json_encode($data)."}";

			$r = true;	
			echo json_encode($r);
		}
		
	}
}