<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Checkout_model extends CI_Model {

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    function save_order($data){ //save sales orders
        $this->db->insert('scp_sales_orders', $data); 
        return $this->db->insert_id();
    }

    function update_order($id, $data){    
        $this->db->where('order_id', $id);
        $this->db->update('scp_sales_orders', $data);
    }

    function save_order_items($data){
        $this->db->insert('scp_sales_order_items', $data);
        return $this->db->insert_id();
    }
    
    function save_order_item_options($data){
        $this->db->insert('scp_sales_order_item_options', $data);
        return $this->db->insert_id();
    }

    function save_cart_items( $data ){
        $this->db->insert('scp_cart', $data);

        return $this->db->insert_id();
    }

    function get_order($order_id){
        $this->db->select('*');
        $this->db->where('order_id',$order_id);        
        return $this->db->get('scp_sales_orders');
    }

    function get_order_by_order_no( $order_no ){
        $this->db->select('*');

        $this->db->where('order_number',$order_no);

        return $this->db->get('scp_sales_orders');
    }

    function save_shipping_billing_details($data){
        $this->db->insert('scp_shipping_billing_details', $data);
        return $this->db->insert_id();
    }

    function get_customer_address($order_id){ // check by order id 
        $row =  array( 'order_id' => $order_id );
        return $this->db->get_where('scp_customer_addresses', $row )->result_array();
    }

    function save_customer_address($order_id, $data){ // add customer addresses by order id
        $row =  array(
                     'customer_id' => $this->session->customer_id,
                    'order_id' => $order_id,
                    'address_type' => 'Both',
                    'name' => $data["ship_firstname"].' '.$data["ship_lastname"],
                    'firstname' => $data["ship_firstname"],
                    'lastname' => $data["ship_lastname"],
                    'contact_no' => $data["ship_phone"],
                    'email' => $data["ship_customer_email"],
                    'address_1' => $data["ship_address"],
                    'address_2' =>'',
                    'city' => $data["ship_city"],
                    'state' => $data["ship_state"],
                    'zip' => $data["ship_zipcode"],
                    'date_created' => date("Y-m-d H:i:s")
                    );

        $saved = $this->db->insert('scp_customer_addresses', $row);
        return $saved;
    } 

     function shipping_customer_address($order_id, $data){ // add customer addresses by order id
        $row =  array(
                    'customer_id' => $this->session->customer_id,
                    'order_id' => $order_id,
                    'address_type' => 'Shipping',
                    'name' => $data["ship_firstname"].' '.$data["ship_lastname"],
                    'firstname' => $data["ship_firstname"],
                    'lastname' => $data["ship_lastname"],
                    'contact_no' => $data["ship_phone"],
                    'email' => $data["ship_customer_email"],
                    'address_1' => $data["ship_address"],
                    'address_2' =>'',
                    'city' => $data["ship_city"],
                    'state' => $data["ship_state"],
                    'zip' => $data["ship_zipcode"],
                    'date_created' => date("Y-m-d H:i:s")
                    );

        $saved = $this->db->insert('scp_customer_addresses', $row);
        return $saved;
    } 

     function billing_customer_address($order_id, $data){ // add customer addresses by order id
        $row =  array(
                    'customer_id' => $this->session->customer_id,
                    'order_id' => $order_id,
                    'address_type' => 'Billing',
                    'name' => $data["firstname"].' '.$data["lastname"],
                    'firstname' => $data["firstname"],
                    'lastname' => $data["lastname"],
                    'contact_no' => $data["phone"],
                    'email' => $data["bill_email"],
                    'address_1' => $data["address"],
                    'address_2' =>'',
                    'city' => $data["city"],
                    'state' => $data["state"],
                    'zip' => $data["zip"],
                    'date_created' => date("Y-m-d H:i:s")
                    );

        $saved = $this->db->insert('scp_customer_addresses', $row);
        return $saved;
    } 

    function update_customer_address($order_id, $data){
        $this->db->where('order_id', $order_id);
        $row =  array('address_type' => $data['address_type']);

        return $this->db->update('scp_customer_addresses', $row);
    }
    

    function get_cart_items($id){
        $this->db->where_in('cart_id', $id);
        $result = $this->db->get('scp_cart')->result();
        return $result;
    }


    function get_customer($customer_id){ 
        $this->db->where( 'customer_id' , $customer_id );
        return $this->db->get( 'scp_customer')->result();
    }

    function update_profile_info($id, $data){
        $this->db->where('customer_id', $id); 
        return $this->db->update('scp_customer', $data);
    }
     function check_generated_image($id){
        $this->db->select('ve_design_id,thumbnail,image_generated');
        $this->db->where('ve_design_id',  $id);
        $result  = $this->db->get('scp_sales_order_item_options')->result();

        return $result;
    }
     public function update_artwork_generated_image( $id, $name )
    {

        $data = array(
               'thumbnail' => $name,        
               'image_generated' => 1
            );

        $this->db->where('ve_design_id', $id);

        $query = $this->db->update('scp_sales_order_item_options', $data); 

        if ( !$query ) {
            $result = 'error';
        } else {
            $result = 'success';
        }

        return $result;
    }

}