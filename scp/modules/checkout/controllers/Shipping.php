<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping extends MX_Controller   {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model(	array('account/Account_model', 'campaign/Campaigns_model', 'signin/Signin_model', 'signin/Fb_user_model', 'Checkout_model' ) );
		$this->load->helper('url');
		$this->load->helper('security');
        $this->load->library( array('form_validation', 'Aauth', 'Curl', 'session', 'pagination', 'upload') );

		$this->load->module('Settings');

        $this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 	
		date_default_timezone_set('America/Los_Angeles');	
    }
	
	public function index()
	{ 

		

		$this->load->module('shipping_methods'); 

		$data['states'] =  $this->Account_model->get_states('236');
		$data['customer_info'] =  $this->Account_model->get_user_info( $this->session->id );
		
		$totalQuantity = 0;
		$totalWeight = 0;
		$totalSubtotal = 0;
		$weight_list = $this->config->item("weight_per_shirt");

		$cartId = $this->session->cart_items_id;
		$data['cartItems'] = $this->Checkout_model->get_cart_items($cartId);
		
		foreach ($data['cartItems'] as $key => $cartTotal) {
			$totalQuantity = $totalQuantity + $cartTotal->quantity;
			$totalSubtotal = $totalSubtotal + $cartTotal->subtotal;

			

			$design = unserialize($cartTotal->designs);

			$mitemsArr = explode("-", $cartTotal->matrix_items);

			if ( array_key_exists( $mitemsArr[1], $weight_list ) ) {
				if(isset($weight_list[ $mitemsArr[1] ][ $mitemsArr[3] ]))
				{
					$totalWeight += $weight_list[ $mitemsArr[1] ][ $mitemsArr[3] ] * $cartTotal->quantity;	
				}
				else
				{		
					$totalWeight += $weight_list[ $mitemsArr[1] ]["M"] * $cartTotal->quantity;
				}
				
			} else {
				$totalWeight += 1 * $cartTotal->quantity;
			}

			
		}

		$data['totalQuantity'] = $totalQuantity;
		$data['totalWeight'] = $totalWeight;
		$data['Subtotal'] = $totalSubtotal;
		
		$data['fedex_shipping'] = '';
		$shipping['message'] = '';
		$shipping = $this->shipping_methods->fedex($data['customer_info'], $totalWeight );
		
		if( $shipping['message'] != "invalid zipcode" ){
			$data['fedex_shipping'] = json_decode($shipping['message']); 
		 
			
		}else{
			$data['fedex_shipping'] =	 json_decode($shipping['message']); 
				$this->session->set_userdata('shipping_method', $shipping['message']);
				
		}
		$data['tax_amount'] = 0;
		
		if(!empty($data['customer_info']['cust_state']))
		{
			
			$this->load->module('settings');
			$setting = $this->settings->get('tax');

			$tax_percent = 0;
			$customer_state = $this->session->customer_bill_state;
			foreach ($setting as $key => $value) {
				if($key===$customer_state) $tax_percent = $setting[$key];
			}

			$tax_amount = ($data['Subtotal'] - $discount_amount) * ($tax_percent / 100);
			$data['tax_amount'] = number_format($tax_amount,2);
		}

		
		$this->load->view_store( 'shipping-details', $data );
	}

	public function get_tax(){
		$post = $this->input->post(null, true);
		
		$discount_amount = 0;
		$data['tax_amount'] = 0.00;
		if(!empty($post['state']))
		{
			$this->load->module('settings');
			$setting = $this->settings->get('tax');

			$tax_percent = 0;
			$customer_state = $post['state'];
			foreach ($setting as $key => $value) {
				if($key===$customer_state) $tax_percent = $setting[$key];
			}

			$tax_price = ($post['subtotal'] - $discount_amount) * ($tax_percent / 100);
			$data['tax_amount'] = number_format($tax_price,2);

		}
		echo json_encode($data);
	}

	public function get_shipping_method_zip(){
		$this->load->module('shipping_methods'); 
		$post = $this->input->post(null,true);
		$shipping = $this->shipping_methods->fedex($post, $post['totalWght'] );
		$data['fedex_shipping'] =	 json_decode($shipping['message']); 
		 
		$data['shipping_method'] = '';
		$data['fedex_fee'] = '';
		$data['shippingHtml'] = '';
		$count = 0;
		foreach ($data['fedex_shipping'] as $key => $fedex_fee) {
				# code...
				$this->session->set_userdata('shipping_method', $key);
				$this->session->set_userdata('shipping_fee', $fedex_fee);
				$data['shipping_method'] = $key;
				$data['fedex_fee'] = $fedex_fee;
				$data['status'] = "success";
				$count++;
				$shipping_method = str_replace("_", " ", $data['shipping_method'] );
				if( $count == 1){
					$data['shippingHtml'] .= '<div class="shipping-radio-cont payment-radio-cont"><input type="radio" name="shipping_method" class="green-circle select-shipping" data-shipping-method="'.$data['shipping_method'].'" value="'.$data['fedex_fee'].'" checked=""><span class="radio-title gregular font-regular gray-dark">'.$shipping_method.'</span><span class="radio-val gregular font-regular gray-dark shipping-method-fee">$ '.$data['fedex_fee'].'</span></div>';
				}
				if( $count >= 2 ){
					$data['shippingHtml'] .= '<div class="shipping-radio-cont payment-radio-cont2"><input type="radio" name="shipping_method" class="green-circle select-shipping" data-shipping-method="'.$data['shipping_method'].'" value="'.$data['fedex_fee'].'" checked=""><span class="radio-title gregular font-regular gray-dark">'.$shipping_method.'</span><span class="radio-val gregular font-regular gray-dark shipping-method-fee">$ '.$data['fedex_fee'].'</span></div>';
				}
		}


		

		echo json_encode($data);

	}


	public function blocks(){

		
		$this->load->view_store( 'blocks-checkout/payment-method');

	}

	public function save_customer_address($order_id){ /* Function for saving the customer details*/		
		$post = $this->input->post(null, true);		
		$get_address = $this->Checkout_model->get_customer_address($order_id);
		if( count($get_address) > 0){
			$this->Checkout_model->update_customer_address($order_id, $post);
			$str['status'] = "success";
		}else{
			$saved = $this->Checkout_model->save_customer_address($order_id, $post);
			if( $saved ){
				$str['status'] = "success";
			}
		}		
		echo json_encode($str);
	}

	public function apply(){
		$shipping_method = explode(':', $_POST['shipping_method_option']);

		$this->session->set_userdata('shipping_method',$shipping_method[0]);
		$this->session->set_userdata('shipping_method_currency',$shipping_method[1]);
		$this->session->set_userdata('shipping_method_fee',$shipping_method[2]);

		echo 'done '. $this->session->shipping_method;
	}
}