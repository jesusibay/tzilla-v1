<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends MX_Controller   {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model(	array('account/Account_model', 'campaign/Campaigns_model', 'signin/Signin_model', 'signin/fb_user_model', 'checkout/Checkout_model') );
		$this->load->helper('url');
		$this->load->helper('security');
        $this->load->library( array('form_validation', 'Aauth', 'Curl', 'session', 'pagination', 'upload') );

		$this->load->module('Settings');

        $this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 	
		date_default_timezone_set('America/Los_Angeles');	
    }
	// 
	public function index()
	{ 	
		$data['states'] =  $this->Account_model->get_states('236');
		
		$cartId = $this->session->cart_items_id;
		$data['cartItems'] = $this->Checkout_model->get_cart_items($cartId);
		
		foreach ($data['cartItems'] as $key => $cartValue) {
			$designs = unserialize($cartValue->designs);			
		}

		$this->load->view_store( 'payment-method', $data );
	}

	function tax_setting(){

		$this->load->module('settings');
		$setting = $this->settings->get('tax');

		$tax_percent = 0;
		$customer_state = $_POST['state'];
		foreach ($setting as $key => $value) {
			if($key===$customer_state) $tax_percent = $setting[$key];
		}
		

		$discount_amount = (isset($this->session->discount_amount))?$this->session->discount_amount:0;

		$tax_amount = ($this->cart->total() - $discount_amount) * ($tax_percent / 100);
		$tax_amount = number_format($tax_amount,2);
		

		echo json_encode(array(
			'tax_amount' 	=> $tax_amount,
			'tax_percent' 	=> $tax_percent			
			));

	}
}