<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends MX_Controller   {
	
	public $customer_id='';
	function __construct()
    {
        parent::__construct();
        $this->load->model(	array('account/Account_model', 'campaign/Campaigns_model', 'signin/Signin_model', 'signin/Fb_user_model', 'order/Order_model', 'settings/Settings_model', 'inventory/Inventory_model', 'Checkout_model') );
        // #change with actual id once login module is working
        // $this->customer_id = 5;
        $this->load->library( array('Aauth', 'session', 'email') );
        $this->smtp_host = $this->config->item('smtp_host');
		$this->smtp_user = $this->config->item('smtp_user');
		$this->smtp_pass = $this->config->item('smtp_pass');
		$this->smtp_port = $this->config->item('smtp_port');
		$this->smtp_timeout = $this->config->item('smtp_timeout'); 	
		date_default_timezone_set('America/Los_Angeles');	
    }
	
	public function index()
	{ 
		echo "cart..";
	}

	/*

	TEST CODE FOR CHECKOUT

	*/

	private function select_random_customer() {
		$this->db->select("customer_code AS customerCode, cust_firstname AS fName, cust_lastname AS lName, cust_email AS userName");
		$this->db->order_by("RAND()");
		$this->db->limit(1);

		$result = $this->db->get_where( "scp_customer", array( "customer_code !=" => NULL ) )->row();
		
		return $result;
	}

	private function select_random_design() {
		$this->db->select("parent_template_id AS template_id");

		$this->db->group_by("template_id");
		$this->db->order_by("RAND()");
		$this->db->limit(1);

		$result = $this->db->get( "scp_design_template_children" )->row();
		
		return $result->template_id;
	}

	public function test_checkout( $line_items = 1 ) {
		// initialize call for printing milliseconds
		$now = DateTime::createFromFormat('U.u', microtime(true));
		// print start execute
		echo "Started @ ".$now->format("H:i:s.u")."<br>";

		// load customer data
		$customer = $this->select_random_customer();

		$this->load->module('template');

		$line_items = $line_items > 0 ? $line_items : 1;
		$order_data = array();
		$order_item_data = array();

		for ( $a = 0; $a < $line_items; $a++ ) {
			// get design id
			// $data['id'] = $this->select_random_design();
			$data['id'] = 701;
			$data['AddOnType'] = "ICON";
			$data['iconCategory'] = "";

			$design_id = $this->template->get_design_id( $data );

			$shirt_alternatives_data = array("NL-1533-AHE-S_ITEM-001107");

			$design_data = array(	"location" => "FRONT",
									"design_id" => $design_id,
									"vector_id" => $design_id,
									"x_coordinate" => "-1",
									"y_coordinate" => "13",
									"position_uom" => "millimeter",
									"design_width" => 1,
									"design_height" => 1,
									"percentage" => 1,
									"design_uom" => "inches",
									"printing_pallet" => "7",
									"art_printer_setup" => "77",
									"temp_image" => "http://162.217.193.70/vector-engine-v4/images/fb2e45a7-8b7a-4e78-bdfd-0fadcd8989a0",
									"nsItemNameFront" => $design_id,
									"nsItemDescriptionFront" => $design_id,
									"nsCostFront" => 0,
									"nsPriceFront" => 7,
									"nsDesignTypeFront" => "",
									"nsDesignUrlFront" => "http://162.217.193.70/vector-engine-v4/images/fb2e45a7-8b7a-4e78-bdfd-0fadcd8989a0",
									"supplierCode" => "SUP-000197",
									"template_id" => $data['id']
								);

			$item_data = array(	"scp_item_id" => "462",
								"quantity" => "1",
								"line_no" => $a+1,
								"matrix_code"  => "NL-1533-AHE-S_ITEM-001107",
								"shirt_alternatives"  => $shirt_alternatives_data,
								"generic_shirt_color_code" => "AHE",
								"generic_color_name" => "ATHLETIC HEATHER",
								"shirt_group_plan" => "Junior/Womens Premium Racerback",
								"design_name" => "Default",
								"garment_type" => "Cotton",
								"printer_setup" => "77",
								"print_type" => "KORNIT",
								"unit_of_measure" => "EACH",
								"asPrice" => "17",
								"asQty" => "1",
								"matrixItem" => "ITEM-001107",
								"asDesignType" => "sample string",
								"asItemName" => $design_id."_ITEM-001107",
								"asItemDescription" => $design_id."_ITEM-001107",
								"templateCode" => "Shirt",
								"asDesignUrl" => "http://162.217.193.70/vector-engine-v4/images/fb2e45a7-8b7a-4e78-bdfd-0fadcd8989a0",
								"itemClassification" => "",
								"classificationFilter" => "",
								"startDate" => "05/29/2017",
								"endDate" => "05/29/2017",
								"salesGoal" => "500.00",
								"title" => "Sample Transactions",
								"colors" => "",
								"school" => "",
								"notes" => "",
								"supplier_code" => "SUP-000197",
								"designs" => array( $design_data )
							);

			array_push( $order_item_data, $item_data );
		}

		$order_data['processOrderModel'] = array(
										"customerCode" => $customer->customerCode,
										"userName" => $customer->userName,
										"passWord" => "admin",
										"freight" => $line_items*4,
										"total" => $line_items,
										"shippingNotes" => "none",
										"shippingMethod" => "FEDEX_GROUND",
										"paymentNotes" => "Wepay Payment",
										"notes" => "none",
										"shipToName" => $customer->fName." ".$customer->lName,
										"shipToEmail" => $customer->userName,
										"shipToPhone" => "1234567890",
										"shipToAddress" => "GSW 123",
										"shipToCity" => "OAKLAND",
										"shipToZip" => "94604",
										"shipToState" => "CA",
										"shipToCounty" => "Los Angeles",
										"shipToCountry" => "United States of America",
										"websiteCode" => "WEB-000001",
										"alternateOrder" => rand(500,1000),
										"wepay_checkout_id" => "171928882"
									);

		$order_data['salesOrderModel'] = array( "items" => $order_item_data );

		$erp_json_data = stripcslashes( json_encode( $order_data ) );

		echo "<pre>";
		print_r( $erp_json_data );
		echo "</pre>";

		// $url = "http://12.9.181.75/erpwebapi/TzillaApi/ProcessSalesOrder";
		// $ch = curl_init();

		// curl_setopt($ch, CURLOPT_URL, $url);
		// curl_setopt($ch, CURLOPT_HEADER, FALSE);
		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/json"));
		// curl_setopt($ch, CURLOPT_POST, 1);
		// curl_setopt($ch, CURLOPT_POSTFIELDS, $erp_json_data);

		// echo $content = curl_exec($ch);

		// curl_close($ch);        

		// $aResponse = json_decode($content,true);

		// print end execute
		echo "<br />Ended @ ".$now->format("H:i:s.u")."<br />";
	}

	public function set_shipping_session(){ //move to payment options
		$post = $this->input->post(null,true);
		
		$userData = array(
			'address_type' => $post['address_type'],
			'customer_ship_lastname' => $post['lastname'],
			'customer_ship_firstname' => $post['firstname'],
			'customer_ship_address' => $post['address_1'] ,
			'customer_ship_city' => $post['city'],
			'customer_ship_state' => $post['state'],
			'customer_ship_country' => 'US',
			'customer_ship_zipcode' => $post['zip'] ,
			'customer_ship_phone' => $post['contact_no'],
			'shipping_method' => $post['shipping_method'],
			'shipping_fee' => $post['shipping_fee'],
			'subtotal' => $post['subtotal'],
			'total' => $post['totalPrice'],
			'tax' => $post['tax_fee'],
			'school_id' => $this->session->school_id
			);


		
		$this->session->set_userdata("customer_data", $userData);


		if( $post['update_info'] > 0 ){ //update user info
			
			$id = $this->session->customer_id;


			$customer = array(			
			'cust_lastname' => $post['lastname'],
			'cust_firstname' => $post['firstname'],
			'cust_email' => $post['email'],
			'cust_address_1' => $post['address_1'] ,
			'cust_address_2' => '' ,
			'cust_city' => $post['city'],
			'cust_state' => $post['state'],
			'cust_zip' => $post['zip'] ,
			'cust_contact_no' => $post['contact_no'],
			'store_id' => STOREID,
			'date_updated' => date('Y-m-d H:i:s')
			
			);

			foreach ($userData as $key => $shipping_data) {
				$this->session->set_userdata($key, $shipping_data);
			}

			$update = $this->Checkout_model->update_profile_info($id, $customer);
			if( $update) {
				$data['status'] = 'success';
			}

		} else{  

			foreach ($userData as $key => $shipping_data) {
				$this->session->set_userdata($key, $shipping_data);
			}
			
			$data['status']= "success";
		}	

		
		echo json_encode($data); 
	}



	public function save_customer_address($order_id){ /* Function for saving the customer details*/		
		
		$postq = $this->session->userdata('shipping_billing_add');
		
			if($this->input->post('optradio') == 'Both'){
				$this->Checkout_model->save_customer_address($order_id, $postq);
				$str['status'] = "success";
			}else{
				$saved2 = $this->Checkout_model->billing_customer_address($order_id, $postq);
				if( $saved2 ){
					$saved = $this->Checkout_model->shipping_customer_address($order_id, $postq);
					$str['status'] = "success";
				}
				
			}
			
		
				
		// echo json_encode($str);
	}


	/*this for saving to scp sales orders 040717 */

	public function save_sales_order()
	{

		
		$post = $this->input->post(null, true);
		
		$shipping_fee = $this->session->shipping_fee;
	

    	$this->session->set_userdata("shipping_billing_add", $post);
		
		$this->session->set_userdata('bill_email', $this->input->post('bill_email', TRUE));

		
		$this->session->set_userdata('grand_total_amount', $this->input->post('grand_total_amount', TRUE));
		$this->session->set_userdata('currency_symbol', $this->input->post('currency_symbol', TRUE));		
			
		if($this->input->post('shipping_method')=="Free Shipping"){ $require_shipping = false;} else{ $require_shipping=true;}

		/***
		* Do re-calculation so any altered post data with amount will not be considered
		*/		
		##compute shipping fee
		$shipping_method = $this->input->post('shipping_method');
		
		
		if(isset($shipping_method)){
			$this->load->module('shipping_methods');
			
			$usps = json_decode($this->shipping_methods->usps($shipping_method));
			
			if(isset($usps->fee)) $shipping_fee = $usps->fee;			
		}
		$shipping_fee;

		##TODO: compute discount
		$discount_id = 0;
		$discount_amount = 0;
		$discount_shipping = 0;	

		$taxAMt =  str_replace(',', '', $this->session->tax);
		$tax_amount = $taxAMt;
		
		
		##compute grand total
		$subtotal_rate = str_replace(',', '', $this->session->subtotal);

		$grand_total_amount = ( ( $subtotal_rate - $discount_amount ) + ( $shipping_fee - $discount_shipping ) + $tax_amount );
		
		/*note: order_number will be the checkout id from wepay. just append STOREID-1234567890*/

		//TODO: save customer information, billing & shipping information to db

		## Saving to orders table
		
		//save order
		$this->load->model('checkout/checkout_model');

		$customer_id = $this->session->customer_id;	
		if(isset($this->session->customer_id)) $customer_id = $this->session->customer_id;
		
		// $agreement_chkbox = ($this->input->post('agreement_chkbox')=="On")? 1: 0;
		// $subscribe_to_newsletter = ( $this->input->post('subscribe_chkbox')=="on")? 1: 0; //not used 
		

		$cartId = $this->session->cart_items_id;
		$data['cartItems'] = $this->Checkout_model->get_cart_items($cartId);
		
		// Data for purchased artworks
		// foreach ($data['cartItems'] as $key => $cartValue) {
		// 	$designs = unserialize($cartValue->designs);
			
		// }

		$order_data = array(
			'customer_id' => $customer_id,
			'store_id' => STOREID,
			'order_status' => 'Order Placed',
			'order_date' => date('Y-m-d H:i:s') ,
			'shipping_method' => $shipping_method,
			'shipping_fee' => $shipping_fee,
			'tax' => $tax_amount,
			'subtotal'  => $subtotal_rate,
			'total' => $grand_total_amount,
			'discount' => $discount_amount,
			'payment_options' => 'WePay',			
			'payment_state' => 'Processing',
			'payment_info' => 'Waiting For Payment Approval',
			'wepay_changed_on' => date('Y-m-d H:i:s'),
			'referer_id' => '0',	
			'tracking_no' => '',
			'browser_used' =>	$this->ExactBrowserName(),	
			'date_created' => date('Y-m-d H:i:s')
		);

		$order_id = $this->checkout_model->save_order($order_data);
		$this->session->set_userdata('order_id', $order_id);

		//update customer address
		$result =  $this->save_customer_address($order_id);
		//update customer address

		
		//save cart items to order items
	
		foreach ($data['cartItems'] as $key => $cartValue) {
			//TODO: get item price from database and override price from cart session	
			$profit = $cartValue->selling_price - $cartValue->price;
			$order_items = array(
				
				'school_id' => $this->session->school_id, 
				'product_id' => $cartValue->product_id, 
				'order_id'=> $order_id,
				'matrix_code' => $cartValue->matrix_items,
				'quantity'=> $cartValue->quantity,
				'price'=> $cartValue->price,
				'product_name' => '',
				'shirt_style_code'=> $cartValue->shirt_style_code, //shirt_plan_id
				'shirt_size_code'=> '',
				'shirt_color_code'=> $cartValue->generic_shirt_color_code,
				'sale_price'=> $cartValue->selling_price,
				'campaign_profit'=> $profit,
				'free_shipping'=> '' ,
				'weight'=> '',
				'images'=> $cartValue->designs,
				'design_position' => '',
				'item_status' => '',
				'supplier_code' =>$cartValue->supplier_code,
				'base_price'=> $cartValue->base_price,
				'note'=> '',
				'date_created' => date('Y-m-d H:i:s')
				);

			$item_id = $this->checkout_model->save_order_items($order_items);

			$design = unserialize($cartValue->designs);

			if ( count( $design ) > 0 ) {
				$order_item_options = array(
					'order_items_id' => $item_id,
					'template_id' => ( isset($design['template_id']) ? $design['template_id'] : 0 ),
					've_design_id' => $design['design_id'],
					'Print_location' => $design['location'],
					'image' => $design['temp_image'],
					'thumbnail' => $design['temp_image'],
					'custom_text_1' => '',
					'custom_text_2' => '',
					'custom_text_3' => '',
					'custom_text_4' => '' ,
					'pattern' => '',
					'texture' => '',
					'emoji' => '',
					'icon' => '',
					'color_1' => '',
					'color_2' => '' 
				);

				$this->checkout_model->save_order_item_options($order_item_options);
			}
			
			// echo $this->session->school_id;
    	}        	

		/***
		* send payment
		*/
		/**** TEST Amounts for wepay
			$3.61, $103.61 Cause a checkout to authorize but then be denied for fraud. 
			$9.61, $109.61 Cause a checkout to authorize but then be reversed due to NSF/chargeback (within 5 minutes). 
			$6.61, $106.61 Cause a checkout to be synchronously denied during authorization. 
			$12.61, $112.61 Cause a checkout to expire within 120 seconds. 
			$763.61, $1763.61 Cause an uncaptured checkout to cancel within 120 seconds (only for checkouts in which the auto_capture flag is set to false). This behavior mimics the situation in which an authorization on a credit card expires because the funds have not been captured via a /checkout/capture call within 7 days. 
			$21.61, $24.61, $121.61, $124.61 Cause a checkout to throw an error. 
			$22.61, $25.61, $122.61, $125.61 Cause a checkout to return a mock checkout response. 
			$1000-$1020 Cause a checkout to be synchronously denied during authorization.

		/**** RESPONSE SAMPLE
		{"status":"failed", "message": "{"response":{"error":"processing_error","error_description":"Unable to charge payment method: general decline","error_code":2004}}"}
		*
		***/
		/*for test purposes:*/
		//$grand_total_amount = 0.90;

		$payment_data = array(
				'credit_card_id' 	=> $_POST['credit_card_id'],
				'require_shipping' 	=> $require_shipping,
				'shipping_fee_amount' 		=> $shipping_fee,
				'grand_total_amount'			=> $grand_total_amount,
				'currency' 			=> 'USD',
				'order_id'		=> $order_id
			);
		
		
		$result =  $this->pay('wepay',$payment_data);
		
		$p_result = json_decode($result);
		
		if(isset($p_result->checkout_id)) $this->session->set_flashdata('order_number',STOREID.'-'.$p_result->checkout_id);

		if($p_result->status=='success')
		{
			
			if(isset($p_result->checkout_id))
			{

				//update order 
				$update_data = array(
					'order_number' => STOREID.'-'.$p_result->checkout_id,
					// 'checkout_id' => $p_result->checkout_id,
					);
				$this->checkout_model->update_order($order_id,$update_data);

				##send the order details to customer
				$this->send_order_details($order_id); 
				$this->session->unset_userdata("cart_items");
			}else{
				//no checkout id
				//TODO: send email notification to SCR & Dev 
			}
			
			// $this->cart->destroy();

		}else{
				
		##update order record
			$message = $p_result->message;
			$error_code = $p_result->error_code;
			$processing_status = $p_result->processing_status;

			$update_data = array(					
					'payment_state_note' => $message. '. error code: '.$error_code. '. Payment processing status: '.$processing_status,
					'payment_state' => $processing_status,
					'state_changed_on' => date('Y-m-d H:i:s') 
					);
				$this->checkout_model->update_order($order_id,$update_data);

		##Notify dev & csr send error codes and messages
			$this->load->module('email_sender');
			
			$email_template = 'checkout-payment-error';
			$email_template_data = array(
				'message'=>$message, 
				'error_code'=>$error_code, 
				'processing_status'=> $processing_status,
				'order_id'=> $order_id
				);			
			// $email_body = $this->email_sender->load_email_template($email_template, $email_template_data);
			$email_body = $this->email_sender->load_email_template($email_template, $email_template_data);

			$this->load->module('settings');
			$store_setting = $this->settings->get('store');

			//set default support email
			$customer_support = (isset($store_setting['support_email']))? $store_setting['support_email']: 'support@tzilla.com';
			if(SITEMODE=='DEV') $customer_support = 'jovani.ogaya@tzilla.com';
			$email_data = array(
				'to'=> $customer_support,
				'from'=> 'systemgenerated@supercustomize.com',
				'from_name'=> 'System Generated SCP Email',
				'cc'=> 'mike.ibay@tzilla.com',
				// 'bcc'=> 'jovani.ogaya@tzilla.com',
				'subject'=> 'TZilla Checkout Payment Error',
				'message'=> $email_body,
			);

			$result = $this->send_smtp($email_data);

		}//end if success payment 



		$this->session->unset_userdata('discount_amount');
		$this->session->unset_userdata('discount_percent');
		$this->session->unset_userdata('discount_code');
		echo $result;

	}

	// TODO: discount
	public function set_discount_code( $discount_value = "" ) {
		$this->session->set_userdata( 'discount_code', $discount_value );
	}

	public function pay($payment_method='wepay',$payment_data=array())
	{		
		switch ($payment_method) {
			case 'wepay':
				$user_id = 0;

				$order_record = $this->db->get_where( 'scp_sales_orders', array( 'order_id' => $payment_data['order_id'] ) )->row();

				if(isset($this->session->customer_id)) $user_id = $this->session->customer_id;
				$order_id 			= $payment_data['order_id'];
				$credit_card_id 	= $payment_data['credit_card_id'];
				$require_shipping 	= $payment_data['require_shipping'];
				$shipping_fee 		= $payment_data['shipping_fee_amount'];
				$amount 			= $payment_data['grand_total_amount'];
				$currency 			= $payment_data['currency'];

				$callback_uri = site_url('checkout/wepay_ipn/'.$user_id.'/'.$order_id);			 	

				$this->load->module('settings');
				$settings		= $this->settings->get('store');

				$wepay_data = array(
								'auto_capture'			=> true,
							    'callback_uri'			=> $callback_uri,
							    'amount'              	=> $amount,
							    'currency'            	=> $currency,
							    'short_description'   	=> $settings['description'],
							    'type'                	=> 'goods',
							    'payment_method'      	=> array(
						        	'type'            	=> 'credit_card',
						        	'credit_card'     	=> array(
						        		'id' 	         	=> $credit_card_id
						        	)
							    )
							);

				
				$this->load->library('payment/wepay');

				$result = json_decode($this->wepay->pay_now($wepay_data));
				
				//defaults:
				$error_code = 0;
				$processing_status = 'success';

				if(isset($result->checkout_id)){

					$data['checkout_id'] = $result->checkout_id;

					$data['order_number'] = STOREID.'-'.$result->checkout_id;

					$this->Checkout_model->update_order( $order_id, $data );
					$wepayResult = $result;
					$this->send_payment_confirmation($user_id,$order_id,$wepayResult);
					$result = '{"status":"success","message": "Credit card accepted. Thank you!.", "checkout_id": "'.$result->checkout_id.'"}';

				}else{
					
					if (!is_object($result)) {
						$message = $result;
						$error_code = 0;
						$processing_status = 'failed';

					}else{
											
						$message = $result->response->error_description;
						$error_code = $result->response->error_code;
						$processing_status = $result->response->error;
					}

					$result = '{"status":"failed", "message": "'.$message.'", "error_code":"'.$error_code.'", "processing_status":"'.$processing_status.'"}';
				}

				break;

			case 'paypal':
            $order_record = $this->db->get_where( 'scp_sales_orders', array( 'order_id' => $payment_data['order_id'] ) )->row();
				//$result = $this->load->library('payment/paypal');

				break;
			default:
				# code...
				break;
		}
		
		return $result;
	}
	public function send_payment_confirmation($user_id,$order_id,$response)
	{
		## Company or store settings
		// $this->load->module('settings');
		$store_setting = $this->settings->get('store');
		$company_name = (isset($store_setting['name']))? $store_setting['name']:'';
		$support_email = (isset($store_setting['support_email']))? $store_setting['support_email']:'';
		$company_phone = (isset($store_setting['phone']))? $store_setting['phone']:'';

	## Order record
		$order_record = $this->db->get_where('scp_sales_orders', array('order_id' => $order_id));

		$customer_data = $this->Settings_model->get_customer_address( $user_id, $order_id );
		// print_r($customer_data);

		$order_row = $order_record->row();
	
		// print_r($order_row);
		if (isset($order_row))
		{
		    $orderno = $order_row->order_number;
		    $total =  $order_row->total;       
		    $order_status = $order_row->order_status;
		    $bill_email = $customer_data->email;
		}else{
			$orderno = '';
			$total = '';
			$order_status = '';
			$bill_email = '';
		}

	## Customer record		
		$customer_record = $this->db->get_where('scp_customer', array('customer_id' => $user_id));
		$customer_row = $customer_record->row();
		if (isset($customer_row))
		{
		    $customer_email= $customer_row->cust_email;		    			      
		}else{
			$customer_email = 'jovani.ogaya@tzilla.com'; //default for now
		}

		if($bill_email != '') $customer_email = $bill_email;
		
		
		$message = "The payment has been reserved from the payer.";

		$order_status = 'Processing';
	
		// initialize now settings to display milliseconds
		$this->db->select("order_id, order_number, erp_so, checkout_id, payment_state, email_sent");
		$this->db->where("order_id", $order_id);
		$updated_order = $this->db->get("scp_sales_orders")->row();

		// send sales order to erp
		$find_match = strrpos( $order_row->erp_so, "SO-" );

		// check if current order has already an SO#
		if ( $find_match === false ) {
			$order_row->erp_so = $this->send_so_to_erp( $orderno );
			// // // // // // // // // // // // // // // // // 

			$this->load->model('checkout/checkout_model');

			$update_data = array(
						'erp_so' => trim( $order_row->erp_so )
						);

			$this->checkout_model->update_order( $order_id, $update_data );

			// // // // // // // // // // // // // // // // // 
		}
		$dateprocess = date("F j, Y g:i a");
		
		$email_body = "";
		$email_body .= ucwords(strtolower($customer_row->cust_firstname)). " " .ucwords(strtolower($customer_row->cust_lastname)) ."<br/>";
		$email_body .= "<br/>";
		$email_body .= "<br/>";
		
		$email_body .= 'This is to confirm your payment in the total amount of $'.$total.' at <a href="http://www.tzilla.com">Tzilla.com</a> on '.$dateprocess.'<br/><br/> ';
		
	 	$email_body .= "Order No.: " . $orderno ."<br/>";
	 	$email_body .= "Payment Reference No.: " .$response->checkout_id ."<br/><br/>";
		$email_body .= "If you need further assistance, contact us at support@tzilla.com, or would like support with a personal touch, give us a ring at (888) 901-1636 .<br/><br/>";
		
		##Send payment confirmation to customer
		$this->load->module('email_sender');
	
		$email_template = 'checkout-payment-captured';
		$email_template_data = array(
			'total' => number_format($total,2), 
			'orderno' => $orderno, 
			'checkout_id' => $response->checkout_id,
			'company_name' => $company_name,
			'support_email' => $support_email,
			'company_phone' => $company_phone,
			'dateprocess' => date("F jS, Y")
			);			

		

		

		// if ( $updated_order->email_sent == 0 ) {
		if ( $find_match === false && $updated_order->email_sent == 0  ) {
			$update_data = array(
						'email_sent' => 1
						);

			$this->checkout_model->update_order( $order_id, $update_data );

			##Send payment confirmation to customer
			$email_data = array(
				'to' => $customer_email,
				'from' => $support_email,
				'from_name' => $company_name,
				'subject' => 'TZilla Payment Confirmation',
				'message' => $email_body,
			);

			$return = $this->send_smtp($email_data);
		}

			
	}

	public function send_so_to_erp( $order_no = 0 ) {
		$order_details = $this->Checkout_model->get_order_by_order_no( $order_no )->row(); // order no is == order id
		$customer_details = $this->Settings_model->get_customer_details( $order_details->customer_id );

		$this->load->module('Settings');
		$this->load->module('Inventory');

		$shipping_address = $this->Settings_model->get_customer_address( $order_details->customer_id, $order_details->order_id, "Shipping" );
		$billing_address = $this->Settings_model->get_customer_address( $order_details->customer_id, $order_details->order_id, "Billing" );

		$order_items = $this->Order_model->get_order_details($order_details->order_id);

		$items = array();
		$design_settings = array();

		foreach ( $order_items as $key => $order_item) {
			$matrix_value = explode("-", $order_item['matrix_code']);

			$brand = $this->Inventory_model->get_inventory_shirt_brand_by_code( $matrix_value[0] ); // inventory brand code
			$style = $this->Inventory_model->get_inventory_shirt_style_by_code( $matrix_value[1] ); // inventory style code
			$color = $this->Inventory_model->get_inventory_shirt_color_details_by_param( $style->shirt_style_id, $matrix_value[2] ); // inventory color code
			$size = $this->Inventory_model->get_inventory_shirt_size_details_by_param( $color->shirt_color_id, $matrix_value[3] ); // inventory size code

			$campaign_details = $this->Campaigns_model->get_campaign_by_product_id( $order_item['product_id'] );
			$generic_color = $this->Settings_model->get_color_by_code( $order_item['shirt_color_code'] );
			// $printer_pallet = $this->Settings_model->get_printer_pallet_by_param( $brand->shirt_brand_id, $style->shirt_style_id, $matrix_value[3] );
			$printer_profile = $this->Settings_model->get_printer_profile_by_param( $style->shirt_style_id, $color->shirt_color_code );
			$alternatives = $this->Settings_model->get_shirt_alternative_details( $style->shirt_style_id, $color->shirt_color_id, $size->shirt_size_code );

			// $item_code = $this->Inventory_model->get_item_code_by_param( $matrix_value[1], $color->shirt_color_id, $matrix_value[3] );
			
			$campaign = "";
			$campaign_goal = "100";
			$supplier_code = "";
			$shirt_alternatives = array();

			foreach ($alternatives as $plan_key => $plan_value) {
				array_push( $shirt_alternatives, $plan_value->brand_code."-".$plan_value->style_code."-".$plan_value->shirt_color_code."-".$plan_value->shirt_size_code."_".$plan_value->item_code );
			}
				
			if ( count( $campaign_details ) > 0 ) {
				foreach ($campaign_details as $campaign_key => $campaign_value) {
					$campaign = $campaign_value->campaign_title;
					$campaign_goal = $campaign_value->campaign_goal;
					$supplier_code = $campaign_value->supplier_code;
				}
			}

			$design_settings = array( unserialize( $order_item['images'] ) );

			$items[$key] = array(
							"scp_item_id" => $order_item['order_items_id'],
							"quantity" => $order_item['quantity'],
							"line_no" => $key+1,
							"matrix_code" => $order_item['matrix_code']."_".$size->item_code,
							"shirt_alternatives" => $shirt_alternatives,
							"generic_shirt_color_code" => $color->shirt_color_code,
							"generic_color_name" => $color->color_name,
							"shirt_group_plan" => $style->shirt_group_plan,
							"design_name" => "Default",
							"garment_type" =>  strlen($style->garment_type) > 0 ? $style->garment_type : "Cotton",
							"printer_setup" => $printer_profile->kornit_printer_id,
							"print_type" => "KORNIT",
							"unit_of_measure" => "EACH",
							"asPrice" => $order_item['price'],
							"asQty" => $order_item['quantity'],
							"matrixItem" => $size->item_code,
							"asDesignType" => "sample string",
							"asItemName" => $design_settings[0]['design_id']."_".$size->item_code,
							"asItemDescription" => $design_settings[0]['design_id']."_".$size->item_code,
							"templateCode" => "Shirt",
							"asDesignUrl" => $design_settings[0]['temp_image'],
							"itemClassification" => "",
							"classificationFilter" => "",
							"startDate" => date('m/d/Y'),
							"endDate" => date('m/d/Y'),
							"salesGoal" => $campaign_goal,
							"title" => $campaign,
							"colors" => "",
							"school" => "",
					        "notes" => "",
					        "supplier_code" => $supplier_code,
							"designs" => $design_settings
						);
		}

		$data['processOrderModel'] = array(
										"customerCode" => $customer_details->customer_code,
										"userName" => $customer_details->cust_email,
										"passWord" => "admin",
										"freight" => 4.0,
										"total" => $order_details->total,
										"shippingNotes" => "none",
										"shippingMethod" => $order_details->shipping_method,
										"paymentNotes" => "Wepay Payment",
										"notes" => "none",
										"shipToName" => $shipping_address->firstname." ".$shipping_address->lastname,
										"shipToEmail" => $shipping_address->email,
										"shipToPhone" => $shipping_address->contact_no,
										"shipToAddress" => $shipping_address->address_1,
										"shipToCity" => $shipping_address->city,
										"shipToZip" => $shipping_address->zip,
										"shipToState" => $billing_address->state,
										"shipToCounty" => "Los Angeles",
										"shipToCountry" => "United States of America",
										"websiteCode" => "WEB-000001",
										"alternateOrder" => $order_details->order_id,
										"wepay_checkout_id" => $order_details->checkout_id
									);


		$discount_amount = 0;


		$data['salesOrderModel'] = array( "items" => $items );

		$jsonData = stripcslashes( json_encode( $data ) );

		$find_match = strrpos( $order_details->erp_so, "SO-" );
		
		if ( $find_match === false ) {
			$url = "http://12.9.181.75/erpwebapi/TzillaApi/ProcessSalesOrder";
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/json"));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); 

			$content = curl_exec($ch);

			curl_close($ch);        

			$aResponse=json_decode($content,true);

			
			$now = DateTime::createFromFormat('U.u', microtime(true));

			$this->db->select("order_id, order_number, erp_so, checkout_id, payment_state");
			$this->db->where("order_id", $order_details->order_id);
			$updated_order = $this->db->get("scp_sales_orders")->row();

			$email_data = array(
				'to' => 'mike.ibay@tzilla.com',
				'from' => 'support@tzilla.com',
				'from_name' => 'Tzilla',
				'subject' => 'TZilla Sales Order Update for '.$updated_order->order_number,
				'message' => $updated_order->order_number.' update SO ( '.$updated_order->erp_so.'@'.$now->format("H:i:s.u").' ) to this value '.$aResponse["erP_SO"].' from checkout id value '.$updated_order->checkout_id,
			);

			if ( strrpos( $updated_order->erp_so, "SO-" ) === true ) {
				$return = $this->send_smtp($email_data);
			}

			return $aResponse["erP_SO"];
		} else {
			return $order_details->erp_so;
		}
	}

	public function send_so_to_erp_test( $order_no = "2-237230815" ) {
		$order_details = $this->Checkout_model->get_order_by_order_no( $order_no )->row(); // order no is == order id
		$customer_details = $this->Settings_model->get_customer_details( $order_details->customer_id );

		$this->load->module('Settings');
		$this->load->module('Inventory');

		$shipping_address = $this->Settings_model->get_customer_address( $order_details->customer_id, $order_details->order_id, "Shipping" );
		$billing_address = $this->Settings_model->get_customer_address( $order_details->customer_id, $order_details->order_id, "Billing" );

		$order_items = $this->Order_model->get_order_details($order_details->order_id);

		$items = array();
		$design_settings = array();

		foreach ( $order_items as $key => $order_item) {
			$matrix_value = explode("-", $order_item['matrix_code']);

			$brand = $this->Inventory_model->get_inventory_shirt_brand_by_code( $matrix_value[0] ); // inventory brand code
			$style = $this->Inventory_model->get_inventory_shirt_style_by_code( $matrix_value[1] ); // inventory style code
			$color = $this->Inventory_model->get_inventory_shirt_color_details_by_param( $style->shirt_style_id, $matrix_value[2] ); // inventory color code
			$size = $this->Inventory_model->get_inventory_shirt_size_details_by_param( $color->shirt_color_id, $matrix_value[3] ); // inventory size code

			$campaign_details = $this->Campaigns_model->get_campaign_by_product_id( $order_item['product_id'] );
			$generic_color = $this->Settings_model->get_color_by_code( $order_item['shirt_color_code'] );
			// $printer_pallet = $this->Settings_model->get_printer_pallet_by_param( $brand->shirt_brand_id, $style->shirt_style_id, $matrix_value[3] );
			$printer_profile = $this->Settings_model->get_printer_profile_by_param( $style->shirt_style_id, $color->shirt_color_code );
			$alternatives = $this->Settings_model->get_shirt_alternative_details( $style->shirt_style_id, $color->shirt_color_id, $size->shirt_size_code );
		
			// $item_code = $this->Inventory_model->get_item_code_by_param( $matrix_value[1], $color->shirt_color_id, $matrix_value[3] );

			$campaign = "";
			$campaign_goal = "100";
			$supplier_code = "";
			$shirt_alternatives = array();

			foreach ($alternatives as $plan_key => $plan_value) {
				array_push( $shirt_alternatives, $plan_value->brand_code."-".$plan_value->style_code."-".$plan_value->shirt_color_code."-".$plan_value->shirt_size_code."_".$plan_value->item_code );
			}
				
			if ( count( $campaign_details ) > 0 ) {
				foreach ($campaign_details as $campaign_key => $campaign_value) {
					$campaign = $campaign_value->campaign_title;
					$campaign_goal = $campaign_value->campaign_goal;
					$supplier_code = $campaign_value->supplier_code;
				}
			}

			$design_settings = array( unserialize( $order_item['images'] ) );

			$items[$key] = array(
							"scp_item_id" => $order_item['order_items_id'],
							"quantity" => $order_item['quantity'],
							"line_no" => $key+1,
							"matrix_code" => $order_item['matrix_code']."_".$size->item_code,
							"shirt_alternatives" => $shirt_alternatives,
							"generic_shirt_color_code" => $color->shirt_color_code,
							"generic_color_name" => $color->color_name,
							"shirt_group_plan" => $style->shirt_group_plan,
							"design_name" => "Default",
							"garment_type" =>  strlen($style->garment_type) > 0 ? $style->garment_type : "Cotton",
							"printer_setup" => $printer_profile->kornit_printer_id,
							"print_type" => "KORNIT",
							"unit_of_measure" => "EACH",
							"asPrice" => $order_item['price'],
							"asQty" => $order_item['quantity'],
							"matrixItem" => $size->item_code,
							"asDesignType" => "sample string",
							"asItemName" => $design_settings[0]['design_id']."_".$size->item_code,
							"asItemDescription" => $design_settings[0]['design_id']."_".$size->item_code,
							"templateCode" => "Shirt",
							"asDesignUrl" => $design_settings[0]['temp_image'],
							"itemClassification" => "",
							"classificationFilter" => "",
							"startDate" => date('m/d/Y'),
							"endDate" => date('m/d/Y'),
							"salesGoal" => $campaign_goal,
							"title" => $campaign,
							"colors" => "",
							"school" => "",
					        "notes" => "",
					        "supplier_code" => $supplier_code,
							"designs" => $design_settings
						);
		}

		$data['processOrderModel'] = array(
										"customerCode" => $customer_details->customer_code,
										"userName" => $customer_details->cust_email,
										"passWord" => "admin",
										"freight" => 4.0,
										"total" => $order_details->total,
										"shippingNotes" => "none",
										"shippingMethod" => $order_details->shipping_method,
										"paymentNotes" => "Wepay Payment",
										"notes" => "none",
										"shipToName" => $shipping_address->firstname." ".$shipping_address->lastname,
										"shipToEmail" => $shipping_address->email,
										"shipToPhone" => $shipping_address->contact_no,
										"shipToAddress" => $shipping_address->address_1,
										"shipToCity" => $shipping_address->city,
										"shipToZip" => $shipping_address->zip,
										"shipToState" => $billing_address->state,
										"shipToCounty" => "Los Angeles",
										"shipToCountry" => "United States of America",
										"websiteCode" => "WEB-000001",
										"alternateOrder" => $order_details->order_id,
										"wepay_checkout_id" => $order_details->checkout_id
									);


		$discount_amount = 0;


		$data['salesOrderModel'] = array( "items" => $items );

		echo $jsonData = stripcslashes( json_encode( $data ) );

		$find_match = strrpos( $order_details->erp_so, "SO-" );
		
		if ( $find_match === false ) {
			$url = "http://12.9.181.75/erpwebapi/TzillaApi/ProcessSalesOrder";
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, FALSE);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  application/json"));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData); 

			$content = curl_exec($ch);

			curl_close($ch);        

			$aResponse=json_decode($content,true);

			
			$now = DateTime::createFromFormat('U.u', microtime(true));

			$this->db->select("order_id, order_number, erp_so, checkout_id, payment_state");
			$this->db->where("order_id", $order_details->order_id);
			$updated_order = $this->db->get("scp_sales_orders")->row();

			$email_data = array(
				'to' => 'mike.ibay@tzilla.com',
				'from' => 'support@tzilla.com',
				'from_name' => 'Tzilla',
				'subject' => 'TZilla Sales Order Update for '.$updated_order->order_number,
				'message' => $updated_order->order_number.' update SO ( '.$updated_order->erp_so.'@'.$now->format("H:i:s.u").' ) to this value '.$aResponse["erP_SO"].' from checkout id value '.$updated_order->checkout_id,
			);

			if ( strrpos( $updated_order->erp_so, "SO-" ) === true ) {
				$return = $this->send_smtp($email_data);
			}

			return $aResponse["erP_SO"];
		} else {
			return $order_details->erp_so;
		}
	}

	#todo
	function wepay_ipn($user_id=0,$order_id=0)
	{
		
	## Retrieve settings
		$this->load->module('settings');
		$settings		= $this->settings->get('wepay');

		$api_mode 		= $settings['api_mode'];

		if($api_mode == 1){
			$client_id 		= $settings['client_id_live'];
	 		$client_secret 	= $settings['client_secret_live'];
	 		$access_token 	= $settings['access_token_live'];	 		
	 		$account_id 	= $settings['account_id_live'];
		}else{
			$client_id 		= $settings['client_id_dev'];
	 		$client_secret 	= $settings['client_secret_dev'];
	 		$access_token 	= $settings['access_token_dev'];	 		
	 		$account_id 	= $settings['account_id_dev'];
		}

		$params = array('token'=>$access_token);

		$this->load->add_package_path(APPPATH.'third_party/Payment/wepay/');

		$this->load->library('wepay_api',$access_token);
			
		//$this->load->library('wepay_sdk',$params);
		
		// change to useProduction for live environments
		if( SITEMODE == "DEV" )
		{ 
			$this->wepay_api->useStaging($client_id, $client_secret);
		}else{
			$this->wepay_api->useProduction($client_id, $client_secret);
		}

		$sales_order_detail = $this->Checkout_model->get_order($order_id)->row();

		if ( $sales_order_detail->checkout_id > 0 ) {
			$wepay_checkout_id = $sales_order_detail->checkout_id;
		} else {
			$wepay_checkout_id = 0;
		}

		$wepay_data = array(
						'checkout_id' => $wepay_checkout_id
						);
		$response = $this->wepay_api->request('checkout', $wepay_data); 

		
		/**
			WePay Payment States:
			new - The checkout was created by the application. 
			authorized - The payer entered their payment info and confirmed the payment on WePay. WePay has successfully charged the card. WePay's risk analysis may still be in progress. 
			captured - The payment has been reserved from the payer. 
			released - The payment has been credited to the payee account. 
			cancelled - The payment has been cancelled by the payer, payee, or application. 
			refunded - The payment was captured and then refunded by the payer, payee, or application. The payment has been debited from the payee account. 
			charged back - The payment has been charged back by the payer and the payment has been debited from the payee account. 
			failed - The payment has failed. 
			expired - Checkouts get expired if they are still in state new after 30 minutes (ie they have been abandoned).
		**/
		switch ($response->state) {
			case 'new':
				$message = 'New checkout payment.';
				break;
			case 'authorized':
				$message = "The payer entered their payment info and confirmed the payment on WePay. WePay has successfully charged the card. WePay's risk analysis may still be in progress. ";
				break;
			case 'captured':
				$message = "The payment has been reserved from the payer.";

				$order_status = 'Processing';
				//update order record
				$r = $this->db->get_where('scp_sales_orders',array('order_id'=>$order_id));

				if ( $r->num_rows() > 0 ) {
					$this->load->model('checkout/checkout_model');

					$update_data = array(
								'order_status' => $order_status,
								'payment_info' => $message,
								'payment_state' => $response->state,
								'wepay_changed_on' => date('Y-m-d H:i:s') 
								);

					$this->checkout_model->update_order( $order_id, $update_data );
				}
				break;
			case 'released':
				$message = "The payment has been credited to the payee account.";
				break;
			case 'cancelled':
				$message = "The payment has been cancelled by the payer, payee, or application.";
				break;
			case 'refunded':
				$message = "The payment was captured and then refunded by the payer, payee, or application. The payment has been debited from the payee account.";
				break;
			case 'charged back':
				$message = "The payment has been charged back by the payer and the payment has been debited from the payee account.";
				break;
			case 'failed':
				$message = "The payment has failed.";
				break;
			case 'expired':
				$message = "Checkouts get expired if they are still in state new after 30 minutes (ie they have been abandoned).";
				break;
			default:
				$message = "";

				break;
		}


	}
    function paypal_ipn(){
        // STEP 1: read POST data
// Reading POSTed data directly from $_POST causes serialization issues with array data in the POST.
// Instead, read raw POST data from the input stream.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
  $keyval = explode ('=', $keyval);
  if (count($keyval) == 2)
    $myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
$req = 'cmd=_notify-validate';
if (function_exists('get_magic_quotes_gpc')) {
  $get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
  if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
    $value = urlencode(stripslashes($value));
  } else {
    $value = urlencode($value);
  }
  $req .= "&$key=$value";
}

$this->db->insert("scp_paypal",array("req"=>$req));
 $paypal_txn_id=$this->db->insert_id();
// Step 2: POST IPN data back to PayPal to validate
//$req="cmd=_notify-validate&payer_email=buyer%40paypalsandbox.com&receiver_email=seller%40paypalsandbox.com&txn_id=912416559&test_ipn=1&payment_type=echeck";
$ch = curl_init('https://ipnpb.sandbox.paypal.com/cgi-bin/webscr');
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));

// In wamp-like environments that do not come bundled with root authority certificates,
// please download 'cacert.pem' from "https://curl.haxx.se/docs/caextract.html" and set
// the directory path of the certificate as shown below:
// curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');
if ( !($res = curl_exec($ch)) ) {
  // error_log("Got " . curl_error($ch) . " when processing IPN data");
  curl_close($ch);
  exit;
}
curl_close($ch);
  //$this->db->insert("scp_paypal",array("req"=>$res));
// inspect IPN validation result and act accordingly
if (strcmp ($res, "VERIFIED") == 0) {
  // The IPN is verified, process it:
  // check whether the payment_status is Completed
  // check that txn_id has not been previously processed
  // check that receiver_email is your Primary PayPal email
  // check that payment_amount/payment_currency are correct
  // process the notification
  // assign posted variables to local variables
  $item_name = $_POST['item_name'];
  $item_number = $_POST['item_number'];
  $payment_status = $_POST['payment_status'];
  $payment_amount = $_POST['mc_gross'];
  $payment_currency = $_POST['mc_currency'];
  $txn_id = $_POST['txn_id'];
  $receiver_email = $_POST['receiver_email'];
  $payer_email = $_POST['payer_email'];
  // IPN message values depend upon the type of notification sent.
  // To loop through the &_POST array and print the NV pairs to the screen:
  foreach($_POST as $key => $value) {
    echo $key . " = " . $value . "<br>";
  }
  
 
  //mysql_query("insert into `net_paypal`(test) VALUES ('{$payer_email}')");
  $this->db->update("scp_paypal",array("item_name"=>$item_name,
                                       "item_number"=>$item_number,
                                       "payment_status"=>$payment_status,
                                       "payment_amount"=>$payment_amount,
                                       "payment_currency"=>$payment_currency,
                                       "txn_id"=>$txn_id,
                                       "receiver_email"=>$receiver_email,
                                       "payer_email"=>$payer_email)
                                ,array("id"=>$paypal_txn_id));
  if($payment_status=='Completed'){
    
  }
  
} else if (strcmp ($res, "INVALID") == 0) {
  // IPN invalid, log for manual investigation
  echo "The response from IPN was: <b>" .$res ."</b>";
}
    }
	function wepay_ipn_test1($user_id=0,$order_id=0)
	{
		$this->load->module('Settings');
		
	## Retrieve settings
		$this->load->module('settings');
		$settings		= $this->settings->get('wepay');

		$api_mode 		= $settings['api_mode'];

		if($api_mode == 1){
			$client_id 		= $settings['client_id_live'];
	 		$client_secret 	= $settings['client_secret_live'];
	 		$access_token 	= $settings['access_token_live'];	 		
	 		$account_id 	= $settings['account_id_live'];
		}else{
			$client_id 		= $settings['client_id_dev'];
	 		$client_secret 	= $settings['client_secret_dev'];
	 		$access_token 	= $settings['access_token_dev'];	 		
	 		$account_id 	= $settings['account_id_dev'];
		}

		$params = array('token'=>$access_token);

		$this->load->add_package_path(APPPATH.'third_party/Payment/wepay/');

		$this->load->library('wepay_api',$access_token);
			
		//$this->load->library('wepay_sdk',$params);
		
		// change to useProduction for live environments
		if( SITEMODE == "DEV" )
		{ 
			$this->wepay_api->useStaging($client_id, $client_secret);
		}else{
			$this->wepay_api->useProduction($client_id, $client_secret);
		}

		$sales_order_detail = $this->Checkout_model->get_order($order_id)->row();

		if ( $sales_order_detail->checkout_id > 0 ) {
			$wepay_checkout_id = $sales_order_detail->checkout_id;
		} else {
			$wepay_checkout_id = 0;
		}

		$wepay_data = array(
						'checkout_id' => $wepay_checkout_id
						);
		$response = $this->wepay_api->request('checkout', $wepay_data); 

	## Company or store settings
		// $this->load->module('settings');
		$store_setting = $this->settings->get('store');
		$company_name = (isset($store_setting['name']))? $store_setting['name']:'';
		$support_email = (isset($store_setting['support_email']))? $store_setting['support_email']:'';
		$company_phone = (isset($store_setting['phone']))? $store_setting['phone']:'';

	## Order record
		$order_record = $this->db->get_where('scp_sales_orders', array('order_id' => $order_id));

		$customer_data = $this->Settings_model->get_customer_address( $user_id, $order_id );
		// print_r($customer_data);

		$order_row = $order_record->row();
		// print_r($order_row);
		if (isset($order_row))
		{
		    $orderno = $order_row->order_number;
		    $total =  $order_row->total;       
		    $order_status = $order_row->order_status;
		    $bill_email = $customer_data->email;
		}else{
			$orderno = '';
			$total = '';
			$order_status = '';
			$bill_email = '';
		}

	## Customer record		
		$customer_record = $this->db->get_where('scp_customer', array('customer_id' => $user_id));
		$customer_row = $customer_record->row();
		if (isset($customer_row))
		{
		    $customer_email= $customer_row->cust_email;		    			      
		}else{
			$customer_email = 'jovani.ogaya@tzilla.com'; //default for now
		}

		if($bill_email != '') $customer_email = $bill_email;
		
		// display the response 
		// if($this->debug) print_r($response);

		/**
			WePay Payment States:
			new - The checkout was created by the application. 
			authorized - The payer entered their payment info and confirmed the payment on WePay. WePay has successfully charged the card. WePay's risk analysis may still be in progress. 
			captured - The payment has been reserved from the payer. 
			released - The payment has been credited to the payee account. 
			cancelled - The payment has been cancelled by the payer, payee, or application. 
			refunded - The payment was captured and then refunded by the payer, payee, or application. The payment has been debited from the payee account. 
			charged back - The payment has been charged back by the payer and the payment has been debited from the payee account. 
			failed - The payment has failed. 
			expired - Checkouts get expired if they are still in state new after 30 minutes (ie they have been abandoned).
		**/
		switch ($response->state) {
			case 'new':
				$message = 'New checkout payment.';
				break;
			case 'authorized':
				$message = "The payer entered their payment info and confirmed the payment on WePay. WePay has successfully charged the card. WePay's risk analysis may still be in progress. ";
				break;
			case 'captured':
			
				$message = "The payment has been reserved from the payer.";

				$order_status = 'Processing';

				// initialize now settings to display milliseconds
				$this->db->select("order_id, order_number, erp_so, checkout_id, payment_state, email_sent");
				$this->db->where("order_id", $order_id);
				$updated_order = $this->db->get("scp_sales_orders")->row();

				// send sales order to erp
				$find_match = strrpos( $order_row->erp_so, "SO-" );
				
				// check if current order has already an SO#
				if ( $find_match === false && $updated_order->email_sent == 0 ) {
					$order_row->erp_so = $this->send_so_to_erp( $orderno );
				}

				##Send payment confirmation to customer
				$this->load->module('email_sender');
			
				$email_template = 'checkout-payment-captured';
				$email_template_data = array(
					'total' => number_format($total,2), 
					'orderno' => $orderno, 
					'checkout_id' => $sales_order_detail->checkout_id,
					'company_name' => $company_name,
					'support_email' => $support_email,
					'company_phone' => $company_phone,
					'dateprocess' => date("F jS, Y")
					);			

				$dateprocess = date("F j, Y g:i a");
				
				$email_body = "";
				$email_body .= ucwords(strtolower($customer_row->cust_firstname)). " " .ucwords(strtolower($customer_row->cust_lastname)) ."<br/>";
				$email_body .= "<br/>";
				$email_body .= "<br/>";
				
				$email_body .= 'This is to confirm your payment in the total amount of $'.$total.' at <a href="http://www.tzilla.com">Tzilla.com</a> on '.$dateprocess.'<br/><br/> ';
				
			 	$email_body .= "Order No.: " .$orderno ."<br/>";
			 	$email_body .= "Payment Reference No.: " .$sales_order_detail->checkout_id ."<br/><br/>";
				$email_body .= "If you need further assistance, contact us at support@tzilla.com, or would like support with a personal touch, give us a ring at (888) 901-1636 .<br/><br/>";

				//update order record
				$r = $this->db->get_where('scp_sales_orders',array('order_id'=>$order_id));

				if ( $r->num_rows() > 0 ) {
					$this->load->model('checkout/checkout_model');

					$update_data = array(
								'order_status' => $order_status,
								'payment_info' => $message,
								'payment_state' => $response->state,
								'wepay_changed_on' => date('Y-m-d H:i:s') 
								);

					$this->checkout_model->update_order( $order_id, $update_data );
				}

				// if ( $updated_order->email_sent == 0 ) {
				if ( $updated_order->email_sent == 0  ) {
					$update_data = array(
								'email_sent' => 1
								);
				
					$this->checkout_model->update_order( $order_id, $update_data );
				
				
					##Send payment confirmation to c ustomer
					$email_data = array(
						'to' => $customer_email,
						'from' => $support_email,
						'from_name' => $company_name,
						'subject' => 'TZilla Payment Confirmation',
						'message' => $email_body,
					);

					$return = $this->send_smtp($email_data);
					
				}

				break;
			case 'released':
				$message = "The payment has been credited to the payee account.";
				break;
			case 'cancelled':
				$message = "The payment has been cancelled by the payer, payee, or application.";
				break;
			case 'refunded':
				$message = "The payment was captured and then refunded by the payer, payee, or application. The payment has been debited from the payee account.";
				break;
			case 'charged back':
				$message = "The payment has been charged back by the payer and the payment has been debited from the payee account.";
				break;
			case 'failed':
				$message = "The payment has failed.";
				break;
			case 'expired':
				$message = "Checkouts get expired if they are still in state new after 30 minutes (ie they have been abandoned).";
				break;
			default:
				$message = "";

				break;
		}

		/*if($response->state=='authorized') $important_message = '<br/>Payment is Authorized. Wepay is now capturing the payment.';
			
		if ($response->state=='captured') {
			$important_message = '<br/><b>Payment is Captured!. Order can now be Processed!</b>';
		}*/

	}

	/*//prevent direct access via url
	public function _remap()
	{
	 	//prevent access to url
	}*/

	function send_order_details($order_id=0) // scpv1-1
	{
		if($order_id)
		{
			$this->load->model('checkout/checkout_model');
			$order_record = $this->checkout_model->get_order($order_id);
			if($order_record->num_rows())
			{
				$this->load->module('settings');
				$store_setting = $this->settings->get('store');

				//set default support email
				$customer_support = (isset($store_setting['support_email']))? $store_setting['support_email']: 'support@tzilla.com';

				$this->load->module('email_sender');
				// $email_template = 'email-sales-order-details';
				$email_template = 'checkout-order-details2';
				// print_r($order_record->row_array());
				// $email_body = 'fdsf';
				// $email_body = $this->email_sender->load_email_template($order_id, $email_template, $order_record->row_array());
				$email_body = $this->_load_email_template($order_id, $order_record->row_array());
				// echo "<pre>";
				// print_r($order_record->row_array());
				// echo $email_body;
				
				//send order details to customer
				$customer_email = (isset($this->session->bill_email))? $this->session->bill_email: 'support@tzilla.com';
				$email_data = array(
					'to'=> $customer_email,
					'from'=> $customer_support,
					'from_name'=>  'TZilla',
					// 'bcc'=> 'jovani.ogaya@tzilla.com',
					'subject'=> 'TZilla Order Details',
					'message'=> $email_body,
				);
				 return $this->send_smtp($email_data);
				
			}
		}else{
			return json_encode(array('status' => 'failed', 'message' => 'order id is required'));
		}
	}

	public function _load_email_template($order_id, $template_data)
	{	

		$this->load->module('Settings');
		
	## Retrieve settings
		$this->load->module('settings');
		$data['link'] = 'orders';
		$data['get_order_details'] = $this->Order_model->get_sales_order( $order_id, $template_data['customer_id'] );
		$data['get_order_items'] = $this->Order_model->get_order_details($order_id );
		$address_type = $this->Order_model->get_address_by_order_id( $order_id );
	
		foreach ($address_type as $key => $val) {
			$type =  $val->address_type;
			if( $type == 'Shipping' ){
				$data['shipping'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
			}
			if( $type == 'Billing' ){
				$data['billing'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
			}
			if( $type == 'Both' ){
				$data['shipping'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
				$data['billing'] = $this->Order_model->get_orders_by_order_type( $order_id,  $type );
			}
		}		

			$data['customer'] = $this->Checkout_model->get_customer($template_data['customer_id']);
			

			foreach ($data['get_order_details'] as $key => $details) { 
				$data['order_date'] = $details['order_date'];
				$data['order_status'] = $details['order_status'];
				$data['order_number'] = $details['order_number'];			

			}

			$data['shipping_method'] = $template_data['shipping_method'];
			$data['subtotal'] = $template_data['subtotal'];
			$data['tax'] = $template_data['tax'];
			$data['total'] = $template_data['total'];
			$data['discount'] = $template_data['discount'];
			$data['shipping_fee'] = $template_data['shipping_fee'];

		$email_body = $this->load->view_store('email-templates/checkout-order-details2', $data, TRUE);
		return $email_body;

		// echo $email_body;
	}


	

	public function send_smtp($data=null)
	{
		if($data != null){
			
			$this->load->module('settings');
			$setting = $this->settings->get('smtp');
				

			//print_r($setting);
			# smtp settings
						
			#send email for ipn
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = $setting['smtp_host'];
			$config['smtp_user'] = $setting['smtp_user'];
			$config['smtp_pass'] = $setting['smtp_pass'];
			$config['smtp_port'] = $setting['smtp_port'];
			$config['smtp_timeout'] = $setting['smtp_timeout'];
			$config['mailtype'] = 'html';
			$config['priority'] = '1';
			//$config['mailpath'] = '/usr/sbin/sendmail';
			$config['charset'] = 'iso-8859-1';
			$config['wordwrap'] = TRUE;
			$config['crlf'] =  "\r\n";
			$config['newline'] = "\r\n";
		
			$this->email->initialize($config);
			$this->email->from($data['from'], $data['from_name']);
			$this->email->to($data['to']); 
			
			if(isset($data['cc'])) $this->email->cc($data['cc']); 

			// if(isset($data['bcc']))	$this->email->bcc($data['bcc']); 
			if(isset($data['bcc']))	$this->email->bcc('cheezel.tin@tzilla.com', 'jovani.ogaya@tzilla.com', 'cherry.valdoz@tzilla.com'); 

			$this->email->subject($data['subject']);
			
			$this->email->message($data['message']);	

			$r = $this->email->send(); //
			// echo "Send email";
			if($r){
				$r = 'success';
				$message = 'Email sent.';
			}else{
				$r = 'failed';
				$message = json_encode($this->email->print_debugger());
			}
			return $r;
			// return "{status:'".$r."',message:'".$message."'}";
		}else{
			return $r;
			// return "{status:'failed',message:'Email data is null'}";
		}
			
	}

	public function load_shirt_plan_detail( $styleCode, $fieldName = 'group_name' ){
		$this->load->model("settings/Settings_model");

		$value = $this->Settings_model->get_shirt_plan_field_by_style_code( $fieldName, $styleCode );

		return $value->$fieldName;
	}

	public function thankyou(){ /* Function for viewing order confirmation/thank you scpv1-1 */	
		// $data['order_id'] = $this->session->order_id;

		// $data['order_confirm'] = $this->Order_model->get_orders_by_order_id( $data['order_id'] );
		// $data['shirt_holder'] = $this->Order_model->get_shirt_by_order_id( $data['order_id'] );
		// $items = $this->Order_model->get_order_details_item( $data['order_id'], $this->session->customer_id );
		// $address_type = $this->Order_model->get_address_by_order_id( $data['order_id'] );

		// foreach ($address_type as $key => $val) {
		// 	$type =  $val->address_type;
		// 	if( $type == 'Shipping' ){
		// 		$data['shipping'] = $this->Order_model->get_orders_by_order_type( $data['order_id'],  $type );
		// 	}
		// 	if( $type == 'Billing' ){
		// 		$data['billing'] = $this->Order_model->get_orders_by_order_type( $data['order_id'],  $type );
		// 	}
		// 	if( $type == 'Both' ){
		// 		$data['shipping'] = $this->Order_model->get_orders_by_order_type( $data['order_id'],  $type );
		// 		$data['billing'] = $this->Order_model->get_orders_by_order_type( $data['order_id'],  $type );
		// 	}
		// }

		// foreach ($items as $key => $value) {
		// 	$image = unserialize( $value['images'] );
		// 	$data['shirt_color_code'] = $this->Order_model->get_order_details_item_color( $value['shirt_color_code'] );
			
		// }
		// echo "<pre>";
		// print_r( $data['shirt'] );	
		// echo "</pre>";	
		// foreach ($image as $key => $value2) {
		// 	$data['order_item_image'] = $value2['temp_image'];	
		// $data['order_item_image'] = $image['temp_image'];
		// }

		// $this->load->view_store('order-confirmation', $data); 
		$this->load->view_store('thankyou'); 
		/*unset all sessions */

		/*	$this->session->unset_userdata('bill_email');
        	$this->session->unset_userdata('customerdata');
        	$this->session->unset_userdata('customer_data');
        	$this->session->unset_userdata('product_details');
        	$this->session->unset_userdata('cart_items_id');*/
        	// $this->session->unset_userdata('customer_data');

        /*unset all sesions */
	}
	public function image_generator()
	{
		if( $post = $this->input->post(null, true) )
		{
			$post['thumbnail'] = str_replace(" ", "%20", $post['thumbnail']);
			$post['thumbnail'] = $this->encodeArtImage($post['thumbnail']);
			$stream = explode("data:image/png;base64,",$post['thumbnail'] );
			$artworksName = $this->Checkout_model->check_generated_image($post['design_id']);
			$stream[0] = str_replace("[removed]","",$stream[0] );
			
			$artworksName[0]->thumbnail = str_replace("/200/200", "", $artworksName[0]->thumbnail);
			
			if($artworksName[0]->image_generated == 1 )
			{
				//remove image
				unlink('public/images/buy/'.$artworksName[0]->thumbnail.'.png');				
			}
			else
			{
				$thumb_clean= explode("/images/", $artworksName[0]->thumbnail );
				$artworksName[0]->thumbnail = $thumb_clean[1];
			}

			file_put_contents('public/images/buy/'.$artworksName[0]->thumbnail.'.png',base64_decode($stream[1]));
			//add the image
			$artworks = $this->Checkout_model->update_artwork_generated_image($post['design_id'],$artworksName[0]->thumbnail);
			echo "done";
			echo "<img src='".$post['thumbnail']."'  />";
		}

		
	}
	public function ExactBrowserName(){
		$ExactBrowserNameUA=$_SERVER['HTTP_USER_AGENT'];

		if (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")) {
		    // OPERA
		    $ExactBrowserNameBR="Opera";
		} elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "chrome/")) {
		    // CHROME
		    $ExactBrowserNameBR="Chrome";
		} elseIf (strpos(strtolower($ExactBrowserNameUA), "msie")) {
		    // INTERNET EXPLORER
		    $ExactBrowserNameBR="Internet Explorer";
		} elseIf (strpos(strtolower($ExactBrowserNameUA), "firefox/")) {
		    // FIREFOX
		    $ExactBrowserNameBR="Firefox";
		} elseIf (strpos(strtolower($ExactBrowserNameUA), "safari/") and strpos(strtolower($ExactBrowserNameUA), "opr/")==false and strpos(strtolower($ExactBrowserNameUA), "chrome/")==false) {
		    // SAFARI
		    $ExactBrowserNameBR="Safari";
		} else {
		    // OUT OF DATA
		    $ExactBrowserNameBR="OUT OF DATA";
		};

		return $ExactBrowserNameBR;
	}

}