

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MX_Controller   {
	
	function __construct()
    {
        parent::__construct();
        $this->load->library( array('Aauth', 'session') );
    }
	
	public function index()
	{ 
        // login validation
        // print_r( $this->aauth->get_user() );

		$this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

		$this->form_validation->set_rules('customer_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('customer_ship_firstname', 'First name', 'trim|required');       
        $this->form_validation->set_rules('customer_ship_lastname', 'Last name', 'trim|required');               
        $this->form_validation->set_rules('customer_ship_address', 'Address', 'trim|required');               
        $this->form_validation->set_rules('customer_ship_city', 'City', 'trim|required');               
        $this->form_validation->set_rules('customer_ship_country', 'Country', 'trim|required');               
        $this->form_validation->set_rules('customer_ship_state', 'State', 'trim|required');               
        $this->form_validation->set_rules('customer_ship_zipcode', 'Zip code', 'trim|required');               
        $this->form_validation->set_rules('customer_ship_phone', 'Phone', 'trim|required');               
       
        $this->form_validation->set_error_delimiters('<label class="bg-danger">', '</label>');	

        if ($this->form_validation->run() == FALSE)
        {
        	$data['content'] = 'checkout';
            $data['sub_content'] = 'customer-info-shipping-address';
            
            $this->load->view_store( 'index', $data );
        }
        else
        {
			
            $this->session->set_userdata($_POST);
            $this->session->set_userdata('customer_bill_state',$_POST['customer_ship_state']);
			
            $this->session->set_userdata('save_customer_info','off');
            if(isset($_POST['save_customer_info'])) $this->session->set_userdata('save_customer_info','on');
			
            
        	//success
            redirect('checkout/shipping');
        }    

	}
}