<!DOCTYPE html>
<html>
<head>
	<title></title>
    <script src="<?php echo base_url('/public/'.STORE.'/js/jquery.min.js');?> "></script>

</head>
<body>

<h3>Test Page For Add To Cart</h3>
<style type="text/css">

#wrap{
    width: 1024px;
}
	ul.products{
    list-style-type: none;
    width: 100%;
    margin: 0;
    padding: 0;
}
 
    ul.products li{
        background: #eeeeee;
        border: 1px solid #d3d3d3;
        padding: 5px;
        width: 250px;
        text-align: center;
        float: left;
        margin-right: 10px;
    }
 
    ul.products h3{
        margin: 0;
        padding: 0px 0px 5px 0px;
        font-size: 14px;
    }
     
    ul.products small{
        display: block;
    }
     
    ul.products form fieldset{
        border: 0px;
    }
     
    ul.products form label{
        font-size: 12px;
    }
    ul.products form input, ul.products form select{
    	padding: 5px;
    }
     
    ul.products form input.quantity{
        width: 18px;
        background: #FFF;
        border: 1px solid #d3d3d3;
    }
    ul.products form input.custom_text{
        width: 100px;
        background: #FFF;
        border: 1px solid #d3d3d3;
    }

    .cart_list{
    	width: 300px;
    	float: right;;
    }
</style>

<ul class="products">
    <?php 

    foreach($products as $p): ?>
    <li>
        <h3><?php echo $p['name']; ?></h3>
        <img src="<?php echo base_url(); ?>public/<?php echo STORE;?>/images/<?php echo $p['image']; ?>" alt="" width="150" />
        <small> $ <?php echo $p['price']; ?></small>
        
        <form name="ajaxform" class="add_to_cart_form" action="" method="POST">
        	<input type="hidden" name="product_thumbnail" value="<?php echo base_url(); ?>public/<?php echo STORE;?>/images/<?php echo $p['image']; ?>">
            <fieldset>
                <label>Quantity</label>
                
                <input type="text" name="quantity" class="quantity" value="1" maxlength="2">
                <br/>
                <b>Options</b><br/>
                <select id="shirt_style" name="shirt_style">
               		<option value="round-neck">round neck</option>
               		<option value="v-neck">v neck</option>
               		<option value="pull-over-hoodie">pull over hoodie</option>
                </select>
                <select id="shirt_color" name="shirt_color">
               		<option value="black">black</option>
               		<option value="white">white</option>
               		<option value="gray">gray</option>
                </select> <br/>
                <label>Size</label> 
                <select class="form-control" id="select-size" name="shirt_size">
					<option value="Small">Small</option>
					<option value="Medium">Medium</option>
					<option value="Large">Large</option>
					<option value="4">4</option>
					<option value="5/6">5/6</option>												  
				</select>
														 <br/>
														 <hr/>
                <input type="text" name="custom_text_1" placeholder="Custom text 1" alt="custom text" class="custom_text"><br/>
                <input type="text" name="custom_text_2" placeholder="Custom text 2" alt="custom text" class="custom_text"><br/>
                <input type="text" name="custom_text_3" placeholder="Custom text 3" alt="custom text" class="custom_text"><br/>
                
                <select id="pattern" name="pattern">
               		<option value="">Select Pattern</option>
               		<option value="pattern_1">Pattern 1</option>
               		<option value="pattern_2">Pattern 2</option>
               		<option value="pattern_3">Pattern 3</option>
                </select> 

                <select id="texture" name="texture">
               		<option value="">Select Texture</option>
               		<option value="texture_1">Texture 1</option>
               		<option value="texture_2">Texture 2</option>
               		<option value="texture_3">Texture 3</option>
                </select>
                <select id="emojie" name="emojie">
                	<option value="">Select Emojie</option>
               		<option value="Emojie_1">Emojie 1</option>
               		<option value="Emojie_2">Emojie 2</option>
               		<option value="Emojie_3">Emojie 3</option>
                </select>

                <select id="Icon" name="Icon">
                	<option value="">Select Icon</option>
               		<option value="Icon_1">Icon 1</option>
               		<option value="Icon_2">Icon 2</option>
               		<option value="Icon_3">Icon 3</option>
                </select> <br/>
                 <select id="Color1" name="Color1">
               		<option value="Color1_1">Color1 1</option>
               		<option value="Color1_2">Color1 2</option>
               		<option value="Color1_3">Color1 3</option>
                </select>
                 <select id="Color2" name="Color2">
               		<option value="Color2_1">Color2 1</option>
               		<option value="Color2_2">Color2 2</option>
               		<option value="Color2_3">Color2 3</option>
                </select>
                <?php echo form_hidden('product_id', $p['id']); ?>
                <br/> 
                <hr>
                <input type="submit" name="add" class="add_to_cart_btn" value="Add to cart">
            </fieldset>
        <?php echo form_close(); ?>
    </li>
    <?php endforeach;?>
</ul>


<div id="wrap">
 
    
     
    <div class="cart_list">
        <h3>Your shopping cart</h3>
        <div id="cart_content">
            <?php echo $this->load->view('cart/test-cartview'); ?>
        </div>
    </div>
</div>


<script type="text/javascript">
	
$(document).ready(function() {
    $('.add_to_cart_form').on('submit', function (e) {

        $.ajax({
            type: 'post',
            url: '<?php echo site_url('cart/add_item');?>',
            data: $(this).serialize(),
            success: function () {
                location.reload();
            }
        });
        e.preventDefault();
    });
});

</script>




</body>
</html>