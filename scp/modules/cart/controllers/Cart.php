<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends MX_Controller   {

	var $test_products = array();

	function __construct()
    {
        parent::__construct();
        $this->load->model(	array('school/Schools_model', 'inventory/Inventory_model', 'settings/Settings_model', 'checkout/Checkout_model', 'signin/Signin_model', 'template/Template_model' ) );
        $this->load->helper('form');
        $this->load->library( array('Aauth', 'session') );

        $this->test_products = array(
				1 => array(
					'id'=>1,
					'name'=>'test product 1',
					'price'=>'15.50',
					'image'=>'shirt-2.jpg',
					
					),
				2 => array(
					'id'=>2,
					'name'=>'test product 2',
					'price'=>'14.50',
					'image'=>'shirt-1.jpg',
					
					)
		);
    }
	
	public function index()
	{ 
		$data['content'] = 'cart-view';

		$this->load->view_store( 'cart', $data );
	}

	// functions for tzilla 3.0 layout
	private function get_shirt_color( $colorId, $arrColorList, $fieldName = '' )
	{
		$returnValue = 'FFFFFF';

		foreach ( $arrColorList as $key => $value ) {
			if ( $value->color_id == $colorId ) {
				if ( strlen($value->$fieldName) != 0 || $value->$fieldName != NULL || $value->$fieldName != null ) {
					$returnValue = $value->$fieldName;
				}
			}
		}

		return $returnValue;
	}

	// functions for tzilla 3.0 layout
	private function get_shirt_color_sizes( $plan_id, $color_id )
	{
		$getdata = $this->Inventory_model->get_all_shirt_item_sizes_default( $plan_id, $color_id );		
		$cols = "";
		$dec_style_id = "";
		$default = 0;       

        $arr_style = "";
       
		if(!empty($getdata)){
			$arr_style = Array();
			$arr_style3 = Array();
			

			 foreach ($getdata as $key => $value) {
	            $dec_style_id = $value->shirt_style_id;
	            $arr_style[] .= $value->shirt_size_code;  
	            $arr_style3[] = array('size_code' => $value->shirt_size_code, 'style_id' => $dec_style_id, 'style_code' => $value->style_code );  
				$cols .= $value->shirt_size_code.':'.$dec_style_id.':'.$value->style_code.',';            
	        }
		}        

        $getdata2 = $this->Inventory_model->get_all_shirt_item_sizes( $plan_id, $color_id, $arr_style );
        if(!empty($getdata2)){
	        $arr_style2 = Array();
	        $arr_style4 = Array();
	        foreach ($getdata2 as $key => $value) {
	        	$dec_style_id = $value->shirt_style_id;
	            $arr_style2[] .= $value->shirt_size_code;  
	            $arr_style4[] = array('size_code' => $value->shirt_size_code, 'style_id' => $dec_style_id, 'style_code' => $value->style_code ); 
				$cols .= $value->shirt_size_code.':'.$dec_style_id.':'.$value->style_code.',';    
	        }
    	}

		$returnValue = rtrim($cols,',');
        return $returnValue;
	}

	private function get_shirt_detail( $shirtId, $arrShirtList, $fieldName = '' )
	{
		$returnValue = '';

		foreach ( $arrShirtList as $key => $value ) {
			if ( $value->id == $shirtId ) {
				if ( strlen($value->$fieldName) != 0 || $value->$fieldName != NULL || $value->$fieldName != null ) {
					$returnValue = $value->$fieldName;
				}
			}
		}

		return $returnValue;
	}

	private function get_shirt_plan_id( $planid, $color_id )
	{
		$returnValue = '';
		$returnValue = $this->Inventory_model->get_colors_by_plan_id( $planid, $color_id );
		return $returnValue['shirt_style_id'];
	}

	private function get_matrix_item_code( $style_id, $colorcode, $size )
	{
		$returnValue = '';
		$returnValue = $this->Inventory_model->get_matrix_item_code( $style_id, $colorcode, $size );
		return $returnValue['matrix_item_code'];
	}

	private function get_campaign_data( $product_id, $fieldName = '' )
	{
		$returnValue = '';
		$returnValue = $this->Inventory_model->get_campaign_data( $product_id );
		return $returnValue[$fieldName];
	}

	private function get_campaign_price( $product_id, $plan_id )
	{
		$returnValue = '';
		$returnValue = $this->Inventory_model->get_campaign_price( $product_id, $plan_id );
		return $returnValue['selling_price'];
	}

	private function get_product_details()
	{
		$arr = $this->session->userdata('product_details');
		$prevTemplate = 0;
		$prevStyle = 0;
		$ctr = 0;
		$indx = 0;
		$styles = array();
		$new_value = array();
		var_dump($arr);
		 foreach ( $arr as $key => $val) {
		 	if ( $prevTemplate == 0 ) {
					$prevTemplate = $val['parent_id'];

					$indx = $ctr;
					$new_value[$ctr]['parent_id'] = $val['parent_id'];
					$new_value[$ctr]['template_id'] = $val['parent_id'];
					$ctr++;
				}

				if ( $prevStyle != $val['shirt_style'] ) {
					if ( $prevTemplate != $val['parent_id'] ) {
						$indx = $ctr;
						$new_value[$ctr]['parent_id'] = $val['parent_id'];
						$new_value[$ctr]['template_id'] = $val['parent_id'];
						$ctr++;
						$styles = array();
					}
				} else if ( $prevTemplate != $val['parent_id'] ) {
					$indx = $ctr;
					$new_value[$ctr]['parent_id'] = $val['parent_id'];
					$new_value[$ctr]['template_id'] = $val['parent_id'];
					$ctr++;
					$styles = array();
				}

				if ( $prevTemplate == $val['parent_id'] && $prevStyle != $val['shirt_style'] ) {
					array_push( $styles, array( "id" => $val['shirt_style'],"shirt_style" => $val['shirt_style'] ) );

					$new_value[$indx]['shirts_and_colors'] = $styles;
				} else if ( $prevTemplate != $val['parent_id'] && $prevStyle != $val['shirt_style'] ) {
					array_push( $styles, array( "id" => $val['shirt_style'],"shirt_style" => $val['shirt_style'] ) );

					$new_value[$indx]['shirts_and_colors'] = $styles;
				} else if ( $prevTemplate != $val['parent_id'] && $prevStyle == $val['shirt_style'] ) {
					array_push( $styles, array( "id" => $val['shirt_style'],"shirt_style" => $val['shirt_style'] ) );

					$new_value[$indx]['shirts_and_colors'] = $styles;
				}

				$prevTemplate = $val['parent_id'];
				$prevStyle = $val['shirt_style'];
		 }

		 	$ctr = 0;
			// loop styles and colors
			foreach ( $arr as $key => $val) {

				for ( $x = 0; $x < count($new_value); $x++ ) {
					for ( $y = 0; $y < count($new_value[$x]['shirts_and_colors']); $y++ ) {
						if ( $new_value[$x]['template_id'] == $val['parent_id'] && $new_value[$x]['shirts_and_colors'][$y]['id'] == $val['shirt_style'] ) {
							$new_value[$x]['shirts_and_colors'][$y]['id'] = $val['shirt_style'];
							$new_value[$x]['shirts_and_colors'][$y]['color'] = implode(",",$val['shirt_colors']);
							$new_value[$x]['shirts_and_colors'][$y]['colors'] = $val['shirt_colors'];
							// $new_value[$x]['shirts_and_colors'][$y]['colors'] = $colors;
							$new_value[$x]['shirts_and_colors'][$y]['data_price'] = ( isset($val['data_price']) ) ? $val['data_price'] : 0;
							$new_value[$x]['shirts_and_colors'][$y]['art_position'] = ( isset($val['art_position']) ) ? $val['art_position'] : "";
							$new_value[$x]['shirts_and_colors'][$y]['canvas_position'] = ( isset($val['canvas_position']) ) ? $val['canvas_position'] : "";
							$new_value[$x]['template_image'] = $val['template_image'];
							$new_value[$x]['properties'] = $val['properties'];
							$new_value[$x]['design_id'] = $val['design_id'];
							// $new_value[$x]['canvas_position'] = $val['canvas_position'];
							// $new_value[$x]['art_position'] = $val['art_position'];
							$new_value[$x]['hex_value'] = $val['hex_value'];
							// $new_value[$x]['campaign_id'] = ( isset($val['campaign_id']) ) ? $val['campaign_id'] : 0;
							$new_value[$x]['product_id'] = ( isset($val['product_id']) ) ? $val['product_id'] : 0;
						}
					}
				}

				$prevTemplate = $val['parent_id'];
				$prevStyle = $val['shirt_style'];
			}

		$designsLists = $new_value;

		/* FINAL */

		$arr2 = $this->session->userdata('product_details');
		$ctr1 = 0;
		$ctr2 = 0;
		$new_value2 = array();

		foreach ( $arr2 as $key => $val2) {
				$new_value2[$ctr1]['parent_id'] = $val2['parent_id'];
				$new_value2[$ctr1]['template_id'] = $val2['template_id'];
				$new_value2[$ctr1]['display_name'] = $val2['display_name'];
				$data_price = ( isset($val2['data_price']) ) ? $val2['data_price'] : 0;
				$art_position = ( isset($val2['art_position']) ) ? $val2['art_position'] : "";
				$canvas_position = ( isset($val2['canvas_position']) ) ? $val2['canvas_position'] : 0;
				$styles2 = array( "id" => $val2['shirt_style'], "shirt_style" => $val2['shirt_style'], "color" => implode(",",$val2['shirt_colors']), "colors" => $val2['shirt_colors'], "data_price" => $data_price, "art_position" => $art_position, "canvas_position" => $canvas_position );
				$new_value2[$ctr1]['shirts_and_colors'] = $styles2;
				$new_value2[$ctr1]['template_image'] = $val2['template_image'];
				$new_value2[$ctr1]['properties'] = $val2['properties'];
				$new_value2[$ctr1]['design_id'] = $val2['design_id'];
				$new_value2[$ctr1]['hex_value'] = $val2['hex_value'];
				$new_value2[$ctr1]['campaign_id'] = ( isset($val2['campaign_id']) ) ? $val2['campaign_id'] : 0;
				$new_value2[$ctr1]['product_id'] = ( isset($val2['product_id']) ) ? $val2['product_id'] : 0;
	
				$ctr1++;
			}

		$designsLists2 = $new_value2;

		return $designsLists2;
	}


	public function review()
	{
		if ( $this->session->userdata('school_id') == null ) {
			redirect("school/store");
		}
		$this->session->unset_userdata("cart_items_id");
		$school_details = $this->Schools_model->get_school( $this->session->userdata('school_id') );
		$data['shirt_colors'] = $this->Inventory_model->get_all_shirt_colors();
		$data['shirt_plans'] = $this->Inventory_model->get_all_shirt_plans();		
		// $data['check_all_shirt_item_sizes'] = $this->Inventory_model->get_all_shirt_item_details(); //for checking data

		$data['school_id'] = $school_details->school_id;
		$data['school_name'] = $school_details->school_name;
		$data['items'] = [];
		$data['colors_lists'] = "";
	
		$data['product_details'] = $this->get_product_details();

		if ( count( $this->session->userdata('product_details') ) > 0 ){
			$ctr = 0;

			foreach ( $this->session->userdata('product_details') as $key => $value ) {

				foreach ( $value['shirt_colors'] as $color_key => $color_value ) {
					$data['colors_lists'] .= ( strlen( $data['colors_lists'] ) > 0 ? "," : "" ).$color_value;
					$shirt_id = $this->get_shirt_plan_id( $this->get_shirt_detail( $value['shirt_style'], $data['shirt_plans'], 'id' ), $color_value );
					$plan_id = $this->get_shirt_detail( $value['shirt_style'], $data['shirt_plans'], 'id' );
					$product_id = ( isset( $value['product_id'] ) ) ? $value['product_id'] : 0;
					$campaign_id = ( isset( $value['campaign_id'] ) ) ? $value['campaign_id'] : 0;
					// $campaign_id = $this->get_campaign_data( $product_id, 'campaign_id');
					$campaign_price = $this->get_campaign_price( $product_id, $plan_id );
					$data['items'][$ctr] = array(
													'parent_id' => $value['parent_id'],
													'template_id' => $value['template_id'],
													'campaign_id' => (strlen( $campaign_id ) > 0)? $campaign_id : 0,
													'product_id' => $product_id,
													'design_id' => $value['design_id'],
													'back_color' => $value['back_color'],
													'template_image' => $value['template_image'],
													'custom_fields' => $value['properties'],
													'canvas_position' => $value['canvas_position'],
													'art_position' => $value['art_position'],
													'shirt_plan_id' => $plan_id,
													'selling_price' => $campaign_price,
													'shirt_style_id' => $shirt_id,
													'shirt_plan_image' => $this->get_shirt_detail( $value['shirt_style'], $data['shirt_plans'], 'front_image_link' ),
													'shirt_plan_name' => $this->get_shirt_detail( $value['shirt_style'], $data['shirt_plans'], 'group_name' ),
													'shirt_color_id' => $this->get_shirt_color( $color_value, $data['shirt_colors'], 'color_id' ),
													'shirt_color_hex' => $this->get_shirt_color( $color_value, $data['shirt_colors'], 'hex_value' ),
													'shirt_color_code' => $this->get_shirt_color( $color_value, $data['shirt_colors'], 'color_code' ),
													'shirt_color_name' => $this->get_shirt_color( $color_value, $data['shirt_colors'], 'color_name' ),
													'shirt_color_sizes' => $this->get_shirt_color_sizes( $plan_id, $this->get_shirt_color( $color_value, $data['shirt_colors'], 'color_id' ) )
												);
					$ctr++;
				}
			}
		}

		// echo "<pre>";
		// print_r( $this->session->userdata('product_details') );
		// echo "</pre>";


		// TODO
		// 1. be able to go to create campaign page
		// 2. update your items button will update the sub totals and update the items in the session variable
		// 3. get default shirt style from the shirt plan loaded for specific color / generic color 
		// 4. function for zoom
		// 5. remove items in the list
		// 6. remove size chart link
		// 7. list available quantites per style and color set
		// 8. get price from retail value if the user came from buy mode
		// 9. load style color individually per style
		// 10. load design setting from tgen customization and save to product templates table before going to checkout
		// 11. save all product details to scp products table
		// 12. save 

		$this->load->view_store( 'cart-review', $data );
	} 

	private function get_data( $shirtId = 0, $colorCode = 0, $sizeCode = 0, $arrList, $field = '' ) {
		$returnValue = "";

		foreach ($arrList as $key => $value) {
			if ( $shirtId == $value->shirt_plan_id && $colorCode == $value->shirt_color_code && $sizeCode == $value->shirt_size_code ) {
				$returnValue = strlen( $value->$field ) > 0 ? $value->$field : '';
				break;
			}
		}

		return $returnValue;
	}

	public function checkout_items() {
		$signin = $this->Signin_model->get_user_by_email( $this->session->logged_in_email );

		$data['stat'] = "-";
		$data['message'] = "-";

		if ( $signin['status'] == 0 ) {
			$data['stat'] = "error";
			$data['message'] = "Please verify your account";
		} else {

			$bscs = $this->Inventory_model->get_all_inventory_list( $this->input->post('selected_colors') );
			$cart_items = $this->input->post('cart_items');			
			$cart_ids = count( $this->session->userdata( "cart_items_id" ) ) > 0 ? $this->session->userdata( "cart_items_id" ) : array();

			if ( count( $cart_items ) > 0 ) {
				// save cart details to scp_cart
				foreach ($cart_items as $key => $value) {
					$cartArray['store_id'] = STOREID;
					$cartArray['base_price'] = $value['base_price']; // can be changed when price matrix is implemented
					$cartArray['price'] = $value['price'];
					$cartArray['selling_price'] = $value['selling_price'];
					$cartArray['quantity'] = $value['quantity'];
					$cartArray['subtotal'] = $value['subtotal'];
					$cartArray['shirt_plan_id'] = $value['shirt_plan_id'];
					$cartArray['shirt_style_code'] = $value['shirt_style_id'];
					$cartArray['shirt_group_plan'] = $value['shirt_group_plan'];
					$cartArray['generic_shirt_color_code'] = $value['generic_shirt_color_code'];
					$cartArray['generic_color_name'] = $value['generic_color_name'];
					$cartArray['note'] = $value['note'];

					$cartArray['printer_setup'] = $this->get_data( $value['shirt_plan_id'], $value['generic_shirt_color_code'], $value['size_code'], $bscs, 'kornit_printer_id' );
					$cartArray['garment_type'] = $this->get_data( $value['shirt_plan_id'], $value['generic_shirt_color_code'], $value['size_code'], $bscs, 'garment_type' );

					// $cartArray['matrix_items'] = $this->get_data( $value['shirt_plan_id'], $value['generic_shirt_color_code'], $value['size_code'], $bscs, 'matrix_item_code' );
					$matrix_code = $this->Inventory_model->get_matrix_code( $value['shirt_style_id'], $value['generic_shirt_color_code'], $value['size_code'] );

					$cartArray['matrix_items'] = "FOTL-3930R-ASH-XL";

					if ( count( $matrix_code ) > 0 ) {
						$cartArray['matrix_items'] = $matrix_code->brand_code.'-'.$matrix_code->style_code.'-'.$matrix_code->shirt_color_code.'-'.$matrix_code->shirt_size_code;
					}

					$cartArray['item_code'] = $this->get_data( $value['shirt_plan_id'], $value['generic_shirt_color_code'], $value['size_code'], $bscs, 'item_code' );

					$campaign_id = $this->get_campaign_data( $value['product_id'], 'campaign_id');
					$product_id = $this->get_campaign_data( $value['product_id'], 'supplier_code');

					$cartArray['campaign_id'] = strlen( $campaign_id ) > 0 ? $campaign_id : 0;
					$cartArray['supplier_code'] = strlen( $product_id ) > 0 ? $product_id : $this->config->item("tzilla_supplier_code");
					$cartArray['product_id'] = $value['product_id'];
					// $cartArray['template_code'] = $value->template_code;

					$artwork_position = $this->Template_model->get_artwork_management_settings( $value['front_template_id'] );

					// setting for default artwork position value
					$art_x = -1;
					$art_y = -1;
					$art_w = 12;
					$art_h = 15;
					$art_p = 1;
					$art_percent = 1;

					foreach ($artwork_position as $position_key => $position_value) {
						if ( $value['shirt_plan_id'] == $position_value->shirt_style_id ) {
							$art_x = $position_value->x_axis;
							$art_y = $position_value->y_axis;
							$art_w = $position_value->design_width;
							$art_h = $position_value->design_height;
							$art_p = $position_value->kornit_pallet_id;
							$art_percent = ( $position_value->fa_percentage / 100 );
						}
					}

					// UPDATE THIS STRUCTURE TO SAVE INTO ANOTHER TABLE FOR LESS DATA TO BE PASSED UPON CREATING SALES ORDER
					$designs = array(
									"location" => "FRONT",
		                            "design_id" => $value['design_id'],
		                            "vector_id" => $value['design_id'],
		                            "x_coordinate" => $art_x,
		                            "y_coordinate" => $art_y,
		                            "position_uom" => "millimeter",
		                            "design_width" => $art_percent,
		                            "design_height" => $art_percent,
		                            "percentage" => $art_percent,
		                            "design_uom" => "inches",
		                            "printing_pallet" => $art_p,
		                            "art_printer_setup" => $cartArray['printer_setup'],
		                            "temp_image" => $value['thumbnail'],
			                        "nsItemNameFront" => $value['design_id'],
			                        "nsItemDescriptionFront" => $value['design_id'],
			                        "nsCostFront" => 0,
			                        "nsPriceFront" => 7.0, /* price to be paid out */
			                        "nsDesignTypeFront" => "",
			                        "nsDesignUrlFront" => $value['thumbnail'],
			                        "supplierCode" => $cartArray['supplier_code'],
			                        "template_id" => $value['front_template_id']
								);
									// /* TODO: the fields below would change for sure in the new future / disregard these */
									// "nsItemNameBack" => "",
									// "nsItemDescriptionBack" => "",
									// "nsCostBack" => 0,
									// "nsPriceBack" => 0,
									// "nsDesignTypeBack" => "",
									// "nsDesignUrlBack" => ""

					$cartArray['designs'] = serialize( $designs );

					// $cartArray['custom_options'] = $value->custom_options;
					$cartId = $this->Checkout_model->save_cart_items( $cartArray ); // save to database;

					array_push( $cart_ids, $cartId );
				}

				$this->session->set_userdata( "cart_items_id", $cart_ids );
				$data['stat'] = "success";
				$data['message'] = "checkout/shipping";
			}
			 
		}
	
		echo json_encode($data);

	}

	// end functions for tzilla 3.0 layout

	public function test_products()
	{
		$data['products'] = $this->test_products;

		$this->load->view('cart/test-products', $data);
	}

	public function edit( $product_slug = '', $cart_row_id = '' )
	{
		if ( strlen( $product_slug ) == 0 || strlen( $cart_row_id ) == 0 ) {
			// do nothing
		} else {
			$this->session->set_userdata('cart_id', $cart_row_id);

			redirect('product/'.$product_slug);
		}
	}


	public function add_item_to_cart()
	{
		$data['status'] = 'loading';		
		#set defaults
		$shirt_style = (isset($_POST['shirt_style'])) ? $_POST['shirt_style'] : '';
		$shirt_size = (isset($_POST['shirt_size']))? $_POST['shirt_size']:'';
		$shirt_color = (isset($_POST['shirt_color']))? $_POST['shirt_color']:'';

		// fill in template options details
		// $templateOptions['template_id'] = $_POST['template_id'];
		// $templateOptions['template_id'] = $_POST['template_id'];
		// $templateOptions['template_id'] = $_POST['template_id'];
		// $templateOptions['template_id'] = $_POST['template_id'];
		// $templateOptions['template_id'] = $_POST['template_id'];
		// $templateOptions['template_id'] = $_POST['template_id'];
		// $templateOptions['template_id'] = $_POST['template_id'];
		// end filling in options


		$templateArr = '['.$this->compose_request( $_POST ).']';
		$newId = $this->vector_save_template( $templateArr );

		// // template customization details
		// 'SCHOOL_NAME' => "",
		// 'CUSTOM_TEXT' => "",
		// 'DECADE' => "",
		// 'YEAR' => "",
		// 'MASCOT_HEAD' => "",
		// 'MASCOT_PATTERN' => "",
		// 'MASCOT_TEXTURE' => "",
		// 'SpotColor' => "",
		// 'PRIMARY_COLOR' => "RGB",
		// 'SECONDARY_COLOR' => "RGB"

		$get_shirt = $this->db->get_where( 'scp_inventory_shirt_style', array( 'style_code' => $shirt_style ) )->row();
		$get_color = $this->db->get_where( 'scp_inventory_shirt_color', array( 'shirt_color_id' => $shirt_color ) )->row();
		$get_size = $this->db->get_where( 'scp_inventory_shirt_size', array( 'shirt_size_id' => $shirt_size ) )->row();

		$data = array(
			'id' => $_POST['product_id'],
			'qty' => $_POST['quantity'],
			'price' => $_POST['price'],
			'name' => $_POST['name'],
			'shirt_style' => $shirt_style,
			'size' => $shirt_size,
			'color' => $shirt_color,
			'style_name' => $get_shirt->style_name,
			'color_name' => $get_color->color_name,
			'shirt_size' => $get_size->shirt_size,
			'options' => array(
				'shirt_style' =>  $shirt_style, 
				'size' =>  $shirt_size, 
				'color' => $shirt_color,

				'details' => array(
						isset( $_POST['template_details'] ) ? json_encode( $_POST['template_details'] ) : ""
					)

			),
			'images' => array(
						'template_id' => $_POST['template_id'],
						'src' => $_POST['src'],
						'design_position' => "center",
						'design_id' => $newId,
	
						'back_print' => 0,
						'back_template_id' => 0,
						'back_src' => (isset($_POST['back_src']))?$_POST['back_src']:"none",
						'back_design_position' => "center"
					)
		);

		// print_r( $data );

		$this->cart->insert($data);

		echo json_encode( $this->cart->contents() );

		// store_id
		// product_id
		// campaign_id

		// route_id

		// sku
		// erp_sku
		// name
		// slug
		// description
		// excerpt

		// price
		// saleprice

		// free_shipping
		// shippable
		// taxable

		// fixed_quantity
		// weight
		// track_stock
		// quanlity

		// related_products

		/*images {
					template_id : 0,
					src : "",
					design_position : "",

					back_print : 0,
					back_template_id : 0,
					back_src : "",
					back_design_position : "",

					// template customization details
					SCHOOL_NAME : "",
					CUSTOM_TEXT : "",
					DECADE : "",
					YEAR : "",
					MASCOT_HEAD : "",
					MASCOT_PATTERN : "",
					MASCOT_TEXTURE : "",
					SpotColor : "",
					PRIMARY_COLOR : RGB,
					SECONDARY_COLOR : RGB
				}*/


		// design_position --
		// back_design_position --

		// back_product_id
		// check_back
		// seo_title
		// meta
		// enabled
		// as_primary
		// shirt_id
		// gender_fit
		// shirt_brand
		// shirt_style_code
		// shirt_back_code
		// color
		// size
		// size_type
		// style
		// base_price
		// design_id
		// note
		// supplier_code
		// item_class
		// item_code
		// matrix_code
	}

	public function add_item()
	{
		print_r($_POST);

		$product_info = $this->test_products[$_POST['product_id']];
		
		#set defaults
		$shirt_style = (isset($_POST['shirt_style'])) ? $_POST['shirt_style'] : '';
		$shirt_size = (isset($_POST['shirt_size']))? $_POST['shirt_size']:'';
		$shirt_color = (isset($_POST['shirt_color']))? $_POST['shirt_color']:'';
		
		/*Front design*/
		//change the template id value to an actual id
		$template_id = 123;
		$ve_design_id = 456;		
		$custom_text_1 = (isset($_POST['custom_text_1']))? $_POST['custom_text_1']:'';
		$custom_text_2 = (isset($_POST['custom_text_2']))? $_POST['custom_text_2']:'';
		$custom_text_3 = (isset($_POST['custom_text_3']))? $_POST['custom_text_3']:'';
		$custom_text_4 = (isset($_POST['custom_text_4']))? $_POST['custom_text_4']:'';
		$pattern = (isset($_POST['pattern']))? $_POST['pattern']:'';
		$texture = (isset($_POST['texture']))? $_POST['texture']:'';
		$emojie = (isset($_POST['emojie']))? $_POST['emojie']:'';
		$icon = (isset($_POST['Icon']))? $_POST['Icon']:'';
		$color1 = (isset($_POST['Color1']))? $_POST['Color1']:'';
		$color2 = (isset($_POST['Color2']))? $_POST['Color2']:'';
		$product_thumbnail = (isset($_POST['product_thumbnail']))? $_POST['product_thumbnail']:'';

		/*Back design*/
		//change the template id value to an actual id
		/*
		Set $template_id_back = 0, for no back print option
		*/
		$template_id_back = 0;
		$ve_design_id_back = 456;		
		$custom_text_1_back = (isset($_POST['custom_text_1_back']))? $_POST['custom_text_1_back']:'';
		$custom_text_2_back = (isset($_POST['custom_text_2_back']))? $_POST['custom_text_2_back']:'';
		$custom_text_3_back = (isset($_POST['custom_text_3_back']))? $_POST['custom_text_3_back']:'';
		$custom_text_4_back = (isset($_POST['custom_text_4_back']))? $_POST['custom_text_4_back']:'';
		$pattern_back = (isset($_POST['pattern_back']))? $_POST['pattern_back']:'';
		$texture_back = (isset($_POST['texture_back']))? $_POST['texture_back']:'';
		$emojie_back = (isset($_POST['emojie_back']))? $_POST['emojie_back']:'';
		$icon_back = (isset($_POST['Icon_back']))? $_POST['Icon_back']:'';
		$color1_back = (isset($_POST['Color1_back']))? $_POST['Color1_back']:'';
		$color2_back = (isset($_POST['Color2_back']))? $_POST['Color2_back']:'';
		$product_thumbnail_back = (isset($_POST['product_thumbnail_back']))? $_POST['product_thumbnail_back']:'';

		$data = array(
        'id'      => $_POST['product_id'],
        'qty'     => $_POST['quantity'],
        'price'   => $product_info['price'],
        'name'    => $product_info['name'],
        'shirt_style' =>  $shirt_style, 
        'size' =>  $shirt_size, 
        'color' => $shirt_color,
        'options' => array(
        	

        	'template_id' => $template_id,
        	've_design_id' => $ve_design_id,
        	'custom_text_1' => $custom_text_1,
        	'custom_text_2' => $custom_text_2,
        	'custom_text_3' => $custom_text_3,
        	'custom_text_4' => $custom_text_4,
        	'pattern' => $pattern,
        	'texture' => $texture,
        	'emojie' => $emojie,
        	'icon' => $icon,
        	'color1' => $color1,
        	'color2' => $color2,
        	'product_thumbnail' => $product_thumbnail,

        	'template_id_back' => $template_id_back,        	
        	've_design_id_back' => $ve_design_id_back,
        	'custom_text_1_back' => $custom_text_1_back,
        	'custom_text_2_back' => $custom_text_2_back,
        	'custom_text_3_back' => $custom_text_3_back,
        	'custom_text_4_back' => $custom_text_4_back,
        	'pattern_back' => $pattern_back,
        	'texture_back' => $texture_back,
        	'emojie_back' => $emojie_back,
        	'icon_back' => $icon_back,
        	'color1_back' => $color1_back,
        	'color2_back' => $color2_back,
        	'product_thumbnail_back' => $product_thumbnail_back
        	),
        'images'=> array(
        	'template_id' => $template_id,
			'design_id' => $ve_design_id,
			'src' => $product_thumbnail
			)
		);

		$this->cart->insert($data);

		print_r($data);
	}

	public function update()
	{
		$data = array();
		foreach ($_POST['rowid'] as $key => $value) {
			$d = array(
		        'rowid' => $value,
		        'qty'   => $_POST[$value]
			);

			array_push($data,$d);

		}
		
		$this->cart->update($data);

		$this->cart->format_number($this->cart->total());

		redirect('cart');
	}


	public function show(){

	}
	
	public function removeitem($rowid){
		echo $this->cart->remove($rowid);
		redirect('cart');
	}

	public function removecartitem($rowid){
		$this->cart->remove($rowid);

		echo json_encode( $this->cart->contents() );
	}

	public function empty_cart(){
		$this->cart->destroy();
		redirect('cart');
	}

	private function fetch_template( $id )
	{
		$this->db->where('template_id', $id);
		return $this->db->get('scp_design_templates')->row();
	}

	private function check_if_field_exist($array_result, $name) {
		$ret = false;

		foreach ( $array_result as $property ) {
			if ( $property->Name == $name ) {
				$ret = true;
			}
		}

		return $ret;
	}

	private function vector_save_template( $templateArray ) {
		$resultdata = '';
		$url = 'http://162.217.193.70/vector-engine-v3/api/pv/design/save';
		$contenttype = "application/json"; // or application/xml
		
		$requestData = $templateArray;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype;charset=UTF-8") );
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$requestData);
		
		$result = curl_exec ($ch);
		if ( $result !== false ) {
			$array = json_decode($result,true);    
			
			foreach ($array as $getValue) {
				$resultdata = $getValue['Id'];
			}

			return $resultdata;
		} else {
			return $resultdata;
		}
		curl_close ($ch);
	}

	private function compose_request( $templateOpts )
	{
		$primary_color = ""; $secondary_color = "";
		
		if ( isset( $templateOpts['pcolor'] ) ) {
			$primary_color = $templateOpts['pcolor'];
			unset( $templateOpts['pcolor'] );
		}
		
		if ( isset( $templateOpts['scolor'] ) ) {
			$secondary_color = $templateOpts['scolor'];
			unset( $templateOpts['scolor'] );
		}
		// $mascotHead = $templateOpts['mascotHead'];

		// lets get the template data from db
		$rec = $this->fetch_template( $templateOpts['template_id'] );

		unset( $templateOpts['id'] );
		unset( $templateOpts['src'] );

		$distress_directory = "F://pvTemplates/OneDrive/distress_patterns/";
		$heads_directory = "F://pvTemplates/OneDrive/mascot_heads/";
		$textures_directory = "F://pvTemplates/OneDrive/textures/";
		$patterns_directory = "F://pvTemplates/OneDrive/patterns/";

		$request = new stdClass() ;

		$request->ReferenceId  = $rec->reference_id;
		$request->Id           = $rec->vector_engine_template_id;
		$request->Name         = $rec->template_name;
		$request->TemplateType = $rec->template_type;
		$request->SourceUrl    = $rec->thumbnail;
		$request->OutputUrl    = "";
		$request->Tags         = array();

		$propArr = json_decode($rec->properties);

		// we dont need the stock properties, lets update it using the user supplied data
		$ctr = 0;
		$properties = [];
		$arr_cnt = count($templateOpts);

		foreach($templateOpts as $opt_name => $opt_data)
		{
			$checkField = $this->check_if_field_exist( $propArr, $opt_name );

			$ctr++;
			// $option = new stdClass();

			if ( $opt_name == "DistressPattern" && strlen( $opt_data ) > 0) {

				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "SetDistress";

				$activity_values = new stdClass();
				$activity_values->href = $distress_directory.$opt_data.".pv";
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_HEAD" && strlen( $opt_data ) > 0 ) {
				// $get_xy = explode("_", $request->Name);
				// echo $get_xy[count($get_xy)]."<br>";

				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "ReplaceMascot";

				$activity_values = new stdClass();
				// $activity_values->href = $heads_directory.$rec->template_name."_".$opt_data.".pv";
				// echo $request->Name."_".$opt_data.".pv ";
				$activity_values->href = $heads_directory.$request->Name."/".$opt_data.".pv";
				$activity_values->X_axis = 0;
				$activity_values->Y_axis = 0;
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_PATTERN" && strlen( $opt_data ) > 0 ) {
				$option = new stdClass();
				$option->Name = 'MASCOT_HEAD';

				$activities = new stdClass();
				$activities->Name = "ReplaceMascot";

				$activity_values = new stdClass();
				$activity_values->href = $heads_directory.$opt_data.".pv";
				$activity_values->X_axis = 0;
				$activity_values->Y_axis = 0;
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_HEAD_PATTERN" && strlen( $opt_data ) > 0 ) {
				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "ReplaceBackgroundImage";

				$activity_values = new stdClass();
				$activity_values->href = $patterns_directory.$opt_data.".png";
				$activity_values->Name = $opt_data;
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "SpotColor" ) {

				// do nothing it will be added at the end of the loop

			}
			else {
				if ( $checkField ) {
					$option = new stdClass();
					$option->Name = $opt_name;

					$activities = new stdClass();
					$activities->Name = "ReplaceText";

					$activity_values = new stdClass();
					$activity_values->Value = $opt_data;
					$activities->KeyValues  = $activity_values;

					$option->Activities[] = $activities; 

					$properties[] = $option;
				}
			}

			// add SpotColor to the item
			if ( strlen( $primary_color ) > 0 && strlen( $secondary_color ) > 0 ) {
				if ( $ctr == $arr_cnt ) {
					$option = new stdClass();
					$option->Name = "SpotColor";

					$activities = new stdClass();
					$activities->Name = "SetColor";

					$activity_values = new stdClass();
					$activity_values->PRIMARY_COLOR = $primary_color;
					$activity_values->SECONDARY_COLOR = $secondary_color;
					$activities->KeyValues  = $activity_values;

					$option->Activities[] = $activities; 

					$properties[] = $option;
				}
			}
		}

		$request->Properties = $properties;

		return json_encode($request);
	}
}