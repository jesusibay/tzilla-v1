<!DOCTYPE html>
<html>
<head>
	<title></title>

	<script src="<?php echo base_url('public/'.STORE.'/js/jquery.min.js');?>"></script>
</head>
<body>


<div id="innerpages" class="container page" style="padding:40px;">

<div style="padding-bottom:50px;">

	TEST FEDEX API
	<form>
		<input type="text" name="firstname" id="shipping_addr_fname" placeholder="firstname" value="John"/> <br/>
		<input type="text" name="lastname" id="shipping_addr_lname" placeholder="lastname"  value="Doe"/><br/>
		<input type="text" name="company" id="shipping_addr_company" placeholder="company" value="ABC co"/><br/>
		<input type="text" name="phone" id="shipping_addr_phone" placeholder="phone" value="1236549"/><br/>
		<input type="text" name="address1" id="shipping_addr_address1" placeholder="address1" value="12 st. WERTY"/><br/>
		<input type="text" name="address2" id="shipping_addr_address2" placeholder="address2" value=""/><br/>
		<input type="text" name="city" id="shipping_addr_city" placeholder="city" value="Santa Ana"/><br/>
		<input type="text" name="state" id="shipping_addr_state" placeholder="state" value="CA"/><br/>
		<input type="text" name="zip" id="shipping_addr_zip" placeholder="zip" value="92704"/><br/>
		<input type="text" name="country_code" id="shipto_country_code" placeholder="country_code" value="US" readonly/><br/>
		<br/>
		<table>
		<tr>
			<td> Weight per shirt: </td>
			<td><input type="text" name="weight_per_shirt" id="weight_per_shirt" placeholder="weight per shirt" value="8.5" style="width:60px; border:#EDEDED solid 1px;" readonly="readonly"/> in ounces </td>
		</tr>
		<tr>
			<td> No. of Shirts (Qty): </td>
			<td> <input type="text" name="quantity" id="quantity" placeholder="quantity" value="1" style="width:60px;"/> </td>
		</tr>
		<tr>
			<td> Total weight  </td>
			<td> <input type="text" name="total_weight" readonly="readonly" id="total_weight" placeholder="total_weight" value="8.5" style="width:60px;  border:#EDEDED solid 1px;"/>
			
			in ounces
			</td>
		</tr>
		
		<tr>
		<td> Total weight  </td>
		<td> <input type="text" name="total_weight_lbs" readonly="readonly" id="total_weight_lbs" placeholder="total_weight_lbs" value="0.53125" style="width:60px;  border:#EDEDED solid 1px;"/> in LBS </td>
		</tr>
		
		</table>
		<br/>
		<input type="button" id="get_shipping_rates" value="Get FedEx Rates"/>
			
		<br/>
		<br/>
		<div id="shipping_option"></div>
	</form>


	</div>
</div>
<script>
	jQuery(document).ready(function(){
	
	var posting;
	var site_url = "<?php echo site_url();?>";
	
	$('#quantity').keyup(function(){
		$('#total_weight').val( parseFloat($('#quantity').val()) * parseFloat($('#weight_per_shirt').val()) );
		$('#total_weight_lbs').val( parseFloat( $('#total_weight').val() ) / 16 );
	});
	
	$('#get_shipping_rates').click(function(){
		 get_shipping_rates();
	});
	
	function get_shipping_rates()
	{
		if ( $("#shipping_addr_state").val().length > 0 && $("#shipping_addr_city").val().length > 0 && $("#shipping_addr_zip").val().length > 0 ) {
			
			
			//console.log(posting); 
			if(posting && posting.readystate != 4){
					posting.abort();
			}
			
			$('#shipping_option').empty().append('<option>Calculating Shipping Rate...</option>');
			$(".loaderx").css("display","block");
			
			$('#checkout-submit').prop('disabled', true);
			
			
			url = site_url+'shipping_methods/shipping_test/get_shipping';
			// Send the data using post
			posting = $.post( url, {
				shipping_addr_fname:$('#shipping_addr_fname').val(), 
				shipping_addr_lname:$('#shipping_addr_lname').val(), 
				shipping_addr_company:$('#shipping_addr_company').val(), 
				shipping_addr_phone:$('#shipping_addr_phone').val(), 
				shipping_addr_address1:$('#shipping_addr_address1').val(), 
				shipping_addr_address2:$('#shipping_addr_address2').val(), 
				shipping_addr_state: $('#shipping_addr_state').val(), 
				shipping_addr_city: $('#shipping_addr_city').val(), 
				shipping_addr_zip:$('#shipping_addr_zip').val(),
				shipping_addr_country_code:$('#shipto_country_code').val(),
				total_weight:$('#total_weight').val()			
			},
			function( data ) {
				/* if(data.status =='error'){
					$('#shipping_option').empty().append('<option></option>');
					$('#shipping_method_notification').empty().append(data.message);
				}else{
					$('#shipping_method_notification').empty();
					$('#shipping_option').empty().append(data.message);
					
				} */
				
				//$('#shipping_option').empty().append(data.message);
				$('#shipping_option').empty().append(data.message);
			}, "json" );
			
		
		}
		
	}
	
	});
</script>


</body>
</html>