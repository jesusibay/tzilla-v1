<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_test extends MX_Controller   {
	
	public function index()
	{ 
			
		$this->load->view('fedex/test');
	}

	function track_order($tracking_code=''){
		#load fedex package
		$this->load->add_package_path(APPPATH.'third_party/Shipping/fedex/');
		$this->load->library('Fedex_testapi');
		
		/*This is a developer fedex credential.
		Replace this with live fedex credential*/
		$fedex_credential = array(
			'key' => 'Ka1c5qir09G4Y1TH',
			'password' => 'pyQ1YJ7UJEHOUqWpIAArA3crv',
			'account' => '510087623',	
			'meter' => '118640684'
			);
		// This is only an example, the numbers below will
		// differ depending on your system

		echo memory_get_usage() . "<br/>"; // 36640

		$a = str_repeat("Hello", 4242);

		echo memory_get_usage() . "<br/>"; // 57960

		unset($a);

		echo memory_get_usage() . "<br/>"; // 36744

		print_r( $this->fedex_testapi->track($tracking_code, $fedex_credential));

		echo memory_get_usage() . "<br/>"; // 36744

	}

	function addSmartPostDetail()
	{
		$smartPostDetail = array( 
			'Indicia' => 'PARCEL_SELECT',
			'AncillaryEndorsement' => 'CARRIER_LEAVE_IF_NO_RESPONSE',
			'SpecialServices' => 'USPS_DELIVERY_CONFIRMATION',
			'HubId' => '5902',
			'CustomerManifestId' => 'XXX'
		);
		return $smartPostDetail;
	}

	function get_shipping()
	{
	
	error_reporting(E_ALL & ~E_NOTICE);
		$ship_address['firstname'] = $_POST['shipping_addr_fname'];
		$ship_address['lastname'] = $_POST['shipping_addr_lname'];
		$ship_address['company'] = $_POST['shipping_addr_company'];
		$ship_address['phone'] = $_POST['shipping_addr_phone'];
		$ship_address['address1'] = $_POST['shipping_addr_address1'];
		$ship_address['address2'] = $_POST['shipping_addr_address2'];
		$ship_address['city'] = $_POST['shipping_addr_city'];
		$ship_address['state'] = $_POST['shipping_addr_state'];
		$ship_address['zone'] = '';
		$ship_address['zip'] = $_POST['shipping_addr_zip'];
		$ship_address['country_code'] = $_POST['shipping_addr_country_code'];
		
		$weight=$_POST['total_weight']/16; //convert to lbs
		//$package_count = (($weight * 16) / 8.5); //4
		
		//echo "package count".$package_count;
		
		if($_POST['shipping_addr_zip'] != '' || $_POST['shipping_addr_zip'] != 0)
		{

			//echo "weight: ".$weight;

			#load fedex package
			$this->load->add_package_path(APPPATH.'third_party/Shipping/fedex/');
			$this->load->library('Fedex_testapi');
			
			
			//$service_type='';
		//	$this->fedex_service_type='FEDEX_GROUND';
			//$service_type='SMART_POST';
			
			
			
			$fedex_rates = $this->fedex_testapi->rates($ship_address, $weight,$this->fedex_service_type, $this->addSmartPostDetail());
			
			//print_r($fedex_rates);
			//echo $ship_address['zip'];
			if($fedex_rates){
				
				if(!is_array($fedex_rates))
				{					
					if($fedex_rates->Notifications->Code==521){
					 	$msg = 'FedEx services are not available from the origin ZIP or postal code to this destination ZIP or postal code: '. $ship_address['zip'];
					
						echo json_encode(array("status"=>"error","fedex_error_code"=>$fedex_rates->Notifications->Code,"message"=>$msg,'fedex_message'=>$fedex_rates->Notifications->Message));						
					}else{
						echo json_encode($fedex_rates);
					}
				}else{
				
					if(is_array($fedex_rates))
					{
						if(count($fedex_rates)>0){
							//$option = '<li value="0">-Choose Shipping Method-</option><optgroup label="[weight:'.$weight .' LBS]">';
							//$option = 'Shipping Method<br/> [weight: '.$weight .' LBS] <br/>';
								$option = '';
								foreach($fedex_rates as $key=>$val)
								{
									$s='';
									if($key=='FEDEX_GROUND'){
										$s='selected="selected"';
										$default_shipping_fee = $val;
										$default_shipping_fee_key = $key;
									}
									$option .= ' <li '.$s.' value="'. $key.'-'.$val.'">'.str_replace('_',' ',$key).' - $'.$val.'</li>';
								}
								
						//	$option .= '</optgroup>';
							
							echo json_encode( array( 'status'=>'success', 'message'=>$option ) );
						}else{	
						
							echo json_encode(array('status'=>'error','message'=>'Rate not available'));
						}
					}else{
						
						echo json_encode(array('status'=>'error','message'=>'Rate not available'));
					}
					
				}
			}else{
				echo json_encode($fedex_rates);
			}
		}else{
			echo json_encode(array('status'=>'error','message'=>'invalid zipcode'));
		}
	}

	/*
	//prevent direct access via url
	public function _remap()
	{
	 	//prevent access via url
	}*/
}