<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_methods extends MX_Controller   {
	
	public function index()
	{ 
		
	}

	function addSmartPostDetail()
	{
		$smartPostDetail = array( 
			'Indicia' => 'PARCEL_SELECT',
			'AncillaryEndorsement' => 'CARRIER_LEAVE_IF_NO_RESPONSE',
			'SpecialServices' => 'USPS_DELIVERY_CONFIRMATION',
			'HubId' => '5902',
			'CustomerManifestId' => 'XXX'
		);

		return $smartPostDetail;
	}

	function rates()
	{
	
		error_reporting(E_ALL & ~E_NOTICE);
		$ship_address['firstname'] = $_POST['shipping_addr_fname'];
		$ship_address['lastname'] = $_POST['shipping_addr_lname'];
		$ship_address['company'] = $_POST['shipping_addr_company'];
		$ship_address['phone'] = $_POST['shipping_addr_phone'];
		$ship_address['address1'] = $_POST['shipping_addr_address1'];
		$ship_address['address2'] = $_POST['shipping_addr_address2'];
		$ship_address['city'] = $_POST['shipping_addr_city'];
		$ship_address['state'] = $_POST['shipping_addr_state'];
		$ship_address['zone'] = '';
		$ship_address['zip'] = $_POST['shipping_addr_zip'];
		$ship_address['country_code'] = $_POST['shipping_addr_country_code'];
		
		$weight=$_POST['total_weight']/16; //convert to lbs
		
		
		$shipping_mothods = array();

		#load fedex package
		$this->load->add_package_path(APPPATH.'third_party/Shipping/fedex')->library('fedex_api');
		#$this->load->add_package_path(APPPATH.'third_party/shipping/fedex/');
		#$this->load->library('fedex_api');

		if($_POST['shipping_addr_zip'] != '' || $_POST['shipping_addr_zip'] != 0)
		{

			//echo "weight: ".$weight;

			
						
			//$service_type='';
			//$this->fedex_service_type='FEDEX_GROUND';
			//$service_type='SMART_POST';
								
			$fedex_rates = $this->fedex_api->rates($ship_address, $weight,$this->fedex_service_type, $this->addSmartPostDetail());
			
			//print_r($fedex_rates);
			//echo $ship_address['zip'];
			if($fedex_rates){
				
				if(!is_array($fedex_rates))
				{					
					if($fedex_rates->Notifications->Code==521){
					 	$msg = 'FedEx services are not available from the origin ZIP or postal code to this destination ZIP or postal code: '. $ship_address['zip'];
					
						echo json_encode(array("status"=>"error","fedex_error_code"=>$fedex_rates->Notifications->Code,"message"=>$msg,'fedex_message'=>$fedex_rates->Notifications->Message));						
					}else{
						echo json_encode($fedex_rates);
					}
				}else{
				
					if(is_array($fedex_rates))
					{
						if(count($fedex_rates)>0)
						{							
							/*$option = '';
							foreach($fedex_rates as $key=>$val)
							{
								$s='';
								if($key=='FEDEX_GROUND'){
									$s='selected="selected"';
									$default_shipping_fee = $val;
									$default_shipping_fee_key = $key;
								}
								$option .= ' <li '.$s.' value="'. $key.'-'.$val.'">'.str_replace('_',' ',$key).' - $'.$val.'</li>';
							}
							
							//$option .= '</optgroup>';
							*/
							
							echo json_encode( array( 'status'=>'success', 'message'=>json_encode($fedex_rates) ) );

						}else{	
						
							echo json_encode(array('status'=>'error','message'=>'Rate not available'));
						}
					}else{
						
						echo json_encode(array('status'=>'error','message'=>'Rate not available'));
					}
					
				}
			}


		}else{
			echo json_encode(array('status'=>'error','message'=>'invalid zipcode'));
		}
	}

	function usps($method=null){
		
		$usps_methods = array(
			'USPS Priority Mail'=> array('method' =>'USPS Priority Mail', 'fee' => '3.99', 'currency' => 'USD', 'currency_symbol' => '$', 'default'=>1),
			'USPS Retail Ground'=> array('method' =>'USPS Retail Ground', 'fee' => '6.75', 'currency' => 'USD', 'currency_symbol' => '$', 'default'=>0)

			);

		if($method != '')
		{
			if(array_key_exists($method, $usps_methods))
			{
				return json_encode($usps_methods[$method]);
			}else{
				return json_encode(array('status'=>'failed', 'message'=>'Shipping method not found'));
			}
			
		}else{
			return json_encode($usps_methods);
		}
		
	}




	function fedex_ajax($data, $totalQuantity )
	{	
		// print_r($data);
		
		error_reporting(E_ALL & ~E_NOTICE);
		$ship_address['firstname'] = $data['cust_firstname'];
		$ship_address['lastname'] = $data['cust_lastname'];
		$ship_address['company'] = $data['cust_address_1'];
		$ship_address['phone'] = $data['cust_contact_no'];
		$ship_address['address1'] = $data['cust_address_1'];
		$ship_address['address2'] = $data['cust_address_2'];
		$ship_address['city'] = $data['cust_city'];
		$ship_address['state'] = $data['cust_state'];
		$ship_address['zone'] = '';
		$ship_address['zip'] = $data['cust_zip'];
		$ship_address['country_code'] = 'US';
		
		$weight=$totalQuantity/16; //convert to lbs
		//$package_count = (($weight * 16) / 8.5); //4
		
		//echo "package count".$package_count;
		
		$shipping_mothods = array();

		#load fedex package
		$this->load->add_package_path(APPPATH.'third_party/Shipping/fedex')->library('fedex_api');
		#$this->load->add_package_path(APPPATH.'third_party/shipping/fedex/');
		#$this->load->library('fedex_api');
		if( $data['cust_zip'] != '' || $data['cust_zip'] != 0)
		{

			//echo "weight: ".$weight;

			
						
			//$service_type='';
			//$this->fedex_service_type='FEDEX_GROUND';
			//$service_type='SMART_POST';
								
			$fedex_rates = $this->fedex_api->rates($ship_address, $weight,$this->fedex_service_type, $this->addSmartPostDetail());
			
			//print_r($fedex_rates);
			//echo $ship_address['zip'];
			if($fedex_rates){

				if(!is_array($fedex_rates))
				{					
					if($fedex_rates->Notifications->Code==521){
					 	$msg = 'FedEx services are not available from the origin ZIP or postal code to this destination ZIP or postal code: '. $ship_address['zip'];
					
						echo json_encode(array("status"=>"error","fedex_error_code"=>$fedex_rates->Notifications->Code,"message"=>$msg,'fedex_message'=>$fedex_rates->Notifications->Message));						
					}else{
						echo json_encode($fedex_rates);
					}
				}else{
				
					if(is_array($fedex_rates))
					{
						if(count($fedex_rates)>0)
						{							
							/*$option = '';
							foreach($fedex_rates as $key=>$val)
							{
								$s='';
								if($key=='FEDEX_GROUND'){
									$s='selected="selected"';
									$default_shipping_fee = $val;
									$default_shipping_fee_key = $key;
								}
								$option .= ' <li '.$s.' value="'. $key.'-'.$val.'">'.str_replace('_',' ',$key).' - $'.$val.'</li>';
							}
							
							//$option .= '</optgroup>';
							*/
							
							echo json_encode( array( 'status'=>'success', 'message'=> json_encode($fedex_rates) ) );

						}else{	
						
							echo json_encode(array('status'=>'error','message'=>'Rate not available'));
						}
					}else{
						
						echo json_encode(array('status'=>'error','message'=>'Rate not available'));
					}
					
				}
			}


		}else{
			echo json_encode(array('status'=>'error','message'=>'invalid zipcode'));
		}
	}




	// function fedex($data, $totalQuantity )
	function fedex($data, $totalWeight )
	{	
		
		
		error_reporting(E_ALL & ~E_NOTICE);
		$ship_address['firstname'] = $data['cust_firstname'];
		$ship_address['lastname'] = $data['cust_lastname'];
		$ship_address['company'] = $data['cust_address_1'];
		$ship_address['phone'] = $data['cust_contact_no'];
		$ship_address['address1'] = $data['cust_address_1'];
		$ship_address['address2'] = $data['cust_address_2'];
		$ship_address['city'] = $data['cust_city'];
		$ship_address['state'] = $data['cust_state'];
		$ship_address['zone'] = '';
		$ship_address['zip'] = $data['cust_zip'];
		$ship_address['country_code'] = 'US';
		
		// $weight=$totalQuantity/16; //convert to lbs
		// $weight = $totalQuantity * 0.53125; //convert to lbs
		$weight = $totalWeight;
		//$package_count = (($weight * 16) / 8.5); //4
		
		//echo "package count".$package_count;
		
		$shipping_mothods = array();

		#load fedex package
		$this->load->add_package_path(APPPATH.'third_party/Shipping/fedex')->library('fedex_api');
		#$this->load->add_package_path(APPPATH.'third_party/shipping/fedex/');
		#$this->load->library('fedex_api');
		if( $data['cust_zip'] != '' || $data['cust_zip'] != 0)
		{

			

			// $service_type='';
			// if ( $weight >= 150 ) {
			// 	$this->fedex_service_type = 'FEDEX_FIRST_FREIGHT';
			// }
			// $service_type='SMART_POST';
								
			$fedex_rates = $this->fedex_api->rates($ship_address, $weight, $this->fedex_service_type, $this->addSmartPostDetail());
			
			//print_r($fedex_rates);
			//echo $ship_address['zip'];
			if($fedex_rates){

				if(!is_array($fedex_rates))
				{					
					if($fedex_rates->Notifications->Code==521){
					 	$msg = 'FedEx services are not available from the origin ZIP or postal code to this destination ZIP or postal code: '. $ship_address['zip'];
					
						return array("status"=>"error","fedex_error_code"=>$fedex_rates->Notifications->Code,"message"=>$msg,'fedex_message'=>$fedex_rates->Notifications->Message);						
					}else{
						return $fedex_rates;
					}
				}else{
				
					if(is_array($fedex_rates))
					{
						if(count($fedex_rates)>0)
						{							
							/*$option = '';
							foreach($fedex_rates as $key=>$val)
							{
								$s='';
								if($key=='FEDEX_GROUND'){
									$s='selected="selected"';
									$default_shipping_fee = $val;
									$default_shipping_fee_key = $key;
								}
								$option .= ' <li '.$s.' value="'. $key.'-'.$val.'">'.str_replace('_',' ',$key).' - $'.$val.'</li>';
							}
							
							//$option .= '</optgroup>';
							*/
							
							return array( 'status'=>'success', 'message'=> json_encode($fedex_rates) );

						}else{	
						
							return array('status'=>'error','message'=>'Rate not available');
						}
					}else{
						
						return array('status'=>'error','message'=>'Rate not available');
					}
					
				}
			}


		}else{
			return array('status'=>'error','message'=>'invalid zipcode');
		}
	}



	/* get shipping rates */



	function shipping_rates()
	{	
		
		error_reporting(E_ALL & ~E_NOTICE);
		$ship_address['firstname'] = $data['cust_firstname'];
		$ship_address['lastname'] = $data['cust_lastname'];
		$ship_address['company'] = $data['cust_address_1'];
		$ship_address['phone'] = $data['cust_contact_no'];
		$ship_address['address1'] = $data['cust_address_1'];
		$ship_address['address2'] = $data['cust_address_2'];
		$ship_address['city'] = $data['cust_city'];
		$ship_address['state'] = $data['cust_state'];
		$ship_address['zone'] = '';
		$ship_address['zip'] = $data['cust_zip'];
		$ship_address['country_code'] = 'US';
		
		$weight=$totalQuantity*0.53125; //convert to lbs
		//$package_count = (($weight * 16) / 8.5); //4
		
		//echo "package count".$package_count;
		
		$shipping_mothods = array();

		#load fedex package
		$this->load->add_package_path(APPPATH.'third_party/Shipping/fedex')->library('fedex_api');
		#$this->load->add_package_path(APPPATH.'third_party/shipping/fedex/');
		#$this->load->library('fedex_api');
		if( $data['cust_zip'] != '' || $data['cust_zip'] != 0)
		{

			//echo "weight: ".$weight;

			
						
			//$service_type='';
			//$this->fedex_service_type='FEDEX_GROUND';
			//$service_type='SMART_POST';
								
			$fedex_rates = $this->fedex_api->rates($ship_address, $weight,$this->fedex_service_type, $this->addSmartPostDetail());
			
			// print_r($fedex_rates);
			//echo $ship_address['zip'];
			if($fedex_rates){

				if(!is_array($fedex_rates))
				{					
					if($fedex_rates->Notifications->Code==521){
					 	$msg = 'FedEx services are not available from the origin ZIP or postal code to this destination ZIP or postal code: '. $ship_address['zip'];
					
						return array("status"=>"error","fedex_error_code"=>$fedex_rates->Notifications->Code,"message"=>$msg,'fedex_message'=>$fedex_rates->Notifications->Message);						
					}else{
						return $fedex_rates;
					}
				}else{
				
					if(is_array($fedex_rates))
					{
						if(count($fedex_rates)>0)
						{							
							/*$option = '';
							foreach($fedex_rates as $key=>$val)
							{
								$s='';
								if($key=='FEDEX_GROUND'){
									$s='selected="selected"';
									$default_shipping_fee = $val;
									$default_shipping_fee_key = $key;
								}
								$option .= ' <li '.$s.' value="'. $key.'-'.$val.'">'.str_replace('_',' ',$key).' - $'.$val.'</li>';
							}
							
							//$option .= '</optgroup>';
							*/
							
							return array( 'status'=>'success', 'message'=> json_encode($fedex_rates) );

						}else{	
						
							return array('status'=>'error','message'=>'Rate not available');
						}
					}else{
						
						return array('status'=>'error','message'=>'Rate not available');
					}
					
				}
			}


		}else{
			return array('status'=>'error','message'=>'invalid zipcode');
		}
	}
}