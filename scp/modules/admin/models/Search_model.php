<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search_model extends CI_Model {

    
    function __construct()
    {
        
        parent::__construct();

        $this->load->library( array('form_validation') );
    }


        function record_term($term)
    {
        $code   = md5($term);
        $this->db->where('code', $code);
        $exists = $this->db->count_all_results('scp_search');
        if ($exists < 1)
        {
            $this->db->insert('scp_search', array('code'=>$code, 'term'=>$term));
        }
        return $code;
    }

    
    function get_term($code)
    {
        $this->db->select('term');
        $result = $this->db->get_where('scp_search', array('code'=>$code));
        $result = $result->row();
        return $result->term;
    }

   

}