<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

    public $pageSize=1;
	function __construct() 
	{

		parent::__construct();

		$this->load->model('Dashboard_model','dashboard');
	}

	/*
	 * Default page for customization of artworks
	 *
	 * @param None
	 * @return TGEN Page
	 */
	public function index()
	{
	   $this->load->view_store( 'dashboard' ); 
	}

	public function getDashboardDetails(){
	   $details=array();
	   $row=$this->dashboard->get_campaigns_active($_REQUEST["id"]);
       $details["cntActiveCampaign"]=$row[0]['cntActiveCampaign'];
       $row=$this->dashboard->get_all_total_sales_count($_REQUEST["id"]);
       $details["totalprice"]=$row['totalprice'];
       $row=$this->dashboard->get_all_total_profit_count($_REQUEST["id"]);
       $details["total_profit"]=$row['total_profit'];
       $row=$this->dashboard->get_all_total_items_sold_count($_REQUEST["id"]);
       $details["quantity"]=$row['quantity'];
       $row=$this->dashboard->get_all_total_items_sold_count_today($_REQUEST["id"]);
       $details["quantity_today"]=$row['quantity'];
       $row=$this->dashboard->get_all_total_profit_count_today($_REQUEST["id"]);
       $details["total_profit_today"]=$row['total_profit'];
       $row=$this->dashboard->get_all_total_sales_count_today($_REQUEST["id"]);
       $details["totalprice_today"]=$row['totalprice'];
       
       
       echo json_encode($details);
	}
	public function getMyFundRaisers(){
        echo $this->dashboard->getFundRaisers();
	}
	public function getTeamScoreBoard(){
        echo $this->dashboard->getTeamScoreboard();
	}
    public function viewTeamMemberSales(){
        echo $this->dashboard->viewTeamMemberSales();
    }
    
    public function viewJoinedFundraisers(){
  echo $this->dashboard->ViewJoinedFundRaisers();
    }
    public function getPayouts(){
  echo $this->dashboard->get_all_payouts();
    }
    public function getSalesOrderDetails(){
        echo $this->dashboard->getSalesOrderDetails();
    }

}