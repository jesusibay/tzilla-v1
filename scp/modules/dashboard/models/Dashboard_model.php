<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    public $pageSize=25;
	function __construct()
	{

		parent::__construct();

	}
     public function get_campaigns_active($id){
        $row =  array( 'campaign_user_id' => $id, 'campaign_status' => 1 );
        $this->db->select("count(1) as cntActiveCampaign");
        $return = $this->db->get_where('scp_campaigns', $row )->result_array();
        return $return;
    }
    public function get_all_total_sales_count( $customer_id){ // get total number of sales date for all campaign
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured');
        $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    } 
    public function get_all_total_profit_count( $customer_id){ // get total number of profit for all campaign
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured');
        $this->db->select('sum(`campaign_profit`*`quantity`) as total_profit', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }

    public function get_all_total_items_sold_count( $customer_id){ // get total number of items sold for all campaign
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured');
        $this->db->select_sum('scp_sales_order_items.quantity');
        // $this->db->select('sum(`quantity`)', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }
    public function get_all_total_items_sold_count_today( $customer_id){ // get total number of items sold current date for all campaign
        $now = date('Y-m-d'); 
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null, 'scp_sales_orders.order_date >=' => $now  );
        $this->db->select_sum('scp_sales_order_items.quantity');
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;

    }
    public function get_all_total_sales_count_today( $customer_id){ // get total number of sales current date for all campaign
        $now = date('Y-m-d'); 
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null, 'scp_sales_orders.order_date >=' => $now  );
        $this->db->select('sum(`sale_price`*`quantity`) as totalprice', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    } 

    public function get_all_total_profit_count_today( $customer_id){ // get total number of profit current date for all campaign
        $now = date('Y-m-d'); 
        $row =  array( 'scp_campaigns.campaign_user_id' => $customer_id, 'scp_sales_orders.payment_state' => 'captured', 'scp_campaigns.campaign_ended !=' => 1, 'scp_sales_orders.checkout_id !=' => null, 'scp_sales_orders.order_date >=' => $now  );
        $this->db->select('sum(`campaign_profit`*`quantity`) as total_profit', FAlSE);
        $this->db->JOIN('scp_sales_order_items',  'scp_sales_order_items.order_id = scp_sales_orders.order_id');
        $this->db->JOIN('scp_campaign_products',  'scp_campaign_products.product_id = scp_sales_order_items.product_id' );
        $this->db->JOIN('scp_campaigns',  'scp_campaigns.campaign_id = scp_campaign_products.campaign_id', 'LEFT' );
        $return = $this->db->get_where('scp_sales_orders', $row )->row_array();
        return $return;
    }
    public function getFundRaisers(){
	   if(!isset($_REQUEST["pageNo"])){
	       $_REQUEST["pageNo"]="0";
	   }
       else{
         if(intval($_REQUEST["pageNo"])>=1)
         $_REQUEST["pageNo"]=(intval($_REQUEST["pageNo"])-1)*$this->pageSize;
         else
         $_REQUEST["pageNo"]="0";
       }
       $sort="";
       if(!isset($_REQUEST["sortColumn"]) || !isset($_REQUEST["sortDirection"])){
            $sort="";
        }
        else{
            $colName="";
            switch(strtolower($_REQUEST["sortColumn"])){
                case "fundraiser name":
                    $colName="scp_campaigns.campaign_title";
                break;
                case "status":
                    $colName="scp_campaigns.campaign_status";
                break;
                case "start date":
                    $colName="scp_campaigns.start_date";
                break;
                case "end date":
                    $colName="scp_campaigns.end_date";
                break;
                case "of units":
                    $colName="total_quantity";
                break;
                case "total sales":
                    $colName="total_sales";
                break;
                case  "total profits":
                    $colName="total_profits";
                break;
                default:
                    $colName="";
                break;    
                
            }
            if($colName!="" && ($_REQUEST["sortDirection"]=="asc" || $_REQUEST["sortDirection"]=="desc")){
                $sort=" ORDER BY $colName {$_REQUEST["sortDirection"]} ";
            }
            
            
        }
	    $res=$this->db->query("SELECT count(1) as cntPages from scp_campaigns WHERE campaign_user_id=?",array("s"=>$_REQUEST["id"]));
        $row=$res->result_array();
        $pages=ceil($row[0]["cntPages"]/$this->pageSize);
        $getCampaign["max_page"]=$pages;
       $res=$this->db->query("SELECT scp_campaigns.campaign_id,
                                     scp_campaigns.campaign_title,
                                     scp_campaigns.campaign_url,
                                     scp_campaigns.campaign_status,
                                     scp_campaigns.start_date,
                                     scp_campaigns.end_date,
                                     sum(scp_sales_order_items.total_quantity) as total_quantity,
                                     sum(scp_sales_order_items.total_sales) as total_sales,
                                     sum(scp_sales_order_items.total_profits) as total_profits
                              FROM scp_campaigns 
                              JOIN scp_campaign_products on scp_campaigns.campaign_id=scp_campaign_products.campaign_id 
                              JOIN (select product_id,sum(quantity) as total_quantity ,sum(sale_price) as total_sales,sum(campaign_profit) as total_profits from scp_sales_order_items group by product_id) as scp_sales_order_items on scp_campaign_products.product_id=scp_sales_order_items.product_id
                              WHERE campaign_user_id=?
                              group by  scp_campaigns.campaign_id,scp_campaigns.campaign_id,scp_campaigns.campaign_url,scp_campaigns.campaign_status,scp_campaigns.start_date,scp_campaigns.end_date
                              $sort
                              limit ?,{$this->pageSize};",array("s"=>$_REQUEST["id"],"s2"=>intval($_REQUEST["pageNo"])));
        
    
       $getCampaign["result"]=$res->result_array();
       //var_dump($this->config);
	   return json_encode($getCampaign);
    }
    public function getTeamScoreboard(){
        	   $arrStatements=array();
       $arrStatements["s1"]=$_REQUEST["id"];
$res=$this->db->query("SELECT count(1) as cntPages from scp_campaign_members WHERE campaign_id=?",array("s"=>$_REQUEST["id"]));
        $row=$res->result_array();
        $pages=ceil($row[0]["cntPages"]/$this->pageSize);
        $getTeamScoreBoard["max_page"]=$pages;
	   $qry="SELECT 
                                     
                                     scp_campaign_members.member_status,
                                     scp_users.name,
                                     scp_users.email,
                                     scp_campaign_members.share_count,
                                     scp_sales_order_items.total_quantity,
                                     scp_sales_order_items.total_sales,
                                     scp_sales_order_items.total_profits
                              FROM scp_campaign_members 
                              left JOIN scp_campaigns  on scp_campaigns.campaign_id=scp_campaign_members.campaign_id 
                              LEFT JOIN scp_users on scp_campaign_members.user_id=scp_users.id
                              LEFT JOIN (SELECT campaign_id,referrer_user_id,sum(quantity) as total_quantity ,sum(sale_price) as total_sales,sum(campaign_profit) as total_profits 
                                         FROM scp_sales_order_items
                                         JOIN scp_campaign_products on scp_sales_order_items.product_id=scp_campaign_products.product_id
                                         GROUP BY referrer_user_id,campaign_id) as scp_sales_order_items on scp_campaign_members.user_id=scp_sales_order_items.referrer_user_id
                                                                                                         and scp_sales_order_items.campaign_id=scp_campaigns.campaign_id
                              WHERE scp_campaigns.campaign_id=?";
                              // 
    if(isset($_REQUEST["name"])){
        $qry.=" and scp_users.name like ?";
        $arrStatements["s2"]='%'.$_REQUEST["name"].'%';
    }
    $sort="";
       if(!isset($_REQUEST["sortColumn"]) || !isset($_REQUEST["sortDirection"])){
            $sort="";
        }
        else{
            $colName="";
            switch(strtolower($_REQUEST["sortColumn"])){
                case "name":
                    $colName="scp_users.name";
                break;
                case "status":
                    $colName="scp_campaign_members.member_status";
                break;
                case "email":
                    $colName="scp_users.email";
                break;
                case "of shares":
                    $colName="scp_campaign_members.share_count";
                break;
                case "of units":
                    $colName="total_quantity";
                break;
                case "total sales":
                    $colName="total_sales";
                break;
                case  "total profits":
                    $colName="total_profits";
                break;
                default:
                    $colName="";
                break;    
                
            }
            if($colName!="" && ($_REQUEST["sortDirection"]=="asc" || $_REQUEST["sortDirection"]=="desc")){
                $sort=" ORDER BY $colName {$_REQUEST["sortDirection"]} ";
            }
            
            
        }
        $qry.=$sort;
    if(!isset($_REQUEST["pageNo"])){
	       $_REQUEST["pageNo"]="0";
	   }
       else{
        if(intval($_REQUEST["pageNo"])>=1)
         $_REQUEST["pageNo"]=(intval($_REQUEST["pageNo"])-1)*$this->pageSize;
         else
         $_REQUEST["pageNo"]="0";
       }
       $qry.=" limit ?,{$this->pageSize}";
       $arrStatements["s3"]=intval($_REQUEST["pageNo"])*$this->pageSize;    
                        
       $res=$this->db->query($qry,$arrStatements);
       $getTeamScoreBoard["result"]=$res->result_array();
       //var_dump($this->config);
	   return json_encode($getTeamScoreBoard);
    }
  public function viewTeamMemberSales(){
    	   if(!isset($_REQUEST["pageNo"])){
	       $_REQUEST["pageNo"]="0";
	   }
       else{
         if(intval($_REQUEST["pageNo"])>=1)
         $_REQUEST["pageNo"]=(intval($_REQUEST["pageNo"])-1)*$this->pageSize;
         else
         $_REQUEST["pageNo"]="0";
       }
       $res=$this->db->query("SELECT count(1) as cntPages from scp_sales_order_items WHERE referrer_user_id=?",array("s"=>$_REQUEST["id"]));
        $row=$res->result_array();
        $pages=ceil($row[0]["cntPages"]/$this->pageSize);
        $viewTeamMemberSales["max_page"]=$pages;
        $sort="";
       if(!isset($_REQUEST["sortColumn"]) || !isset($_REQUEST["sortDirection"])){
            $sort="";
        }
        else{
            $colName="";
            switch(strtolower($_REQUEST["sortColumn"])){
                case "date":
                    $colName="scp_sales_orders.date_created";
                break;
                case "supporter":
                    $colName="scp_users.name";
                break;
                case "order":
                    $colName="scp_sales_orders.order_number";
                break;
                case "of units":
                    $colName="scp_sales_order_items.quantity";
                break;
                case "total sale":
                    $colName="scp_sales_order_items.sale_price";
                break;
                case  "profit":
                    $colName="scp_sales_order_items.campaign_profit";
                break;
                case "status":
                    $colName="scp_sales_orders.order_status";
                break;
                default:
                    $colName="";
                break;    
                
            }
            if($colName!="" && ($_REQUEST["sortDirection"]=="asc" || $_REQUEST["sortDirection"]=="desc")){
                $sort=" ORDER BY $colName {$_REQUEST["sortDirection"]} ";
            }
            
            
        }
        $where="";
        if(!isset($_REQUEST["whereColumn"]) || !isset($_REQUEST["whereText"])){
            $where="";
        }
        else{
            $colName="";
            switch(strtolower($_REQUEST["whereColumn"])){
                case "supporter name":
                    $colName="scp_users.name";
                break;
                case "order":
                    $colName="scp_sales_orders.order_number";
                break;
                default:
                    $colName="";
                break;    
                
            }
            if($colName!=""){
                $where=" and $colName LIKE ?";
            }
            
            
        }
        $statements["s1"]=$_REQUEST["id"];
        
        if($where!="" && $_REQUEST["whereText"]!=""){
            $statements["s2"]='%'.$_REQUEST["whereText"].'%';
        }
        $statements["s3"]=intval($_REQUEST["pageNo"]);
	       $res=$this->db->query("SELECT 
                                     scp_sales_orders.date_created,
                                     scp_users.name,
                                     scp_sales_orders.order_number,
                                     scp_sales_order_items.quantity,
                                     scp_sales_order_items.sale_price,
                                     scp_sales_order_items.campaign_profit,
                                     scp_sales_orders.order_status
                              FROM scp_sales_order_items 
                              JOIN scp_sales_orders on scp_sales_orders.order_id=scp_sales_order_items.order_id 
                              JOIN scp_users on scp_sales_order_items.referrer_user_id=scp_users.id
                              WHERE scp_sales_order_items.referrer_user_id=?
                              $where
                              $sort
                              limit ?,{$this->pageSize};",$statements);
    
       $viewTeamMemberSales["result"]=$res->result_array();
	   return json_encode($viewTeamMemberSales);
  }
  public function ViewJoinedFundRaisers(){
       if(!isset($_REQUEST["pageNo"])){
	       $_REQUEST["pageNo"]="0";
	   }
       else{
         if(intval($_REQUEST["pageNo"])>=1)
         $_REQUEST["pageNo"]=(intval($_REQUEST["pageNo"])-1)*$this->pageSize;
         else
         $_REQUEST["pageNo"]="0";
       }
              $sort="";
       if(!isset($_REQUEST["sortColumn"]) || !isset($_REQUEST["sortDirection"])){
            $sort="";
        }
        else{
            $colName="";
            switch(strtolower($_REQUEST["sortColumn"])){
                case "fundraiser name":
                    $colName="scp_campaigns.campaign_title";
                break;
                case "start date":
                    $colName="scp_campaigns.start_date";
                break;
                case "end date":
                    $colName="scp_campaigns.end_date";
                break;
                case "of units":
                    $colName="total_quantity";
                break;
                case "total sales":
                    $colName="total_sales";
                break;
                case  "total profits":
                    $colName="total_profits";
                break;
                default:
                    $colName="";
                break;    
                
            }
            if($colName!="" && ($_REQUEST["sortDirection"]=="asc" || $_REQUEST["sortDirection"]=="desc")){
                $sort=" ORDER BY $colName {$_REQUEST["sortDirection"]} ";
            }
        }
       $res=$this->db->query("SELECT count(1) as cntPages from scp_campaign_members WHERE user_id=?",array("s"=>$_REQUEST["id"]));
        $row=$res->result_array();
        $pages=ceil($row[0]["cntPages"]/$this->pageSize);
        $viewJoinedFundraisers["max_page"]=$pages;
        
	       $res=$this->db->query("SELECT 
                                     scp_campaigns.campaign_id,
                                     scp_campaigns.campaign_title,
                                     scp_campaigns.campaign_url,
                                     scp_campaigns.campaign_status,
                                     scp_campaigns.start_date,
                                     scp_campaigns.end_date,
                                     sum(scp_sales_order_items.total_quantity) as total_quantity,
                                     sum(scp_sales_order_items.total_sales) as total_sales,
                                     concat(sum(scp_sales_order_items.total_profits),'/',scp_campaigns.campaign_goal) as total_profits
                              FROM scp_campaigns 
                              JOIN scp_campaign_members on scp_campaign_members.campaign_id=scp_campaigns.campaign_id 
                               JOIN scp_campaign_products on scp_campaign_products.campaign_id=scp_campaigns.campaign_id 
                              JOIN (select product_id,sum(quantity) as total_quantity ,sum(sale_price) as total_sales,sum(campaign_profit) as total_profits from scp_sales_order_items group by product_id) as scp_sales_order_items on scp_campaign_products.product_id=scp_sales_order_items.product_id
                              WHERE scp_campaign_members.user_id=?
                              GROUP BY scp_campaigns.campaign_id,
                                     scp_campaigns.campaign_title,
                                     scp_campaigns.campaign_url,
                                     scp_campaigns.campaign_status,
                                     scp_campaigns.start_date,
                                     scp_campaigns.end_date,
                                     scp_campaigns.campaign_goal
                              $sort
                              limit ?,{$this->pageSize};",array("s1"=>$_REQUEST["id"],"s2"=>intval($_REQUEST["pageNo"])));
    
       $viewJoinedFundraisers["result"]=$res->result_array();
       //var_dump($this->config);
	   return json_encode($viewJoinedFundraisers);
  }
     public function get_all_payouts(){
        $this->db->select("scp_payouts.*, scp_campaigns.campaign_title, scp_campaigns.end_date, scp_campaigns.campaign_status as camp_status");
        $this->db->join( 'scp_campaigns', 'scp_payouts.campaign_id = scp_campaigns.campaign_id ', 'LEFT' );
        $this->db->JOIN('scp_campaign_payee', 'scp_campaign_payee.campaign_id = scp_campaigns.campaign_id');
        $this->db->JOIN('scp_payee_details', 'scp_payee_details.id = scp_campaign_payee.payee_id');
        return $this->db->get(" scp_payouts")->result();
    }

      public function getSalesOrderDetails(){
        if(!isset($_REQUEST["pageNo"])){
	       $_REQUEST["pageNo"]="0";
	   }
       else{
         if(intval($_REQUEST["pageNo"])>=1)
         $_REQUEST["pageNo"]=(intval($_REQUEST["pageNo"])-1)*$this->pageSize;
         else
         $_REQUEST["pageNo"]="0";
       }
                  $sort="";
       if(!isset($_REQUEST["sortColumn"]) || !isset($_REQUEST["sortDirection"])){
            $sort="";
        }
        else{
            $colName="";
            switch(strtolower($_REQUEST["sortColumn"])){
                case "date":
                    $colName="scp_sales_orders.date_created";
                break;
                case "referrence":
                    $colName="scp_sales_orders.order_number";
                break;
                case "of items":
                    $colName="total_quantity";
                break;
                case "total price":
                    $colName="total_sales";
                break;
                default:
                    $colName="";
                break;    
                
            }
            if($colName!="" && ($_REQUEST["sortDirection"]=="asc" || $_REQUEST["sortDirection"]=="desc")){
                $sort=" ORDER BY $colName {$_REQUEST["sortDirection"]} ";
            }
        }
       
         $res=$this->db->query("SELECT count(1) as cntPages from scp_sales_orders WHERE customer_id=?",array("s"=>$_REQUEST["id"]));
        $row=$res->result_array();
        $pages=ceil($row[0]["cntPages"]/$this->pageSize);
        $getSalesOrderDetails["max_page"]=$pages;
        
        $res=$this->db->query("SELECT scp_sales_orders.date_created,scp_sales_orders.order_number,sum(scp_sales_order_items.quantity) as total_quantity,sum(scp_sales_order_items.price) as total_price,scp_sales_orders.order_status  
                               FROM scp_sales_orders 
                               JOIN scp_sales_order_items on scp_sales_order_items.order_id=scp_sales_orders.order_id
                               WHERE customer_id=?
                               group by scp_sales_orders.date_created,scp_sales_orders.order_number,scp_sales_orders.order_status
                                $sort
                               limit ?,{$this->pageSize};",
                               array("s1"=>$_REQUEST["id"],"s2"=>intval($_REQUEST["pageNo"])));
        $getSalesOrderDetails["result"]=$res->result_array();
        return json_encode($getSalesOrderDetails);
  }
}