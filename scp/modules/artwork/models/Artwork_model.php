<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Artwork_model extends CI_Model {

	function __construct()
	{

		parent::__construct();

	}

	/*
	 * This function loads all the artworks from the database based on the parameters set
	 *
	 * @param $query about this param
	 * @param $offset sets the index on where to start the return of records that matched the query
	 * @param $limit sets the limit of rows that will be fetched from the database
	 * @return Array Result from Database
	 */
	public function searchArtworks( $limit = 30, $offset = 0, $searchType = 0, $keyword = "" )
	{

		/*
		 * Get these fields from the query
		 * template id
		 * display name
		 * thumbnail url
		 * hex value ( background color )
		 * parent template id
		 */

		$this->db->select( "scp_design_templates.template_id, scp_design_templates.display_name, scp_design_templates.thumbnail, scp_design_templates.tags, IFNULL(parent_color.hex_value,scp_shirt_colors.hex_value) AS hex_value, IFNULL( scp_design_template_children.parent_template_id, scp_design_templates.template_id ) AS 'parent_id'" );

		// join this table to get also the child artwork variations
		$this->db->join("scp_design_template_children", "scp_design_template_children.template_id = scp_design_templates.template_id", "left");

		// join this table to get the back ground color set for the artwork
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_design_templates.shirt_color_id", "left");

		// load artwork background color the same as the parent
		$this->db->join("scp_design_templates AS parent_bg_color", "parent_bg_color.template_id = scp_design_template_children.parent_template_id", "left");
		$this->db->join("scp_shirt_colors AS parent_color", "parent_color.color_id = parent_bg_color.shirt_color_id", "left");

		// $this->db->where( array( "scp_design_templates.template_id" => $id, "scp_design_templates.is_active" => 1 ) );
		$this->db->where( array( "scp_design_templates.is_active" => 1 ) );
		// TODO: update the query to show only the Approved UAT Passed Artworks

		$this->db->where( "scp_design_templates.display_name !=", "" );

		if ( $searchType == 1 || $searchType == 2 )
		{
			$this->db->like( "scp_design_templates.tags", $keyword );
		}

		if ( $searchType == 3 )
		{
			$this->db->select( "SUM(scp_sales_order_items.quantity) AS quantity" );

			// join this tables to get the most units sold over the previous month. ( Most Popular Search )
			$this->db->join("scp_sales_order_item_options", "scp_sales_order_item_options.template_id = scp_design_templates.template_id");
			$this->db->join("scp_sales_order_items", "scp_sales_order_items.order_items_id = scp_sales_order_item_options.order_items_id");

			$this->db->group_by( "scp_design_templates.template_id" );
			$this->db->order_by( "quantity", "desc" );
		}

		if ( $searchType == 4 )
		{
			$this->db->order_by( "scp_design_templates.date_created", "desc" );
		}

		$this->db->order_by( "scp_design_templates.date_created", "desc" );

		$this->db->limit( $limit );

		$result = $this->db->get( "scp_design_templates" )->result();

		return $result;

	}

	/*
	 * This function loads all the categories from the database based on the parameters set
	 *
	 * @param $query about this param
	 * @param $offset sets the index on where to start the return of records that matched the query
	 * @param $limit sets the limit of rows that will be fetched from the database
	 * @return Array Result from Database
	 */
	public function searchArtworkCategories( $offset = 0, $limit = 10 )
	{

		/*
		 * Get these fields from the query
		 * category id
		 * category name
		 * category code
		 * parent id
		 */

		$this->db->select( "scp_design_template_categories.id, scp_design_template_categories.name, scp_design_template_categories.code, scp_design_template_categories.parent_id" );

		$this->db->where( array( "type" => "CAT", "is_active" => 1 ) );

		$result = $this->db->get( "scp_design_template_categories" )->result();

		return $result;

	}

	public function fetch( $id )
	{
		$this->db->where('template_id', $id);

		return $this->db->get('scp_design_templates')->row();
	}

	public function get_artwork_colors( $primary = true )
	{
		if ( $primary ) {
			$this->db->where( array( "scp_design_template_colors.color_group" => 1 ) );
		} else {
			$this->db->where( array( "scp_design_template_colors.color_group" => 0 ) );
		}

		$result = $this->db->get("scp_design_template_colors")->result();

		return $result;
	}

	public function get_template_kernings( $template_id = 0 )
	{
		$this->db->order_by( 'kerning_length', 'desc' );

		$result = $this->db->get_where( 'scp_design_template_children', array('parent_template_id' => $template_id, 'child_type' => 'KRNG') )->result();

		return $result;
	}

	public function get_artwork_management_settings( $template_id = 0, $data = array(), $design_location = 'FRNT' )
	{
		if ( count( $data ) > 0 )
		{
			foreach ( $data as $field )
			{
				$this->db->select( $field );
			}
		}
		else
		{
			$this->db->select( "*" );
		}

		// load artwork position settings
		$this->db->join("scp_design_location", "scp_design_location.design_position_id = scp_design_template_pairs.design_position_id", "left");
		$this->db->join("scp_design_template_position_settings", "scp_design_template_position_settings.template_pair_id = scp_design_template_pairs.id", "left");

		$this->db->where( array( "scp_design_template_pairs.template_id" => $template_id, "scp_design_location.location_code" => $design_location, "scp_design_template_position_settings.is_default" => 1 ) );
		// $this->db->where( array( "scp_design_template_position_settings.is_default" => 1 ) );

		$result = $this->db->get("scp_design_template_pairs")->result();

		return $result;
	}

	public function get_artwork_addons( $template_id, $addon_type )
	{
		$this->db->select( "scp_design_template_addon_types.addon_type_code, scp_design_template_addon_types.addon_type_name, scp_design_template_addons.addon_code, scp_design_template_addons.category, scp_design_template_addons.addon_name, scp_design_template_addons.thumbnail" );
		$this->db->join("scp_design_templates", "scp_design_templates.template_id = scp_design_template_addon_settings.template_id");
		$this->db->join("scp_design_template_addons", "scp_design_template_addons.id = scp_design_template_addon_settings.addon_id");
		$this->db->join("scp_design_template_addon_types", "scp_design_template_addon_types.id = scp_design_template_addons.addon_type_id");

		$this->db->where( array( "scp_design_templates.template_id" => $template_id, "scp_design_template_addons.is_active" => 1 ) );

		if ( strlen( $addon_type ) > 0 ) {
			$this->db->where( "scp_design_template_addon_types.addon_type_code", $addon_type);
		}
			$this->db->order_by("scp_design_template_addons.category", "ASC");	
		$result = $this->db->get("scp_design_template_addon_settings")->result();

		return $result;
	}

	public function get_artwork_variations( $template_id, $data = array() )
	{
		if ( count( $data ) > 0 )
		{
			foreach ( $data as $field )
			{
				$this->db->select( $field );
			}
		}
		else
		{
			$this->db->select( "scp_design_templates.template_id, scp_design_templates.properties, scp_design_templates.display_name, scp_design_templates.tags, scp_design_templates.thumbnail" );
		}

		$this->db->join("scp_design_template_children", "scp_design_template_children.template_id = scp_design_templates.template_id");

		$this->db->where( array( "scp_design_templates.is_active" => 1, "scp_design_template_children.parent_template_id" => $template_id, "scp_design_template_children.child_type" => "VRTN" ) );

		$this->db->order_by("scp_design_templates.display_name");

		$result = $this->db->get("scp_design_templates")->result();

		return $result;
	}

	public function get_artwork_property( $template_id, $data = array() )
	{
		if ( count( $data ) > 0 )
		{
			foreach ( $data as $field )
			{
				$this->db->select( $field );
			}
		}
		else
		{
			$this->db->select( "scp_design_templates.template_id, scp_design_templates.properties, scp_design_templates.display_name, scp_design_templates.tags, scp_design_templates.thumbnail" );
		}

		$this->db->where( array( "scp_design_templates.is_active" => 1, "scp_design_templates.template_id" => $template_id ) );

		$this->db->order_by("scp_design_templates.display_name");

		$result = $this->db->get("scp_design_templates")->result();

		return $result;
	}

	private function vector_save_template( $templateArray )
	{
		$resultdata = '';
		$url = 'http://69.43.181.56/vector-engine-v4/api/pv/design/save';
		$contenttype = "application/json"; // or application/xml
		
		$requestData = $templateArray;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype;charset=UTF-8") );
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$requestData);
		
		$result = curl_exec ($ch);

		curl_close ($ch);

		if ( $result !== false ) {
			$array = json_decode($result,true);
			
			foreach ($array as $getValue) {
				$resultdata = $getValue['Id'];
			}

			return $resultdata;
		} else {
			return $resultdata;
		}
	}

	/*
	 * This function loads all the artworks from the database based on the parameters set
	 *
	 * @param $query about this param
	 * @param $offset about this param
	 * @param $limit about this param
	 * @return Array Result from Database
	 */
	public function featuredFundraiser($offset = 0, $limit = 4)
	{

		/*
		 * Get these fields from the query
		 * campaign_id
		 * campaign_title
		 * product_id
		 * thumbnail1
		 * school_name
		 * count_all_ordered_qty
		 */
		
		 $row =  array('scp_campaigns.campaign_ended' => 0 );

		$this->db->select( "scp_campaigns.campaign_id, scp_campaigns.campaign_title,scp_products.product_id AS pid,scp_products.thumbnail1, 
 scp_school.school_name, SUM(scp_sales_order_items.quantity) AS count_all_ordered_qty ", FALSE);

		$this->db->join("scp_school", "scp_sales_order_items.school_id = scp_school.school_id");

		$this->db->join("scp_products", "scp_sales_order_items.product_id = scp_products.product_id");

		$this->db->join("scp_campaign_products", "scp_products.product_id = scp_campaign_products.product_id");

		$this->db->join("scp_campaigns", "scp_campaign_products.campaign_id = scp_campaigns.campaign_id");

		$this->db->group_by("scp_campaigns.campaign_id");
		$this->db->order_by("count_all_ordered_qty","desc");
	
		$this->db->limit($limit, $offset);
		//Return an object result
		$result = $this->db->get_where("scp_sales_order_items", $row)->result();

		return $result;

	}



}