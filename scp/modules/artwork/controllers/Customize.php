<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customize extends MX_Controller {

	function __construct() 
	{

		parent::__construct();

		$this->load->model(	array( 'Artwork_model', 'inventory/Inventory_model' ) );

	}

	/*
	 * Default page for customization of artworks
	 *
	 * @param None
	 * @return TGEN Page
	 */
	public function index()
	{

		$this->load->view_store( 'artwork-customize' ); 

	}

	public function load_shirt_colors()
	{
		$data['colors'] = $this->Inventory_model->get_all_shirt_colors();

		echo json_encode( $data['colors'] );
	}

	public function load_products()
	{
		$data['shirts'] = $this->Inventory_model->get_all_shirt_plans();

		echo json_encode( $data['shirts'] );
	}

	public function load_shirt_color_by_product( $id = 0 )
	{
		$data['colors'] = $this->Inventory_model->get_all_colors_by_plan_id( $id );

		echo json_encode( $data['colors'] );
	}

	public function load_specs( $param )
	{
		$data['size_specs'] = $this->Inventory_model->get_size_chart_details( $param );

		echo json_encode( $data['size_specs'] );
	}

	public function load_selectable_colors()
	{
		$data["color1"] = $this->Artwork_model->get_artwork_colors(true);
		$data["color2"] = $this->Artwork_model->get_artwork_colors(false);

		echo json_encode( $data );
	}

	public function load_kerning( $templateID = 0 )
	{
		$data['kerning'] = $this->Artwork_model->get_template_kernings( $templateID );

		echo json_encode( $data );
	}

	public function load_artwork_management_settings( $designLocation = '', $templateId = 0 )
	{
		$data["ams"] = $this->Artwork_model->get_artwork_management_settings( $templateId, $designLocation );

		echo json_encode( $data["ams"] );
	}

	public function load_artwork_addons( $templateId = 0, $addOnType = '' )
	{
		$data['addons'] = $this->Artwork_model->get_artwork_addons( $templateId, $addOnType );

		echo json_encode( $data['addons'] );
	}

	public function load_all_artwork()
	{
		$data = $this->Artwork_model->searchArtworks();

		if ( count( $data ) > 0 )
		{
			for ( $i = 0; $i < count( $data ); $i++ )
			{
				$result[$i] = $data[$i];
				//For testing only please remove once artworks are all updated
				$result[$i]->thumbnail = str_replace(" ", "%20", $result[$i]->thumbnail);
				//until here
				
				if (strpos($result[$i]->thumbnail, 'localhost') !== false) {
				   $result[$i]->thumbnail = $this->encodeArtImage($result[$i]->thumbnail);
				}
				else
				{

					$result[$i]->thumbnail = base_url()."public/images/artworks/".$result[$i]->thumbnail.".png";
				}
			}
		}

		echo json_encode( $result );
	}
}