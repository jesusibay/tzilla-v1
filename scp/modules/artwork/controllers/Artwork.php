<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Artwork extends MX_Controller {

	function __construct() 
	{

		parent::__construct();

		$this->load->model(	array( 'Artwork_model' ) );

	}

	/*
	 * Default page for artworks
	 *
	 * @param None
	 * @return Artwork Templates Page
	 */
	public function index()
	{

		$this->load->view_store( 'artwork-templates' ); 

	}
	/*
	 * Default page for Home
	 *
	 * @param None
	 * @return Home Page
	 */
	public function home()
	{

		$this->load->view_store( 'home-page' ); 

	}

	/*
	 *This function returns top 4 fundraiser
	 *all image are encoded on base64	 *
	 * @param None
	 * @return JSON Data
	 */
	public function fetchFeaturedFundraiser()
	{
		$data = $this->Artwork_model->featuredFundraiser();
		if ( count( $data ) > 0 )
		{
			for ( $i = 0; $i < count( $data ); $i++ )
			{ 
				$result[$i] = $data[$i];
				//For testing only please remove once artworks are all updated
				$data[$i]->thumbnail1 = str_replace("162.217.193.70", "localhost", $data[$i]->thumbnail1);
				//until here
				if (strpos($data[$i]->thumbnail1, 'localhost') !== false) {
				    $data[$i]->thumbnail1 = $this->encodeArtImage($data[$i]->thumbnail1);
				}
				else
				{

					$data[$i]->thumbnail1 = base_url()."public/images/artworks/".$data[$i]->thumbnail1.".png";
				}
				
			}
		}
		echo json_encode( $data );
	}

	/*
	 * This function loads all the available artwork for selection on store front
	 *
	 * @param None
	 * @return JSON Data
	 */
	public function templates(  )
	{

		$data = $this->Artwork_model->searchArtworks();

		if ( count( $data ) > 0 )
		{
			for ( $i = 0; $i < count( $data ); $i++ )
			{
				$result[$i] = $data[$i];
				//For testing only please remove once artworks are all updated
				$result[$i]->thumbnail = str_replace(" ", "%20", $result[$i]->thumbnail);
				//until here
				
				if (strpos($result[$i]->thumbnail, 'localhost') !== false) {
				   $result[$i]->thumbnail = $this->encodeArtImage($result[$i]->thumbnail);
				}
				else
				{

					$result[$i]->thumbnail = base_url()."public/images/artworks/".$result[$i]->thumbnail.".png";
				}
			}
		}

		echo json_encode( $result );

	}

	/*
	 * This function loads all the available artwork for selection on store front
	 *
	 * @param None
	 * @return JSON Data
	 */
	public function variations( $templateId = 0 )
	{
		$fields = array( 'scp_design_templates.template_id', 'scp_design_templates.thumbnail' );

		$data['variations'] = $this->Artwork_model->get_artwork_variations( $templateId, $fields );

		if ( count( $data['variations'] ) > 0 )
		{
			for ( $i = 0; $i < count( $data['variations'] ); $i++ )
			{
				$result[$i] = $data['variations'][$i];
				//For testing only please remove once artworks are all updated
				$result[$i]->thumbnail = str_replace(" ", "%20", $result[$i]->thumbnail);
				//until here
				
				if (strpos($result[$i]->thumbnail, 'localhost') !== false) {
				   $result[$i]->thumbnail = $this->encodeArtImage($result[$i]->thumbnail);
				}
				else
				{

					$result[$i]->thumbnail = base_url()."public/images/artworks/".$result[$i]->thumbnail.".png";
				}
			}
		}

		echo json_encode( $result );
	}

	/*
	 * This function loads only the properties of the selected artwork for customization
	 *
	 * @param None
	 * @return JSON Data
	 */
	public function properties( $templateId = 0 )
	{
		$fields = array( 'scp_design_templates.template_id', 'scp_design_templates.properties' );

		$data['property'] = $this->Artwork_model->get_artwork_property( $templateId, $fields );

		echo json_encode( $data['property'] );
	}

	/*
	 * This function loads only the position settings of the selected artwork for customization
	 *
	 * @param None
	 * @return JSON Data
	 */
	public function position( $templateId = 0 )
	{
		$fields = array( 'template_id', 'location_code', 'location_name', 'shirt_style_id', 'x_axis', 'y_axis', 'design_width', 'design_height', 'palletDistance', 'fa_percentage', 'tgen_percentage', 'staticWidth', 'staticHeight', 'y_axis' );

		$data['position'] = $this->Artwork_model->get_artwork_management_settings( $templateId, $fields );

		echo json_encode( $data['position'] );
	}
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
	public function image()
	{
		if ( $templateOpts = $this->input->post(null, true) )
		{
			$request = $this->_compose_request($templateOpts);
			$request = str_replace("___", "-", $request);
			// echo $request."<br>";
			$this->_call_api($request);
		}
	}

	private function _call_api( $request )	
	{
		$apiURL = "http://69.43.181.56/vector-engine-v4/api/Generate";

		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $apiURL );
		// curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
		curl_setopt( $ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array(
													'Content-Type: application/json',
													'Content-Length: ' . strlen( $request )
													) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $request );
		// curl_setopt($ch, CURLOPT_POSTFIELDS, '='.$request); 
		$content = curl_exec( $ch );
		
		curl_close( $ch );
		
		$objResponse = json_decode( $content );

		if ( isset( $objResponse->OutputUrl ) )
		{
			$data['url'] = $objResponse->OutputUrl;
			$data['image'] = $this->encodeArtImage($objResponse->OutputUrl);

			echo json_encode( $data );
		}
		else
		{
			echo "OutputUrl non-existing".$objResponse.' '.$apiURL;
		}
	}

	public function design_id( $data = array() )
	{
		$newId = "no design id generated";

		if( $post = $this->input->post(null, true) )
		{
			// lets save the shirt details first...
			$templateOpts = $post;

			$request = '['.$this->_compose_request($templateOpts).']';
			$newId = $this->vector_save_template( $request );

		}
		else if ( count( $data ) > 0 ) {
			$request = '['.$this->_compose_request( array( 'id' => $data['id'], 'AddOnType' => $data['AddOnType'], 'iconCategory' => $data['iconCategory'] ) ).']';
			$newId = $this->vector_save_template( $request );
			
			return $newId;
		}

		echo $newId;

	}

	private function _compose_request( $templateOpts )
	{
		$primary_color = ""; $secondary_color = "";
		
		if ( isset( $templateOpts['pcolor'] ) ) {
			$primary_color = $templateOpts['pcolor'];
			unset( $templateOpts['pcolor'] );
		}
		
		if ( isset( $templateOpts['scolor'] ) ) {
			$secondary_color = $templateOpts['scolor'];
			unset( $templateOpts['scolor'] );
		}

		// lets get the template data from db
		$rec = $this->Artwork_model->fetch( $templateOpts['id'] );

		unset( $templateOpts['id'] );
		unset( $templateOpts['src'] );

		$heads_directory = "E://VE API - SCP/".$templateOpts['AddOnType']."/".$templateOpts['iconCategory']."/";

		$distress_directory = "E://VE API - SCP/DISTRESS/DISTRESS/";
		$patterns_directory = "E://VE API - SCP/PATTERNS/".$templateOpts['iconCategory']."/";

		$request = new stdClass() ;

		$request->ReferenceId  = $rec->reference_id;
		$request->Id           = $rec->vector_engine_template_id;
		$request->Name         = $rec->template_name;
		$request->TemplateType = $rec->template_type;
		$request->SourceUrl    = $rec->thumbnail;
		$request->OutputUrl    = "";
		$request->Tags         = array();

		$propArr = json_decode($rec->properties);

		// we dont need the stock properties, lets update it using the user supplied data
		$ctr = 0;
		$arr_cnt = count($templateOpts);
		$properties = [];

		foreach($templateOpts as $opt_name => $opt_data)
		{
			$ctr++;
			// $option = new stdClass();

			if ( $opt_name == "DistressPattern" && strlen( $opt_data ) > 0) {

				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "SetDistress";

				$activity_values = new stdClass();
				$activity_values->href = $distress_directory.$opt_data.".pv";
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_HEAD" && strlen( $opt_data ) > 0 ) {

				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "ReplaceMascot";

				$activity_values = new stdClass();
				$activity_values->href = $heads_directory.$opt_data.".pv";
				$activity_values->X___axis = 0; // replace underscore with dash
				$activity_values->Y___axis = 0; // replace underscore with dash
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_PATTERN" && strlen( $opt_data ) > 0 ) {
				$option = new stdClass();
				$option->Name = 'MASCOT_HEAD';

				$activities = new stdClass();
				$activities->Name = "ReplaceMascot";

				$activity_values = new stdClass();
				$activity_values->href = $patterns_directory.$opt_data.".pv";
				$activity_values->X___axis = 0;
				$activity_values->Y___axis = 0;
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "MASCOT_HEAD_PATTERN" && strlen( $opt_data ) > 0 ) {
				$option = new stdClass();
				$option->Name = $opt_name;

				$activities = new stdClass();
				$activities->Name = "ReplaceBackgroundImage";

				$activity_values = new stdClass();
				$activity_values->href = $patterns_directory.$opt_data.".png";
				$activity_values->Name = $opt_data;
				$activities->KeyValues  = $activity_values;

				$option->Activities[] = $activities; 

				$properties[] = $option;

			}
			else if ( $opt_name == "SpotColor" ) {

				// do nothing it will be added at the end of the loop

			}
			else {

				if ( strlen( $opt_data ) > 0 ) {
					$option = new stdClass();
					$option->Name = $opt_name;

					$activities = new stdClass();
					$activities->Name = "ReplaceText";

					$activity_values = new stdClass();
					$activity_values->Value = $opt_data;
					$activities->KeyValues  = $activity_values;

					$option->Activities[] = $activities; 

					$properties[] = $option;
				}

			}

			// add SpotColor to the item
			if ( strlen( $primary_color ) > 0 && strlen( $secondary_color ) > 0 ) {
				if ( $ctr == $arr_cnt ) {
					$option = new stdClass();
					$option->Name = "SpotColor";

					$activities = new stdClass();
					$activities->Name = "SetColor";

					$activity_values = new stdClass();
					$activity_values->PRIMARY_COLOR = $primary_color;
					$activity_values->SECONDARY_COLOR = $secondary_color;
					$activities->KeyValues  = $activity_values;

					$option->Activities[] = $activities; 

					$properties[] = $option;
				}
			}
		}

		$request->Properties = $properties;

		return json_encode($request);
	}

	private function vector_save_template( $templateArray )
	{
		$resultdata = '';
		$url = 'http://69.43.181.56/vector-engine-v4/api/pv/design/save';
		$contenttype = "application/json"; // or application/xml
		
		$requestData = $templateArray;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: $contenttype;charset=UTF-8") );
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$requestData);
		
		$result = curl_exec ($ch);

		curl_close ($ch);

		if ( $result !== false ) {
			$array = json_decode($result,true);
			
			foreach ($array as $getValue) {
				$resultdata = $getValue['Id'];
			}

			return $resultdata;
		} else {
			return $resultdata;
		}
	}
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
// =====================================================================================================================================================
	/*
	 * This function converts the image link into a base64 image
	 *
	 * @param image url
	 * @return base64 string
	 */
	public function convertImage()
	{
		$param = $this->input->post(null, true);

		if ( strlen( $param["url"] ) > 0 )
		{
			$image = str_replace( "162.217.193.70", "localhost", $param["url"] );
			$image = str_replace( " ", "%20", $image );
			
			$result = $this->encodeArtImage( $image );

			echo $result;
		}

	}

	/*
	 * This function loads all the available categories for selection on store front
	 *
	 * @param None
	 * @return JSON Data
	 */
	public function categories(  )
	{

		$data = $this->Artwork_model->searchArtworkCategories();

		if ( count( $data ) > 0 )
		{
			for ( $i = 0; $i < count( $data ); $i++ )
			{ 
				$result[$i] = $data[$i];
			}
		}

		echo json_encode( $data );

	}

	/*
	 * This function loads all the available artwork based on what type of search is being called
	 *
	 * @param JSON Data Search parameters
	 * @return JSON Data Artworks
	 */
	public function search(  )
	{

		$param = $this->input->post(null, true);

		if ( $param["local"] == true )
		{
			$data = $this->Artwork_model->searchArtworks( 0 );

			if ( count( $data ) > 0 )
			{
				$result = [];

				for ( $i = 0; $i < count( $data ); $i++ ) {
					$result[$i]['id'] = $data[$i]->template_id;
					$result[$i]['image'] = $data[$i]->thumbnail;
					$result[$i]['encoded'] = 0;
				}

				echo json_encode( $result );
			}
			else
			{
				echo "No results found.";
			}
		}
		else
		{
			$data = $this->Artwork_model->searchArtworks( $param['limit'], $param['offset'], $param['searchType'], $param['keyword'] );

			if ( count( $data ) > 0 )
			{
				echo json_encode( $data );
			}
			else
			{
				echo "No results found.";
			}
		}

	}

}