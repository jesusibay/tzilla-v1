<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends MX_Controller {
	
	function __construct() 
	{
		parent::__construct();

		$this->load->model(	array('Inventory_model') );
	}

	public function load_shirt_colors(){
		$data['shirt_colors'] = $this->Inventory_model->get_all_shirt_colors();

		echo json_encode( $data['shirt_colors'] );
	}

	public function load_shirt_colorss($id){
		$data['shirt_colors'] = $this->Inventory_model->get_all_colors_by_plan_id($id);

		echo json_encode( $data['shirt_colors'] );
	}	

	public function load_shirt_plan_details(){
		$shirtColorIds = $this->input->post("colors");

		$data['items'] = $this->Inventory_model->get_all_shirt_item_details( $shirtColorIds );

		echo json_encode( $data['items'] );
	}

	public function load_shirt_plans_by_campaign( $campaign_id = 0 ) {
		$data['items'] = $this->Inventory_model->get_all_shirts_colors_by_campaign( $campaign_id );

		echo json_encode( $data['items'] );
	}

	public function show_styles($groupid){
		$data['shirt_styles'] = $this->Inventory_model->get_shirt_by_category($groupid); 
		echo json_encode($data['shirt_styles']);
	}

}

?>

