<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory_model extends CI_Model {

    function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    public function get_all_shirt_colors()
	{
		$result = $this->db->get("scp_shirt_colors")->result();

		return $result;
	}

	public function get_inventory_shirt_brand_by_code( $code )
	{
		$this->db->where( "brand_code", $code );

		$result = $this->db->get( "scp_inventory_shirt_brand" )->row();

		return $result;
	}

	public function get_inventory_shirt_size_details_by_param( $colorId, $code )
	{
		$this->db->where( "scp_inventory_shirt_size.shirt_color_id", $colorId );
		$this->db->where( "scp_inventory_shirt_size.shirt_size_code", $code );

		$result = $this->db->get( "scp_inventory_shirt_size" )->row();

		return $result;
	}

	public function get_item_code_by_param( $styleCode, $colorId, $code )
	{
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_color_id = scp_inventory_shirt_size.shirt_color_id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_inventory_shirt_color.shirt_style_id");
		
		$this->db->where( "scp_inventory_shirt_style.style_code", $styleCode );
		$this->db->where( "scp_inventory_shirt_size.shirt_color_id", $colorId );
		$this->db->where( "scp_inventory_shirt_size.shirt_size_code", $code );

		$result = $this->db->get( "scp_inventory_shirt_size" )->row();

		return $result;
	}

	public function get_inventory_shirt_color_details_by_param( $styleID, $code )
	{
		$this->db->where( "scp_inventory_shirt_color.shirt_style_id", $styleID );
		$this->db->where( "scp_inventory_shirt_color.shirt_color_code", $code );

		$result = $this->db->get( "scp_inventory_shirt_color" )->row();

		return $result;
	}

	public function get_inventory_shirt_style_by_code( $code )
	{
		$this->db->select("scp_shirt_alternative_group.group_name AS shirt_group_plan, scp_inventory_shirt_style.*");

		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_inventory_shirt_style.shirt_style_id");
		$this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");

		$this->db->where( "scp_inventory_shirt_style.style_code", $code );

		$result = $this->db->get( "scp_inventory_shirt_style" )->row();

		return $result;
	}

	public function get_all_shirt_plans() //only shirt has color
	{
    	$this->db->select("scp_shirt_alternative_group.*, scp_shirt_alternative_plan.shirt_style_id,scp_inventory_shirt_style.shirt_group_id as newgroup");
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
		$this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $row =  array( 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active' => 1 );
        $this->db->where( $row );
        $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
		
        $result = $this->db->get("scp_shirt_color_alternatives")->result();
        return $result;

	}	

	public function get_default_shirt() // only shirt has color default style
	{
		$this->db->limit(1);
		$this->db->select("scp_shirt_alternative_group.*, scp_shirt_alternative_plan.shirt_style_id,scp_inventory_shirt_style.shirt_group_id as newgroup");
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
		$this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $row =  array( 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active' => 1 );
        $this->db->where( $row );
        $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
		
        $result = $this->db->get("scp_shirt_color_alternatives")->row_array();
        return $result;
	}

	public function get_default_shirt_color($style)
	{
		$this->db-where("shirt_style_id", $style);
		$result = $this->db->get("scp_inventory_shirt_color")->result();
		return $result;
	}

	public function get_shirt_category(){
        return $this->db->get("scp_inventory_shirt_group")->result();
        
    }

    public function get_shirt_by_category($groupid){
        $this->db->select("scp_shirt_alternative_group.*, scp_shirt_alternative_plan.shirt_style_id,scp_inventory_shirt_style.shirt_group_id as newgroup");
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
		$this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $row =  array( 'scp_inventory_shirt_style.shirt_group_id' => $groupid );
        $this->db->where( $row );
        $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
		
        $result = $this->db->get("scp_shirt_color_alternatives")->result();
        return $result;
    }

	public function get_all_colors_by_plan_id( $planId ) {
			$this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value, scp_shirt_alternative_plan.shirt_style_id");

			$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id", "left");
			$this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id", "left");
			$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");

	        $row =  array( 'scp_shirt_alternative_group.id' => $planId, 'scp_shirt_alternative_plan.is_active' => 1 );
	        $this->db->where( $row );
	        $this->db->group_by( "scp_shirt_colors.color_code" );
	        $this->db->order_by( "scp_shirt_alternative_plan.id", 'DESC' );
			
	        $result = $this->db->get("scp_shirt_alternative_group")->result();

	        return $result;
    }

    public function get_colors_by_plan_id( $planId, $color_id ) {   	

    		$check_active = $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
			$check_active = $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
			$check_active = $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
			$check_active = $this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
	        $row =  array( 'scp_shirt_alternative_group.id' => $planId, "scp_shirt_color_alternatives.shirt_color_id" => $color_id, 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active' => 1 );
	        $check_active = $this->db->where( $row );
	        $check_active = $this->db->limit("1");
	        $check_active = $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
			
	        $check_active = $this->db->get("scp_shirt_color_alternatives")->result();
	        

	        if( count($check_active) > 0 ){ //with default
	        	$this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value, scp_shirt_alternative_plan.shirt_style_id");

	        	$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
				$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
				$this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
				$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		        $row1 =  array( 'scp_shirt_alternative_group.id' => $planId, "scp_shirt_color_alternatives.shirt_color_id" => $color_id, 'scp_shirt_alternative_plan.is_default' => 1 );
		        $this->db->where( $row1 );
				$this->db->limit("1");
				$this->db->order_by( "scp_shirt_alternative_plan.id", 'ASC' );
		        $result = $this->db->get("scp_shirt_color_alternatives")->row_array();

	        }else{
	        	$this->db->select("scp_shirt_colors.color_id, scp_shirt_colors.color_code, scp_shirt_colors.color_name, scp_shirt_colors.hex_value, scp_shirt_alternative_plan.shirt_style_id");

				$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id", "left");
				$this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id");
				$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
		        $row2 =  array( 'scp_shirt_alternative_group.id' => $planId, 'scp_shirt_alternative_plan.is_active' => 1, "scp_shirt_color_alternatives.shirt_color_id" => $color_id );
		        
		        $this->db->where( $row2 );
		        $this->db->limit("1");
		        $this->db->order_by( "scp_shirt_alternative_plan.id", 'ASC' );
	        	$result = $this->db->get("scp_shirt_alternative_group")->row_array();
	        }
	        return $result;
    }

    public function get_matrix_code( $styleid, $colorcode, $size ) {
    	$this->db->select("scp_inventory_shirt_brand.brand_code, scp_inventory_shirt_style.style_code, scp_inventory_shirt_color.shirt_color_code, scp_inventory_shirt_size.shirt_size_code");

    	$this->db->join("scp_inventory_shirt_brand", "scp_inventory_shirt_brand.shirt_brand_id = scp_inventory_shirt_style.shirt_brand_id");
    	$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_inventory_shirt_style.shirt_style_id");
    	$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id");

        $this->db->where('scp_inventory_shirt_style.shirt_style_id', $styleid);
        $this->db->where('scp_inventory_shirt_color.shirt_color_code', $colorcode);
        $this->db->where('scp_inventory_shirt_size.shirt_size_code', $size);

    	$result = $this->db->get("scp_inventory_shirt_style")->row();

		return $result;
    }

     public function get_matrix_item_code( $styleid, $colorcode, $size ) { // get_matirix_code() zhhh
	        $this->db->select(' CONCAT( scp_inventory_shirt_brand.brand_code, "-", scp_inventory_shirt_style.style_code, "-", scp_shirt_colors.color_code, "-", scp_inventory_shirt_size.shirt_size_code ) AS matrix_item_code');

		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
		$this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_inventory_shirt_style.shirt_style_id");
		$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id");
		$this->db->join("scp_kornit_printer_setup", "scp_kornit_printer_setup.kornit_printer_id = scp_inventory_shirt_color.kornit_setup_id");
		$this->db->join("scp_inventory_shirt_brand", "scp_inventory_shirt_brand.shirt_brand_id = scp_inventory_shirt_style.shirt_brand_id");
		$row =  array( 'scp_shirt_color_alternatives.shirt_alternative_id' => $styleid, 'scp_shirt_alternative_plan.is_active' => 1, "scp_shirt_colors.color_code" => $colorcode, "scp_inventory_shirt_size.shirt_size_code" => $size );
       
        $this->db->where( $row );
        $this->db->limit("1");
        $this->db->group_by( "scp_inventory_shirt_size.shirt_size_code" );
        $this->db->order_by( "scp_shirt_alternative_group.id", 'ASC' );
		$result = $this->db->get("scp_shirt_alternative_group")->row_array();
		return $result;
    }

    public function get_campaign_data( $product_id ) { // zhhh
    	 $this->db->select('scp_campaigns.*');
		$this->db->where('scp_campaign_products.product_id', $product_id);
        $this->db->join('scp_campaign_products', 'scp_campaign_products.campaign_id = scp_campaigns.campaign_id'); 
        return $this->db->get('scp_campaigns')->row_array();
    }

    public function get_campaign_price( $product_id, $plan_id ) { // zhhh campaign product selling price
    	$this->db->select('scp_product_templates.template_id, scp_product_templates.product_id, scp_product_settings.shirt_style_id as plan_id, scp_product_settings.selling_price');
		$row =  array( 'scp_product_templates.product_id' => $product_id, "scp_product_settings.shirt_style_id" => $plan_id );
        $this->db->join('scp_product_templates', 'scp_product_templates.product_id = scp_products.product_id'); 
        $this->db->join('scp_product_settings', 'scp_product_settings.product_template_id = scp_product_templates.product_template_id'); 
        $this->db->where( $row );        
        $this->db->limit("1");
        $this->db->order_by( "scp_products.product_id", 'ASC' );
        $data = $this->db->get('scp_products')->row_array();
        return $data;
    }
    

	public function get_all_shirts_colors_by_campaign( $campaign_id )
	{
		$this->db->select("scp_shirt_alternative_group.id AS shirt_plan_id, scp_shirt_alternative_group.group_name AS shirt_plan_name, scp_inventory_shirt_style.style_code AS shirt_style_code, scp_inventory_shirt_size.retail_price AS retail, scp_inventory_shirt_size.wholesale_price AS wholesale, scp_inventory_shirt_size.shirt_size_code, scp_inventory_shirt_color.generic_color_id AS generic_color");

		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_inventory_shirt_style.shirt_style_id", "left");
		$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id", "left");

		$result = $this->db->get("scp_shirt_alternative_group")->result();

		return $result;
	}

	public function get_all_shirt_item_details( $param = "" )
	{
		$this->db->select("scp_shirt_alternative_group.id AS shirt_plan_id, 
			scp_shirt_alternative_group.group_name AS shirt_plan_name, 
			scp_inventory_shirt_style.shirt_style_id,
			scp_inventory_shirt_style.style_code AS shirt_style_code,
			scp_inventory_shirt_style.style_description, 
			inventory_shirt_size.retail_price AS retail, 
			inventory_shirt_size.wholesale_price AS wholesale, 
			inventory_shirt_size.shirt_size_code, 
			inventory_shirt_size.shirt_size_data,
			inventory_shirt_size.shirt_size_name,
			scp_inventory_shirt_group.shirt_group_id,
			scp_inventory_shirt_color.generic_color_id AS generic_color, 
			inventory_shirt_size.shirt_size_order");

		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_group", "scp_inventory_shirt_group.shirt_group_id = scp_inventory_shirt_style.shirt_group_id", "left");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id", "left");
		$this->db->join("( SELECT retail_price, wholesale_price, shirt_size_code, shirt_size_data, shirt_size_name, shirt_size_order, shirt_color_id, is_active FROM scp_inventory_shirt_size ) AS inventory_shirt_size", "inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id", "left");
		
		 $row =  array( 'scp_shirt_alternative_plan.is_active' => 1,
        				'inventory_shirt_size.is_active' => 1,
        				'inventory_shirt_size.retail_price >' => 0,
						'inventory_shirt_size.wholesale_price >' => 0 );

        $this->db->where( $row );

        if ( strlen( $param ) > 0 ) {
        	$this->db->where_in( 'scp_inventory_shirt_color.generic_color_id', explode( ",", $param ) );
        }

		$result = $this->db->get("scp_shirt_alternative_group")->result();
		return $result;		
	}

	public function get_all_shirt_item_sizes_default( $plan_id, $color_id )
	{
		$query = $this->db->join('scp_inventory_shirt_style', 'scp_inventory_shirt_style.shirt_style_id = scp_inventory_shirt_color.shirt_style_id'); 
		$query = $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_inventory_shirt_style.shirt_style_id");       
        $query = $this->db->join('scp_shirt_colors', 'scp_shirt_colors.color_id = scp_inventory_shirt_color.generic_color_id');
        $query = $this->db->get_where('scp_inventory_shirt_color', array( 'scp_shirt_alternative_plan.shirt_alternative_group_id' => $plan_id, 'scp_inventory_shirt_color.generic_color_id' => $color_id  ))->result();
      
		$arr_colors = Array();
        foreach ($query as $key => $val) {
             $arr_colors[] .= $val->shirt_color_id;            
        }

		$check_active = $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
		$check_active = $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_shirt_color_alternatives.shirt_alternative_id");
		$check_active = $this->db->join("scp_shirt_alternative_group", "scp_shirt_alternative_group.id = scp_shirt_alternative_plan.shirt_alternative_group_id");
		$check_active = $this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
        $row =  array( 'scp_shirt_alternative_group.id' => $plan_id, "scp_shirt_color_alternatives.shirt_color_id" => $color_id, 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active' => 1 );
        $check_active = $this->db->where( $row );
        $check_active = $this->db->limit("1");
        $check_active = $this->db->group_by("scp_shirt_alternative_plan.shirt_alternative_group_id");
		
        $check_active = $this->db->get("scp_shirt_color_alternatives")->result();
        

        if( count($check_active) > 0 ){ //with default
        	$this->db->select( "scp_shirt_alternative_plan.shirt_alternative_group_id, scp_inventory_shirt_size.shirt_size_code, scp_inventory_shirt_size.is_active as active_size, scp_shirt_alternative_plan.shirt_style_id, scp_shirt_alternative_plan.shirt_alternative_group_id,scp_inventory_shirt_color.shirt_color_id, scp_inventory_shirt_color.generic_color_id, scp_shirt_alternative_plan.is_default, scp_inventory_shirt_style.style_code" );
			$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
			$this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id");
			$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
			$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_inventory_shirt_style.shirt_style_id", "left");
			$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id", "left");
			$this->db->join("scp_inventory_shirt_brand", "scp_inventory_shirt_brand.shirt_brand_id = scp_inventory_shirt_style.shirt_brand_id");
			$row =  array( 'scp_shirt_alternative_plan.is_default' => 1, 'scp_shirt_alternative_plan.is_active' => 1, 'scp_inventory_shirt_size.is_active' => 1, 'scp_inventory_shirt_size.retail_price >' => 0, 'scp_inventory_shirt_size.wholesale_price >' => 0  );
	        $this->db->where( $row );
	        $this->db->where_in('scp_inventory_shirt_size.shirt_color_id', $arr_colors);	
	        $this->db->group_by( 'scp_inventory_shirt_size.shirt_size_code' );	
	        $this->db->order_by( "scp_shirt_alternative_plan.shirt_style_id", 'ASC' );	
	        
			$results = $this->db->get("scp_shirt_alternative_group")->result();

        }else{
        	$results = "";
        }
        return $results;
	}

	public function get_all_shirt_item_sizes( $plan_id, $color_id, $arr )
	{
		$query = $this->db->join('scp_inventory_shirt_style', 'scp_inventory_shirt_style.shirt_style_id = scp_inventory_shirt_color.shirt_style_id'); 
		$query = $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_style_id = scp_inventory_shirt_style.shirt_style_id");       
        $query = $this->db->join('scp_shirt_colors', 'scp_shirt_colors.color_id = scp_inventory_shirt_color.generic_color_id');

        $query = $this->db->get_where('scp_inventory_shirt_color', array( 'scp_shirt_alternative_plan.shirt_alternative_group_id' => $plan_id, 'scp_inventory_shirt_color.generic_color_id' => $color_id  ))->result();
      
		$arr_colors = Array();
        foreach ($query as $key => $val) {
             $arr_colors[] .= $val->shirt_color_id;            
        }    

	     $check_active = $this->db->select("scp_shirt_colors.color_id, scp_shirt_alternative_plan.shirt_style_id");
		 $check_active = $this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id", "left");
		 $check_active = $this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id");
		 $check_active = $this->db->join("scp_shirt_colors", "scp_shirt_colors.color_id = scp_shirt_color_alternatives.shirt_color_id");
	     $row2 =  array( 'scp_shirt_alternative_group.id' => $plan_id, 'scp_shirt_alternative_plan.is_active' => 1, "scp_shirt_color_alternatives.shirt_color_id" => $color_id );
	    
	     $check_active =  $this->db->where( $row2 );
	     $check_active = $this->db->limit("1");
	     $check_active = $this->db->order_by( "scp_shirt_alternative_plan.id", 'ASC' );
		 $check_active = $this->db->get("scp_shirt_alternative_group")->row_array();
    	

    	$this->db->select( "scp_shirt_alternative_plan.shirt_alternative_group_id, scp_inventory_shirt_size.shirt_size_code, scp_inventory_shirt_size.is_active as active_size, scp_shirt_alternative_plan.shirt_style_id, scp_shirt_alternative_plan.shirt_alternative_group_id,scp_inventory_shirt_color.shirt_color_id, scp_inventory_shirt_color.generic_color_id, scp_inventory_shirt_style.style_code" );
		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
		$this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_inventory_shirt_style.shirt_style_id", "left");
		$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id", "left");
		$this->db->join("scp_inventory_shirt_brand", "scp_inventory_shirt_brand.shirt_brand_id = scp_inventory_shirt_style.shirt_brand_id");
		$row =  array( 'scp_shirt_alternative_plan.is_default' => 0, 'scp_shirt_alternative_plan.is_active' => 1, 'scp_inventory_shirt_size.is_active' => 1, 'scp_inventory_shirt_size.retail_price >' => 0, 'scp_inventory_shirt_size.wholesale_price >' => 0  );
        $this->db->where( $row );
        $this->db->where_in('scp_inventory_shirt_size.shirt_color_id', $arr_colors);
        $this->db->where_in('scp_shirt_alternative_plan.shirt_style_id', $check_active['shirt_style_id']);
        $this->db->where_not_in('scp_inventory_shirt_size.shirt_size_code', $arr);
        $this->db->group_by( 'scp_inventory_shirt_size.shirt_size_code' );	
        $this->db->order_by( "scp_shirt_alternative_plan.shirt_style_id", 'ASC' );	
        
		$results = $this->db->get("scp_shirt_alternative_group")->result();
       
        return $results;

	}

	public function get_all_inventory_list( $param = "", $styleid = 0 ) {
		$this->db->select('
							scp_shirt_alternative_group.id AS shirt_plan_id,
							scp_shirt_alternative_plan.shirt_style_id,
							scp_inventory_shirt_size.shirt_size_code,
							scp_inventory_shirt_size.retail_price,
							scp_inventory_shirt_size.wholesale_price,
							scp_inventory_shirt_size.item_code,
							scp_inventory_shirt_size.item_class,
							scp_inventory_shirt_color.shirt_color_code,
							scp_inventory_shirt_color.color_name,
							scp_kornit_printer_setup.kornit_printer_id,
							scp_inventory_shirt_style.style_code,
							scp_inventory_shirt_style.garment_type,
							scp_inventory_shirt_brand.brand_code,
							CONCAT( scp_inventory_shirt_brand.brand_code, "-", scp_inventory_shirt_style.style_code, "-", scp_inventory_shirt_color.shirt_color_code, "-", scp_inventory_shirt_size.shirt_size_code ) AS matrix_item_code
						');

		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
		$this->db->join("scp_shirt_color_alternatives", "scp_shirt_color_alternatives.shirt_alternative_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_inventory_shirt_style.shirt_style_id", "left");
		$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id", "left");
		$this->db->join("scp_kornit_printer_setup", "scp_kornit_printer_setup.kornit_printer_id = scp_inventory_shirt_color.kornit_setup_id");
		$this->db->join("scp_inventory_shirt_brand", "scp_inventory_shirt_brand.shirt_brand_id = scp_inventory_shirt_style.shirt_brand_id");

        $this->db->where( 'scp_shirt_alternative_plan.is_active', 1 );
        $this->db->where( 'scp_inventory_shirt_size.retail_price >', 0 );
        $this->db->where( 'scp_inventory_shirt_size.wholesale_price >', 0 );
        $this->db->where( 'scp_inventory_shirt_size.item_code !=', "" );

        if ( $styleid != 0 ) {
        	$this->db->where( 'scp_shirt_alternative_group.id', $styleid );
        }

        if ( strlen( $param ) > 0 ) {
        	$this->db->where_in( 'scp_inventory_shirt_color.generic_color_id', explode( ",", $param ) );
        }

        $this->db->group_by( "scp_inventory_shirt_size.item_code" );
        $this->db->order_by( "scp_shirt_alternative_group.id", 'ASC' );

		$result = $this->db->get("scp_shirt_alternative_group")->result();

		return $result;
	}

	public function get_size_chart_details( $shirt_group_id = 0, $inventory_group_id = 0 ) {
		$this->db->select('
							scp_shirt_alternative_group.id,
							scp_inventory_shirt_style.style_code,
							scp_inventory_shirt_size.shirt_size_order,
							scp_inventory_shirt_size.shirt_size_code,
							scp_shirt_alternative_group.group_name AS style_name,
							scp_shirt_alternative_group.front_image_link AS thumbnail,
							scp_inventory_shirt_style.shirt_style_id,
							scp_inventory_shirt_style.style_description,
							scp_inventory_shirt_color.color_name,
							scp_inventory_shirt_size.shirt_size_data,
							scp_inventory_shirt_size.shirt_size_name,
							scp_inventory_shirt_group.shirt_group_id
						');

		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_group", "scp_inventory_shirt_group.shirt_group_id = scp_inventory_shirt_style.shirt_group_id", "left");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id", "left");
		$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id", "left");

		$this->db->where( array("scp_shirt_alternative_plan.is_active" => 1) );

		if ( $shirt_group_id > 0 ) {
			$this->db->where( "scp_shirt_alternative_group.id", $shirt_group_id );
		}
		$this->db->group_by('scp_shirt_alternative_group.id, scp_inventory_shirt_size.shirt_size_code');
		$this->db->order_by('SUBSTRING( scp_shirt_alternative_group.id FROM 1 FOR 9 ) ASC, scp_inventory_shirt_size.shirt_size_order ASC, scp_shirt_alternative_plan.is_default DESC');

		$result = $this->db->get("scp_shirt_alternative_group")->result();

		return $result;


	}

	public function get_size_chart_details_style( $style_id = 0 ) { //zh edit
		$this->db->select('
							scp_shirt_alternative_group.id,
							scp_shirt_alternative_group.group_name AS style_name,
							scp_shirt_alternative_group.front_image_link AS thumbnail,
							scp_inventory_shirt_group.shirt_group_name AS group_name,
							scp_inventory_shirt_style.shirt_style_id,
							scp_inventory_shirt_style.style_description,
							scp_inventory_shirt_size.shirt_size_order,
							scp_inventory_shirt_size.shirt_size_data,
							scp_inventory_shirt_size.shirt_size_name,
							scp_inventory_shirt_size.shirt_size_code
						');

		$this->db->join("scp_inventory_shirt_group", "scp_inventory_shirt_group.shirt_group_id = scp_shirt_alternative_group.shirt_group_id");
		$this->db->join("scp_shirt_alternative_plan", "scp_shirt_alternative_plan.shirt_alternative_group_id = scp_shirt_alternative_group.id");
		$this->db->join("scp_inventory_shirt_style", "scp_inventory_shirt_style.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id");
		$this->db->join("scp_inventory_shirt_color", "scp_inventory_shirt_color.shirt_style_id = scp_shirt_alternative_plan.shirt_style_id", "left");
		$this->db->join("scp_inventory_shirt_size", "scp_inventory_shirt_size.shirt_color_id = scp_inventory_shirt_color.shirt_color_id", "left");
		
		if ( $style_id > 0 ) {
			$this->db->where( "scp_inventory_shirt_style.shirt_style_id", $style_id );
		}

		$this->db->group_by('scp_inventory_shirt_size.shirt_size_code');
		$this->db->order_by('scp_inventory_shirt_size.shirt_size_order');

		$result = $this->db->get("scp_shirt_alternative_group")->result();

		return $result;
	}

}

?>