<?php defined('BASEPATH') OR exit('No direct script access allowed.');
/*	
	This helper function can be used to sanitize data before storing to DB 	
	to protect against malicious inputs.
	
	IMPORTANT NOTE:
	This Helper can only be used if php version is 5 or above
 */

//USE THIS FUNCTION IF YOU ARE GOING TO STORE THE DATA TO DB

//This function is for OBJECTS only use arrayString_toDB if your passing an ARRAY
if ( ! function_exists('objectString_toDB')){
   	function objectString_toDB($param = array()){
   		//SET A NEW ARRAY
		$sanitized = array();
			
		array_walk_recursive($param, function(&$value) {
			//LIST OF KEYWORDS THAT ARE CONSIDERED MALICIOUS
			$invalidKeywords = array('DROP ','TRUNCATE ','SELECT ','DELETE ','UPDATE ', 'DESC ', 'SHOW ');			
			foreach ($value as $key => $values) {
				//CHECK IF VALUE IS LISTED ON THE BANNED KEYWORDS
				if (in_array($values, $invalidKeywords)) 
				{
					//EXPLODE THE ELEMENT AND GET THE 1st
					$arr = explode(" ",$values);
					$values = $arr."_";
				}
				//Remove Newline and replace with space
				$values = str_replace("\r\n","",$values);
				//Remove the space that are 2 or more and replace it with single space only
				$values= preg_replace("/ {2,}/", " ",$values);
				//Encodes special characters to preserve its meaning when serve to client-side
				$values = htmlspecialchars($values, ENT_QUOTES);
				$values = str_replace('\\\\', '\\', $values);
				//Encode the string 
				$value->$key = urlencode($values);
			}
			
		});
		foreach ($param as $key => $value) {
			$sanitized[$key] = $value;
		}
       	return $sanitized;
   	}
}

//This functions is for passing single variables to Server-side
if ( ! function_exists('string_toDB')){
   	function string_toDB($param = ""){
   		$invalidKeywords = array('DROP ','TRUNCATE ','SELECT ','DELETE ','UPDATE ', 'DESC ', 'SHOW ');			
		
			//CHECK IF VALUE IS LISTED ON THE BANNED KEYWORDS
			if (in_array($param, $invalidKeywords)) 
			{
				//EXPLODE THE ELEMENT AND GET THE 1st
				$arr = explode(" ",$param);
				$param = $arr."_";
			}
			//Remove Newline and replace with space
			$param = str_replace("\r\n","",$param);
			//Remove the space that are 2 or more and replace it with single space only
			$param= preg_replace("/ {2,}/", " ",$param);
			//Encodes special characters to preserve its meaning when serve to client-side
			$param = htmlspecialchars($param, ENT_QUOTES);
			$param = str_replace('\\\\', '\\', $param);
			//Encode the string 
			$param = urlencode($param);		
		
       	return $param;
   	}
}

//This functions is for passing single variables to Client-side
if ( ! function_exists('db_toString')){
   	function db_toString($param = ""){
   		$invalidKeywords = array('DROP ','TRUNCATE ','SELECT ','DELETE ','UPDATE ', 'DESC ', 'SHOW ');			
		
			//CHECK IF VALUE IS LISTED ON THE BANNED KEYWORDS
			if (in_array($param, $invalidKeywords)) 
			{
				//EXPLODE THE ELEMENT AND GET THE 1st
				$arr = explode(" ",$param);
				$param = $arr."_";
			}
			//Remove Newline and replace with space
			$param = str_replace("\r\n","",$param);
			//Remove the space that are 2 or more and replace it with single space only
			$param= preg_replace("/ {2,}/", " ",$param);
			//Encodes special characters to preserve its meaning when serve to client-side
			$param = htmlspecialchars($param, ENT_QUOTES);
			$param = str_replace('\\\\', '\\', $param);
			//Encode the string 
			$param = urldecode($param);		
		
       	return $param;
   	}
}


//This function is for ARRAY only use objectString_toDB if your passing an OBJECTS
if ( ! function_exists('arrayString_toDB')){
   	function arrayString_toDB($param = array()){
   		//SET A NEW ARRAY
		$sanitized = array();
			
		array_walk_recursive($param, function(&$value) {
			
			//LIST OF KEYWORDS THAT ARE CONSIDERED MALICIOUS
			$invalidKeywords = array('DROP ','TRUNCATE ','SELECT ','DELETE ','UPDATE ', 'DESC ', 'SHOW ');			
			
				//CHECK IF VALUE IS LISTED ON THE BANNED KEYWORDS
				if (in_array($value, $invalidKeywords)) 
				{
					//EXPLODE THE ELEMENT AND GET THE 1st
					$arr = explode(" ",$value);
					$value = $arr."_";
				}
				//Remove Newline and replace with space
				$value = str_replace("\r\n","",$value);
				//Remove the space that are 2 or more and replace it with single space only
				$value= preg_replace("/ {2,}/", " ",$value);
				//Encodes special characters to preserve its meaning when serve to client-side
				$value = htmlspecialchars($value, ENT_QUOTES);
				$value = str_replace('\\\\', '\\', $value);
				//Encode the string 
				$value = urlencode($value);
			
		});
		foreach ($param as $key => $value) {
			$sanitized[$key] = $value;
		}		
       	return $sanitized;
   	}
}

//This function is for parsing OBJECTS when serving to Client-side
if ( ! function_exists('object_parseToString')){
   	function object_parseToString($param = array()){
   		//SET A NEW ARRAY
		$sanitized = array();
			
		array_walk_recursive($param, function(&$value) {
			//LIST OF KEYWORDS THAT ARE CONSIDERED MALICIOUS
			$invalidKeywords = array('DROP ','TRUNCATE ','SELECT ','DELETE ','UPDATE ', 'DESC ', 'SHOW ');			
			foreach ($value as $key => $values) {
				//CHECK IF VALUE IS LISTED ON THE BANNED KEYWORDS
				if (in_array($values, $invalidKeywords)) 
				{
					//EXPLODE THE ELEMENT AND GET THE 1st
					$arr = explode(" ",$values);
					$values = $arr."_";
				}
				//Remove Newline and replace with space
				$values = str_replace("\r\n","",$values);
				//Remove the space that are 2 or more and replace it with single space only
				$values= preg_replace("/ {2,}/", " ",$values);
				//Encodes special characters to preserve its meaning when serve to client-side
				$values = htmlspecialchars($values, ENT_QUOTES);
				$values = str_replace('\\\\', '\\', $values);
				//Encode the string 
				$value->$key = urldecode($values);
			}
			
		});
		foreach ($param as $key => $value) {
			$sanitized[$key] = $value;
		}
       	return $sanitized;
   	}
}

//This function is for parsing ARRAY when serving to Client-side
if ( ! function_exists('array_parseToString')){
   	function array_parseToString($param = array()){
   		//SET A NEW ARRAY
		$sanitized = array();
			
		array_walk_recursive($param, function(&$value) {
			//LIST OF KEYWORDS THAT ARE CONSIDERED MALICIOUS
			$invalidKeywords = array('DROP ','TRUNCATE ','SELECT ','DELETE ','UPDATE ', 'DESC ', 'SHOW ');			
			
				//CHECK IF VALUE IS LISTED ON THE BANNED KEYWORDS
				if (in_array($value, $invalidKeywords)) 
				{
					//EXPLODE THE ELEMENT AND GET THE 1st
					$arr = explode(" ",$value);
					$value = $arr."_";
				}
				//Remove Newline and replace with space
				$value = str_replace("\r\n","",$value);
				//Remove the space that are 2 or more and replace it with single space only
				$value= preg_replace("/ {2,}/", " ",$value);
				//Encodes special characters to preserve its meaning when serve to client-side
				$value = htmlspecialchars($value, ENT_QUOTES);
				$values = str_replace('\\\\', '\\', $value);
				//Encode the string 
				$value = urldecode($value);
			
			
		});
		foreach ($param as $key => $value) {
			$sanitized[$key] = $value;
		}
       	return $sanitized;
   	}
}

