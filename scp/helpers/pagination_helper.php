<?php defined('BASEPATH') OR exit('No direct script access allowed.');
if ( ! function_exists('create_pagination'))
{
	error_reporting(E_ALL);

	function create_pagination($uri, $total_rows, $limit = null, $uri_segment = 4, $full_tag_wrap = true)
	{
		$ci = & get_instance();
		$ci->load->library('pagination');
		
		$current_page = $ci->uri->segment($uri_segment, 0);
		
		$config['base_url']		= base_url().$uri;
		$config['uri_segment']	= $uri_segment;
		$config['per_page']		= $limit;
		
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div class="pagination"><ul>';
		$config['full_tag_close'] = '</ul></div>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['prev_link'] = '&laquo;';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';

		$config['next_link'] = '&raquo;';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['total_rows']	= $total_rows;
		
		$ci->pagination->initialize($config);
		$links = $ci->pagination->create_links($full_tag_wrap);
		
		$offset = $limit * ($current_page - 1);
		//avoid having a negative offset
		if ($offset < 0) $offset = 0;
		
		return array(
			'current_page' => $current_page,
			'per_page' => $limit,
			'limit' => $limit,
			'offset' => $offset,
			'links' => $links
		);
	}
}