<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('is_home')){
   function is_home($home_controller){
       //get main CodeIgniter object
       $ci =& get_instance();
       
      	$is_home = $ci->router->fetch_class() === $home_controller ? true : false;
       //print_r($ci->router->fetch_class());
       return $is_home;
   }
}
