<?php $this->load->view_store('header'); 
$this->load->helper('textparser'); ?>  
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/school-search.css');?>">

<script type="text/javascript">     
  onloadCallback = function() {
   
    gregister = grecaptcha.render('register_element', {
      'sitekey' : '6LfkVxkUAAAAAJ9P3_Kzq8T6N4Me2F8DiE4LyCGd',
      'callback' : verifyCallback
    });

    gschool_search = grecaptcha.render('school_search_element', {
      'sitekey' : '6LfkVxkUAAAAAJ9P3_Kzq8T6N4Me2F8DiE4LyCGd',
      'callback' : verifyCallback
    });

  };
</script>

<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/school-search.js').'?df='.rand(1, 99999);?>"></script>

<?php $colors = $this->Schools_model->get_all_genColors(); ?>
<div class="container-fluid search-area">
	<div class="search-area-opt"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6">
				<h1 class="white search-title">School pride starts here. Search for your school.</h1>
					<div class="form-group handler schoolname">
						<label class="hidden-lg hidden-md hidden-sm hidden-xs" for="school-name">School Name</label>
						<input type="text" name="scname" class="form-control gitalic schoolname handle_enter font-regularz" data-enter="btn-search-sch" id="schoolname" placeholder="School Name">
						
					</div>
					<div  class="form-group schoolname2">
						<div class="suggest-name" schoolid="" id="suggest-name"></div>
					</div>
					<div class="form-group handler">
						<label class="hidden-lg hidden-md hidden-sm hidden-xs"  for="state">State</label>
							<select name="state" class="form-control gitalic gray font-regularz" id="state-select">
								<option class="noval" value="">Select Your State</option>
								<?php foreach ($states as $key => $val) {?>
                                    <option data-value="<?php echo $val['state_name'];?>" value="<?php echo $val['state_code'];?>"><?php echo ucfirst($val['state_name']);?></option>
                                <?php } ?>			
							</select>
							<!-- <div id="notification-state" style="color:#FF0000; display:none;">This field is required.</div> -->
					</div>
					<div class="form-group handler">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 pad-right-zero">
								<label class="hidden-lg hidden-md hidden-sm hidden-xs" for="zip-code">Zip Code</label>
								<input type="text" class="form-control gitalic container-full handle_enter font-regularz" data-enter="btn-search-sch" id="zipcode" placeholder="Zip Code (optional)">
								
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
								<button id="btn-search-sch" class="btn btn-primary gsemibold btn-balance btn-search-sch"><i class="icon-search"></i></button>
							</div>

						</div><div id="notification-zip" style="color:#FF0000; display:none;" >Please enter state or zip code.</div>
					</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<?php if( count( $featured_campaign) > 0 ):?>
	<div class="row">
		<div class="col-lg-12">
			<h2 class="text-center gray">Featured Campaigns</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="row">
				<?php foreach (array_parseToString( $featured_campaign ) as $key => $campaign) {  
					$quantity = $this->Campaigns_model->get_all_featured_campaigns_count_sold( $campaign['campaign_url']);
					$img = (!empty($campaign['thumbnail1']))? str_replace('/200/200', "",$campaign['thumbnail1']) : str_replace('/200/200', "",$campaign['thumbnail']);?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 vertical-spacing text-center">
					<div class="sales-counter"><span class="font-medium white text-uppercase"><?php echo (!empty($quantity['quantity']))? $quantity['quantity'] : '0'; ?> Sold</span></div>
					<div class="image-box" style="background:#<?php echo $campaign['shirt_background_color']; ?>;">
						<a href="<?php echo base_url($campaign['campaign_url']);?>"><img class="image-overflow-center" src="<?php echo $img;?>/200/200" alt="" /></a>
					</div>
					<a class="shirt-name-link" href="<?php echo base_url($campaign['campaign_url']);?>"><h3 class="gsemibold font-medium title-c-height"><?php echo ucwords(ucfirst($campaign['campaign_title'] )); ?></h3>
					<div class="lbl-overflow gray" title="<?php echo db_toString( $campaign['school_name'] ); ?>"><?php echo db_toString( $campaign['school_name'] ); ?></div></a>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php endif;?>

	<?php if( count( $trending_campaign) > 0 ):?>
	<div class="row">
		<div class="col-lg-12">
			<h2 class="text-center gray">Trending Campaigns</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="row">
				<?php 
				foreach (array_parseToString( $trending_campaign ) as $key => $campaign) {  
					$quantity = $this->Campaigns_model->get_all_featured_campaigns_count_sold( $campaign['campaign_url']);
					$img = (!empty($campaign['thumbnail1']))? str_replace('/200/200', "",$campaign['thumbnail1']) : str_replace('/200/200', "",$campaign['thumbnail']); ?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 vertical-spacing text-center">
					<div class="sales-counter"><span class="font-medium white text-uppercase"><?php echo (!empty($quantity['quantity']))? $quantity['quantity'] : '0'; ?> Sold</span></div>
					<div class="image-box" style="background:#<?php echo $campaign['shirt_background_color']; ?>;">
						<a href="<?php echo base_url($campaign['campaign_url']);?>"><img class="image-overflow-center" src="<?php echo $img;?>/200/200" alt="" /></a>
					</div>
					<a class="shirt-name-link" href="<?php echo base_url($campaign['campaign_url']);?>"><h3 class="gsemibold font-medium title-c-height"><?php echo ucwords(ucfirst($campaign['campaign_title'] )); ?></h3></a>
					<div class="lbl-overflow gray" title="<?php echo db_toString( $campaign['school_name'] ); ?>"><?php echo db_toString( $campaign['school_name'] ); ?></div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php endif;?>
</div>
<div class="container-fluid featured-area">
	<div class="featured-area-opt"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 vertical-spacing">
				<h2 class="font-xlarge text-left white label-center">It's so easy to set up a t-shirt fundraiser.<br> It's fast and it's free. Give it a try now!</h2>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div id="searchz" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content login-modal-holder search-result">
      <div class="school-pop-header white gregular font-medium">
			<span class="search-count">(6)</span> Results for “<span class="search-term">Poway High School</span>”
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="school-pop-cont">
						<div class="school-pop-data">
							<a href="" class="gray-darker school-storelink">
							<div class="school-pop-holder">
								<div class="school-pop-title gsemibold font-regular"> </div>
								<p class="school-pop-desc gregular font-regular"> </p>
							</div>
							</a>
						</div>
						<div class="cant-find-holder">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<button class="orange-btn white gsemibold cant-find-btn">Can’t find your school?</button>
							</div>
						</div>
						</div>
					</div>
						
				</div>
	  </div>
    </div>
	
	
	
	
	
	<!-- Modal content-->
    <div class="modal-content login-modal-holder cant-find">
      <div class="school-pop-header white gregular font-medium">
			We just need a few details so we can research, and we’ll back to you.
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
			<div class="cantfind-cont">
			<form id="cantfindform" action="" method="POST">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<label class="cantfind-label1 blackz">Your information</label>
						<div class="form-group handler">
						  <input type="text" name="fullname" class="form-control gregular luk-required" data-name="Full Name" id="luk_fullname" placeholder="Full Name">
						</div>
						<div class="form-group handler">
						  <!-- <select class="form-control luk-required" id="luk_school_name" name="popschoolname">
							<option value="">School Name</option>
							<option value="1">Poway High school</option>
							<option value="2">Cavite High school</option>
						  </select> -->
						  <input type="text" name="popschoolname" class="form-control gregular luk-required" data-name="School Name" id="luk_school_name" placeholder="School Name">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 align-margin">
						<div class="form-group handler">
						  <input type="text" name="email" class="form-control gregular luk-required" data-name="Email" id="luk_email" placeholder="Email">
						</div>
						<div class="form-group handler">
						    <select class="form-control gregular luk-required school-state" data-name="State" id="luk_state" name="state">
						    	<option class="noval" value="">Select your State</option>
								<?php foreach ($states as $key => $val) {?>
                                    <option data-value="<?php echo $val['state_name'];?>" value="<?php echo $val['state_code'];?>"><?php echo ucfirst($val['state_name']);?></option>
                                <?php } ?>
							</select>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 align-margin">
						<div class="form-group handler">
								<input type="text" name="contact" class="form-control gregular luk-required" data-name="Contact" id="luk_contact" placeholder="Contact Number (xxx) xxx-xxxx" onkeypress="return isNumberKey(event)">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<label class="cantfind-label1 blackz">What is your particular role in your school?</label>
						<div class="form-group handler">
							  <select name="role" class="form-control gregular luk-required" data-name="Role" id="luk_role">
								<option value="">Select one</option>
								<option value="Principal">Principal</option>
								<option value="Teacher">Teacher</option>
								<option value="Student">Student</option>
							  </select>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<label class="cantfind-label1 blackz">Select Your School Colors</label>
						<div class="form-group handler">
							<div class="text-col primarycol form-control">
								<span class="selected-col-text1 gregular">Select Color 1</span><i class="icon-circle pull-right cantfind-col selected-col1"></i>
								<div class="color-primary">
									<?php 
										foreach ($colors as $key => $color_val) { 
											if($color_val->hex_value != ""){
									?> 
										<span class="col-selection" style="background:#<?php echo $color_val->hex_value;?>;" data-toggle="tooltip" data-placement="top" title="<?php echo $color_val->color_name; ?>" data-hex="<?php echo $color_val->hex_value;?>"></span>
										<?php }	}
									?> 
									<!-- <span class="col-selection" data-toggle="tooltip" data-placement="top" title="yellow" data-hex="fee500"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="green" data-hex="173819"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="blue" data-hex="001c43"></span>									
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="blue" data-hex="001c43"></span> -->
								</div>
							</div>
							<input type="hidden" class="luk-required" data-name="Color1" name="color1" id="luk_color1">
						</div>
					</div>

					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 align-margin">
						<div class="form-group handler">
							<div class="text-col secondarycol form-control">
								<span class="selected-col-text2 gregular">Select Color 2</span><i class="icon-circle pull-right cantfind-col selected-col2"></i>
								<div class="color-secondary">
									<?php 
										foreach ($colors as $key => $color_value) {
											if($color_value->hex_value != ""){
									 ?> 
										
										<span class="col-selection" style="background:#<?php echo $color_value->hex_value; ?>;" data-toggle="tooltip" data-placement="top" title="<?php echo $color_value->color_name; ?>" data-hex="<?php echo $color_value->hex_value; ?>"></span>
											
									<?php  } }
									?> 
									<!-- <span class="col-selection" data-toggle="tooltip" data-placement="top" title="yellow" data-hex="000000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="green" data-hex="00ff00"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="blue" data-hex="0000ff"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="red" data-hex="ff0000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="yellow" data-hex="000000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="green" data-hex="00ff00"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="blue" data-hex="0000ff"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="red" data-hex="ff0000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="yellow" data-hex="000000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="green" data-hex="00ff00"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="blue" data-hex="0000ff"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="red" data-hex="ff0000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="yellow" data-hex="000000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="green" data-hex="00ff00"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="blue" data-hex="0000ff"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="red" data-hex="ff0000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="yellow" data-hex="000000"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="green" data-hex="00ff00"></span>
									<span class="col-selection" data-toggle="tooltip" data-placement="top" title="blue" data-hex="0000ff"></span> -->
								</div>
							</div>
							<div class="remove-col2 pointer gsemibold font-small" style="display: none;"><span class="icon-circled-x"></span><span class="gregular font-xsmall">Remove Color</span></div>
							<input type="hidden" name="color2" class="luk-required" data-name="Color2" id="luk_color2">
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<label class="cantfind-label1 blackz">Upload your School Mascot (optional)</label>
						<div class="cantfind-upload-holder form-control text-center">
							<p class="gregular font-regular gray text-center">If you have an image of your mascot, please upload for our reference</p>
							<div class="cantfind-uploader font-regular gitalic text-center">Browse</div>
							<input class="hidden" type="file" name="userfile" id="userfile" />
							<button type="button" id="uploadButton" class="btn btn-primary gsemibold text-center upload-btn header-dropdown"><i class="icon-upload"></i></button>
							<p class="gregular font-regular gray text-center" style="padding-top: 8px;">We accept all common image<br>file types</p>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<label class="cantfind-label1 blackz">Notes</label>
						<div class="form-group">
						  <textarea class="form-control gregular luk-required" data-name="Notes" rows="9" id="luk_notes"></textarea>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div id="school_search_element"></div>
					<span class="validation-school" style="color:#FF0000;"></span>
					<!-- <div class="g-recaptcha captcha-holder" data-sitekey="6LcNAQ0UAAAAAGO6y86wcOukg9QA18t1g_W7myDZ">
							<div class="captcha-checkbox pull-left">
							  <span class="checkbox-robot"></span><span class="gsemibold font-regular gray-darker">I'm not a robot</span>
							</div>
							<img class="captcha-img img-responsive pull-right" src="<?php echo base_url('public/'.STORE.'/images/captcha.png');?>" alt="" />
							<div class="clearfix"></div>
						</div> -->
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="cantfind-submit-holder"><button class="cantfind-submit btn btn-primary gsemibold text-center">Submit</button></div>
					</div>
					
				</div>
				</form>
			</div>
	  </div>

	</div>
	
	
	
	

  </div>
</div>
</div>




<!-- Modal -->
<div id="Righton" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content login-modal-holder">
      <div class="pop-header-notxt white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 right-on-img">
					<img class="img-responsive" src="<?php echo base_url('public/'.STORE.'/images/likecircle.png');?>" style="margin:0 auto;" />
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cantfind-submit-holder">
					<label class="right-on-capt">Right On!</label>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 infosubmit-holder">
					<label class="info-submit">Your information was submitted!</label>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<div class="homepage-btn-holder"><button type="button" class="cantfind-submit btn btn-primary gsemibold text-center" data-toggle="modal" data-target="#" data-dismiss="modal">Go to homepage</button></div>
				</div>
				<div class="col-md-3"></div>
			</div>
				
	  </div>

	</div>
  </div>
</div>

<?php $this->load->view_store('footer'); ?>