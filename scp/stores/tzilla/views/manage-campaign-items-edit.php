<?php $this->load->view_store('inner-header');  ?>  
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/manage-campaign-items-edit.js').'?df='.rand(1, 99999);?>"></script>
<script src="<?php echo base_url('public/'.STORE.'/js/art-positioning.js').'?df='.rand(1, 99999);?>"></script>
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/manage-campaign.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/productsizechart.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/css/settings.css');?>">
<script type="text/javascript">
	setAssetUrl("<?php echo base_url('public/'.STORE); ?>");
	setShirtGroupId($(".design-display").attr('group-id'));
	setProductDetails( '<?php echo addslashes(json_encode( $product_details ) ); ?>');
	setAllColors( '<?php echo json_encode( $get_style_colors ); ?> ');
	setAllStyles( '<?php echo json_encode( $shirt_styles ); ?> ');
	setStyleAlternatives( '<?php echo json_encode( $shirt_alternatives ); ?> ');
	setGenColors( '<?php echo json_encode( $generic_colors ); ?> ');
	setSchoolId( '<?php echo json_encode( $school_id ); ?> ');
	setCampaignID( '<?php echo $campaign_id; ?>');

</script>


<div class="container-fluid cart-review-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
				
				<h1 class="white">Set Your Pricing</h1>
				<p class="white">Click on each design below to set pricing for each of your selected products.<br>Once complete, the <span class="gsemibold">NEXT</span> button will turn green.</p>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row review-design-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 buy-instead-btn text-left">
					<span class="gregular font-large buy-items-link buy-instead my-collection-lbl">My Collection</span>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right">
					<button class="green-btn managecamp-setup-btn white gsemibold font-small goto-preview-campaign" disabled="disabled">Save Changes</button>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="container managecamp-designlist-holder">
	<div class="row design-templates">
		
		<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 dsgn-hold bg-cont" data-parent=""> 
			<div class="design-display" data-parent="" group-id="" shirt-colors="" back-color="" data-campid="<?php echo $campaign_id; ?>">			
			
				<div class="product-details" data-parent="" data-template="" data-shirt="" data-colors="" data-price="" data-image="" ></div>
				<!-- cant touch this --> 
				<div class="managecamp-design-holder selected-design" data-id="" data-parent="" data-properties="">
					<img class="img-responsive template-design" src="" alt="" />
					<div class="managecamp-designbtn-holder onhover-editdelete" style="display:none;">
						<div class="managecamp-designbtn-holder-inner">
							<div class="managecamp-designbtn-edit"></div>
							<div class="managecamp-designbtn-remove "  data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="managecamp-do-btn-holder dsgn-hold" data-parent="">
				<div class="managecamp-designcolor-holder back-selectcolor">
					<div class="managecamp-designcolor-header gregular white font-xsmall">Set this design’s background color</br>to be previewed in your Campaign.</div>
					<div class="managecamp-designcolor-cont select-background">
						
						<div class="back-selector selected-color" data-id="" title="" data-hex="" style=""> <img class="img-responsive " src="" alt="" /> </div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- <button class="gray-btn managecamp-do-btn white glight font-xsmall text-uppercase ">Background Options</button> -->
				<button class="gray-btn managecamp-do-btn white gsemibold font-xsmall text-uppercase ">Edit Design</button>
			</div> 
		</div>
		
					
	 </div>  

		
</div> 
<div class="container">
	<div class="row review-design-section">
		<div class="container">
			
			<div class="row addmore-stylesec"> <!-- start -->
					
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 tshirtdisplay" style="display:none;">

					<div class="cart-review-item-holder item-details"  splan-id="" shirt-data="">

					
						<span class="cart-review-remove removeshirt" data-toggle="modal" data-target="#confirm" data-dismiss="modal"></span>
						<!-- <span class="cart-review-zoom" data-toggle="modal" data-target="#zoomer" data-dismiss="modal"></span> -->
						<div class="cart-review-shirt-holder tshirt-image">
							
							<img class="img-responsive img-center load-plan-shirt-image" src="" alt="" />
							<div class="design-holder"><img class="img-responsive img-center load-temp-image" src="" alt="" /></div>
						</div>
						<div class="managecamp-detail-holder">
							<span class="review-title gray-darker gregular font-small name-plan" stylegroup-id=""></span>
							
							<span class="review-title gregular font-small" style="padding-top: 5px;">Style Color</span>
							<div class="managecamp-stylecolor-cont" style-planid="" >
								<div class="color-catcher">
									
								</div>
								<div class="selected-stylecolor-plus " >
									<div class="managecamp-designcolor-holder addmore-colors">
										<div class="managecamp-designcolor-header gregular white font-xsmall text-center">Add more colors for this style</div>
										<div class="managecamp-designcolor-cont2 colors-data">
										
											
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="managecamp-detail-number">
							<div class="managecamp-left-number">
								<div class="blackz gsemibold font-small text-center">Min. Price</div>
								<div class="blackz gregular font-small text-center min-price" id="min-price">$10.00</div>
							</div>
							<div class="managecamp-center-number">
								<div class="blackz gsemibold font-small text-center">Selling Price</div>
								<input type="text" class="form-control gregular font-small gray-darker text-center managecamp-sellingprice sell-price" name="sell-price" id="sell-price" value="$12.00" onkeypress="return isNumberKey(event)" />
							</div>
							<div class="managecamp-right-number">
								<div class="blackz gsemibold font-small text-center">Profit</div>
								<div class="blackz gsemibold font-small text-center profit" id="profit">$0.00</div>
							</div>
							<div class="clearfix"></div>
						</div>


					</div>		
				</div>	 
				  <!-- end of tshirt display -->


				
		</div>
				
	</div>
</div>
</div>


<!-- Modal zoom-->
<div id="zoomer" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content zoom-modal-holder">
      <div class="pop-header-notxt white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large popup-close" data-dismiss="modal"></span>
	  </div>
	   <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
					<div class="img-zoom-holder "><img class="img-responsive img-center zoom-image" src="<?php echo base_url('public/'.STORE.'/images/logo.png');?>" alt="" /></div>
					</div>
				</div>
		</div>

	</div>
  </div>
</div>



<!-- Modal confirm-->
<div id="confirm" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this item?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">All style colors and set quantities will be lost and this cannot be undone.</div>
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small"  data-dismiss="modal">No, go back to the page</button>
							<button class="orange-btn yesremove-btn white gsemibold font-small remove-itemshirt">Yes, remove the item</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Modal confirm-->
<div id="removedesign" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this design?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">Removing this design will also delete all the styles and colors you have selected.<br>This cannot be undone</div>
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
							<button class="orange-btn yesremove-btn white gsemibold font-small remove-design-confirm"  >Yes, remove this design</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Add more style modal-->
<div id="addmorestyle" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">Add more styles
			<span class="addmorestyle-paginate white gregular font-medium">4/6</span>
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
										  <select class="form-control custom-input gregular font-regular gray shirt-group-select" id="shirt-group-select">
												<option value="">Choose a category</option>
											<?php foreach ($shirt_category as $key => $group) { ?>
												<option value="<?php echo $group->shirt_group_id; ?>"><?php echo ucwords(strtolower($group->shirt_group_name)); ?></option>
											<?php	}  ?>
											
										  </select>
										</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<hr class="popup-border">
						</div>
					</div> 
					<!-- styles display -->
					<div class="row addmorestyle-holder-inner show-all-styles" plan-id="">
					<?php
						 
						 	 // echo $_POST['shirt_grp'];
						  

					 ?>
					 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 tshirt-plan">
						 <div class="gregular font-small gray group-name"></div>
						 <div class="addmorestyle-shirt-holder" shirt-id="" shirt-group-id="">
							 <span class="style-check "></span><img class="img-responsive img-center styleshirt-col"  src="" alt="" />
						 </div>
					</div>
						
					</div>
				</div>
	  </div>
	
		<div class="modal-footer">
          <button type="button" class="done-btn green-btn white gsemibold font-small text-uppercase Done-Style-Select" data-dismiss="modal">Done</button>
        </div>
	</div>
  </div>
</div>
<!-- Modal sizechart-->
<div id="sizechart" class="modal fade" role="dialog">
  	<div class="modal-dialog cantfind-dialog" style="width: 671px;">

	    <!-- Modal content-->
	    <div class="modal-content sizechart-modal-holder">
	      	<div class="confirm-header white gregular font-medium">
				<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
		  	</div>
		  	<div class="container-fluid modal-bg">
					<div class="row">
						<div class="col-lg-12 text-center measure-pad">
							<div class="gregular font-large blackz text-center sizechart-lbl">Size Chart for <p class="style-label">Style Name</p></div>
							
							<div class="cont-holder size-chart-info">
								<p class="gray gregular font-small text-left ">Classic:<br>
By far our best selling tee. Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. Check out the specs for the actual measurements and dimensions before you order.</p>
							</div>
							<div class="table-responsive size-chart-data"> 
								<table class="table table-bordered text-left font-small size-chart-table">
									<thead>
										<tr>
											<th class="gsemibold blackz">Body Specs</th>
											<th class="gsemibold blackz">S</th>
											<th class="gsemibold blackz">M</th>
											<th class="gsemibold blackz">L</th>
											<th class="gsemibold blackz">XL</th>
											<th class="gsemibold blackz">2XL</th>
											<th class="gsemibold blackz">3XL</th>
											<th class="gsemibold blackz">Tolerance</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="gregular blackz">Body Width</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Body Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Sleeve Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Arm hole</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php $this->load->view_store('footer');  ?>  
