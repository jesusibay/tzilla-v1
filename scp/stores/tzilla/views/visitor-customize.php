<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/visitor-customize.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/productsizechart.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/slick.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/slick-theme.css');?>">
<script src="<?php echo base_url('public/'.STORE.'/js/slick.js');?>"></script>
<div class="slider-overlay"></div>
<div class="container-fluid tgen-visitor-customize">
					<div id="wrap">
						<span class="vert-prev"></span>
						<span class="vert-next"></span>
						<div class="custom-inner-hold">
                        <div class="custom-slider">
                            <div class="slide charcoal" data-hex="4e4e4e"><img id="a" data-id="a" src="<?php echo base_url('public/'.STORE.'/images/a.1.png');?>" class="center-block img-responsive"><span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span></div>
							<div class="slide ash" data-hex="e1dfdf"><img id="b" data-id="b" src="<?php echo base_url('public/'.STORE.'/images/b.1.png');?>" class="center-block img-responsive"><span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span></div>
                            <div class="slide" data-hex="7C2328" style="background:#7C2328;"><img id="c" data-id="c" src="<?php echo base_url('public/'.STORE.'/images/c.1.png');?>" class="center-block img-responsive"><span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span></div>
							<div class="slide" data-hex="00843D" style="background:#00843D;"><img id="d" data-id="d" src="<?php echo base_url('public/'.STORE.'/images/d.1.png');?>" class="center-block img-responsive"><span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span></div>
                            <div class="slide" data-hex="EBFF08" style="background:#EBFF08;"><img id="e" data-id="e" src="<?php echo base_url('public/'.STORE.'/images/e.1.png');?>" class="center-block img-responsive"><span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span></div>
							<div class="slide" data-hex="582C83" style="background:#582C83;"><img id="f" data-id="f" src="<?php echo base_url('public/'.STORE.'/images/f.1.png');?>" class="center-block img-responsive"><span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span></div>
                        </div>
						<div class="add-more-slide" data-toggle="modal" data-target="#addmoredesign" data-dismiss="modal"></div>
						</div>
                    </div>
					
	<div class="container tgen-content-holder">
		<div class="row">
			<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
				<div class="tgen-right-holder">
					<div class="tgen-lefticon-holder">
						<div class="tgen-icon-holder" data-toggle="modal" data-target="#zoomer" data-dismiss="modal"><span class="tgen-zoom ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">zoom</div></div>
						<!-- <div class="tgen-icon-holder"><span class="tgen-spec ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">Size</br>Chart</div></div> -->
						<div class="tgen-icon-holder" data-toggle="modal" data-target="#sizechart"><span class="tgen-spec ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">Size</br>Chart</div></div>
						<div class="tgen-icon-holder"><span class="tgen-share ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">share</div>
							<div class="social-share">
								<span class="icon-facebook"></span>
								<span class="icon-twitter"></span>
								<span class="icon-gplus"></span>
								<span class="icon-email"></span>
							</div>
						</div>
					</div>
					<div class="tgen-righticon-holder super-custom-btn">
						<div class="tgen-icon-holder2"><span class="tgen-sc ease-effect"></span><div class="tgen-icon-label2 gsemibold font-xsmall gray">Super Customize</div></div>
						
					</div>
					<div class="tgen-cart-btn"></div>
					
					<div class="tgen-shirt-holder">
								<img class="img-responsive img-center shirt-col" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
								<div class="design-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/a.1.png');?>" alt="" data-id="a"/></div>
					</div>
					<div class="tgen-fb-btn-holder">
						<button class="front-btn gray gregular font-medium text-center activepane">Front</button>
						<button class="back-btn gray gregular font-medium text-center">Back</button>
					</div>
				</div>
				<div class="tgen-right-holder-footer">
					<div class="design-variation-label gregular font-regular blackz text-center">See other design variations</div>
					<div class="tgen-variation-slider">
							<div class="owl-carousel">
								<div class="design-variation-holder selected-variation"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/a.1.png');?>" alt="" /></div>
								<div class="design-variation-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/a.2.png');?>" alt="" /></div>
							</div>
							<span class="design-variation-prev"></span>
							<span class="design-variation-next"></span>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 super-custom-container">
				<div class="close-sc-slider-cont"><span class="close-sc-slider icon-circled-x"></span></div>
				<div class="super-custom-holder">
					<img class="img-responsive img-center pull-left" src="<?php echo base_url('public/'.STORE.'/images/super-customize-logo.png');?>" alt="" />
					<span class="inf pull-left" data-toggle="tooltip" data-placement="bottom" title="" data-hex="ff0000" data-original-title="Play around and some have fun personalizing your own design."></span>
					<div class="clearfix"></div>
				</div>
			<div class="super-custom-container-scroll">	
				<div class="tgen-tab-holder"> 
					<ul class="nav nav-tabs">
					  <li><a data-toggle="tab" href="#styles" class="gregular font-medium text-center">Styles</a></li>
					  <li class="active"><a data-toggle="tab" href="#custom" class="gregular font-medium text-center">Custom</a></li>
					  <li><a data-toggle="tab" href="#patterns" class="gregular font-medium text-center">Patterns</a></li>
					</ul>

					<div class="tab-content">
					<!---------1st tab------------->
					  <div id="styles" class="tab-pane fade">
					    <div class="style-holder-outer">
						<div class="style-label gregular font-small gray-darker">Classic Crew T-Shirt</div>
						<div class="style-holder-inner">
							<div class="style-holder-item style-holder-active">
								<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
								<div class="remove-style-btn-holder remove-style-btn-active" data-toggle="modal" data-target="#removestyle" data-dismiss="modal"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/large-remove-button2.png');?>" alt="" /></div>
							</div>
							<div class="style-holder-item">
								<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
								<div class="remove-style-btn-holder" data-toggle="modal" data-target="#removestyle" data-dismiss="modal"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/large-remove-button2.png');?>" alt="" /></div>
							</div>
							<div class="style-holder-item">
								<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
								<div class="remove-style-btn-holder" data-toggle="modal" data-target="#removestyle" data-dismiss="modal"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/large-remove-button2.png');?>" alt="" /></div>
							</div>
							<div class="addstyle-holder-item" data-toggle="modal" data-target="#addmorestyle" data-dismiss="modal"></div>
							<div class="clearfix"></div>
						</div>
						</div>
					  </div>
					  <!----------2nd tab------------>
					  <div id="custom" class="tab-pane fade in active">
							<div class="row">
							<div class="col-lg-12">
								<div class="custom-left-holder">
								<div class="form-group">
									<label class="gregular font-xsmall gray-darker custom-label" for="">Color 1</label>
									<div class="custom-select form-control primarycol">
										<i class="icon-circle pull-left custom-circle"></i>
										<i class="icon-caret-down pull-right custom-select-arrow"></i>
										<div class="color-primary" style="">
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
										</div>
									</div>
								</div>
								</div>
								
								<div class="custom-right-holder">
								<div class="form-group">
									<label class="gregular font-xsmall gray-darker custom-label" for="">Color 2</label>
									<div class="custom-select form-control secondarycol">
										<i class="icon-circle pull-left custom-circle"></i>
										<i class="icon-caret-down pull-right custom-select-arrow"></i>
										<div class="color-secondary" style="">
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="ff0000" data-original-title="red"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="000000" data-original-title="yellow"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00ff00" data-original-title="green"></span>
											<span class="col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="blue"></span>
										</div>
									</div>
								</div>
								</div>
								
								<div class="clearfix"></div>
									<div class="form-group">
									  <label class="gregular font-xsmall gray-darker custom-label" for="">Text 1</label>
									  <input type="text" class="form-control custom-input gitalic font-xsmall gray-dark" id="" Placeholder="Text1">
									</div>
									
									<div class="form-group">
									  <label class="gregular font-xsmall gray-darker custom-label" for="">Text 2</label>
									  <input type="text" class="form-control custom-input gitalic font-xsmall gray-dark" id="" Placeholder="Text2">
									</div>
								<div class="clearfix"></div>
								
									<div class="decade-holder">
										<div class="form-group">
										  <label class="gregular font-xsmall gray-darker custom-label" for="">Decade</label>
										  <input type="text" class="form-control custom-input gitalic font-xsmall gray-dark" id="" Placeholder="19">
										</div>
									</div>
									
									<div class="year-holder">
										<div class="form-group">
										  <label class="gregular font-xsmall gray-darker custom-label" for="">Year</label>
										  <input type="text" class="form-control custom-input gitalic font-xsmall gray-dark" id="" Placeholder="71">
										</div>
									</div>
									
									<div class="distress-holder">
										<div class="form-group">
										  <label class="gregular font-xsmall gray-darker custom-label" for="">Distress</label>
										  <select class="form-control custom-input gitalic font-xsmall gray-dark" id="">
											<option>No</option>
											<option>Yes</option>
										  </select>
										</div>
									</div>
									
									<div class="clearfix"></div>
									<div class="tgen-btn-holder">
										<button class="orange-btn tgen-apply-btn white gsemibold font-small">Apply</button>
									</div>
							</div>
							</div>
					  </div>
					  <!----------3rd tab------------>
					  <div id="patterns" class="tab-pane fade">
						<div class="pattern-holder-outer">
							<div class="pattern-label gregular font-small gray-darker">Cammo Green</div>
							<div class="pattern-holder-inner">
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-1.jpg');?>" alt="" />
									<span class="remove-pattern remove-pattern-active"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-2.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-3.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-4.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-5.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-6.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-7.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-8.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-9.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-10.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-11.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-12.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								
								<div class="clearfix"></div>
							</div>
						</div>
					  </div>
					</div>
				</div>
				
				
				<div class="custom-shirtcol-label gregular font-large blackz">Select Style Colors</div>
					<div class="custom-shirtcol-holder">
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="EBFF08" data-original-title="Neon Safety Green" style="background:#EBFF08;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FEE500" data-original-title="yellow" style="background:#FEE500;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FFA800" data-original-title="Gold" style="background:#FFA800;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FD6513" data-original-title="Neon Safety Orange" style="background:#FD6513;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FA5016" data-original-title="Orange" style="background:#FA5016;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FFCADE" data-original-title="Light Pink" style="background:#FFCADE;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FF5FB6" data-original-title="Neon Pink" style="background:#FF5FB6;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="C4146E" data-original-title="Hot Pink" style="background:#C4146E;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="AB0C20" data-original-title="Red" style="background:#AB0C20;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="B45017" data-original-title="Texas Orange" style="background:#B45017;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="7C2328" data-original-title="Cardinal Red" style="background:#7C2328;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="572932" data-original-title="Maroon" style="background:#572932;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="00843D" data-original-title="Kelly Green" style="background:#00843D;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="173819" data-original-title="Forest Green" style="background:#173819;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="3D441E" data-original-title="Military Green" style="background:#3D441E;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="8BB8E8" data-original-title="Light Blue" style="background:#8BB8E8;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="5E83B4" data-original-title="Columbia Blue" style="background:#5E83B4;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="1D4099" data-original-title="Royal Blue" style="background:#1D4099;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="001C43" data-original-title="Deep Blue" style="background:#001C43;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="1E1C2C" data-original-title="Navy" style="background:#1E1C2C;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="8CE2D0" data-original-title="Celadon Green" style="background:#8CE2D0;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="007DBA" data-original-title="Turquoise" style="background:#007DBA;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="4C3300" data-original-title="Coffee Brown" style="background:#4C3300;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="462C15" data-original-title="Chocolate Brown" style="background:#462C15;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="582C83" data-original-title="Purple" style="background:#582C83;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FFF8EB" data-original-title="Natural" style="background:#FFF8EB;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="D7CAB7" data-original-title="Sand" style="background:#D7CAB7;"><span class="selected"></span></div>
						<div class="shirt-col-selection athletic" data-toggle="tooltip" data-placement="top" title="" data-hex="888888" data-original-title="Athletic Heather" ><span class="selected"></span></div>
						<div class="shirt-col-selection denim" data-toggle="tooltip" data-placement="top" title="" data-hex="254195" data-original-title="Denim Heather" ><span class="selected"></span></div>
						<div class="shirt-col-selection charcoal" data-toggle="tooltip" data-placement="top" title="" data-hex="4e4e4e" data-original-title="Charcoal Heather" ><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="FFFFFF" data-original-title="White" style="background:#FFFFFF;"><span class="selected"></span></div>
						<div class="shirt-col-selection ash" data-toggle="tooltip" data-placement="top" title="" data-hex="e1dfdf" data-original-title="Ash"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="D8D7DF" data-original-title="Silver" style="background:#D8D7DF;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="3D4240" data-original-title="Charcoal" style="background:#3D4240;"><span class="selected"></span></div>
						<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="" data-hex="282828" data-original-title="Black" style="background:#000000;"><span class="selected"></span></div>
						
						
					</div>
					<div class="tgen-btn-holder2">
						<button class="green-btn tgen-addtocollection-btn white gsemibold font-small">Add to Collection</button>
						<div class="custom-shirtcol-label glight font-xsmall gray-dark" style="padding-top: 6px;padding-bottom: 6px;"><span style="color: #ff0000;">*</span>Please Note: Colors and sizing of art may vary.</div>
					</div>
					
					
			</div>
			</div>
		</div>
	</div>
	
	
	<!--DESIGN CART-->
		<div class="design-container">
			<div class="design-cont-btn dc-open"></div>
			<div class="design-cont-header gsemibold font-medium white text-center">My Collection <span class="close-collection-slider icon-circled-x"></span></div>
			<div class="mycoll-item-holder">
			<div class='empty-collection gregular gray-light'>Empty</div>
			
				
			</div>
			<div class="design-cont-footer">
				<button class="gray-btn proceed-btn white gsemibold font-small"  data-toggle="modal" data-target="#loginmodal" data-dismiss="modal" disabled>Next</button>
			</div>
		</div>
</div>



<!-- Modal sizechart-->
<div id="sizechart" class="modal fade" role="dialog">
  	<div class="modal-dialog cantfind-dialog" style="width: 671px;">

	    <!-- Modal content-->
	    <div class="modal-content sizechart-modal-holder">
	      	<div class="confirm-header white gregular font-medium">
				<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
		  	</div>
		  	<div class="container-fluid modal-bg">
					<div class="row">
						<div class="col-lg-12 text-center measure-pad ">
							
							<div class="sizechart-label1 gregular font-large blackz text-center sizechart-lbl">Size Chart for <span class="style-label">Style Name</span></div>
							
							<div class="cont-holder size-chart-info">
								<p class="gray gregular font-small text-left ">Classic:<br>
By far our best selling tee. Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. Check out the specs for the actual measurements and dimensions before you order.</p>
							</div>
							<div class="table-responsive size-chart-data"> 
								<table class="table table-bordered text-left font-small size-chart-table">
									<thead>
										<tr>
											<th class="gsemibold blackz">Body Specs</th>
											<th class="gsemibold blackz">S</th>
											<th class="gsemibold blackz">M</th>
											<th class="gsemibold blackz">L</th>
											<th class="gsemibold blackz">XL</th>
											<th class="gsemibold blackz">2XL</th>
											<th class="gsemibold blackz">3XL</th>
											<th class="gsemibold blackz">Tolerance</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="gregular blackz">Body Width</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Body Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Sleeve Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Arm hole</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal confirm-->
<div id="removedesign" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this design?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">Removing this design will also delete all the styles and colors you have selected.<br>
This cannot be undone</div>
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
							<button class="orange-btn yesremove-btn white gsemibold font-small" data-dismiss="modal">Yes, remove this design</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>



<!-- Modal confirm-->
<div id="removestyle" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this style?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">Removing this style will also delete all the colors you’ve selected for it.<br>
This cannot be undone.</div>
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
							<button class="orange-btn yesremove-btn white gsemibold font-small" data-dismiss="modal">Yes, remove this design</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Modal confirm-->
<div id="removecollection" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this item?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">Removing this item will delete all your customizations.<br>
This cannot be undone.</div>
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
							<button class="orange-btn yesremove-btn yesremove-btn-collection white gsemibold font-small" data-dismiss="modal">Yes, remove this design</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Modal zoom-->
<div id="zoomer" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content zoom-modal-holder">
      <div class="pop-header-notxt white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large popup-close" data-dismiss="modal"></span>
	  </div>
	   <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
					<div class="img-zoom-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/logo.png');?>" alt="" /></div>
					</div>
				</div>
		</div>

	</div>
  </div>
</div>


<!-- Modal buy or create campaign
<div id="buyorcreate" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium text-center">Looks Great! What would like to do next?
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="createorbuy-left text-center">
							<div class="createorbuy-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/buy-now.png');?>" alt="" /></div>
							<div class="createorbuy-label-big gsemibold font-xlarge gray-darker text-center">BUY<br>NOW</div>
							<div class="createorbuy-label1 glight font-regular gray-darker text-center">Buy your customized garments.<br></div>
							<button class="orange-btn white gsemibold font-small text-uppercase">Start Now</button>
						
					</div>
					<div class="createorbuy-right text-center">
							<div class="createorbuy-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/create-a-campaign-circle.png');?>" alt="" /></div>
							<div class="createorbuy-label-big gsemibold font-xlarge gray-darker text-center">CREATE A<br>CAMPAIGN</div>
							<div class="createorbuy-label2 glight font-regular gray-darker text-center">Use your designs to create a <br>fundraiser for your school.</div>
							<button class="green-btn white gsemibold font-small text-uppercase">Start Now</button>
						
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>-->



<!-- Add more style modal-->
<div id="addmorestyle" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">Add more styles
			<span class="addmorestyle-paginate white gregular font-medium">4/6</span>
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
										  <select class="form-control custom-input gregular font-regular gray" id="">
											<option>Choose a category</option>
											<option>Shirt 1</option>
											<option>Shirt 2</option>
										  </select>
										</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<hr class="popup-border">
						</div>
					</div>
					<div class="row addmorestyle-holder-inner">
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="gsemibold font-xsmall gray">STYLE NAME</div>
							<div class="addmorestyle-shirt-holder addmorestyle-shirt-holder-active">
								<span class="style-remove"></span>
								<span class="style-check style-check-active"></span>
								<img class="img-responsive img-center styleshirt-col" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="gsemibold font-xsmall gray">STYLE NAME</div>
							<div class="addmorestyle-shirt-holder">
								<span class="style-remove"></span>
								<span class="style-check"></span>
								<img class="img-responsive img-center styleshirt-col" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="gsemibold font-xsmall gray">STYLE NAME</div>
							<div class="addmorestyle-shirt-holder">
								<span class="style-remove"></span>
								<span class="style-check"></span>
								<img class="img-responsive img-center styleshirt-col" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="gsemibold font-xsmall gray">STYLE NAME</div>
							<div class="addmorestyle-shirt-holder">
								<span class="style-remove"></span>
								<span class="style-check"></span>
								<img class="img-responsive img-center styleshirt-col" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="gsemibold font-xsmall gray">STYLE NAME</div>
							<div class="addmorestyle-shirt-holder">
								<span class="style-remove"></span>
								<span class="style-check"></span>
								<img class="img-responsive img-center styleshirt-col" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="gsemibold font-xsmall gray">STYLE NAME</div>
							<div class="addmorestyle-shirt-holder">
								<span class="style-remove"></span>
								<span class="style-check"></span>
								<img class="img-responsive img-center styleshirt-col" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />
							</div>
						</div>
					</div>
				</div>
	  </div>
	
		<div class="modal-footer">
          <button type="button" class="done-btn green-btn white gsemibold font-small text-uppercase" data-dismiss="modal">Done</button>
        </div>
	</div>
  </div>
</div>


<!-- Add more design modal-->
<div id="addmoredesign" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">Add more designs to your collection.
			<span class="addmorestyle-paginate white gregular font-medium">4/6</span>
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
										  <select class="form-control custom-input gregular font-regular gray" id="">
											<option>Choose a category</option>
											<option>Shirt 1</option>
											<option>Shirt 2</option>
										  </select>
										</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<hr class="popup-border">
						</div>
					</div>
					<div class="row addmorestyle-holder-inner">
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="design-item-holder" data-id="a">
							<div class="design-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">Slant</span></div>
							<div class="store-image-box">
								<span class="design-backprint"></span>
								<div class="store-selection-holder">
									<span class="design-unselect"></span>
									<span class="design-select"></span>
								</div>
								<img class="img-responsive img-center box-design" src="<?php echo base_url('public/'.STORE.'/images/a.1.png');?>" alt="" />
							</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="design-item-holder" data-id="b">
							<div class="design-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">Slant</span></div>
							<div class="store-image-box">
								<span class="design-backprint"></span>
								<div class="store-selection-holder">
									<span class="design-unselect"></span>
									<span class="design-select"></span>
								</div>
								<img class="img-responsive img-center box-design" src="<?php echo base_url('public/'.STORE.'/images/b.1.png');?>" alt="" />
							</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="design-item-holder" data-id="c">
							<div class="design-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">Slant</span></div>
							<div class="store-image-box">
								<span class="design-backprint"></span>
								<div class="store-selection-holder">
									<span class="design-unselect"></span>
									<span class="design-select"></span>
								</div>
								<img class="img-responsive img-center box-design" src="<?php echo base_url('public/'.STORE.'/images/c.1.png');?>" alt="" />
							</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="design-item-holder" data-id="d">
							<div class="design-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">Slant</span></div>
							<div class="store-image-box">
								<span class="design-backprint"></span>
								<div class="store-selection-holder">
									<span class="design-unselect"></span>
									<span class="design-select"></span>
								</div>
								<img class="img-responsive img-center box-design" src="<?php echo base_url('public/'.STORE.'/images/d.1.png');?>" alt="" />
							</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="design-item-holder" data-id="e">
							<div class="design-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">Slant</span></div>
							<div class="store-image-box">
								<span class="design-backprint"></span>
								<div class="store-selection-holder">
									<span class="design-unselect"></span>
									<span class="design-select"></span>
								</div>
								<img class="img-responsive img-center box-design" src="<?php echo base_url('public/'.STORE.'/images/e.1.png');?>" alt="" />
							</div>
							</div>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="design-item-holder" data-id="f">
							<div class="design-label gregular font-xsmall gray text-uppercase">Design:<span class="design-name gsemibold">Slant</span></div>
							<div class="store-image-box">
								<span class="design-backprint"></span>
								<div class="store-selection-holder">
									<span class="design-unselect"></span>
									<span class="design-select"></span>
								</div>
								<img class="img-responsive img-center box-design" src="<?php echo base_url('public/'.STORE.'/images/f.1.png');?>" alt="" />
							</div>
							</div>
						</div>
						
						
					</div>
				</div>
	  </div>
	
		<div class="modal-footer">
          <button type="button" class="done-btn gray-btn white gsemibold font-small text-uppercase"  disabled data-dismiss="modal">Done</button>
        </div>
	</div>
  </div>
</div>

<script>

  $(document).on('ready', function() {
	  setTimeout(function(){
		$("#wrap .slide:eq(0)").click();
	 }, 500);
	 
	 
      $(".custom-slider").slick({
        dots: false,
        infinite: false,
		adaptiveHeight: true,
        slidesToShow: 5,
		verticalSwiping:true,
		vertical:true,
        slidesToScroll: 1,
		prevArrow: $('.vert-next'),
		nextArrow: $('.vert-prev'),
		arrows: true,
		 responsive: [
		  {
      breakpoint: 1515,
      settings: {
		verticalSwiping:false,
		vertical:false,
		slidesToShow: 5,
		slidesToScroll: 1,
		adaptiveHeight: true
	    }
    },
		 {
      breakpoint: 1325,
      settings: {
		verticalSwiping:false,
		vertical:false,
		slidesToShow: 5,
		slidesToScroll: 1,
		adaptiveHeight: true
	    }
    },
		 {
      breakpoint: 1024,
      settings: {
		verticalSwiping:false,
		vertical:false,
		slidesToShow: 5,
		slidesToScroll: 1,
		adaptiveHeight: true
	    }
    },
    {
      breakpoint: 992,
      settings: {
		verticalSwiping:false,
		vertical:false,
		slidesToShow: 5,
		slidesToScroll: 1,
		adaptiveHeight: true
	    }
    },
	{
      breakpoint: 768,
      settings: {
		verticalSwiping:false,
		vertical:false,
		slidesToShow: 5,
		slidesToScroll: 1,
		adaptiveHeight: true
	    }
    },
    {
      breakpoint: 480,
      settings: {
		verticalSwiping:false,
		vertical:false,
		slidesToShow: 4,
		slidesToScroll: 1,
		adaptiveHeight: true
	    }
    },
	{
      breakpoint: 320,
      settings: {
		verticalSwiping:false,
		vertical:false,
		slidesToShow: 4,
		slidesToScroll: 1,
		adaptiveHeight: true
	    }
    }
  ]
      });
	  
	  
		  var img = [];
		  var slideid="";
		  var idArray = [];
		  var slideArray = [];
		  executeDesignArray();
		  var removeid ="";
		$(document).on("click", "#addmoredesign .done-btn", function(){
		  var item = $(".custom-slider .slide").length;
		  var id="";
		 
		  
		  if(item < 6){
		  $(".custom-slider").find(".slide").removeClass("selected");
		  
			
			
			executeDesignArray();
			
			for (i = 0; i < idArray.length; i++) { 
				if(jQuery.inArray(idArray[i],slideArray) !== -1 ) {
					//console.log("In Array "+idArray[i]);
					
				} else {
					//console.log("Wala sa Array"+idArray[i]);
					//console.log(img[i]);
					if($("[data-id*='"+idArray[i]+"'").find(".design-select").hasClass("design-active")){
						$('.custom-slider').slick('slickAdd','<div class="slide" data-hex="000000"><img data-id="'+idArray[i]+'" src="'+img[i]+'" class="center-block img-responsive"><span data-toggle="modal" data-target="#removedesign" data-dismiss="modal"></span></div>');
						slideArray.push(idArray[i]);
					}
				}
			}
	
			  setTimeout(function(){
				$(".custom-slider").find("[data-slick-index*='0']").click();
			  }, 1000);
			  
			 
		  }else {
			  alert("Only 6 design can add to your slider!");
			  //$(".addmorestyle-holder-inner").find(".design-select").removeClass("design-active");
		  }
		});
		
		
		function executeDesignArray() {
			
			slideArray = [];
			
			$(".addmorestyle-holder-inner .design-select").each(function() {
					id = $(this).parent().parent().parent().attr("data-id");
					img.push($("[data-id*='"+id+"'").find(".box-design").attr("src"));
					
					if(jQuery.inArray(id,idArray) === -1 ){
						idArray.push(id);
					}
					
			});
			console.log("idArray "+idArray);
			$(".custom-slider .slide").each(function() {
				slideid = $(this).find("img").attr("data-id");
				
				if(jQuery.inArray(slideid,slideArray) === -1 ){
						slideArray.push(slideid);
						console.log("push slide "+slideid);
					}
					
				
				
			});
			console.log("slideArray"+slideArray);
		}
		
		$(document).on("click", ".add-more-slide", function(){
			
			executeDesignArray();
			
			for (i = 0; i < idArray.length; i++) { 
				if(jQuery.inArray(idArray[i],slideArray) !== -1 ) {
					$("[data-id*='"+idArray[i]+"'").find(".design-select").addClass("design-active");
				} else {
					$("[data-id*='"+idArray[i]+"'").find(".design-select").removeClass("design-active");
				}
			}
			var check = $(".addmorestyle-holder-inner").find(".design-active").length;
				if(check != 6) {
					$(".done-btn").addClass("green-btn").removeClass("gray-btn").removeAttr("disabled");
				}else {
					$(".done-btn").addClass("gray-btn").removeClass("green-btn").attr("disable","disable");
				}
			
		});

		$('#removedesign .yesremove-btn').on('click', function() {
			var index = $(".custom-slider").find(".selected").attr("data-slick-index");
			var id = $(".custom-slider").find(".selected img").attr("data-id");
			$("[data-id*='"+id+"'").find(".design-select").removeClass("design-active");
			var j = 0;
			$('.custom-slider').slick('slickRemove',index);
			$(".custom-slider .slick-slide").each(function(){
			   $(this).attr("data-slick-index",j);
			   j++;
			});
			
		});
		
	  
	  $(document).on("click", "#wrap .slide", function(){
		  $("#wrap .slide").removeClass("selected");
		  $(this).addClass("selected");
		  var img = $(this).find("img").attr("src");
		  var id = $(this).find("img").attr("data-id");
		  $(".design-holder img").attr("src",img);
		  $(".design-holder img").attr("data-id",id);
		  var style="";
		  var hex = $(this).attr("data-hex");
		  var imgtexture = "<?php echo base_url('public/'.STORE.'/images/heather-texture.png');?>";

		  if(hex == "888888" || hex == "254195" || hex == "4e4e4e" || hex == "e1dfdf") {
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture+")"});
			var bg = "background-color:#"+hex+";";
			var col = "background-image:url('<?php echo base_url('public/'.STORE.'/images/heather-texture.png');?>')"+";";
			style = bg+col;
		  }else{
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex});
			var bg = "background-color:#"+hex+";";
			style = bg;
		  }
		  
		  variation(id,img,style);
	  });
	  
	
	  
	  $(document).on("click", ".pattern-holder-item", function(){
		  $(".pattern-holder-item .remove-pattern").removeClass("remove-pattern-active");
		  $(this).find(".remove-pattern").addClass("remove-pattern-active");
	  });
	  
	  
	  $(document).on("click", ".shirt-col-selection", function(){
		  var hex = $(this).attr("data-hex");
		  var imgtexture = "<?php echo base_url('public/'.STORE.'/images/heather-texture.png');?>";

		  if(hex == "888888" || hex == "254195" || hex == "4e4e4e" || hex == "e1dfdf") {
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex+" url("+imgtexture+")"});
		  }else{
			$(".tgen-shirt-holder img.shirt-col").css({"background":"#"+hex});
		  }
		  
		  if($(this).hasClass("shirt-col-selection-active")){
			$(this).removeClass("shirt-col-selection-active");
		  }else {
			$(this).addClass("shirt-col-selection-active");
		  }
		  
	  });
	  
	  
	  
	   $(document).on("click", ".style-holder-item", function(){
		  $(".style-holder-item").removeClass("style-holder-active");
		  $(".style-holder-item").find(".remove-style-btn-holder").removeClass("remove-style-btn-active");
		  $(this).addClass("style-holder-active");
		   $(this).find(".remove-style-btn-holder").addClass("remove-style-btn-active");
	  });
	  
	  $(document).on("click", ".design-variation-holder", function(){
		  $(".design-variation-holder").removeClass("selected-variation");
		  $(this).addClass("selected-variation");
	  });
	  
	  $(document).on("click", ".primarycol", function(){
		  $(this).find(".color-primary").toggle();
		  $(".color-secondary").hide();
	  });
	  $(document).on("click", ".secondarycol", function(){
		  $(this).find(".color-secondary").toggle();
		  $(".color-primary").hide();
	  });
	  $(document).mouseup(function (e){
			var container = $(".color-primary, .color-secondary");

			if (!container.is(e.target) // if the target of the click isn't the container...
				&& container.has(e.target).length === 0) // ... nor a descendant of the container
			{
				container.hide();
			}
	  });
	  
	  
	  $(document).on("click", ".mycoll-showall", function(){
		  $(this).parent().find(".mycoll-color-holder-inner").toggleClass("mycoll-color-holder-inner-show");
		  var check = $(this).parent().find(".mycoll-color-holder-inner").hasClass("mycoll-color-holder-inner-show");
		  if(check==true) {
			 $(this).html("Show less");
		  }else{
			 $(this).html("Show more");
		  }
	  });
	  
	  $(document).on("click", ".super-custom-btn", function(){
		if($(".design-container").hasClass("design-cont-open")){
			$(".design-cont-btn").click();
		}
		
		$(".super-custom-container").toggleClass("scc-show");
		if($(".super-custom-container").hasClass("scc-show")){
			$(".slider-overlay").fadeIn();
		}else {
			$(".slider-overlay").fadeOut();
		}
	});
	
	
	$(document).on("click", ".tgen-cart-btn", function(){
		if($(".super-custom-container").hasClass("scc-show")){
			$(".tgen-cart-btn").click();
		}
		
		$(".design-container").toggleClass("design-cont-open");
		
		if($(".design-container").hasClass("design-cont-open")){
			$(".slider-overlay").fadeIn();
		}else {
			$(".slider-overlay").fadeOut();
		}
	});
	$(document).on("click", ".slider-overlay, .close-collection-slider, .close-sc-slider-cont", function(){
		$(".design-container").removeClass("design-cont-open");
		$(".super-custom-container").removeClass("scc-show");
		$(".slider-overlay").fadeOut();
	});
	
	$(document).on("click", ".design-select", function(){
		$(this).addClass("design-active");
	});
	$(document).on("click", ".design-unselect", function(){
		$(this).parent().find(".design-select").removeClass("design-active");
	});
	
	$(document).on("click", ".style-check", function(){
		$(this).addClass("style-check-active");
		$(this).parent().addClass("addmorestyle-shirt-holder-active");
	});
	$(document).on("click", ".style-remove", function(){
		$(this).parent().find(".style-check").removeClass("style-check-active");
		$(this).parent().removeClass("addmorestyle-shirt-holder-active");
	});
	
	/* $(document).on("click", ".slide", function(){
		var data = $(this).find("img").attr("data-id");
		var img = $(this).find("img").attr("href");
		variation(data,img);
	}); */
	
	$(document).on("click", ".design-variation-holder", function(){
		var variationimg = $(this).find("img").attr("src");
		$(".design-holder").find("img").attr("src",variationimg);
	});
	
	$(document).on("click", ".mycoll-remove", function(){
		removeid = $(this).parent().attr("cart-id");
		$(".yesremove-btn-collection").attr("deleteid",removeid);
	});
	
	$(document).on("click", ".yesremove-btn-collection", function(){
		var key = $(this).attr("deleteid");
		$("[cart-id*='"+key+"']").remove();
		var item = $(".mycoll-shirt-holder-item").length;
		if(item < 1){
			$(".mycoll-item-holder").append("<div class='empty-collection gregular gray-light'>Empty</div>");
			$(".design-cont-btn").click();
			$(".proceed-btn").removeClass("green-btn").addClass("gray-btn").attr("disabled","disabled");
		}
	});
	
	$(document).on("click", ".tgen-addtocollection-btn", function(){
		var selectedcol = $(".custom-shirtcol-holder .shirt-col-selection-active").length;
		var hex = $(".custom-shirtcol-holder .shirt-col-selection-active:last").attr("data-hex");
		var src = $(".tgen-shirt-holder .design-holder img").attr("src");
		var id = $(".tgen-shirt-holder .design-holder img").attr("data-id");
		var item = '<div class="mycoll-shirt-holder-item" cart-id="'+id+'">';
			item +='<div class="mycoll-shirt-holder">';
			item +='<img class="img-responsive img-center shirt-col" style="background:#'+hex+'" src="<?php echo base_url('public/'.STORE.'/images/shirt-test.png');?>" alt="" />';
			item +='<div class="mycoll-design-holder"><img class="img-responsive img-center" src="'+src+'" alt="" /></div>';
			item +='</div>';
			item +='<div class="mycoll-color-holder-outer">';
			item +='<span class="mycoll-design-name gsemibold font-xsmall gray text-left text-uppercase">Design Name</span>';
			item +='<span class="mycoll-style-name gregular font-xsmall gray text-left">Style Name</span>';
			item +='<div class="mycoll-color-holder-inner">';
				for (i = 0; i < selectedcol; i++) {
					item +='<span class="small-shirt-col" data-toggle="tooltip" data-placement="top" title="" data-hex="0000ff" data-original-title="Neon Safety Green"></span>';
				}
			item +='<div class="clearfix"></div>';
			item +='</div>';
			item +='<div class="mycoll-showmore mycoll-showall gregular font-xsmall gray text-left">Show more</div>';
			item +='</div>';
			item +='<span class="mycoll-remove" data-toggle="modal" data-target="#removecollection" data-dismiss="modal"></span>';
			item +='<div class="clearfix"></div>';
			item +='</div>';
			if(selectedcol > 0){
				$(".empty-collection").remove();
				$(".mycoll-item-holder").prepend(item);
				$(".design-cont-btn").click();
				$(".proceed-btn").removeClass("gray-btn").addClass("green-btn").removeAttr("disabled");
			}else{
				alert("You need to select color first!");
			}
	});
	
	$(document).on("click", ".tgen-icon-holder:eq(2)", function(){
		$(this).find(".social-share").toggle();
	});
	
	
	$(document).on("click", ".proceed-btn", function(){
		$(".login-box").hide();
		$(".reg-box").hide();
		$(".buy-create").show();
		$(".login-modal-header").text("Looks great! What would like to do next?");
	});
	
	$(document).on("click", ".startbuy-btn, .startcreate-btn", function(){
		$(".login-box").show();
		$(".reg-box").hide();
		$(".buy-create").hide();
		$(".login-modal-header").text("Please Log in or Register to Continue");
	});
});

function variation(data,img,style) {
	console.log(style);
	switch(data) {
    case "a":
        $(".design-holder").find("img").attr("src",img);
		$('.owl-carousel').trigger('replace.owl.carousel', '<div class="design-variation-holder selected-variation" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/a.1.png');?>" alt="" /></div><div class="design-variation-holder" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/a.2.png');?>" alt="" /></div>');
		$('.owl-carousel').trigger('refresh.owl.carousel');
        break;
    case "b":
        $(".design-holder").find("img").attr("src",img);
		$('.owl-carousel').trigger('replace.owl.carousel', '<div class="design-variation-holder selected-variation" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/b.1.png');?>" alt="" /></div><div class="design-variation-holder" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/b.2.png');?>" alt="" /></div>');
		$('.owl-carousel').trigger('refresh.owl.carousel');
        break;
	case "c":
        $(".design-holder").find("img").attr("src",img);
		$('.owl-carousel').trigger('replace.owl.carousel', '<div class="design-variation-holder selected-variation" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/c.1.png');?>" alt="" /></div><div class="design-variation-holder" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/c.2.png');?>" alt="" /></div>');
		$('.owl-carousel').trigger('refresh.owl.carousel');
        break;
	case "d":
        $(".design-holder").find("img").attr("src",img);
		$('.owl-carousel').trigger('replace.owl.carousel', '<div class="design-variation-holder selected-variation" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/d.1.png');?>" alt="" /></div><div class="design-variation-holder" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/d.2.png');?>" alt="" /></div>');
		$('.owl-carousel').trigger('refresh.owl.carousel');
        break;
	case "e":
        $(".design-holder").find("img").attr("src",img);
		$('.owl-carousel').trigger('replace.owl.carousel', '<div class="design-variation-holder selected-variation" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/e.1.png');?>" alt="" /></div><div class="design-variation-holder" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/e.2.png');?>" alt="" /></div>');
		$('.owl-carousel').trigger('refresh.owl.carousel');
        break;
	case "f":
        $(".design-holder").find("img").attr("src",img);
		$('.owl-carousel').trigger('replace.owl.carousel', '<div class="design-variation-holder selected-variation" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/f.1.png');?>" alt="" /></div><div class="design-variation-holder" style='+style+'><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/f.2.png');?>" alt="" /></div>');
		$('.owl-carousel').trigger('refresh.owl.carousel');
        break;
    default:
        
	}
}

</script>