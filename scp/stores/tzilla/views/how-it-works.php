<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/howitworks.css');?>">
<div class="container-fluid howitworks-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<div class="howitworks-lefthead-holder">
					<h1 class="white gsemibold font-xlarge">How It Works</h1>
					<p class="white gregular font-regular">We're artists. Like you, we're also very busy people who want better shirt designs than the status quo (and in less time). We've spent two years developing an industry-first: A super-easy way to make a collection of </p>
					<button class="howitworks-getstarted-btn green-btn gsemibold font-regular white">Get Started</button>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<video class="howitworks-video" controls>
					<source src="<?php echo base_url('public/'.STORE.'/video/Tower Sessions - Valley of Chrome - Love and Devotion S03E20.3.mp4');?>" type="video/mp4">
				</video>
			</div>
		</div>
	</div>
</div>

<div class="container howitworks-pork-holder">
	<div class="row">
		<div class="col-lg-12">
			<div class="howitworks-pattern-holder text-center">
				<img class="img-responsive img-center pork-icon" src="<?php echo base_url('public/'.STORE.'/images/design-icon-large.png');?>" alt=""/>
				<h1 class="howitworks-text-label text-uppercase gsemibold font-medium gray-darker">DESIGN SOMETHING GREAT</h1>
				<p class="text-center font-regular gregular gray-dark">Use our simple design tools to quickly finish your shirt, Your way.</p>
				<div class="dotted-devider-down"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="howitworks-pattern-holder text-center">
				<img class="img-responsive img-center pork-icon" src="<?php echo base_url('public/'.STORE.'/images/buy-icon-large.png');?>" alt="" />
				<h1 class="howitworks-text-label text-uppercase gsemibold font-medium gray-darker">BUY IT</h1>
				<p class="text-center font-regular gregular gray-dark">You can just get a shirt, or build a campaign of up to 6 shirts, to raise funds for causes that matter to you.</p>
				<div class="dotted-devider-right"></div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="howitworks-pattern-holder text-center">
				<img class="img-responsive img-center pork-icon" src="<?php echo base_url('public/'.STORE.'/images/campaign-icon-large.png');?>" alt="" />
				<h1 class="howitworks-text-label text-uppercase gsemibold font-medium gray-darker">CREATE A CAMPAIGN</h1>
				<p class="text-center font-regular gregular gray-dark">You can just get a shirt, or build a campaign of up to 6 shirts, to raise funds for causes that matter to you.</p>
				<div class="dotted-devider-left"></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="howitworks-pattern-holder text-center">
				<img class="img-responsive img-center pork-icon" src="<?php echo base_url('public/'.STORE.'/images/rockit-icon-large.png');?>" alt="" />
				<h1 class="howitworks-text-label text-uppercase gsemibold font-medium gray-darker">ROCK IT</h1>
				<p class="text-center font-regular gregular gray-dark">Promote and share your custom campaign page with friends, family and supporters.</p>
				<div class="dotted-devider-top"></div>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
			<div class="howitworks-pattern-holder text-center">
				<img class="img-responsive img-center pork-icon" src="<?php echo base_url('public/'.STORE.'/images/getpaid-icon-large.png');?>" alt="" />
				<h1 class="howitworks-text-label text-uppercase gsemibold font-medium gray-darker">GET PAID</h1>
				<p class="text-center font-regular gregular gray-dark">Promote and share your custom campaign page with friends, family and supporters.</p>
				<div class="dotted-devider-top"></div>
			</div>
		</div>
	</div>
</div>
<?php 
$this->load->module('campaign/Campaigns_model');
$this->load->helper('textparser');
$featured = $this->Campaigns_model->get_all_featured_campaigns(); 
if( count( $featured) > 0 ):?>
<div class="container-fluid howitworks-featured-holder">
	<div class="container howitworks-featured-inner">
		<div class="howitworks-featured-label text-center gregular font-large gray">Featured Campaigns</div>
		<div class="row">
			<?php foreach (array_parseToString( $featured ) as $key => $campaign) {  
			$quantity = $this->Campaigns_model->get_all_featured_campaigns_count_sold( $campaign['campaign_id']); 
			$img = (!empty($campaign['thumbnail1']))? str_replace('/200/200', "",$campaign['thumbnail1']) : str_replace('/200/200', "",$campaign['thumbnail']); ?>
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
				<div class="sales-counter"><span class="font-medium white text-uppercase"><?php echo (!empty($quantity['quantity']))? $quantity['quantity'] : '0'; ?> Sold</span></div>
					<div class="campaign-image-holder" style="background:#<?php echo $campaign['shirt_background_color']; ?>;">
						<a href="<?php echo base_url($campaign['campaign_url']);?>"><img class="image-overflow-center" src="<?php echo $img.'/200/200'; ?>" alt="" /></a>
					</div>
				<a class="shirt-name-link" href="<?php echo base_url($campaign['campaign_url']);?>"><h3 class="gsemibold font-medium title-c-height"><?php echo ucwords(ucfirst($campaign['campaign_title'] )); ?></h3><span><?php echo db_toString( $campaign['school_name'] ); ?></span></a>
			</div>
			<?php } ?>
		</div>
		<div class="howitworks-featured-label text-center gregular font-large gray">Lorem Ipsum ipsum lorem loren</div>
		<div class="howitworks-featuredbtn-holder text-center"><a href="<?php echo base_url('school/search'); ?>"><button class="howitworks-getstarted-btn2 green-btn gsemibold font-regular white">Get Started</button></a></div>
	</div>
</div>

<?php endif;?>

<script type="text/javascript">
$(document).ready(function(){
	$("#storetitle").text('TZilla.com - How It Works');

	$(document).on("click", ".howitworks-getstarted-btn, .howitworks-getstarted-btn2", function(){
		$(".start-design-btn").click();
	});
});
</script>
