<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/journal.css');?>">
<div class="container-fluid journal-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1 class="white gregular font-xlarge">TZilla Journal</h1>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="container">
		<div class="row journal-body-marg">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="journal-left-cont">
					<div class="journal-pic"></div>
					<div class="journal-details">
						<h2 class="journal-title gregular blackz">My Daily Reading List</h2>
						<p class="gregular font-regular gray-dark text-left">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor aeneanssa. Cum</br> sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,</br>ultricies nec, pellentesque eu, pretium quis, sem. Etiam rhoncus maecenas tempus tellus eget.</p>
						<button class="journal-continueread-btn text-uppercase text-center gregular font-xsmall">continue reading <img class="continueread-btn-icon" src="<?php echo base_url('public/'.STORE.'/images/arrow-right-xs.png');?>" alt="" /></button>
					</div>
					<div class="journal-details-footer">
						<div class="journal-profile-holder">
							<div class="journal-profile">
								<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/sample-profile.jpg');?>" alt="" />
							</div>
							<div class="journal-profile-detail">
								<div class="journal-date gregular font-xxsmall gray">Posted on 18th March 2015 by Mike Ross</div>
								<div class="journal-fav gregular font-xxsmall gray">10 favorites</div>
								<div class="journal-comm gregular font-xxsmall gray">12 comments</div>
								<div class="journal-adds gregular font-xxsmall gray">life style, friends, wonderful</div>
							</div>
						</div>
						<div class="journal-icon-holder">
							<span class="journal-icon-heart"></span>
							<span class="journal-icon-share"></span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				
				<div class="journal-left-cont">
					<div class="journal-pic2"></div>
					<div class="journal-details">
						<h2 class="journal-title gregular blackz">A Trip to Forest</h2>
						<p class="gregular font-regular gray-dark text-left">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor aeneanssa. Cum</br> sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis,</br>ultricies nec, pellentesque eu, pretium quis, sem. Etiam rhoncus maecenas tempus tellus eget.</p>
						<button class="journal-continueread-btn text-uppercase text-center gregular font-xsmall">continue reading <img class="continueread-btn-icon" src="<?php echo base_url('public/'.STORE.'/images/arrow-right-xs.png');?>" alt="" /></button>
					</div>
					<div class="journal-details-footer">
						<div class="journal-profile-holder">
							<div class="journal-profile">
								<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/sample-profile.jpg');?>" alt="" />
							</div>
							<div class="journal-profile-detail">
								<div class="journal-date gregular font-xxsmall gray">Posted on 18th March 2015 by Mike Ross</div>
								<div class="journal-fav gregular font-xxsmall gray">10 favorites</div>
								<div class="journal-comm gregular font-xxsmall gray">12 comm</div>
								<div class="journal-adds gregular font-xxsmall gray">life style, friends, wonderful</div>
							</div>
						</div>
						<div class="journal-icon-holder">
							<span class="journal-icon-heart"></span>
							<span class="journal-icon-share"></span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="paginate-holder">
							 <ul class="pagination">
								<li class="disabled"><a href="#">«</a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">»</a></li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="journal-right-cont">
					<div class="journal-label-header gregular white font-medium">Categories</div>
					<ul class="journal-cat-group gregular font-xsmall gray-darker">
						<li>Featured <div class="bg1 white font-xxsmall">10</div></li>
						<li class="active">Lifestyle <div class="bg2 white font-xxsmall">9</div></li>
						<li>Events <div class="bg3 white font-xxsmall">6</div></li>
						<li>Campaign News <div class="bg4 white font-xxsmall">12</div></li>
						<li>Learn <div class="bg5 white font-xxsmall">12</div></li>
					</ul>
				</div>
				<div class="journal-right-cont">
					<div class="journal-label-header gregular white font-medium">Latest Posts</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				
				<div class="journal-banner-cont">
					<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/sample-blog-1.jpg');?>" alt="" />
					<div class="journal-banner-text white gregular font-xsmall text-uppercase">Banner Images</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$(document).on("click", ".journal-continueread-btn", function(){
		window.location.href = get_mainLink()+'journal';
	});
});
</script>