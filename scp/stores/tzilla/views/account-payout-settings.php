<?php $this->load->view_store('account-header');  ?> 
<style type="text/css">
    .sort{
        cursor: pointer;
    }
</style>
<?php 
function sorting( $payout ) {
    switch ( $payout ) {
        case 'ASC':
            return "down";
            break;
        default:
            return "up";
            break;
    }
}
?>
                

    <div class="dash-title">
        <h3 class="title">Payout Settings</h3>
    </div>
    <section class="section">
        <div class="row sameheight-container">
            <div class="col-lg-6 col-md-12">
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <p>Payout is once a week. Campaign managers must submit their payout requests by Wednesday, then check payment will be mailed out the following Tuesday.</p>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6 col-xs-12">
                            <label for="">Set Your Payment Amount</label>
                            <input type="text" class="form-control" id="payout_amount" placeholder="" value="$ <?php echo (!empty($payout_amount['payout_amount']))? number_format($payout_amount['payout_amount'],2) : '0.00'; ?>" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="col-md-offset-1 col-md-5 col-xs-12">
                            <button id="payout_update" class="green-btn cartrev-update-btn white gsemibold font-small margin-btn">Update</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="card card-block sameheight-item">
                   <!--  <div class="card-block"> -->
                   <div class="row form-group">
                        <div class="col-xs-12">
                            <div class="title-block">
                                <h3 class="title">Payout History</h3>
                            </div>
                        </div>
                    </div>
                        
                        <section class="example">
                            <div class="row form-group">
                                <div class="col-md-4 col-xs-12">                                                    
                                       <select id="account_name" class="form-control">
                                        <option value="">Select Account</option>
                                        <?php  foreach ($accounts as $key => $acct) {
                                            echo '<option value="'.$acct->campaign_account_name.'" >'.ucwords(ucfirst($acct->campaign_account_name)).'</option>';
                                        }?>
                                        </select>
                                </div>
                            </div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="gregular font-regular blackz text-center pointer">Date <i class="fa fa-angle-<?php if($sort_by == 'date_created'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-payout" data-table="date_created"></i></th>
                                                <th class="gregular font-regular blackz text-center pointer">Activity/Status <i class="fa fa-angle-<?php if($sort_by == 'payout_status'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-payout" data-table="payout_status"></i></th>
                                                <th class="gregular font-regular blackz text-center pointer">Payout Amount <i class="fa fa-angle-<?php if($sort_by == 'payout_amount'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-payout" data-table="payout_amount"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody class="transaction_payout">
                                            <tr>
                                                <td class="text-center" colspan="3">No Record Yet</td>
                                            </tr> 
                                        </tbody>
                                    </table>
                                </div>                                            
                        </section>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </section>
    
</article>
                


<script type="text/javascript">
$(document).ready(function(){
    $("#storetitle").text('TZilla.com - Account Payout Settings');
});
$(document).on("blur","#payout_amount", function(){
    var thisVal = + Number( $(this).val().replace('$ ', '') );
    
    $(this).val('$ '+numberWithCommas( Number(thisVal).toFixed(2) ));
});

$(document).on("keyup","#payout_amount", function(){
    if( $(this).val() != "" ){
        $(this).removeClass("error");
    }
});

$(document).on("click","#payout_update", function(){
    var payouttext = $("#payout_amount").val();
    if ( Number( payouttext.replace('$', '') ) < 75 ) {
        $("#payout_amount").addClass("error");
        $('#conModal').modal('show');
        $(".text-confirm").text( 'Payment amount should not less than $75' );
        setTimeout(function() {  $(".verify-close").click(); }, 3500);  
    }else{        
       payout_update(); 
    }

     if( payouttext == "" ){
         $('#conModal').modal('show');
        $(".text-confirm").text( 'Please input payout amount.' );
        setTimeout(function() {  $(".verify-close").click(); }, 3500);
    }

});

$(document).on("change", "#account_name", function(){
        var account_name = $(this).val();
        var ajaxURL = get_mainLink()+'account/get_campaigns_user_account/';
         $(".transaction_payout").html('');
        if( account_name != ""){
            $.post( ajaxURL, { name: account_name }, function(data){
                
                var data = JSON.parse(data);
                var htmlContent = "";

                if (data.length > 0) {
                    for ( var i = 0; i < data.length; i++ ) {
                       $(".transaction_payout").html(data);
                    }
                   
                }else{
                    $(".transaction_payout").html('<tr><td class="text-center" colspan="3">No Record Yet</td></tr>');
                }

            }); 
        }            
});

function payout_update(){
    var ajaxURL = get_mainLink()+'account/payout_update/';
    var amount = $("#payout_amount").val();         
    $.post( ajaxURL, {
        amount: amount.replace('$ ', '')
      }, function(data){   
          $('#conModal').modal('show');
            $(".text-confirm").text( 'Successfully Updated.' );
            setTimeout(function() {  $(".verify-close").click(); }, 3500);      
    });
}
</script>
