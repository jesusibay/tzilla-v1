<?php $this->load->view_store('inner-header');  ?>

<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/cart-review.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/productsizechart.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/css/settings.css');?>">
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/cart-review.js').'?df='.rand(1, 99999);?>"></script>

<script type="text/javascript">
	setSchoolId( '<?php echo $school_id; ?>' );
	setSchoolName( '<?php echo $school_name; ?>' );
	setCartItems( '<?php echo json_encode( $items ); ?>' );
	setProductDetails( '<?php echo json_encode( $product_details ); ?>' );
	setShirtColors( '<?php echo json_encode( $shirt_colors ); ?>' );
	setSizeArrangement( '<?php echo json_encode( $this->config->item("shirt_sizes") ); ?>' );
	setColorsLists( '<?php echo $colors_lists; ?>' );
	setAssetUrl( '<?php echo base_url("public/".STORE); ?>' );
	setShirtWeight( '<?php echo json_encode( $this->config->item("weight_per_shirt") ); ?>' );	
	setTgenValue( '<?php echo $this->session->last_tgen_mode; ?>' );
</script>
<div class="container-fluid cart-review-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
				<h1 class="white">Review Your Cart</h1>
				<p class="gray">Select the shirt styles, colors, and sizes you'd like to purchase for each design in your collection.</p>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row review-design-header">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12 create-camp-instead text-left" style="display:none;">
					<img class="img-responsive camp-horn" src="<?php echo base_url('public/'.STORE.'/images/create-a-campaign.png');?>" alt="" />
					<a href="javascript:void(0);" class="create-camp-link gregular font-large create-campaign-instead">Create a Campaign instead?</a>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
					<button class="orange-btn cartrev-update-btn white gsemibold font-small update-design-items">EDIT YOUR ITEMS</button>
				</div>				
			</div>	
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row review-design-section">
		<div class="container">
			<div class="row cart-items">

			</div>	
		</div>
		
		
		
		<!--DESIGN CART-->
		<div class="design-container">
			<div class="design-cont-btn dc-open"></div>
			<div class="design-cont-header gsemibold font-medium white text-center">Subtotal</div>
			<div class="reviewdesign-cont-item-holder">
				<div class="total-items-holder">
					<div class="gregular font-regular blackz pull-left">Total Items</div>
					<div class="gsemibold font-medium gray-dark pull-right total-items-count">0</div>
					<div class="clearfix"></div>
				</div>
				<div class="total-border">
					<div class="gitalic font-small gray-darker pull-left"><strong>Total Price</strong></div>
					<div class="gsemibold font-medium blackz pull-right total-items-amount">$ 0.00</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="design-cont-footer">
				<button class="gray-btn proceed-btn white gsemibold font-small">Proceed to Checkout<span class="proceed-loader">
					<img class="img-responsive img-loader" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt=""/>
				</span></button>
			</div>
		</div>
		
		
		
	</div>
</div>



<!-- Modal confirm-->
<div id="confirm" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this item?</div>
						<!-- <div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div> -->
						<div class="warning-label2 gregular font-regular gray text-center">All style colors and set quantities will be lost and this cannot be undone.</div>
						<div class="confirm-btn-holder text-center">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<button class="green-btn nogoback-btn white gsemibold font-small">No, go back to the page</button>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<button class="orange-btn yesremove-btn white gsemibold font-small remove-item">Yes, remove the item</button>
						</div>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Modal zoom-->
<div id="modzoom" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content zoom-modal-holder">
      <div class="pop-header-notxt white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large popup-close" data-dismiss="modal"></span>
	  </div>
	   <div class="container-fluid">
				<div class="row item-image-background">
					<div class="col-lg-12 text-center">
					<div class="img-zoom-holder"><img class="img-responsive img-center zoomed-image" src="<?php echo base_url('public/'.STORE.'/images/sample-design.png');?>" alt="" /></div>
					</div>
				</div>
		</div>

	</div>
  </div>
</div>


<!-- Modal sizechart-->
<div id="sizechart" class="modal fade" role="dialog">
  	<div class="modal-dialog cantfind-dialog" style="width: 671px;">

	    <!-- Modal content-->
	    <div class="modal-content sizechart-modal-holder">
	      	<div class="confirm-header white gregular font-medium">
				<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
		  	</div>
		  	<div class="container-fluid modal-bg">
					<div class="row">
						<div class="col-lg-12 text-center measure-pad ">
							
							<div class="sizechart-label1 gregular font-large blackz text-center sizechart-lbl">Size Chart for <span class="style-label">Style Name</span></div>
							
							<div class="cont-holder size-chart-info">
								<p class="gray gregular font-small text-left ">Classic:<br>
By far our best selling tee. Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. Check out the specs for the actual measurements and dimensions before you order.</p>
							</div>
							<div class="table-responsive size-chart-data"> 
								<table class="table table-bordered text-left font-small size-chart-table">
									<thead>
										<tr>
											<th class="gsemibold blackz">Body Specs</th>
											<th class="gsemibold blackz">S</th>
											<th class="gsemibold blackz">M</th>
											<th class="gsemibold blackz">L</th>
											<th class="gsemibold blackz">XL</th>
											<th class="gsemibold blackz">2XL</th>
											<th class="gsemibold blackz">3XL</th>
											<th class="gsemibold blackz">Tolerance</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="gregular blackz">Body Width</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Body Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Sleeve Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Arm hole</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view_store('footer'); ?>