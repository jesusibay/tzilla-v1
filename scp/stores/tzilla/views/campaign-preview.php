<?php $this->load->view_store('header');  ?>  
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/createcampaign.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/jquery.countdown.css');?>">
<script src="<?php echo base_url('public/'.STORE.'/js/jquery.plugin.js');?>"></script>
<script src="<?php echo base_url('public/'.STORE.'/js/jquery.countdown.js');?>"></script>

<?php 
$new_campaign_end1 = "";
$new_start1 = "";
$campaign_percentage = 0;
$sub = 0;

if( isset($campaigns) || isset($payee_details) ){
	foreach ($campaigns as $campaign) {} 
	foreach ($payee_details as $payee) {} 

	$date1 = new DateTime( $campaign['end_date'] );
	$new_campaign_end1 = $date1->format('F d, Y H:i:s');
	$startdate1 = new DateTime( $campaign['end_date'] );
	$new_start1 = $startdate1->format('F d, Y H:i:s');
	$today1 = date("Y-m-d H:i:s");

	if( $customer_id !== $campaign['campaign_user_id'] || $this->session->is_logged_in==FALSE ){
		redirect( base_url() );
	}
	
	$sales = $this->Campaigns_model->get_campaign_sales( $campaign['campaign_url']);
    $total_base_price = $this->Campaigns_model->get_campaign_base_price( $campaign['campaign_url']);
    $sub = $total_base_price['total_profit'];
	$campaign_percentage = round( ( $sub / $campaign['campaign_goal'] ) * 100, 2 );
}

if($campaign_percentage > 100){
	$width = 100;
}else{
	$width = $campaign_percentage;
}

?>
<input type="hidden" id="customer_id" value="<?php echo $customer_id; ?>">
<input type="hidden" id="user_email" value="<?php echo $this->session->logged_in_email; ?>">
<input type="hidden" id="edit_campaign" value="<?php echo $edit_campaign; ?>">
<input type="hidden" id="campaign_id" value="<?php if(isset($campaign)) echo $campaign_id; ?>">
<input type="hidden" id="status" value="<?php if(isset($campaign)): echo $campaign['campaign_status']; else: echo "0"; endif; ?>">
<input type="hidden" id="campaign_feature" value="<?php if(isset($campaign)): echo $campaign['campaign_feature']; else: echo "0"; endif; ?>">
<input type="hidden" id="school_id" value="<?php echo (isset($campaign))? $campaign['school_id'] : $this->session->school_id; ?>"> <!-- school id -->
<input type="hidden" id="supplier_code" value="<?php if(isset($campaign)) $campaign['supplier_code']; ?>">
<input type="hidden" id="number_sold" value=""> <!-- use for progress bar -->
<?php if(isset($campaign)): if( $campaign['campaign_cover_photo'] == 1 ){ ?><img src="<?php echo base_url('public/tzilla/campaigns').'/'.$campaign['campaign_url'].'.jpg'; ?>" id="checkphoto_path" style="display:none;"/> <?php } endif; ?>

<section class="container-fluid">
	<div class="row">
		<div class="camp-header-holder" <?php if(isset($campaign)): if( $campaign['campaign_cover_photo'] == 1 ){ $img = base_url('public/tzilla/campaigns').'/'.$campaign['campaign_url'].'.jpg';  echo 'style="background:url('.$img.'); background-repeat:no-repeat; background-position:center; background-size: cover;"'; } endif; ?> >
			<div class="camp-prev-label gsemibold font-xlarge white text-uppercase text-center">Campaign Preview</div>
			<div class="container">
				<div class="camp-upload-holder-outer">
					<div class="camp-upload-holder-inner">
						<div class="<?php // if(isset($campaign)) echo ($campaign['campaign_cover_photo'] == 1)? 'camp-remove-cover-btn' : 'camp-upload-btn'; ?>camp-upload-btn">
							<div class="camp-upload-btn-left white gsemibold font-regular"><?php // if(isset($campaign)) echo ($campaign['campaign_cover_photo'] == 1)? 'Remove Cover Photo' : 'Upload Your Cover Photo'; ?>Upload Your Cover Photo</div>							
							<div class="<?php // if(isset($campaign)) echo ($campaign['campaign_cover_photo'] == 1)? 'camp-remove-cover-btn-right' : 'camp-upload-btn-right'; ?>camp-upload-btn-right"></div>
						</div>
						<input type="file" id="cover_photo" style="display:none;">
						<input type="hidden" id="cover_photo_int" value="<?php if(isset($campaign)): echo $campaign['campaign_cover_photo']; else: echo "0"; endif; ?>">

						<div class="camp-upload-btn-label white gitalic font-small text-center">Recommended Specs: 1920x1080 pixels, .jpg format</div>
						<div class="camp-upload-btn-label white gitalic font-small text-center">Maximum file size is 2MB.</div>
					</div>
					
				</div>
				
				<div class="camp-title-holder">
					<h1 class="camp-title white gregular"><?php if(isset($campaign)): echo $campaign['campaign_title']; else: echo "Campaign Title"; endif; ?></h1>
					<div class="camp-sub-title white gregular font-large"><?php if(isset($campaign)): echo db_toString($campaign['school_name']); else: echo "School Name"; endif; ?></div>
					<p class="camp-p white gregular font-small"><?php if(isset($campaign)): echo db_toString($campaign['school_address_1']); else: echo "School Address"; endif; ?></p>
					<!-- <div class="cam-title-dash"></div> -->
				</div>
			</div>
			<div class="gradient"></div>
		</div>
	</div>
</section>
<section class="container-fluid">
	<div class="row">
		<div class="camp-prev-cont-holder">
			<div class="container">
				<div class="row">
					<div class="col-lg-3">
						<div class="campaign-cta-holder"><div class="dropdown">
							<button class="btn dropdown-toggle manage-camp-btn gsemibold" type="button" id="menu1" data-toggle="dropdown">Manage Campaign
							<span class="camp-btn-caret caret pull-right"></span></button>
							<ul class="dropdown-menu manage-camp-option" role="menu" aria-labelledby="menu1">
							  	<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('campaign/edit_items');if(isset($campaign)) echo '/'.$campaign_id; ?>">Manage Items</a></li>
							  	<li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0);" class="update-campaign-details">Update Campaign Details</a></li>
							</ul>
						 </div></div>
					</div>
					<div class="row camp-cta-outer">
						<div class="col-lg-3">
							<div class="campaign-cta-holder"><button class="orange-btn gsemibold font-regular white save-changes">Save Changes</button></div>
						</div>
						<div class="col-lg-3">
							<div class="campaign-cta-holder"><?php if(isset($campaign)): if( $campaign['campaign_status'] == 0){ ?><button class="green-btn gsemibold font-regular white publish" data-toggle="modal" data-target="#confirm-launch" data-dismiss="modal">Launch Campaign</button><?php } endif; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-lg-3">
					<div class="camp-sidebar">
						
					  <div class="campaign-title-div camp-sidebar-title gregular font-medium blackz"><span class="edit-title"><?php if(isset($campaign)): echo $campaign['campaign_title']; else: echo "Campaign Title"; endif; ?></span><span class="edit-lbl1 font-small">&nbsp;(edit)</span></div>
					  <p class="campaign-des-p camp-sidebar-p gregular font-small gray-dark"><span class="edit-desc"><?php if(isset($campaign)): echo $campaign['campaign_description']; else: echo "Create a short description for your campaign. Your visitors will read this first, so be sure to include the most relevant information here."; endif; ?></span><span class="edit-lbl2 font-small">&nbsp;(edit)</span></p>
					  <hr class="camp-devider">
					  <div class="camp-fundraised-title gregular font-medium blackz">Funds Raised</div>
					  <div class="camp-todate gregular font-small gray">To Date</div>
					  <div class="sidebar-loadrail"><div class="sidebar-loadgreen" style="width:<?php echo $width; ?>%;"><span class="sidebar-tooltip p-hover" style="display:none;">$<?php echo number_format($sub,2); ?></span></div></div>
					  <div class="camp-goal-min gregular font-small gray-dark pull-left">$0.00</div>
					  <div class="camp-goal-max gregular font-small gray-dark pull-right">$<?php if(isset($campaign)): echo number_format($campaign['campaign_goal'],2); else: echo "0"; endif; ?></div>
					  <div class="clearfix"></div>
					  <div class="camp-adjust-label font-medium gregular blackz">Adjust Campaign Goal</div>
					  
					  <input type="text" id="input-goal" class="camp-input form-control gregular font-medium blackz numberwithdecimal" value="<?php if(isset($campaign)): echo "$".number_format($campaign['campaign_goal'],2); else: echo "$".number_format(500,2); endif; ?>" onkeypress="return isNumberKey(event)"/>
					  <label class="error-input-goal"></label>
					  <hr class="camp-devider2">
					  <div class="camp-length-title gregular font-medium blackz">Adjust Campaign Duration</div>
					  <div class="camp-length-holder">
						<div class="camp-length-left-sidebar camp-margin-bot">
							<div class="camp-duration-cont gsemibold font-small gray-dark">Start</div>
						    <input type='text' class="form-control camp-date" id="datepicker1" value="<?php if(isset($campaign)) echo date("Y-m-d", strtotime($campaign['start_date'])); ?>" />
						</div>
						<div class="camp-length-left-sidebar">
							<div class="camp-duration-cont camp-pad-left gsemibold font-small gray-dark">End</div>
							<input type='text' class="form-control camp-date" id="datepicker2" value="<?php if(isset($campaign)) echo date("Y-m-d", strtotime($campaign['end_date'])); ?>" />
						</div>
						<div class="clearfix"></div>
					  </div>
					  
					  <div class="camp-lastday-title gregular font-medium blackz">This Campaign ends in</div>
					  <div id="camptimer" class="gregular font-small gray-dark text-center"></div>
					  <div class="camp-report-abuseholder camp-report-abuse gregular font-regular text-uppercase">
					  	<span class="report-abuse-lbl">Report Abuse</span>
					  	<span class="q-icon">
							<div class="q-pop">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div>
						</span>
					  </div>
					  <hr class="camp-devider3">
					  <button class="sidebar-btn gray-btn gsemibold font-regular white save-changes">Save Changes</button>
					  <?php if(isset($campaign)): if( $campaign['campaign_status'] == 0){ ?><button class="sidebar-btn green-btn gsemibold font-regular white publish" data-toggle="modal" data-target="#confirm-launch" data-dismiss="modal">Launch Campaign</button><?php } endif; ?>
					</div>
				</div>
				<div class="col-lg-9 camp-cta-outer-holder">
					
					<div class="row">

						<?php if(isset( $campaign_designs )):
						foreach ($campaign_designs as $key => $design) {  
						$type = $this->Campaigns_model->get_by_campaign_design_type( $design['template_type_id'] ); 
						$parent = $this->Campaigns_model->get_by_campaign_design_parent( $design['template_id'] );  
						$img = (!empty($design['thumbnail1']))? str_replace('/200/200', "",$design['thumbnail1']) : str_replace('/200/200', "",$design['thumbnail']); ?>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
							<div class="camp-design-item-holder">
								<div class="camp-design-label gregular font-xsmall gray text-uppercase"><span class="design-name gsemibold"><?php echo $design['display_name']; ?></span></div>
								<div class="camp-image-box" style="background:#<?php echo $design['shirt_background_color']; ?>;">
									<img class="img-responsive img-center" src="<?php echo $img.'/200/200'; ?>" alt="" />
								</div>
								<input type="button" class="<?php echo($campaign['campaign_status'] == 3)? 'campaign-ended-btn' : 'camp-update-btn'; ?> update-btn gsemibold font-regular" product-id="<?php echo $design['product_id']; ?>" data-id="<?php echo $design['template_id']; ?>" product-id="<?php echo $design['product_id']; ?>" data-parent="<?php echo (!empty($parent['parent_template_id']))? $parent['parent_template_id'] : $design['template_id']; ?>" <?php echo($campaign['campaign_status'] == 3)? 'disabled="disabled"' : ''; ?> value="Edit">
							</div>
							<?php $ready = $this->Campaigns_model->get_campaign_product_items( $design['product_id']);
								$arr_shirt_plan = Array();
						        foreach ($ready as $key => $items) {
						            $colors = $this->Campaigns_model->get_campaign_colors_by_style_id($design['product_id'], $items['shirt_plan_id']);
						            $cols = "";
						            foreach ($colors as $col) {
						                $cols .= $col.',';        
									}
						            echo '<input type="hidden" class="shirt-plan-id" plan-id="'.$items['shirt_plan_id'].'" data-price="'.$items['price'].'" data-properties="'.$items['template_properties'].'"  design-id="'.$items['design_id'].'" background-color="'.$items['bg_color'].'" data-art-position="'.$items['art_setting'].'" data-canvas-position="'.$items['canvas_setting'].'" data-colors="'.rtrim($cols,',').'">';  
						        }
								?>
						</div>
						<?php }
						else:
							if(isset( $product )){
							foreach ($product as $product_id) {  
							$designs = $this->Campaigns_model->get_by_design( $product_id );
							$type = $this->Campaigns_model->get_by_campaign_design_type( $designs['template_type_id'] ); 
							$parent = $this->Campaigns_model->get_by_campaign_design_parent( $designs['template_id'] );  
							$img = (!empty($designs['thumbnail1']))? str_replace('/200/200', "",$designs['thumbnail1']) : str_replace('/200/200', "",$designs['thumbnail']);?>
							<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
								<div class="camp-design-item-holder">
									<div class="camp-design-label gregular font-xsmall gray text-uppercase"><span class="design-name gsemibold"><?php echo $designs['display_name']; ?></span></div>
									<div class="camp-image-box" style="background:#<?php echo $designs['shirt_background_color']; ?>;">
										<img class="img-responsive img-center" src="<?php echo $img.'/200/200'; ?>" alt="" />
									</div>
									<input type="button" class="camp-update-btn update-btn gsemibold font-regular" product-id="<?php echo $designs['product_id']; ?>" data-id="<?php echo $designs['template_id']; ?>" data-parent="<?php echo (!empty($parent['parent_template_id']))? $parent['parent_template_id'] : $designs['template_id']; ?>"  value="Edit">
								</div>
							</div>
							<?php }
						 	}
						 endif; ?>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<!-- Modal -->
<div id="setcampdetails" class="modal fade" role="dialog">
   <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content login-modal-holder">
      <div class="setcamp-header white gregular font-medium text-center">
			Set Campaign Details
			<?php if(isset($campaign)) echo '<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>'; ?>			
	  </div>
		<div class="container-fluid">
			<div class="setup-cont">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">1. Set Your Campaign Goal</label>
							<div id="slider-range-min"><div id="custom-handle" class="ui-slider-handle"></div></div>
							<input type="text" name="campaignamount" id="amount" class="camp-input form-control gsemibold font-small blackz trim-height numberwithdecimal" onkeypress="return isNumberKey(event)" value="<?php if(isset($campaign)): echo "$".number_format($campaign['campaign_goal'],2); else: echo "$".number_format(500,2); endif; ?>"/>
						</div>
						<div class="form-group">
							<?php $today = date("Y-m-d");
				              $date1 = str_replace('-', '/', $today);
				              $tomorrow = date('Y-m-d',strtotime($date1 . "+30 days")); ?>
							<label class="blackz gsemibold" for="">2. Campaign Duration</label>
							<div class="camp-duration">
								<div class="camp-length-left handler">
									<div class="camp-duration-cont gsemibold font-small gray-dark">Start</div>
									<input type='text' name="campaignstartdate" data-name="Start Date" class="form-control camp-date start_date required" id="datepicker3" value="<?php if(isset($campaign)): echo date("Y-m-d", strtotime($campaign['start_date'])); else: echo $today; endif; ?>"/>	
								</div>
								<div class="camp-length-right handler">
									<div class="camp-duration-cont gsemibold font-small gray-dark">End</div>
									<input type='text' name="campaignenddate" data-name="End Date" class="form-control camp-date end_date required" id="datepicker4" value="<?php if(isset($campaign)): echo date("Y-m-d", strtotime($campaign['end_date'])); else: echo $tomorrow; endif; ?>"/>
								</div>
								<div class="clearfix campaign-duration-div"></div>
							</div>
						</div>
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">3. Campaign Title</label>
							<input type="text" name="campaigntitle" id="campaign_title" data-name="Campaign Title" class="form-control gregular font-small gray trim-height camp-text required" data-holder="eg. Fundraiser for Poway High School" Placeholder="eg. Fundraiser for Poway High School" value="<?php if(isset($campaign)) echo $campaign['campaign_title'];?>" maxlength="100"/>
						</div>
						<div class="form-group handler">
							<label class="blackz gsemibold" for="" style="display:block;">4. Custom URL</label>
							<div class="camp-custom-url-holder"><div class="camp-custom-url gsemibold font-regular gray-darker trim-height">tzilla.com/</div><input type="text" name="campaignurl" id="campaign_url" data-name="Campaign URL" class="form-control gregular font-small gray camp-custom-urltext trim-height camp-text required" Placeholder="This is where you will send buyers to view your campaign." data-holder="This is where you will send buyers to view your campaign." value="<?php if(isset($campaign)) echo $campaign['campaign_url'];?>" <?php if(isset($campaign)) if( $campaign['campaign_status'] != 0) echo 'readonly="readonly"'; ?>/></div>
							<div class="clearfix"></div>
						</div>
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">5. Tell Us Your Story</label>
							<textarea name="campaigndescription" id="campaign_description" data-name="Campaign Description" class="form-control camp-text required" rows="5" placeholder="Create a short description for your campaign. Your visitors will read this first, so be sure to include the most relevant information here." data-holder="Create a short description for your campaign. Your visitors will read this first, so be sure to include the most relevant information here."><?php if(isset($campaign)) echo $campaign['campaign_description'];?></textarea>
						</div>
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">6. Who or what will benefit from this Campaign?</label>
							<input type="text" name="campaignbenefit" id="campaign_benefit" data-name="Campaign Beneficiary " class="form-control gregular font-small gray trim-height camp-text required" Placeholder="eg. Funds raised will benefit TECHNOLOGY, MUSIC, ART, PE and SCIENCE." data-holder="Funds raised will benefit TECHNOLOGY, MUSIC, ART, PE and SCIENCE." value="<?php if(isset($campaign)) echo $campaign['campaign_benefits'];?>"/>
						</div>
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">7.Keywords</label>
							<div class="gregular font-xsmall gray" style="margin-bottom: 5px;">Tags will allow buyers to find your fundraiser on social media. Here's a few to get you started:</div>
							<input name="" id="singleFieldTags2" type="text" name="keywords" class="form-control gregular font-small gray campaign-keywords" value="<?php if(isset($campaign)) echo $campaign['campaign_hashtags'];?>"/>
						</div>
						<div class="row setup-footer">
							<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
							<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
								<button id="<?php if($edit_campaign === 0): echo "back_btn1"; endif;?>" class="setup-footer-btn orange-btn gsemibold font-regular white" data-dismiss="modal">Go Back</button>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
								<button id="next_btn1" class="setup-footer-btn green-btn gsemibold font-regular white validate-first-detail">Next</button>
							</div>
							<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
						</div>
					</div>
				</div>
		
			</div>	
			

			<div class="payeedetail-cont">
				<div class="payeedetail-label blackz gsemibold">All payouts will be delivered via check. Please fill out the information below so we know to whom we’ll deliver the funds.</div>
				<div class="form-group handler">
					<label class="blackz gsemibold" for="">Full Name of Payee</label>
					<input type="text" id="account_name"  name="accountname" data-name="Payee Full Name" class="form-control gregular font-small gray trim-height required2" Placeholder="" value="<?php if(isset($payee)) echo $payee['name'];?>" />
				</div>
				<div class="form-group handler">
					<label class="blackz gsemibold" for="">Address 1</label>
					<input type="text" id="account_add1" name="accountadd1" data-name="Address" class="form-control gregular font-small gray trim-height required2" Placeholder="" value="<?php if(isset($payee)) echo $payee['address_1'];?>" />
				</div>
				<div class="form-group">
					<label class="blackz gsemibold" for="">Address 2</label>
					<input type="text" id="account_add2" class="form-control gregular font-small gray trim-height" Placeholder="" value="<?php if(isset($payee)) echo $payee['address_2'];?>"/>
				</div>
				<div class="payeedetail-inner">
					<div class="payeedetail-left">
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">Contact Number</label>
							<input type="text" id="campaign_contact" name="campaigncontact" data-name="Contact" class="form-control gregular font-small gray trim-height camp-text required2" Placeholder="Your Contact Number (xxx) xxx-xxxx" data-holder="Your Contact Number" onkeypress="return isNumberKey(event)" value="<?php if(isset($payee)) echo $payee['contact_no'];?>"/>
						</div>
					</div>
					<div class="payeedetail-right">
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">City</label>
							<input type="text" id="account_city" name="accountcity" data-name="City" class="form-control gregular font-small gray trim-height camp-text text-input required2" Placeholder="Your City" data-holder="Your City" value="<?php if(isset($payee)) echo $payee['city'];?>"/>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="payeedetail-left handler">
						<label class="blackz gsemibold" for="">State</label>
						<select id="account_state" name="accountstate" data-name="State" class="form-control gitalic camp-select trim-height required2">
								<option value="">Select a State</option>
                            <?php foreach ($states as $key => $val) {?>
                                <option data-value="<?php echo $val['state_name'];?>" <?php if(isset($payee)): if( $val['state_code'] == $payee['state'] ){echo 'selected="selected"';} endif; ?>  value="<?php echo $val['state_code'];?>"><?php echo ucfirst($val['state_name']);?></option>
                            <?php } ?>
						</select>
					</div>
					
				</div>
				<div class="payeedetail-right">
						<div class="form-group handler">
							<label class="blackz gsemibold" for="">Zip Code</label>
							<input type="text" id="account_zip" name="accountzip" data-name="Zip" class="form-control gregular font-small gray trim-height camp-text required2" Placeholder="Zip Code" data-holder="Zip Code" onkeypress="return isNumberKey(event)" value="<?php if(isset($payee)) echo $payee['zip'];?>" />
						</div>
				</div>
				<div class="clearfix"></div>
					<div class="payeedetail-checkl"><span class="checker-box"></span></div>
					<div class="payeedetail-checkr"><span class="checker-box-agree gregular font-regular">I agree to the Terms & Conditions and confirm that all content, copy, used in my fundraiser do not infringe upon the rights of a third party.</span></div>
				<div class="row setup-footer">
					<!-- <div class="col-lg-2"></div> -->
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
					<div class="col-lg-5">
						<button id="back_btn2" class="setup-footer-btn orange-btn gsemibold font-regular white">Go Back</button>
					</div>
					<div class="col-lg-5">
						<button id="next_btn2" class="save-campaign setup-footer-btn gray-btn gsemibold font-regular white validate-second-detail" disabled="disabled">Next
						<span class="next-loader">
								<img class="img-responsive img-loader" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt=""/>
							</span>
						</button>
					</div>
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
				</div>
			</div>

		</div>
	</div>
	</div>
</div>


<!-- Modal confirm-->
<div id="confirm-launch" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="setcamp-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row confirmlaunch-cont">
					<div class="col-lg-12 text-center">
						<div class="warning-img-holder camp-img-holder"><img class="img-responsive img-center megaphone-img" src="<?php echo base_url('public/'.STORE.'/images/megaphone-large.jpg');?>" alt="" /></div>
						<div class="warning-label1 gregular font-large blackz text-center camp-ready-launch">Are you sure you’re ready to launch your Campaign?</div>
						<div class="warning-label2 gregular font-regular gray text-center camp-done-edit">If you’re not done editing your Campaign, you can go back to edit and<br>save your changes.</div>
						<div class="confirm-btn-holder text-center camp-confirm-cont">
							
							<button class="green-btn yeslaunch-btn yesremove-btn white gsemibold font-small">Yes, Launch My Campaign!
							<span class="publish-loader">
									<img class="img-responsive img-loader" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt=""/>
								</span>
							</button>
							<button class="orange-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">Go Back</button>
						</div>
					</div>
				</div>
				
				<div class="row successlaunch-cont">
					<div class="col-lg-12 text-center">
						<div class="warning-img-holder camp-img-holder"><img class="img-responsive img-center megaphone-img" src="<?php echo base_url('public/'.STORE.'/images/megaphone-large.jpg');?>" alt="" /></div>
						<div class="col-md-offset-2 col-md-8">
							<div class="warning-label1 gregular font-large blackz text-center font-congrats camp-congrats">Congratulations! You have launched your Fundraiser!</div>
						</div>
						<!-- <div class="warning-label2 gregular font-regular gray text-center">Now, see your funds sky rocket in the next few days.</div> -->
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">Go back to preview</button>
							<a href="<?php echo base_url('campaign/lists'); ?>"><button class="orange-btn yesremove-btn white gsemibold font-small">Support other Campaigns</button></a>
						</div>
					</div>
					<div class="share-label text-center gregular font-regular blackz">Share this Campaign</div>
					<div class="share-icon-holder">
						<span class="icon-facebook live-fb" data-url="<?php echo base_url($campaign['campaign_url']);?>" data-text="<?php echo $campaign['campaign_title']; ?>" data-via="tzilladotcom"></span>
						<span class="icon-twitter live-twitter" data-url="<?php echo base_url($campaign['campaign_url']);?>" data-text="<?php echo $campaign['campaign_title']; ?>" data-via="tzilladotcom"></span>
						<span class="icon-gplus live-gplus" data-url="<?php echo base_url($campaign['campaign_url']);?>" data-text="<?php echo $campaign['campaign_title']; ?>" data-via="tzilladotcom"></span>
						<span class="icon-email live-email" data-url="<?php echo base_url($campaign['campaign_url']);?>" data-text="<?php echo $campaign['campaign_title']; ?>" data-via="tzilladotcom"></span>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>

<!-- Modal Remove Cover Image Confirmation-->
<div id="removecover" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this cover image?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">This action will completely the cover image<br>
This cannot be undone</div>
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
							<button id="remove_cover_confirm"class="orange-btn yesremove-btn white gsemibold font-small" data-dismiss="modal">Yes, remove the cover image</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>

<?php $this->load->view_store('footer');  ?> 
<link href="<?php echo base_url('public/'.STORE.'/css/jquery.tagit.css');?>" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="<?php echo base_url('public/'.STORE.'/js/campaign-preview.js').'?df='.rand(1, 99999);?>"></script>
<script>
$(document).ready(function(){
	var start_date1 = '<?php echo $new_start1; ?>';
	var ended_date1 = "<?php echo $new_campaign_end1; ?>";
	setDateStarted( start_date1 );
	setDateEnded( ended_date1 );
	$(function() { 
		setInterval(function(){ doCountDown(); }, 1000);
	});

	setcampaignGoal( "<?php if(isset($campaign)): echo $campaign['campaign_goal']; else: echo '500'; endif; ?>" );
	$(document).on("click", ".live-fb", function(e){
    e.preventDefault();
	    FB.ui(
	    {
	      method: 'feed',
	      name: "",
	      link: '<?php if( isset($campaigns)) echo base_url( $campaign["campaign_url"] ); ?>',
	      caption: '<?php if( isset($campaigns)) echo $campaign["campaign_title"]; ?>',
	      picture: $(".camp-image-box").eq(0).find("img").attr('src'),
	      display: 'iframe',
	      message: '',
	      description: '<?php if( isset($campaigns)) echo $campaign["campaign_description"]; ?>'
	    });
    FB.Canvas.setSize({ width: 200, height: 200 });
  });

});
</script>