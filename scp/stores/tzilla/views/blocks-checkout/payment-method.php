
<span class="span-19 ship-label">Payment Method</span>
<span class="span-22" style="display:block;">All transactions are secure and encrypted. Credit card information is never stored.</span>
	
<ul class="list-group">
  <li class="list-group-item shipping-list">
		<input type="radio" name="opt_creditcard_payment" value="" class="blue-circle cc" checked="checked"><span class="span-21">Credit card</span>
		<span class="pull-right span-21 ship-price payment-img"><img class="cc-provide" style="    padding-right: 3px;" src="<?php echo base_url('public/'.STORE.'/images/credit-card-providers.jpg');?>" alt="">and more...</span>
  </li>

  <?php $this->load->view_store('blocks-checkout/wepay-payment-form');?>
  
  <div id="credit-card-notification"></div>
	  
</ul>

<form class="" id="BillingForm" method="post" action="">

	<?php $this->load->view_store('blocks-checkout/billing-address');?>
	
	<div class="form-group return-link-desktop">
	<div class="row">
		<div class="col-lg-12">
			<a href="<?php echo site_url('checkout/shipping');?>"><span class="checkbox-label">< Return to shipping method</span></a>
		</div>
	</div>
	</div>
	
	<div class="form-group">
	<div class="row">
		<div class="col-lg-7">
			<div class="cd-check-holder"><span class="checker-box3" id="agreement_chkbox_span"></span>
			<div style="display:none">
				<input type="checkbox" name="agreement_chkbox" id="agreement_chkbox" value="On" class="paymentinput">
			</div>
			</div>
			<div class="cd-approval">I have reviewed my order for accuracy of spelling, color, sizes, and Super-Customization choices.
			I understand that Teelocity cannot accept returns on customized designs.</div>
		</div>
		<div class="col-lg-5">
			<input type="button" id="cc-submit" title="Check 'I have reviewed my order...' " class="btn btn-primary btn-lg btn-lg4 pull-right complt-ord disabled" disabled="disabled" value="Complete order" />
						
		</div>
	</div>
	</div>
	
	<div class="form-group  news-letter">
	<div class="row">
		<div class="col-lg-12">
			<div class="cd-check-holder"><span class="checker-box4 selected"></span>
				<div style="display:none">
					<input type="checkbox" name="subscribe_chkbox" id="subscribe_chkbox" checked="" class="paymentinput2">
				</div>
			</div>
			<div class="span-11">Subscribe to our newsletter</div>
		</div>
	</div>
	</div>
	
	<div class="form-group return-link">
	<div class="row">
		<div class="col-lg-12">
			<a href="<?php echo site_url('checkout/shipping');?>">
				<span class="checkbox-label">< Return to shipping method</span>
			</a>
		</div>
	</div>
	</div>

	<input type="hidden" name="validation_passed_status" id="validation_passed_status" value="true">
</form>



<script type="text/javascript" src="https://static.wepay.com/min/js/tokenization.v2.js"></script>


<script type="text/javascript">
function check_zip_validity(state, zip){
		if(state)
		{

		 	$.ajax({
				type: "POST",
				url: "<?php echo site_url('us_states/validate_state_zip'); ?>",
		        data: {
		          	state: state,
		          	zip_code: zip
		        },
				dataType:'json'
				}).done(function( data ) 
				{				

					if(data=="true" || data ===true){ 
						$('#validation_passed_status').val("true");
						console.log(data);
					}else{
											
						$('#cc-submit').val('Complete order');
						$('#cc-submit').addClass('disabled');
						$('#cc-submit').prop('disabled', true);

						$('#credit-card-notification').addClass('alert-danger alert');

						$('#credit-card-notification').html(data);

						$('#validation_passed_status').val("false");

						$(".checker-box3").removeClass("selected");
						
						$('.paymentinput').removeAttr('checked');
						
						if($("#agreement_chkbox:checked" ).length) {
					       
							$('#cc-submit').removeClass('disabled');
							$('#cc-submit').prop('disabled', false);	
					    }
					}					
							
				});

		}else{
			$('#cc-submit').val('Complete order');
			$('#cc-submit').addClass('disabled');
			$('#cc-submit').prop('disabled', true);

			$('#validation_passed_status').val("false");

			$(".checker-box3").removeClass("selected");
						
			$('.paymentinput').removeAttr('checked');
			
			if($("#agreement_chkbox:checked" ).length) {
		       
				$('#cc-submit').removeClass('disabled');
				$('#cc-submit').prop('disabled', false);	
		    }

			//$('#credit-card-notification').addClass('alert-danger alert');

			//$('#credit-card-notification').html('State is required.');
		}

	}
(function() {

	var site_url = '<?php echo site_url(); ?>';
	
	<?php		
		if( SITEMODE == "DEV" ){ 
			echo 'var w_endpnt = "stage";';
			echo 'var client_id = 12231;'; //todo: load from database settings
	 	}else{ 
			echo 'var w_endpnt = "production";';
			echo 'var client_id = 121048;'; //todo: load from database settings
		}		
	?>

    WePay.set_endpoint(w_endpnt); // change to "production" when live


	$( "body" ).delegate( "#cc-submit", "click", function() {
	 	console.log('submit clicked');

	 	if($('#cc-number').val().length === 0 )
	 	{
	 		$('#credit-card-notification').html('Invalid credit card number.');	
		 	$('#credit-card-notification').addClass('alert-danger alert');
	 	}else{

		 	//validate 
		 	var cc_expiration = $('#cc-expiration').val().split('/');
		 	if(cc_expiration.length < 2 || cc_expiration[0] == '' || cc_expiration[1] == '' ||
		 		cc_expiration[0] == '0' || cc_expiration[1] == '0'){
		 		$('#credit-card-notification').html('Invalid credit card expiration. Follow this format: MM/YY.');	
		 		$('#credit-card-notification').addClass('alert-danger alert');

		 	}else{	 		

		 		if($('#cc-cvv').val().length === 0){
		 			$('#credit-card-notification').html('Invalid credit card CVV.');	
		 			$('#credit-card-notification').addClass('alert-danger alert');
		 		}else{

				 	var validator = $( "#BillingForm" ).validate();
					validator.form();

					if(validator.valid()){
						var v = is_fieldempty();

						var cc_expiration = $('#cc-expiration').val().split('/');

						var state = $('#ship_state').val();

						if($('.opt_billing_sameas_shipping:checked').val()=="no") state = $('#customer_bill_state').val();

						check_zip_validity(state, $( "#cc-zip" ).val());

						if($('#validation_passed_status').val() == "true"){

							if(v==true){
						        var userName = [$('#cc-name')].join(' ');

						            response = WePay.credit_card.create({
						            "client_id":        client_id,
						            "user_name":        $('#cc-name').val(),
						            "email":            $('#cc-email').val(),
						            "cc_number":        $('#cc-number').val(),
						            "cvv":              $('#cc-cvv').val(),
						            "expiration_month": cc_expiration[0],
						            "expiration_year":  cc_expiration[1],
						            "address": {
						                "zip": $('#cc-zip').val(),
						                "region": state
						            }
						        }, function(data) {
						        	$('#credit-card-notification').html();
						            if (data.error) {
						                // handle error response
						                var error_message = data.error_description;			               
						                var lastChar = error_message[error_message.length -1];
						                
						                if(lastChar != '.') error_message = error_message +'.';

						                $('#credit-card-notification').html(error_message);
						                $('#credit-card-notification').addClass('alert-danger alert');

						                $('#validation_passed_status').val("false");
						            } else {
						            	do_payment(data.credit_card_id);
						            }

						        });
						    }else{
						    	$('#credit-card-notification').addClass('alert-danger alert');
						    	$('#credit-card-notification').html(v);
						    }
						}
					}else{
						console.log('Billing address is incomplete.');
					}

		 		}
			}
		}
	});   

    /*todo: change this with jquery validation*/
    var is_fieldempty = function(){
    	
		var v = true;
	    $("input.required").each(function(){
	        if ($.trim($(this).val()).length == 0 && v==true){
	            $(this).addClass("highlight");
	            v = $(this).attr('name')+" is required.";
	        }
	        else{
	            $(this).removeClass("highlight");	           
	        }

	    });
	            console.log(v+"asd");
	    
	    return v;
    };

    //todo: jquery validation
    var do_payment = function (credit_card_id) {
	
		var url = site_url+'checkout/save_order';
			
		$('#credit-card-notification').removeClass('alert-danger alert').html("");

		$('#cc-submit').val('Processing...');
		$('#cc-submit').addClass('disabled');
		$('#cc-submit').prop('disabled', true);
		
		var values = $('#CheckoutForm').serialize();
		values += "&" + $('#BillingForm').serialize();		
		values += "&credit_card_id=" +credit_card_id;
		values += "&bill_email="+$('#cc-email').val();		
		
		$.ajax({
			type: "POST",
			url: url,
			data: values,
			dataType:'json'
			}).done(function( data ) 
			{				

				if(data.status=="success"){
					
					console.log('payment accepted');
					window.location.href = "<?php echo site_url('thank-you');?>";
				}else{
					//TODO: show friendly error message. and further instruction

					
					$('#cc-submit').val('Complete order');
					$('#cc-submit').removeClass('disabled');
					$('#cc-submit').prop('disabled', false);

					console.log('payment rejected');
					$('#credit-card-notification').addClass('alert-danger alert');

					$('#credit-card-notification').html(data.message);
				}
				
							
			});
	}

})();


$(document).ready(function(){

	var maskoptions =  {
	  onComplete: function(cep) {
	    //alert('CEP Completed!:' + cep);
	  },
	  onKeyPress: function(cep, event, currentField, options){
	    console.log('An key was pressed!:', cep, ' event: ', event, 'currentField: ', currentField, ' options: ', options);
	  },
	  onChange: function(cep){
	    //console.log('cep changed! ', cep);
	  },
	  onInvalid: function(val, e, f, invalid, options){
	    var error = invalid[0];
	    console.log ("Digit: ", error.v, " is invalid for the position: ", error.p, ". We expect something like: ", error.e);

	  }
	};

	$('#cc-number').mask('0#', maskoptions);
	$('#cc-cvv').mask('0000', maskoptions);
	$('#cc-expiration').mask('00/00', maskoptions);
	$('#cc-zip').mask('00000');

	$("#agreement_chkbox_span").click(function(){

		var validator = $( "#BillingForm" ).validate();
		validator.form();

		var state = $('#ship_state').val();

		if($('.opt_billing_sameas_shipping:checked').val()=="no")
		{
			state = $('#customer_bill_state').val();
			$('#cc-zip').val($('#customer_bill_zipcode').val());
		} 

		check_zip_validity(state, $( "#cc-zip" ).val());

		if($('#validation_passed_status').val() == "true"){
			$(".checker-box3").toggleClass("selected");
			
			if ($(this).hasClass('selected')) {
				$('.paymentinput').prop('checked','checked');
			} else {
				$('.paymentinput').removeAttr('checked');
			}

			if($("#agreement_chkbox:checked" ).length) {
		       
				$('#cc-submit').removeClass('disabled');
				$('#cc-submit').prop('disabled', false);	
		    }else{
		    	$('#cc-submit').addClass('disabled');
				$('#cc-submit').prop('disabled', true);	
		    }
		}else{
			alert("There are missing or incorrect information required to complete your order.")
		}
	});

	$( "#cc-zip" ).blur(function() 
	{
		console.log('blur '+ $('.opt_billing_sameas_shipping:checked').val());

		var state = $('#ship_state').val();

		if($('.opt_billing_sameas_shipping:checked').val()=="no") state = $('#customer_bill_state').val();

		check_zip_validity(state, $( this ).val());
	});

	

	$('#customer_bill_zipcode').keyup(function(){
		$( "#cc-zip" ).val($(this).val());
	});
	$('#customer_bill_zipcode').blur(function(){
		$( "#cc-zip" ).val($(this).val());
	});
	
	$('#customer_bill_state').blur(function(){
		$( "#cc-zip" ).val($('#customer_bill_zipcode').val());
	});

	$('#customer_bill_state').change(function(){
		$( "#cc-zip" ).val($('#customer_bill_zipcode').val());
	});


});



</script>