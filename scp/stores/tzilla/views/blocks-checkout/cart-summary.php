<div class="container-fluid order-summary-holder">
				<button class="btn btn-cart-drop" data-toggle="collapse" data-target="#summary">
					<div class="container">
						<span class="cart-icon"></span>
						<span class="cart-icon-label">Show order summary <i class="fa fa-angle-down arow-icon"></i></span>
						<span class="cart-icon-total pull-right">
						$ <?php 		
							$this->load->module('settings');
							$setting = $this->settings->get('tax');

							$tax_percent = 0;
							$customer_state = $this->session->customer_bill_state;
							foreach ($setting as $key => $value) {
								if($key===$customer_state) $tax_percent = $setting[$key];
							}
							//echo $tax_percent;

							$tax_amount = $cart_total * ($tax_percent / 100);
							$tax_amount = number_format($tax_amount,2);
							
							$grand_total_amount = $cart_total + $shipping_fee_amount + $tax_amount;
							
							 echo number_format($grand_total_amount,2); ?>
						</span>
					</div>
				</button>
</div>