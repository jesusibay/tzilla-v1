
<div class="row-padd-top3">
	<span class="span-19 ship-label">Billing Address</span>
	<ul class="list-group">
	  <li class="list-group-item shipping-list">
			<input type="radio" name="opt_billing_sameas_shipping" value="yes" class="blue-circle ship opt_billing_sameas_shipping" checked="checked"><span class="span-21">Same as shipping address</span>
	  </li>
	  <li class="list-group-item shipping-list last-elem bottom-radius">
			<input type="radio" name="opt_billing_sameas_shipping" value="no" class="blue-circle bill opt_billing_sameas_shipping"><span class="span-21">Use a different billing address</span>
	  </li>
	  <div id="bill-holder" class="collapse">
		<div class="cardpayment-holder bill-holder-inner bottom-radius">
			<div class="form-group">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 f-name">
						<input type="text" class="form-control custom-input span-12" value="<?php echo $this->session->customer_ship_firstname;?>" name="customer_bill_firstname" id="customer_bill_firstname" placeholder="First name">
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<input type="text" class="form-control custom-input span-12" value="<?php echo $this->session->customer_ship_lastname;?>" name="customer_bill_lastname" id="customer_bill_lastname" placeholder="Last name">
					</div>
				</div>
			</div>
			<div class="form-group">
				<input type="text" class="form-control custom-input span-12" value="<?php echo $this->session->customer_ship_address;?>" name="customer_bill_address" id="customer_bill_address" placeholder="Address">
			</div>
			<div class="form-group">
				<input type="text" class="form-control custom-input span-12" value="<?php echo $this->session->customer_ship_city;?>" name="customer_bill_city" id="customer_bill_city" placeholder="City">
			</div>
			<div class="form-group row">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd-r-zero">
					<select name="customer_bill_country" id="customer_bill_country" class="form-control custom-select span-12">
						<option value="US">United States</option>
						
					</select>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd-r-zero">
					<select name="customer_bill_state" id="customer_bill_state" class="form-control custom-select span-12">
					<?php 
					$this->load->module('us_states');
					
					$states =  json_decode($this->us_states->get_states());

					//print_r($states);
					
					foreach ($states as $key => $value) {
						$checked = '';
						if($value->state == $this->session->customer_ship_state) $checked = ' selected="selected" ';
						?><option value="<?php echo $value->state;?>" <?php echo $checked;?>><?php echo $value->state_name;?></option><?php
						
					}
					?>
					
					</select>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<input type="text" class="form-control custom-input span-12" value="<?php echo $this->session->customer_ship_zipcode;?>" name="customer_bill_zipcode" id="customer_bill_zipcode" placeholder="Zip Code">
				</div>
			</div>
			<div class="form-group" style="margin-bottom: 0;">
				<input type="text" class="form-control custom-input span-12" value="<?php echo $this->session->customer_ship_phone;?>" name="customer_bill_phone" id="customer_bill_phone" placeholder="Phone">
			</div>
		</div>
	  </div>
	</ul>
</div>

<input type="hidden" name="ship_firstname" id="ship_firstname" value="<?php echo $this->session->customer_ship_firstname;?>" >
<input type="hidden" name="ship_lastname" id="ship_lastname" value="<?php echo $this->session->customer_ship_lastname;?>" >
<input type="hidden" name="ship_address" id="ship_address" value="<?php echo $this->session->customer_ship_address;?>" >
<input type="hidden" name="ship_city" id="ship_city" value="<?php echo $this->session->customer_ship_city;?>" >
<input type="hidden" name="ship_state" id="ship_state" value="<?php echo $this->session->customer_ship_state;?>" >
<input type="hidden" name="ship_country" id="ship_country" value="<?php echo $this->session->customer_ship_country;?>" >
<input type="hidden" name="ship_zipcode" id="ship_zipcode" value="<?php echo $this->session->customer_ship_zipcode;?>" >
<input type="hidden" name="ship_phone" id="ship_phone" value="<?php echo $this->session->customer_ship_phone;?>" >
<input type="hidden" name="ship_customer_email" id="ship_customer_email" value="<?php echo $this->session->customer_email;?>" >

<script>
	$(document).ready(function(){
		if (window.matchMedia('(max-width: 992px)').matches)
		{
			$("#summary").addClass("collapse");
		}
		
		$(".cc").click(function(){
			
			$("#cardinfo-holder").slideDown();
			
		});
		
		$(".pay").click(function(){
			
			$("#cardinfo-holder").slideUp();
			
		});
		
		$(".ship").click(function(){
			
			$("#bill-holder").slideUp();
			$(".last-elem").addClass("bottom-radius");
		});
		
		$(".bill").click(function(){
			$(".last-elem").removeClass("bottom-radius");
			$("#bill-holder").slideDown();
			
		});
		
		$(".btn-cart-drop").click(function(){
			$("#summary").removeClass("collapse");
			$(this).find("i.arow-icon").toggleClass("fa-angle-down").toggleClass("fa-angle-up");
			$("#summary").addClass("collapse");


			$('#summary').on('shown.bs.collapse', function () {
		   	$("i.arow-icon").removeClass("fa-angle-down").addClass("fa-angle-up");
			});

			$('#summary').on('hidden.bs.collapse', function () {
		   	$("i.arow-icon").removeClass("fa-angle-up").addClass("fa-angle-down");
			});
		});

		$("#username").removeClass('email-fail error');
        $("#username").parent().find('.log-error-label').last().remove();

	});

	
	$().ready(function() {
		// validate form fields
		var billing_maskoptions =  {
		  onComplete: function(cep) {
		    //alert('CEP Completed!:' + cep);
		  },
		  onKeyPress: function(cep, event, currentField, options){
		    console.log('An key was pressed!:', cep, ' event: ', event, 'currentField: ', currentField, ' options: ', options);
		  },
		  onChange: function(cep){
		    //console.log('cep changed! ', cep);
		  },
		  onInvalid: function(val, e, f, invalid, options){
		    var error = invalid[0];
		    console.log ("Digit: ", error.v, " is invalid for the position: ", error.p, ". We expect something like: ", error.e);

		  }
		};
		$("#BillingForm").validate({			
			errorClass: "bg-danger",
			rules: {
				customer_bill_firstname: "required",
				customer_bill_lastname: "required",
				customer_bill_address: "required",
				customer_bill_city: "required",
				customer_bill_country: "required",
				customer_bill_state: "required",
				customer_bill_zipcode:{
			      	required: true,
			      	digits: true,
			      	remote: {
				        url: "<?php echo site_url('us_states/validate_state_zip'); ?>",
				        type: "post",
				        data: {
				          	state: function() {	return $('#customer_bill_state').val(); },
				          	zip_code: function(){ return $('#customer_bill_zipcode').val(); }
				        }
				    }
			    },
				customer_bill_phone: "required",				
				customer_bill_email: {
					required: true,
					email: true
				}				
			}
		});
	});

	$(document).ready(function(){
	  	$('#customer_bill_phone').mask('(000) 000-0000');
	  	$('#customer_bill_zipcode').mask('00000');

	  	$('.opt_billing_sameas_shipping').on('click',function()
	  	{	  
	  		$(".checker-box3").removeClass("selected");
						
			$('.paymentinput').removeAttr('checked');
			
			if($("#agreement_chkbox:checked" ).length) {
		       
				$('#cc-submit').removeClass('disabled');
				$('#cc-submit').prop('disabled', false);	
		    }

		    if($('.opt_billing_sameas_shipping:checked').val()=="yes")
			{				
				$('#cc-zip').val($('#ship_zipcode').val());
				$('#validation_passed_status').val("true");
			}else{
				$('#cc-zip').val($('#customer_bill_zipcode').val());
			} 

			$('#cc-submit').addClass('disabled');
			$('#cc-submit').prop('disabled', true);	
	  	});
	  	
	});

</script>