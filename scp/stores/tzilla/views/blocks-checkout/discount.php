
<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 second-col col-md-push-7 sidebar">
	<div id="summary" class="custom-profile-holder">
		<div class="row">
			<form method="post" name="discount_form" id="discount_form">
				<div class="col-lg-9 col-md-8 col-sm-9 col-xs-8 padd-r-zero-btn">
					<input type="text" class="form-control custom-input span-12" id="discount_code" name="discount_code" placeholder="Discount">
				</div>
				<div class="col-lg-3 col-md-4 col-sm-3 col-xs-4">
					<button class="btn custom-apply-btn pull-right apply-discount-btn">Apply</button>
					<button class="btn custom-apply-btn pull-right apply-discount-btn discount-btn"><i class="arrow_right discount-icon"></i></button>
				</div>
			</form>
		</div>
		<hr class="hr1">
		<div class="row">
			<div class="col-lg-12 num1">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-left line-height">Subtotal</div>
				<div id="display_checkout_subtotal" class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-right line-height"><?php echo $currency_symbol;?> <?php echo number_format($cart_total,2);?></div>
				<input type="hidden" name="hidden_checkout_subtotal" id="hidden_checkout_subtotal" value="<?php echo $cart_total;?>">

				<div class="discount_applied col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-left line-height">Discount</div>
				<div id="display_checkout_discount_amt" class="discount_applied col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-right line-height">(<?php echo $currency_symbol;?> <?php echo number_format($discount_amount,2);?>)</div>
				<input type="hidden" name="hidden_checkout_discount_amt" id="hidden_checkout_discount_amt" value="<?php echo $discount_amount;?>">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-left line-height">Shipping</div>
				<div id="display_checkout_shipping_fee" class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-right line-height"><?php echo $currency_symbol;?> <?php echo number_format($shipping_fee_amount,2);?></div>
				<input type="hidden" name="hidden_checkout_shipping_fee" id="hidden_checkout_shipping_fee" value="<?php echo $shipping_fee_amount;?>">

				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-left line-height">Taxes</div>
				<?php 		
					$this->load->module('settings');
					$setting = $this->settings->get('tax');

					$tax_percent = 0;
					$customer_state = $this->session->customer_bill_state;
					foreach ($setting as $key => $value) {
						if($key===$customer_state) $tax_percent = $setting[$key];
					}
					//echo $tax_percent;

					$tax_amount = ($cart_total-$discount_amount) * ($tax_percent / 100);
					$tax_amount = number_format($tax_amount,2);
					
					$grand_total_amount = $cart_total + $shipping_fee_amount + $tax_amount;
				?>
				<div id="display_checkout_tax" class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-13 text-right line-height"><?php echo $currency_symbol;?> <?php echo number_format($tax_amount, 2);?></div>
				<input type="hidden" name="hidden_checkout_tax" id="hidden_checkout_tax" value="<?php echo $tax_amount;?>">

			</div>
		</div>
		<hr class="hr1">
		<div class="row">
			<div class="col-lg-12 num2">
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-14 text-left line-height">Total</div>
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padd-all-zero span-14 text-right line-height">
				<span class="text-uppercase total-usd"><?php echo $currency;?></span> <span id="display_grand_total" class="total-val"><?php echo $currency_symbol;?> <?php echo number_format($grand_total_amount, 2);?></span></div>
				<input type="hidden" name="hidden_grand_total" id="hidden_grand_total" value="<?php echo number_format($grand_total_amount, 2);?>">
			</div>
		</div>
	



	<div class="row">
		<div class="col-lg-12 num2">
			<form id="CheckoutForm">

				<input type="hidden" id="customer_email" name="customer_email" value="<?php echo $customer_email;?>" readonly="readonly"> 

				<input type="hidden" id="shipping_method" name="shipping_method" value="<?php echo $shipping_method;?>" readonly="readonly">
				
				<input type="hidden" id="shipping_fee_amount" name="shipping_fee_amount" value="<?php echo $shipping_fee_amount;?>" readonly="readonly">
										
				<input type="hidden" id="discount_amount" name="discount_amount" value="<?php echo $discount_amount;?>" readonly="readonly"> 
				
				<input type="hidden" id="tax_amount" name="tax_amount" value="<?php echo $tax_amount;?>" readonly="readonly">
				
				<input type="hidden" id="grand_total_amount" name="grand_total_amount" value="<?php echo $grand_total_amount;?>" readonly="readonly"> 

				<input type="hidden" id="currency" name="currency" value="<?php echo $currency;?>" readonly="readonly">

				<input type="hidden" id="currency_symbol" name="currency_symbol" value="<?php echo $currency_symbol;?>" readonly="readonly">
			</form>
		</div>
	</div>


	</div>
</div>