<?php 
	#if shipping address fields are blank, do not show shipping methods, set shipping address first
	if($this->session->customer_ship_zipcode === '' || $this->session->customer_ship_address ==='' || $this->session->customer_ship_firstname===''){
		echo "Please set your shipping address.";
		header( "refresh:1; url=". site_url('checkout/customer') ); //redirect();
	}else{
	
		?>
		<span class="span-19">Shipping address</span>
		<div class="row">
			<div class="col-lg-4">
				<p class="span-20"><?php echo $this->session->customer_ship_firstname.' '.$this->session->customer_ship_lastname ;?></p>
				<p class="span-20"><?php echo $this->session->customer_ship_address .' ' . $this->session->customer_ship_city.', '.
				$this->session->customer_ship_state.', '.$this->session->customer_ship_zipcode.', '.$this->session->customer_ship_country;?></p>
				<p class="span-20"><?php echo $this->session->customer_ship_phone;?></p>
				<div class="row-padd-top2"><a class="span-12 edit-ship" href="<?php echo site_url('checkout/customer');?>">Edit Shipping address</a></div>
			</div>
		</div>
		
		<form class="" id="shipping_method_form" name="shipping_method_form" method="post" action="<?php echo site_url('checkout/shipping');?>">

		<div class="row-padd-top3">
			<span class="span-19 ship-label">Shipping Method</span>
			
			<ul class="list-group">
				<?php
				
				
				$this->load->module('discount');
				

				if($this->discount->check_free_shipping()){
					?>
						<li class="list-group-item shipping-list">

							<input type="radio" name="shipping_method_option" value="Free Shipping:$:0.00" checked="checked" class="blue-circle shipping_method_option"><span class="span-21">Free Shipping</span>
							<span class="pull-right span-21 ship-price">$0.00</span>
						</li>	
						
					<?php
				}else{

					$this->load->module('shipping_methods');

					$usps = json_decode($shipping_methods = $this->shipping_methods->usps());
					
					foreach ($usps as $key => $value) {								
						$checked = '';
						if($this->session->shipping_method===''){
							if( $value->default==1 ) $checked = ' checked="checked" ';
						}else{
							if( $value->method == $this->session->shipping_method ){
								$checked = ' checked="checked" ';	
							}else{
								if( $value->default==1) $checked = ' checked="checked" ';
							} 						
						}					
						?>
						 <li class="list-group-item shipping-list">

							<input type="radio" name="shipping_method_option" class="blue-circle shipping_method_option" value="<?php echo $value->method.':'.$value->currency_symbol.':'.$value->fee; ?>" <?php echo $checked;?>  >
							<span class="span-21"><?php echo $value->method;?></span>
							<span class="pull-right span-21 ship-price"><?php echo $value->currency_symbol.$value->fee;?></span>
						  </li>	
						<?php					
					}
				}
										
				
				?>

					
			</ul>
			
		</div>

		<div class="form-group row row-padd-top2 ">
			<div class="col-lg-7 col-padd return-link-desktop">
				<a href="<?php echo site_url('checkout/customer');?>"><span class="checkbox-label">< Return to customer information</span></a>
			</div>
			<div class="col-lg-5">
				<input type="submit" name="submit" id="continue-to-payment" value="Continue to payment" class="btn btn-primary btn-lg btn-lg4 pull-right">
			</div>
		</div>

		</form>

	<?php 
	} //end of if shipping address are blank, do not show shipping methods, set shipping address first
?>
		
		<div class="form-group return-link">
		<div class="row">
			<div class="col-lg-12">
				<a href="<?php echo site_url('checkout/customer');?>"><span class="checkbox-label">< Return to customer information</span></a>
			</div>
		</div>
		</div>


<script type="text/javascript">
	$(document).ready(function(){
		$("#shipping_method_form").validate({
			errorClass: "bg-danger",
			rules: {
				shipping_method_option: "required"						
			}
		});
	});

</script>		


		
		