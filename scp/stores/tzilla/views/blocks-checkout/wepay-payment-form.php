<?php 

if(SITEMODE=='DEV'){

	$cc_name = $this->session->customer_ship_firstname. ' '.$this->session->customer_ship_lastname;
	$cc_card_number = '5496198584584769';
	$cc_email = 'jovani.ogaya@gmail.com';
	$cc_expiry_month = 12;
	$cc_expiry_year = 20;
	$cc_expiry_cvv = 123;
	$cc_expiry_zipcode = $this->session->customer_ship_zipcode;

}else{
	
	$cc_name = $this->session->customer_ship_firstname. ' '.$this->session->customer_ship_lastname;
	$cc_card_number = '';
	$cc_email = '';
	$cc_expiry_month = '';
	$cc_expiry_year = '';
	$cc_expiry_cvv = '';
	$cc_expiry_zipcode = $this->session->customer_ship_zipcode;
}

?>

<div id="cardinfo-holder" class="collapse in" style="width: 100%;">
	<div class="cardpayment-holder bottom-radius">
		<div class="form-group">
			<input type="text" class="form-control custom-input span-12 cc-text required" id="cc-number" name="Card Number" placeholder="Card number" value="<?php echo $cc_card_number;?>">
		</div>
		<div class="form-group row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd-r-zero">
				<input type="text" class="form-control custom-input span-12 required" id="cc-name" name="Name" placeholder="Name on card" value="<?php echo $cc_name;?>">
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd-r-zero">
				<input type="text" class="form-control custom-input span-12 required" id="cc-expiration" name="Expiration Month / Year" placeholder="MM/YY" value="<?php echo $cc_expiry_month.'/'.$cc_expiry_year;?>" data-mask="00/00">
				<span data-toggle="tooltip" data-placement="top" title="2 digit credit card expiration Month/Year" class="questionz"></span>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 cvv-holder">
				<input type="text" class="form-control custom-input span-12 required" id="cc-cvv" name="CVV" placeholder="CVV" value="<?php echo $cc_expiry_cvv;?>">
				<span data-toggle="tooltip" data-placement="top" title="3-digit security code on the back of your card" class="questionz"></span>
			</div>
			
		</div>
		<div class="form-group row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd-r-zero">
				<input type="text" class="form-control custom-input span-12 required" id="cc-email" name="Email" placeholder="Email" value="<?php echo $customer_email;?>">
				
			</div>

			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 cvv-holder">
				<input type="text" class="form-control custom-input span-12 required" id="cc-zip" name="Zipcode" placeholder="Zip" value="<?php echo $cc_expiry_zipcode;?>">
				
			</div>
		</div>	
	</div>
</div>