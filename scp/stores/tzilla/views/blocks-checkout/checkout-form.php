<?php 

if(SITEMODE=='DEV'){
	$cart_total = '100.00';
	$shipping_method = 'FEDEX GROUND';
	$shipping_fee_amount = '7.00';
	$discount_amount = '0.00';
	$tax_amount = '2.00';
	$grand_total_amount = ($cart_total + $shipping_fee_amount - $discount_amount) + $tax_amount;
	$currency = 'USD';

}else{
	$cart_total = '';
	$shipping_method = '';
	$shipping_fee_amount = '';
	$discount_amount = '';
	$tax_amount = '';
	$grand_total_amount = '';
	$currency = 'USD';
}

?>

<div>	
	<form id="CheckoutForm">

		<input type="text" id="customer_email" name="customer_email" value="<?php echo $customer_email;?>" readonly="readonly"> <br/>

		<input type="text" id="shipping_method" name="shipping_method" value="<?php echo $shipping_method;?>" readonly="readonly"> <br/>
		
		Shipping Fee: <input type="text" id="shipping_fee_amount" name="shipping_fee_amount" value="<?php echo $shipping_fee_amount;?>" readonly="readonly"> <br/>
								
		Discount Amount: <input type="text" id="discount_amount" name="discount_amount" value="<?php echo $discount_amount;?>" readonly="readonly">  <br/>
		
		Tax Amount: <input type="text" id="tax_amount" name="tax_amount" value="<?php echo $tax_amount;?>" readonly="readonly"> <br/>
		
		Grand Total: <input type="text" id="grand_total_amount" name="grand_total_amount" value="<?php echo $grand_total_amount;?>" readonly="readonly">  <br/>

		Currency: <input type="text" id="currency" name="currency" value="<?php echo $currency;?>" readonly="readonly">
	</form>
</div>

<br/>
