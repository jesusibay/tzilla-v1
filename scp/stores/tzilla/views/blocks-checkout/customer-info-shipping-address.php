<?php 
	$this->session->set_userdata('goback_here', $this->uri->uri_string());
?>
<?php 


if($this->session->is_logged_in==TRUE){
	?>
	<div class="custom-profile-img">
		<img class="img-responsive logged_in_profile_pic" data-original="<?php echo base_url('public/'.STORE.'/images/user.jpg');?>" alt="" style="margin:0 auto;">
	</div>


	<div class="custom-profile-info">
		<span class="span-12"><?php echo $this->session->full_name;?> (<?php echo $this->session->logged_in_email;?>)</span>
		<a class="span-12 logout" href="<?php echo site_url('user/fb_login/logout_user');?>">Log out</a>
	</div>
	<?php
}
?>

<form class="" id="customer_info_form" method="post" action="<?php  echo site_url('checkout/customer');?>">
	<div class="form-group row-padd-top1">
		<label class="span-12 custom-label " for="customer_email">Customer information</label>
		<input type="email" value="<?php echo $this->session->customer_email;?>" class="form-control custom-input span-12" name="customer_email" id="customer_email" placeholder="Email"><br/>
		<?php echo form_error('customer_email'); ?>

		<?php 
		if($this->session->is_logged_in==FALSE){
		?>
		<label class="span-12 custom-label" style="padding-top: 3px;" for="">Already have an account with us? <a class="span-12 login" data-redirec_here="<?php echo site_url('checkout/customer');?>" href="<?php echo site_url('sign-in');?>">Log in</a></label>
			<?php
		}
		?>
	</div>
	<div class="form-group row-padd-top1">
		<label class="span-12 custom-label" for="">Shipping address</label>
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 f-name">
				<label class="" for="customer_ship_firstname"></label>
				<input type="text" value="<?php echo $this->session->customer_ship_firstname;?>" class="form-control custom-input span-12" name="customer_ship_firstname" id="customer_ship_firstname" placeholder="First name">
				<?php echo form_error('customer_ship_firstname'); ?>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<label class="" for="customer_ship_lastname"></label>
				<input type="text" value="<?php echo $this->session->customer_ship_lastname;?>" class="form-control custom-input span-12" name="customer_ship_lastname" id="customer_ship_lastname" placeholder="Last name">
				<?php echo form_error('customer_ship_lastname'); ?>
			</div>

		</div>
	</div>
	<div class="form-group">
		<label class="" for="customer_ship_address"></label>
		<input type="text" value="<?php echo $this->session->customer_ship_address;?>" class="form-control custom-input span-12" name="customer_ship_address" id="customer_ship_address" placeholder="Address">
		<?php echo form_error('customer_ship_address'); ?>
	</div>
	<div class="form-group">
		<label class="" for="customer_ship_city"></label>
		<input type="text" value="<?php echo $this->session->customer_ship_city;?>" name="customer_ship_city" id="customer_ship_city" class="form-control custom-input span-12" placeholder="City">
		<?php echo form_error('customer_ship_city'); ?>
	</div>
	<div class="form-group row">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd-r-zero">
			<label class="" for="customer_ship_country"></label>
			<select name="customer_ship_country" id="customer_ship_country" class="form-control custom-select span-12">
				<option value="US">United States</option>									
			</select>
			<?php echo form_error('customer_ship_country'); ?>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd-r-zero">
			<label class="" for="customer_ship_state"></label>
			<select name="customer_ship_state" id="customer_ship_state" class="form-control custom-select span-12">
				<?php 
					$this->load->module('us_states');
					
					$states =  json_decode($this->us_states->get_states());

					//print_r($states);
					
					foreach ($states as $key => $value) {
						$checked = '';
						if($value->state== $this->session->customer_ship_state ) $checked = ' selected="selected" ';
						?><option <?php echo $checked;?> value="<?php echo $value->state;?>" > <?php echo $value->state_name;?></option><?php
						
					}
				?>
			</select>
			<?php echo form_error('customer_state'); ?>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
			<label class="" for="customer_ship_zipcode"></label>
			<input type="text" value="<?php echo $this->session->customer_ship_zipcode; ?>" class="form-control custom-input span-12" name="customer_ship_zipcode" id="customer_ship_zipcode" placeholder="Zip Code">
			<?php echo form_error('customer_ship_zipcode'); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="" for="customer_ship_phone"></label>
		<input type="text" value="<?php echo $this->session->customer_ship_phone; ?>" class="form-control custom-input span-12" name="customer_ship_phone" id="customer_ship_phone" placeholder="Phone">
		<?php echo form_error('customer_ship_phone'); ?>
	</div>


	<div class="form-group row row-padd-top2">
		<div class="col-lg-7 col-padd">

			<span class="checker-box2 selected"></span><span class="checkbox-label">Save this information for next time.</span>
			<div style="display:none">
				<input type="checkbox" checked="checked" value="on" name="save_customer_info" id="save_customer_info" class="customerinput">
			</div>

		</div>
		<div class="col-lg-5">
			
			<input class="btn btn-primary btn-lg btn-lg4 pull-right" id="continue-to-shipping" type="submit" value="Continue to shipping">
			
		</div>
	</div>

</form>

<hr class="customhr">

<div class="form-group return-link1">
<div class="row">
	<div class="col-lg-12">
		<a href="<?php echo site_url('cart'); ?>"><span class="checkbox-label">< Return to cart</span></a>
	</div>
</div>
</div>

<script type="text/javascript">
	
	$().ready(function() {
		// validate form fields
		
		$("#customer_info_form").validate({
			errorClass: "bg-danger",
			rules: {
				customer_ship_firstname: "required",
				customer_ship_lastname: "required",
				customer_ship_address: "required",
				customer_ship_city: "required",
				customer_ship_country: "required",
				customer_ship_state: "required",
				customer_ship_zipcode:{
			      	required: true,
			      	digits: true,
			      	remote: {
				        url: "<?php echo site_url('us_states/validate_state_zip'); ?>",
				        type: "post",
				        data: {
				          	state: function() {	return $('#customer_ship_state').val(); },
				          	zip_code: function(){ return $('#customer_ship_zipcode').val(); }
				        }
				    }
			    },
				customer_ship_phone: "required",				
				customer_email: {
					required: true,
					email: true
				}				
			}
		});
	});

	$(document).ready(function(){
	  	$('#customer_ship_phone').mask('(000) 000-0000');
	  	$('#customer_ship_zipcode').mask('00000');

	  	
	});

</script>