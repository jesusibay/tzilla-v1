<?php $this->load->view_store('header');  ?>
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/checkout.css');?>">
<input type="hidden" id="customer_id" value="<?php echo $this->session->customer_id; ?>">
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/payment-method.js').'?df='.rand(1, 99999);?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/jquery.mask.min.js').'?df='.rand(1, 99999);?>"></script>
<script type="text/javascript" src="https://static.wepay.com/min/js/tokenization.v2.js"></script>

<script type="text/javascript">
		setUsValidate('<?php echo site_url('us_states/validate_state_zip'); ?>');
		setSitemode('<?php echo SITEMODE; ?>');
</script>
<input type="hidden" id="address_type" value="">
<section style="overflow:hidden;">

<div class="container shipping-cont">
	<div class="row">
	
		<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 discount-holder-main col-lg-push-7 col-md-push-7">
			<div class="discount-holder-outer payment-outer">
				<!-- <div class="discount-holder-cont">
					<div class="discount-holder-inner">
						<input type="text" class="form-control checkout-input gregular discount-text" id="" PlaceHolder="Discount">
					</div>	
					<button class="gray-btn white glight font-small checkout-apply-btn">Apply</button>
				</div> -->
				<!-- <div class="discount-holder-summary">
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Total Items</span>
						<span class="pull-right gregular font-regular gray-darker">12</span>
						<div class="clearfix"></div>
					</div>
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Total Price</span>
						<span class="pull-right gregular font-regular gray-darker">$120.00</span>
						<div class="clearfix"></div>
					</div>
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Discount</span>
						<span class="pull-right gregular font-regular gray-darker">10%</span>
						<div class="clearfix"></div>
					</div>
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Less</span>
						<span class="pull-right gregular font-regular gray-darker">$12.00</span>
						<div class="clearfix"></div>
					</div> 
				</div> -->
				<div class="discount-holder-summary">
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Subtotal</span>
						<span class="pull-right gregular font-regular gray-darker subtotal computeall" >$ <?php echo $this->session->subtotal; ?></span>
						<div class="clearfix"></div>
					</div>
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Shipping</span>
						<span class="pull-right gregular font-regular gray-darker shipping-fee computeall">$ <?php echo $this->session->shipping_fee; ?></span>
						<div class="clearfix"></div>
					</div>
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Taxes</span>
						<span class="pull-right gregular font-regular gray-darker tax-fee computeall">$ <?php echo $this->session->tax; ?></span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="discount-holder-perline">
						<div class="pull-left gsemibold font-regular gray-darker">Total</div>
						<div class="pull-right">

							<span class="gsemibold font-small gray">USD</span>
							<span class="gsemibold font-large gray-darker total"> <?php echo $this->session->total; ?></span>
						</div>
						<div class="clearfix"></div>
				</div>
				
			</div>
		</div>
	
	
	
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 shipping-outer col-lg-pull-5 col-md-pull-5">
			<div class="shipping-inner">	
				<div class="payment-title gsemibold gray-darker font-large" style="margin-bottom: 2px;">Payment Method</div>
				<p class="payment-p glight gray-dark font-regular">All transactions are secure and encrypted. Credit card information is never stored.</p>
				<div class="shipping-radio-cont payment-radio-cont">
					<input type="radio" name="optradio1" class="green-circle " checked>
					<span class="radio-title gregular font-regular gray-dark">Credit Card</span>
					<div class="cc-holder">
						<img class="img-responsive cc-cards" src="<?php echo base_url('public/'.STORE.'/images/credit-card-providers.jpg');?>" alt=""/>
						<!-- <span class="cc-more gregular font-regular gray-dark">and more...</span> -->
					</div>
				</div>
				<!-- <form id="paymentmethod" method="post" action="" role="form"> -->
				<div class="payment-cc-cont">
					<div class="row-line">
						<div class="form-group handler">

							  <input type="text" name="cardnumber" onkeypress="return isNumberKey(event);" data-name="Credit card number" class="cc-input form-control checkout-input gregular cc-number" id="cc-number" PlaceHolder="Card Number">
						</div>
					</div>
					<div class="row-line">
						<div class="shipping-col-left">
							<div class="form-group handler">

							  <input type="text" name="cardholder" class="form-control checkout-input gregular cardholder cc-input name" data-name="Name" id="cc-name" PlaceHolder="Name on Card">

							</div>
						</div>
						<div class="shipping-col-right">
							<div class="form-group handler">
							  <input type="text" name="email" class="form-control checkout-input gregular cardemail cc-input" id="cc-email" data-name="Email" PlaceHolder="Email" value="<?php echo $this->session->logged_in_email; ?>">
							  <!-- <span id="error" style="display:none;color:red;">Wrong email</span> -->
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="row-line">
						<div class="cc-col-left" style="margin-right: 2%;">
							<div class="form-group handler">
							  <input type="text" name="cardexpiry" class="form-control checkout-input gregular cardexp cc-input" data-name="Credit card expiration" id="cc-expiration" PlaceHolder="MM / YY">
							</div>
						</div>
						<div class="cc-col-right">
							<div class="form-group handler">
							  <input type="text" name="cvv" onkeypress="return isNumberKey(event);" class="cc-input form-control checkout-input gregular cvv-number" data-name="Credit card cvv" id="cc-cvv" PlaceHolder="CVV">
							  <input type="hidden" class="form-control custom-input span-12 required " id="cc-zip" name="Zipcode" placeholder="Zip" value="<?php echo $this->session->customer_ship_zipcode;?>">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
						<!--<div id="credit-card-notification"></div>-->
				</div>
	<form class="" id="BillingForm" name="" method="post" action="javascript:void(0)">			
				<div class="payment-title gsemibold font-large gray-darker bill-add-label">Billing Address</div>
				<div class="shipping-radio-cont payment-radio-cont">
					<input type="radio" name="optradio" class="green-circle radio-add-type shipadd" value="Both">
					<span class="radio-title gregular font-regular gray-dark">Same as Shipping Address</span>
				</div>
				<div class="shipping-radio-cont payment-radio-cont2" style="border-top:none;">
					<input type="radio" name="optradio" class="green-circle radio-add-type billadd" checked value="Billing">
					<span class="radio-title gregular font-regular gray-dark">Use a Different Billing Address</span>
				</div>
				<div class="payment-billing-cont">
				<div class="row-line">
					<div class="shipping-col-left">
						<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">First Name</label>
						  <input type="text" name="firstname" class="form-control checkout-input gitalic billing-input " data-name="Firstname" id="customer_bill_firstname" PlaceHolder="First Name">
						</div>
					</div>
					<div class="shipping-col-right">
						<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">Last Name</label>
						  <input type="text" name="lastname" class="form-control checkout-input gitalic billing-input name" data-name="Lastname" id="customer_bill_lastname" PlaceHolder="Last Name">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row-line">
					<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">Address</label>
						  <input type="text" name="address" class="form-control checkout-input gitalic billing-input" data-name="Address" id="customer_bill_address" PlaceHolder="Address">
					</div>
				</div>
				<div class="row-line">
					<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">City</label>
						  <input type="text" name="city" class="form-control checkout-input gitalic billing-input" data-name="City" id="customer_bill_city" PlaceHolder="City">
					</div>
				</div>
				<div class="row-line">
					<div class="shipping-col-one">
						<div class="form-group handler">
							  <label for="" class="glight font-regular gray-dark">Country</label>
							  <input type="text" name="country" class="form-control checkout-input gitalic billing-input" data-name="Country" id="customer_bill_country" value="United States"readonly PlaceHolder="United States">
						</div>
					</div>
					<div class="shipping-col-two">
						<div class="form-group handler">
							  <label for="" class="glight font-regular gray-dark">State</label>
							  <select name="state" class="form-control gitalic checkout-select billing-input" data-name="State" id="customer_bill_state">
							  	<option value="">Select a State</option>
                            <?php foreach ($states as $key => $val) {?>
                                <option data-value="<?php echo $val['state_name'];?>" value="<?php echo $val['state_code'];?>"><?php echo ucfirst($val['state_name']);?></option>
                            <?php } ?>
							  </select>
						</div>
					</div>
					<div class="shipping-col-three">
						<div class="form-group handler">
							  <label class="glight font-regular gray-dark">Zip Code</label>
							  <input type="text" name="zip" class="form-control checkout-input gitalic billing-input" maxLength="" data-name="Zip" id="customer_bill_zipcode" placeHolder="Zip Code" onkeypress="return isNumberKey(event)" >
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row-line">
					<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">Phone</label>
						  <input type="text" name="phone" class="form-control checkout-input gitalic billing-input" data-name="Phone" id="customer_bill_phone" onkeypress="return isNumberKey(event)" placeholder="Your Contact Number (xxx) xxx-xxxx">
					</div>
				</div>
				</div>
				<div id="notification" style="color:#FF0000;"></div>
				<input type="hidden" name="validation_passed_status" id="validation_passed_status" value="true">
				<input type="hidden" name="shipping_method" id="shipping_method" value="<?php echo $this->session->shipping_method;?>" >
				<input type="hidden" name="shipping_fee" id="shipping_fee" value="<?php echo $this->session->shipping_fee;?>" >
				</form>
				<div class="row" style="margin-top: 40px;">
					<div class="col-lg-12">
						<a  class="green gsemibold font-small backtoShipping pointer">❮ Return to shipping details</a>
					</div>
				</div>
				<div class="proceeder-cont2">
					<div class="proceeder-one">
						<span class="checker-box2" id="agreement_chkbox_span"></span>
						<div style="display:none">
							<input type="checkbox" name="agreement_chkbox" id="agreement_chkbox" value="On" class="paymentinput">
						</div>
					</div>
					<div class="proceeder-two">
						<span class="checkbox-label2 gsemibold font-small">I have reviewed my order for accuracy of spelling, color, sizes, and Super Customization choices. I understand that TZilla cannot accept returns on customized designs.</span>
						</br>
						<span class="checkbox-label3 gregular font-small">When you receive your garment(s), you may notice a unique scent. This is a normal part of the direct-to-garment (DTG) printing process and will go away after the first wash.</span>
					</div>
					<div class="proceeder-three">
						<button class="gray-btn white gsemibold font-small pull-right complete-order-btn" disabled="disabled" id="cc-submit">Complete Order <span class="order-btn-loader" style="display: none;">
							<img class="img-responsive cc-cards" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" style="width: 20px;height: 20px;" alt=""/>
						</span></button>
					</div>
					<!--<div class="col-lg-12">
						<span class="checkbox-label3 gregular font-small">*Once you receive your garments, you may find a</br>
						unique scent. This is due to the quality paint we</br>
						use from our printer. The scent will go away in just</br>
						one wash.</span>
					</div>-->
					<div class="clearfix"></div>
				</div>
				
				<div class="footer-holder">
						<a class="footer-links gsemibold font-small" href="#" type="button" data-toggle="modal" data-target="#privacypolicy" data-dismiss="modal">Privacy Policy</a>
						<a class="footer-links gsemibold font-small" href="#" type="button" data-toggle="modal" data-target="#refundpolicy">Refund Policy</a>
						<a class="footer-links gsemibold font-small" href="#" type="button" data-toggle="modal" data-target="#termsCondition" data-dismiss="modal">Terms of Service</a>
				</div>
			</div>
		</div>	
		<!-- hidden items -->
		
		<form id="ShippingForm" >
		<input type="hidden" name="ship_firstname" id="ship_firstname" value="<?php echo $this->session->customer_ship_firstname;?>" >
		<input type="hidden" name="ship_lastname" id="ship_lastname" value="<?php echo $this->session->customer_ship_lastname;?>" >
		<input type="hidden" name="ship_address" id="ship_address" value="<?php echo $this->session->customer_ship_address;?>" >
		<input type="hidden" name="ship_city" id="ship_city" value="<?php echo $this->session->customer_ship_city;?>" >
		<input type="hidden" name="ship_state" id="ship_state" value="<?php echo $this->session->customer_ship_state;?>" >
		<input type="hidden" name="ship_country" id="ship_country" value="<?php echo $this->session->customer_ship_country;?>" >
		<input type="hidden" name="ship_zipcode" id="ship_zipcode" value="<?php echo $this->session->customer_ship_zipcode;?>" >
		<input type="hidden" name="ship_phone" id="ship_phone" value="<?php echo $this->session->customer_ship_phone;?>" >
		<input type="hidden" name="ship_customer_email" id="ship_customer_email" value="<?php echo $this->session->logged_in_email; ?>" >
		</form>
	</div>
</div>
</section>
<?php $this->load->view_store('footer');  ?>



<script type="text/javascript">

		
	<?php		
		// if( SITEMODE == "DEV" ){ 
		// 	echo 'var w_endpnt = "stage";';
		// 	echo 'var client_id = 12231;'; //todo: load from database settings
	 // 	}else{ 
		// 	echo 'var w_endpnt = "production";';
		// 	echo 'var client_id = 121048;'; //todo: load from database settings
		// }		
	?>


</script>