<script type="text/javascript">   
            
    onloadCallback = function() {
        gregister = grecaptcha.render('register_element', {
          'sitekey' : '6LfkVxkUAAAAAJ9P3_Kzq8T6N4Me2F8DiE4LyCGd',
          'callback' : verifyCallback
        });

        gcontactus = grecaptcha.render('contactus_recap', {
            'sitekey' : '6LfkVxkUAAAAAJ9P3_Kzq8T6N4Me2F8DiE4LyCGd',
            'callback' : verifyCallback,
            'theme' : 'light'
        });
    };
</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>
<script src="<?php echo base_url('public/'.STORE.'/js/contact-us.js');?>"></script>

<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/contact.css');?>">

<div class="container-fluid contact-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <h1 class="white">Contact Us</h1>
                <p class="gray">Give us a shout below if you have any questions, comments, or feedback.</p>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid contact-pad-cont">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="contact-img-holder">
                    <img class="img-responsive img-center shirt-col" src="<?php echo base_url('public/'.STORE.'/images/contact-img.jpg');?>" alt="">
                </div>
                <div class="form-group">
                    <h3 class="blackz">TZilla Headquarters</h3>
                    <div class="gray font-small custom-label cont-details"><div class="contact-add"></div>13860 Stowe Drive, CA 92064</div>
                    <div class="gray font-small custom-label cont-details pointer"><div class="contact-email"></div><u>support@tzilla.com</u></div>
                    <div class="gray font-small custom-label cont-details">
                        <div class="contact-phone"></div><span class="gray-dark">(888) 901-1636 </span>(Mon-Fri: 9am to 6pm PST)
                    </div>       
                </div>

            </div>
            <div class="col-md-8">
                <h2 class="blackz getintouch-lbl-margin">Get In Touch with TZilla</h2>
                    <div class="row form-group">
                        <div class="col-xs-6">
                            <label class="gregular font-xsmall gray-darker custom-label" for=""><span class="red-ast">*</span> Name</label>
                            <input type="text" class="form-control gitalic font-xsmall gray-dark req-field" data-name="Name" id="contact-name" name="contactname" placeholder="Name">                           
                        </div>
                        <div class="col-xs-6">
                            <label class="gregular font-xsmall gray-darker custom-label" for=""><span class="red-ast">*</span> Email</label>
                            <input type="email" class="form-control gitalic font-xsmall gray-dark req-field" id="contact-email" name="contactemail" placeholder="Email" data-name="Email">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-6">
                            <label class="gregular font-xsmall gray-darker custom-label" for="">Phone Number</label>
                            <input type="text" class="form-control gitalic font-xsmall gray-dark " maxLength="" id="contact-phone" name="contactphone" placeholder="Your Contact Number (xxx) xxx-xxxx" data-name="Phone">
                        </div>
                        <div class="col-xs-6">
                            <label class="gregular font-xsmall gray-darker custom-label" for="">School Name</label>
                            <input type="text" class="form-control gitalic font-xsmall gray-dark " id="contact-schoolname" name="contactschoolname" placeholder="School Name" data-name="School name">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label class="gregular font-xsmall gray-darker custom-label" for=""><span class="red-ast">*</span> Subject</label>
                            <input type="text" class="form-control gitalic font-xsmall gray-dark req-field" id="contact-subject" name="contactsubject" placeholder="Subject" data-name="Subject"> 
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label class="gregular font-xsmall gray-darker custom-label" for=""><span class="red-ast">*</span> Message</label>
                            <textarea rows="8" class="form-control gitalic font-xsmall gray-dark req-field" id="contact-message" name="contactmessage" placeholder="Type Your Message Here" data-name="Message"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div id="contactus_recap" ></div>
                            <span class="validation-error-contactus" style="color:#FF0000;"></span>
                        </div>
                   
                    
                       <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">

                            <button class="green-btn cartrev-update-btn white gsemibold font-small margin-btn send-message" id="send-message">Send Your Message
                                <span class="search-loader">
                                    <img class="img-responsive img-loader" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt=""/>
                                </span>
                            </button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>