<?php $this->load->view_store('header');
$this->load->helper('textparser');
?>  
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/school-store.css');?>">

<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/all-campaigns.js').'?df='.rand(1, 99999);?>"></script>
<script type="text/javascript">
	$('img').bind('contextmenu', function(e){
	        return false;
	  }); 
	setArtworkCategories( '<?php echo $category; ?>' );
	setActive( '<?php echo $cat_selected; ?>' );
	setBaseURL( '<?php echo base_url("/public/".STORE);?>' );
</script>
<div class="container-fluid store-header camp-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-7 col-sm-12 col-xs-12">
				<h1 class="white">Current Campaigns</h1>
				<p class="white">Browse through some existing campaigns to see how students are using great design to raise funds for the causes that matter most to them.</p>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row store-design-header">
		<!-- search box -->
		<form action="<?php echo base_url("campaign/search"); ?>" id="campaign-search" method="post" >

			<div class="searchbox" style="margin-left: 375px;"> 
				<div class="col-lg-2" >
						<select id="selectFilter" class="form-control"> 
							<option id="moreFilter" value="">All</option>
							<option <?php if( $filter == "mostpopular" ) { echo "selected"; } ?> id="mostpopular" value="mostpopular">Most Popular</option>
							<option <?php if( $filter == "latest"  ) { echo "selected"; } ?> id="latest" value="latest">Latest</option>
							<option <?php if( $filter == "endingsoon" ) { echo "selected"; } ?> id="endingsoon" value="endingsoon">Ending Soon</option>
						</select>
						
				</div>
				<div class="col-lg-3" >
					<input type="text" id="search-term" name="term" class="form-control search-term" value="<?php if($search){ echo $search; } ?>" placeholder="Campaign Title/School Name " autocomplete="off"> 
						<!-- <input id="search-campaign" type="text" class="form-control" value="" placeholder="Campaign Title/School Name "> -->
					<?php $message = "";
						echo "<span> ".$message." </span>"; 
					?>
				</div> 

				<div class="col-lg-2">
					<button id="term_search" class="btn btn-primary btn-warning" type="submit" name="submit" style="height: 42px;">Search</button>
				</div>	 
				
			</div>
		</form>

		

		<div class="col-lg-12">
			

			<h2 class="text-center gregular font-large blackz">Browse or Support Current Campaigns</h2>
			<div class="row" style="padding-top: 20px;display:none;">
				<div class="container schoolstore-mob-holder">
				
				
<div class="mob-menu navbar navbar-default" role="navigation">
    
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapster">
            <span class="sr-only">Toggle navigation</span>
           <span class="glyphicon glyphicon-triangle-bottom"></span>
        </button>
       <a href="#" class="mob-selector navbar-brand gregular">Select Category</a>
    </div>
<div  id="collapsterz" class="collapse navbar-collapse category-lists">
		<ul class="nav nav-tabs store-nav artwork-categories">
			<li class="<?php if($cat_selected == 0): echo 'active'; endif; ?>">
				<a href="javascript:void(0);" class="t-cat"><label class="art-category" data-id="0">All</label></a>
			</li>
			<?php foreach ($category_z as $key => $cat) {
				// echo $cat->id;
				$active = ($cat_selected == $cat->id)? 'active' : ""; ?>	
				<li class="<?php echo $active; ?>"><a href="javascript:void(0);"><label class="art-category" data-id="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></label></a></li>
			<!--<li class="dropdown <?php echo $active; ?>"><a class="dropdown-toggle t-cat" data-toggle="dropdown" href="javascript:void(0);"><label class="art-category" data-id="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></label><?php  $subcat = $this->Category_model->get_all_artwork_sub_categories($cat->id); echo ( count($subcat) > 0 )? '<span class="caret"></span>' : ""; ?></a>-->
				<?php 
				//echo ( count($subcat) > 0 )? '<ul class="dropdown-menu '.$active.'">' : ""; 
				foreach ($subcat as $key => $sub){?>
					<!--<li><a href="javascript:void(0);" class="sub-cat"><div class="art-category" data-id="<?php echo $sub->id; ?>"><?php echo $sub->name; ?></div></a></li>-->
				<?php } 
				//echo ( count($subcat) > 0 )? '</ul>' : ""; ?>									
			<!--</li>-->
			<?php } ?>
		</ul>
    </div>
</div>




				</div>
			</div>
		</div>
	</div>
	<div class="row store-design-holder-outer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="store-design-holder  allcampaigns ">
						<?php if(isset($search)) echo $msg; ?>
						<div class="row">
							<?php 								 
								foreach (object_parseToString( $get_campaigns ) as $key => $campaign_val) {
									$type = $this->Campaigns_model->get_by_campaign_design_type( $campaign_val->template_type_id ); 
				
							 ?>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="design-item-holder camp-item">
									<div class="design-label gregular font-xsmall gray text-uppercase text-left"><span class="design-name gsemibold"><?php echo $campaign_val->display_name; ?></span>
									</div>
									<div class="store-image-box" style="background:#<?php echo $campaign_val->shirt_background_color; ?>;">
										<!-- <span class="design-backprint" style="opacity: 1;z-index: 1;"></span> -->
										<div class="store-selection-holder">
											<span class="design-unselect"></span>
											<span class="design-select"></span>
										</div>
										<?php $thumbnail = str_replace("/200/200", "", $campaign_val->thumbnail); ?>
										<a href="<?php echo base_url().$campaign_val->campaign_url; ?>"><img class="img-responsive img-center" src="<?php echo $thumbnail;?>" alt="" /></a>
									</div>
									<a class="shirt-name-link" href="<?php echo base_url().$campaign_val->campaign_url; ?>"><h3 class="gsemibold font-medium title-c-height"><?php echo $campaign_val->campaign_title; ?></h3>
									<div class="lbl-overflow gray" title="<?php echo db_toString( $campaign_val->school_name ); ?>"><?php echo db_toString( $campaign_val->school_name ); ?></div></a>
									
								</div>
								
							</div>

							<?php } ?>						
						
						</div>
					
				</div>
			</div>
		</div>
		</div>
		
	</div>
	
	<!--pagination-->
	<div class="row">
		
		 <div class="form-group text-center">
            <div class="row">
                <nav aria-label="Page navigation example">
                    <?php echo $this->pagination->create_links();?>               
                </nav>
            </div>
        </div>
	</div>
	<!--end of pagination-->
	
</div>

<?php $this->load->view_store('footer');  ?>  



