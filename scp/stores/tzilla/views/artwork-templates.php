<style type="text/css">
	.list-of-functions, .display-result-here, .art-template
	{
		float: left;
	}

	.list-of-functions
	{
		width: 19%;
	}

	.display-result-here
	{
		width: 80%;
	}

	.art-template 
	{
		border: 1px solid black;
		background: #cccccc;
		width: 200px;
		height: 200px;
	}

</style>

<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/jquery-1.9.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/artwork-templates.js').'?df='.rand(1, 99999);?>"></script>

<script type="text/javascript">
	setMainUrl( '<?php echo base_url(); ?>' );
</script>
<div class="list-of-functions">
	<div>goto<a href="#" class="next-page">TGEN</a> </div>
	<div>backend functions for art templates page <a href="#" class="clear-result">clear</a> </div>
	<a href="#" class="get-artworks">function 1 - getArtworks (Done)</a><br>
	<a href="#" class="get-categories">function 3 - getCategories (Done)</a><br>
	<a href="#" class="search-school">function 2 - seachArtworks(bySchool) (Done)</a><br>
	<a href="#" class="search-mascot">function 4 - seachArtworks(byMascot) (Done)</a><br>
	<a href="#" class="search-popular">function 5 - seachArtworks(byMostPopular) (Done)</a><br>
	<a href="#" class="search-newest">function 6 - seachArtworks(byNewest) (Done)</a><br>
	<a href="#">function 7 - seachArtworks(byAjaxPaginate)</a> ( <div class="pagination-links"><a href="#">1</a> <a href="#">2</a> <a href="#">3</a></div> )


	<div class="selected-artworks">
		
	</div>
</div>
<div class="display-result-here">
	
</div>