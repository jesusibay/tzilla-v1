
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/about.css');?>">
<div class="container-fluid about-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <h1 class="white">About Us</h1>
                <p class="gray">Simply get a shirt for yourself, or create a campaign<br>to raise funds for your school</p>
            </div>
        </div>
    </div>
</div>
<div class="containter-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="cont-holder">
					<h2 class="blackz gregular">Design to Change the World</h2>
					<p class="gray gregular font-small">We're artists. Like you, we're busy people who want better shirt designs than the status quo (and in less time). We've spent two years developing an industry-first: A super-easy way to create a collection of great designs that can all be personalized (to make them even greater), and then use them in a campaign to raise funds for the causes that matter to you. No up-front costs, inventory, or hassles. It's free, so give it a go. TZilla⎯Design Something Great.
					</p>
				</div>
				<div class="cont-holder">
					<h3 class="blackz gregular">The Core Values of TZilla</h3>
					<p class="gray gregular font-small">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
			</div>
			
		</div>
	</div>
</div>

