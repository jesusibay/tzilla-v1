<?php $this->load->view_store('header');  ?>  

<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/school-store.css');?>">
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/school-store.js').'?df='.rand(1, 99999);?>"></script>

<script type="text/javascript">
	setArtworkCategories( '<?php echo $category; ?>' );
	setSchoolId( '<?php echo $school->school_id; ?>' );
	setPreviousLoadedDesigns( '<?php echo json_encode( $this->session->userdata("designs") ); ?>' );
	setPrimaryColor( '<?php echo $primary_color_hex->hex_value; ?>' );
	setSecondaryColor( '<?php echo $secondary_color_hex->hex_value; ?>' );
</script>

<div class="container-fluid store-header">
	<div class="school-bg"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-7 col-sm-12 col-xs-12">
				<h1 class="white"><?php echo $school->school_name; ?></h1>
				<span class="sub-title gregular font-large">Home of the <?php echo $school->school_mascot; ?> </span>
				<span class="sub-title2 gregular font-medium white">Apparel Store</span>
				<p class="white">Simply get a shirt for yourself, or create a campaign<br>to raise funds for your school</p>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row store-design-header">
		<div class="col-lg-12">
			<h2 class="text-center gregular font-large blackz">Select Designs You'd Like to Customize</h2>
			<div class="row" style="padding-top: 20px;">
				<div class="container schoolstore-mob-holder">
					<div class="mob-menu navbar navbar-default" role="navigation">
					    <div class="navbar-header">
					        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapsterz">
					            <span class="sr-only">Toggle navigation</span>
					           <span class="glyphicon glyphicon-triangle-bottom"></span>
					        </button>
					       <a href="#" class="mob-selector navbar-brand gregular">Select Category</a>
					    </div>
					    <div  id="collapsterz" class="collapse navbar-collapse category-lists">
							<ul class="nav nav-tabs store-nav artwork-categories no-border">
								<li class="<?php if($cat_selected == 0): echo 'active'; endif; ?>">
									<a href="javascript:void(0);" class="t-cat"><label class="art-category" data-id="0">All</label></a>
								</li>
								<?php foreach ($category_z as $key => $cat) {
									$active = ($cat_selected == $cat->id)? 'active' : ""; ?>	
									<li class="<?php echo $active; ?>"><a href="javascript:void(0);"><label class="art-category" data-id="<?php echo $cat->id; ?>"><?php echo $cat->name; ?></label></a></li>							
									
								<?php } ?>
							</ul>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row store-design-holder-outer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="store-design-holder">
						<div class="row artwork-results">
							<?php
							if( count( $templates ) > 0){
								foreach ($templates as $key => $template) {
									$bg = strlen($template->hex_value) > 0 ? 'style="background-color: #'.$template->hex_value.'"' : '';
									$img = $template->thumbnail;
							 ?>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="design-item-holder">
									<div class="design-label gregular font-small gray text-uppercase text-left lbl-overflow"><span class="design-name gsemibold design-name"><?php echo $template->display_name; ?></span></div>
									<div class="store-image-box" <?php echo $bg; ?> >
										<div class="store-selection-holder">
											<span class="design-select"></span>
										</div>
										<img class="img-responsive img-center designmain design-template-image" data-id="<?php echo $template->template_id; ?>" data-source="<?php echo $img; ?>" src="<?php echo $img; ?>" alt="" />
									</div>
									<div class="store-holder-variation design-variation-selection" data-hex="<?php echo $template->hex_value; ?>">
										<div class="store-image-variation" data-parent="<?php echo $template->template_id; ?>">
											<div class="variation-title gregular font-small gray-darker text-center">Select a Variation</div>
											<div class="variation-slider">
												<div class="owl-carousel variation_design design-variations">
													<div class="design-variation-holder" id="design1"><img class="img-responsive img-center design-image" src="<?php echo base_url('public/'.STORE.'/images/a.1.png');?>" alt="" /></div>
													<div class="design-variation-holder" id="design2"><img class="img-responsive img-center design-image" src="<?php echo base_url('public/'.STORE.'/images/a.2.png');?>" alt="" /></div>
												</div>
												<div class="variation-arrow design-variation-prev gregular font-large">«</div>
												<div class="variation-arrow design-variation-next gregular font-large">»</div>
											</div>
										</div>
										<button class="orange-btn adddesign-variation-btn text-uppercase white gregular font-xsmall add-design">ADD THIS DESIGN</button>
									</div>
									<button class="design-variation-btn text-uppercase white gregular font-xsmall load-variations" data-id="<?php echo $template->template_id; ?>" temp-id="<?php echo $template->template_id; ?>">SEE DESIGN VARIATIONS</button>
								</div>
							</div>
							<?php }}else{ echo 'NO DESIGN'; } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- DESIGN CART -->
		<div class="design-container">
			<div class="design-cont-btn dc-close"></div>
			<div class="design-cont-header gsemibold font-medium white text-center">Selected Designs (<label class="design-count">0</label>/6)<span type="button" class="close icon-circled-x pull-right font-large login-close selected-des-close"></span></div>
			<div class="design-cont-item-holder">
				<div class='empty-collection gregular gray-light'>Empty</div>
			</div>
			<div class="design-cont-footer">
				<button class="gray-btn customize-btn white gsemibold font-small customize-designs" disabled="disabled">Customize</button>
			</div>
		</div>
	</div>
	
	<!-- pagination -->
	
		<div class="form-group text-center">
            <div class="row">
                <nav aria-label="Page navigation example">
				 <?php echo $this->pagination->create_links();?>               
                </nav>
            </div>
        </div>
	
	<!-- end of pagination -->
	
</div>

<?php if( count( $school_campaign) > 0 ):?>
	<div class="container-fluid">
		<div class="row">
			
				<div class="current-camp-header text-center">
					<h2 class="text-center gregular font-large gray">Current Campaigns For <?php echo $school->school_name; ?></h2>
				</div>
			
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			
				<div class="current-camp-holder text-center">
					<div class="container">
							
						<div class="row campaign_section">
						
						<?php 
							foreach ($school_campaign as $key => $campaigns) {
							$sales = $this->Campaigns_model->get_campaign_sales( $campaigns['campaign_url']);
                            $total_base_price = $this->Campaigns_model->get_campaign_base_price( $campaigns['campaign_url']);
    						$sub = $total_base_price['total_profit'];
                            $img = $campaigns['thumbnail'];
							$status = "";
							if($campaigns['campaign_status'] == 1){
								$status = '<span class="stat text-uppercase gsemibold font-medium green">Active</span>';
							}

							if($campaigns['campaign_status'] == 3){
								$status = '<span class="stat ended text-uppercase gsemibold font-medium green">Ended</span>';
							}?>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 vertical-spacing text-center ">
								<div class="image-box" style="background:#<?php echo $campaigns['shirt_background_color']; ?>;">
									<a href="<?php echo base_url( $campaigns['campaign_url'] );?>"><img class="image-overflow-center temporary" src="<?php echo $img; ?>" alt="" /></a>
								</div>
								
									<a class="shirt-name-link" href="<?php echo base_url( $campaigns['campaign_url'] );?>"><h3 class="gsemibold font-medium text-uppercase lbl-overflow"><?php echo ucwords(ucfirst($campaigns['campaign_title'] )); ?></h3></a>
									<span class="text-uppercase">ENDS: <?php echo date("m/d/Y", strtotime($campaigns['end_date'])); ?></span>
									<span class="pr text-uppercase">$<?php echo (!empty($sales['totalprice']))? number_format($sales['totalprice'],2) : '0.00'; ?></span>
									<?php echo $status; ?>
								
							</div>
						<?php } ?>

						</div>
					</div>
				</div>
			
		</div>
	</div>
	
	<?php endif; ?>
	
	<!-- Modal confirm-->
<div id="confirmitem" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Can't add more than 6 styles</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">You can only change to another design variation or remove<br> a design to add another</div>
						<div class="confirm-btn-holder text-center">
							<button class="orange-btn yesremove-btn white gsemibold font-small" data-dismiss="modal">Go back</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>

<?php $this->load->view_store('footer'); ?>