<?php $this->load->view_store('account-header');  ?> 

                <div class="row row form-group">
                    <div class="col-md-10">
                        <h3 class="title">My Orders</h3>
                         <p class="">Order Tracking</p>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                        <a href="<?php echo base_url('account-orders'); ?>"><button class="orange-btn cartrev-update-btn white gsemibold font-small margin-btn2">Go Back</button></a>
                    </div>
                </div>
                <section class="section">
                    <div class="row sameheight-container">
                        <div class="col-lg-6 col-md-12">
                            <div class="card card-block sameheight-item">
                                <div class="row form-group">
                                    <div class="col-xs-12">
                                        <div class="title-block">
                                            <h3 class="title">Track Your Order</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-xs-6">
                                        <label for="">Enter the tracking number sent to your email</label>
                                        <input type="text" class="form-control handle_enter" id="tracking_number" placeholder="eg. 00001114401" value="<?php echo $tracking_number; ?>" data-enter="tracking_btn" onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="col-md-offset-1  col-md-5 col-xs-12">
                                        <button id="tracking_btn" class="orange-btn cartrev-update-btn white gsemibold font-small margin-btn">Track</button>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php if( count($tracking) > 0){ ?>
                <section class="section">
                    <div class="row sameheight-container">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="blackz gsemibold font-medium text-center">Status of Ref #:
                                                <span class="blackz gsemibold font-medium text-center"><?php echo $tracking_number; ?></span>
                                            </div>
                                            <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/order-tracking-01.jpg');?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="section">
                    <div class="row sameheight-container">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <div class="track-activ gsemibold blackz font-large">Activities</div>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <div class="dash-subtitle">
                                                <p class="gregular">Travel History</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-md-12">
                                            <div class="panel-group" id="accordion">
                                                <div class="panel panel-default pointer" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                                    <div class="panel-heading">
                                                      <h4 class="panel-title">
                                                        <div data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="gregular font-regular blackz">
                                                        <span class="blackz track-collapse glyphicon glyphicon-plus"></span> 6/15/2016 - Wednesday</div>
                                                      </h4>
                                                    </div>
                                                    <div id="collapse1" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="gregular font-regular blackz">Date/Time</th>
                                                                            <th class="gregular font-regular blackz">Activity</th>
                                                                            <th class="gregular font-regular blackz">Location</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>12:00 PM</td>
                                                                            <td>Delivered</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>08:51 AM</td>
                                                                            <td>On FedEx vehicle for Delivery</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>06:31 AM</td>
                                                                            <td>At local FedEx facility</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default pointer">
                                                    <div class="panel-heading">
                                                      <h4 class="panel-title">
                                                        <div data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="gregular font-regular blackz">
                                                        <span class="blackz track-collapse glyphicon glyphicon-plus"></span> 6/15/2016 - Wednesday</div>
                                                      </h4>
                                                    </div>
                                                    <div id="collapse2" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="gregular font-regular blackz">Date/Time</th>
                                                                            <th class="gregular font-regular blackz">Activity</th>
                                                                            <th class="gregular font-regular blackz">Location</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>12:00 PM</td>
                                                                            <td>Delivered</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>08:51 AM</td>
                                                                            <td>On FedEx vehicle for Delivery</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>06:31 AM</td>
                                                                            <td>At local FedEx facility</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="panel panel-default pointer" data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                                    <div class="panel-heading">
                                                      <h4 class="panel-title">
                                                        <div data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="gregular font-regular blackz">
                                                        <span class="blackz track-collapse glyphicon glyphicon-plus"></span> 6/15/2016 - Wednesday</div>
                                                      </h4>
                                                    </div>
                                                    <div id="collapse3" class="panel-collapse collapse">
                                                        <div class="panel-body">
                                                            <div class="table-responsive">
                                                                <table class="table table-striped table-bordered table-hover">
                                                                    <thead>
                                                                        <tr>
                                                                            <th class="gregular font-regular blackz">Date/Time</th>
                                                                            <th class="gregular font-regular blackz">Activity</th>
                                                                            <th class="gregular font-regular blackz">Location</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>12:00 PM</td>
                                                                            <td>Delivered</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>08:51 AM</td>
                                                                            <td>On FedEx vehicle for Delivery</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>06:31 AM</td>
                                                                            <td>At local FedEx facility</td>
                                                                            <td>Los Angeles, CA</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>    
                <?php }else{ echo '<span style="">Check you reference number.</span>'; } ?>                
            </article>
            



      
<script type="text/javascript">
    $(document).ready(function(){      
        $('#collapse1').on('shown.bs.collapse', function () {
           $(this).find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        });

        $('#collapse1').on('hidden.bs.collapse', function () {
           $(this).find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
        
        $('#collapse2').on('shown.bs.collapse', function () {
           $(this).find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        });

        $('#collapse2').on('hidden.bs.collapse', function () {
           $(this).find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
        
        $('#collapse3').on('shown.bs.collapse', function () {
           $(this).find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        });

        $('#collapse3').on('hidden.bs.collapse', function () {
            $(this).find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
        $("#storetitle").text('TZilla.com - Account Order Tracking');
    });
</script>

