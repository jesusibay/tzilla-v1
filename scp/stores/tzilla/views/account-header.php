<!DOCTYPE html>
<html lang="en">
	<head>
	  <title id="storetitle">Tzilla.com - Account Dashboard</title>
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <!-- account dashboard -->
	  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
	  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/dashboardcss/dashboard-custom.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/dashboardcss/vendor.css');?>">        
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/dashboardcss/app-seagreen.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/dashboardcss/app.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/main.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/cart-review.css');?>">
	 
	  <!-- head -->
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/bootstrap.min.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/fonts/stylesheet.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/fonts/tzilla-icons.css');?>">
	  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/main.css');?>">
	  <script src="<?php echo base_url('public/'.STORE.'/js/jquery.min.js');?>"></script>
	  <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
      <script src="<?php echo base_url('public/'.STORE.'/js/vendor.js');?>"></script>
      <script src="<?php echo base_url('public/'.STORE.'/js/app.js');?>"></script>
	  <!--  -->
	</head>
    	<body>
        <div class="loader-container">
            <div id="loader-img"></div>
        </div>
    <div class="main-wrapper">
            <div class="app" id="app">

	<header class="header">
        <div class="header-block header-block-collapse hidden-lg-up"> 
            <button class="collapse-btn mob-slide-btn" id="sidebar-collapse-btn"><i class="fa fa-bars"></i></button>
        </div>

        <!-- breadcrumbs -->
        <ul class="breadcrumb breadcrumb-dash font-small">
            <li>My Account</li>
            <li class="<?php echo ( isset($thr))? '' : 'active-dash';  ?>"><?php echo $br; ?></li>
            <?php if( isset($thr)) echo '<li class="active-dash"><a href="javascript:void(0);">'.$thr.'</a></li>'; ?>
        </ul>
        <!--  -->
        
        <div class="header-block header-block-nav">
            <ul class="nav-profile">
                <?php $this->load->module('account/Account_model');
                $payout_amount = $this->Account_model->get_payout_amount( $this->session->id );
                $total_sales = $this->Account_model->get_all_total_sales_count( $this->session->customer_id );
                $total_profit = $this->Account_model->get_all_total_profit_count( $this->session->customer_id ); ?>
                <?php if ( $link == "payout"): ?><div class="gregular font-medium blackz balance-cont">Balance: <span class="grayz gregular font-regular">$<?php echo (!empty($payout_amount['payout_amount']))? number_format($payout_amount['payout_amount'],2) : '0.00'; ?> / $<?php echo (!empty($total_profit['total_profit']) )? number_format( $total_profit['total_profit'], 2 ) : '0.00'; ?></span></div><?php endif;?>  
                <li class="profile dropdown">
                    <button class="btn btn-default dropdown-toggle header-dropdown2 rounded" type="button" id="menu1" data-toggle="dropdown"><span class="icon-user-icon"></span>
                        
                     </button>
                    <div class="dropdown-menu profile-dropdown-menu head-bg" aria-labelledby="dropdownMenu1">
                        <!-- <a class="dropdown-item" href="<?php echo base_url('account-profile'); ?>"> <i class="fa fa-user icon"></i> Profile </a>
                        <a class="dropdown-item" href="#"> <i class="fa fa-bell icon"></i> Notifications </a> -->
                        <!-- <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="<?php echo base_url(); ?>"> <i class="fa fa-gear icon whitez"></i> Go to Home Page </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?php echo base_url('signin/signout'); ?>"> <i class="fa fa-power-off icon whitez"></i> Log Out </a>
                    </div>
                </li>
            </ul>
        </div>
    </header>
    <aside class="sidebar">
        <div class="sidebar-container">
            <div class="sidebar-header">
                <!-- <div class="brand">
                    <div class="logo"> <span class="l l1"></span> <span class="l l2"></span> <span class="l l3"></span> <span class="l l4"></span> <span class="l l5"></span> </div> Modular Admin </div> -->
                <div class="tzilla-logo">
                    <a href="<?php echo base_url(); ?>">
                        <img class="img-responsive " src="<?php echo base_url('public/'.STORE.'/images/tzilla-logo.png');?>" alt=""/>
                    </a>
                </div>
            </div>
            <nav class="menu">
                <ul class="nav metismenu" id="sidebar-menu">
                    <li class="dash-dashboard <?php if ( $link == "dashboard"): echo 'active'; endif;?> ">
                        <a href="<?php echo base_url('account-dashboard'); ?>"> <i class="fa fa-tachometer"></i> Dashboard </a>
                    </li>
                    <li class="dash-campaigns <?php if ( $link == "campaigns"): echo 'active'; endif;?>">
                        <a href="<?php echo base_url('account-campaigns'); ?>"> <i class="glyphicon glyphicon-bullhorn"></i> My Campaigns </a>
                    </li>
                    <li class="dash-orders <?php if ( $link == "orders"): echo 'active'; endif;?>">
                        <a href="<?php echo base_url('account-orders'); ?>"> <i class="fa fa-shopping-cart"></i> Orders </a>
                    </li>
                    <li class="dash-payout <?php if ( $link == "payout"): echo 'active'; endif;?>" style="display:none;">
                        <a href="<?php echo base_url('account-payout'); ?>"> <i class="fa fa-usd"></i> Payout Settings </a>
                    </li>
                    <li class="dash-profile <?php if ( $link == "profile"): echo 'active'; endif;?>">
                        <a href="<?php echo base_url('account-profile'); ?>"> <i class="fa fa-user"></i> Profile Settings </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('signin/signout'); ?>"> <i class="fa fa-power-off"></i> Logout </a>
                    </li>
                </ul>
            </nav>
        </div>
        <footer class="sidebar-footer">
            <ul class="nav metismenu" id="customize-menu">
                <li>
                    <ul>
                        <li class="customize">
                            <div class="customize-item">
                                <div class="row customize-header">
                                    <div class="col-xs-4"> </div>
                                    <div class="col-xs-4"> <label class="title">fixed</label> </div>
                                    <div class="col-xs-4"> <label class="title">static</label> </div>
                                </div>
                                <div class="row hidden-md-down">
                                    <div class="col-xs-4"> <label class="title">Sidebar:</label> </div>
                                    <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed" >
                                <span></span>
                            </label> </div>
                                    <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="sidebarPosition" value="">
                                <span></span>
                            </label> </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4"> <label class="title">Header:</label> </div>
                                    <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="headerPosition" value="header-fixed">
                                <span></span>
                            </label> </div>
                                    <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="headerPosition" value="">
                                <span></span>
                            </label> </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4"> <label class="title">Footer:</label> </div>
                                    <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="footerPosition" value="footer-fixed">
                                <span></span>
                            </label> </div>
                                    <div class="col-xs-4"> <label>
                                <input class="radio" type="radio" name="footerPosition" value="">
                                <span></span>
                            </label> </div>
                                </div>
                            </div>
                            <div class="customize-item">
                                <ul class="customize-colors">
                                    <li> <span class="color-item color-red" data-theme="red"></span> </li>
                                    <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
                                    <li> <span class="color-item color-green" data-theme="green"></span> </li>
                                    <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
                                    <li> <span class="color-item color-blue active" data-theme=""></span> </li>
                                    <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <!-- <a href=""> <i class="fa fa-cog"></i> Customize </a> -->
                </li>
            </ul>
        </footer>
    </aside>
<div class="sidebar-overlay" id="sidebar-overlay"></div>

<article class="content forms-page">
<!-- breadcrumbs mobile -->
    <div class="container-fluid breadcrumb-outer">
        <ul class="breadcrumb breadcrumb-dash-mob font-small">
            <li><a href="#">My Account</a></li>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Payout Settings</a></li>
            <li class="active-dash"><a href="#">000001</a></li>
        </ul>
    </div>
<!--  -->

<!-- For Zheng-->
<div id="conModal" class="modal fade" role="dialog">
      <div class="modal-dialog cantfind-dialog">
        <!-- Modal content-->
        <div class="modal-content confirm-modal-holder modal-notification-width ">
          <div class="confirm-header white gregular font-medium">
                <span type="button" class="close icon-circled-x pull-right font-large verify-close" data-dismiss="modal"></span>
          </div>
          <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 text-center">                     
                        <div class="warning-label2 gregular font-regular text-center text-confirm"></div>
                    </div>
                </div>
          </div>
        </div>
      </div>
</div>

<?php 
$this->session->unset_userdata("product_details");
$this->session->unset_userdata("go_back");
if($this->session->is_logged_in==FALSE){ 
    redirect( base_url() );
}?>
<script src="<?php echo base_url('public/'.STORE.'/js/account-header.js');?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    set_mainLink( '<?php echo base_url(); ?>' );

    $('.nav-profile').on('click', function() {
        setTimeout(function() {
            $('.profile').toggleClass('open');
        })
    });
});
</script>