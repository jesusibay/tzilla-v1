<?php $this->load->view_store('header');  ?>  
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/createcampaign.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/jquery.countdown.css');?>">
<script src="<?php echo base_url('public/'.STORE.'/js/jquery.plugin.js');?>"></script>
<script src="<?php echo base_url('public/'.STORE.'/js/jquery.countdown.js');?>"></script>
<?php 
	$date = new DateTime( $campaign_live['end_date'] );
	$new_campaign_end = $date->format('F d, Y H:i:s');
	$startdate = new DateTime( $campaign_live['end_date'] );
	$new_start = $startdate->format('F d, Y H:i:s');
	$today = date("Y-m-d H:i:s");
	$campaign_percentage = 0;
	$sub = 0;

	$sales = $this->Campaigns_model->get_campaign_sales( $campaign_live['campaign_url']);
    $total_base_price = $this->Campaigns_model->get_campaign_base_price( $campaign_live['campaign_url']);
    $sub = $total_base_price['total_profit'];

	$campaign_percentage = round( ( $sub / $campaign_live['campaign_goal'] ) * 100, 2 );

	if($campaign_percentage > 100){
		$width = 100;
	}else{
		$width = $campaign_percentage;
	}
$this->session->unset_userdata("product_details");
$this->session->set_userdata( "school_id", $campaign_live['school_id'] );
$this->session->set_userdata( "school_name", db_toString( $campaign_live['school_name'] ) );
?>
<?php if(isset($campaign_live)): if( $campaign_live['campaign_cover_photo'] == 1 ){ ?>
<img src="<?php echo base_url('public/tzilla/campaigns').'/'.$campaign_live['campaign_url'].'.jpg'; ?>" id="checkphoto_path" style="display:none;"/> 
<?php } endif; ?>
<input type="hidden" id="campaign_id" value="<?php echo $campaign_live['campaign_id']; ?>"> <!-- campaign id -->
<input type="hidden" id="school_id" value="<?php echo $campaign_live['school_id']; ?>"> <!-- school id -->
<section class="container-fluid">
	<div class="row">
		<div class="camp-header-holder" <?php if(isset($campaign_live)): if( $campaign_live['campaign_cover_photo'] == 1 ){ $img = base_url('public/tzilla/campaigns').'/'.$campaign_live['campaign_url'] .'.jpg';  echo 'style="background:url('.$img.'); background-repeat:no-repeat; background-position:center; background-size: cover;"'; } endif; ?> >
			<div class="container">
				<div class="camp-title-holder camp-prev-titleholder">
					<h1 class="camp-title white gregular"><?php echo $campaign_live['campaign_title']; ?></h1>
					<div class="camp-sub-title white gregular font-large"><?php echo db_toString( $campaign_live['school_name'] ); ?></div>
					<p class="camp-p white gregular font-small"><?php echo db_toString( $campaign_live['school_address_1'] ); ?></p>
					<!-- <div class="cam-title-dash"></div> -->
				</div>
			</div>
			<div class="gradient"></div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<div class="camp-sidebar">
				
				  <div class="camp-sidebar-title gregular font-medium blackz camp-prev-title"><?php echo $campaign_live['campaign_title']; ?></div>
				  <p class="camp-sidebar-p gregular font-small gray-dark camp-prev-sidebar-p"><?php echo $campaign_live['campaign_description']; ?></p>
				  <hr class="camp-devider">
				  <div class="camp-fundraised-title gregular font-medium blackz">Share this Campaign</div>
				  <div class="campprev-share-icon-holder">
						<span class="icon-facebook live-fb" data-url="<?php echo base_url($campaign_live['campaign_url']);?>" data-text="<?php echo $campaign_live['campaign_title']; ?>" data-via="tzilladotcom"></span>
						<span class="icon-twitter live-twitter" data-url="<?php echo base_url($campaign_live['campaign_url']);?>" data-text="<?php echo $campaign_live['campaign_title']; ?>" data-via="tzilladotcom"></span>
						<span class="icon-gplus live-gplus" data-url="<?php echo base_url($campaign_live['campaign_url']);?>" data-text="<?php echo $campaign_live['campaign_title']; ?>" data-via="tzilladotcom"></span>
						<span class="icon-email live-email" data-url="<?php echo base_url($campaign_live['campaign_url']);?>" data-text="<?php echo $campaign_live['campaign_description']; ?>" data-via="tzilladotcom"></span>
				  </div>
				  <hr class="camp-devider">
				  <div class="camp-fundraised-title gregular font-medium  blackz">Funds Raised</div>
				  <div class="camp-todate gregular font-small gray">To date</div>
				  <div class="sidebar-loadrail"><div class="sidebar-loadgreen" style="width:<?php echo $width; ?>%;"><span class="sidebar-tooltip p-hover" style="display:none;">$<?php echo number_format($sub,2); ?></span></div></div>
				  <div class="camp-goal-min gregular font-small gray-dark pull-left">$0.00</div>
				  <div class="camp-goal-max gregular font-small gray-dark pull-right">$<?php echo $campaign_live['campaign_goal']; ?></div>
				  <div class="clearfix"></div>
				 
				  
				  <hr class="camp-devider1">
				  <div class="camp-length-title gregular font-medium  blackz">Campaign Duration</div>
				  <div class="camp-last-day gregular font-small gray-dark text-center"><?php echo date("m-d-Y", strtotime($campaign_live['start_date'])); ?> to <?php echo date("m-d-Y", strtotime($campaign_live['end_date'])); ?></div>
				  <!--<div class="camp-lastday-title gregular font-large blackz">Last day to order is</div>
				  <div class="camp-last-day gregular font-small gray-dark text-center">June 16, 2015</div>-->
				  <div class="camp-lastday-title gregular font-medium  blackz">This Campaign ends in</div>
				  <div id="camptimer2" class="gregular font-small gray-dark text-center"></div>
				  <hr class="camp-devider1">
				  <div class="camp-report-abuseholder"><a href="<?php echo base_url('contact-us'); ?>" class="camp-report-abuse gregular font-regular text-uppercase">Report Abuse</a>
				  	<span class="q-icon">
						<div class="q-pop">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</div>
					</span>
				  </div>
				  
				</div>
			</div>
			<div class="col-lg-9 camp-cta-outer-holder">
			
				<div class="row">
					<?php if(isset( $campaign_designs )):
					$counter = 0;
					foreach ($campaign_designs as $key => $design) {  
					$type = $this->Campaigns_model->get_by_campaign_design_type( $design['template_type_id'] ); 
					$parent = $this->Campaigns_model->get_by_campaign_design_parent( $design['template_id'] );  
					$img = (!empty($design['thumbnail1']))? $design['thumbnail1'] : $design['thumbnail']; ?>
					<div class="col-lg-4">
						<div class="camp-design-item-holder">
							<div class="camp-design-label gregular font-xsmall gray text-uppercase"><span class="design-name gsemibold"><?php echo $design['display_name']; ?></span></div>
							<div class="camp-image-box" style="background:#<?php echo $design['shirt_background_color']; ?>;">
								<img class="img-responsive img-center" src="<?php echo $img; ?>" alt="" />
							</div>
							<input type="button" data-counter="<?php echo $counter; ?>" class="<?php echo($campaign_live['campaign_status'] == 3)? 'campaign-ended-btn' : 'camp-update-btn'; ?> buy-btn gsemibold font-regular" product-id="<?php echo $design['product_id']; ?>" data-id="<?php echo $design['template_id']; ?>" data-parent="<?php echo (!empty($parent['parent_template_id']))? $parent['parent_template_id'] : $design['template_id']; ?>" <?php echo($campaign_live['campaign_status'] == 3)? 'disabled="disabled" value="Ended"' : 'value="Buy"'; ?> >
						</div>
						<?php $ready = $this->Campaigns_model->get_campaign_product_items( $design['product_id']);
								$arr_shirt_plan = Array();
						        foreach ($ready as $key => $items) {
						            $colors = $this->Campaigns_model->get_campaign_colors_by_style_id($design['product_id'], $items['shirt_plan_id']);
						            $cols = "";
						            foreach ($colors as $col) {
						                $cols .= $col.',';        
									}
						            echo '<input type="hidden" class="shirt-plan-id" plan-id="'.$items['shirt_plan_id'].'" data-price="'.$items['price'].'" data-properties="'.htmlentities( $items['template_properties'] ).'" design-id="'.$items['design_id'].'" background-color="'.$items['bg_color'].'" data-art-position="'.$items['art_setting'].'" data-canvas-position="'.$items['canvas_setting'].'" data-colors="'.rtrim($cols,',').'">';  
						        }
								?>
					</div>
					<?php $counter++; }
					endif; ?>
					
				</div>
			</div>
		</div>
	</div>
</section>

<input id="start_date" type="hidden" value="<?php echo $new_start; ?>">
<input id="end_date" type="hidden" value="<?php echo $new_campaign_end; ?>">
<input id="today_date" type="hidden" value="<?php echo $today; ?>">
<input id="status" type="hidden" value="<?php echo $campaign_live['campaign_status']; ?>">
<input type="hidden" id="number_sold" value=""> <!-- use for progress bar -->

<script src="<?php echo base_url('public/'.STORE.'/js/campaign-live.js').'?df='.rand(1, 99999);?>"></script>
<?php $this->load->view_store('footer');  ?>  
<script>
$(document).ready(function(){
	setDateStarted( $("#start_date").val() );
	setDateEnded( $("#end_date").val() );

	$(function() { 
		setInterval(function(){ doCountDown(); }, 1000);
	});


  $(document).on("click", ".live-fb", function(e){
    e.preventDefault();
	    FB.ui(
	    {
	      method: 'feed',
	      name: "",
	      link: '<?php echo base_url( $campaign_live["campaign_url"] ); ?>',
	      caption: '<?php echo $campaign_live["campaign_title"]; ?>',
	      picture: $(".camp-image-box").eq(0).find("img").attr('src'),
	      display: 'iframe',
	      message: '',
	      description: "<?php echo $campaign_live["campaign_description"]; ?>"
	    });
    FB.Canvas.setSize({ width: 200, height: 200 });
  });

});
</script>

