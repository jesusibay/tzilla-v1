<style type="text/css">
	.list-of-functions, .display-result-here
	{
		float: left;
	}

	.list-of-functions
	{
		width: 25%;
	}

	.display-result-here
	{
		width: 74%;
	}

	img 
	{
		border: 1px solid black;
		background: grey;
	}
</style>

<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/jquery.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/fundraiser.js').'?df='.rand(1, 99999);?>"></script>
<script type="text/javascript">
	setMainUrl("<?php echo base_url(); ?>");
</script>
<div  class="list-of-functions">
	<div>Image Streaming for Set Price</div>
	<a href="#" class="load-collection-items">function 1 - load collection from TGEN JSON Format (Done)</a><br>
	<a href="#" class="load-shirts">function 1.a - load shirts selected for that design with price (Done)</a><br>
	<a href="#" class="load-colors">function 1.b - load colors selected for the shirt</a><br>
	<a href="#" class="save-products">function 2 - save the set pricing details to scp_products</a><br>
	<!-- <a href="#" class="get-artworks">function A - Set Pricing Image Stream</a><br> -->
	<a href="#" class="get-previewStoreFront">function A - Manage Store Front Image Stream</a><br>
</div>
<div class="display-result-here">
	
</div>

