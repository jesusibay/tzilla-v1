</body>
<footer class="footer">      	
<div class="footer-sect1">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="gregular font-large footer-stay-con">Stay Connected with <span class="gsemibold">TZilla</span></div>
			</div>
			<div class="col-lg-12">
				<span id="like_fb" data-url="https://facebook.com/tzilladotcom" class="footer-sect1-icon icon-facebook"></span>
				<span id="follow_twitter" data-url="https://twitter.com/tzilladotcom" class="footer-sect1-icon icon-twitter"></span>
				<span id="follow_gplus" data-url="https://plus.google.com/u/0/100752144155063306184/about" class="footer-sect1-icon icon-gplus"></span>
				<span id="follow_instagram" data-url="https://instagram.com/tzilladotcom" class="footer-sect1-icon icon-instagram"></span>				
			</div>
		</div>
	</div>
</div>

<div class="footer-sect2">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
			
				<div class="footer-logo-holder">
					<a href="<?php echo base_url(); ?>"><img class="img-responsive footer-logo" src="<?php echo base_url('public/'.STORE.'/images/tzilla-logo.png');?>" alt=""/></a>
				</div>
					<div class="footer-nav pull-left font-small gray-dark">
						<span class=""><a href="<?php echo base_url("about"); ?>">About Us</a></span><span class="footer-link-border font-xxsmall gray-light">|</span>
						<!-- <span><a href="<?php echo base_url("all-journals"); ?>">Blog</a></span><span class="footer-link-border font-xxsmall gray-light">|</span> -->
						<span><a href="<?php echo base_url("settings/size_specifications"); ?>">Products</a></span><span class="footer-link-border font-xxsmall gray-light">|</span>
						<!-- <span><a href="<?php echo base_url("contact-us"); ?>">Contact Us</a></span><span class="footer-link-border font-xxsmall gray-light">|</span> -->
						<span><a href="<?php echo base_url("order/track"); ?>">Track Your Order</a></span><span class="footer-link-border font-xxsmall gray-light">|</span>
						<span><a data-toggle="modal" data-target="#faq" data-dismiss="modal" href="">FAQs</a></span><span class="footer-link-border font-xxsmall gray-light">|</span>
						<span><a href="<?php echo base_url("contact-us"); ?>">Contact</a></span><span class="footer-link-border font-xxsmall gray-light"></span>
						
					</div>
				<p class="footer-detail footer-p text-justify font-small gray-dark">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
				</p>
				
				<div class="footer-copy font-xxsmall gray-dark">Copyright © 2014 TZilla   |  <a  class="gray-dark" data-toggle="modal" data-target="#termsCondition" data-dismiss="modal" href="">Terms & Conditions</a>  |  <a class="gray-dark" data-toggle="modal" data-target="#privacypolicy" data-dismiss="modal" href="">Privacy Policy</a></div>
				
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<p class="footer-email-text-header footer-p text-justify font-small gray-dark">Sign up for promotions, discounts and updates!</p>
				
				<div class="input-group footer-email-text-holder"  data-toggle="tooltip" data-placement="top" title="">
				 <input type="text" id="subscribe_email_text" class="footer-email-text form-control font-small gray-dark gitalic handle_enter" data-enter="subscribe_email_btn" placeholder="Enter your email address" value="<?php if($this->session->is_logged_in==TRUE): echo $this->session->logged_in_email; endif; ?>">
				  <span class="input-group-btn">
					<button id="subscribe_email_btn" class="btn btn-primary font-small btn-lg gsemibold footer-email-btn submit-width" type="button">Submit
						<span class="subscribe-loader">
							<img class="img-responsive img-loader2" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt="">
						</span>
					</button>
				  </span>
				</div>
				
				<p class="footer-p font-small gray-dark">Our TZilla support team is here for you</p>
				<p class="footer-p font-small gray-dark">MON - FRI: 9am to 6pm PST</p>
				
				<div class="footer-contact-holder">
					<span class="icon-email-2 footer-contact-icon"></span>
					<div class="footer-contact-left">
						<p class="footer-p font-xsmall gray-dark">Send us a message</p>
						<p class="footer-p font-xsmall gray-dark">support@tzilla.com</p>
					</div>
					<span class="icon-phone footer-contact-icon"></span>
					<div class="footer-contact-right">
						<p class="footer-p text-justify font-xsmall gray-dark">Any questions?</p>
						<p class="footer-p text-justify font-xsmall gray-dark">(888) 901-1636</p>
					</div>
				</div>
				
				<div class="">
					<span class="font-social gregular font-medium">Social</span>
					<span id="fb_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="footer-small-icon icon-facebook"></span>
					<span id="twitter_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="footer-small-icon icon-twitter"></span>
					<span id="gplus_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="footer-small-icon icon-gplus"></span>
					<span id="email_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="footer-small-icon icon-email"></span>					
					<!-- <span class="footer-small-icon icon-instagram"></span> -->
				</div>
				
			</div>
		</div>
	</div>
</div>
</footer>
	
<!-- privacy-->
<div id="privacypolicy" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder font-small gray-dark glight">
				<div class="container-fluid">
					<div class="row">
						
							<div class="privacypolicy-content">
								<h3 class="gregular font-large gray-darker privacy-title">Privacy Policy</h3>
								<div class="term-privacy">
								<p class="term-privacy">
									TZilla acknowledges and highly values your privacy. This Privacy Policy covers the personal information that TZilla collects, uses, or discloses and sets out the way in which TZilla manages and protects your personal information when you visit our websites, Tzilla.net, TZilla.org, and TZilla.com; set up a campaign; donate; or place an order via our online storefront.
								</p>
									<p class="term-privacy pad">
									This policy is effective upon your use of this site. However, whenever necessary, TZilla may revise this policy from time to time.
								</p>
									<p class="term-privacy pad">
									<strong>How we collect your information</strong>
								</p>
									<p class="term-privacy">
									TZilla may knowingly collect personal information from you, which may include:
								</p>
									<p class="term-privacy">
									Your name, address, birthdate (to verify your age), email address, and phone number
								</p>
									<p class="term-privacy">
									Your credit card details and billing information
								</p>
									<p class="term-privacy pad">
									Like most websites, each time you visit, Tzilla.net, TZilla.org, and TZilla.com will automatically collect information about your computer such as your type of browser, date and time of visit, operating system, and your server’s IP address.
								</p>
									<p class="term-privacy">
									TZilla will take responsible steps to advise you if TZilla finds it necessary to collect your information from someone else or any other party.
								</p>
									<p class="term-privacy pad">
									<strong>How we use your information</strong>
								</p>
									<p class="term-privacy">
									TZilla may use the personal information you have knowingly submitted to store and process information in order to provide you with its goods and services. These may include, but are not limited to:
								</p>
								<ul>
									<li>To set up your personal TZilla account</li>
									<li>To set up your campaign</li>
									<li>To process and fulfil your purchase/s</li>
									<li>To determine the number of visitors to our websites</li>
									<li>To conduct reviews of our services and websites</li>
									<li>To award prizes and discounts</li>
									<li>To respond to any of your specific requests</li>
									<li>To update you with the changes in our policies, services, and websites</li>
									<li>To conduct market researches to improve TZilla’s websites and services</li>
									<li>To send you offers and information on products and services which may be of interest to you</li>
									<li>To verify information should any conflict arise with other users</li>
								</ul>
									<p class="term-privacy">
									TZilla may request you to submit the email addresses of your friends or relatives in order to promote our services and/or the campaign you have set up on TZilla.com. Rest assured, TZilla will only use them for the success of your campaign and not disclose nor sell them elsewhere.
								</p>
									<p class="term-privacy pad">
									<strong>Access via Third Party</strong>
								</p>
									<p class="term-privacy">
									TZilla.com may be accessed via links on other websites and third-party applications such as, but not limited to:
								</p>
								<ul>
									<li>Facebook</li>
									<li>Twitter</li>
									<li>Instagram</li>
									<li>Google Plus</li>
									<li>Email</li>
								</ul>
									<p class="term-privacy pad">
									<strong>Security and Accuracy</strong>
								</p>
									<p class="term-privacy">
									TZilla will take responsible steps to protect your personal information from misuse, loss, or any unauthorized modification.
								</p>
									<p class="term-privacy">
									You may access and request modification of your personal information at any time. TZilla also gives you the option to request the deletion of your account and personal information by contacting us as long as it does not create any conflict with any campaign and/or other TZilla users.
								
									However, with or without consent, TZilla may access and disclose your information should we be requested by law or any government body. We will do so in order to investigate frauds and/or validity of campaigns, clear conflicts with other users, and protect our rights, your rights, and the rights of other TZilla users.
								</p>
									<p class="term-privacy pad">
									<strong>Cookies</strong>
								</p>
									<p class="term-privacy">
									Whenever you visit Tzilla.net, TZilla.org, and TZilla.com, our web server may send “cookies” to your machine. Cookies are pieces of information which help us recognize your re-visit and coordinate your access to different pages on Tzilla.net, TZilla.org, and TZilla.com.
								</p>
									<p class="term-privacy">
									With most Internet Browsers, you can erase cookies from your machine, block them, or be advised before cookies are stored. To learn more, you may refer to your browser instructions and options. However, should you disable all cookies, you may not be able to take advantage of all the features of our site.
								</p>
									<p class="term-privacy">
									Links to other sites
								</p>
									<p class="term-privacy pad">
									TZilla may provide links to other websites on its own websites, Tzilla.net, TZilla.org, and TZilla.com. A link to another website is not an endorsement of the accuracy nor trustworthiness of that website nor its content.
								</p>
								</div>
							</div>
						
					</div>
				</div>
	  </div>
	
	
	</div>
  </div>
</div>


<!-- refund-->
<div id="refundpolicy" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder font-small gray-dark glight">
				<div class="container-fluid">
					<div class="row">
						
							<div class="privacypolicy-content">
								<h3 class="gregular font-large gray-darker privacy-title">Privacy Policy</h3>
								<div class="term-privacy">
								<p class="term-privacy">
									TZilla acknowledges and highly values your privacy. This Privacy Policy covers the personal information that TZilla collects, uses, or discloses and sets out the way in which TZilla manages and protects your personal information when you visit our websites, Tzilla.net, TZilla.org, and TZilla.com; set up a campaign; donate; or place an order via our online storefront.
								</p>
									<p class="term-privacy pad">
									This policy is effective upon your use of this site. However, whenever necessary, TZilla may revise this policy from time to time.
								</p>
									<p class="term-privacy pad">
									<strong>How we collect your information</strong>
								</p>
									<p class="term-privacy">
									TZilla may knowingly collect personal information from you, which may include:
								</p>
									<p class="term-privacy">
									Your name, address, birthdate (to verify your age), email address, and phone number
								</p>
									<p class="term-privacy">
									Your credit card details and billing information
								</p>
									<p class="term-privacy pad">
									Like most websites, each time you visit, Tzilla.net, TZilla.org, and TZilla.com will automatically collect information about your computer such as your type of browser, date and time of visit, operating system, and your server’s IP address.
								</p>
									<p class="term-privacy">
									TZilla will take responsible steps to advise you if TZilla finds it necessary to collect your information from someone else or any other party.
								</p>
									<p class="term-privacy pad">
									<strong>How we use your information</strong>
								</p>
									<p class="term-privacy">
									TZilla may use the personal information you have knowingly submitted to store and process information in order to provide you with its goods and services. These may include, but are not limited to:
								</p>
								<ul>
									<li>To set up your personal TZilla account</li>
									<li>To set up your campaign</li>
									<li>To process and fulfil your purchase/s</li>
									<li>To determine the number of visitors to our websites</li>
									<li>To conduct reviews of our services and websites</li>
									<li>To award prizes and discounts</li>
									<li>To respond to any of your specific requests</li>
									<li>To update you with the changes in our policies, services, and websites</li>
									<li>To conduct market researches to improve TZilla’s websites and services</li>
									<li>To send you offers and information on products and services which may be of interest to you</li>
									<li>To verify information should any conflict arise with other users</li>
								</ul>
									<p class="term-privacy">
									TZilla may request you to submit the email addresses of your friends or relatives in order to promote our services and/or the campaign you have set up on TZilla.com. Rest assured, TZilla will only use them for the success of your campaign and not disclose nor sell them elsewhere.
								</p>
									<p class="term-privacy pad">
									<strong>Access via Third Party</strong>
								</p>
									<p class="term-privacy">
									TZilla.com may be accessed via links on other websites and third-party applications such as, but not limited to:
								</p>
								<ul>
									<li>Facebook</li>
									<li>Twitter</li>
									<li>Instagram</li>
									<li>Google Plus</li>
									<li>Email</li>
								</ul>
									<p class="term-privacy pad">
									<strong>Security and Accuracy</strong>
								</p>
									<p class="term-privacy">
									TZilla will take responsible steps to protect your personal information from misuse, loss, or any unauthorized modification.
								</p>
									<p class="term-privacy">
									You may access and request modification of your personal information at any time. TZilla also gives you the option to request the deletion of your account and personal information by contacting us as long as it does not create any conflict with any campaign and/or other TZilla users.
								
									However, with or without consent, TZilla may access and disclose your information should we be requested by law or any government body. We will do so in order to investigate frauds and/or validity of campaigns, clear conflicts with other users, and protect our rights, your rights, and the rights of other TZilla users.
								</p>
									<p class="term-privacy pad">
									<strong>Cookies</strong>
								</p>
									<p class="term-privacy">
									Whenever you visit Tzilla.net, TZilla.org, and TZilla.com, our web server may send “cookies” to your machine. Cookies are pieces of information which help us recognize your re-visit and coordinate your access to different pages on Tzilla.net, TZilla.org, and TZilla.com.
								</p>
									<p class="term-privacy">
									With most Internet Browsers, you can erase cookies from your machine, block them, or be advised before cookies are stored. To learn more, you may refer to your browser instructions and options. However, should you disable all cookies, you may not be able to take advantage of all the features of our site.
								</p>
									<p class="term-privacy">
									Links to other sites
								</p>
									<p class="term-privacy pad">
									TZilla may provide links to other websites on its own websites, Tzilla.net, TZilla.org, and TZilla.com. A link to another website is not an endorsement of the accuracy nor trustworthiness of that website nor its content.
								</p>
								</div>
							</div>
						
					</div>
				</div>
	  </div>
	
	
	</div>
  </div>
</div>



<!-- privacy-->
<div id="termsCondition" class="modal fade" role="dialog">
<div class="modal-dialog cantfind-dialog">

<!-- Modal content-->
<div class="modal-content confirm-modal-holder">
  <div class="confirm-header white gregular font-large">
		<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
  </div>
  <div class="addmorestyle-holder font-small gray-dark glight">
			<div class="container-fluid">
				<div class="row">
					
						<div class="privacypolicy-content">
							<h3 class="gregular font-large gray-darker privacy-title">Terms & Conditions</h3>
							
								<p class="term-privacy">
									1	<strong>User Agreement</strong>
								</p>
									<p class="term-privacy">
									This User
							Agreement (hereinafter “Agreement”) is by and between anyone that
							accesses www.tzilla.com or www.tzilla.net or www.tzilla.org
							(including any and/or all of its pages and links) (hereinafter
							collectively the “Site”) and TZilla. As used in this Agreement
							anyone that accesses the Site will be referred to as a “User”
							and/or “You” and/or “Your.” This Agreement will be applicable
							with respect to use of the Site and any Goods and/or Services
							provided by TZilla to the User.
								</p>
								<p class="term-privacy">
									IF YOU ARE
							UNDER 13 YEARS OF AGE, YOU ARE NOT PERMITTED TO USE THIS SITE.  IF
							YOU ARE UNDER 18 YEARS OF AGE BUT BETWEEN THE AGES OF 13 AND 17, YOU
							MAY USE THIS SITE WITH THE CONSENT, PERMISSION AND SUPERVISION OF
							YOUR PARENT OR LEGAL GUARDIAN SO LONG AS THAT PARENT OR LEGAL
							GUARDIAN IS AT LEAST 18 YEARS OF AGE, AGREES TO THIS AGREEMENT, AND
							AGREES TO BE LEGALLY RESPONSIBLE FOR YOUR USE OF THE SITE.
								</p>
								<p class="term-privacy">
									PLEASE READ
							THIS AGREEMENT VERY CAREFULLY BEFORE USING THE SITE.  THIS AGREEMENT
							APPLIES TO YOUR USE OF ALL OR ANY PART OF THIS SITE AND TO ANY EMAIL
							CORRESPONDENCE BETWEEN TZILLA AND YOU. BY USING THE SITE, YOU HEREBY
							ACKNOWLEDGE AND AGREE THAT YOU HAVE READ AND UNDERSTAND THIS
							AGREEMENT AND AGREE TO BE BOUND BY IT. YOU ALSO ACKNOWLEDGE AND AGREE
							THAT YOU ARE AT LEAST 18 YEARS OF AGE AND ARE CAPABLE OF ENTERING
							INTO A LEGALLY BINDING CONTACT.  IF YOU DO NOT AGREE TO ANY OF THE
							TERMS AND CONDITIONS OF THIS AGREEMENT AND/OR OUR PRIVACY POLICY, DO
							NOT USE THIS SITE.
								</p>
								<p class="term-privacy">
									<strong>1. 
									</strong><strong>Terminology in this Agreement</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									The following
							terms will have the meanings as explained in this Section 1.
								</p>
								<p class="term-privacy">
									a.  <em>Account</em>:
							a methodology to gain access to the Services via logging into the
							Site with a username and password.
								</p>
								<p class="term-privacy">
									b. 
									<em>Beneficiary</em>: the individual, company, entity or Charity who
							receives the funds from a particular Campaign.
								</p>
								<p class="term-privacy">
									c.  <em>Campaign</em>:
							a fundraising mechanism used to sell Goods and/or Services to provide
							funds to the Beneficiary.
								</p>
								<p class="term-privacy">
									d. <em>Charity</em>:
							a charitable entity that is considered tax exempt under the United
							States Internal Revenue Code Section 501(c)(3).
								</p>
								<p class="term-privacy">
									e.  <em>Content</em>:
							includes, but is not limited to, logos, graphics, photos, images,
							text and fonts.
								</p>
								<p class="term-privacy">
									f.  <em>Customer</em>:
							the individuals, companies, entities or Charities that purchase Goods
							from TZilla.
								</p>
								<p class="term-privacy">
									g.  <em>Goods</em>:
							the clothing apparel that is available for purchase on the Site.
								</p>
								<p class="term-privacy">
									h. 
									<em>Non-Personal Information</em>: Your Internet Service Provider
							(“ISP”), Your Internet Protocol (“IP”) Address, Your browser
							type, the URL of the previous website You visited, Your operating
							system, device information and location.
								</p>
								<p class="term-privacy">
									i. 
									<em>Organizer</em>: the individual, company, entity or Charity who sets
							up a Campaign and/or Storefront on the Site.
								</p>
								<p class="term-privacy">
									j.  <em>Personal
							Information
									</em>: Your name, mailing address, phone number, email
							address, credit card number, tax identification number and other
							financial and personal information.
								</p>
								<p class="term-privacy">
									k.  <em>Services</em>:
							the functionality and services that are offered on the Site to
							Registered Users including, but not limited to, the T-Shirt
							Generator.
								</p>
								<p class="term-privacy">
									l.  <em>Site</em>:
							means www.tzilla.com and/or www.tzilla.net and/or www.tzilla.org.
								</p>
								<p class="term-privacy">
									m. 
									<em>Storefront</em>: the dedicated on-line webpage(s) for each Campaign
							(and/or group of Campaigns) that acts as an e-commerce portal for the
							sale of Goods to the Customers that access the Site.
								</p>
								<p class="term-privacy">
									n.  <em>T-Shirt
							Generator
									</em>: a feature of the Site that is available to Users (both
							Visitors and Registered Users) to custom design or modify graphic
							images for use on clothing apparel using TZilla’s and/or the User’s
							Content for use in a Campaign and/or on a Storefront.
								</p>
								<p class="term-privacy">
									o.  <em>User</em>:
							the individuals, companies, entities or Charities that use and/or
							access this Site regardless of whether You are an unregistered
							Visitor or a Registered User.
								</p>
								<p class="term-privacy">
									<strong>2. 
									</strong><strong>General Description of the Site and the Goods and Services
							which TZilla Provides
									</strong><strong>. </strong>
								</p>
								<p class="term-privacy">
									TZilla provides the public with
							original graphic art designs that can be personalized with limited
							customizations. These personalized designs can simply be purchased by
							the consumer, or, if the consumer is an authorized Organizer for
							his/her group or organization, the design can be utilized and sold in
							the form of a fundraiser to support his/her group or organization.
							TZilla will provide various online tools to help him/her manage the
							Campaign, including a dedicated webpage that is housed in TZilla's
							servers, that his/her organization can use to allow consumers to
							conduct purchase transactions.  The final product sold by TZilla is
							in the form of a printed graphic on a garment.  TZilla will collect
							payments directly from consumers, handle fulfillment,
							distribution/shipping, and provide customer support.  At the end of
							the Campaign, TZilla will send a physical check to the Organizer (or
							its designated Beneficiary) for the difference between the price sold
							to his/her supporters (determined by the Organizer), and the cost of
							the TZilla finished product (disclosed to the Organizer on TZilla's
							webpage before he/she activates the Campaign).  Post Campaign, TZilla
							will continue to send the Organizer (or its designated Beneficiary)
							monthly payments for any additional transactions generated from their
							dedicated webpage.
								</p>
								<p class="term-privacy">
									<strong>3.  </strong><strong>Types of Users</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									a.  <em>Registered User</em>: Anyone
							that establishes an Account on the Site will be a Registered User. As
							part of the Account registration process, You will be required to
							enter a valid email address, a username and a password to access the
							Services. You are not permitted to share or transfer Your Account
							with any other persons or entities. You are solely responsible for
							any and all use of Your Account. Please notify us immediately if You
							become aware that Your Account is being used without Your permission.
							 If You register another person, group or entity, You hereby warrant
							that You are authorized to act on their or its behalf.  You also
							warrant and agree that all of the  information which You provide to
							us during the registration process will be true, correct, accurate
							and current and You agree to promptly update such information if
							anything should change. You may set up an Account either directly on
							the Site or via a third party sign-on service such as FaceBook or
							Twitter. As a Registered User, You can be an Organizer and/or a
							Customer.
								</p>
								<p class="term-privacy">
									b. <em>Visitor</em>: Anyone that does
							not have an Account but accesses the Site.  As a Visitor, You may
							browse the Site but You will not have full access until You become a
							Registered User.
								</p>
								<p class="term-privacy">
									<strong>4.  </strong><strong>Fees and Return
							Policies
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									a.  <em>TZilla Fees.</em> 
							Establishing an Account on the Site is free of charge. TZilla does
							however subtract its costs and charges a service fee as a portion of
							the total sales made during a Campaign and/or through a Storefront. 
							By using the Site, You agree to our costs and fees. TZilla also
							charges credit card or other third-party processing fees in addition
							to our fees and costs. Any changes to fees and costs are effective
							after we post notice of the changes on the Site. The new fee/cost
							amounts will be used for Campaigns and/or Storefronts launched after
							the notice is posted.
								</p>
								<p class="term-privacy">
									b.  <em>Returns</em>.
								</p>
								<p class="term-privacy">
									If the Goods are defective or if the
							Content on the Goods are materially different than the Content
							created for the Campaign and/or Storefront, TZilla will accept
							returns provided that such issues are brought to our attention in
							writing within fifteen (15) days of shipping.  If a return is
							warranted, TZilla will issue a Return Merchandise Authorization
							(RMA).  No returns may be made without a TZilla issued RMA.
								</p>
								<p class="term-privacy">
									<strong>5.  </strong><strong>Intellectual
							Property Rights and Policies
									</strong>.
								</p>
								<p class="term-privacy">
									a.  <em>Intellectual Property
							Matters
									</em>.
								</p>
								<p class="term-privacy">
									Unless otherwise stated, TZilla owns
							the copyright, trade marks and all other intellectual property rights
							for the Site and expressly reserves all rights in the same. You may
							print and download extracts from the Site for Your own noncommercial
							use and in respect of Your receipt of our Service, provided that You
							do not modify any of the TZilla Content and You do not remove any
							copyright or trade mark notification or other proprietary notices
							from such extracts.  Unless we state otherwise, all other
							reproduction or use of extracts of Content is strictly prohibited.
								</p>
								<p class="term-privacy">
									b.  <em>Ownership of this Site and
							its Content
									</em>.
								</p>
								<p class="term-privacy">
									TZilla is the owner of this Site,
							and as such, retains all right, title and interest in and to the
							Site, the Services, TZilla’s Content, all Storefronts and all of
							their related intellectual property rights. Please note that all
							software, code, methods, tools, functionality and the like used via
							the Site are owned by TZilla (and/or its vendors and partners) and
							may not be copied, reproduced, modified, posted, transmitted,
							republished, sold, or offered for sale in any way without TZilla’s
							prior written consent. The TZilla name and logo as well as look and
							feel of the Site are trademarks and trade dress of TZilla. Other
							trademarks and logos that appear on the Site are the property of
							their respective owners. Ownership of all such trademarks and trade
							dress shall belong to TZilla or those other trademark owners.
								</p>
								<p class="term-privacy">
									c.  <em>Ownership of Your Content</em>.
								</p>
								<p class="term-privacy">
									You may be able to post and use Your
							own logos, graphics, photos, images, text and fonts (collectively
							“User Content”) while using the Services of the Site. If You do
							so, You hereby grant TZilla and our Users a perpetual, non-exclusive,
							royalty-free, transferable, sub-licensable, worldwide license to use,
							publicly display, publicly perform, store, reproduce, modify, create
							derivative works, and distribute Your User Content on or in
							connection with the Site and the Goods and Services offered on the
							Site. You represent and warrant that Your User Content and our use of
							Your User Content will not infringe any third party’s intellectual
							property rights, industrial rights, proprietary rights, privacy
							rights, confidentiality rights, publicity rights or otherwise violate
							any laws, statutes, treaties or ordinances.
								</p>
							<p class="term-privacy">
									d.  <em>Intellectual Property
							Infringement
									</em>.
								</p>
								<p class="term-privacy">
									If You are an intellectual property
							rights owner and consider that any Content infringes Your
							intellectual property rights, You may notify us of such infringement
							by sending the following information to us: (i) identification of the
							work which You claim to be infringed; (ii) identification of the
							Content which You claim to infringe that work in order to allow us to
							locate the allegedly infringing material; (iii) Your complete contact
							details (including full name, postal address, telephone number, and
							email address); (iv)confirmation that You have not authorized the use
							of the Content in the manner complained of; (v) confirmation under
							penalty of perjury that the information You have provided to us is
							accurate and that You are the intellectual property owner or that You
							are authorized to act on behalf of the intellectual property property
							owner; and (vi) Your physical or electronic signature.  Please send
							details of such infringement and signed statement under penalty of
							perjury to TZilla: by email to: generalcounsel@tzilla.com or by post
							to: TZilla, 13860 Stowe Drive, Poway CA 92064 U.S.A., Attn: General
							Counsel.
								</p>
								<p class="term-privacy">
									<strong>6.  </strong><strong>Privacy Issues and
							Policies
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									In the course of accessing and/or
							using the Site, TZilla may obtain information about You or You may be
							required to provide certain Personal Information to TZilla. All uses
							of Your Personal Information will be treated in accordance with our
							Privacy Policy which is hereby incorporated into this Agreement by
							reference. If You use the Site, You are accepting our Privacy Policy,
							as may be amended and/or revised from time to time. If You do not
							agree to have Your information used in any of the ways described in
							the Privacy Policy You must discontinue use of this Site immediately.
								</p>
								<p class="term-privacy">
									<strong>7.  </strong><strong>Site Restictions;
							Charity; No Tax Advice; and Disputes Between Users
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									a.  <em>Restrictions</em>.  
							Organizers are not permitted to create a Campaign and/or Storefront
							to raise funds for any illegal and/or immoral purposes.  TZilla
							reserves the right, in its sole discretion, to take down any Campaign
							and/or Storefront that is in violation of this Agreement.  TZilla may
							also terminate any Account that violates this Agreement.
								</p>
								<p class="term-privacy">
									b.  <em>Charity</em>.  Each Organizer
							and/or Beneficiary of a Campaign and/or Storefront shall be
							responsible for complying with all laws relating to the raising of
							funds for charity. If TZilla is contacted by any state and/or federal
							agency overseeing charitable solicitation activity, TZilla may refer
							that agency to the respective Organizer and/or Beneficiary. TZilla
							will also cooperate with any investigation by an agency or law
							enforcement officials.  If any Organizer wishes to specify a
							charitable organization who will benefit from a Campaign, the
							Organizer must provide us with the U.S. Tax Identification Number for
							that charitable organization.
								</p>
								<p class="term-privacy">
									c.  <em>No Tax Advice from TZilla</em>.
							  Please note that TZilla does not provide any tax advice. It is the
							responsibility of each User to consult with its own tax advisor and
							comply with all tax laws. Such responsibility may include reporting
							and remitting the correct tax, if any, to the appropriate taxing
							authority for any monies that TZilla may pay to the Organizer and/or
							the Beneficiary in as much as the taxing authorities may classify
							funds raised via TZilla as taxable income.  As such, we may require
							You to provide a Tax Identification Number to us for any Organizer
							and/or Beneficiary of Your Campaign so that we may report taxable
							income to the relevant taxing authorities if required to do so.  It
							is also the responsibility of each Organizer and each Beneficiary to
							maintain proper records that may be required for tax purposes.
								</p>
								<p class="term-privacy">
									d.  <em>Disputes
							Between Users
									</em>.  You acknowledge and agree that TZilla is not
							responsible for resolving disputes between various Users.
							Nevertheless, if a dispute between Users is brought to our attention,
							we may (but are not obligated) to provide each User with the other
							User’s contact information to enable the Users to resolve their
							dispute amongst themselves.
								</p>
								<p class="term-privacy">
									<strong>8.  </strong><strong>Linking
							To This Site
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									You may create a
							link from Your own website to the Site provided that You obtain our
							prior written consent and provided that You only do so on the basis
							that You link to, and do not replicate, the home page of this Site
							and You DO NOT: (a) create a frame or any other browser or border
							environment around this Site; (b) in any way imply that TZilla
							endorses any properties, products or services other than its own; (c)
							misrepresent Your relationship with TZilla or present any other false
							information about us; (d) use any of TZilla’s Content or trade
							marks without our express written permission; (e) link from a website
							that is not owned by You except for social media sites, including but
							not limited to FaceBook and Twitter where You have a registered
							account; or (f) display any content on Your website that is
							distasteful, offensive or controversial, infringes any intellectual
							property rights or other rights of any other person or otherwise does
							not comply with all applicable laws and regulations. TZilla expressly
							reserves the right to revoke without prior notice any right we grant
							to You to link to our Site should You breach any of these terms and
							conditions.
								</p>
								<p class="term-privacy">
									<strong>9.  </strong><strong>Assignment</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									This Agreement
							including its terms and conditions and any rights and/or licenses
							granted hereby shall not be transferred and/or assigned by You under
							any circumstances.   TZilla may transfer or assign this Agreement
							without restrictions.
								</p>
								<p class="term-privacy">
									<strong>10.  </strong><strong>No
							Formal Business Obligations
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									This Agreement shall
							not constitute, create, give effect to, or otherwise imply a joint
							venture, pooling arrangement, partnership, or formal business
							organization of any kind between You and TZilla.
								</p>
								<p class="term-privacy">
									<strong>11.  </strong><strong>Governing
							Law
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									This Agreement will
							be governed by the laws of the State of California, U.S.A. You agree
							that any claim or cause of action related to this Agreement, the
							Site, and/or any Goods and Services provided by TZilla must be filed
							within one (1) year after such claim or cause of action arose or be
							forever barred. You further agree that any claim or cause of action,
							can only be brought in an individual capacity and specifically may
							not be brought as a class action or any other type of representative
							action or proceeding.
								</p>
								<p class="term-privacy">
									<strong>12.  </strong><strong>Venue</strong>.
								</p>
								<p class="term-privacy">
									You and TZilla agree
							that venue for any dispute arising out of and/or relating to this
							Agreement or any
								</p>
								<p class="term-privacy">
									Goods and Services
							provided by TZilla shall be in San Diego County, California, U.S.A.
							and hereby irrevocably submit to the exclusive jurisdiction of the
							state and federal courts located in San Diego County, California,
							U.S.A. except as otherwise agreed in the Arbitration section below.
								</p>
								<p class="term-privacy">
									<strong>13.  </strong><strong>Arbitration</strong>.
							YOU AND TZILLA AGREE THAT TZILLA MAY ELECT TO HAVE ANY DISPUTE
							RELATED TO THIS AGREEMENT, THE SITE AND/OR THE GOODS AND SERVICES
							PROVIDED BY TZILLA RESOLVED THROUGH BINDING ARBITRATION IN SAN DIEGO
							COUNTY, CALIFORNIA VIA AN ESTABLISHED ALTERNATIVE DISPUTE RESOLUTION
							PROVIDER SELECTED BY TZILLA. IF TZILLA MAKES THIS ELECTION, YOU AGREE
							TO MOVE OR TRANSFER ANY CASE OR PROCEEDING TO THE ALTERNATIVE DISPUTE
							RESOLUTION PROVIDER SELECTED BY TZILLA. ANY DECISION, JUDGMENT OR
							AWARD BY THE ARBITRATOR MAY BE ENTERED IN ANY COURT OF COMPETENT
							JURISDICTION.
								</p>
								<p class="term-privacy">
									<strong>14.  </strong><strong>Headings</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									All headings used
							herein are intended for reference purposes only and shall not affect
							the interpretations or validity of this Agreement.
								</p>
								<p class="term-privacy">
									<strong>15.  </strong><strong>Entire
							Agreement
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									This Agreement
							together with the Privacy Policy located on this Site constitutes the
							entire agreement and understanding between You and TZilla with
							respect to the subject matter of this Agreement. No waiver of any
							term or condition of this Agreement shall be deemed a further or
							continuing waiver of such term or condition, and TZilla’s failure
							to assert any right or provision under these Agreement shall not
							constitute a waiver of such right or provision. If any term,
							condition or provision of this Agreement is found by a court of
							competent jurisdiction to be invalid, You agree that the court should
							endeavor to give full effect to the intentions reflected in the
							invalid term, condition or provision, and the other terms, conditions
							and provisions of this Agreement shall remain in full force and
							effect.
								</p>
								<p class="term-privacy">
									<strong>16.  </strong><strong>Disclaimer
							&amp; Limitations of Liability
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									WHILST WE USE OUR
							REASONABLE ENDEAVORS TO ENSURE THAT OUR CONTENT IS ACCURATE AND
							UP-TO-DATE, WE DO NOT GIVE ANY WARRANTY OR GUARANTEE AS TO THE
							ACCURACY OR COMPLETENESS OF THE CONTENT. YOU ACKNOWLEDGE AND AGREE
							THAT THE CONTENT IS PROVIDED “AS IS” AND THAT YOU USE THE WEBSITE
							AND RELY ON THE CONTENT ENTIRELY AT YOUR OWN RISK.
								</p>
								<p class="term-privacy">
									Although we employ
							security measures to protect Your information from access by
							unauthorized persons and against unlawful processing, accidental
							loss, destruction and damage, You acknowledge that communications
							sent via the Internet cannot be guaranteed to be completely secure.
								</p>
								<p class="term-privacy">
									WE GIVE NO
							REPRESENTATION, WARRANTY OR GUARANTEE IN RESPECT OF THE SITE, THE
							CONTENT OR THE USER CONTENT OR ANY OF THE GOOD AND SERVICES FROM TIME
							TO TIME AVAILABLE IN CONNECTION WITH THE WEBSITE. WE DO NOT ENDORSE
							ANY USER CONTENT AND WE MAKE NO REPRESENTATION THAT THE INFORMATION
							SUPPLIED BY OUR SITE USERS IS VALID. WE EXPRESSLY DISCLAIM ANY AND
							ALL LIABILITY IN CONNECTION WITH ANY USER CONTENT, FROM TIME TO TIME
							AVAILABLE ON OUR SITE. WE RELY SOLELY UPON THE INTEGRITY OF OUR SITE
							USERS. WE DO NOT VERIFY, EDIT OR CONTROL ANY INFORMATION UPLOADED
							ONTO OUR SITE BY YOU. TO THE MAXIMUM EXTENT PERMITTED BY LAW, WE
							EXCLUDE:  ALL EXPRESS AND IMPLIED WARRANTIES, REPRESENTATIONS AND
							CONDITIONS WITH RESPECT TO THE SITE AND THE CONTENT; ALL LIABILITY
							FOR ANY COMMUNICATIONS THAT ARE LOST, INTERCEPTED, ALTERED OR
							OTHERWISE ACCESSED BY THIRD PARTIES; ALL LIABILITY FOR ANY DIRECT OR
							INDIRECT LOSSES, LOSS OF PROFITS OR OTHER CONSEQUENTIAL LOSS,
							DAMAGES, COSTS, EXPENSES OR LIABILITIES THAT YOU MAY SUFFER OR INCUR
							ARISING FROM YOUR USE OF THIS SITE AND/OR RELIANCE ON ANY CONTENT,
							INCLUDING (WITHOUT LIMITATION) ARISING OUT OF ANY COMMUNICATION SENT
							BY YOU TO US OR ANY COMMUNICATION SENT BY US TO YOU VIA THE INTERNET.
								</p>
								<p class="term-privacy">
									Without limiting the
							foregoing, under no circumstances shall we be held liable for any
							delay or failure in performance resulting directly or indirectly from
							acts of nature, forces or causes beyond our reasonable control,
							including, without limitation, Internet failures, computer equipment
							failures, telecommunication equipment failures, other equipment
							failures, electrical power failures, strikes, labor disputes, riots,
							insurrections, civil disturbances, shortages of labor or materials,
							fires, floods, storms, explosions, acts of God, war, governmental
							actions, orders of domestic or foreign courts or tribunals,
							non-performance of third parties, or loss of or fluctuations in heat,
							light, or air conditioning.
								</p>
								<p class="term-privacy">
									<strong>17.  </strong><strong>General
							Release
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									If You are a
							resident of California, You hereby agree to waive the applicability
							of the California Civil Code section 1542 for unknown claims which
							states:
								</p>
								<p class="term-privacy">
							“
									<strong>A general release does not extend to claims which the creditor
							does not know or suspect to exist in his or her favor at the time of
							executing the release, which if known by him or her must have
							materially affected his or her settlement with the debtor." 
									</strong>
								</p>
								<p class="term-privacy">
									All Users, including
							those located in California, hereby waive this section of the
							California Civil Code and any similar provision in law, regulation or
							code that has the same effect or intent as the foregoing general
							release language.
								</p>
								<p class="term-privacy">
									<strong>18.  </strong><strong>Indemnity</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									You agree to defend
							and indemnify TZilla from and against any and all claims, actions,
							proceedings, damages, losses, liabilities and expenses (including
							legal fees) suffered by us arising out of or in connection with any
							one of the following: (a) Your use of the Site; (b) a breach by You
							of any of the terms and conditions of this Agreement; or (c) any
							infringement by You of the intellectual property rights or privacy
							rights of a third party or a breach by You of a duty of
							confidentiality to a third party. This defense and indemnification
							obligation will survive this Agreement and Your use of the Site
							and/or Service.
								</p>
								<p class="term-privacy">
									<strong>19.  </strong><strong>Written
							Communications
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									When using our
							Website, You accept that communication with us will be mainly
							electronic. TZilla will contact You by e-mail or provide You with
							information by posting notices on our Site. For contractual purposes,
							You agree to this electronic means of communication and You
							acknowledge that all contracts, notices, information and other
							communications that we provide to You electronically comply with any
							legal requirement that such communications be in writing.
								</p>
								<p class="term-privacy">
									<strong>20.  </strong><strong>Notices
							&amp; Complaints
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
							All notices and complaints given by You to TZilla (including any
							complaints that You may have) should be sent to TZilla:   by e-mail
							to: support@tzilla.com or by post to: TZilla, 13860 Stowe Drive,
							Poway, CA 92064 U.S.A. We may give notice to You via postings on our
							Site or via an email address supplied by You to us. Notice will be
							deemed received and properly served immediately when posted on our
							Site, 48 hours after an e-mail is sent, or two days after the date of
							posting of any letter. In proving the service of any notice, it will
							be sufficient to prove, in the case of a letter, that such letter was
							properly addressed, stamped and placed in the post and, in the case
							of an e-mail, that such e-mail was sent to the specified email
							address of the addressee.
								</p>
								<p class="term-privacy">
									<strong>21.  </strong><strong>Changes
							&amp; Modifications To This Site And The Services
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									TZilla may make
							changes to the Site and/or the Services from time to time with or
							without notice. If You object to any such changes, Your sole recourse
							will be to stop using the Site and Services. Continued use of the
							Site and/or Services will indicate Your acknowledgment and agreement
							to such changes.  TZilla also reserves the right to discontinue the
							Site and/or Services in whole or in part at any time with or without
							notice
								</p>
								<p class="term-privacy">
									<strong>22.  </strong><strong>Changes
							&amp; Modifications To This Agreement
									</strong><strong>.</strong>
								</p>
								<p class="term-privacy">
									TZilla may make
							changes to this Agreement from time to time.  Any such changes will
							become effective when posted on the Site. If You object to any such
							changes, Your sole recourse will be to stop using the Site and the
							Services offered on the Site. Continued use of the Site and/or the
							Services will indicate Your acknowledgment of such changes and Your
							agreement to be bound by the modified Agreement.
								</p>
								<p class="term-privacy">
									<strong>23.  </strong><strong>Date
							of Last Agreement Update
									</strong><strong>.</strong><em>August 1, 2014 </em>at
							Poway, California, U.S.A.
								</p>
		
						</div>
					
				</div>
			</div>
	  </div>
	
	
	</div>
  </div>
</div>

<!-- refund-->
<div id="faq" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder font-small gray-dark glight">
				<div class="container-fluid">
					<div class="row">
						
							<div class="privacypolicy-content">
								<h3 class="gregular font-large gray-darker privacy-title">Frequently Asked Questions</h3>
								<div class="term-privacy">
								<p class="term-privacy">
									TZilla acknowledges and highly values your privacy. This Privacy Policy covers the personal information that TZilla collects, uses, or discloses and sets out the way in which TZilla manages and protects your personal information when you visit our websites, Tzilla.net, TZilla.org, and TZilla.com; set up a campaign; donate; or place an order via our online storefront.
								</p>
									<p class="term-privacy pad">
									This policy is effective upon your use of this site. However, whenever necessary, TZilla may revise this policy from time to time.
								</p>
									<p class="term-privacy pad">
									<strong>How we collect your information</strong>
								</p>
									<p class="term-privacy">
									TZilla may knowingly collect personal information from you, which may include:
								</p>
									<p class="term-privacy">
									Your name, address, birthdate (to verify your age), email address, and phone number
								</p>
									<p class="term-privacy">
									Your credit card details and billing information
								</p>
									<p class="term-privacy pad">
									Like most websites, each time you visit, Tzilla.net, TZilla.org, and TZilla.com will automatically collect information about your computer such as your type of browser, date and time of visit, operating system, and your server’s IP address.
								</p>
									<p class="term-privacy">
									TZilla will take responsible steps to advise you if TZilla finds it necessary to collect your information from someone else or any other party.
								</p>
									<p class="term-privacy pad">
									<strong>How we use your information</strong>
								</p>
									<p class="term-privacy">
									TZilla may use the personal information you have knowingly submitted to store and process information in order to provide you with its goods and services. These may include, but are not limited to:
								</p>
								<ul>
									<li>To set up your personal TZilla account</li>
									<li>To set up your campaign</li>
									<li>To process and fulfil your purchase/s</li>
									<li>To determine the number of visitors to our websites</li>
									<li>To conduct reviews of our services and websites</li>
									<li>To award prizes and discounts</li>
									<li>To respond to any of your specific requests</li>
									<li>To update you with the changes in our policies, services, and websites</li>
									<li>To conduct market researches to improve TZilla’s websites and services</li>
									<li>To send you offers and information on products and services which may be of interest to you</li>
									<li>To verify information should any conflict arise with other users</li>
								</ul>
									<p class="term-privacy">
									TZilla may request you to submit the email addresses of your friends or relatives in order to promote our services and/or the campaign you have set up on TZilla.com. Rest assured, TZilla will only use them for the success of your campaign and not disclose nor sell them elsewhere.
								</p>
									<p class="term-privacy pad">
									<strong>Access via Third Party</strong>
								</p>
									<p class="term-privacy">
									TZilla.com may be accessed via links on other websites and third-party applications such as, but not limited to:
								</p>
								<ul>
									<li>Facebook</li>
									<li>Twitter</li>
									<li>Instagram</li>
									<li>Google Plus</li>
									<li>Email</li>
								</ul>
									<p class="term-privacy pad">
									<strong>Security and Accuracy</strong>
								</p>
									<p class="term-privacy">
									TZilla will take responsible steps to protect your personal information from misuse, loss, or any unauthorized modification.
								</p>
									<p class="term-privacy">
									You may access and request modification of your personal information at any time. TZilla also gives you the option to request the deletion of your account and personal information by contacting us as long as it does not create any conflict with any campaign and/or other TZilla users.
								
									However, with or without consent, TZilla may access and disclose your information should we be requested by law or any government body. We will do so in order to investigate frauds and/or validity of campaigns, clear conflicts with other users, and protect our rights, your rights, and the rights of other TZilla users.
								</p>
									<p class="term-privacy pad">
									<strong>Cookies</strong>
								</p>
									<p class="term-privacy">
									Whenever you visit Tzilla.net, TZilla.org, and TZilla.com, our web server may send “cookies” to your machine. Cookies are pieces of information which help us recognize your re-visit and coordinate your access to different pages on Tzilla.net, TZilla.org, and TZilla.com.
								</p>
									<p class="term-privacy">
									With most Internet Browsers, you can erase cookies from your machine, block them, or be advised before cookies are stored. To learn more, you may refer to your browser instructions and options. However, should you disable all cookies, you may not be able to take advantage of all the features of our site.
								</p>
									<p class="term-privacy">
									Links to other sites
								</p>
									<p class="term-privacy pad">
									TZilla may provide links to other websites on its own websites, Tzilla.net, TZilla.org, and TZilla.com. A link to another website is not an endorsement of the accuracy nor trustworthiness of that website nor its content.
								</p>
								</div>
							</div>
						
					</div>
				</div>
	  </div>
	
	
	</div>
  </div>
</div>

<!-- For Zheng-->
<div id="alertmodal" class="modal fade" role="dialog">
      <div class="modal-dialog cantfind-dialog">
        <!-- Modal content-->
        <div class="modal-content confirm-modal-holder modal-notification-width">
          <div class="confirm-header white gregular font-medium">
                <span type="button" class="close icon-circled-x pull-right font-large verify-close" data-dismiss="modal"></span>
          </div>
          <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 text-center">                     
                        <div class="warning-label2 gregular font-regular text-center text-confirm"></div>
                    </div>
                </div>
          </div>
        </div>
      </div>
</div>

<!-- For Zheng-->
<div id="verifypop" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder modal-notification-width">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large verify-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">						
						<div class="warning-label2 gregular font-regular text-center for-verification">Please verify your account by clicking the activation link that has been send to your email or <u class="pointer resend-verification">resend</u> a verification.</div>
						
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>

<script src='https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit' async defer></script>
</html>
