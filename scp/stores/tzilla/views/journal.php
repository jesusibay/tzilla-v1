<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/journal.css');?>">
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container-fluid journal-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h1 class="white gregular font-xlarge">TZilla Journal</h1>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid journal-view-bg">
	<div class="container">
		<div class="row journal-body-marg">
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
				<div class="journal-pic"></div>
				<div class="journal-full-details">
					<h1 class="journal-full-title gregular blackz">My Daily Reading List</h1>
					<div class="journal-posted gregular font-xxsmall gray-dark">Posted on 8 March 2015 by Mike Ross</div>
					<div class="journal-view gregular font-xxsmall gray-dark">20 views</div>
					<div class="journal-comm gregular font-xxsmall gray-dark">12 comments</div>
					<div class="journal-share gregular font-xxsmall gray-dark">50 shares</div>
					<div class="journal-adds2 gregular font-xxsmall gray-dark">life style, friends, wonderful</div>
					<p class="journal-p gregular font-regular gray-dark">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt labore dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim laborum. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque. Lorem ipsum dolor sit amet.</br></br>

						Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. </br></br>

						Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. 
					</p>
						<ul class="journal-ul">
							<li>Consectetur adipiscing elit vtae elit libero.</li>
							<li>Nullam id dolor id eget lacinia odio posuere erat a ante.</li>
							<li>Integer posuere erat dapibus posuere velit.</li>
						</ul>
					<p class="gregular font-regular gray-dark">
						Nulla sed mi leo, sit amet molestie nulla. Phasellus lobortis blandit ipsum, at adipiscing eros porta quis. Phasellus in nisi ipsum, quis dapibus magna. Phasellus odio dolor, pretium sit amet aliquam a, gravida eget dui. Pellentesque eu ipsum et quam faucibus scelerisque vitae ut ligula. Ut luctus fermentum commodo. Mauris eget justo turpis, eget fringilla mi. Duis lobortis cursus mi vel tristique. Maecenas eu lorem hendrerit neque dapibus cursus id sit amet nisi. Proin rhoncus semper sem nec aliquet. Aenean lacinia bibendum nulla sed consectetur.
					</p>
				</div>
				<div class="journal-share-holder">
					<div class="journal-share-inner">
						<span class="icon-f"></span>
						<span class="icon-t"></span>
						<span class="icon-g"></span>
						<span class="icon-e"></span>
					</div>
				</div>
				<div class="journal-written-holder">
					<div class="journal-written-profile">
						<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/sample-profile.jpg');?>" alt="" />
					</div>
					<div class="journal-written-profile-detail">
						<div class="journal-written-label gregular blackz font-medium">Written by Mike Ross</div>
						<p class="journal-written-p gregular gray-dark font-regular">Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus vari laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. </p>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="journal-alsolike gregular blackz font-large">You may also like</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="journal-otherblog-holder">
							<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/sample-blog-1.jpg');?>" alt="" />
							<div class="journal-otherblog-label-holder">
								<div class="journal-otherblog-label gregular blackz font-medium">Wonderful time with Friends</div>
								<div class="gregular font-xxsmall gray-dark">Posted on 10th March 2015</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="journal-otherblog-holder">
							<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/sample-blog-2.jpg');?>" alt="" />
							<div class="journal-otherblog-label-holder">
								<div class="journal-otherblog-label gregular blackz font-medium">Wonderful time with Friends</div>
								<div class="gregular font-xxsmall gray-dark">Posted on 10th March 2015</div>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="journal-alsolike gregular blackz font-large">3 Thoughts on “Single Post”</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="journal-comment-holder">
							<span class="gregular font-xxsmall green">Kate Winless</span><span class="journal-postdate gregular font-xxsmall gray-dark">14th Feb 2015 at 9.00 am</span>
							<p class="journal-comment-p glight gray-dark font-regular">
								Phasellus odio dolor, pretium sit amet aliquam a, gravida eget dui. Pellentesque ipsum sque vitae ut ligula. Ut luctus fermentum commodo. Mauris eget justo turpis, eget fringilla. Duis lobortis cursus mi vel tristique.
							</p>
							<div class="text-right">
								<span class="journal-a gregular font-xsmall green text-uppercase">Reply</span>
							</div>
						</div>
						<div class="journal-reply-holder ">
							<span class="gregular font-xxsmall green">Tom</span><span class="journal-postdate gregular font-xxsmall gray-dark">14th Feb 2015 at 9.00 am</span>
							<p class="journal-comment-p glight gray-dark font-regular">
								Phasellus odio dolor, pretium sit amet aliquam a, gravida eget dui. Pellentesque ipsum sque vitae ut ligula. Ut luctus fermentum commodo. Mauris eget justo turpis, eget fringilla. Duis lobortis cursus mi vel tristique.
							</p>
							<div class="text-right">
								<span class="journal-a gregular font-xsmall green text-uppercase">Reply</span>
							</div>
						</div>
						<div class="journal-comment-holder">
							<span class="gregular font-xxsmall green">Katy</span><span class="journal-postdate gregular font-xxsmall gray-dark">14th Feb 2015 at 9.00 am</span>
							<p class="journal-comment-p glight gray-dark font-regular">
								Phasellus odio dolor, pretium sit amet aliquam a, gravida eget dui. Pellentesque ipsum sque vitae ut ligula. Ut luctus fermentum commodo. Mauris eget justo turpis, eget fringilla. Duis lobortis cursus mi vel tristique.
							</p>
							<div class="text-right">
								<span class="journal-a gregular font-xsmall green text-uppercase">Reply</span>
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<form id="validatecomment" role="form">
					<div class="journal-label-holder">
						<div class="journal-alsolike gregular blackz font-large">Leave a Comment</div>
						<div class="gregular font-small gray-dark">Your email address will not be published. Required fields are marked</div>
						<div class="clearfix"></div>
					</div>
						<div class="row journal-input-holder">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 name-pad">
								<input type="text" name="name" class="form-control journal-comment-text journal-name" placeholder="Name"/>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<input type="email" name="email" class="form-control journal-comment-text" placeholder="Email"/>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<textarea name="comment" class="form-control" rows="10" placeholder="Type your comment here."></textarea>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="row journal-trigger">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="g-recaptcha captcha-holder" data-sitekey="6LcNAQ0UAAAAAGO6y86wcOukg9QA18t1g_W7myDZ"></div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
								<button class="journal-comment-btn green-btn white gsemibold font-small">Submit</button>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<div class="journal-right-cont">
					<div class="journal-label-header gregular white font-medium">Categories</div>
					<ul class="journal-cat-group gregular font-xsmall gray-darker">
						<li>Featured <div class="bg1 white font-xxsmall">10</div></li>
						<li class="active">Lifestyle <div class="bg2 white font-xxsmall">9</div></li>
						<li>Events <div class="bg3 white font-xxsmall">6</div></li>
						<li>Campaign News <div class="bg4 white font-xxsmall">12</div></li>
						<li>Learn <div class="bg5 white font-xxsmall">12</div></li>
					</ul>
				</div>
				<div class="journal-right-cont">
					<div class="journal-label-header gregular white font-medium">Latest Posts</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="journal-latest-post">
						<div class="journal-latest-left"></div>
						<div class="journal-latest-right">
							<div class="journal-latest-title gregular font-xsmall gray-darker">Wonderful time with my Friends in Hanoi</div>
							<div class="gregular font-xxsmall gray">9th March 2015</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				
				
				<div class="journal-banner-cont">
					<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/sample-blog-1.jpg');?>" alt="" />
					<div class="journal-banner-text white gregular font-xsmall text-uppercase">Banner Images</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
	
	$(document).on("click", ".journal-a", function(){
		var target = $(".journal-comment-text").parent().parent().parent();
		$(".journal-alsolike").html("Reply");
		 $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
		$(".journal-name").focus();
	});
	
	
	
			$("#validatecomment").validate({
			debug: true,
			errorClass: 'error',
			validClass: 'success',
			errorElement: 'label',
			
			rules: {
			name: {
				required:true,
			},
			email: {
				required: true,
				email: true
            },
			comment: {
				required:true
			}
			
		  },
			messages: {
			name: {
			   required: "Please specify your name",
			},
			email: {
			  required: "We need your email address to contact you",
			  email: "Your email address must be in the format of name@domain.com"
			},
			comment: {
				required: "This field is required.",
			}
		  }, 
		  submitHandler: function(form) {
			
			alert("put your action here");
		  }
		  
	
		
	});
	
	//custom validation rule
	$.validator.methods.email = function( value, element ) {
	return this.optional( element ) || /[a-z]+@[a-z]+\.[a-z]+/.test( value );
	}
		
	});
</script>