<?php $this->load->view_store('account-header');  ?> 
<?php foreach ($get_order_details as $key => $details) { } ?>

    <div class="dash-title">
        <div class="row form-group">
            <div class="col-md-10">
                <h3 class="title">Sales Order Details</h3>
            </div>
            <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12">
                <a href="<?php echo base_url('account-orders'); ?>"><button class="orange-btn cartrev-update-btn white gsemibold font-small margin-btn2">Go Back</button></a>
            </div>
        </div>
    </div>
    <section class="section">
        <div class="row sameheight-container">
            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12">
                <div class="card card-block sameheight-item ref-id">
                    
                        <span class="gsemibold font-small">Ref # :</span>
                        <span class="gsemibold font-small"> <?php echo $details['order_number']; ?></span>
                   
                </div>
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Date Purchased:</span>
                            <span class="blackz gregular font-small"><?php echo date("M d, Y", strtotime($details['order_date'])); ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Status:</span>
                            <span class="blackz gregular font-small"><?php echo $details['order_status']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Shipping Method:</span>
                            <span class="blackz gregular font-small"><?php echo  str_replace("_"," ",$details['shipping_method']); ?></span>
                        </div>
                    </div>
                   
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12">
                <div class="card card-block sameheight-item ref-id">
                    
                        <span class="gsemibold font-small">Shipping Details</span>
                       <?php if(isset($shipping)) if(!empty($shipping['address_2'])): $add2 = $shipping['address_2'].' '; else: $add2 = ''; endif;  ?>
                   
                </div>
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                       <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Name:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['firstname'].' '.$shipping['lastname']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Email:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['cust_email']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Phone:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['contact_no']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Address:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['address_1'].' '.$add2; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">State:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['state']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">City:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['city']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Zip:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['zip']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Country:</span>
                            <span class="blackz gregular font-small">United States</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12">
                <div class="card card-block sameheight-item ref-id">
                    
                        <span class="gsemibold font-small">Billing Details</span>
                       <?php if(isset($billing)) if(!empty($billing['address_2'])): $billing_add2 = $billing['address_2'].' '; else: $billing_add2 = ''; endif;  ?>
                   
                </div>
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                       <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Name:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['firstname'].' '.$billing['lastname']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Email:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['cust_email']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Phone:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['contact_no']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Address:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['address_1'].' '.$billing_add2; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">State:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['state']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">City:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['city']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Zip:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['zip']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Country:</span>
                            <span class="blackz gregular font-small">United States</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-title-block">
                        </div>
                        <section class="example">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="gregular font-regular blackz col-md-2">Design Preview</th>
                                            <th class="gregular font-regular blackz">Description</th>
                                            <th class="gregular font-regular blackz">Specification</th>
                                            <th class="gregular font-regular blackz">Quantity</th>
                                            <th class="gregular font-regular blackz">Price</th>
                                            <th class="gregular font-regular blackz">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        foreach ($get_order_items as $key => $item) {
                                            $shirt_color_code = $this->Order_model->get_order_details_item_color( $item['shirt_color_code'] );
                                            $image = unserialize( $item['images'] );
                                            $size = explode("-", $item['matrix_code']);
                                            $order_item_size = $this->Order_model->get_shirt_size_name( $size[3] );
                                            $shirt = $this->Order_model->get_order_shirt_styles_details( $item['shirt_style_code'] );
                                            $image_url = "";
                                            $img_ash = base_url("public/".STORE).'/images/ash.jpg';
                                            $img_ahe = base_url("public/".STORE).'/images/atheltic-heather.jpg';
                                            $img_dh = base_url("public/".STORE).'/images/denim-heather.jpg';
                                            $img_ch = base_url("public/".STORE).'/images/charcoal-heather.jpg';
                                            if( $shirt_color_code['color_code'] == 'ASH' ){
                                                $image_url = 'url('.$img_ash.')';
                                            }else if ($shirt_color_code['color_code'] == 'AHE') {
                                                 $image_url = 'url('.$img_ahe.')';
                                            }elseif ( $shirt_color_code['color_code'] == 'DHE' ) {
                                                $image_url = 'url('.$img_dh.')';
                                            }elseif ( $shirt_color_code['color_code'] == 'CHE' ) {
                                                $image_url = 'url('.$img_ch.')';
                                            }
                                            ?>
                                        <tr>
                                            <td>
                                                <div class="img-holder" style="background: #<?php echo $shirt_color_code['hex_value'].' '.$image_url; ?>;">
                                                    <?php
                                                        $order_item_image = str_replace("/200/200", "", $image['temp_image']);                                                         
                                                        $template = $this->Order_model->get_order_shirt_template( $image['template_id'] );  
                                                        $type = $this->Campaigns_model->get_by_campaign_design_type( $template['template_type_id'] );  
                                                    ?>
                                                    <img class="img-responsive img-center" src="<?php if(isset($order_item_image)) echo $order_item_image.'/200/200'; ?>" alt="">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Design Name:</span>
                                                        <span class="blackz gregular font-small"><?php if(isset($template)) echo $template['display_name'] ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">SKU:</span>
                                                        <span class="blackz gregular font-small"><?php echo $item['matrix_code']; ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group" style="display:none;">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Type:</span>
                                                        <span class="blackz gregular font-small"><?php echo $type['name']; ?></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Style:</span>
                                                        <span class="blackz gregular font-small"><?php echo $item['group_name']; ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Style Type:</span>
                                                        <span class="blackz gregular font-small"><?php echo $item['garment_type']; ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Size:</span>
                                                        <span class="blackz gregular font-small"><?php if(isset($order_item_size)) echo $order_item_size['shirt_size_name']; ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Color:</span>
                                                        <span class="spec-color" style="background: #<?php echo $shirt_color_code['hex_value'].' '.$image_url; ?>; <?php if( $shirt_color_code['color_name'] == "WHITE"){ echo 'border: 1px solid #91999D;';}?>"></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?php if(isset($get_order_items)) echo $item['quantity']; ?></td>
                                            <td>$<?php if(isset($get_order_items)) echo number_format($item['sale_price'],2); ?></td>
                                            <td>$<?php if(isset($get_order_items)) $totalprice = $item['quantity'] * $item['sale_price']; echo number_format($totalprice,2); ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- total cont bot -->
                                <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 pull-right"> 
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Subtotal:</span>
                                            <span class="blackz gregular font-small">$<?php echo $details['subtotal']; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Tax:</span>
                                            <span class="blackz gregular font-small">$<?php echo (!empty($details['tax']))? number_format($details['tax'],2) : '0.00'; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Shipping Fee:</span>
                                            <span class="blackz gregular font-small">$<?php echo (!empty($details['shipping_fee']))? number_format($details['shipping_fee'],2) : '0.00'; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Discount:</span>
                                            <span class="blackz gregular font-small">$<?php echo (!empty($details['discount']))? number_format($details['discount'],2) : '0.00'; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 borders-design-styles"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Grand Total:</span>
                                            <span class="blackz gregular font-small">$<?php echo $details['total']; ?></span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>


<script type="text/javascript">
$(document).ready(function(){
    $("#storetitle").text('TZilla.com - Account Order Details');
});
</script>