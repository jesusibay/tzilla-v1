<?php $this->load->view_store('inner-header');  ?>

<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/visitor-customize.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/productsizechart.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/slick.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/slick-theme.css');?>">
<script src="<?php echo base_url('public/'.STORE.'/js/slick.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/colpick.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/customize.js').'?df='.rand(1, 99999);?>"></script>
<script type="text/javascript">
	setMainUrl( '<?php echo base_url(); ?>' );
	setMode( '<?php echo $mode; ?>' );
	setSchoolId( '<?php echo $school->school_id; ?>' );
	setSchoolName( '<?php echo $school->school_name; ?>' );
	setAssetUrl( '<?php echo base_url("public/".STORE); ?>' );
	setPreloadDesigns( '<?php echo addslashes (json_encode( $designs) ); ?>' );
	setCartItems( '<?php echo json_encode( $cart_items ); ?>' );
	setGoback( '<?php echo $go_back; ?>' );
	setCustomer( '<?php echo $customer_id; ?>' );
	setProductSettings( '<?php echo json_encode( $product_settings ); ?>' );
	setCampaign( '<?php echo $campaign_id; ?>' );
	setDefaultArtwork( '<?php echo $template_id; ?>' );
</script>
<div class="slider-overlay"></div>
<div class="container-fluid tgen-visitor-customize">
	<div id="wrap">
		<span class="vert-prev"></span>
		<span class="vert-next"></span>
		<div class="custom-inner-hold">
			<div class="custom-slider">
				<!-- append elements here -->
			</div>
			<div class="add-more-slide"></div>
		</div>
	</div>
					
	<div class="container tgen-content-holder">
		<div class="row">
			<div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
				<div class="tgen-right-holder">
					<div class="tgen-lefticon-holder">
						<div class="tgen-icon-holder" data-toggle="modal" data-target="#zoomer" data-dismiss="modal"><span class="tgen-zoom ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">Zoom</div></div>
						<!-- <div class="tgen-icon-holder size-specs" data-toggle="modal" data-target="#sizechart"><span class="tgen-spec ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">Size</br>Chart</div></div> -->
						<div class="tgen-icon-holder size-chart"><span class="tgen-spec ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">Size</br>Chart</div></div>
						<div class="tgen-icon-holder"><span class="tgen-share ease-effect"></span><div class="tgen-icon-label gsemibold font-xsmall gray">Share</div>
							<div class="social-share">
								<span id="fb_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="icon-facebook"></span>
								<span id="twitter_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="icon-twitter"></span>
								<span id="gplus_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="icon-gplus"></span>
								<span id="email_share_page" data-url="<?php echo base_url(); ?>" data-text="Design Something Great!" data-via="tzilladotcom" class="icon-email"></span>	
							</div>
						</div>
					</div>
					<div class="tgen-righticon-holder super-custom-btn">
						<div class="tgen-icon-holder2"><span class="tgen-sc ease-effect"></span><div class="tgen-icon-label2 gsemibold font-xsmall gray">Super Customize</div></div>
						
					</div>
					<div class="tgen-cart-btn"></div>
					
					<div class="tgen-shirt-holder">
							<div class="tgen-loader-overlay"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt="" /></div>
								<img class="img-responsive img-center shirt-col" <?php if(isset($defaultShirt)) echo 'data-id="'.$defaultShirt['id'].'" src="'.base_url( $defaultShirt['front_image_link'] ).'" alt=""'; ?>   />
								<div class="print-canvas">
									<div class="art-holder">

										<!--<canvas id="pallete" width="500" height="500"></canvas>-->
										<img class="img-center design-generated-image" src="" alt="" data-id="a" />										

									</div>
								</div>
					</div>
				</div>
				<div class="tgen-right-holder-footer variation-container">
					<div class="design-variation-label gregular font-regular blackz text-center">Design Variations</div>
					<div class="tgen-variation-slider">
							<div class="owl-carousel">
								<div class="design-variation-holder selected-variation"><img class="img-responsive img-center" src="" alt="" /></div>
								<div class="design-variation-holder"><img class="img-responsive img-center" src="" alt="" /></div>
							</div>
							<span class="variation-arrow design-variation-prev"></span>
							<span class="variation-arrow design-variation-next"></span>
					</div>
				</div>
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 super-custom-container">
				<div class="close-sc-slider-cont"><span class="close-sc-slider icon-circled-x"></span></div>
				<div class="super-custom-holder">
					<img class="img-responsive img-center pull-left" src="<?php echo base_url('public/'.STORE.'/images/super-customize-logo.png');?>" alt="" />
					<span class="inf pull-left" data-toggle="tooltip" data-placement="bottom" title="" data-hex="ff0000" data-original-title="Play around and some have fun personalizing your own design."></span>
					<div class="clearfix"></div>
				</div>
			<div class="super-custom-container-scroll">	
				<div class="tgen-tab-holder"> 
					<ul class="nav nav-tabs">
					  <li class="tab-panel active"><a data-toggle="tab" href="#styles" class="gregular font-medium text-center tab-styles">Products</a></li>
					  <li class="tab-panel customizable-fields"><a data-toggle="tab" href="#custom" class="gregular font-medium text-center tab-customize">Personalize</a></li>
					  <li class="tab-panel pattern-selection"><a data-toggle="tab" href="#patterns" class="gregular font-medium text-center tab-pattern">Patterns</a></li>
					</ul>

					<div class="tab-content">
					<!-- 1st tab -->
					  <div id="styles" class="tab-pane fade in active">
					    <div class="style-holder-outer">
						<div class="style-label gregular font-small gray-darker style-name">Classic Crew T-Shirt</div>
						<div class="clearfix"></div>
						<div class="style-holder-inner">
							<div class="addstyle-holder-item"></div>
							<div class="clearfix"></div>
						</div>
						</div>
					  </div>
					  <!-- 2nd tab -->
					  <div id="custom" class="tab-pane fade">
							<div class="row">
							<div class="col-lg-12">
							<div class="custom-holder">
								<div class="custom-left-holder design-color-selection">
								<div class="form-group">
									<label class="gregular font-xsmall gray-darker custom-label" for="">Color 1</label>
									<div class="custom-select form-control primarycol">
										<span class="pull-left custom-circle"></span>
										<i class="icon-caret-down pull-right custom-select-arrow"></i>
										<div class="color-primary" style="">
											<!--  -->
										</div>
									</div>
								</div>
								</div>
								
								<div class="custom-right-holder design-color-selection">
								<div class="form-group">
									<label class="gregular font-xsmall gray-darker custom-label" for="">Color 2</label>
									<div class="custom-select form-control secondarycol">
										<span class="pull-left custom-circle"></span>
										<i class="icon-caret-down pull-right custom-select-arrow"></i>
										<div class="color-secondary" style="">
											<!--  -->
										</div>
									</div>
								</div>
								</div>
								<div class="clearfix"></div>
								<div class="custom-field-options">
									<!-- append custom fields -->
									<div class="clearfix"></div>
									<div class="decade-holder">
										<div class="form-group">
										  <label class="gregular font-xsmall gray-darker custom-label" for="">Decade</label>
										  <input type="text" class="form-control custom-input gitalic font-xsmall gray-dark" id="" Placeholder="19">
										</div>
									</div>
									
									<div class="year-holder">
										<div class="form-group">
										  <label class="gregular font-xsmall gray-darker custom-label" for="">Year</label>
										  <input type="text" class="form-control custom-input gitalic font-xsmall gray-dark" id="" Placeholder="71">
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
									<div class="distress-holder">
										<div class="form-group">
										  <label class="gregular font-xsmall gray-darker custom-label" for="">Distress</label>
										  <select class="form-control custom-input gitalic font-xsmall gray-dark" id="distress-selection">
											<option value="0">No</option>
											<option value="1">Yes</option>
										  </select>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="addon-selection">
										<div class="form-group">
										  <label class="gregular font-xsmall gray-darker custom-label" for="">Icons</label>
											<div class="icons-text-holder custom-input gray-dark font-xsmall gitalic form-control">
												<span class="icon-title span-23 gitalic font-xsmall gray-dark">Select Icon</span>	
												<i class="icon-caret-down pull-right custom-select-arrow"></i>
												<div class="icon-selector-holder">
													<div class="icon-selector-inner">
														<ul class="icon-toggle-list icons-lists">
															<li>
																<div class="icon-box">
																	<i class="icon-volleyball icon-style" title="Volleyball"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-academic icon-style" title="Academic"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-anchor icon-style" title="Anchor"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-arts icon-style" title="Arts"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-asb icon-style" title="ASB"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-atom icon-style" title="Atom"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-bandleader icon-style" title="Band Leader"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-baseball icon-style" title="Baseball"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-basketball icon-style" title="Basketball"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-bolt icon-style" title="Bolt"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-book icon-style" title="Book"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-bowtie icon-style" title="Bowtie"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-cheer icon-style" title="Cheer"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-chess icon-style" title="Chess"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-choir icon-style" title="Choir"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-clarinet icon-style" title="Clarinet"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-clover icon-style" title="Clover"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-club icon-style" title="Club"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-crown icon-style" title="Crown"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-cymbals icon-style" title="Cymbals"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-dance icon-style" title="Dance"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-debate icon-style" title="Debate"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-diamond icon-style" title="Diamond"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-drama icon-style" title="Drama"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-drums icon-style" title="Drums"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-filmvideo icon-style" title="Film Video"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-flute icon-style" title="Flute"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-football icon-style" title="Football"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-frenchhorn icon-style" title="French Horn"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-globe icon-style" title="Globe"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-gradcap icon-style" title="Grand Cap"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-heart icon-style" title="Heart"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-horseshow icon-style" title="Horse Shoe"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-keyclub icon-style" title="Key Club"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-lacrosse icon-style" title="Lacrosse"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-lightbulb icon-style" title="Lightbulb"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-rose icon-style" title="Rose"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-sax icon-style" title="Sax"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-skull icon-style" title="Skull"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-soccer icon-style" title="Soccer"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-softball icon-style" title="Softball"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-spade icon-style" title="Spade"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-stache icon-style" title="Stache"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-star-1 icon-style" title="Star"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-swimming icon-style" title="Swimming"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-tennis icon-style" title="Tennis"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-tophat icon-style" title="Tophat"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-trackfield icon-style" title="Track & Field"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-trombone icon-style" title="Trombone"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-trumpet icon-style" title="Trumpet"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-tuba icon-style" title="Tuba"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-volleyball icon-style" title="Volleyball"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-wrestling icon-style" title="Wrestling"></i>
																</div>
															</li>
															<li>
																<div class="icon-box">
																	<i class="icon-xylophone icon-style" title="Xylophone"></i>
																</div>
															</li>
															<div class="clearfix"></div>		
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
									<div class="tgen-btn-holder">
										<button class="orange-btn tgen-apply-btn white gsemibold font-small generate-design">Apply</button>
									</div>
							</div>
							</div>
					  </div>
					  <!-- 3rd tab -->
					  <div id="patterns" class="tab-pane fade">
						<div class="pattern-holder-outer">
							<div class="pattern-label gregular font-small gray-darker">Select A Pattern</div>
							<div class="pattern-holder-inner pattern-container">
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-1.jpg');?>" alt="" />
									<span class="remove-pattern remove-pattern-active"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-2.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-3.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-4.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-5.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-6.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-7.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-8.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-9.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-10.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-11.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								<div class="pattern-holder-item">
									<img class="img-responsive img-center " src="<?php echo base_url('public/'.STORE.'/images/pattern-12.jpg');?>" alt="" />
									<span class="remove-pattern"></span>
								</div>
								
								<div class="clearfix"></div>
							</div>
						</div>
					  </div>
					</div>
				</div>
				
				
				<div class="custom-shirtcol-label gregular font-large blackz">Select Product Colors</div>
				<div class="custom-shirtcol-label glight font-xsmall gray" style="padding: 0;padding-bottom: 6px;">Click to add/remove product colors</div>
					<div class="custom-shirtcol-holder">
						<?php
							foreach ($shirt_colors as $key => $value) {
								echo '<div class="shirt-col-selection" data-toggle="tooltip" data-placement="top" title="'.$value->color_name.'" data-id="'.$value->color_id.'" data-code="'.$value->color_code.'" data-hex="'.$value->hex_value.'" data-original-title="'.$value->color_name.'" style="background:#'.$value->hex_value.';"><span class="selected"></span></div>';
							}
						?>
					</div>
					<div class="tgen-btn-holder2">
						<button class="orange-btn tgen-addtocollection-btn white gsemibold font-small" style="display: none;">Add to Collection</button>
						<button class="orange-btn tgen-back-to-campaign-btn white gsemibold font-small" style="display: none; margin-top: 15px;">Go Back</button>
						<div class="custom-shirtcol-label glight font-xsmall gray-dark" style="padding-top: 6px;padding-bottom: 6px;"><span style="color: #ff0000;">*</span>Please Note: Colors and sizing of art may vary.</div>
					</div>
					
					
			</div>
			</div>
		</div>
	</div>
	
	
	<!--DESIGN CART-->
		<div class="design-container">
			<div class="design-cont-btn dc-open"></div>
			<div class="design-cont-header gsemibold font-medium white text-center">My Collection <span class="close-collection-slider icon-circled-x"></span></div>
			<div class="mycoll-item-holder">
			<div class='empty-collection gregular gray-light'>Empty</div>			
				
			</div>
			<div class="design-cont-footer">
				<button class="gray-btn proceed-btn white gsemibold font-small cart-items-btn" data-code="" disabled>Next</button>
			</div>
		</div>
</div>



<!-- Modal confirm-->
<div id="removedesign" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this design?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">Removing this design will also delete all the styles and colors you have selected.<br>
This cannot be undone</div>
						<div class="confirm-btn-holder text-center">
							<button class="orange-btn yesremove-btn white gsemibold font-small" data-dismiss="modal">Yes, remove this design</button>
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>



<!-- Modal confirm-->
<div id="removestyle" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this style?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">Removing this style will also delete all the colors you’ve selected for it.<br>
This cannot be undone.</div>
						<div class="confirm-btn-holder text-center">
							<button class="removestyle orange-btn yesremove-btn white gsemibold font-small" data-dismiss="modal">Yes, remove this style</button>
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Modal confirm-->
<div id="removecollection" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to remove this item?</div>
						<div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div>
						<div class="warning-label2 gregular font-regular gray text-center">Removing this item will delete all your customizations.<br>
This cannot be undone.</div>
						<div class="confirm-btn-holder text-center">
							<button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
							<button class="orange-btn yesremove-btn yesremove-btn-collection white gsemibold font-small" data-dismiss="modal">Yes, remove this design</button>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Modal zoom-->
<div id="zoomer" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content zoom-modal-holder">
      <div class="pop-header-notxt white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large popup-close" data-dismiss="modal"></span>
	  </div>
	   <div class="container-fluid zoom-container">
				<div class="row">
					<div class="col-lg-12 text-center">
					<div class="img-zoom-holder"><img class="img-responsive img-center design-zoom-image" src="" alt="" /></div>
					</div>
				</div>
		</div>

	</div>
  </div>
</div>

<!-- Modal sizechart-->
<div id="sizechart" class="modal fade" role="dialog">
  	<div class="modal-dialog cantfind-dialog" style="width: 671px;">

	    <!-- Modal content-->
	    <div class="modal-content sizechart-modal-holder">
	      	<div class="confirm-header white gregular font-medium">
				<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
		  	</div>
		  	<div class="container-fluid modal-bg">
					<div class="row">
						<div class="col-lg-12 text-center measure-pad ">
							
							<div class="sizechart-label1 gregular font-large blackz text-center sizechart-lbl">Size Chart for <span class="style-label">Style Name</span></div>
							
							<div class="cont-holder size-chart-info">
								<p class="gray gregular font-small text-left ">Classic:<br>
By far our best selling tee. Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. Check out the specs for the actual measurements and dimensions before you order.</p>
							</div>
							<div class="table-responsive size-chart-data"> 
								<table class="table table-bordered text-left font-small size-chart-table">
									<thead>
										<tr>
											<th class="gsemibold blackz">Body Specs</th>
											<th class="gsemibold blackz">S</th>
											<th class="gsemibold blackz">M</th>
											<th class="gsemibold blackz">L</th>
											<th class="gsemibold blackz">XL</th>
											<th class="gsemibold blackz">2XL</th>
											<th class="gsemibold blackz">3XL</th>
											<th class="gsemibold blackz">Tolerance</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="gregular blackz">Body Width</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Body Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Sleeve Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Arm hole</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!-- Modal confirm-->
<div id="confirm" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-medium">
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="container-fluid">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to add this item?</div>
						<!-- <div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/warning-alert.png');?>" alt="" /></div> -->
						<div class="warning-label2 gregular font-regular gray text-center">Artwork already exist.</div>
						<div class="confirm-btn-holder text-center">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<button class="green-btn nogoback-btn white gsemibold font-small updating-artwork">PROCEED</button>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<button class="orange-btn white gsemibold font-small" data-dismiss="modal">CANCEL</button>
						</div>
						</div>
					</div>
				</div>
	  </div>
	

	</div>
  </div>
</div>


<!-- Add more style modal-->
<div id="addmorestyle" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">Add more styles
			<!--<span class="addmorestyle-paginate white gregular font-medium">4/6</span>-->
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
										  <select class="form-control custom-input gregular font-regular gray" id="shirt-group-select" autofocus>
												<option value="">Choose All</option>
											<?php foreach ($shirt_category as $key => $group) { ?>
												<option value="<?php echo $group->shirt_group_id; ?>"><?php echo ucwords(strtolower($group->shirt_group_name)); ?></option>
											<?php	} ?>											
										  </select>
										</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<hr class="popup-border">
						</div>
					</div>
					<div class="row addmorestyle-holder-inner style-container">
						<!-- load selectable styles -->
						<?php
							foreach ( $styles as $key => $value ) {
						?>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 main-style-holder">
							<div class="gregular font-small gray style-desc"><?php echo ucwords($value->group_name);?></div> 
							<div class="addmorestyle-shirt-holder style-selection" data-id="<?php echo $value->id; ?>" style-id="<?php echo $value->shirt_style_id; ?>" group-id="<?php echo $value->newgroup; ?>">
								<span class="style-check"></span>
								<img class="img-responsive img-center styleshirt-col" src="<?php echo base_url($value->front_image_link); ?>" alt="" />
							</div>
						</div>
						<?php
							}
						?>

					</div>
				</div>
	  </div>
	
		<div class="modal-footer">
          <button type="button" class="done-btn add-style green-btn white gsemibold font-small text-uppercase add-more-styles-button" data-dismiss="modal">Done</button>
        </div>
	</div>
  </div>
</div>


<!-- Add more design modal-->
<div id="addmoredesign" class="modal fade" role="dialog">
  <div class="modal-dialog cantfind-dialog">

    <!-- Modal content-->
    <div class="modal-content confirm-modal-holder">
      <div class="confirm-header white gregular font-large">Add more designs to your collection.
			<span class="addmorestyle-paginate white gregular font-medium">4/6</span>
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
	  </div>
	  <div class="addmorestyle-holder">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="form-group">
										  <select class="form-control custom-input gregular font-regular gray" id="design-category">
											<option value="">Choose All</option>
											<?php foreach ($art_category as $key => $cat) { ?>
												<option value="<?php echo $cat->id; ?>"><?php echo ucwords(strtolower($cat->name)); ?></option>
											<?php	} ?>
										  </select>
										</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<hr class="popup-border">
						</div>
					</div>
					<div class="row addmorestyle-holder-inner artwork-selection">
						<?php
							foreach ($templates as $key => $value) {
								$template_img = $value->thumbnail;
								$img = $value->encodedThumbnail;
								$img2 = $value->encodedImage;
						?>
						<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 design-cont-holder" cat-id="<?php echo $value->template_category_id; ?>">
							<div class="design-item-holder design-selection" data-id="<?php echo $value->template_id; ?>" data-hex="<?php echo $value->hex_value; ?>" data-category="<?php echo $value->template_category; ?>" data-property='<?php echo $value->properties; ?>' >
							<div class="design-label gregular font-xsmall gray text-uppercase lbl-overflow">Design:<span class="design-name gsemibold"><?php echo $value->display_name; ?></span></div>
							<div class="store-image-box" style="background-color: #<?php echo $value->hex_value; ?>">
								<!-- <span class="design-backprint"></span> -->
								<div class="store-selection-holder">
									<!--<span class="design-unselect"></span>-->
									<span class="design-select"></span>
								</div>
								<img class="img-responsive img-center box-design" data-imgurl="<?php echo $template_img; ?>" data-viewThumbnail="<?php echo $img2; ?>" src="<?php echo $img; ?>" alt="" />
							</div>
							</div>
						</div>
						<?php
							}
						?>
					</div>
				</div>
	  </div>
	
		<div class="modal-footer">
          <button type="button" class="done-btn gray-btn white gsemibold font-small text-uppercase add-more-designs-button" disabled data-dismiss="modal">Done</button>
        </div>
	</div>
  </div>
</div>
<?php $this->load->view_store('footer'); ?>