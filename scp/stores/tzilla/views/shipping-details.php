<?php $this->load->view_store('header');  ?>
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/checkout.css');?>">
<input type="hidden" id="customer_id" value="<?php echo $this->session->customer_id; ?>">
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/shipping-details.js').'?df='.rand(1, 99999);?>"></script>

<script type="text/javascript">
		setShippingMethod('<?php echo $this->session->shipping_method; ?>');
		// setShippingFee('<?php echo $this->session->shipping_fee; ?>');
		setWeight('<?php echo $totalWeight; ?>');
		setUsValidate('<?php echo site_url('us_states/validate_state_zip'); ?>');
		$(document).ready(function(){
			$("#storetitle").text('TZilla.com - Shipping Details');
		});
</script>
<section style="overflow:hidden;">

<div class="container shipping-cont">
	<div class="row">		
		<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 discount-holder-main col-lg-push-7 col-md-push-7">
			<div class="discount-holder-outer">
				<div class="discount-holder-cont">
					<div class="discount-holder-inner">
						<input type="text" class="form-control checkout-input gregular discount-text" id="discount_detail" name="discount_detail" PlaceHolder="Discount / Promo Code" value="<?php echo $this->session->userdata('discount_code'); ?>">
					</div>	
					<button class="gray-btn white glight font-small checkout-apply-btn apply-discount">Apply</button>
				</div>
				<div class="discount-holder-summary">
					<div class="discount-holder-perline totalQty" data-total-items="<?php echo $totalQuantity; ?>" >
						<span class="pull-left gregular font-regular gray-darker">Subtotal</span>
						<span class="pull-right gregular font-regular gray-darker subtotal computeall">$ <?php echo $Subtotal; ?></span>
						<div class="clearfix"></div>
					</div>
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Shipping</span>
						<span class="pull-right gregular font-regular gray-darker shipping-fee computeall">Free</span>
						<div class="clearfix"></div>
					</div>
					<div class="discount-holder-perline">
						<span class="pull-left gregular font-regular gray-darker">Taxes</span>
						<span class="pull-right gregular font-regular gray-darker tax-fee computeall">$ <?php echo $tax_amount; ?></span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="discount-holder-perline">
						<div class="pull-left gsemibold font-regular gray-darker">Total</div>
						<div class="pull-right">
							<span class="gsemibold font-small gray">USD</span>
							<span class="gsemibold font-large gray-darker total" id="totalprice">$ <?php echo $Subtotal;?></span>
						</div>
						<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<form id="validatedetails" method="post" action="javascript:void(0)" role="form">
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 shipping-outer col-lg-pull-5 col-md-pull-5">
			<div class="shipping-inner">	
				<div class="shipping-title1 gregular blackz font-large">Shipping Details</div>
				<div class="row-line">
					<div class="shipping-col-left">
						<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">First Name</label>
						  <input type="text" name="firstname" class="form-control checkout-input-req gitalic name" data-name="Firstname" id="firstname" PlaceHolder="First Name" value="<?php if(isset($customer_info)) echo $customer_info['cust_firstname']; ?>">
						</div>
					</div>
					<div class="shipping-col-right">
						<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">Last Name</label>
						  <input type="text" name="lastname" class="form-control checkout-input-req gitalic name" data-name="Lastname" id="lastname" PlaceHolder="Last Name" value="<?php if(isset($customer_info)) echo $customer_info['cust_lastname']; ?>">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row-line">
					<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">Address</label><?php if(isset($customer_info)) if(!empty($customer_info['address_2'])): $add2 = ' '.$customer_info['address_2']; else: $add2 = ''; endif;  ?>
						  <input type="text" name="address" class="form-control checkout-input-req gitalic"  data-name="Address" id="add1" PlaceHolder="Address" value="<?php if(isset($customer_info)) echo $customer_info['cust_address_1'].$add2; ?>">
					</div>
				</div>
				<div class="row-line">
					<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">City</label>
						  <input id="city" type="text" name="city" class="form-control checkout-input-req gitalic" data-name="City" id="city" PlaceHolder="City" value="<?php if(isset($customer_info)) echo $customer_info['cust_city']; ?>">
					</div>
				</div>
				<div class="row-line">
					<div class="shipping-col-one">
						<div class="form-group handler">
							  <label for="" class="glight font-regular gray-dark">Country</label>
							  <input type="text" name="country" class="form-control checkout-input gitalic" data-name="Country" id="country" value="United States"readonly PlaceHolder="United States">
						</div>
					</div>
					<div class="shipping-col-two">
						<div class="form-group handler">
							  <label for="" class="glight font-regular gray-dark">State</label>
							  <select name="state" class="form-control gitalic checkout-select checkout-input-req" data-name="State" id="state">
							  	<option value="">Select a State</option>
                            <?php foreach ($states as $key => $val) {?>
                                <option data-value="<?php echo $val['state_name'];?>" <?php if(isset($customer_info)): if( $val['state_code'] == $customer_info['cust_state'] ){echo 'selected="selected"';} endif; ?>  value="<?php echo $val['state_code'];?>"><?php echo ucfirst($val['state_name']);?></option>
                            <?php } ?>
							  </select>
						</div>
					</div>
					<div class="shipping-col-three">
						<div class="form-group handler">
							  <label for="" class="glight font-regular gray-dark">Zip Code</label> 
							  <input id="zip" type="text" name="zip" class="form-control gitalic zipcode checkout-input-req" data-name="Zipcode" id="zipcode" PlaceHolder="Zip code" onkeypress="return isNumberKey(event)" value="<?php if(isset($customer_info)) echo $customer_info['cust_zip']; ?>">
						</div>
						<input type="hidden" name="validation_passed_status" class="validation_passed_status" id="validation_passed_status" value="false">
					</div>
					<div class="clearfix"></div>
					<div id="notification" style="color:#FF0000;"></div>
				</div>
				<div class="row-line">
					<div class="form-group handler">
						  <label for="" class="glight font-regular gray-dark">Phone</label>
						  <input name="phone" type="text" class="form-control checkout-input-req gitalic phoneno" data-name="Phone" onkeypress="return isNumberKey(event)" id="contact_no" PlaceHolder="Phone (xxx) xxx-xxxx" value="<?php if(isset($customer_info)) echo $customer_info['cust_contact_no']; ?>">
					</div>
				</div>
				<div class="shipping-title2 gsemibold font-medium gray-darker">Shipping Method</div>
				<div class="row-line shipping-method-cont">
					<?php
					if( count($fedex_shipping) > 0 ){
						foreach ($fedex_shipping as $key => $fedex_fee){
						$shipping_key = $key;
						$shipping_method = str_replace("_", " ", $shipping_key);
					?>
						<div class="shipping-radio-cont payment-radio-cont">
							<input type="radio" name="shipping_method " class="green-circle" data-shipping-method="<?php echo $shipping_key; ?>" value="<?php echo $fedex_fee; ?>" checked>
							<span class="radio-title gregular font-regular gray-dark shipping-method"><?php echo $shipping_method ; ?></span>

							<span class="radio-val gregular font-regular gray-dark shipping-method-fee">$<?php echo $fedex_fee; ?></span>
						</div>
				
				<?php }
					 }else{ ?>
						<div class="shipping-radio-cont">
							<input type="radio" name="shipping_method" class="green-circle" data-shipping-method="" value="" checked>
							<span class="radio-title gregular font-regular gray-dark shipping-method"></span>

							<span class="radio-val gregular font-regular gray-dark shipping-method-fee"></span>
						</div>

				<?php } ?>
				</div>
				<div class="row proceeder-cont">
					<div class="col-lg-7">
						<span class="checker-box"></span><span class="checkbox-label gsemibold font-small save-for-next" >Save my shipping info for next time</span>
					</div>
					<div class="col-lg-5">
						<button class="gray-btn white gsemibold font-small pull-right next-btn add-shipping-details" disabled="disabled">Next <span class="ship-nxt-loader" style="padding-left: 8px; display: none;">
							<img class="img-responsive cc-cards" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" style="width: 20px;height: 20px;" alt=""/>
						</span></button>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<a href="#" class="green gsemibold font-small goback-cart">❮ Go Back to Cart</a>
					</div>
				</div>
				<div class="footer-holder">
						<a class="footer-links gsemibold font-small" href="#" type="button" data-toggle="modal" data-target="#privacypolicy" data-dismiss="modal">Privacy Policy</a>
						<a class="footer-links gsemibold font-small" href="#" type="button" data-toggle="modal" data-target="#refundpolicy">Refund Policy</a>
						<a class="footer-links gsemibold font-small" href="#" type="button" data-toggle="modal" data-target="#termsCondition" data-dismiss="modal">Terms of Service</a>
				</div>
			</div>
		</div>	
		</form>
	</div>
</div>
</section>

<?php $this->load->view_store('footer');  ?>  
