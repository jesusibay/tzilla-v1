<?php $this->load->view_store('account-header');  ?> 
<?php foreach ($getdata as $key => $user) {  } ?>
    <section class="section">
        <div class="row sameheight-container">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card card-block sameheight-item text-center ref-id">
                    <div class="gsemibold font-medium long-text">Active Campaigns</div> 
                </div>
                <div class="card card-block sameheight-item text-center">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/megaphone-gray.png');?>" alt="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gregular font-slarge"><?php echo count($active); ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-8 col-lg-offset-2 col-md-12 col-xs-12">
                            <a href="<?php echo base_url('account-campaigns'); ?>"><button class="orange-btn white gsemibold font-small mob-btn">See All Campaigns</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card card-block sameheight-item text-center ref-id">
                    <div class="gsemibold font-medium long-text">Total Sales</div>
                </div>
                <div class="card card-block sameheight-item text-center">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/dollar-gray.png');?>" alt="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gregular font-slarge">$<?php echo (!empty($total_sales['totalprice']))? number_format($total_sales['totalprice'], 2) : '0.00'; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12 total-sales-label">
                            <span class="grayz gregular font-regular">Today:</span>
                            <span class="grayz gregular font-regular">$<?php echo (!empty($total_sales_today['totalprice']))? number_format($total_sales_today['totalprice'], 2) : '0.00'; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card card-block sameheight-item text-center ref-id">
                    <div class="gsemibold font-medium long-text">Total Profits</div>
                </div>
                <div class="card card-block sameheight-item  text-center">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/coins-gray.png');?>" alt="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gregular font-slarge">$<?php echo (!empty($total_profit['total_profit']) )? number_format( $total_profit['total_profit'], 2 ) : '0.00'; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12 total-sales-label">
                            <span class="grayz gregular font-regular">Today:</span>
                            <span class="grayz gregular font-regular">$<?php echo (!empty($total_profit_today['total_profit']) )? number_format( $total_profit_today['total_profit'],2 ) : '0.00'; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card card-block sameheight-item text-center ref-id">
                    <div class="gsemibold font-medium long-text">Total Items Sold</div>
                </div>
                <div class="card card-block sameheight-item text-center">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/tag-gray.png');?>" alt="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gregular font-slarge"><?php echo (!empty($total_items['quantity']))? $total_items['quantity'] : 0; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12 total-sales-label">
                            <span class="grayz gregular font-regular">Today:</span>
                            <span class="grayz gregular font-regular"><?php echo (!empty($total_items_today['quantity']))? $total_items_today['quantity'] : 0; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="card card-block sameheight-item text-center ref-id">
                    <div class="gsemibold font-medium long-text">Campaigns Ending Soon</div>
                </div>
                <div class="card card-block sameheight-item text-center">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/cash-gray.png');?>" alt="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                           <span class="blackz gregular font-slarge"><?php echo $ended_soon; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-8 col-lg-offset-2 col-md-12 col-xs-12">
                            <a href="<?php echo base_url('account-campaigns'); ?>"><button class="orange-btn white gsemibold font-small mob-btn">Extend Campaigns</button></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style="display:none;">
                <div class="card card-block sameheight-item text-center ref-id">
                    <div class="gsemibold font-medium long-text">Your Payout Settings</div>       
                </div>
                <div class="card card-block sameheight-item text-center">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/megaphone-gray.png');?>" alt="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gregular font-slarge">$<?php echo (!empty($payout_amount['payout_amount']))? number_format($payout_amount['payout_amount'],2) : '0.00'; ?></span>
                            <span class="grayz gsemibold font-small">/cutoff</span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-8 col-lg-offset-2 col-md-12 col-xs-12">
                            <a href="<?php echo base_url('account-payout'); ?>"><button class="orange-btn white gsemibold font-small mob-btn">Update</button></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</article>
