<?php $this->load->view_store('header');  ?>
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/trackorder.css');?>">
<div class="container-fluid track-header">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 col-md-7 col-sm-12 col-xs-12">
				<h1 class="white">Order Tracking</h1>
				<p class="white">Simply get a shirt for yourself, or create a campaign<br>to raise funds for your school</p>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid track-number-holder">
<div class="container">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blackz font-medium gregular" style="margin-bottom: 5px;">Enter Your Tracking Number</div>
		<!--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 gregular gray-dark font-regular">Enter the tracking number sent to your email.</div>-->
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
			<input type="text" class="track-num-text form-control"/>
		</div>
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
				<button class="orange-btn white gsemibold font-regular">Track  Your Order</button>
		</div>
	</div>
</div>
</div>
	<div class="container track-detail-holder">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="blackz gsemibold font-medium text-center track-activ">Status of Ref #:
                        <span class="blackz gsemibold font-medium text-center">0000001</span>
                    </div>
                   <img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/order-tracking-01.jpg');?>" alt="">
                </div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="track-activ gsemibold blackz font-large">Activities</div>
				<div class="track-trav gregular blackz font-medium">Travel History</div>
				<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<label class="blackz gsemibold">Date</label>
				</div>
				
				</div>
				<div class="panel-group" id="accordion">
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="gregular font-regular blackz">
						<span class="track-collapse glyphicon glyphicon-plus"></span> 6/16/2016 - Wednesday</a>
					  </h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse">
					 <div class="panel-body">
						<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="gregular blackz">Time</th>
											<th class="gregular blackz">Activity</th>
											<th class="gregular blackz">Location</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>12:00 PM</td>
											<td>Delivered</td>
											<td>Los Angeles, CA</td>
										</tr>
										<tr>
											<td>08:51 AM</td>
											<td>On FedEx vehicle for Delivery</td>
											<td>Los Angeles, CA</td>
										</tr>
										<tr>
											<td>06:31 AM</td>
											<td>At local FedEx facility</td>
											<td>Los Angeles, CA</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					  </div>
					</div>
				  
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="gregular font-regular blackz">
						<span class="track-collapse glyphicon glyphicon-minus"></span> 6/15/2016 - Tuesday</a>
					  </h4>
					</div>
					<div id="collapse2" class="panel-collapse collapse in">
					  <div class="panel-body">
					   <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Time</th>
										<th>Activity</th>
										<th>Location</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>12:00 PM</td>
										<td>Delivered</td>
										<td>Los Angeles, CA</td>
									</tr>
									<tr>
										<td>08:51 AM</td>
										<td>On FedEx vehicle for Delivery</td>
										<td>Los Angeles, CA</td>
									</tr>
									<tr>
										<td>06:31 AM</td>
										<td>At local FedEx facility</td>
										<td>Los Angeles, CA</td>
									</tr>
								</tbody>
							</table>
						</div>
					  </div>
					</div>
				  </div>
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="gregular font-regular blackz">
						<span class="track-collapse glyphicon glyphicon-plus"></span> 6/14/2016 - Monday</a>
					  </h4>
					</div>
					<div id="collapse3" class="panel-collapse collapse">
					  <div class="panel-body">
					   <div class="table-responsive">
							<table class="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Time</th>
										<th>Activity</th>
										<th>Location</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>12:00 PM</td>
										<td>Delivered</td>
										<td>Los Angeles, CA</td>
									</tr>
									<tr>
										<td>08:51 AM</td>
										<td>On FedEx vehicle for Delivery</td>
										<td>Los Angeles, CA</td>
									</tr>
									<tr>
										<td>06:31 AM</td>
										<td>At local FedEx facility</td>
										<td>Los Angeles, CA</td>
									</tr>
								</tbody>
							</table>
						</div>
					  </div>
					</div>
				  </div>
				</div>


			</div>
		</div>
	</div>
<?php $this->load->view_store('footer');  ?>	
<script>
$(document).ready(function(){
	$(document).on("click", ".panel-title>a", function(){
		
	});
	
			$('#collapse1').on('shown.bs.collapse', function () {
			   $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
			});

			$('#collapse1').on('hidden.bs.collapse', function () {
			   $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
			});
			
			$('#collapse2').on('shown.bs.collapse', function () {
			   $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
			});

			$('#collapse2').on('hidden.bs.collapse', function () {
			   $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
			});
			
			$('#collapse3').on('shown.bs.collapse', function () {
			   $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
			});

			$('#collapse3').on('hidden.bs.collapse', function () {
			   $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
			});
});
</script>