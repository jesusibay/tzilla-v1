<?php $this->load->view_store('account-header');  ?> 
<style type="text/css">
    .sort{
        cursor: pointer;
    }
</style>
<?php 
function sorting( $order ) {
        switch ( $order ) {
            case 'ASC':
                return "down";
                break;
            default:
                return "up";
                break;
        }
    }
    ?>
        <section class="section">
       
            <div class="col-md-9">
                <div class="dash-title">
                    <h3 class="title">My Campaigns</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <a href="<?php echo base_url('school/search'); ?>"><button class="green-btn cartrev-update-btn white gsemibold font-small margin-btn2">Create New Campaign</button></a>
            </div>

        </section>
        
        <section class="section">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-block pad-mob">
                            <div class="card-title-block">
                                <!-- <h3 class="title">Responsive simple</h3> -->
                            </div>
                            <section class="example">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th class="gregular font-regular blackz ">Campaign Name <i class="fa fa-angle-<?php if($sort_by == 'campaign_title'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort" data-table="campaign_title"></i></th>
                                                <th class="gregular font-regular blackz ">Start <i class="fa fa-angle-<?php if($sort_by == 'start_date'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort" data-table="start_date"></i></th>
                                                <th class="gregular font-regular blackz ">End <i class="fa fa-angle-<?php if($sort_by == 'end_date'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort" data-table="end_date"></i></th>
                                                <th class="gregular font-regular blackz ">Total Sales <i class="fa fa-angle-<?php if($sort_by == 'sales'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort" data-table="sales"></i></th>
                                                <th class="gregular font-regular blackz ">Total Profits (Funds Raised) <i class="fa fa-angle-<?php if($sort_by == 'profit'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort" data-table="profit"></i></th>
                                                <th class="gregular font-regular blackz ">Status <i class="fa fa-angle-<?php if($sort_by == 'campaign_status'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort" data-table="campaign_status"></i></th>
                                                <th class="col-md-1"></th>
                                                <th class="col-md-1"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if( count($campaigns) > 0){
                                            foreach ($campaigns as $key => $campaign) { 
                                                $now = time(); // or your date as well
                                                $end_date = strtotime( $campaign->end_date );
                                                $datediff = $end_date - $now;
                                                $days_remaining = round((($datediff/24)/60)/60);                                                            
                                                $stat = '';
                                                $status = '';
                                                $sales = $this->Campaigns_model->get_campaign_sales( $campaign->campaign_url);
                                                $total_base_price = $this->Campaigns_model->get_campaign_base_price( $campaign->campaign_url);

                                                if($campaign->campaign_status == 1 ){
                                                    $status = 'Published';
                                                    
                                                    if( 0 <= $days_remaining && $days_remaining <= 1 ){
                                                        $stat = '<i class="glyphicon glyphicon-exclamation-sign pointer" style="color: #e1564b;" title="You have '.$days_remaining.' day left.">';
                                                    }
                                                    if( 2 <= $days_remaining && $days_remaining <= 7 ){
                                                        $stat = '<i class="glyphicon glyphicon-exclamation-sign pointer" style="color: #e1564b;" title="You have '.$days_remaining.' days left.">';
                                                    }
                                                }
                                                else if($campaign->campaign_status == 0 ){
                                                    $status = 'Draft';
                                                }

                                                else{
                                                    $status = 'Ended';
                                                }

                                                ?>
                                            <tr>
                                                <td><?php if($campaign->campaign_status != 0 ): ?><a class="tzilla-green" href="<?php echo base_url($campaign->campaign_url); ?>"><?php echo $campaign->campaign_title; ?></a> <?php else: echo ucwords(ucfirst($campaign->campaign_title)); endif;?></td>
                                                <td><?php echo date("m-d-Y", strtotime($campaign->start_date)); ?></td>
                                                <td><?php echo date("m-d-Y", strtotime($campaign->end_date)); ?> <?php echo $stat;?></td>
                                                <td>$<?php echo (!empty($sales['totalprice']))? number_format($sales['totalprice'],2) : '0.00'; ?></td>
                                                <td>$<?php echo (!empty($total_base_price['total_profit']) )? number_format( $total_base_price['total_profit'], 2 ) : '0.00'; ?>/$<?php echo number_format($campaign->campaign_goal, 2); ?></td>
                                                <td><?php echo $status; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('campaign/edit').'/'.$campaign->campaign_id.'/'.$campaign->campaign_user_id; ?>"><button class="green-btn cartrev-update-btn white gsemibold font-small dashb-btn">Edit</button></a>
                                                </td>
                                                <td>
                                                    <button <?php if( $campaign->campaign_status != 1 ): echo 'disabled="disabled"'; endif;?>data-toggle="modal" data-target="#removecamp" data-id="<?php echo $campaign->campaign_id; ?>" class="gsemibold font-regular white dashb-btn <?php if( $campaign->campaign_status == 1 ): echo 'orange-btn force_end_campaign'; else: echo "gray-btn"; endif; ?>" data-amount="<?php echo (!empty($total_base_price['total_profit']) )? number_format( $total_base_price['total_profit'], 2 ) : '0.00'; ?>">End Campaign</button>
                                                </td>
                                            </tr>
                                            <?php } 
                                                    }else{?>
                                                    <tr><td colspan="8" align="center"> No records yet.</td></tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="form-group text-center">
            <div class="row">
                <nav aria-label="Page navigation example">
                   <?php echo $this->pagination->create_links();?> 
                </nav>
            </div>
        </div>
    </article>

    <!-- remove camp modal -->
    <div id="removecamp" class="modal fade" role="dialog">
        <div class="modal-dialog">

        <!-- Modal content-->
            <div class="modal-content confirm-modal-holder">
                <div class="confirm-header white gregular font-medium">
                    <span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <div class="warning-label1 gregular font-large blackz text-center">Are you sure you want to end your Campaign?</div>
                            <div class="warning-img-holder"><img class="img-responsive img-center" src="<?php echo base_url().'public/'.STORE.'/images/warning-alert.png'; ?>" alt=""></div>
                            <div class="warning-label2 gregular font-regular gray text-center">
                                All your data (Campaign settings, garments, customizations) will be <br>permanently deleted. This cannot be undone.
                            </div>
                            <div class="confirm-btn-holder text-center">
                                <button class="green-btn nogoback-btn white gsemibold font-small" data-dismiss="modal">No, go back to the page</button>
                                <button id="cancel_campaign" class="orange-btn yesremove-btn white gsemibold font-small" data-dismiss="modal">Yes, cancel my Campaign</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  -->

<script type="text/javascript">
$(document).ready(function(){
    $("#storetitle").text('TZilla.com - Account My Campaigns');
});
</script>