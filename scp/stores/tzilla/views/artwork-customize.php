<style type="text/css">
	.list-of-functions, .display-result-here, .selected-artworks, .super-customize-functions, .display-image, .artwork-properties, .product-settings, .art-template
	{
		float: left;
	}

	.list-of-functions
	{
		width: 25%;
	}

	.display-result-here, .selected-artworks, .super-customize-functions
	{
		width: 74%;
	}

	.display-image, .artwork-properties, .product-settings, .collection-items
	{
		width: 30%;
		border: 1px solid black;
	}

	img 
	{
		border: 1px solid black;
		background: #cccccc;
		width: 200px;
		height: 200px;
	}

</style>

<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/jquery-1.9.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/artwork-customize.js').'?df='.rand(1, 99999);?>"></script>

<script type="text/javascript">
	setMainUrl( '<?php echo base_url(); ?>' );
</script>
<div  class="list-of-functions">
	<div>goto<a href="#" class="next-page">SET PRICING</a> </div><br>
	<div>backend functions for TGEN page <a href="#" class="clear-result">clear</a> </div>
	<a href="#1" class="load-garments">function 1 - load available garments ( Done )</a><br>
	<a href="#2" class="load-shirt-colors">function 2 - load available color per garment ( Done )</a><br>
	<a href="#3" class="load-size-chart">function 3 - load size chart information ( Done )</a><br>
	<a href="#4" class="load-color-selection">function 4 - load personalize ( color 1 )</a><br>
	<a href="#5" class="load-color-selection">function 5 - load personalize ( color 2 )</a><br>
	<a href="#6" class="load-icons">function 6 - load effects ( icons )</a><br>
	<a href="#7" class="load-patterns">function 7 - load effects ( patterns )</a><br>
	<a href="#8" class="load-distress">function 8 - load effects ( distress )</a><br>
	<a href="#9" class="load-textures">function 9 - load effects ( textures ) (Future Feature)</a><br>
	<a href="#10" class="load-gradients">function 10 - load effects ( gradients ) (Future Feature)</a><br>
	<a href="#11" class="load-artworks">function 11 - load selected artworks from local storage on-load (Done)</a><br>
	<a href="#12" class="load-all-artworks">function 12 - load all artworks</a><br>

	<br>
	<br>
	<div>Campaign Manager T-GEN Functions</div>
	<a href="#12" class="check-slider-artwork-limit">1. Templates slider on the left set limitation checker is up to 24 templates ( Done )</a><br>
	<a href="#13" class="check-collection-limit-fundraiser">2. *Similar TGEN - add limitation of up to 6 designs on collection function, ( Done )</a><br>
	<a href="#14" class="selected-artwork-templates">3. load selected artwork from art templates onload ( Done )</a><br>
	<a href="#15" class="load-variations">4. load design variations per artwork ( Done )</a><br>
	<a href="#16" class="set-artwork-limit">5. implement limit for selecting maximum number of artworks ( Done )</a><br>
	<a href="#17" class="">6. load artwork setting ( image , addons, properties ) ( Done )</a><br>
	<a href="#18" class="load-products">7. load available products from AMM per artwork ( Done )</a><br>
	<a href="#19" class="">8. create json data for generating artwork images base 64 output</a><br>
	<a href="#20" class="">9. create function for generating design id for collection tagging for unique item</a><br>
	<a href="#21" class="">10. validation of text fields based on rules</a><br>
	<a href="#22" class="">11. create data format to be saved on local storage before proceeding to the next page cart review ( Done )</a><br>
	<a href="#23" class="">12. create data format to be saved on local storage before proceeding to the next page set pricing ( Done )</a><br>
	<a href="#24" class="">13. assign products on the selected artwork (garments and colors)</a><br>
	<a href="#25" class="load-position">14. load AMM positioning settings</a><br>
	<a href="#26" class="">15. load the artwork in the collection tab / slider to be updated</a><br>
	<a href="#27" class="load-size-chart">16. load size information on products ( Done )</a><br>
	<a href="#28" class="load-garments">17. load available colors per garment ( Done )</a><br>
	<a href="#29" class="">18. create a function to remove items on localstorage for collection items ( Done )</a><br>
</div>
<div class="selected-artworks">

</div>
<div class="super-customize-functions">
	<div class="display-image">
		<img class="generated-image" src="">
		<div class="artwork-variations"></div>
	</div>
	<div class="artwork-properties">
		artwork properties
	</div>
	<div class="product-settings">
		products<input type="button" name="collection_add" value="Add to Collection" class="collection-add"><input type="button" name="collection_update" value="Update Collection" class="collection-update">
	</div>
	<div class="collection-items">
		collection
	</div>
</div>
<div class="display-result-here">

</div>