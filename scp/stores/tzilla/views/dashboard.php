<style type="text/css">
.sourcecode{
    display: none;
}
</style>

<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/jquery-1.9.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/js/artwork-templates.js').'?df='.rand(1, 99999);?>"></script>

<div class="list-of-functions">
<a id="func_1">Get Dashboards</a><br />
<a id="func_2">Get My FundRaisers</a><br />
<a id="func_3">get Team ScoreBoard</a><br />
<a id="func_4">View Team Member Sales</a><br />
<a id="func_5">Joined FundRaisers</a><br />

</div>

<div id="global_variable">
<strong>Global Variables:</strong><br />
var apiURL="&lt;?php echo base_url(); ?&gt;";
</div>
<div id="Parameters">
id<br />
pageNo<br />
sortColumn<br />
sortDirection<br />
------------------------------------------------<br />
for viewteammembersales<br />
------------------------------------------------<br />
whereColumn<br />
whereText<br />

</div>

<div class="sourcecode" id="sc_1">
<strong>func1 sourcecode:</strong><br />
<textarea>
$.post(apiURL+"dashboard/getDashboardDetails",{id:139},function(data){
                    $(".display-result-here").html(data);
                   }); 
</textarea>
</div>

<div class="sourcecode" id="sc_2">
<strong>func2 sourcecode:</strong><br />
<textarea rows="4" cols="50">
$.post(apiURL+"dashboard/getmyfundraisers",{id:139},function(data){
                    $(".display-result-here").html(data);
});
</textarea>
</div>
<div class="sourcecode" id="sc_3">
<strong>func3 sourcecode:</strong><br />
<textarea rows="4" cols="50">
                $.post(apiURL+"dashboard/getTeamScoreBoard",{id:1},function(data){
                    $(".display-result-here").html(data);
                   });
</textarea>
</div>
<div class="sourcecode" id="sc_4">
<strong>func4 sourcecode:</strong><br />
<textarea rows="4" cols="50">
                $.post(apiURL+"dashboard/viewTeamMemberSales",{id:139},function(data){
                    $(".display-result-here").html(data);
                   });
</textarea>
</div>
<div class="sourcecode" id="sc_5">
<strong>func5 sourcecode:</strong><br />
<textarea rows="4" cols="50">
                   $.post(apiURL+"dashboard/viewJoinedFundraisers",{id:139},function(data){
                    $(".display-result-here").html(data);
                   });
</textarea>
</div>
<div class="result" id="res">
<strong>Result</strong><br />

</div>
<div class="display-result-here">
	
</div>

<script type="text/javascript">
var apiURL="<?php echo base_url(); ?>";
$(document).ready(function(){
   $("[id^=func_]").click(function(e){
    var funcid = $(this).attr("id").split("_")[1];
    $(".sourcecode").hide();
    $("#sc_"+funcid).show();
    switch(Number(funcid)){
        case 1:
                   $.post(apiURL+"dashboard/getDashboardDetails",{id:139},function(data){
                    $(".display-result-here").html(data);
                   }); 
        break;
        case 2:
                $.post(apiURL+"dashboard/getmyfundraisers",{id:139},function(data){
                    $(".display-result-here").html(data);
                   });
        break;
        case 3:
                $.post(apiURL+"dashboard/getTeamScoreBoard",{id:1},function(data){
                    $(".display-result-here").html(data);
                   });
        break;
        case 4:
                $.post(apiURL+"dashboard/viewTeamMemberSales",{id:139},function(data){
                    $(".display-result-here").html(data);
                   });
        break;
        case 5:
                $.post(apiURL+"dashboard/viewJoinedFundraisers",{id:139},function(data){
                    $(".display-result-here").html(data);
                   });
        break;
        
        
    }
    
   }); 
   
});
</script>