<?php
$this->load->module('settings');
$store_setting = $this->settings->get('store');
foreach ($get_order_details as $key => $details) { } ?>

<article id=":148" class="content forms-page a3s aXjCH m156547e8f061dd10">
    <div   class="dash-title">
        <div class="row form-group">
            <div class="col-md-10">
                <h3 class="title">Sales Order Details</h3>
            </div>
            <!-- <div class="col-lg-2 col-md-6 col-sm-12 col-xs-12"> -->
                <p> You can check your Order Status here: <a href="<?php echo base_url('account-orders'); ?>" target="_blank"> <?php echo base_url('account-orders'); ?></a></p>
            <!-- </div> -->
        </div>
    </div>
    <section class="section">
        <div class="row sameheight-container">
            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12">
                <div class="card card-block sameheight-item ref-id">
                    
                        <span class="gsemibold font-small">Ref # :</span>
                        <span class="gsemibold font-small"> <?php echo $details['order_number']; ?></span>
                   
                </div>
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Date Purchased:</span>
                            <span class="blackz gregular font-small"><?php echo date("M d, Y", strtotime($details['order_date'])); ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Status:</span>
                            <span class="blackz gregular font-small">Delivered</span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <div class="borders-sales-details"></div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Subtotal:</span>
                            <span class="blackz gregular font-small">$<?php echo $details['subtotal']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Tax:</span>
                            <span class="blackz gregular font-small">$<?php echo (!empty($details['tax']))? number_format($details['tax'],2) : '0.00'; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Shipping Fee:</span>
                            <span class="blackz gregular font-small">$<?php echo (!empty($details['shipping_fee']))? number_format($details['shipping_fee'],2) : '0.00'; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Discount:</span>
                            <span class="blackz gregular font-small">$<?php echo (!empty($details['discount']))? number_format($details['discount'],2) : '0.00'; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12 borders-sales-details"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Grand Total:</span>
                            <span class="blackz gregular font-small">$<?php echo $details['total']; ?></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12">
                <div class="card card-block sameheight-item ref-id">
                    
                        <span class="gsemibold font-small">Shipping Details</span>
                       <?php if(isset($shipping)) if(!empty($shipping['address_2'])): $add2 = $shipping['address_2'].' '; else: $add2 = ''; endif;  ?>
                   
                </div>
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                       <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Name:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['firstname'].' '.$shipping['lastname']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Email:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['cust_email']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Phone:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['contact_no']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Address:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['address_1'].' '.$add2; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">State:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['state']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">City:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['city']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Zip:</span>
                            <span class="blackz gregular font-small"><?php if(isset($shipping)) echo $shipping['zip']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Country:</span>
                            <span class="blackz gregular font-small">United States</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12">
                <div class="card card-block sameheight-item ref-id">
                    
                        <span class="gsemibold font-small">Billing Details</span>
                       <?php if(isset($billing)) if(!empty($billing['address_2'])): $billing_add2 = $billing['address_2'].' '; else: $billing_add2 = ''; endif;  ?>
                   
                </div>
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                       <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Name:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['firstname'].' '.$billing['lastname']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Email:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['cust_email']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Phone:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['contact_no']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Address:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['address_1'].' '.$billing_add2; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">State:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['state']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">City:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['city']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Zip:</span>
                            <span class="blackz gregular font-small"><?php if(isset($billing)) echo $billing['zip']; ?></span>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <span class="blackz gsemibold font-small">Country:</span>
                            <span class="blackz gregular font-small">United States</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-title-block">
                        </div>
                        <section class="example">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" style="border: 1px solid #ccc!important;">
                                    <thead>
                                        <tr>
                                            <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Style and Design</th>
                                            <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Description</th>
                                            <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Specification</th>
                                            <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Quantity</th>
                                            <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Price</th>
                                            <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        foreach ($get_order_items as $key => $item) {
                                            $shirt_color_code = $this->Order_model->get_order_details_item_color( $item['shirt_color_code'] );
                                            $image = unserialize( $item['images'] );

                                            $shirt = $this->Order_model->get_order_shirt_styles( $item['shirt_style_code'] ); 
                                            // echo '<pre>';
                                            // print_r($shirt);
                                            // print_r($get_order_items);
                                            // echo '</pre>';
                                            // $type = $this->Campaigns_model->get_by_campaign_design_type( $item['template_type_id'] );     ?>
                                        <tr>
                                            <td style="border: 1px solid #ccc!important; text-align: center!important;">
                                                <div class="img-holder" style="background: #<?php echo $shirt_color_code['hex_value']; ?>;">
                                                    <?php  foreach ($image as $key => $value2) { $order_item_image = $value2['temp_image'];  } ?>
                                                    <img class="img-responsive img-center" src="<?php echo $order_item_image; ?>" alt="">
                                                </div>
                                            </td >
                                            <td style="border: 1px solid #ccc!important;">
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Design Name:</span>
                                                        <span class="blackz gregular font-small"><?php // if(isset($order_details)) echo $item['template_name'] ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">SKU:</span>
                                                        <span class="blackz gregular font-small"><?php echo $item['matrix_code']; ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Type:</span>
                                                        <span class="blackz gregular font-small"><?php // echo $type['name']; ?></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="border: 1px solid #ccc!important;">
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Style:</span>
                                                        <span class="blackz gregular font-small"><?php echo $shirt['group_name']; ?></span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Style Type:</span>
                                                        <span class="blackz gregular font-small">Adult</span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Size:</span>
                                                        <span class="blackz gregular font-small">Medium</span>
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-xs-12">
                                                        <span class="blackz gsemibold font-small">Color:</span>
                                                        <span class="spec-color" style="background: #<?php echo $shirt_color_code['hex_value']; ?>;"></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="border: 1px solid #ccc!important;" ><?php if(isset($get_order_items)) echo $item['quantity']; ?></td>
                                            <td style="border: 1px solid #ccc!important;">$<?php if(isset($get_order_items)) echo number_format($item['price'],2); ?></td>
                                            <td style="border: 1px solid #ccc!important;">$<?php if(isset($get_order_items)) $totalprice = $item['quantity'] * $item['price']; echo number_format($totalprice,2); ?></td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- total cont bot -->
                                <div class="col-lg-3 col-md-6 col-xs-12 col-sm-12 pull-right"> 
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Subtotal:</span>
                                            <span class="blackz gregular font-small">$<?php echo $details['subtotal']; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Tax:</span>
                                            <span class="blackz gregular font-small">$<?php echo (!empty($details['tax']))? number_format($details['tax'],2) : '0.00'; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Shipping Fee:</span>
                                            <span class="blackz gregular font-small">$<?php echo (!empty($details['shipping_fee']))? number_format($details['shipping_fee'],2) : '0.00'; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Discount:</span>
                                            <span class="blackz gregular font-small">$<?php echo (!empty($details['discount']))? number_format($details['discount'],2) : '0.00'; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 borders-design-styles"></div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Grand Total:</span>
                                            <span class="blackz gregular font-small">$<?php echo $details['total']; ?></span>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>

