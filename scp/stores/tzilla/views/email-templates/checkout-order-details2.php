<?php 
$this->load->module('settings');
$this->load->module('order');
$this->load->module('campaign');
$store_setting = $this->settings->get('store');


?>
<style type="text/css">
 .img-holder {
     padding-top: 5%;
     padding-bottom: 5%;
 }
 .form-group {
     margin-bottom: 15px;
 }
 .row {
     margin-left: -15px;
     margin-right: -15px;
 }
</style>

<?php $name = "";
	foreach ($customer as $key => $customer_val) {
		$name = $customer_val->cust_firstname ." ".$customer_val->cust_lastname;
		
	}
	
?>
<div id=":148" class="a3s aXjCH m156547e8f061dd10"><p><?php echo ucwords($name);?>, </p><p><br>Thank you for your purchase. <br></p><p>Your order should be processed and ready to be shipped within 7-10 business days.</p><p> You can check your Order Status here: <a href="<?php echo base_url('account-orders'); ?>" target="_blank"> <?php echo base_url('account-orders'); ?><wbr></a></p><p><br>
<br>	
	</p><table border="0" width="800px" cellpadding="0" cellspacing="0" style="border:1px solid #ccc;font-size:11px;padding:10px;margin:0 auto">
		
		<tbody><tr>			
			<td width="40%" colspan="2" style="border-top:1px solid #a2af9c;border-bottom:1px solid #a2af9c;padding-bottom:0px">	
				<table border="0" cellpadding="0" cellspacing="0">		
					<tbody><tr>
						<td>	
						<div style="padding:10px;width:120px; background:#000;">

						<img width="100" src="<?php echo base_url('public/'.STORE.'/images/tzilla-logo.png');?>" class="CToWUd">
						</div>
						</td>
						<td style="padding-left:5px">
							<table border="0" cellpadding="0" cellspacing="0">		
								<tbody><tr>
									<td colspan="2" style="font-size:12px"><b><span class="il">SALES</span> ORDER</b></td>
								</tr>
								<tr>
									<td style="font-size:12px">Order Date: </td>
									<td style="font-size:12px"><?php echo date('M d, Y h:i a', strtotime($order_date)); ?></td>
								</tr>
																	
							</tbody></table>
						</td>
					</tr>
					</tbody></table>
			</td>
			
			<td style="border-top:1px solid #a2af9c;border-bottom:1px solid #a2af9c;border-right:1px solid #a2af9c;padding-bottom:0px">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">		
					<tbody><tr>
						<td style="font-size:12px">Order Number:</td>
						<td style="font-size:12px"><?php echo $order_number;?></td>
					</tr>
					<tr>
						<td style="font-size:12px">Order Status:</td>
						<td><?php echo $order_status;?></td>
					</tr>
					<!-- <tr>
						<td style="font-size:12px">Checkout ID:</td>
						<td style="font-size:12px"><?php echo $checkout_id;?> </td>
					</tr> -->
					
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td colspan="3"><br></td>
		</tr>
		
		<tr>		
			<td width="33%" style="vertical-align:top;padding-left:10px">
				
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody><tr>
							<td width="33%" style="font-size:12px"><b>Customer</b><br>
						<?php 

							$firstname = "";
							$lastname = "";
							$contact_no = "";
							$email = "";

							foreach ($customer as $key => $customer_val) {
								$firstname = $customer_val->cust_firstname;
								$lastname = $customer_val->cust_lastname;
								$email = $customer_val->cust_email;
								$contact_no = $customer_val->cust_contact_no;
							
						?> 
							Name: <?php echo ucwords($firstname) .' '. ucwords($lastname);?><br>
							Email: <a href="mailto:<?php if(isset($customer)) echo $email; ?>" target="_blank"><?php if(isset($customer)) echo $email; ?></a><br>
							Phone: <?php  echo $contact_no;?><br>
						<?php } ?>			
						<br>
							<b>Shipping Method</b> <br>
						 <?php echo str_replace("_"," ",$shipping_method);?>						
						</td>
					</tr>	
				</tbody></table>
			</td>
			<td width="33%">
				<table border="0" cellpadding="0" cellspacing="0">	
					<tbody><tr>
						<td style="font-size:12px"><b>Shipping Information</b>
							<br>
							Name: <?php if(isset($shipping)) echo $shipping['firstname'].' '.$shipping['lastname']; ?><br>
							Phone: <?php if(isset($shipping)) echo $shipping['contact_no']; ?><br>
							Address1: <?php if(isset($shipping)) echo ucwords($shipping['address_1']); ?><br>
							City: <?php if(isset($shipping)) echo ucwords($shipping['city']); ?><br>
							State: <?php if(isset($shipping)) echo ucwords($shipping['state']); ?><br>
							Zip: <?php if(isset($shipping)) echo ucwords($shipping['zip']); ?><br>
							Country: <?php echo ucwords('United States');?>
							<br>						
						</td>
					</tr>
				</tbody></table>
			</td>
			<td width="33%">
				<table border="0" cellpadding="0" cellspacing="0">
					<tbody><tr>
						<td style="font-size:12px"><b>Billing Information</b><br>
							Name: <?php if(isset($billing)) echo ucwords($billing['firstname']).' '.ucwords($billing['lastname']); ?><br>
							Phone: <?php if(isset($billing)) echo $billing['contact_no']; ?><br>
							Address1: <?php if(isset($billing)) echo ucwords($billing['address_1']).' '.ucwords($billing['address_2']); ?><br>
							City: <?php if(isset($billing)) echo $billing['city']; ?><br>
							State: <?php if(isset($billing)) echo $billing['state']; ?><br>
							Zip: <?php if(isset($billing)) echo $billing['zip']; ?><br>
							Country: <?php echo ucwords('United States');?>										
						</td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		 		
		<tr>
			<td colspan="3">
				<b>&nbsp;&nbsp;&nbsp;Items</b><br>
				
				<table width="100%" cellpadding="5" cellspacing="0" border="" style="border: 1px solid #ccc!important;">
					<tbody>
					<tr>
							<th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Design Preview</th>
	                        <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Description</th>
	                        <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Specification</th>
	                        <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Quantity</th>
	                        <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Price</th>
	                        <th class="gregular font-regular blackz" style="border: 1px solid #ccc!important;">Total</th>
					</tr>
					
					<?php 
						$totalQty = 0;
						foreach ($get_order_items as $key => $item) {
								$totalQty = $totalQty + $item['quantity'] ;
                                $shirt_color_code = $this->Order_model->get_order_details_item_color( $item['shirt_color_code'] );
                                $image = unserialize( $item['images'] );
                               

                                // $shirtgroup_name =""; 
                                // $garment_type = "";
                             

                                $shirt = $this->Order_model->get_order_shirt_styles2( $item['shirt_style_code'] );
                                $size = explode("-", $item['matrix_code']);
                                $order_item_size = $this->Order_model->get_shirt_size_name( $size[3] );
                                // echo '<pre>';
                                // print_r($shirt);
                                // // print_r($get_order_items);
                                // echo '</pre>';
                               	$image_url = "";
                                $img_ash = base_url("public/".STORE).'/images/ash.jpg';
                                $img_ahe = base_url("public/".STORE).'/images/atheltic-heather.jpg';
                                $img_dh = base_url("public/".STORE).'/images/denim-heather.jpg';
                                $img_ch = base_url("public/".STORE).'/images/charcoal-heather.jpg';
                                if( $shirt_color_code['color_code'] == 'ASH' ){
                                    $image_url = 'url('.$img_ash.')';
                                }else if ($shirt_color_code['color_code'] == 'AHE') {
                                     $image_url = 'url('.$img_ahe.')';
                                }elseif ( $shirt_color_code['color_code'] == 'DHE' ) {
                                    $image_url = 'url('.$img_dh.')';
                                }elseif ( $shirt_color_code['color_code'] == 'CHE' ) {
                                    $image_url = 'url('.$img_ch.')';
                                }
                                ?>
                            <tr>
                                <td style="border: 1px solid #ccc!important; text-align:center!important">
                                    <div class=" img-holder" style="background: #<?php echo $shirt_color_code['hex_value'].' '.$image_url; ?>;">
                                        <?php  
                                           $order_item_image = str_replace("/200/200", "", $image['temp_image']);
                                           $template = $this->Order_model->get_order_shirt_template( $image['template_id'] );  
                                          
                                        ?>
                                        <img class=" img-responsive img-center" src="<?php echo $order_item_image.'/200/200'; ?>" alt="">
                                    </div>
                                </td>
                                <td style="border: 1px solid #ccc!important;">
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Design Name:</span>
                                            <span class="blackz gregular font-small"><?php if(isset($template)) echo $template['display_name']; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">SKU:</span>
                                            <span class="blackz gregular font-small"><?php echo $item['matrix_code']; ?></span>
                                        </div>
                                    </div>
                                  
                                  
                                </td>
                                <td style="border: 1px solid #ccc!important;">
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Style:</span>
                                            <span class="blackz gregular font-small"><?php echo $item['group_name']; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Style Type:</span>
                                            <span class="blackz gregular font-small"><?php echo $item['garment_type']; ?></span>
                                        </div>
                                    </div>

                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Size:</span>
                                            <span class="blackz gregular font-small"><?php if(isset($order_item_size)) echo $order_item_size['shirt_size_name']; ?></span>
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12">
                                            <span class="blackz gsemibold font-small">Color:</span>
                                            <div class="spec-color" style="background: #<?php echo $shirt_color_code['hex_value'].' '.$image_url; ?>; height: 20px;width: 20px; <?php if( $shirt_color_code['color_name'] == "WHITE"){ echo 'border: 1px solid #91999D;';} ?>"></div>
                                        </div>
                                    </div>

                                </td>
                                <td style="border: 1px solid #ccc!important;"><?php if(isset($get_order_items)) echo $item['quantity']; ?></td>
                                <td style="border: 1px solid #ccc!important;">$<?php if(isset($get_order_items)) echo number_format($item['sale_price'],2); ?></td>
                                <td style="border: 1px solid #ccc!important;">$<?php if(isset($get_order_items)) $totalprice = $item['quantity'] * $item['sale_price']; echo number_format($totalprice,2); ?></td>
                            </tr>
                            <?php 
						}
					?>
											
				</tbody></table>
			</td>
		</tr>
		
		<tr><td colspan="3"><br></td></tr>
		<tr>
			<td colspan="2" style="font-size:12px;padding-left:10px"><br>
			
			</td>
			
			<td>
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tbody>
					<tr>
						<td style="font-size:12px">Total Quantity</td>
						<td style="font-size:12px;text-align:right"><?php echo $totalQty ;?></td>
					</tr>
					<tr>
						<td style="font-size:12px">Sub Total</td>
						<td style="font-size:12px;text-align:right">$<?php echo number_format($subtotal,2);?></td>
					</tr>
					<?php 
					if($discount > 0){
					?>
					<tr>
						<!-- <td style="font-size:12px">Discount</td>
						<td style="font-size:12px;text-align:right">-$<?php echo (!empty($discount))? number_format($discount,2) : '0.00'; ?></td> -->
					</tr>
					<?php } ?>
					<tr>
						<td style="font-size:12px">Tax</td>
						<td style="font-size:12px;text-align:right">$<?php echo (!empty($tax))? number_format($tax,2) : '0.00'; ?></td>
					</tr>
					<tr>
						<td style="font-size:12px">Shipping</td>
						<td style="font-size:12px;text-align:right">$<?php echo number_format($shipping_fee,2);?></td>
					</tr>
					<tr>
						<td style="border-top:1px solid #999;font-size:12px">Grand Total</td>
						<td style="border-top:1px solid #999;font-size:12px;text-align:right">$<?php echo number_format($total,2);?></td>
					</tr>
				</tbody></table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<p style="font-size:11px;padding-top:2px;padding-left:10px">scp-v1.0</p>
			</td>
		</tr>
	</tbody></table>
<br> 
	<p></p><p><?php echo ucfirst(STORE);?> reserves the right, in its sole discretion, to cancel the order at any time prior to shipment without liability.</p>
</div>

</div>