<!DOCTYPE html>
<html>
<head>
	<title><?php echo $company_name;?></title>
</head>
<body>

 This is to confirm your payment in the total amount of $<?php echo $total;?> at <a href="<?php echo $_SERVER['HTTP_HOST'];?>"><?php echo $_SERVER['HTTP_HOST'];?></a> on <?php echo $dateprocess;?><br/><br/>
						
Order No.: <?php echo $orderno;?><br/>
Payment Reference No.:<?php echo $checkout_id;?>
<br/>
<br/>
If you need further assistance, contact us at <?php echo $support_email;?>, or would like support with a personal touch, give us a ring at <?php echo $company_phone;?> .<br/><br/>

</body>
</html>