<?php $this->load->view_store('header');  ?> 
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/checkout.css');?>">
<?php
	$order_details = "";
	foreach ($order_confirm as $key => $order) {
		$matrix_code = explode( "-", $order->matrix_code );
		$plan_name = $this->Settings_model->get_shirt_plan_field_by_style_code( 'group_name', $matrix_code[1] );

		$price = ( $order->sale_price > 0 ? number_format( $order->sale_price, 2 ) : number_format( $order->price, 2 ) );

		$order_details .= '<div class="gsemibold font-small gray-dark"><span class="font-small gregular gray-darker">'.$plan_name->group_name.' - </span>$ '.$price.'</div>';
	}
?>
<section class="container">
<div class="orderconf-header">
	<div class="orderconf-header-left"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/circled-check-large.jpg');?>" alt="" /></div>
	<div class="orderconf-header-right">
		<div class="order-trace-number gregular gray-dark font-small">Order Number: <?php echo $order->order_number;?></div>
		<h1 class="order-label gsemibold gray-darker font-large">Thank you, <?php echo $order->cust_firstname;?>!</h1>
	</div>
	<div class="clearfix"></div>
</div>
<div class="orderconf-main-holder">
	<div class="orderconf-first-layer">
		<div class="orderconf-label1 gregular blackz font-large">Your Order Is Confirmed</div>
		<p class="orderconf-p gregular gray-dark font-small">We‘ve accepted your order and we‘re getting it ready.</br>A confirmation has been sent to <?php echo $order->cust_email;?>.</p>
	</div>
	<div class="orderconf-second-layer">
		<!-- <div class="orderconf-second-left">
			<img class="img-responsive img-center shirt-col img-shirt" src="<?php //echo base_url( $shirt_holder['front_image_link'] );?>" alt="" style="background: #<?php //echo $shirt_color_code['hex_value']; ?>;"/>
			<div class="orderconf-design-holder"><img class="img-responsive img-center img-artwork" src="<?php //echo $order_item_image; ?>" alt="" /></div>
		 </div>-->
			<canvas width="500" height="500" id="canvas" style="display:none"></canvas>
		<div class="orderconf-second-left">
			<div class="orderconf-label1 gregular blackz font-large">Spread the Word!</div>
			<p class="orderconf-p gregular gray-dark font-small">Did you enjoy the TZilla experience? Spread the word to friends and family!</p>
		</div>
		<div class="orderconf-second-right">
			<div class="orderconf-icon-holder">
				<span class="icon-facebook share-fb" data-url="<?php echo base_url(); ?>" data-text="" data-via="tzilladotcom"></span>
				<span class="icon-twitter share-twitter" data-url="<?php echo base_url(); ?>" data-text="" data-via="tzilladotcom"></span>
				<span class="icon-gplus share-gplus" data-url="<?php echo base_url(); ?>" data-text="" data-via="tzilladotcom"></span>
				<span class="icon-email share-email" data-url="<?php echo base_url(); ?>" data-text="" data-via="tzilladotcom"></span>
				<!-- <span class="icon-email share-email" data-url="<?php echo base_url(); ?>" data-text="" data-via="tzilladotcom"></span> -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="orderconf-third-layer">
		<div class="orderconf-label1 gregular blackz font-large padd">Customer Information</div> 
		<div class="orderconf-third-left">
			<div class="orderconf-label2 gsemibold blackz font-small">Shipping Address</div><?php if(isset($shipping)) if(!empty($shipping['address_2'])): $add2 = $shipping['address_2'].' '; else: $add2 = ''; endif;  ?>
			<p class="orderconf-p2 gregular gray-dark font-small"><?php if(isset($shipping)) echo $shipping['firstname'].' '.$shipping['lastname'].'</br>'.$shipping['address_1'].' '.$add2.'</br>'.$shipping['city'].' '.$shipping['state'].' '.$shipping['zip']; ?></br>United States <?php if(isset($shipping)) echo $shipping['contact_no']; ?></p>
			<div class="orderconf-label2 gsemibold blackz font-small padd2">Shipping Method</div>
			<div class="gsemibold font-small gray-dark"><span class="font-small gregular gray-darker"><?php echo str_replace("_", " ", $order->shipping_method );?> - </span>$<?php echo $order->shipping_fee;?></div>
		</div>
		<div class="orderconf-third-right">
			<div class="orderconf-label2 gsemibold blackz font-small">Billing Address</div><?php if(isset($billing)) if(!empty($billing['address_2'])): $add2 = $billing['address_2'].' '; else: $add2 = ''; endif;  ?>
			<p class="orderconf-p2 gregular gray-dark font-small"><?php if(isset($billing)) echo $billing['firstname'].' '.$billing['lastname'].'</br>'.$billing['address_1'].' '.$add2.'</br>'.$billing['city'].' '.$billing['state'].' '.$billing['zip']; ?></br>United States <?php if(isset($billing)) echo $billing['contact_no']; ?></p>
			<div class="orderconf-label2 gsemibold blackz font-small padd2">Payment Method</div>
			<div class="gsemibold font-small gray-dark"><span class="font-small gregular gray-darker"><?php echo $order->payment_info;?> - </span>$<?php echo $order->total;?></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div class="orderconf-footer">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a href="<?php echo base_url('school/search'); ?>"><button class="orderconf-btn green-btn white gsemibold font-small">Continue Shopping</button></a>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<a href="<?php echo base_url('campaign/lists'); ?>"><button class="orderconf-btn green-btn white gsemibold font-small">Browse Campaigns</button></a>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view_store('footer');  ?> 

<script src="<?php echo base_url('public/'.STORE.'/js/order-confirmation.js');?>"></script>

<script>
$(document).ready(function(){
	 $("#storetitle").text('TZilla.com - Order Confirmation');    
	 $(document).on("click", ".share-fb", function(e){
		e.preventDefault();

		FB.ui(
		{
			method: 'feed',
			name: "",
			link: '<?php echo base_url() ?>',
			caption: '',
			picture: $(".footer-logo").attr("src"),
			display: 'iframe',
			message: '',
			description: ''
		});

		FB.Canvas.setSize({ width: 200, height: 200 });
	});	

});
</script>