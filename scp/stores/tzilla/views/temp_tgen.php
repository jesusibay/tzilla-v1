<?php $this->load->view_store('header'); ?>
<!-- style -->	
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/createcampaign.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/jquery.countdown.css');?>">
<script src="<?php echo base_url('public/'.STORE.'/js/jquery.plugin.js');?>"></script>
<script src="<?php echo base_url('public/'.STORE.'/js/jquery.countdown.js');?>"></script>

<style type="text/css">
	.campprev-share-icon-holder {
	    position: relative;
	    text-align: left;
	    padding-top: 10px;
	}
	.camp-last-day {
		border-radius: 3px;
		border: none;
	}
	.edit-lbl {
		color: #7fc241;
	}
	.camp-todate {
		margin-bottom: 30px;
	}
	.camp-sidebar-title {
		margin-top: 30px;
		margin-bottom: 30px;
	}
	.design-name {
		letter-spacing: 5px;
	}
	.text-center {
		text-align: center!important;
	}
	.shirt-holder {
		position: relative;
	}
	.camp-design-label {
		padding-bottom: 15px;
	}
	.design-holder {
	    position: absolute;
	    left: 0;
	    right: 0;
	    margin: 0 auto;
	    top: 16%;
	    width: 24%;
	}
	.select-btn {
	    border: 1px solid #7fc241;
	    border-radius: 3px;
	    background: #7fc241;
	    color: #fff;
	    text-align: center;
	    padding: 10px 40px;
	    margin-top: 15px;
	    -moz-transition: all 300ms ease;
	    -webkit-transition: all 300ms ease;
	    -o-transition: all 300ms ease;
	    transition: all 300ms ease;
	    cursor: pointer;
	}
	.camp-design-item-holder {
		margin-bottom: 30px;
	}
	.camp-cta-outer-holder {
		padding-top: 50px;
	}
	.camp-sidebar {
		padding-bottom: 1px;
	}

</style>
<!-- end style  -->
<div class="container-fluid breadcrumb-outer font-xxsmall">
	<ul class="breadcrumb container visib-hidden">
		<li><a href="http://development.tzilla.com/">Home</a></li>
		<li><a href="http://development.tzilla.com/school/search">Search</a></li>
		<li class="school-abbr">Poway High School</li>
		<li>Select Design</li>
		<li class="active">Customize</li>			    
	</ul>
</div>
<section class="container-fluid tgen-bg">
	<div class="container">
		
	</div>
	<div class="gradient"></div>
</section>
<section class="container-fluid">
	<div class="container">
		<div class="row">
			<div class="col-lg-9 camp-cta-outer-holder">
				<div class="row">
					<div class="col-lg-12">
						<div class="gregular font-medium gray-dark"><strong>To get started, select a design below.</strong> By using our exclusive SuperCustomize™ platform, you'll be able to<br> pick product styles and colors as well as personalize the text within the design. Price will vary by product.</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="camp-design-item-holder">
							<div class="camp-design-label gregular font-xsmall gray text-uppercase text-center">
								<span class="design-name gsemibold text-center gray-dark">box drill</span>
							</div>
							<div class="shirt-holder">
								<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/blank-shirt/color1.png');?>">
								<div class="design-holder">
									<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/boxdrill-art.png');?>">
								</div>
							</div>
							<input type="button" class="select-btn gsemibold font-regular" value="Select">
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="camp-design-item-holder">
							<div class="camp-design-label gregular font-xsmall gray text-uppercase text-center">
								<span class="design-name gsemibold text-center gray-dark">stairs</span>
							</div>
							<div class="shirt-holder">
								<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/blank-shirt/color2.png');?>">
								<div class="design-holder">
									<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/boxdrill-art.png');?>">
								</div>
							</div>
							<input type="button" class="select-btn gsemibold font-regular" value="Select">
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="camp-design-item-holder">
							<div class="camp-design-label gregular font-xsmall gray text-uppercase text-center">
								<span class="design-name gsemibold text-center gray-dark">overlay</span>
							</div>
							<div class="shirt-holder">
								<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/blank-shirt/color3.png');?>">
								<div class="design-holder">
									<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/boxdrill-art.png');?>">
								</div>
							</div>
							<input type="button" class="select-btn gsemibold font-regular" value="Select">
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="camp-design-item-holder">
							<div class="camp-design-label gregular font-xsmall gray text-uppercase text-center">
								<span class="design-name gsemibold text-center gray-dark">box drill</span>
							</div>
							<div class="shirt-holder">
								<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/blank-shirt/color1.png');?>">
								<div class="design-holder">
									<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/boxdrill-art.png');?>">
								</div>
							</div>
							<input type="button" class="select-btn gsemibold font-regular" value="Select">
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="camp-design-item-holder">
							<div class="camp-design-label gregular font-xsmall gray text-uppercase text-center">
								<span class="design-name gsemibold text-center gray-dark">stairs</span>
							</div>
							<div class="shirt-holder">
								<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/blank-shirt/color2.png');?>">
								<div class="design-holder">
									<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/boxdrill-art.png');?>">
								</div>
							</div>
							<input type="button" class="select-btn gsemibold font-regular" value="Select">
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
						<div class="camp-design-item-holder">
							<div class="camp-design-label gregular font-xsmall gray text-uppercase text-center">
								<span class="design-name gsemibold text-center gray-dark">overlay</span>
							</div>
							<div class="shirt-holder">
								<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/blank-shirt/color3.png');?>">
								<div class="design-holder">
									<img class="img-responsive img-center shirt-col shirt-image" src="<?php echo base_url('public/'.STORE.'/images/boxdrill-art.png');?>">
								</div>
							</div>
							<input type="button" class="select-btn gsemibold font-regular" value="Select">
							<div class="fb-share-button" data-href="http://development.tzilla.com/template/tempgen" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Share</a></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="camp-sidebar">
				
				  <div class="camp-sidebar-title gsemibold font-medium gray-dark camp-prev-title text-uppercase">About us</div>
				  <p class="camp-sidebar-p gregular font-medium gray-dark camp-prev-sidebar-p">Create a short description for your campaign. Your visitors will read this first, so be sure to include the most relevant information here. Create a short description for your campaign. Your visitors will read this first, so be sure to include the most relevant information here. 
				  <span class="edit-lbl font-medium pointer">(Edit)</span></p>
				  <hr class="camp-devider">
				  <div class="camp-fundraised-title gsemibold font-medium  gray-dark">Funds Raised</div>
				  <div class="gsemibold font-large gray-dark text-uppercase">$1035.20 USD</div>
				  <div class="camp-todate gsemibold font-medium gray-dark">Units Sold</div>
				  <div class="sidebar-loadrail"><div class="sidebar-loadgreen" style="width:65%;"><span class="sidebar-tooltip p-hover" style="display:none;">356</span></div></div>
				  <div class="camp-goal-min gsemibold font-small gray-dark pull-left">0</div>
				  <div class="camp-goal-max gsemibold font-small gray-dark pull-right"><span class="gregular font-small">Goal:</span> 500</div>
				  <div class="clearfix"></div>
				 
				  
				  <hr class="camp-devider1">
				  <!-- <div class="camp-length-title gregular font-medium  blackz">Campaign Duration</div>
				  <div class="camp-last-day gregular font-small gray-dark text-center"></div> -->
				  <div class="camp-lastday-title gregular font-medium blackz">Last Day to Order!</div>
				  <div class="camp-last-day gregular font-small gray-dark text-center">June 7, 2017, 11:59 pm</div>
				  <div class="camp-lastday-title gregular font-medium  blackz">This Campaign ends in</div>
				  <div id="camptimer2" class="gregular font-small gray-dark text-center"></div>
				  <hr class="camp-devider1">
				  <div class="camp-fundraised-title gregular font-medium blackz">Share This Campaign</div>
				  <div class="campprev-share-icon-holder">
						<span class="icon-facebook live-fb"  data-text="" data-via="tzilladotcom"></span>
						<span class="icon-twitter live-twitter"  data-text="" data-via="tzilladotcom"></span>
						<!-- <span class="icon-gplus live-gplus"  data-text="" data-via="tzilladotcom"></span> -->
						<span class="icon-email live-email"  data-text="" data-via="tzilladotcom"></span>
				  </div>
				</div>
			</div>
		</div>
	</div>
</section>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.10";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script src="<?php echo base_url('public/'.STORE.'/js/campaign-live.js').'?df='.rand(1, 99999);?>"></script>
<script>

$(document).ready(function(){
	setDateStarted( $("#start_date").val() );
	setDateEnded( $("#end_date").val() );

	$(function() { 
		setInterval(function(){ doCountDown(); }, 1000);
	});
});
</script>
<?php $this->load->view_store('footer'); ?>