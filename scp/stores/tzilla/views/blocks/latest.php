<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/latest.css');?>">
<section class="latest-section-holder">
	<div class="container clear-padd-left clear-padd-right">
		<div class="row custom-padd">
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-center">
				<div class="text-center"><span class="gregular font-xlarge gray-darker latest-text l-height">School Pride starts here</span><span class="gregular font-xlarge gray-darker latest-text l-height">Super Customize your tee</span></div>
				<a href="<?php echo base_url('school/search'); ?>"><button class="btn btn-primary gsemibold white font-medium latest-btn">Start Personalizing</button></a>
			</div>
		</div>
	</div>
</section>

