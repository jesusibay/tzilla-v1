		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css');?>">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/fonts/font-awesome/css/font-awesome.min.css');?>">
		
		<!-- REVOLUTION STYLE SHEETS -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/css/settings.css');?>">
		<!-- REVOLUTION LAYERS STYLES -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/css/layers.css');?>">
			
		<!-- REVOLUTION NAVIGATION STYLES -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/css/navigation.css');?>">
		
		<!-- Custom STYLES -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/css/banner.css');?>">
		
		<!-- REVOLUTION JS FILES -->
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/jquery.themepunch.tools.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/jquery.themepunch.revolution.min.js');?>"></script>

		<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  
			(Load Extensions only on Local File Systems !  
			The following part can be removed on Server for On Demand Loading) -->	
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.actions.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.carousel.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.kenburn.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.layeranimation.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.migration.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.navigation.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.parallax.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.slideanims.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/'.STORE.'/revolution/js/extensions/revolution.extension.video.min.js');?>"></script>

<!-- SLIDER EXAMPLE -->
<section class="">
	<article class="content">			
		<div class="rev_slider_wrapper">
			<!-- START REVOLUTION SLIDER 5.0 auto mode -->
			<div id="rev_slider" class="rev_slider"  data-version="5.0">
				<ul>	
				<!-- SLIDE  -->
					<li data-transition="fade" data-slotamount="default"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="1000" data-rotate="0"  data-saveperformance="off"  data-title="" data-description="">

					<!-- MAIN IMAGE -->
					<img class="" src="<?php echo base_url('public/'.STORE.'/images/slide-sample.jpg');?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>						

						
						
						
						<!--LAYER 5-->	
						<h2 class="tp-caption gregular white text-center" 
						id="" 

						data-x="['center','center','center','center']" 
						data-hoffset="['0','0','0','0']" 

						data-y="['middle','middle','bottom','bottom']" 
						data-voffset="['120','80','100','90']" 

						data-whitespace="nowrap"

						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;"

						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
						data-start="1400" 
						data-splitin="none" 
						data-splitout="none" 
						data-responsive_offset="on"
						style="line-height: 40px;"
						data-fontsize="['35','35','35','32']">
							
							New Mascot Designs<br>available now!

						</h2>


						<!--LAYER 6-->		
						<div class="tp-caption rev-btn rev-maxround rev-bordered  " 
						id="" 
						data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
						data-y="['middle','middle','middle','middle']" data-voffset="['210','160','140','150']" 
						data-width="none"
						data-height="none"
						data-whitespace="nowrap"
						data-transform_idle="o:1;"
						data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power3.easeOut;"
						data-style_hover="c:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);"

						data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
						data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
						data-mask_out="x:inherit;y:inherit;" 
						data-start="1600" 
						data-splitin="none" 
						data-splitout="none" 
						data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-164","delay":""}]'
						data-basealign="slide" 
						data-responsive_offset="on" 
						data-responsive="off">

							<a href="<?php echo base_url('school/search'); ?>"><button class="gsemibold font-regular super-custom-btn">Super Customization</button></a>

						</div>

					</li>
				</ul>				
				<!--</div><!-- END REVOLUTION SLIDER -->
			</div>
		</div>
	</article>
</section>
	
<!-- CALLING THE SLIDER -->
<script>
	/******************************************
	-	PREPARE PLACEHOLDER FOR SLIDER	-
	******************************************/
	
	var revapi;
	jQuery(document).ready(function() {		
		revapi = jQuery("#rev_slider").revolution({
			sliderType:"standard",
			sliderLayout:"auto",
			delay:3000,
			/* stopLoop:"on",
								stopAfterLoops:0,
								stopAtSlide:1,
								shuffle:"off", */
			navigation: {
				arrows:{enable:true}				
			},	
			minheight:500,
			minwidth:320,
			autoHeight:"on",			
			responsiveLevels: [1680, 1366, 800, 480],
            gridwidth: [1170, 880, 700, 320],
            gridheight: [422, 390, 422, 422],
			
			lazyType:"none",
			
			parallax: {
			type:"mouse",
			origo:"slidercenter",
			speed:2000,
			levels:[2,3,4,5,6,7,12,16,10,50],
			disable_onmobile:"off"
			},
			
			shadow:0,
			spinner:"off",
			disableProgressBar:"on",
			
			navigation: {
				keyboardNavigation:"off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation:"off",
				onHoverStop:"on",
				arrows:{enable:true},
			touch:{
				touchenabled:"on",
				swipe_threshold: 75,
				swipe_min_touches: 1,
				swipe_direction: "horizontal",
				drag_block_vertical: false
				},
			bullets: {
				enable:true,
				hide_onmobile:false,
				hide_under:0,
				style:"zeus",
				hide_onleave:false,
				direction:"horizontal",
				h_align:"center",
				v_align:"bottom",
				h_offset:0,
				v_offset:20,
				space:5,
				tmp:'<span class="tp-bullet-image"></span><span class="tp-bullet-imageoverlay"></span><span class="tp-bullet-title">{{title}}</span>'
				}
			},
			
			debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
		});	
		
	
						
	});	/*ready*/
</script>	