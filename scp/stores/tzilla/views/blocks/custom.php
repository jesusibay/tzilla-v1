<?php $this->load->module('campaign/Campaigns_model');
$this->load->helper('textparser');
$featured = $this->Campaigns_model->get_all_featured_campaigns(); 
if( count( $featured) > 0 ):?>
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/custom.css');?>">
<section class="custom-section-holder"> 
	<div class="container-fluid">
		<div class="container">
			<h1 class="header1 text-center gsemibold font-large gray-darker">SUPERCUSTOMIZE YOUR CAMPAIGN!</h1>
			<h2 class="header2 text-center gregular font-large gray-dark">We supply the kickass designs, then you change the text, color, pattern, and shirt styles, using our trademarked customization engine.</h2>
			<div class="row">
				<?php foreach (array_parseToString( $featured ) as $key => $campaign) { 
					$type = $this->Campaigns_model->get_by_campaign_design_type( $campaign['template_type_id'] );
					$parent = $this->Campaigns_model->get_by_campaign_design_parent( $campaign['template_id'] ); 
					$img = (!empty($campaign['thumbnail1']))? str_replace('/200/200', "",$campaign['thumbnail1']) : str_replace('/200/200', "",$campaign['thumbnail']); ?>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="shirt-design-holder" >
						<div class="design-label gregular font-xsmall gray text-uppercase text-left lbl-overflow">Design: <span class="design-name gsemibold"><?php echo $campaign['display_name']; ?></span></div>
						<div class="campaign-image-holder" style="background:#<?php echo $campaign['shirt_background_color']; ?>;">
							<a href="<?php echo base_url($campaign['campaign_url']); ?>"><img class="img-responsive img-center" src="<?php echo $img.'/200/200'; ?>" alt=""/></a>
						</div>
						<a class="shirt-name-link" href="<?php echo base_url($campaign['campaign_url']); ?>"><h3 class="gsemibold font-medium title-c-height"><?php echo ucwords(ucfirst($campaign['campaign_title'] )); ?></h3><span><?php echo db_toString( $campaign['school_name'] ); ?></span></a>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<?php endif;?>
