<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/featured.css');?>">
<section class="featured-section-holder">
	<div class="container-fluid ">
	<div class="row">
		<div class="col-lg-6 lpad">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 f-bg1"></div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
					<div class="text-center gregular font-medium gray-dark text-padd">Finally, Class T-shirts that don't suck</div>
					<a href="<?php echo base_url('school/search'); ?>"><button class="btn btn-default gsemibold white font-medium start-personalizing-btn">Start Personalizing</button></a>
				</div>
		</div>	
		
		<div class="col-lg-6 rpad">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 f-bg2"></div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
					<div class="text-center gregular font-medium gray-dark text-padd">Designs that deserve your selfie</div>					
					<a href="<?php echo base_url('school/search'); ?>"><button class="btn btn-default gsemibold white font-medium start-personalizing-btn">Start Personalizing</button></a>
			</div>
			
		</div>
		
	</div>
	</div>
</section>