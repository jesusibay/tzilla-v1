<?php $this->load->view_store('header');  ?>

<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/productsizechart.css');?>">
<link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/school-store.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/css/settings.css');?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/'.STORE.'/revolution/css/layers.css');?>">

<div class="container-fluid bg-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <h1 class="white">Products</h1>
                <p class="gray">Simply get a shirt for yourself, or create a campaign<br>to raise funds for your school</p>
            </div>
        </div>
    </div>
</div>
<?php $style = ($styleID == 0)? "" : $styleID; ?>
<div class="container-fluid">

	<div class="row store-design-header">
		<div class="col-lg-12">
			<h2 class="text-center gregular font-large blackz">Product Specs</h2>
			<div class="row" style="padding-top: 20px;">
				<div class="container">
					<div class="mob-menu navbar navbar-default" role="navigation">
					    <div class="navbar-header">
					        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
					            <span class="sr-only">Toggle navigation</span>
					            <span class="glyphicon glyphicon-triangle-bottom"></span>
					        </button>
					        <!-- <a href="#" class="mob-selector navbar-brand gregular">Select Category</a> -->
					        <a class="mob-selector navbar-brand gregular">Select Category</a>
					    </div>
					    <div class="collapse navbar-collapse" id="navbar-collapse">
							<ul class="nav nav-tabs store-nav" style="border-bottom: none;">
								<li class="dropdown category-selection-tab" data-id="0"><a href="#holders">All Categories</a></li>
								<?php
									foreach ($category as $key => $value) {
										// echo base_url('settings/size_specifications').'/'.$styleID.'/'.$value->shirt_group_id;
								?>
								<li class="dropdown category-selection-tab" data-id="<?php echo $value->shirt_group_id; ?>">
									<a href="#holders" class="dropdown-toggle" href="#" ><?php echo $value->shirt_group_name; ?></a>
								</li>
								<?php
									}
								?>
							</ul>    
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row store-design-holder-outer">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div id="holders" class="product-holder">
						<div class="row specification-preview">
						<!--  -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal sizechart-->
<div id="sizechart" class="modal fade" role="dialog">
  	<div class="modal-dialog cantfind-dialog" style="width: 671px;">

	    <!-- Modal content-->
	    <div class="modal-content sizechart-modal-holder">
	      	<div class="confirm-header white gregular font-medium">
				<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
		  	</div>
		  	<div class="container-fluid modal-bg">
					<div class="row">
						<div class="col-lg-12 text-center measure-pad ">
							
							<div class="sizechart-label1 gregular font-large blackz text-center sizechart-lbl">Size Chart for <span class="style-label">Style Name</span></div>
							
							<div class="cont-holder size-chart-info">
								<p class="gray gregular font-small text-left ">Classic:<br>
By far our best selling tee. Heavyweight, long-lasting, and preshrunk to minimize shrinkage. This style of shirt sports a fit which hangs loosely off of the body. 100% Cotton, 5-6oz. Check out the specs for the actual measurements and dimensions before you order.</p>
							</div>
							<div class="table-responsive size-chart-data"> 
								<table class="table table-bordered text-left font-small size-chart-table">
									<thead>
										<tr>
											<th class="gsemibold blackz">Body Specs</th>
											<th class="gsemibold blackz">S</th>
											<th class="gsemibold blackz">M</th>
											<th class="gsemibold blackz">L</th>
											<th class="gsemibold blackz">XL</th>
											<th class="gsemibold blackz">2XL</th>
											<th class="gsemibold blackz">3XL</th>
											<th class="gsemibold blackz">Tolerance</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="gregular blackz">Body Width</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Body Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-1”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Sleeve Length</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
										<tr>
											<td class="gregular blackz">Arm hole</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>00.00</td>
											<td>+/-5/8”</td>
										</tr>
									</tbody>
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var xhr;
	var a = 0;
	var sizeData = [];
	var activeCategory = "";

	function setSizeData( value ) {
		sizeData = value;
	}

	function getSizeData() {
		return sizeData;
	}

	function setActiveCategory( value ) {
		activeCategory = value;
	}

	function getActiveCategory() {
		return activeCategory;
	}

	function loadSizeData( toDo = "", shirt_id = "" ) {

		if(xhr && xhr.readystate != 4){
			xhr.abort();
		}

		// lets submit the form
		xhr = $.ajax({
			type: 'POST',
			url: get_mainLink()+'settings/get_size_chart_details'+( shirt_id.length > 0 ? '/'+shirt_id : ''),
			success: function(data){
				var nData = JSON.parse(data);

				if( nData.length > 0 ){
					setSizeData( nData );

					// console.log(toDo + " == " + nData);

					if ( toDo == 'populate' ) {
						console.log("sa populate");
						populateSpecifictionDetails();
					} else if ( toDo == 'chart' ) {
						console.log(" sa chart");
						populateChartDetails( shirt_id );
					}

				}
			},
			async:true
		});
	}

	function populateChartDetails( shirt_id = 0 ) {
		$("#sizechart").modal('show');
		var shirtData = getSizeData(); // JSON.parse(getSizeData());

		$(".style-label").html($('.display-shirts[data-id="'+shirt_id+'"]').find(".shirt-title").html());

		$('.size-chart-table thead tr').html($("<th />").addClass("gsemibold blackz").attr("data-code", "").text("Body Specs"));
		$('.size-chart-table tbody tr').html("");

		for ( var x = 0; x < shirtData.length; x++ ) {
			if ( shirtData[x]['id'] == shirt_id ) {
				$(".style-label").text(shirtData[x]['style_name']);
				$(".size-chart-info").html($("<p />").addClass("gray gregular font-small text-left").text(shirtData[x]['style_description']));

				var sizeString1 = shirtData[x]['shirt_size_data'].replace("{","").replace("}","");
				var sizeArray1 = sizeString1.split(",");

				for ( var y = 0; y < sizeArray1.length; y++ ) {
					var strArr = sizeArray1[y].split(":");
					if ( $('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table thead tr').append($("<th />").addClass("gsemibold blackz").attr("data-code", shirtData[x]['shirt_size_code']).text(shirtData[x]['shirt_size_code']));
						}
					}	
					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq(0)').size() == 0 ) {
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").addClass("gregular blackz").text(strArr[0]));
						}
					}

					if ( $('.size-chart-table tbody tr:eq('+y+') td:eq('+$('.size-chart-table thead tr th[data-code="'+shirtData[x]['shirt_size_code']+'"]').index()+')').size() == 0 ) {						
						if(strArr[1] != "")
						{
							$('.size-chart-table tbody tr:eq('+y+')').append($("<td />").text(strArr[1]));	
						}
						
					}
				}
			}
		}
	}

	function populateSpecifictionDetails() {
		var shirtData = getSizeData(); // JSON.parse(getSizeData());
		var sizeIndex = 0;
		var sizeRange = "";
		var previousShirt = '';

		if ( shirtData.length > 0 ) {
			$(".specification-preview").html("");

			for ( var a = 0; a < shirtData.length; a++ ) {
				if ( shirtData[a]['style_name'] != previousShirt ) {
					$("<div />").addClass("col-lg-3 col-md-3 col-sm-6 col-xs-12 display-shirts").attr({"data-id":shirtData[a]['id'],"style-id":shirtData[a]['shirt_style_id'],"data-style":"0","data-category":shirtData[a]['shirt_group_id']}).css("min-height","500px").appendTo(".specification-preview").append($("<div />").addClass("design-item-holder camp-item cont-item").append($("<img />").attr({"class":"img-responsive img-center shirt-col","src":get_mainLink()+shirtData[a]['thumbnail'],"alt":""})).append($("<a />").attr({"class":"shirt-name-link","href":"#"}).append($("<h3 />").addClass("gregular font-medium-2 gray-darker shirt-title").text(shirtData[a]['style_name'])).append($("<span />").text(sizeRange)))).append($("<div />").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12").append($("<button />").attr({"class":"orderconf-btn green-btn white gsemibold font-small view-size-chart"}).text("View Size Chart")));
				}

				previousShirt = shirtData[a]['style_name'];
			}
		}			
		 	
	}

	$( window ).load(function() {
		var style_id = '<?php echo $style; ?>';
		setTimeout(function() { $('.display-shirts[style-id="'+style_id+'"]').find(".view-size-chart").click(); }, 1000);
	});
	
	$(document).ready(function(){
		$(function(){
		 	$('.left-slide').hide();
		 	$('.store-nav .dropdown[data-id="'+getActiveCategory()+'"]').addClass("active");
		 	loadSizeData( 'populate', getSizeData() );
		 	$("#storetitle").text('TZilla.com - Size Specifications');
		});

		$(document).on("click", ".view-size-chart", function(e){
			var shirt_style_id = $(this).parent().parent().attr("data-id");
			populateChartDetails( shirt_style_id );
		});

		$(document).on("click", ".category-selection-tab", function(){
			$('.store-nav .dropdown').removeClass("active");

			if ( $(this).attr("data-id") == 0 ) {
				$(".display-shirts").show();
			} else {
				$(".display-shirts").hide();
				$('.display-shirts[data-category="'+$(this).attr("data-id")+'"]').show();
			}

			$(this).addClass("active");
		});

	    $('.right-slide').click(function(event) {
		    var pos1 = $('.prod-holder').scrollLeft() + 550;
		    $('.prod-holder').scrollLeft(pos1);
		    $('.left-slide').show();
		});

		$('.left-slide').click(function(event) {
		    var pos2 = $('.prod-holder').scrollLeft() - 550;
		    $('.prod-holder').scrollLeft(pos2);
		});

		$("a").on('click', function(){
		    $('#navbar-collapse').collapse('hide');
		});

	});

</script>

<script type="text/javascript">
	setActiveCategory( '<?php echo $inventoryGroupID; ?>' );
</script>

<?php $this->load->view_store('footer'); ?>