<!DOCTYPE html>
<html lang="en">
<head>
  <title id="storetitle">Tzilla.com</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/fonts/stylesheet.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/fonts/tzilla-icons.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/owl.carousel.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('public/'.STORE.'/css/main.css');?>">
  <script src="<?php echo base_url('public/'.STORE.'/js/jquery-1.9.1.min.js');?>"></script>
  <script src="<?php echo base_url('public/'.STORE.'/js/bootstrap.min.js');?>"></script>
  <script src="<?php echo base_url('public/'.STORE.'/js/owl.carousel.js');?>"></script>
  <script src="<?php echo base_url('public/'.STORE.'/js/art-positioning.js').'?df='.rand(1, 99999);?>"></script>
    <script type="text/javascript">
      var verifyCallback = function(response) {
      };
      var gregister;
      var onloadCallback = function() {
        tgengregister = grecaptcha.render('tgen_register_element', {
          'sitekey' : '6LfkVxkUAAAAAJ9P3_Kzq8T6N4Me2F8DiE4LyCGd',
          'theme' : 'light'
        });

      };
    </script>
</head>
<body>
<div class="container-fluid header-outer-holder">

<nav class="navbar navbar-inverse">
  <div class="container header-inner-holder">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span> 
		  </button>
		  <a class="navbar-brand header-logo" href="<?php echo base_url(); ?>">
				<img class="img-responsive logo1" src="<?php echo base_url('public/'.STORE.'/images/tzilla-logo.png');?>" alt=""/>
				<img class="img-responsive logo2" src="<?php echo base_url('public/'.STORE.'/images/tzilla-logo-2.png');?>" alt=""/>
		  </a>
		</div>
		</div>
		
		<div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
		<div class="collapse navbar-collapse" id="myNavbar" style="overflow: hidden;">
		  <ul class="inner-nav nav navbar-nav font-regular">
			<li class="<?php echo ($this->uri->segment(2) == 'store')? 'white-trigger' : 'gray-darker-trigger';?>"><a href="javascript:void(0);"><span class="inner-nav-icon <?php echo ($this->uri->segment(2) == 'store')? 'nav-active' : "";?>">1</span>Select Designs</a></li>
			<li class="<?php echo ($this->uri->segment(2) == 'customize')? 'white-trigger' : 'gray-darker-trigger';?>"><a href="javascript:void(0);"><span class="inner-nav-icon <?php echo ($this->uri->segment(2) == 'customize')? 'nav-active' : "";?>">2</span><span class="second-step">Customize</span></a></li>
			<li class="laster <?php echo ($this->uri->segment(2) == 'review' || $this->uri->segment(2) == 'create' )? 'white-trigger' : 'gray-darker-trigger';?>"><a href="javascript:void(0);"><span class="inner-nav-icon <?php echo ($this->uri->segment(2) == 'review' || $this->uri->segment(2) == 'create')? 'nav-active' : "";?>">3</span><?php echo ( $this->uri->segment(2) == 'review')? 'Buy' : '<span class="third-step">Sell<span>'; ?></a></li> 
	
			<?php if($this->session->is_logged_in==FALSE){ ?>
				<li class="dropdown small-dev"><a class="dropdown-toggle" data-toggle="modal" data-target="#loginmodal">Sign in/Register</a></li>
			<?php }else{ ?>
				<li class="dropdown small-dev">
					<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);"><?php echo $this->session->logged_in_email; ?>
					<span class="caret"></span></a>
					<ul class="dropdown-menu">
					  <!--<li><a href="<?php echo base_url('account-campaigns'); ?>">Campaigns</a></li>-->
					  <li><a href="<?php echo base_url('account-dashboard'); ?>">Account Dashboard</a></li>
					  <li><a class="logout_fb" href="<?php echo base_url('signin/signout'); ?>">Log Out</a></li> 
					</ul>
				</li>
			<?php } ?>
		  </ul>
		</div>
		</div>
		
		<div class="col-lg-4 col-md-3 col-sm-4 col-xs-12" style="padding-left: 0;">
		<div class="large-dev header-dropdown-holder">
			
			<div class="dropdown header-nav-email">
				<span class="btn btn-default dropdown-toggle header-dropdown" type="button" id="menu1" data-toggle="dropdown">
				<?php if($this->session->is_logged_in==FALSE){ ?>
				<span class="icon-user-icon" data-toggle="modal" data-target="#loginmodal"></span><span data-toggle="modal" data-target="#loginmodal" class="sign-in user-email mob-text font-xxsmall">Sign in/Register</span>
				<?php }else{ ?>
				<span class="icon-user-icon"></span><span class="user-email  font-xxsmall"><?php echo $this->session->logged_in_email; ?></span>
				<span class="caret"></span></span>
				<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
				  <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('account-campaigns'); ?>">Campaigns</a></li>-->
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('account-dashboard'); ?>">Account Dashboard</a></li>
				  <li role="presentation"><a role="menuitem" tabindex="-1" href="<?php echo base_url('signin/signout'); ?>">Log Out</a></li>
				</ul>
				<?php } ?>
			</div>
		</div>
		</div>
		
	</div>
  </div>
</nav>


<div class="container-fluid breadcrumb-outer font-xxsmall">
	<ul class="breadcrumb container visib-hidden">
			<li><a href="<?php echo base_url(); ?>">Home</a></li>
			<li><a href="<?php echo base_url().'school/search'; ?>">Search</a></li>
			<li class="school-abbr"><?php echo $this->session->school_name; ?></li>
			<li >Select Design</li>
			<?php echo ($this->uri->segment(2) == 'customize')? '<li class="active">Customize</li>' : "";?>
			<?php echo ($this->uri->segment(2) == 'create')? '<li class="active">Campaign Creation</li>' : "";?>
    </ul>
</div>

</div>

<!-- Modal -->
<div id="loginmodal" class="modal fade" role="dialog">
  <div class="vertical-alignment-helper">
	  <div class="modal-dialog vertical-align-center">

		<!-- Modal content-->
		<div class="modal-content login-modal-holder">
		  <div class="login-modal-header white gregular text-center font-medium">			
			<span class="header-title">Please Log in or Register to Continue</span>
			<span type="button" class="close icon-circled-x pull-right font-large login-close" data-dismiss="modal"></span>
		  </div>

			<div class="container-fluid login-box">
				<div class="row">
					<div class="col-lg-6 text-center login-left-col">
						<img class="img-responsive img-center facebook-icon " src="<?php echo base_url('public/'.STORE.'/images/facebook-icon.jpg');?>">
						<div class="white text-center gregular font-regular gray-darker font-medium fast-label">The Fastest & Easiest Way<br>to Log in!</div>
						<button id="login_fb" class="btn btn-primary gsemibold white font-regular facebook-login-btn text-uppercase">SIGN IN USING FACEBOOK</button>
						<span class="text-center text-uppercase gsemibold font-regular gray-darker circle-or">or</span>
					</div>
					<div class="col-lg-6 login-right-col">
						<div class="form-group" style="margin-bottom:10px;">
						  <label class="gsemibold font-regular blackz label1" for="">Log in to TZilla.com</label>
						  <input type="text" class="form-control gitalic font-small gray handle_enter text-field" id="username" data-enter="login_btn" placeHolder="Username">
						</div>
						<div class="form-group">
						  <input type="password" class="form-control gitalic font-small gray handle_enter text-field" id="password" data-enter="login_btn" placeHolder="Password">
						  <label class="forgotpass gsemibold font-regular gray label2" for="">Forgot password?</label>
						</div>
						<div class="btn-green-holder">
							<button id="login_btn" class="btn btn-primary gsemibold white font-regular text-uppercase login-btn">LOG IN NOW</button>
							<button class="btn btn-primary gsemibold white font-regular text-uppercase reg-btn">Register</button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="container-fluid reg-box">
				<div class="gsemibold font-regular blackz" for="">Register to TZilla.com</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group reg-marg handler">
						  <input id="reg_fname" type="text" class="form-control gitalic font-small gray text-input text-field reg-required name" data-name="First Name" placeHolder="First Name">
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="form-group reg-marg handler">
						  <input id="reg_lname" type="text" class="form-control gitalic font-small gray text-input text-field reg-required name" data-name="Last Name" placeHolder="Last Name">
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group handler">
						<input id="reg_email" type="text" class="form-control gitalic font-small gray reg-marg text-field reg-required" data-name="Email Address" placeHolder="Email">
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group handler">
						<input id="reg_pword" type="password" class="form-control gitalic font-small gray reg-marg text-field reg-required" data-name="Password" placeHolder="Password">
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group handler">
						<input id="reg_con" type="password" class="form-control gitalic font-small gray reg-marg text-field reg-required" data-name="Confirm Password" placeHolder="Confirm password">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="reg-left">
							<div id="tgen_register_element"></div>
							<span class="validation-error-reg"></span>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
						<div class="reg-right">
						<button id="register_btn" class="orange-btn gsemibold white font-regular text-uppercase">Register
							<span class="register-loader">
								<img class="img-responsive img-loader" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt=""/>
							</span>
						</button>
						<a href="javascript:void(0);" class="reg-goback gregular blackz font-regular">Go back to login instead</a>
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="container-fluid forgotpass-box text-center">
				<div class="gsemibold font-regular blackz text-center" for="">Enter your email</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group forgot-marg handler">
						  <input id="forgotemail" type="email" class="form-control gitalic font-small gray text-field require-forgotemail handle_enter" data-enter="forgot_password_btn" data-name="Email" placeHolder="Email">
						  <input id="resetpassword_userID" type="hidden" value="<?php if(isset($resetpassword_userID)) echo $resetpassword_userID; ?>" data-id="<?php if(isset($resetuserID)) echo $resetuserID; ?>">
						</div>
						<div class="font-regular gregular gray-dark text-center">You’ll receive an email containing a link to reset your password.</div>
						<div class="notif font-regular gregular green text-center sent-notif"></div>
						<button id="forgot_password_btn" class="orange-btn gsemibold white font-regular text-uppercase sendemail-btn">Send Password Reset Link</button>
						<a href="javascript:void(0);" class="reg-goback gregular blackz font-regular">Go back to login</a>
					</div>
				</div>
			</div>
			
			<div class="container-fluid enterpass-box text-center">
				<div class="gsemibold font-regular blackz text-center" for="">Enter your new password</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="form-group forgot-marg handler">
						  <input id="forgot_newpassword_text" type="password" class="form-control gitalic font-small gray text-field require-forgotpass handle_enter" data-enter="forgot_password_confirm_btn" data-name="New Password" placeHolder="New Password">
						</div>
						<div class="form-group forgot-marg handler">
						  <input id="forgot_conpassword_text" type="password" class="form-control gitalic font-small gray text-field require-forgotpass handle_enter" data-enter="forgot_password_confirm_btn" data-name="Confirm Password" placeHolder="Confirm Password">
						</div>
						
						<div class="notif font-regular gregular green text-center password-notif"></div>
						<button id="forgot_password_confirm_btn" class="orange-btn gsemibold white font-regular text-uppercase reset-btn">Reset My Password</button>					
					</div>
				</div>
			</div>
			
			 <div class="container-fluid buy-create">
				<div class="row">
					<div class="createorbuy-left text-center">
							<div class="createorbuy-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/buy-now.png');?>" alt="" /></div>
							<div class="createorbuy-label-big gsemibold font-xlarge gray-darker text-center">BUY<br>NOW</div>
							<div class="createorbuy-label1 glight font-regular gray-darker text-center">Buy your customized garments.<br></div>
							<button class="orange-btn white gsemibold font-small text-uppercase startbuy-btn btn-goto-tgen" data-code="BUY">Start Now</button>
						
					</div>
					<div class="createorbuy-right text-center">
							<div class="createorbuy-img-holder"><img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/create-a-campaign-circle.png');?>" alt="" /></div>
							<div class="createorbuy-label-big gsemibold font-xlarge gray-darker text-center">CREATE A<br>CAMPAIGN</div>
							<div class="createorbuy-label2 glight font-regular gray-darker text-center">Use your designs to create a <br>fundraiser for your school.</div>
							<button class="green-btn white gsemibold font-small text-uppercase startcreate-btn btn-goto-tgen" data-code="CREATE">Start Now</button>						
					</div>
				</div>
			</div>

			<!-- acct activation -->
			<div class="container-fluid account-activate">
				<div class="row">
					<div class="check-holder">
						<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/large-circled-check.jpg');?>">
					</div>
					<div class="gsemibold font-medium blackz text-center cont-pad-bot2">Account has been successfully activated!</div>
					
				</div>
			</div>

			<!--  -->

			<!-- success register -->
			<div class="container-fluid success-register">
				<div class="row">
					<div class="col-lg-12 text-center">
						<div class="check-holder">
							<img class="img-responsive img-center" src="<?php echo base_url('public/'.STORE.'/images/large-circled-check.jpg');?>">
						</div>
						<div class="gsemibold font-medium blackz text-center cont-pad-bot">Thank you for registering with TZilla.
						<br>
							We've sent you an email to activate  your account.
						</div>
					</div>
				</div>
			</div>
			<!--  -->
			
		</div>

	  </div>
  </div>
</div>

<script src="<?php echo base_url('public/'.STORE.'/js/inner-header.js');?>"></script>
 <?php 
if($_SERVER['HTTP_HOST'] == 'http://development.tzilla.com/'){
	$fb_app_id = '154427225078186';
}else{
	$fb_app_id = '154427225078186';
}
?>
<script>
window.fbAsyncInit = function() {
	//SDK loaded, initialize it
	FB.init({
		appId      : '<?php echo $fb_app_id;?>',
		xfbml      : true,
		version    : 'v2.7'
	});
 
 	<?php 
 	if($this->session->is_logged_in == TRUE){ ?> //check login
		//check user session and refresh it
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				//user is authorized		
			} else {
				//user is not authorized
			}
		});

	<?php  }  ?> //check login
}; 
//load the JavaScript SDK
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>

<script type="text/javascript">	
	var fb_xhr;	
	function getUserData() {
		FB.api('/me', {fields: 'name,last_name,first_name,middle_name,email,gender'},function(response) {
			//ajax save user data if not yet existing
			var user_data = {
				name: response.name,
				last_name: response.last_name, 
				first_name: response.first_name,
				email: response.email, 
				gender: response.gender,
				middle_name: response.middle_name };

			if(fb_xhr && fb_xhr.readyState != 4){
	            fb_xhr.abort();
	        }

	        fb_xhr = $.ajax({
					type: "POST",
					url: "<?php echo site_url('signin/fb_login/setlogin'); ?>",
					data: user_data,
					dataType:'json'
				}).done(function( data ) {
					console.log(data.message);
					if( data.stat == 'success'){
						sendDetailsToCampaign();
					}
				});
		});

		FB.api(
	    "/me/picture",
	    function (response) {
	      if(response && !response.error) {
	        /* handle the result */
	        console.log(response);
	      	}
	    });
	}

	$(window).load(function() {
	    var comment_callback = function(response) {
	        console.log("comment_callback");
	        console.log(response);
	    }
	    FB.Event.subscribe('comment.create', comment_callback);
	    FB.Event.subscribe('comment.remove', comment_callback);
	});

	$(document).ready(function(){
		set_mainLink( '<?php echo base_url(); ?>' );
		set_loggedin('<?php echo ($this->session->is_logged_in == TRUE)? 1 : 0; ?>');

	$(document).on("click", "#login_fb", function(e){
		e.preventDefault();		
		FB.login(function(response) { //do the login
			console.log(response);
			if (response.authResponse) {
				//user just authorized your app
				getUserData();
			}
		}, {scope: 'email,public_profile', return_scopes: true});
	});

	$(document).on("click", "#fb_share_page", function(e){
		e.preventDefault();

		FB.ui(
		{
			method: 'feed',
			name: "",
			link: '<?php echo base_url() ?>',
			caption: '',
			picture: "",
			display: 'iframe',
			message: '',
			description: ''
		});

		FB.Canvas.setSize({ width: 200, height: 200 });
	});		
		
		$('[data-toggle="tooltip"]').tooltip(); 
		
		$('.owl-carousel').owlCarousel({
			
				animateOut: 'fadeOut',
				animateIn: 'flipInY',
				addClassActive: true,
				items:1,
				loop:false,
				nav:false,
				margin:10,
				autoplay:false,
				autoplayHoverPause:true,
				responsiveClass:true,
				responsive:{
					0:{
						items:2
					},
					991:{
						items:2
					},
					1000:{
						items:2
					}
				}
		});
		
		$('.carouselHolder .item').click(function(){
			$('.carouselHolder .item').removeClass("activez");
			$(this).addClass("activez");
		});
		
		$(".owl-next").removeClass("icon-arrow-right-2").addClass("icon-arrow-right-2").html("");
		$(".owl-prev").removeClass("icon-arrow-left-2").addClass("icon-arrow-left-2").html("");
		
		$('.zoom').click(function(){
			$('.zoom').toggleClass("icon-circled-plus").toggleClass("icon-circled-minus");
		});
		
		$(document).on("click", ".design-select", function(){
		  var check = $(this).hasClass("design-active");
		  if(check==false){
			  $(this).addClass("design-active");
		  }else{
			  $(this).removeClass("design-active");
		  }
		});
	  
	  
		$(document).on("click", ".style-check", function(){
		  var check = $(this).hasClass("style-check-active");
		  if(check==false){
			  $(this).parent().addClass("addmorestyle-shirt-holder-active");
			  $(this).addClass("style-check-active");
		  }else{
			  $(this).parent().removeClass("addmorestyle-shirt-holder-active");
			  $(this).removeClass("style-check-active");
		  }
		});


		$(document).on("click", ".style-remove", function(){
			$(this).parent().removeClass("addmorestyle-shirt-holder-active");
			$(this).parent().find(".style-check").removeClass("style-check-active");	
		});

		$(document).on("click", ".btn-goto-tgen", function(){
			var redirect_to = $(this).attr('data-code'); 
			set_directlink( redirect_to.toLowerCase() );
		});
	  
	
	});
	
</script>
