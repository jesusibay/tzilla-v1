<?php $this->load->view_store('account-header');  ?> 
<style type="text/css">
    .sort-order{
        cursor: pointer;
    }
</style>
<?php 
function sorting( $order ) {
        switch ( $order ) {
            case 'ASC':
                return "down";
                break;
            default:
                return "up";
                break;
        }
    }
    ?>
    <div class="dash-title">
        <h3 class="title">My Orders</h3>
    </div>
    <section class="section">
        <div class="row sameheight-container">
            <div class="col-lg-6 col-md-12 tracking-cont" style="display:none;">
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <div class="title-block">
                                <h3 class="title">Track Your Order</h3>
                            </div>
                        </div>
                    </div>
                   <div class="row form-group">
                        <div class="col-md-6 col-xs-12">
                            <label for="">Enter the tracking number sent to your email</label>
                            <!-- <input type="text" class="form-control handle_enter" id="tracking_number" placeholder="eg. 00001114401" value="" data-enter="tracking_btn" onkeypress="return isNumberKey(event)"> -->
                            <input type="text" class="form-control handle_enter" id="tracking_number" placeholder="eg. 00001114401" value="" data-enter="tracking_btn" onkeypress="return isNumberKey(event)">
                        </div>
                        <div class="col-md-offset-1 col-md-5 col-xs-12">
                            <button id="tracking_btn" class="orange-btn cartrev-update-btn white gsemibold font-small margin-btn">Track</button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-title-block">
                            <!-- <h3 class="title">Responsive simple</h3> -->
                        </div>
                        <section class="example">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="gregular font-regular blackz">Date <i class="fa fa-angle-<?php if($sort_by == 'order_date'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-order" data-table="order_date"></i></th>
                                            <th class="gregular font-regular blackz">Reference # <i class="fa fa-angle-<?php if($sort_by == 'order_number'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-order" data-table="order_number"></i></th>
                                            <th class="gregular font-regular blackz"># of Items <i class="fa fa-angle-<?php if($sort_by == 'quantity'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-order" data-table="quantity"></i></th>
                                            <th class="gregular font-regular blackz">Total Price <i class="fa fa-angle-<?php if($sort_by == 'total'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-order" data-table="total"></i></th>
                                            <th class="gregular font-regular blackz">Status <i class="fa fa-angle-<?php if($sort_by == 'order_status'): echo sorting($sort_order); else: echo sorting($sort_by); endif;?> sort-order" data-table="order_status"></i></th>
                                            <th class="col-md-1"></th>
                                            <th class="col-md-1"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if( count($orders) > 0){ 
                                            foreach ($orders as $key => $order) { ?>
                                        <tr>
                                            <td><?php echo date("m-d-Y", strtotime($order->order_date)); ?></td>
                                            <td><?php echo $order->order_number; ?></td>
                                            <td><?php echo $order->totalquantity; ?></td>
                                            <td>$<?php echo $order->total; ?></td>
                                            <td><?php echo $order->order_status; ?></td>
                                            <td>
                                                <a href="<?php echo base_url('account/order').'/'.$order->order_id; ?>"><button data-id=""class="green-btn cartrev-update-btn white gregular font-small dashb-btn">View Details</button></a>
                                            </td>
                                            <td>
                                                <button data-id="cancel-my-order" class="gray-btn gregular font-regular white dashb-btn" disabled>Cancel Order</button>
                                            </td>
                                        </tr>

                                        <?php } 
                                                }else{?>
                                               <tr><td colspan="7" align="center"> No records yet.</td></tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="form-group text-center">
        <div class="row">
            <nav aria-label="Page navigation example">
               <?php echo $this->pagination->create_links();?> 
            </nav>
        </div>
    </div>
</article>
<script type="text/javascript">
$(document).ready(function(){
    $("#storetitle").text('TZilla.com - Account Orders');
});
</script>