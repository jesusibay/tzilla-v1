<?php $this->load->view_store('account-header');  ?> 
<?php foreach ($getdata as $key => $cust) {  } ?>
<input type="hidden" id="loginvia" value="<?php echo $cust->loggedin_via; ?>">
    <div class="dash-title">
        <h3 class="title">Profile Settings</h3>
    </div>
    
    <section class="section">        
        <div class="row sameheight-container">
            <div class="col-md-6">
                <div class="card card-block sameheight-item">
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label for="exampleInputEmail1">First Name</label>
                                <input type="text" name="fname" class="form-control text-input required name handle_enter" data-enter="update_profile_btn" id="cust_fname" value="<?php echo $cust->cust_firstname; ?>" placeholder="First Name">
                            </div>
                            <div class="col-xs-6">
                                <label for="exampleInputEmail1">Last Name</label>
                                <input type="text" name="lname" class="form-control text-input required name handle_enter" data-enter="update_profile_btn" id="cust_lname" value="<?php echo $cust->cust_lastname; ?>" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label for="exampleInputEmail1">Phone Number</label>
                                <input type="text" name="phone" class="form-control required handle_enter" data-enter="update_profile_btn" id="cust_contact" value="<?php echo $cust->cust_contact_no; ?>" placeholder="(xxx) xxx-xxxx" onkeypress="return isNumberKey(event)">
                            </div>
                            <div class="col-xs-6">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" id="cust_email" value="<?php echo $cust->cust_email; ?>" readonly placeholder="Email">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <label for="exampleInputEmail1">Address</label>
                                <input type="text" name="address" class="form-control required handle_enter" data-enter="update_profile_btn" id="cust_add" value="<?php echo $cust->cust_address_1; ?>" placeholder="Address">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <label for="exampleInputEmail1">City</label>
                                <input type="text" name="city" class="form-control text-input required handle_enter" data-enter="update_profile_btn" id="cust_city" value="<?php echo $cust->cust_city; ?>" placeholder="City">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-4">
                                <label for="exampleInputEmail1">Country</label>
                                <input type="text" name="country" class="form-control" id="" placeholder="Country" value="United States" readonly>
                            </div>
                            <div class="col-xs-4">
                                <label for="exampleInputEmail1">State</label>
                                   <select class="form-control required" name="state" id="cust_state">
                                        <option value="">Select a State</option>
                                        <?php foreach ($states as $key => $val) {?>
                                            <option data-value="<?php echo $val['state_name'];?>" <?php if($getdata): if( $val['state_code'] == $cust->cust_state ){echo 'selected="selected"';} endif; ?> value="<?php echo $val['state_code'];?>"><?php echo ucfirst($val['state_name']);?></option>
                                        <?php } ?>
                                    </select>
                            </div>
                            <div class="col-xs-4">
                                <label for="exampleInputEmail1">Zip Code</label>
                                <input type="text"name="zip" class="form-control required" id="cust_zip" value="<?php echo $cust->cust_zip; ?>" placeholder="Zip Code" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-6">
                                <label for="exampleInputEmail1">Birth Date</label>
                                <input type="date" class="form-control" id="cust_birthdate" value="<?php echo $cust->cust_birthdate; ?>" placeholder="Birth Date">
                            </div>
                            <div class="col-xs-6">
                                <label for="exampleInputEmail1">Your School</label>
                                <?php $schl =  $this->Account_model->get_school_id( $cust->school ); 
                                foreach ($schl as $key => $value) {}?>
                                <input type="text" name="school" class="form-control required" id="cust_school" value="<?php if($schl): echo $value->school_name; endif; ?>" placeholder="Your School">
                                <div class="search_results"></div>
                                <input type="hidden" id="school_id" value="<?php echo $cust->school; ?>" placeholder="Your School">
                            </div>
                            
                        </div>
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="card card-block sameheight-item">
                    <div class="row form-group">
                        <div class="col-xs-12">
                            <label class="control-label" for="">Public Info (Short Bio)</label>
                            <textarea rows="5" class="form-control" id="cust_bio" value="<?php echo $cust->cust_bio; ?>" placeholder="About Me..."><?php echo $cust->cust_bio; ?></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row form-group">
                        <div class="col-xs-12">                            
                            <div class="title-block">
                                <h3 class="title">Change Your Password</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6 col-xs-12">
                            <label for="">Current Password</label>
                            <input type="password" class="form-control" id="prev_pword" placeholder="Current Password" <?php if( $cust->loggedin_via == 'facebook'): echo 'readonly="readonly"'; endif;?> >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6 col-xs-12">
                            <label for="">New Password</label>
                            <input type="password" class="form-control" id="new_pword" placeholder="New Password" <?php if( $cust->loggedin_via == 'facebook'): echo 'readonly="readonly"'; endif;?> >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6 col-xs-12">
                            <label for="">Confirm Password</label>
                            <input type="password" class="form-control handle_enter" data-enter="update_profile_btn" id="con_pword" placeholder="Confirm Password" <?php if( $cust->loggedin_via == 'facebook'): echo 'readonly="readonly"'; endif;?> >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6 col-xs-12">
                            <br>
                            <button id="update_profile_btn" class="green-btn cartrev-update-btn white gsemibold font-small margin-btn">Update Profile
                                <span class="update-profile-loader">
                                    <img class="img-responsive img-loader" src="<?php echo base_url('public/'.STORE.'/images/tzloader.gif');?>" alt=""/>
                                </span>
                            </button>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>  
<script type="text/javascript">
$(document).ready(function(){
    $("#storetitle").text('TZilla.com - Account Profile Setting');
});
</script>