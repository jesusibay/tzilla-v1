<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class fedex_testapi
{
	var $CI;
	var $server;
	var $package_types;
	var $service_list;
	var $path_to_wsdl;
	var $major;

	var $tracking_details;
	var $print_reply;

	function __construct()
	{
		//we're going to have this information in the back end for editing eventually
		//username password, origin zip code etc.
		$this->CI =& get_instance();
		$this->CI->lang->load('fedex');

		$this->major = 18;
		//$this->path_to_wsdl = APPPATH."third_party/shipping/fedex/libraries/RateService_v14.wsdl";
		//$this->path_to_wsdl = APPPATH."third_party/shipping/fedex/libraries/RateService_v16.wsdl";
		$this->path_to_wsdl = APPPATH."third_party/shipping/fedex/wsdl/RateService_v18.wsdl";

		// Drop Off Types
		$this->dropoff_types['BUSINESS_SERVICE_CENTER'] = lang('BUSINESS_SERVICE_CENTER');
		$this->dropoff_types['DROP_BOX'] = lang('DROP_BOX');
		$this->dropoff_types['REGULAR_PICKUP'] = lang('REGULAR_PICKUP');
		$this->dropoff_types['REQUEST_COURIER'] = lang('REQUEST_COURIER');
		$this->dropoff_types['STATION'] = lang('STATION');

		// Packaging types
		$this->package_types['FEDEX_10KG_BOX'] = lang('FEDEX_10KG_BOX');
		$this->package_types['FEDEX_25KG_BOX'] = lang('FEDEX_25KG_BOX'); 
		$this->package_types['FEDEX_BOX'] = lang('FEDEX_BOX');
		$this->package_types['FEDEX_ENVELOPE'] = lang('FEDEX_ENVELOPE'); 
		$this->package_types['FEDEX_PAK'] = lang('FEDEX_PAK');
		$this->package_types['FEDEX_TUBE'] = lang('FEDEX_TUBE');
		$this->package_types['YOUR_PACKAGING'] = lang('YOUR_PACKAGING');


		// Available Services
		$this->service_list['EUROPE_FIRST_INTERNATIONAL_PRIORITY']	= lang('EUROPE_FIRST_INTERNATIONAL_PRIORITY');
		$this->service_list['FEDEX_1_DAY_FREIGHT']	= lang('FEDEX_1_DAY_FREIGHT'); 
		$this->service_list['FEDEX_2_DAY']	= lang('FEDEX_2_DAY');
		$this->service_list['FEDEX_2_DAY_FREIGHT']	= lang('FEDEX_2_DAY_FREIGHT'); 
		$this->service_list['FEDEX_3_DAY_FREIGHT']	= lang('FEDEX_3_DAY_FREIGHT'); 
		$this->service_list['FEDEX_EXPRESS_SAVER']	= lang('FEDEX_EXPRESS_SAVER');
		$this->service_list['FEDEX_GROUND']	= lang('FEDEX_GROUND'); 
		$this->service_list['FIRST_OVERNIGHT']	= lang('FIRST_OVERNIGHT');
		$this->service_list['GROUND_HOME_DELIVERY']	= lang('GROUND_HOME_DELIVERY'); 
		$this->service_list['INTERNATIONAL_ECONOMY']	= lang('INTERNATIONAL_ECONOMY'); 
		$this->service_list['INTERNATIONAL_ECONOMY_FREIGHT']	= lang('INTERNATIONAL_ECONOMY_FREIGHT'); 
		$this->service_list['INTERNATIONAL_FIRST']	= lang('INTERNATIONAL_FIRST'); 
		$this->service_list['INTERNATIONAL_PRIORITY']	= lang('INTERNATIONAL_PRIORITY'); 
		$this->service_list['INTERNATIONAL_PRIORITY_FREIGHT']	= lang('INTERNATIONAL_PRIORITY_FREIGHT'); 
		$this->service_list['PRIORITY_OVERNIGHT']	= lang('PRIORITY_OVERNIGHT');
		$this->service_list['SMART_POST']	= lang('SMART_POST'); 
		$this->service_list['STANDARD_OVERNIGHT']	= lang('STANDARD_OVERNIGHT'); 
		$this->service_list['FEDEX_FREIGHT']	= lang('FEDEX_FREIGHT'); 
		$this->service_list['FEDEX_NATIONAL_FREIGHT']	= lang('FEDEX_NATIONAL_FREIGHT');
		$this->service_list['INTERNATIONAL_GROUND']	= lang('INTERNATIONAL_GROUND');

	}

	/*tracking shipment*/
	function track($tracking_number='',$fedex_credential=''){
		// Copyright 2009, FedEx Corporation. All rights reserved.
		// Version 6.0.0

		/*load settings*/
		// retrieve settings
		$this->CI->load->module('settings');
		$settings	= $this->CI->settings->get('fedex');

		//check if we're enabled
		if(!$settings['enabled'] || $settings['enabled'] < 1)
		{
			return array();
		}

		if($fedex_credential==''){
			$key 				= $settings['key'];
			$password			= $settings['password'];
			$shipaccount 		= $settings['shipaccount'];
			$meter				= $settings['meter'];	
		}else{
			$key 				= $fedex_credential['key'];
			$password			= $fedex_credential['password'];
			$shipaccount 		= $fedex_credential['account'];
			$meter				= $fedex_credential['meter'];	

		}
		

		
		/*end settings retrieval*/

		//require_once('../../library/fedex-common.php');

		//The WSDL is not included with the sample code.
		//Please include and reference in $path_to_wsdl variable.
		//$path_to_wsdl = "../../wsdl/TrackService_v12.wsdl";
		$path_to_wsdl = APPPATH."third_party/Shipping/fedex/wsdl/TrackService_v12.wsdl";
		
		ini_set("soap.wsdl_cache_enabled", "0");

		$client = new SoapClient($path_to_wsdl, array('trace' => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information
		
		$request['WebAuthenticationDetail'] = array(
			'ParentCredential' => array(
				'Key' => '', 
				'Password' => ''
			),
			'UserCredential' => array(
				'Key' => $key, 
				'Password' => $password
			)
		);

		$request['ClientDetail'] = array(
			'AccountNumber' => $shipaccount, 
			'MeterNumber' => $meter
		);
		$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Track Request using PHP ***');
		$request['Version'] = array(
			'ServiceId' => 'trck', 
			'Major' => '12', 
			'Intermediate' => '0', 
			'Minor' => '0'
		);
		$request['SelectionDetails'] = array(
			'PackageIdentifier' => array(
				'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
				'Value' => $tracking_number // Replace 'XXX' with a valid tracking identifier
			)
		);

		$return = array();

		try {
			if($this->setEndpoint('changeEndpoint')){
				$newLocation = $client->__setLocation($this->setEndpoint('endpoint'));
			}
			
			$response = $client ->track($request);

		    if ($response -> HighestSeverity != 'FAILURE' && $response -> HighestSeverity != 'ERROR'){
				if($response->HighestSeverity != 'SUCCESS'){
					$return['body'] = '<table border="1">';
					$return['body'] .= '<tr><th>Track Reply</th><th>&nbsp;</th></tr>';
					$this->trackDetails($response->Notifications, ''); 
					$return['body'] .= $this->tracking_details; 					
					$return['body'] .= '</table>';					
				}else{
			    	if ($response->CompletedTrackDetails->HighestSeverity != 'SUCCESS'){
						$return['body'] = '<table border="1">';
					    $return['body'] .= '<tr><th>Shipment Level Tracking Details</th><th>&nbsp;</th></tr>';
					    $this->trackDetails($response->CompletedTrackDetails, '');
					    $return['body'] .= $this->tracking_details;
						$return['body'] .= '</table>';
					}else{
						$return['body'] = '<table border="1">';
					    $return['body'] .= '<tr><th>Package Level Tracking Details</th><th>&nbsp;</th></tr>';
					    $this->trackDetails($response->CompletedTrackDetails->TrackDetails, '');
					    $return['body'] .= $this->tracking_details;
						$return['body'] .= '</table>';
					}
				}
		        $this->printReply($client, $response);
		        $return['success_data'] = $this->print_reply;
		    }else{
		        $this->printReply($client, $response);
		        $return['error_data'] = $this->print_reply;
		    }

		    return $return; 
		    
		    //writeToLog($client);    // Write to log file   
		} catch (SoapFault $exception) {
		    $this->printFault($exception, $client);
		}
	}

	function trackDetails($details, $spacer){
		foreach($details as $key => $value){
			if(is_array($value) || is_object($value)){
	        	$newSpacer = $spacer. '&nbsp;&nbsp;&nbsp;&nbsp;';
	    		$this->tracking_details .= '<tr><td>'. $spacer . $key.'</td><td>&nbsp;</td></tr>';
	    		$this->trackDetails($value, $newSpacer);
	    	}elseif(empty($value)){
	    		$this->tracking_details .= '<tr><td>'.$spacer. $key .'</td><td>'.$value.'</td></tr>';
	    	}else{
	    		$this->tracking_details .= '<tr><td>'.$spacer. $key .'</td><td>'.$value.'</td></tr>';
	    	}
	    }
	}

	function printReply($client, $response){		
		$highestSeverity=$response->HighestSeverity;
		if($highestSeverity=="SUCCESS"){ $this->print_reply .= '<h2>The transaction was successful.</h2>';}
		if($highestSeverity=="WARNING"){$this->print_reply .= '<h2>The transaction returned a warning.</h2>';}
		if($highestSeverity=="ERROR"){$this->print_reply .= '<h2>The transaction returned an Error.</h2>';}
		if($highestSeverity=="FAILURE"){$this->print_reply .= '<h2>The transaction returned a Failure.</h2>';}
		
		$this->printNotifications($response -> Notifications);
		$this->printRequestResponse($client, $response);
	}

	function printNotifications($notes){		
		foreach($notes as $noteKey => $note){
			if(is_string($note)){    
	            $this->print_reply .= $noteKey . ': ' . $note . '<br/>';
	        }
	        else{
	        	$this->printNotifications($note);
	        }
		}		
	}
	function printRequestResponse($client){
		$this->print_reply .= '<h2>Request</h2>' . "\n";
		$this->print_reply .= '<pre>' . htmlspecialchars($client->__getLastRequest()). '</pre>';  
		$this->print_reply .= "\n";
	   
		$this->print_reply .= '<h2>Response</h2>'. "\n";
		$this->print_reply .= '<pre>' . htmlspecialchars($client->__getLastResponse()). '</pre>';
		$this->print_reply .= "\n";
	}

	function setEndpoint($var){
		if($var == 'changeEndpoint') Return false;
		if($var == 'endpoint') Return 'XXX';
	}
	/*END tracking shipment*/

	function rates($ship_address='',$weight_set=0,$service_type='',$smart_post_detail='',$package_count=1)
	{

		//$this->CI->load->library('session');

		// get customer info
		//$customer = $this->CI->go_cart->customer();

		// $customer_address = $customer['ship_address'];
		// $customer_address = $customer['ship_address'];
		
		if($ship_address){
			$customer_address = $ship_address;
		}

		// Weight of order
		//$weight	= $this->CI->go_cart->order_weight();
		//if($weight== null || $weight==''){
			$weight = $weight_set;
		//}
		
		
		// retrieve settings
		$this->CI->load->module('settings');
		$settings	= $this->CI->settings->get('fedex');
		
		//print_r($settings);

		if(empty($settings)){
			//print_r('etet');
			return json_encode(array('status'=>'warning','message'=>'settings is empty'));
			//exit();	
		} 

		//check if we're enabled
		if(!$settings['enabled'] || $settings['enabled'] < 1)
		{
			return array();
		}

		$key 				= $settings['key'];
		$password			= $settings['password'];
		$shipAccount 		= $settings['shipaccount'];
		$meter				= $settings['meter'];
		$dropofftype 		= $settings['dropofftype'];
		$service			= explode(',',$settings['service']);
		$package 			= $settings['package'];
		$handling_method	= $settings['handling_method'];
		$handling_amount	= $settings['handling_amount'];
		$pkg_width	 		= $settings['width'];
		$pkg_height			= $settings['height'];
		$pkg_length 		= $settings['length'];
		$insurance 			= $settings['insurance'];
		$billAccount 		= $shipAccount;



		// Build Request

		ini_set("soap.wsdl_cache_enabled", "0");
		 
		$client = new SoapClient($this->path_to_wsdl, array('trace' => 1, "exceptions" => 1)); // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

		$request['WebAuthenticationDetail'] = array(
			'ParentCredential' => array(
				'Key' => $key,
				'Password' => $password
			),
			'UserCredential' =>array(
				'Key' => $key,
				'Password' => $password
			)
		); 
		$request['ClientDetail'] = array(
			'AccountNumber' => $shipAccount,
			'MeterNumber' => $meter
		);
		$request['TransactionDetail'] = array('CustomerTransactionId' => '*** Rate Request using PHP ***');

		$request['Version'] = array(
			'ServiceId' => 'crs', 
			'Major' => $this->major, 
			'Intermediate' => '0', 
			'Minor' => '0'
		);
		

		$shipper	= $this->CI->settings->get('shipper');

		//print_r($shipper);

		$request['ReturnTransitAndCommit'] = false;
		$request['RequestedShipment']['RequestedCurrency'] = $shipper['currency'];
		$request['RequestedShipment']['DropoffType'] = $dropofftype;
		//$request['RequestedShipment']['ShipTimestamp'] = date('c');
		$request['RequestedShipment']['PackagingType'] = $package; 
		
		if($service_type) $request['RequestedShipment']['ServiceType'] = $service_type;
		
		/*if($insurance=='yes')
		{
			$request['RequestedShipment']['TotalInsuredValue']=array(
				'Amount'=> $this->CI->go_cart->order_insurable_value(),
				'Currency'=> $this->CI->config->item('currency')
			);
		}*/

		$request['RequestedShipment']['Shipper'] = array(
				'Contact' => array(
					'CompanyName' => $shipper['company_name'],
					'EMailAddress' => $shipper['shipper_email']
				),
				'Address' => array(
					'StreetLines' => array($shipper['address1'], $shipper['address2']),
					'City' => $shipper['city'],
					'StateOrProvinceCode' => $shipper['state'],
					'PostalCode' => $shipper['zip'],
					'CountryCode' => $shipper['country']
				)
			);


		$request['RequestedShipment']['Recipient'] =  array(
				'Contact' => array(
					'PersonName' => "{$customer_address['firstname']} {$customer_address['lastname']}",
					'CompanyName' => $customer_address['company'],
					'PhoneNumber' => $customer_address['phone']
				),
				'Address' => array(
					'StreetLines' => array($customer_address['address1'], $customer_address['address2']),
					'City' => $customer_address['city'],
					//'StateOrProvinceCode' => $customer_address['zone'],
					'StateOrProvinceCode' => $customer_address['state'],
					'PostalCode' => $customer_address['zip'],
					'CountryCode' => $customer_address['country_code'],
					//'Residential' => false   // no way to determine this
				)
			);

		$request['RequestedShipment']['RateRequestTypes'] = 'LIST'; // LIST, NONE, PREFERRED
		#$request['RequestedShipment']['RateRequestTypes'] = 'NONE'; // LIST, NONE, PREFERRED
		
		$request['RequestedShipment']['PackageCount'] = $package_count;
		/*
		$package_count
		*/
		$request['RequestedShipment']['RequestedPackageLineItems'] =  array(
				'SequenceNumber'=>1,
				'GroupPackageCount'=>1,
				'Weight' => array(
					'Value' => $weight,
					'Units' => $shipper['weight_unit']
				),
				'Dimensions' => array(
					'Length' => $pkg_length,
					'Width' => $pkg_width,
					'Height' => $pkg_height,
					'Units' => str_replace('INCHES', 'IN',$shipper['dimension_unit'])
				)
			); 
		
		$request['RequestedShipment']['SmartPostDetail'] = $smart_post_detail;
		
		//print_r( $request);
try {
		// Send the request to FedEx
		$response = $client->getRates($request);
		//echo "<pre>";
		
		
		// Handle response
		if ($response->HighestSeverity != 'FAILURE' && $response->HighestSeverity != 'ERROR' )
		{
			
			$rates = array();
			//print_r( $response);
			if(!is_array(@$response->RateReplyDetails))
			{
				//echo "jovani";
				$rateReply = $response -> RateReplyDetails;
				$amount = $rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
					
					if(is_numeric($handling_amount)) // valid entry?
					{
						if($handling_method=='price')
						{
							$amount += $handling_amount;
						}
						elseif($handling_method=='percent')
						{
							$amount += $amount * ($handling_amount/100);
						}
					}
					
					$rates[$rateReply->ServiceType] = number_format($amount,2,".",",");
				//return array('noresults'); // No Results
			}else{ 

				foreach ($response->RateReplyDetails as $rateReply)
				{		  
					//echo "wayne";
					//print_r($rateReply);
					if(in_array($rateReply->ServiceType, $service))
					{
						//print_r( $rateReply->RatedShipmentDetails[0]->ShipmentRateDetail);
						$amount = $rateReply->RatedShipmentDetails[0]->ShipmentRateDetail->TotalNetCharge->Amount;
						$amount = $amount + $rateReply->RatedShipmentDetails[0]->EffectiveNetDiscount->Amount; // plus the discount amount
						//echo $amount;
						if(is_numeric($handling_amount)) // valid entry?
						{
							if($handling_method=='price')
							{
								$amount += $handling_amount;
							}
							elseif($handling_method=='percent')
							{
								$amount += $amount * ($handling_amount/100);
							}
						}
						//$langservice = $rateReply->ServiceType;
						
						//if(array_key_exists($rateReply->ServiceType,$this->service_list)){
						//	if($this->service_list[$rateReply->ServiceType]){
								//$langservice = $rateReply->ServiceType;
								//$langservice = $this->service_list[$rateReply->ServiceType];
						//	}
						//}
						
						$rates[$rateReply->ServiceType] = number_format($amount,2,".",",");
					}
				}
			}

			return $rates;
		}
		else
		{
			//echo "fail to fill in rates";
			//print_r( $response);
			//return json_encode($response);
			return array('failed'); // fail
		}
	}catch (SoapFault $exception) {
   		$this->printFault($exception, $client);        
	}

	}

	/**
	 *  Print SOAP Fault
	 */  
	function printFault($exception, $client) {
	    echo '<h2>Fault</h2>' . "<br>\n";                        
	    echo "<b>Code:</b>{$exception->faultcode}<br>\n";
	    echo "<b>String:</b>{$exception->faultstring}<br>\n";
	    $this->writeToLog($client);
	    
	    echo '<h2>Request</h2>' . "\n";
		echo '<pre>' . htmlspecialchars($client->__getLastRequest()). '</pre>';  
		echo "\n";
	}

	/**
	 * SOAP request/response logging to a file
	 */                                  
	function writeToLog($client){  
		/**
		 * __DIR__ refers to the directory path of the library file.
		 * This location is not relative based on Include/Require.
		 */
		if (!$logfile = fopen(__DIR__.'/fedextransactions.log', "a"))
		{
	   		error_func("Cannot open " . __DIR__.'/fedextransactions.log' . " file.\n", 0);
	   		exit(1);
		}
		fwrite($logfile, sprintf("\r%s:- %s",date("D M j G:i:s T Y"), $client->__getLastRequest(). "\r\n" . $client->__getLastResponse()."\r\n\r\n"));
	}

	function install()
	{
		$default_settings	= array(
			'enabled'=>0,
			'key'=>'',
			'password'=>'',
			'meter'=>'',
			'shipaccount'=>'',
			'handling_method'=>'Price',
			'handling_amount'=>5,
			'length' => 10,
			'width' => 10,
			'height' => 3,
			'dropofftype'=>'',
			'insurance'=>'',
			'package'=>'YOUR_PACKAGING',
			'service' => implode(',', array_keys($this->service_list))
			);
		//set a default blank setting for flatrate shipping
		$this->CI->Settings_model->save_settings('fedex', $default_settings);
	}

	function uninstall()
	{
		$this->CI->Settings_model->delete_settings('fedex');
	}

	function form($post	= false)
	{
		$this->CI->load->helper('form');

		$data['service_list'] = $this->service_list;
		$data['package_types'] = $this->package_types;
		$data['dropoff_types']	= $this->dropoff_types;

		if(!$post)
		{
			$settings		= $this->CI->Settings_model->get_settings('fedex');

			$data['package']			= $settings['package'];
			$data['service']			= explode(',', $settings['service']);
			$data['dropofftype'] 		= $settings['dropofftype'];
			$data['key']				= $settings['key'];
			$data['shipaccount']		= $settings['shipaccount'];
			$data['meter']				= $settings['meter'];
			$data['password']			= $settings['password'];
			$data['handling_method']	= $settings['handling_method'];
			$data['handling_amount']	= $settings['handling_amount'];
			$data['height'] 			= $settings['height'];
			$data['width'] 				= $settings['width'];
			$data['length']				= $settings['length'];
			$data['insurance'] 			= $settings['insurance'];
			$data['enabled']			= $settings['enabled'];
		}
		else
		{
			$data['package']			= $post['package'];
			$data['service'] 			= $post['service'];
			$data['dropofftype'] 		= $post['dropofftype'];
			$data['key']				= $post['key'];
			$data['password']			= $post['password'];
			$data['shipaccount']		= $post['shipaccount'];
			$data['meter']				= $post['meter'];
			$data['handling_method']	= $post['handling_method'];
			$data['handling_amount']	= $post['handling_amount'];
			$data['height'] 			= $post['height'];
			$data['width']				= $post['width'];
			$data['length']				= $post['length'];
			$data['insurance'] 			= $post['insurance'];
			$data['enabled']			= $post['enabled'];

		}

	return $this->CI->load->view('admin_form', $data, true);

}

function check()
{	
	$error	= false;


	//count the errors
	if($error)
	{
		return $error;
	}
	else
	{

		$save = $_POST;
		$save['service'] = implode(',', $save['service']);

		//we save the settings if it gets here
		$this->CI->Settings_model->save_settings('fedex', $save);

		return false;
	}
}
}
