<?php (defined('BASEPATH')) or exit('No direct script access allowed');

/** load the CI class for Modular Extensions **/
require dirname(__file__) . '/Base.php';

/**
 * Modular Extensions - HMVC
 *
 * Adapted from the CodeIgniter Core Classes
 * @link	http://codeigniter.com
 *
 * Description:
 * This library replaces the CodeIgniter Controller class
 * and adds features allowing use of modules and the HMVC design pattern.
 *
 * Install this file as application/third_party/MX/Controller.php
 *
 * @copyright	Copyright (c) 2015 Wiredesignz
 * @version 	5.5
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 **/
class MX_Controller
{
    public $autoload = array();

    public function __construct()
    {
        $class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class
            ($this));
        log_message('debug', $class . " MX_Controller Initialized");
        Modules::$registry[strtolower($class)] = $this;

        /* copy a loader instance and initialize */
        $this->load = clone load_class('Loader');
        $this->load->initialize($this);

        /* autoload module items */
        $this->load->_autoloader($this->autoload);
    }

    public function __get($class)
    {
        return CI::$APP->$class;
    }
    
    public function encodeArtImage($var1 = '')
    {
        if ($var1 == "") {
            $var1 = $_REQUEST['var1'];
        }
        $sant = str_replace('localhost', '69.43.181.56', $var1);

        $headers = get_headers($sant);
        $response = substr($headers[0], 9, 3);
        if ($response != "404") {
            
            $image = base64_encode(file_get_contents($sant));
            //echo $_REQUEST['var1'];
            if (isset($_REQUEST['var1'])) {
                echo "data:image/png;base64," . $image;
            }
            return "data:image/png;base64," . $image;
        } else {
            return "";
        }
       
    }

    public function audit($action, $CustomerNumber, $SalesOrderNumber, $lineOrderItems,
        $Fields, $Old, $New, $Transaction)
    {
        $aInsert = array(
            'action' => $action,
            'CustomerNumber' => $CustomerNumber,
            'SalesOrderNumber' => $SalesOrderNumber,
            'lineOrderItems' => $lineOrderItems,
            'Fields' => $Fields,
            'Old' => $Old,
            'New' => $New,
            'Transaction' => $Transaction,
            'CreatedBy' => $this->session->userdata('id').'-'.$this->session->userdata('name'));
        $this->db->insert('scp_audit_trail',$aInsert);
    }
}
