<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MX_Controller {

	/**
	 * Index Page for this controller.
	 **
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{ 
		
		if(STORE == 'scp_admin') redirect('/admin');

		//$this->output->enable_profiler(TRUE);
		
		$this->load->view_store('home');
	}

	function load($page_view)
	{
		
		if(STORE == 'scp_admin') redirect('/admin');

		//$this->output->enable_profiler(TRUE);
		
		$page_title = 'title of the page here...';
		$data['title'] = $page_title;
		$data['content'] = $page_view;

		//echo ' var 1 = '.$var_1;

		//print_r($page_view);

		$uri_params = $this->uri->uri_to_assoc();
		$data['uri_assoc'] = $uri_params;

		$data['uri_segments'] = $this->uri->segment_array();

		$this->load->view_store( 'index', $data );

	}
	
}
