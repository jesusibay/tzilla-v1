<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require 'fb-src/facebook.php';

class Auth_fb {
	
	var $CI;
	var $FB;
	var $FBuser;
	var $FBParams;
	
	public function __construct(){
		
		$this->CI =& get_instance();

		$this->FBParams['init']['appId']  = $this->CI->config->item('fb_app_id');
		$this->FBParams['init']['secret'] = $this->CI->config->item('fb_app_secret');		
		$this->FBParams['scope']		  = $this->CI->config->item('fb_app_scope');		
		$this->FB = new Facebook($this->FBParams['init']);
			
	}
		
	function getUserDetails()
	{
						
		$this->FBuser = $this->FB->getUser();
		
		if($this->FBuser)
		{
			// profile details we have access to are [id, username, name, email]	
			
			try {
				return $this->FB->api('/me');
			}catch (FacebookApiException $e){
				return false;
			}
				 				
		}
		
		return false;
		
	}
		
	function getLoginUrl()
	{
				
		$this->FBParams['scope'] = 'email';
		
		return $this->FB->getLoginURL($this->FBParams['scope']);
		
	}
	
	function logoutUser()
	{
		$this->FB->destroySession();
	}
		
	function getContent($redirect)
	{
		
		
		$strContent = "
		<div id='fb-root'></div>
		<script text='text/javascript'>
		   window.fbAsyncInit = function() {
		     FB.init({appId   : '". $this->FBParams['init']['appId']."',
			          oauth   : true,
			          status  : true, // check login status
			          cookie  : true, // enable cookies to allow the server to access the session			         
			    });
			
			};			
					
			(function() {
			    var e = document.createElement('script');
			    e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
			    e.async = true;
			    document.getElementById('fb-root').appendChild(e);
			}());

			function fb_login(){			    
				FB.login(function(response) {			
			        if (response.authResponse) {			          	
			        	window.location.href='".site_url($redirect)."';			        	
			        }			         
			    }, {scope: '". implode(',',$this->FBParams['scope']) ."'});			   
			}
			  
		</script>";		
		
		return $strContent;
	}
	
}

// END Auth class

