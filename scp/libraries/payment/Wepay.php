<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Wepay{

	var $CI;

	function __construct()
	{
		//we're going to have this information in the back end for editing eventually
		//username password, origin zip code etc.
		$this->CI =& get_instance();
	}
	
	function pay_now($wepay_data)
	{

		// retrieve settings
		$this->CI->load->module('settings');
		$settings		= $this->CI->settings->get('wepay');

		$api_mode 		= $settings['api_mode'];

		if($api_mode == 1){
			$client_id 		= $settings['client_id_live'];
	 		$client_secret 	= $settings['client_secret_live'];
	 		$access_token 	= $settings['access_token_live'];	 		
	 		$account_id 	= $settings['account_id_live'];
		}else{
			$client_id 		= $settings['client_id_dev'];
	 		$client_secret 	= $settings['client_secret_dev'];
	 		$access_token 	= $settings['access_token_dev'];	 		
	 		$account_id 	= $settings['account_id_dev'];
		}

		//echo $client_id;
 		

		/*do some validations first*/


		//$response = array('message'=>$data, $_POST);
		
		//$params =  array('token'=>$access_token);
		

		
		$this->CI->load->add_package_path(APPPATH.'third_party/Payment/wepay/');

		$this->CI->load->library('wepay_api',$access_token);
		
		//$this->CI->load->library('wepay_sdk',$params);
		
		#echo $this->wepay_sdk->test();
		
		// change to useProduction for live environments
		if($api_mode == 0)
		{
			$this->CI->wepay_api->useStaging($client_id, $client_secret);
		}else{
			$this->CI->wepay_api->useProduction($client_id, $client_secret);
		}

		//$wepay = $this->wepay_api->new WePay($access_token);
		
		/**
		 * Make the API request to get the checkout_uri
		 * 
		 */
		$error_message = '';
		
		try 
		{
			$wepay_data['account_id'] = $account_id;

			$response = $this->CI->wepay_api->request('/checkout/create', $wepay_data);
		
		} catch (WePayException $e) { // if the API call returns an error, get the error message for display later
			
			$error_message = $e->getMessage();

			/*todo: MODULE FOR SYSTEM ERRORS EMAIL NOTIFICATION*/		
			// if ( $response->error_code == 2004 ) {
			/*if ( $response->error_code > 0 ) 
			{
				$message  = 'Unable to charge payment method: General Decline';

				$system_error_data = array('subject'=>'General Decline', 'message' => $error_message ); 
				
				//Modules::run('emailer/system_notifications/error', $system_error_data);
			}*/

			$response = $e;
		}

		return json_encode($response);
	}
	
}