###################
Development setup:
###################


*******************
Create an entry to your host file:
*******************

C:\Windows\System32\drivers\etc\hosts

----

127.0.0.1	teelocity.loc
127.0.0.1	tzilla.loc
127.0.0.1	scp.loc

-----

*******************
Create a virtual hosts in your apache:
*******************

my vhost is located:
C:\xampp\apache\conf\extra\httpd-vhosts.conf

------


<VirtualHost *:80>
  DocumentRoot "C:\xampp\htdocs"
  ServerName localapp
</VirtualHost>


# site 1 domain

<VirtualHost *:80>
    ServerName teelocity.loc
    DocumentRoot C:\xampp\htdocs\SCP
</VirtualHost>

# site 2 domain

<VirtualHost *:80>
    ServerName tzilla.loc
    DocumentRoot C:\xampp\htdocs\SCP
</VirtualHost>

# site 3 domain

<VirtualHost *:80>
    ServerName scp.loc
    DocumentRoot C:\xampp\htdocs\SCP
</VirtualHost>

------
*******************
Access the site on your browser:
*******************
1. teelocity.loc
2. tzilla.loc
3. scp.loc - the scp administration site


-----
*******************
The Platform
*******************

SCP is a multi-store, multi-domain, 1 admin backend platform.

It is made on top of Codeigniter 3 Framework. 
It runs a modular (HMVC) extension.

Reference: https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/overview

Modules are located here:

| scp\modules\<module>

----

Since it is a multi-domain platform, we have to define the store in scp/config/routes.php like this:

define('STORE','teelocity');
define('STOREID','1');

we can also dynamically change the STORE and STOREID values via database.

//TODO SQL query
switch ($_SERVER['HTTP_HOST']) {      
  case 'teelocity.loc':
    define('STORE','teelocity');
    define('STOREID','1');
    break;
  case 'tzilla.loc':
    define('STORE','tzilla');
    define('STOREID','2');
    break;  
  case 'scp.loc':
    define('STORE','SCP_ADMIN');
    define('STOREID','3');
    break;
  default:
    define('STORE','SCP_ADMIN');
    break;
} 

----

To keep view files and asset files organized and separated from admin and store,
It has overrides for Loader class:

1. $this->load->view_store('<view_file>');
2. $this->load->view_admin('<view_file>');

View files for a store is located here:

scp\stores\<store_name>\views


Asset files for a store is located here:

public\<store_name>\img
public\<store_name>\js
public\<store_name>\css
public\<store_name>\font

----

Admin who manages all stores is a one big module located here:
| scp\modules\admin

It has it's own controllers, models and views.

To load admin views:  

$this->load->view_admin('<view_file>');

view files are located here: 
| scp\modules\admin\views

asset files are located here:

public\scp_admin\img
public\scp_admin\js
public\scp_admin\css
public\scp_admin\font

----
*******************
Admin module can load other modules.
*******************

To load a module in a view file:

<?php

$this->load->module('product');

$product = $this->product->get_by_slug($uri_segments[1]);

print_r($product);
?>
----
<?php

$this->load->module('product');

$product = $this->product->get_by_id(1);

print_r($product);

?>
----

<?php

$this->load->module('content');

$product = $this->content->get_by_slug($uri_segments[1]);

print_r($product);
?>

----

*******************
SCP URLs are saved on app_routes table
*******************

--
-- Table structure for table `app_routes`
--

CREATE TABLE IF NOT EXISTS `app_routes` (
`id` bigint(20) NOT NULL,
  `store_id` int(11) NOT NULL,
  `slug` varchar(192) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `app_routes`
--

INSERT INTO `app_routes` (`id`, `store_id`, `slug`, `controller`) VALUES
(1, 1, 'about', 'page/load/about'),
(2, 1, 'categories', 'page/load/categories'),
(3, 1, 'a-test-product-slug', 'page/load/product/123/4/5/6'),
(4, 1, 'terms-and-conditions', 'page/load/content/456'),
(5, 1, 'b-test-product-slug', 'page/load/product'),
(6, 1, 'c-test-product-slug', 'page/load/product');


page/load/content:

database driven contents, good for static informational contents. so editing will be via updating the table entry and not by manually doing it on the view files.


page/load/product:

is for product page. the view files is located here: 
| scp\stores\<store_name>\views\product.php

page/load/categories:

is for category products page. located here:
| scp\stores\<store_name>\views\categories.php


you can also create physical view files if needed and create an entry to app_routes like this:

page/load/<view_file_name>

e.g.:

page/load/update_profile


----

On \scp\controllers\Page.php, we have a load method.
It is responsible for loading store pages or contents.

On view files, you can get the url variables via $uri_assoc, and/or uri_segments.

reference: https://www.codeigniter.com/userguide3/libraries/uri.html





